const VueLoaderPlugin = require('vue-loader/lib/plugin')
const path = require('path')
const utils = require('./utils')
const vueLoaderConfig = require('./vue-loader.conf')

module.exports = {
  components: 'src/**/*.{vue,js,jsx,ts,tsx}',
  ignore: ['src/vue-lib.js'],
  assetsDir: path.join(__dirname, './assets'),
  require: [
    // path.join(__dirname, './assets/css/variables.scss')
    path.join(__dirname, './modernizr-custom.js')
  ],
  webpackConfig: {
    resolve: {
      extensions: ['.js', '.json', '.vue', '.ts', '.css', '.scss'],
      alias: {
        '~': path.resolve(__dirname, 'src'),
        '@': path.resolve(__dirname, 'src')
      }
    },
    module: {
      rules: [
        {
          test: /\.vue$/,
          loader: 'vue-loader',
          options: vueLoaderConfig
        },
        {
          test: /(\.jsx|\.js)$/,
          loader: 'babel-loader',
          options: {
            comments: false,
            cacheDirectory: true
          },
          exclude: /node_modules/
        },
        {
          test: /\.(png|jpe?g|gif)(\?.*)?$/,
          loader: 'file-loader',
          options: {
            // name: path.join(__dirname, './assets')('img/[name].[hash:7].[ext]')
          }
        }
        // {
        //   test: /\.(js|vue|jsx)$/,
        //   loader: 'eslint-loader',
        //   options: {
        //     formatter: require('eslint-friendly-formatter'),
        //     emitWarning: true
        //   },
        //   exclude: /node_modules/
        // }
      ].concat(utils.styleLoaders({
        sourceMap: false,
        extract: false,
        usePostCSS: true
      }))
    },
    plugins: [new VueLoaderPlugin()]
  },
  usageMode: 'expand',
  styleguideDir: 'public',
  updateDocs(docs, file) {
    if (docs.doclets && docs.doclets.version) {
      const versionFilePath = path.resolve(
        path.dirname(file),
        docs.doclets.version
      )
      const version = require(versionFilePath).version

      docs.doclets.version = version
      docs.tags.version[0].description = version
    }

    return docs
  }  
  // exampleMode: 'expand'
}
