const path = require('path')

// https://eslint.org/docs/user-guide/configuring
module.exports = {
  root: true,
  parserOptions: {
    parser: 'babel-eslint',
    sourceType: 'module'
  },
  env: {
    browser: true,
  },
  extends: [
    // https://github.com/vuejs/eslint-plugin-vue#priority-a-essential-error-prevention
    // consider switching to `plugin:vue/strongly-recommended` or `plugin:vue/recommended` for stricter rules.
    'plugin:vue/essential',
    // 'plugin:import/errors',
    // 'plugin:import/warnings',
    'plugin:promise/recommended',
    'plugin:compat/recommended',
    // https://github.com/standard/standard/blob/master/docs/RULES-en.md
    'standard'
  ],
  plugins: [
    'import',
    'promise',
    'compat',
    'html',
    // required to lint *.vue files
    'vue'
  ],
  settings: {
    'import/resolver': {
      'eslint-import-resolver-lerna': {
        packages: path.resolve(__dirname, '../../packages')
      },
      webpack: {
        config: path.join(__dirname, './webpack.config.js')
      }
    },
    polyfills: [
      // Example of marking entire API and all methods and properties as polyfilled
      "Promise"
    ]
  },
  // add your custom rules here
  rules: {
    // allow async-await
    'generator-star-spacing': 'off',
    // allow debugger during development
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off'
  }
}
