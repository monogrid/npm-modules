import ElasticVector3 from '@/3d/utils/ElasticVector3'
import ElasticVector2 from '@/3d/utils/ElasticVector2'
import { ElasticNumber } from '@monogrid/js-utils'
import { Vector2 } from 'three-full/sources/math/Vector2'
import { Vector3 } from 'three-full/sources/math/Vector3'
import { Spherical } from 'three-full/sources/math/Spherical'
/*
 * TODO: documentation
 *
 *
 *
 *
 *
 */
export default class ElasticCameraController {
  /**
   *
   * @param {Camera} camera
   * @param {Number} width
   * @param {Number} height
   * @param {Number} sensitivity
   * @param {Number} thetaMax
   * @param {Number} thetaMin
   * @param {Number} phiMax
   * @param {Number} phiMin
   * @param {Number} radiusMin
   * @param {Number} radiusMax
   * @param {Number} radius
   * @param {Vector3} target
   */
  constructor ({ camera, width, height, sensitivity, thetaMax, thetaMin, phiMax, phiMin, radiusMin, radiusMax, radius, target }) {
    this._camera = camera
    this.width = width || 800
    this.height = height || 600

    this.sensitivity = sensitivity || 10
    this.mouseEnabled = true
    this.enabled = true
    this.thetaMax = thetaMax || Number.POSITIVE_INFINITY
    this.thetaMin = thetaMin || Number.NEGATIVE_INFINITY
    this.phiMax = phiMax || Math.PI
    this.phiMin = phiMin || 0
    this.radiusMin = radiusMin || 0
    this.radiusMax = radiusMax || Number.POSITIVE_INFINITY

    this._spherical = new Spherical()
    this._mousePosition = new Vector2(0, 0)
    this._radius = new ElasticNumber(radius || 20)
    this._target = new ElasticVector3(target || new Vector3(0, 0, 0))
    this._phiTheta = new ElasticVector2(new Vector2(Math.PI / 2, 0))
    this._phiThetaDownPosition = new Vector2(0, 0)
    this._mouseDownPosition = new Vector2(0, 0)
    this._mousePickingPosition = new Vector3(0, 0, 0.5)
    this._isMouseDown = false
  }

  set radius (value) {
    this._radius.target = Math.max(this.radiusMin, Math.min(this.radiusMax, value))
  }

  get radius () {
    return this._radius.target
  }

  set immediateRadius (value) {
    this._radius.value = Math.max(this.radiusMin, Math.min(this.radiusMax, value))
  }

  get immediateRadius () {
    return this._radius.value
  }

  set target (value) {
    return this._target.copy(value)
  }

  get target () {
    return this._target
  }

  set immediateTarget (value) {
    return this._target.value.copy(value)
  }

  get immediateTarget () {
    return this._target.value
  }

  get phi () {
    return this._phiTheta.x
  }

  set phi (value) {
    this._phiTheta.x = value
  }

  get immediatePhi () {
    return this._phiTheta.value.x
  }

  set immediatePhi (value) {
    this._phiTheta.value.x = value
  }

  get theta () {
    return this._phiTheta.y
  }

  set theta (value) {
    this._phiTheta.y = value
  }

  get immediateTheta () {
    return this._phiTheta.value.y
  }

  set immediateTheta (value) {
    this._phiTheta.value.y = value
  }

  onMouseMove (event) {
    // if (event.touches.length > 1) {
    //   let touch1 = new Vector2(ElasticCameraController.getMousePositionX(event), ElasticCameraController.getMousePositionY(event))
    //   let touch2 = new Vector2(ElasticCameraController.getMousePositionX(event, 1), ElasticCameraController.getMousePositionY(event, 1))
    //   let distance = touch1.distanceTo(touch2)
    //   let distanceDelta = this.touchDistance - distance
    //   this.radius += distanceDelta * 0.1
    //   this.touchDistance = distance
    //   return
    // }
    this._updateMousePosition(event)
  }

  onMouseDown (event) {
    // if (event.touches.length > 1) {
    //   let touch1 = new Vector2(ElasticCameraController.getMousePositionX(event), ElasticCameraController.getMousePositionY(event))
    //   let touch2 = new Vector2(ElasticCameraController.getMousePositionX(event, 1), ElasticCameraController.getMousePositionY(event, 1))
    //   this.touchDistance = touch1.distanceTo(touch2)
    //   return
    // }
    this._isMouseDown = true
    this._updateMousePosition(event)
    this._mouseDownPosition.copy(this._mousePosition)
    this._phiThetaDownPosition.copy(this._phiTheta)
  }

  onMouseUp (event) {
    this._isMouseDown = false
  }

  static getMousePositionX (event, touchIndex = 0) {
    if (event.touches && event.touches.length > 0) {
      return event.touches[touchIndex].pageX
    }
    return event.pageX
  }

  static getMousePositionY (event, touchIndex = 0) {
    if (event.touches && event.touches.length > 0) {
      return event.touches[touchIndex].pageY
    }
    return event.pageY
  }

  _updateMousePosition (event) {
    this._mousePosition.x = ElasticCameraController.getMousePositionX(event)
    this._mousePosition.y = ElasticCameraController.getMousePositionY(event)

    this._mousePickingPosition.x = (this._mousePosition.x / this.width) * 2 - 1
    this._mousePickingPosition.y = -(this._mousePosition.y / this.height) * 2 + 1
  }

  update (delta) {
    this._target.update(delta)
    this._phiTheta.update(delta)
    this._radius.update(delta)

    if (!(!this._isMouseDown || !this.mouseEnabled)) {
      const mouseDeltaY = this._mouseDownPosition.y - this._mousePosition.y
      const sensitivityScaleY = this.height / this.sensitivity
      this._phiTheta.x = this._phiThetaDownPosition.x + mouseDeltaY / sensitivityScaleY

      const mouseDeltaX = this._mouseDownPosition.x - this._mousePosition.x
      const sensitivityScaleX = this.width / this.sensitivity
      this._phiTheta.y = this._phiThetaDownPosition.y + mouseDeltaX / sensitivityScaleX
    }

    if (isNaN(this._phiTheta.x)) this._phiTheta.x = 0
    if (isNaN(this._phiTheta.y)) this._phiTheta.y = 0

    this._phiTheta.x = Math.max(this.phiMin, Math.min(this.phiMax, this._phiTheta.x))
    this._phiTheta.y = Math.max(this.thetaMin, Math.min(this.thetaMax, this._phiTheta.y))

    if (this.enabled) {
      this._spherical.set(this._radius.value, this._phiTheta.value.x, this._phiTheta.value.y)
      this._spherical.makeSafe()
      this._camera.position.setFromSpherical(this._spherical)
      this._camera.position.add(this._target.value)
      this._camera.lookAt(this._target.value)
    }
  }
}
