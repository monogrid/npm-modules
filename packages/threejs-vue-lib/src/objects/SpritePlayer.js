/**
 *
 */
export class SpritePlayer {
  /**
   *
   * @param {*} texture
   */
  constructor (texture, divisions, fps = 25) {
    this.texture = texture
    this.divisions = divisions
    this.fps = fps
    this.singleX = 1 / this.divisions.x
    this.singleY = 1 / this.divisions.y

    this.reset()
  }

  reset () {
    this.time = 0
    this.texture.repeat.x = this.singleX
    this.texture.repeat.y = this.singleY
    this.texture.offset.x = 0
    this.texture.offset.y = 1 - this.singleY
  }

  /**
   *
   * @param {*} delta
   */
  update (delta) {
    this.time += delta
    const totalIndices = (this.divisions.x * this.divisions.y)
    const spriteIndex = Math.floor(this.time * this.fps) % totalIndices
    this.texture.offset.x = this.singleX * (spriteIndex % this.divisions.x)
    this.texture.offset.y = (1 - this.singleY) - (this.singleY * Math.floor(spriteIndex / this.divisions.y))
    // console.log('playing: ', spriteIndex, this.texture.offset.x, this.texture.offset.y)
  }
}
