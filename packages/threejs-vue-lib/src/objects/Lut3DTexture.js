import { Texture } from 'three-full/sources/textures/Texture'
import { LinearFilter, RGBAFormat, RGBFormat, UnsignedByteType, ClampToEdgeWrapping } from 'three-full/sources/constants'
import { ImageLoader } from 'three-full/sources/loaders/ImageLoader'
import { Color } from 'three-full/sources/math/Color'
import { DataTexture3D } from 'three-full/sources/textures/DataTexture3D'
import parseCubeLUT from 'parse-cube-lut'

const NEUTRAL16X16X16 = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQAAAAAQCAIAAAB2sTYeAAAACXBIWXMAAAsTAAALEwEAmpwYAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAAAEtJREFUeNrs07ENACAQA7G8xALZf1lmoARspUx7k6Qnm9Tf/5n/SgPfEgACAAGAAEAAIAAQAAgABAACAAGAAEAAIAC42QYAAP//AwA/+RI7ehnROQAAAABJRU5ErkJggg=='

export default class Lut3DTexture extends Texture {
  //
  constructor (image, mapping, wrapS, wrapT, magFilter, minFilter, format, type, anisotropy, encoding) {
    super(image, mapping, wrapS, wrapT, magFilter, minFilter, format, type, anisotropy, encoding)
    this.minFilter = LinearFilter
    this.magFilter = LinearFilter
    this.wrapS = this.wrapT = ClampToEdgeWrapping
    this.isLut3DTexture = true
    this.isNeutral = true // this is a neutral lut, postProcessing can deactivate the grading pass
    this.cubeSize = 16
    this.flipY = false
    this.needsUpdate = true
  }

  toDataTexture3D () {
    if (!this.dataTexture3d && this.image && this.image.width !== 0 && this.image.height !== 0) {
      const canvas = document.createElement('canvas')
      canvas.width = this.image.width
      canvas.height = this.image.height
      const context = canvas.getContext('2d')
      context.drawImage(this.image, 0, 0)

      const dataLength = this.cubeSize * this.cubeSize * this.cubeSize
      // rgb = length * 3
      let data = new Uint8Array(dataLength * 3) // eslint-disable-line
      const canvasData = context.getImageData(0, 0, canvas.width, canvas.height).data
      for (let i = 0; i < canvasData.length; i += 4) {
        const r = canvasData[i]
        const g = canvasData[i + 1]
        const b = canvasData[i + 2]
        const ind = (i / 4) * 3
        data[ind] = r
        data[ind + 1] = g
        data[ind + 2] = b
      }

      const texture = new DataTexture3D(data, this.cubeSize, this.cubeSize, this.cubeSize)
      texture.format = RGBFormat
      texture.type = UnsignedByteType
      texture.wrapR = texture.wrapS = texture.wrapT = ClampToEdgeWrapping
      texture.minFilter = texture.magFilter = LinearFilter
      texture.generateMipmaps = false
      texture.flipY = false
      texture.unpackAlignment = 1
      texture.needsUpdate = true

      texture.isNeutral = this.isNeutral
      texture.isLut3DTexture = this.isLut3DTexture
      texture.cubeSize = this.cubeSize
      this.dataTexture3d = texture
    }
    if (this.dataTexture3d) return this.dataTexture3d
    return this
  }

  copy (source) {
    super.copy(source)
    // needs to clone deeper
    // if (source.image && source.image.src) {
    //   this.image = new Image()
    //   this.image.src = source.image.src
    // }
    this.isNeutral = source.isNeutral
    this.isLut3DTexture = source.isLut3DTexture
    this.cubeSize = source.cubeSize
    this.dataTexture3d = null
    return this
  }

  getNeutral () {
    this.image = new Image()
    this.isNeutral = true // this is a neutral lut, postProcessing can deactivate the grading pass
    this.image.src = NEUTRAL16X16X16
    this.cubeSize = 16
    this.dataTexture3d = null
    return this
  }

  parse (data) {
    const lut = parseCubeLUT(data)
    this.cubeSize = lut.size
    this.format = RGBAFormat
    this.isNeutral = false
    const width = lut.size * lut.size
    const height = lut.size
    const canvas = document.createElement('canvas')
    const ctx = canvas.getContext('2d')
    canvas.width = width
    canvas.height = height

    const pixel = new Color()

    for (let i = 0; i < lut.size; i++) {
      const slice = document.createElement('canvas')
      const sliceCtx = slice.getContext('2d')

      sliceCtx.scale(-1, 1)
      sliceCtx.rotate(90 * Math.PI / 180)

      for (let x = 0; x < lut.size; x++) {
        for (let y = 0; y < lut.size; y++) {
          sliceCtx.fillStyle = '#' + pixel.fromArray(lut.data.shift()).getHexString()
          sliceCtx.fillRect(x, y, 1, 1)
        }
      }

      ctx.drawImage(slice, lut.size * i, 0)
    }
    this.image = canvas
    this.needsUpdate = true
    this.dataTexture3d = null
    return this
  }

  load (url, onLoad, onProgress, onError) {
    const loader = new ImageLoader()
    loader.load(url, (image) => {
      this.image = image
      this.cubeSize = image.height
      this.format = RGBAFormat
      this.needsUpdate = true
      this.isNeutral = false
      this.dataTexture3d = null
      if (onLoad !== undefined) {
        this.toDataTexture3D()
        onLoad(this)
      }
    }, onProgress, onError)

    return this
  }
}
