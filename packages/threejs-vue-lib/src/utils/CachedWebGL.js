import { WebGL } from 'three-full/sources/helpers/WebGL'

let cachedIsWebGL2Available
const isWebGL2Available = () => {
  if (cachedIsWebGL2Available === undefined) {
    cachedIsWebGL2Available = WebGL.isWebGL2Available()
  }
  return cachedIsWebGL2Available
}

let cachedIsWebGLAvailable
const isWebGLAvailable = () => {
  if (cachedIsWebGLAvailable === undefined) {
    cachedIsWebGLAvailable = WebGL.isWebGLAvailable()
  }
  return cachedIsWebGLAvailable
}

export { isWebGL2Available, isWebGLAvailable }
