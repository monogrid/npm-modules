/**
 * ES6 Promise Wrapper for threejs Loaders
 *
 * usage:
 *
 * import { promiseLoad, promiseParse } from 'loaderPromise'
 *
 * let gltfLoader = new GLTFLoader()
 * let gltf = await promiseParse(gltfLoader, gltfJSON, '')
 *
 * or
 *
 * let envMapLoader = new CubeTextureLoader()
 * let envMap = await promiseLoad(envMapLoader, [...images])
 *
 * @param loader
 * @param fn
 * @param args
 * @returns {Promise<any>}
 */
const execute = function (loader, fn, ...args) {
  return new Promise((resolve, reject) => {
    // define callbacks
    const onComplete = (result) => {
      resolve(result)
    }
    const onProgress = (progress) => {
      // in es6 wen can't handle progress :\
    }
    const onError = (error) => {
      reject(error)
    }
    const loaderCallbacks = fn === 'load' ? [onComplete, onProgress, onError] : [onComplete, onError]
    loader[fn].apply(loader, args.concat(loaderCallbacks))
  })
}

const promiseLoad = function (loader, ...args) {
  return execute.apply(null, [loader, 'load'].concat(args))
}

const promiseParse = function (loader, ...args) {
  return execute.apply(null, [loader, 'parse'].concat(args))
}

export { promiseLoad, promiseParse }
