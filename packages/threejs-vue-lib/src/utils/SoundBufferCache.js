import { AudioLoader } from 'three-full/sources/loaders/AudioLoader'

class SoundBufferCache {
  constructor () {
    this.cache = {}
  }

  load (path, sound) {
    if (this.cache[path]) {
      sound.setBuffer(this.cache[path])
      sound.play()
      return
    }
    const loader = new AudioLoader()
    loader.load(path, buffer => {
      this.cache[path] = buffer
      sound.setBuffer(buffer)
      sound.play()
    })
  }
}

export default new SoundBufferCache()
