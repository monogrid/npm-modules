import { Vector3 } from 'three-full/sources/math/Vector3'

const tempVector3 = new Vector3()
/**
 *
 * @param light
 * @param boundingSphere
 * @param farOffset
 */
const directionalShadowToBoundingSphere = function (light, boundingSphere, farOffset = 0) {
  if (boundingSphere && light) {
    light.shadow.camera.bottom = -boundingSphere.radius
    light.shadow.camera.left = -boundingSphere.radius

    light.shadow.camera.top = boundingSphere.radius
    light.shadow.camera.right = boundingSphere.radius

    light.getWorldPosition(tempVector3)

    light.shadow.camera.near = tempVector3.distanceTo(boundingSphere.center) - boundingSphere.radius
    light.shadow.camera.far = light.shadow.camera.near + (boundingSphere.radius * 2) + farOffset
    light.shadow.camera.updateProjectionMatrix()
  }
}

export { directionalShadowToBoundingSphere }
