import { FileUtils } from '@monogrid/js-utils'
import ChromeColorPicker from 'vue-color/src/components/Chrome.vue'

export default [
  {
    match (object) {
      return object && object.isColor
    },
    getControl (h, target, prop, path) {
      return (
        <div class="wrap-collabsible">
          <input id={path} class="toggle" type="checkbox" />
          <label for={path} class="lbl-toggle set-border-color" style={{
            margin: '2px 0',
            width: '100%',
            height: '20px',
            backgroundColor: '#' + target[prop].getHexString()
          }}/>
          <div class="collapsible-content">
            <ChromeColorPicker disableAlpha={true} value={'#' + target[prop].getHexString()} on-input={(value) => { target[prop].set(value.hex) }} />
          </div>
        </div>
      )
    }
  },
  {
    match (object) {
      return object && object.isLut3DTexture
    },
    getControl (h, target, prop, path) {
      let backImage = null
      if (target[prop] && target[prop].image) {
        const image = target[prop].image
        if (image.src) {
          backImage = 'url(' + image.src + ')'
        } else if (typeof target[prop].image.toDataURL === 'function') {
          // canvas
          backImage = 'url(' + image.toDataURL() + ')'
        }
      }
      const style = {
        width: '100%',
        height: '20px',
        cursor: 'pointer',
        backgroundImage: backImage,
        backgroundSize: 'cover',
        backgroundPosition: '50% 50%',
        display: 'inline-block',
        boxSizing: 'border-box'
      }
      const onclick = async () => {
        const files = await FileUtils.loadFile('.cube')
        const fr = new FileReader()
        fr.onload = (event) => {
          target[prop].parse(event.target.result)
        }
        fr.readAsText(files[0])
      }
      return (
        <div id={path} style={style} on-click={onclick} class="set-border-color" />
      )
    }
  },
  {
    match (object) {
      return object && object.isTexture
    },
    getControl (h, target, prop, path) {
      let backImage = null
      if (target[prop] && target[prop].image) {
        const image = target[prop].image
        if (image.src) {
          backImage = 'url(' + image.src + ')'
        } else if (typeof target[prop].image.toDataURL === 'function') {
          // canvas
          backImage = 'url(' + image.toDataURL() + ')'
        }
      }
      const style = {
        width: '100%',
        height: '20px',
        cursor: 'pointer',
        backgroundImage: backImage,
        backgroundSize: 'cover',
        backgroundPosition: '50% 50%',
        display: 'inline-block',
        boxSizing: 'border-box'
      }
      const onclick = async () => {
        target[prop].image = await FileUtils.loadImage()
        target[prop].needsUpdate = true
      }
      return (
        <div id={path} style={style} on-click={onclick} class="set-border-color" />
      )
    }
  },
  {
    match (object) {
      return object && object.isVector4
    },
    getControl (h, target, prop, path) {
      const xHandler = function (event) { target[prop].x = parseFloat(event.target.value) }
      const yHandler = function (event) { target[prop].y = parseFloat(event.target.value) }
      const zHandler = function (event) { target[prop].z = parseFloat(event.target.value) }
      const wHandler = function (event) { target[prop].w = parseFloat(event.target.value) }
      return (
        <div style={{ whiteSpace: 'nowrap' }}>
          <span style={{ display: 'inline-block', width: '10%', textAlign: 'right' }}>X:</span>
          <input type="number" step="0.1" value={target[prop].x} style={{ width: '16%' }} on-change={xHandler} on-input={xHandler} />
          <span style={{ display: 'inline-block', width: '10%', textAlign: 'right' }}>Y:</span>
          <input type="number" step="0.1" value={target[prop].y} style={{ width: '16%' }} on-change={yHandler} on-input={yHandler}/>
          <span style={{ display: 'inline-block', width: '10%', textAlign: 'right' }}>Z:</span>
          <input type="number" step="0.1" value={target[prop].z} style={{ width: '16%' }} on-change={zHandler} on-input={zHandler}/>
          <span style={{ display: 'inline-block', width: '10%', textAlign: 'right' }}>W:</span>
          <input type="number" step="0.1" value={target[prop].w} style={{ width: '16%' }} on-change={wHandler} on-input={wHandler}/>
        </div>
      )
    }
  },
  {
    match (object) {
      return object && object.isVector3
    },
    getControl (h, target, prop, path) {
      const xHandler = function (event) { target[prop].x = parseFloat(event.target.value) }
      const yHandler = function (event) { target[prop].y = parseFloat(event.target.value) }
      const zHandler = function (event) { target[prop].z = parseFloat(event.target.value) }
      return (
        <div style={{ whiteSpace: 'nowrap' }}>
          <span style={{ display: 'inline-block', width: '10%', textAlign: 'right' }}>X:</span>
          <input type="number" step="0.1" value={target[prop].x} style={{ width: '22%' }} on-change={xHandler} on-input={xHandler} />
          <span style={{ display: 'inline-block', width: '10%', textAlign: 'right' }}>Y:</span>
          <input type="number" step="0.1" value={target[prop].y} style={{ width: '22%' }} on-change={yHandler} on-input={yHandler} />
          <span style={{ display: 'inline-block', width: '10%', textAlign: 'right' }}>Z:</span>
          <input type="number" step="0.1" value={target[prop].z} style={{ width: '22%' }} on-change={zHandler} on-input={zHandler} />
        </div>
      )
    }
  },
  {
    match (object) {
      return object && object.isVector2
    },
    getControl (h, target, prop, path) {
      const xHandler = function (event) { target[prop].x = parseFloat(event.target.value) }
      const yHandler = function (event) { target[prop].y = parseFloat(event.target.value) }
      return (
        <div style={{ whiteSpace: 'nowrap' }}>
          <span style={{ display: 'inline-block', width: '10%', textAlign: 'right' }}>X:</span>
          <input type="number" step="0.1" value={target[prop].x} style={{ width: '33%' }} on-change={xHandler} on-input={xHandler} />
          <span style={{ display: 'inline-block', width: '10%', textAlign: 'right' }}>Y:</span>
          <input type="number" step="0.1" value={target[prop].y} style={{ width: '33%' }} on-change={yHandler} on-input={yHandler} />
        </div>
      )
    }
  }
]
