export default class ElasticVector3 {
  /**
   *
   * @param value
   */
  constructor (value) {
    this.value = value
    this.x = value.x
    this.y = value.y
    this.z = value.z
    this.speed = 3
  }

  /**
   *
   * @param value
   */
  copy (value) {
    this.x = value.x
    this.y = value.y
    this.z = value.z
  }

  /**
   *
   * @param {Number} delta
   */
  update (delta) {
    // delta = Math.min(delta, 1 / 20);

    const distx = this.x - this.value.x
    const disty = this.y - this.value.y
    const distz = this.z - this.value.z
    this.value.x += distx * (this.speed * delta)
    this.value.y += disty * (this.speed * delta)
    this.value.z += distz * (this.speed * delta)
  }
}
