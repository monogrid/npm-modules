/*
 *   ColorGradingPass
 *
 *
 *   let colorGradingPass = new ColorGradingPass()
 *   this.composer.addPass(this.colorGradingPass)
 *
 *   this.colorGradingPass.renderToScreen = true
 *
 *   ------------------------------------------------
 *
 *
 */
import Lut3DTexture from '@/3d/objects/Lut3DTexture'
import { ShaderPass } from 'three-full/sources/postprocessing/ShaderPass'
import { isWebGL2Available } from '@/3d/utils/CachedWebGL'

import vertexShader from './ColorGrading.vert'
import fragmentShader from './ColorGrading.frag'

export default class ColorGradingPass extends ShaderPass {
  //
  constructor () {
    let neutralLut = new Lut3DTexture().getNeutral()
    // get true 3d texture if using webgl2
    if (isWebGL2Available()) {
      neutralLut = neutralLut.toDataTexture3D()
    }
    const shaderSettings = {
      uniforms: {
        tDiffuse: { value: null },
        mixAmount: { value: 0.0 },
        cubeSize1: { value: neutralLut ? neutralLut.cubeSize : 0 },
        cubeSize2: { value: neutralLut ? neutralLut.cubeSize : 0 },
        lutTexture1: { value: neutralLut },
        lutTexture2: { value: neutralLut }
      },
      vertexShader: vertexShader,
      fragmentShader: fragmentShader
    }

    super(shaderSettings)
    this.webgl2 = isWebGL2Available()
    this._lut1 = neutralLut
    this._lut2 = neutralLut
    this.alpha = 0

    // this.enabled = true
    // this.needsSwap = true
    this.clear = false
  }

  setSize (width, height) {
    //
  }

  set lut1 (value) {
    if (value && value.constructor === Lut3DTexture) {
      // get true 3d texture if using webgl2
      if (this.webgl2 === true) {
        this._lut1 = value.toDataTexture3D()
      } else {
        this._lut1 = value
      }
    } else {
      console.warn('[ColorGradingPass] Please use a Lut3DTexture instance')
    }
  }

  get lut1 () {
    return this._lut1
  }

  set lut2 (value) {
    if (value && value.constructor === Lut3DTexture) {
      // get true 3d texture if using webgl2
      if (this.webgl2 === true) {
        this._lut2 = value.toDataTexture3D()
      } else {
        this._lut2 = value
      }
    } else {
      console.warn('[ColorGradingPass] Please use a Lut3DTexture instance')
    }
  }

  get lut2 () {
    return this._lut2
  }

  render (renderer, writeBuffer, readBuffer, delta, maskActive) {
    this.uniforms.lutTexture1.value = this.lut1
    this.uniforms.cubeSize1.value = this.lut1 ? this.lut1.cubeSize : 0
    this.uniforms.lutTexture2.value = this.lut2
    this.uniforms.cubeSize2.value = this.lut2 ? this.lut2.cubeSize : 0
    this.uniforms.mixAmount.value = this.alpha

    super.render(renderer, writeBuffer, readBuffer, delta, maskActive)
  }
}
