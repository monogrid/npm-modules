varying vec2 vUv;

uniform sampler2D tDiffuse; // diffuse texture likely from screen
uniform float mixAmount;  // amount of mix between colorCube0 and colorCube1
uniform float cubeSize1;  // the size of the color cube 1
uniform float cubeSize2;  // the size of the color cube 2

// see if we are in webgl2
#if !defined(gl_FragColor)
uniform sampler2D lutTexture1;  // target colorCube
uniform sampler2D lutTexture2;  // source colorCube

  // trick to get 3D textures with webgl1
  vec4 sampleAs3DTexture(sampler2D texture, vec3 uv, float width) {
    float innerWidth = width - 1.0;
    float sliceSize = 1.0 / width; // space of 1 slice
    float slicePixelSize = sliceSize / width; // space of 1 pixel
    float sliceInnerSize = slicePixelSize * innerWidth; // space of width pixels
    float zSlice0 = min(floor(uv.z * innerWidth), innerWidth);
    float zSlice1 = min(zSlice0 + 1.0, innerWidth);
    float xOffset = slicePixelSize * 0.5 + uv.x * sliceInnerSize;
    float s0 = xOffset + (zSlice0 * sliceSize);
    float s1 = xOffset + (zSlice1 * sliceSize);
    float yPixelSize = sliceSize;
    float yOffset = yPixelSize * 0.5 + uv.y * (1.0 - yPixelSize);
    vec4 slice0Color = texture2D(texture, vec2(s0, yOffset), 0.0);
    vec4 slice1Color = texture2D(texture, vec2(s1, yOffset), 0.0);
    float zOffset = mod(uv.z * innerWidth, 1.0);
    vec4 result = mix(slice0Color, slice1Color, zOffset);
    return result;
  }
#else
precision mediump sampler3D;
uniform sampler3D lutTexture1;  // target colorCube
uniform sampler3D lutTexture2;  // source colorCube
#endif


void main() {
    // TODO: check if we need to apply gamma correction here?
    // vec4 srcColor = mapTexelToLinear(texture2D(tDiffuse, vUv));
    // read the screen texture color
    vec4 srcColor = texture2D(tDiffuse, vUv);

    // read matching color in lutTexture1 and lutTexture2
    #if !defined(gl_FragColor)
      // webgl1
      vec4 color0 = sampleAs3DTexture(lutTexture1, srcColor.rgb, cubeSize1);
      vec4 color1 = sampleAs3DTexture(lutTexture2, srcColor.rgb, cubeSize2);
    #else
      // webgl2
      vec4 color0 = texture(lutTexture1, srcColor.rbg);
      vec4 color1 = texture(lutTexture2, srcColor.rbg);
    #endif

    // mix colors from each color cubes, and keep the alpha from original screen
    gl_FragColor = vec4(mix(color0, color1, mixAmount).rgb, srcColor.a);
    // gl_FragColor = linearToOutputTexel(gl_FragColor);
}
