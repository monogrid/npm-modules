import { Pass } from 'three-full/sources/postprocessing/Pass'
import { OrthographicCamera } from 'three-full/sources/cameras/OrthographicCamera'
import { Scene } from 'three-full/sources/scenes/Scene'
import { LinearFilter, RGBAFormat } from 'three-full/sources/constants'
import { Mesh } from 'three-full/sources/objects/Mesh'
import { WebGLRenderTarget } from 'three-full/sources/renderers/WebGLRenderTarget'
import { ShaderMaterial } from 'three-full/sources/materials/ShaderMaterial'
import { PlaneBufferGeometry } from 'three-full/sources/geometries/PlaneGeometry'

import vertexShader from './StandardRenderShader.vert'
import fragmentShader from './StandardRenderShader.frag'
/*
 * TODO: documentation
 *
 *
 *
 *
 *
 */
export default class StandardRenderPass extends Pass {
  constructor ({ scene1, camera1, scene2, camera2, width, height, clearColor, clearAlpha }) {
    super()

    this.camera = new OrthographicCamera(width / -2, width / 2, height / 2, height / -2, -10, 10)
    this.scene = new Scene()

    const renderTargetParameters = {
      minFilter: LinearFilter,
      magFilter: LinearFilter,
      format: RGBAFormat,
      stencilBuffer: false
    }
    this.clear = true
    this.clearDepth = false
    this.needsSwap = false

    this.width = width
    this.height = height
    this.scene1 = scene1
    this.camera1 = camera1
    this.scene2 = scene2
    this.camera2 = camera2
    this.alpha = 0
    this.clearColor = clearColor
    this.clearAlpha = (clearAlpha !== undefined) ? clearAlpha : 0

    this.useMixTexture = false
    this.mixTextureThreshold = 0.1
    this.mixTexture = null

    this.fbo1 = new WebGLRenderTarget(width, height, renderTargetParameters)
    this.fbo2 = new WebGLRenderTarget(width, height, renderTargetParameters)

    this.quadmaterial = new ShaderMaterial({
      uniforms: {
        tDiffuse1: {
          value: this.fbo1.texture
        },
        tDiffuse2: {
          value: this.fbo2.texture
        },
        mixRatio: {
          value: 0.0
        },
        mixTextureThreshold: {
          value: this.mixTextureThreshold
        },
        useMixTexture: {
          value: this.useMixTexture
        },
        tMixTexture: {
          value: this.mixTexture
        },
        aspectRatio: {
          value: 1920 / 1080
        }
      },
      vertexShader,
      fragmentShader
    })
    this.quad = new Mesh(new PlaneBufferGeometry(width, height), this.quadmaterial)
    this.quad.frustumCulled = false // Avoid getting clipped
    this.scene.add(this.quad)
  }

  render (renderer, writeBuffer, readBuffer, delta, maskActive) {
    this.quadmaterial.uniforms.mixRatio.value = 1 - this.alpha
    // save
    const oldAutoClear = renderer.autoClear
    const oldRenderTarget = renderer.getRenderTarget()
    renderer.autoClear = false
    let oldClearColor
    let oldClearAlpha
    if (this.clearColor) {
      oldClearColor = renderer.getClearColor().getHex()
      oldClearAlpha = renderer.getClearAlpha()
      renderer.setClearColor(this.clearColor, this.clearAlpha)
    }

    // update uniforms
    if (this.useMixTexture && !this.quadmaterial.defines.USE_MIX_TEXTURE) {
      this.quadmaterial.defines.USE_MIX_TEXTURE = true
      this.quadmaterial.needsUpdate = true
    } else if (!this.useMixTexture && this.quadmaterial.defines.USE_MIX_TEXTURE) {
      delete this.quadmaterial.defines.USE_MIX_TEXTURE
      this.quadmaterial.needsUpdate = true
    }
    this.quadmaterial.uniforms.mixTextureThreshold.value = this.mixTextureThreshold
    this.quadmaterial.uniforms.tMixTexture.value = this.mixTexture

    // draw

    // if (this.alpha === 0) {
    //   if (this.scene1 && this.camera1) renderer.render(this.scene1, this.camera1, this.renderToScreen ? null : readBuffer, this.clear)
    // } else if (this.alpha === 1) {
    //   if (this.scene2 && this.camera2) renderer.render(this.scene2, this.camera2, this.renderToScreen ? null : readBuffer, this.clear)
    // } else {
    //   if (this.scene1 && this.camera1) renderer.render(this.scene1, this.camera1, this.fbo1, true)
    //   if (this.scene2 && this.camera2) renderer.render(this.scene2, this.camera2, this.fbo2, true)
    //   renderer.render(this.scene, this.camera, this.renderToScreen ? null : readBuffer, this.clear)
    // }

    if (this.alpha === 0) {
      if (this.scene1 && this.camera1) {
        renderer.setRenderTarget(this.renderToScreen ? null : readBuffer)
        if (this.clear) renderer.clear()
        renderer.render(this.scene1, this.camera1)
      }
    } else if (this.alpha === 1) {
      if (this.scene2 && this.camera2) {
        renderer.setRenderTarget(this.renderToScreen ? null : readBuffer)
        if (this.clear) renderer.clear()
        renderer.render(this.scene2, this.camera2)
      }
    } else {
      // alpha is in the middle, we need to fade
      if (this.scene1 && this.camera1) {
        renderer.setRenderTarget(this.fbo1)
        renderer.clear()
        renderer.render(this.scene1, this.camera1)
      }

      if (this.scene2 && this.camera2) {
        renderer.setRenderTarget(this.fbo2)
        renderer.clear()
        renderer.render(this.scene2, this.camera2)
      }
      // clear render target
      renderer.setRenderTarget(this.renderToScreen ? null : readBuffer)
      if (this.clear) renderer.clear()
      renderer.render(this.scene, this.camera)
    }
    // restore
    if (this.clearColor) {
      renderer.setClearColor(oldClearColor, oldClearAlpha)
    }
    renderer.setRenderTarget(oldRenderTarget)
    renderer.autoClear = oldAutoClear
  }

  dispose () {
    if (this.quadmaterial.uniforms.tDiffuse1.value) this.quadmaterial.uniforms.tDiffuse1.value.dispose()
    if (this.quadmaterial.uniforms.tDiffuse2.value) this.quadmaterial.uniforms.tDiffuse2.value.dispose()
    if (this.quadmaterial.uniforms.tMixTexture.value) this.quadmaterial.uniforms.tMixTexture.value.dispose()
    this.quadmaterial.dispose()
    this.quad.geometry.dispose()
  }

  setScene1 (scene1, camera1) {
    this.scene1 = scene1
    this.camera1 = camera1
  }

  setScene2 (scene2, camera2) {
    this.scene2 = scene2
    this.camera2 = camera2
  }

  setSize (width, height) {
    this.fbo1.setSize(width, height)
    this.fbo2.setSize(width, height)

    this.quadmaterial.uniforms.aspectRatio.value = width / height
  }
}
