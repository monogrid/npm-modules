uniform float mixRatio;
uniform float aspectRatio;
uniform sampler2D tDiffuse1;
uniform sampler2D tDiffuse2;
uniform sampler2D tMixTexture;
uniform float mixTextureThreshold;
varying vec2 vUv;

void main() {
  vec4 texel1 = texture2D( tDiffuse1, vUv );
  vec4 texel2 = texture2D( tDiffuse2, vUv );

  vec4 outColor;

  #ifdef USE_MIX_TEXTURE
    vec2 scale = vec2(aspectRatio, 1.0);
    if (aspectRatio >= 1.0) {
      scale = vec2(1.0, 1.0 / aspectRatio);
    }
    vec2 offset = vec2((1.0 - scale.x) * 0.5, (1.0 - scale.y) * 0.5);

    vec4 transitionTexel = texture2D( tMixTexture, (vUv * scale) + offset );

    float r = mixRatio * (1.0 + mixTextureThreshold * 2.0) - mixTextureThreshold;

    float mixf = clamp((transitionTexel.r - r) * (1.0 / mixTextureThreshold), 0.0, 1.0);

    outColor = mix( texel1, texel2, mixf );
  #else
    outColor = mix( texel2, texel1, mixRatio );
  #endif

  gl_FragColor = outColor;
}
