import WorldRenderer from './WorldRenderer'

import StandardRenderPass from './postprocessing/StandardRenderPass'
import ColorGradingPass from './postprocessing/ColorGradingPass'

import { EffectComposer } from 'three-full/sources/postprocessing/EffectComposer'
import { UnrealBloomPass } from 'three-full/sources/postprocessing/UnrealBloomPass'
import { ShaderPass } from 'three-full/sources/postprocessing/ShaderPass'
import { FXAAShader } from 'three-full/sources/shaders/FXAAShader'
import { SMAAPass } from 'three-full/sources/postprocessing/SMAAPass'
import { Vector2 } from 'three-full/sources/math/Vector2'
import { Color } from 'three-full/sources/math/Color'
import { Texture } from 'three-full/sources/textures/Texture'
import { WebGLMultisampleRenderTarget } from 'three-full/sources/renderers/WebGLMultisampleRenderTarget'
import { RGBAFormat, FloatType, UnsignedByteType } from 'three-full/sources/constants'

import findLastIndex from 'lodash/findLastIndex'
/*
 * TODO: documentation
 *
 *
 *
 *
 *
 */
export default {
  name: 'StandardWorldRenderer',

  // use world renderer as base
  mixins: [WorldRenderer],

  // this is a system to define properties of object
  // which can be interpolated between worlds
  // and which can be controlled through an UI
  //
  // these will also be the only reactive
  // properties of the specified objects
  reactiveProps () {
    return {
      renderScenePass: {
        enabled: true,
        useMixTexture: false,
        mixTexture: new Texture(),
        mixTextureThreshold: 0.1,
        clearAlpha: 0,
        clearColor: new Color(0xffffff)
      },
      bloomPass: {
        enabled: true,
        strength: 1,
        radius: 0.4,
        threshold: 0.85
      },
      colorGradingPass: {
        enabled: false
      },
      FXAAPass: {
        enabled: true
      },
      SMAAPass: {
        enabled: false
      },
      hdr: false
    }
  },

  reactiveValues: {
    renderScenePass () {
      return new StandardRenderPass({ width: this.width, height: this.height })
    },
    bloomPass () {
      return new UnrealBloomPass(new Vector2(this.width, this.height), 0, 0, 0)
    },
    colorGradingPass () {
      return new ColorGradingPass()
    },
    FXAAPass () {
      return new ShaderPass(FXAAShader)
    },
    SMAAPass () {
      return new SMAAPass(this.width, this.height)
    }
  },

  data: function () {
    return {
      alpha: 0
    }
  },

  watch: {
    hdr (value) {
      const type = this.hdr ? FloatType : UnsignedByteType

      const switchRT = (obj, rt) => {
        const newRT = obj[rt].clone()
        newRT.texture.type = type
        // newRT.texture.needsUpdate = true
        // newRT.texture.needsUpdate = true
        obj[rt].dispose()
        obj[rt] = newRT
      }

      switchRT(this.composer, 'renderTarget1')
      this.composer.writeBuffer = this.composer.renderTarget1
      switchRT(this.composer, 'renderTarget2')
      this.composer.readBuffer = this.composer.renderTarget2
      switchRT(this.renderScenePass, 'fbo1')
      this.renderScenePass.quadmaterial.uniforms.tDiffuse1.value = this.renderScenePass.fbo1.texture
      switchRT(this.renderScenePass, 'fbo2')
      this.renderScenePass.quadmaterial.uniforms.tDiffuse2.value = this.renderScenePass.fbo1.texture
      this.renderScenePass.quadmaterial.needsUpdate = true

      // bloom pass RT replace
      switchRT(this.bloomPass, 'renderTargetBright')
      this.bloomPass.renderTargetsHorizontal.forEach((rt, i) => {
        switchRT(this.bloomPass.renderTargetsHorizontal, i)
      })

      this.bloomPass.renderTargetsVertical.forEach((rt, i) => {
        switchRT(this.bloomPass.renderTargetsVertical, i)
      })
      this.bloomPass.compositeMaterial.uniforms.blurTexture1.value = this.bloomPass.renderTargetsVertical[0].texture
      this.bloomPass.compositeMaterial.uniforms.blurTexture2.value = this.bloomPass.renderTargetsVertical[1].texture
      this.bloomPass.compositeMaterial.uniforms.blurTexture3.value = this.bloomPass.renderTargetsVertical[2].texture
      this.bloomPass.compositeMaterial.uniforms.blurTexture4.value = this.bloomPass.renderTargetsVertical[3].texture
      this.bloomPass.compositeMaterial.uniforms.blurTexture5.value = this.bloomPass.renderTargetsVertical[4].texture
      this.bloomPass.compositeMaterial.needsUpdate = true

      this.handleResize()
    },
    'bloomPass.strength' (value) {
      // if (this.alpha === 0 && this.world1 && this.world1.rendererProps.bloomPass.enabled && value <= 0) this.world1.rendererProps.bloomPass.enabled = false
      // if (this.alpha === 1 && this.world2 && this.world2.rendererProps.bloomPass.enabled && value <= 0) this.world2.rendererProps.bloomPass.enabled = false

      // disable/enable bloom pass based on strength
      if (this.alpha === 0 && this.world1) this.world1.rendererProps.bloomPass.enabled = value > 0
      if (this.alpha === 1 && this.world2) this.world2.rendererProps.bloomPass.enabled = value > 0
    },
    'SMAAPass.enabled' (value) {
      // disable FXAA if this is enabled
      if (this.alpha === 0 && this.world1 && value && this.world1.rendererProps.FXAAPass.enabled) this.world1.rendererProps.FXAAPass.enabled = false
      if (this.alpha === 1 && this.world2 && value && this.world2.rendererProps.FXAAPass.enabled) this.world2.rendererProps.FXAAPass.enabled = false
    },
    'FXAAPass.enabled' (value) {
      // disable SMAA if this is enabled
      if (this.alpha === 0 && this.world1 && value && this.world1.rendererProps.SMAAPass.enabled) this.world1.rendererProps.SMAAPass.enabled = false
      if (this.alpha === 1 && this.world2 && value && this.world2.rendererProps.SMAAPass.enabled) this.world2.rendererProps.SMAAPass.enabled = false
    }
  },

  created () {
    // restrict multisample
    if (this.renderer.capabilities.isWebGL2 && this.renderer.capabilities.maxTextureSize > 4096 && window.devicePixelRatio < 2) {
      const parameters = {
        format: RGBAFormat,
        stencilBuffer: false
      }
      const size = this.renderer.getDrawingBufferSize(new Vector2())
      this.multiSampleRenderTarget = new WebGLMultisampleRenderTarget(size.width, size.height, parameters)
      this.composer = new EffectComposer(this.renderer, this.multiSampleRenderTarget)
      // this.composer = new EffectComposer(this.renderer)
    } else {
      this.composer = new EffectComposer(this.renderer)
    }
    // this.composer.readBuffer.texture.encoding = GammaEncoding
    // this.composer.writeBuffer.texture.encoding = GammaEncoding

    // override bloom so it works on transparent bgs
    // this.bloomPass.renderTargetBright.texture.format = RGBAFormat
    // this.bloomPass.renderTargetsHorizontal.forEach(rt => { rt.texture.format = RGBAFormat })
    // this.bloomPass.renderTargetsVertical.forEach(rt => { rt.texture.format = RGBAFormat })

    this.composer.addPass(this.renderScenePass)
    this.composer.addPass(this.bloomPass)
    this.composer.addPass(this.colorGradingPass)
    this.composer.addPass(this.FXAAPass)
    this.composer.addPass(this.SMAAPass)
    this.handleResize()
  },

  methods: {
    dispose () {
      // call WorldRenderer mixin method
      WorldRenderer.methods.dispose.bind(this)()
      this.renderScenePass.dispose()
      this.bloomPass.dispose()
    },

    handleMaterialsUpdate (world1, world2) {
      // console.log('materials update')
      this.renderScenePass.quadmaterial.needsUpdate = true

      this.bloomPass.basic.needsUpdate = true
      this.bloomPass.compositeMaterial.needsUpdate = true
      this.bloomPass.materialCopy.needsUpdate = true
      this.bloomPass.materialHighPassFilter.needsUpdate = true

      this.FXAAPass.material.needsUpdate = true

      this.SMAAPass.materialBlend.needsUpdate = true
      this.SMAAPass.materialEdges.needsUpdate = true
      this.SMAAPass.materialWeights.needsUpdate = true

      this.colorGradingPass.material.needsUpdate = true

      WorldRenderer.methods.handleMaterialsUpdate.bind(this)(world1, world2)
    },

    handleResize () {
      // call WorldRenderer mixin method
      WorldRenderer.methods.handleResize.bind(this)()
      // this calls setSize in all passes

      const pixelRatio = window.devicePixelRatio || 0
      this.composer.setSize(this.width * pixelRatio, this.height * pixelRatio)

      this.FXAAPass.uniforms.resolution.value.set(1 / this.width, 1 / this.height)
      if (this.multiSampleRenderTarget) {
        this.multiSampleRenderTarget.setSize(this.width * pixelRatio, this.height * pixelRatio)
      }
    },

    handlePassesRenderToScreen () {
      for (let i = 0; i < this.composer.passes.length; i++) {
        this.composer.passes[i].renderToScreen = false
      }
      const renderToScreenIndex = findLastIndex(this.composer.passes, { enabled: true })
      this.composer.passes[renderToScreenIndex].renderToScreen = true
    },
    render (world1, world2, alpha) {
      this.world1 = world1
      this.world2 = world2
      this.alpha = alpha

      this.renderScenePass.alpha = alpha
      this.colorGradingPass.alpha = alpha

      // set the last enabled pass in the list to renderToScreen = true
      // all preceding passes are set renderToScreen = false
      this.handlePassesRenderToScreen()

      // handle color grading transitions
      // let the destination world decide if (at the end)
      // the pass is left enabled or not
      if (alpha !== 0 && alpha !== 1) {
        this.colorGradingPass.enabled = (world1 && world2 && (world1.lut || world2.lut) && ((world1.lut && !world1.lut.isNeutral) || (world2.lut && !world2.lut.isNeutral)))
      }

      // set render passes variables
      if (world1) {
        this.renderScenePass.setScene1(world1.scene, world1.camera)
        this.colorGradingPass.lut1 = world1.lut
      }
      if (world2) {
        this.renderScenePass.setScene2(world2.scene, world2.camera)
        this.colorGradingPass.lut2 = world2.lut
      }

      this.composer.render()

      if (this._needsMaterialsUpdate) {
        this.handleMaterialsUpdate(world1, world2)
        this._needsMaterialsUpdate = false
      }
    }
  }
}
