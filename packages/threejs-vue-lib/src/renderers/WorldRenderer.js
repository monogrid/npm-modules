import { WebGLRenderer } from 'three-full/sources/renderers/WebGLRenderer'
import { Uncharted2ToneMapping, PCFSoftShadowMap } from 'three-full/sources/constants'
import { isWebGL2Available } from '@/3d/utils/CachedWebGL'

import { VueUtils } from '@monogrid/js-utils'

import merge from 'lodash/merge'
import keys from 'lodash/keys'

/*
 * TODO: documentation
 *
 *
 *
 *
 *
 */
export default {
  name: 'WorldRenderer',

  // define defaults
  // these can be interpolated between scenes
  // these will also be reactive properties
  reactiveProps () {
    return {
      renderer: {
        gammaInput: true,
        gammaOutput: true,
        gammaFactor: 2,
        physicallyCorrectLights: false,
        shadowMap: {
          enabled: false
        },
        sortObjects: true,
        toneMappingExposure: 1,
        toneMappingWhitePoint: 1
      }
    }
  },

  reactiveValues: {
    renderer () {
      if (isWebGL2Available()) {
        try {
          const context = this.canvas.getContext('webgl2', {
            stencil: false,
            depth: false,
            antialias: this.antialias,
            preserveDrawingBuffer: false,
            failIfMajorPerformanceCaveat: true,
            powerPreference: 'high-performance'
          })
          return new WebGLRenderer({ canvas: this.canvas, context, alpha: this.alpha })
        } catch (e) {
          return new WebGLRenderer({ canvas: this.canvas, alpha: this.alpha, antialias: this.antialias, preserveDrawingBuffer: false })
        }
      }
      return new WebGLRenderer({ canvas: this.canvas, alpha: this.alpha, antialias: this.antialias, preserveDrawingBuffer: false })
    }
  },

  props: {
    canvas: {
      required: true
    },
    pixelRatio: {
      type: Number,
      required: false,
      default: window.devicePixelRatio || 1
    },
    width: {
      type: Number,
      required: true
    },
    height: {
      type: Number,
      required: true
    },
    alpha: {
      type: Boolean,
      required: false,
      default: true
    },
    antialias: {
      type: Boolean,
      required: false,
      default: false
    }
  },

  watch: {
    'renderer.gammaInput' (value) { this._needsMaterialsUpdate = true },
    'renderer.gammaOutput' (value) { this._needsMaterialsUpdate = true },
    'renderer.gammaFactor' (value) { this._needsMaterialsUpdate = true },
    'renderer.shadowMap.enabled' (value) { this._needsMaterialsUpdate = true },
    'renderer.physicallyCorrectLights' (value) { this._needsMaterialsUpdate = true },
    width () { this.handleResize() },
    height () { this.handleResize() }
  },

  created () {
    // instantiate default values
    if (this.$options.reactiveValues) {
      const k = keys(this.$options.reactiveValues)
      for (let i = 0; i < k.length; i++) {
        const prop = k[i]
        const valueGetter = this.$options.reactiveValues[prop]
        const def = this.reactiveProps[prop]

        const instance = valueGetter.bind(this)()
        merge(instance, def)
        VueUtils.defineReactive(instance, def)
        this[prop] = instance
      }
    }
    this.renderer.setPixelRatio(this.pixelRatio)
    this.renderer.shadowMap.type = PCFSoftShadowMap
    this.renderer.toneMapping = Uncharted2ToneMapping
    this._needsMaterialsUpdate = false
  },

  methods: {
    handleResize () {
      this.renderer.setSize(this.width, this.height)
    },

    handleMaterialsUpdate (world1, world2) {
      if (world1) world1.handleMaterialsUpdate()
      if (world2) world2.handleMaterialsUpdate()
    },

    render (world1, world2, alpha) {
      // simple rendering
      let world
      if (alpha < 0.5) {
        world = world1
      } else {
        world = world2
      }

      this.renderer.render(world.scene, world.camera)

      if (this._needsMaterialsUpdate) {
        this.handleMaterialsUpdate(world1, world2)
        this._needsMaterialsUpdate = false
      }
    },

    dispose () {
      this.renderer.dispose()
      this.renderer.forceContextLoss()
    }
  }
}
