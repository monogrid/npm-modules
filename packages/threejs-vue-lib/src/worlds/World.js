import { PerspectiveCamera } from 'three-full/sources/cameras/PerspectiveCamera'
import { Scene } from 'three-full/sources/scenes/Scene'
import { RGBAFormat } from 'three-full/sources/constants'
import { FileUtils, VueUtils } from '@monogrid/js-utils'

import merge from 'lodash/merge'
import keys from 'lodash/keys'
import mergeWith from 'lodash/mergeWith'
// import cloneDeepWith from 'lodash/cloneDeepWith'
// import defer from 'lodash/defer'

import Lut3DTexture from '@/3d/objects/Lut3DTexture'
/*
 * TODO: documentation
 *
 *
 *
 *
 *
 */
export default {
  name: 'BaseWorld',
  /**
   *
   * @returns {{rendererProps: {}, lut: *, saveState(): void, loadState(): void}}
   */
  reactiveProps () {
    return {
      rendererProps: {

      },
      lut: new Lut3DTexture().getNeutral(),
      saveState () {
        this.doSaveState()
      },
      loadState () {
        this.doLoadState()
      }
    }
  },

  // reactiveValues: {
  //   lut () {
  //     return new Lut3DTexture()
  //   }
  // },

  props: {
    width: {
      type: Number,
      required: true,
      default: 800
    },
    height: {
      type: Number,
      required: true,
      default: 600
    }
  },
  /**
   *
   * @returns {{isLoaded: boolean}}
   */
  data () {
    return {
      isLoaded: false
    }
  },
  /**
   *
   */
  created () {
    this.camera = new PerspectiveCamera(50, this.width / this.height, 0.1, 1000)
    this.scene = new Scene()
    this.saveState = this.saveState.bind(this)
    this.loadState = this.loadState.bind(this)
    this.isLoaded = false

    // clone reactive props, this instance needs to be unique
    // let clonedReactiveProps = cloneDeepWith(this.$options.reactiveProps, function (value, key, object, stack) {
    //   // avoid cloning sub objects of clonable objects
    //   if (object && typeof object.clone === 'function') return value
    //   if (value && typeof value.clone === 'function') {
    //     return value.clone()
    //   }
    // })

    // instantiate default values
    if (this.$options.reactiveValues) {
      const k = keys(this.$options.reactiveValues)
      for (let i = 0; i < k.length; i++) {
        const prop = k[i]
        const def = this.reactiveProps[prop]
        const instance = this.$options.reactiveValues[prop].bind(this)()
        merge(instance, def)
        VueUtils.defineReactive(instance, def)
        this[prop] = instance
      }
    }
  },

  watch: {
    width () { this.handleResize() },
    height () { this.handleResize() }
  },

  methods: {
    /**
     *
     * @param delta
     */
    update (delta = 1 / 60) {

    },
    /**
     *
     * @returns {Promise<any>}
     */
    load () {
      return new Promise((resolve, reject) => {
        this.isLoaded = true
        resolve()
      })
    },
    /**
     *
     * @returns {Promise<any>}
     */
    intro () {
      return new Promise((resolve, reject) => {
        resolve()
      })
    },
    /**
     *
     * @returns {Promise<any>}
     */
    outro () {
      return new Promise((resolve, reject) => {
        resolve()
      })
    },

    handleMouseUp (event) {

    },

    handleMouseDown (event) {

    },

    handleMouseMove (event) {

    },

    handleMouseWheel (event) {

    },
    /**
     *
     */
    handleResize () {
      if (this.camera) {
        this.camera.aspect = this.width / this.height
        this.camera.updateProjectionMatrix()
      }
    },

    dispose () {
      if (this.scene) {
        this.scene.traverse(this.destroySceneChildren)
        if (this.scene.background) this.scene.background.dispose()
        this.scene.dispose()
      }
      if (this.lut) {
        this.lut.dispose()
      }
      this.$destroy()
    },
    /**
     *
     * @param value
     */
    destroySceneChildren (value) {
      if (typeof value.dispose === 'function') {
        value.dispose()
      } else {
        if (value.geometry) value.geometry.dispose()

        if (value.material) {
          if (Array.isArray(value.material)) value.material.forEach(m => this.disposeMaterial(m))
          else this.disposeMaterial(value.material)
        }
      }
    },

    disposeMaterial (m) {
      if (m.map) m.map.dispose()
      if (m.envMap) m.envMap.dispose()
      if (m.aoMap) m.aoMap.dispose()
      if (m.alphaMap) m.alphaMap.dispose()
      if (m.bumpMap) m.bumpMap.dispose()
      if (m.displacementMap) m.displacementMap.dispose()
      if (m.emissiveMap) m.emissiveMap.dispose()
      if (m.lightMap) m.lightMap.dispose()
      if (m.metalnessMap) m.metalnessMap.dispose()
      if (m.normalMap) m.normalMap.dispose()
      if (m.roughnessMap) m.roughnessMap.dispose()
      m.dispose()
    },

    handleMaterialsUpdate () {
      this.scene.traverse(this.doMaterialUpdate)
    },

    doMaterialUpdate (value) {
      if (value.material) {
        value.material.needsUpdate = true
      }
    },
    /**
     *
     * @param object
     * @param model
     */
    getObjectState: function (object, model) {
      const result = {}
      for (const prop in model) {
        // functions are not serialized
        if (Object.prototype.hasOwnProperty.call(object, prop) && typeof object[prop] !== 'function') {
          if (typeof object[prop] !== 'object') {
            // all non-objects are put as they are in the file
            result[prop] = object[prop]
          } else if (object[prop]) {
            // special handling for textures
            if (object[prop].isTexture) {
              // let meta = {textures: {}, images: {}}
              // let serialized = object[prop].toJSON(meta)
              // result[prop] = serialized

              const serialized = {}
              // save texture to state
              if (object[prop].image) {
                if (object[prop].image.src) {
                  // this is an image
                  serialized.image = { src: object[prop].image.src }
                } else if (typeof object[prop].image.toDataURL === 'function') {
                  // this is a canvas?
                  const mime = object[prop].format === RGBAFormat ? 'image/png' : 'image/jpeg'
                  serialized.image = { src: object[prop].image.toDataURL(mime) }
                }
              } else {
                serialized.image = { src: '' }
              }

              if (object[prop].isLut3DTexture) {
                if (object[prop].isNeutral) {
                  // don't serialize neutral LUTs
                  delete serialized.image
                } else {
                  // add a field for lut textures
                  serialized.cubeSize = object[prop].cubeSize
                  serialized.isNeutral = false
                }
              }
              result[prop] = serialized
            } else {
              result[prop] = this.getObjectState(object[prop], model[prop])
            }
          }
        }
      }
      return result
    },
    /**
     *
     */
    doSaveState () {
      const result = this.getObjectState(this, this.reactiveProps)
      return FileUtils.downloadFile(JSON.stringify(result, null, 2), this.$options.name + '-state.json', 'text/plain')
    },
    /**
     *
     * @returns {Promise<void>}
     */
    async doLoadState () {
      const file = await FileUtils.loadJSON()
      // merge(this, file)
      mergeWith(this, file, (objValue, srcValue, key, object, source, stack) => {
        // if we have a texture we need to force it as an image
        // and mark it as needsUpdate = true
        if (objValue && objValue.isTexture) {
          objValue.image = new Image()
          objValue.needsUpdate = true
        }
        return undefined
      })
    }
  }
}
