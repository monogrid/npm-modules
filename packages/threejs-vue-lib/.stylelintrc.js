'use strict'

module.exports = {
  extends: 'stylelint-config-sass-guidelines',
  plugins: ['stylelint-scss'],
  rules: {
    'order/properties-alphabetical-order': null,
    // 'selector-max-compound-selectors': 3,
    'selector-no-qualifying-type': null,
    'selector-class-pattern': null,
    'max-nesting-depth': 3,

    'at-rule-no-unknown': [true, {
      'ignoreAtRules': ['include', 'function', 'mixin', 'content', 'at-root', 'for', 'if', 'else', 'each', 'return']
    }],

    'selector-pseudo-element-no-unknown': [true,{
      ignorePseudoElements: ['v-deep'],
    }]
  }
}
