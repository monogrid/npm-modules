var utils = require('./utils')
const env = require('yargs').argv.env
var isProduction = env === 'build'

const loaders = utils.cssLoaders({
  sourceMap: !isProduction,
  extract: false,
  usePostCSS: true
})

module.exports = {
  loaders
}
