module.exports = {
  presets: [
    ['@babel/env', {
      modules: false,
      // useBuiltIns: 'entry',
      targets: ['> 1%', 'last 2 versions', 'not ie <= 10']
      // corejs: {
      //   version: 3,
      //   proposals: true
      // }
    }]
  ],
  sourceType: 'unambiguous',
  plugins: [
    '@babel/plugin-transform-runtime', // babel-plugin-transform-runtime
    '@babel/plugin-syntax-dynamic-import', // https://babeljs.io/docs/en/babel-plugin-syntax-dynamic-import
    'transform-vue-jsx',
    'babel-plugin-add-module-exports'
  ]
}
