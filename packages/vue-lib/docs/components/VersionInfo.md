# VersionInfo

> A component to display a version identificator for a webapp
> Useful for QA teams

## Props

| Prop name      | Description                                                                                      | Type    | Values | Default    |
| -------------- | ------------------------------------------------------------------------------------------------ | ------- | ------ | ---------- |
| version        | Version (eg 1.2.3)                                                                               | string  | -      | 'untagged' |
| commitHash     | Shortened Commit hash of the version (eg 8dfd465)                                                | string  | -      | ''         |
| autoPositioned | Determines if the component is automatically positioned<br>in the app in the bottom right corner | boolean | -      | true       |
