# ControlKit

> A self contained interface able to modify a target object
> based on a model object.

It automatically infers the control types and supports
infinitely nested subObjects

> functional

## Props

| Prop name          | Description                                                                                                                        | Type   | Values | Default   |
| ------------------ | ---------------------------------------------------------------------------------------------------------------------------------- | ------ | ------ | --------- |
| model              | a settings model object with properties<br>to be modified in the target object.<br><br>It can have sub-objects and sub-properties. |        | -      |           |
| target             | The target object to modify.                                                                                                       |        | -      |           |
| additionalControls | An array of additional controls to use.<br>TODO: Documentation                                                                     | array  | -      |           |
| title              | Root title displayed                                                                                                               | string | -      | 'Options' |
| settings           | Object containing a definition of settings<br>TODO: Documentation                                                                  | object | -      | {}        |
