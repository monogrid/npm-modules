# SpriteAnimator

> Instances a `canvas` element that plays a spritesheet
> from TexturePacker

[See](<[TexturePacker](https://www.codeandweb.com/texturepacker)>)

## Props

| Prop name     | Description                                                                                                                                                  | Type    | Values | Default                        |
| ------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------ | ------- | ------ | ------------------------------ |
| spritesheet   | PNG image path to spritesheet, must be a valid path or blob string.                                                                                          | string  | -      | ''                             |
| json          | The TexturePacker json file, parsed                                                                                                                          | object  | -      | {}                             |
| fps           | Frames per second for playback                                                                                                                               | number  | -      | 30                             |
| autoplay      | Signals the spritesheet to start playing as soon as its ready.                                                                                               | boolean | -      | true                           |
| autoplayDelay | Adds an artificial delay to the autoplay feature, in milliseconds.                                                                                           | number  | -      | function() {<br> return 0<br>} |
| yoyo          | Plays the spritesheet in reverse once the end is reached,<br>starts playing forward again after that is done.<br><br>Must be used in conjuction with `loop`. | boolean | -      | false                          |
| loop          | Loops the spritesheet playback.                                                                                                                              | boolean | -      | false                          |
| id            | Custom ID for the spritesheet root element.                                                                                                                  | string  | -      | 'vue-sprite'                   |

## Events

| Event name | Type | Description                                                                   |
| ---------- | ---- | ----------------------------------------------------------------------------- |
| init       |      | Emitted when the spritesheet is ready to be played<br>(image has been loaded) |
| ended      |      | Emitted when the spritesheet ends,<br>never emitted while looping.            |
| stop       |      | Emitted when the spritesheet is<br>explicitly told to stop playing            |
| play       |      | Emitted when the spritesheet begins to play                                   |
