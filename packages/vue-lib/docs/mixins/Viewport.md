# Viewport

> Include this `mixin` when you want a component to be aware of
> `window.innerWidth` and `window.innerHeight`
