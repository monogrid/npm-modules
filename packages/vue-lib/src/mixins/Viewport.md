```vue
  <template>
    <div>Viewport Width: {{viewPort.width}}, Viewport Height: {{viewPort.height}}</div>
  </template>

  <script>
    import Viewport from '@monogrid/vue-lib/lib/mixins/Viewport'

    export default {
      mixins: [Viewport]
    }  
  </script>
```
