```vue
  <template>
    <div class="controlkit-wrapper">
      <ControlKit :model="dataModel" :target="dataTarget" :settings="settings" />
      <p>dataTarget value</p>
      <textarea v-model="JSON.stringify(dataTarget, null, 2)" rows="10" cols="40"></textarea>
    </div>
  </template>

  <script>
    export default {
      data() {
        return {
          settings: {
            sliders: {
              aNumberWithSlider: {
                min: 0,
                max: 100,
                step: 1
              }
            }
          },
          dataModel: {
            aNumber: 0,
            aNumberWithSlider: 0,
            aBoolean: false,
            aString: '',
            aFunction: function() {},
            anObject: {
              aString: 'Hello'
            },
          },
          dataTarget: {
            aNumber: 0,
            aNumberWithSlider: 0,
            aBoolean: false,
            aString: 'Hello World',
            aFunction: function() { alert('Clicked this button') },
            anObject: {
              aString: 'Hello'
            }
          }
        }
      }
    }
  </script>

  <style scoped>
    .controlkit-wrapper {
      position: relative;
      width: 100%;
      height: 500px;
    }
  </style>
```
