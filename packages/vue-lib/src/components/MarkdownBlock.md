```vue
  <template>
    <div class="container">
      <p>Props:</p>
      <table>
        <tr>
          <td><label for="inlinecb">Inline</label></td>
          <td><input type="checkbox" id="inlinecb" v-model="inline"></input></td>
        </tr>
        <tr>
          <td><label for="linkifycb">Linkify</label></td>
          <td><input type="checkbox" id="linkifycb" v-model="linkify"></input></td>
        </tr>
        <tr>
          <td><label for="typographercb">Typographer</label></td>
          <td><input type="checkbox" id="typographercb" v-model="typographer"></input></td>
        </tr>
        <tr>
          <td><label for="tagselect">Tag</label></td>
          <td>
            <select id="tagselect" v-model="tag">
              <option value="div">div</option>
              <option value="aside">aside</option>
              <option value="article">article</option>
              <option value="span">span</option>
            </select>
          </td>
        </tr>
      </table>
      <p>Result:</p>
      <MarkdownBlock
        :inline="inline"
        :tag="tag"
        :text="text"
        :linkify="linkify"
        :typographer="typographer"
        class="markdown-output"
        />
        <!-- :links-attributes="linksAttributes" -->
    </div>
  </template>

  <script>
    export default {
      data() {
        return {
          inline: false,
          linkify: true,
          typographer: true,
          tag: 'div',
          text: `
# h1 Heading
## h2 Heading
### h3 Heading
#### h4 Heading
##### h5 Heading
###### h6 Heading


## Horizontal Rules

___

---

***


## Typographic replacements

Enable typographer option to see result.

(c) (C) (r) (R) (tm) (TM) (p) (P) +-

test.. test... test..... test?..... test!....

!!!!!! ???? ,,  -- ---

"Smartypants, double quotes" and 'single quotes'


## Emphasis

**This is bold text**

__This is bold text__

*This is italic text*

_This is italic text_

~~Strikethrough~~


## Blockquotes


> Blockquotes can also be nested...
>> ...by using additional greater-than signs right next to each other...
> > > ...or with spaces between arrows.


## Lists

Unordered

+ Create a list by starting a line with \`+\`, \`-\`, or \`*\`
+ Sub-lists are made by indenting 2 spaces:
  - Marker character change forces new list start:
    * Ac tristique libero volutpat at
    + Facilisis in pretium nisl aliquet
    - Nulla volutpat aliquam velit
+ Very easy!

Ordered

1. Lorem ipsum dolor sit amet
2. Consectetur adipiscing elit
3. Integer molestie lorem at massa


1. You can use sequential numbers...
1. ...or keep all the numbers as \`1.\`

Start numbering with offset:

57. foo
1. bar


## Code

Inline \`code\`

Indented code

    // Some comments
    line 1 of code
    line 2 of code
    line 3 of code


Block code "fences"

\`\`\`
Sample text here...
\`\`\`

Syntax highlighting

\`\`\` js
var foo = function (bar) {
  return bar++;
};

console.log(foo(5));
\`\`\`

## Tables

| Option | Description |
| ------ | ----------- |
| data   | path to data files to supply the data that will be passed into templates. |
| engine | engine to be used for processing templates. Handlebars is the default. |
| ext    | extension to be used for dest files. |

Right aligned columns

| Option | Description |
| ------:| -----------:|
| data   | path to data files to supply the data that will be passed into templates. |
| engine | engine to be used for processing templates. Handlebars is the default. |
| ext    | extension to be used for dest files. |


## Links

[link text](http://dev.nodeca.com)

[link with title](http://nodeca.github.io/pica/demo/ "title text!")

Autoconverted link https://github.com/nodeca/pica (enable linkify to see)


## Images

![Minion](https://via.placeholder.com/350x150)
![Stormtroopocat](https://via.placeholder.com/300x150 "The Stormtroopocat")

Like links, Images also have a footnote style syntax

![Alt text][id]

With a reference later in the document defining the URL location:

[id]: https://via.placeholder.com/350x150  "The Dojocat"


## Plugins

The killer feature of \`markdown-it\` is very effective support of
[syntax plugins](https://www.npmjs.org/browse/keyword/markdown-it-plugin).


### [Custom Block](https://github.com/posva/markdown-it-custom-block)

@[mark](This is marked text, done with a with a custom block)

### [Custom containers](https://github.com/markdown-it/markdown-it-container)

Hello this is normal text with and 
::: router-link to="home"
this should be a router-link to home
:::
`
        }
      }
    }
  </script>

  <style scoped>
    .container {
      font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", "Roboto", "Oxygen", "Ubuntu", "Cantarell", "Fira Sans", "Droid Sans", "Helvetica Neue", sans-serif;
    }

    table {
      border-collapse: collapse;
      width: 100%;
    }

    td, th {
      border: 1px solid #dddddd;
      text-align: left;
      padding: 8px;
    }

    .markdown-output {
      border: 1px solid #dddddd;
      padding: 12px;
    }
  </style>
```
