```vue
  <template>
    <div class="container">
      <p>Props:</p>
      <table>
        <tr>
          <td><button @click="$refs.spritesheet.play()">Play</button></td>
          <td><button @click="$refs.spritesheet.stop()">Stop</button></td>
        </tr>
        <tr>
          <td><label for="autoplaycb">Autoplay</label></td>
          <td><input type="checkbox" id="autoplaycb" v-model="autoplay"></input></td>
        </tr>
        <tr>
          <td><label for="loopcb">Loop</label></td>
          <td><input type="checkbox" id="loopcb" v-model="loop"></input></td>
        </tr>
        <tr>
          <td><label for="yoyocb">YoYo</label></td>
          <td><input type="checkbox" id="yoyocb" v-model="yoyo"></input></td>
        </tr>
        <tr>
          <td><label for="fpsInput">FPS</label></td>
          <td><input type="number" id="fpsInput" v-model="fps" min="0" max="60" step="1"></input></td>
        </tr>
      </table>
      <p>Result:</p>

      <SpriteAnimator
        ref="spritesheet"
        :spritesheet="rabbitPNG"
        :json="rabbitJSON"
        :autoplay="autoplay"
        :loop="loop"
        :yoyo="yoyo"
        :fps="fps"
        />
        <p>Drawings are courtesy of <a href="http://eledimassimo.blogspot.co.uk/">Eleonora Di Massimo.</a></p>
    </div>
  </template>

  <script>

    import rabbitPNG from '../../assets/rabbit.png'
    import rabbitJSON from '../../assets/rabbit.json'

    export default {
      data() {
        return {
          autoplay: true,
          loop: true,
          yoyo: false,
          fps: 10,
          rabbitPNG,
          rabbitJSON
        }
      }
    }
  </script>

  <style scoped>
    .container {
      font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", "Roboto", "Oxygen", "Ubuntu", "Cantarell", "Fira Sans", "Droid Sans", "Helvetica Neue", sans-serif;
    }

    table {
      border-collapse: collapse;
      width: 100%;
    }

    td, th {
      border: 1px solid #dddddd;
      text-align: left;
      padding: 8px;
    }
  </style>
```
