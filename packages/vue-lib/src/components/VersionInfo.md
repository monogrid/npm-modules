```vue
  <template>
    <div class="container">
      <p>Props:</p>
      <table>
        <tr>
          <td><label for="autopositionedcb">Auto Positioned</label></td>
          <td><input type="checkbox" id="autopositionedcb" v-model="autoPositioned"></input></td>
        </tr>
      </table>    
      <p>Result:</p>
      <VersionInfo version="1.2.4" commit-hash="8dfd465" :auto-positioned="autoPositioned" />
    </div>
  </template>
  <script>
    export default {
      data() {
        return {
          autoPositioned: false
        }
      }
    }
  </script>
  
  <style scoped>
    .container {
      font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", "Roboto", "Oxygen", "Ubuntu", "Cantarell", "Fira Sans", "Droid Sans", "Helvetica Neue", sans-serif;
    }

    table {
      border-collapse: collapse;
      width: 100%;
    }

    td, th {
      border: 1px solid #dddddd;
      text-align: left;
      padding: 8px;
    }
  </style>  
```
