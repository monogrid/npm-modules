(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("@monogrid/js-utils"));
	else if(typeof define === 'function' && define.amd)
		define("vue-lib", ["@monogrid/js-utils"], factory);
	else if(typeof exports === 'object')
		exports["vue-lib"] = factory(require("@monogrid/js-utils"));
	else
		root["vue-lib"] = factory(root["@monogrid/js-utils"]);
})(window, function(__WEBPACK_EXTERNAL_MODULE__monogrid_js_utils__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/components/SpriteAnimator.vue");
/******/ })
/************************************************************************/
/******/ ({

/***/ "../../node_modules/@babel/runtime/helpers/typeof.js":
/*!*************************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/@babel/runtime/helpers/typeof.js ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _typeof(obj) {
  "@babel/helpers - typeof";

  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    module.exports = _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    module.exports = _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}

module.exports = _typeof;

/***/ }),

/***/ "../../node_modules/babel-loader/lib/index.js!../../node_modules/vue-loader/lib/index.js?!./src/components/SpriteAnimator.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************************************************************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/babel-loader/lib!D:/Users/dani/Desktop/@monogrid/node_modules/vue-loader/lib??vue-loader-options!./src/components/SpriteAnimator.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _monogrid_js_utils__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @monogrid/js-utils */ "@monogrid/js-utils");
/* harmony import */ var _monogrid_js_utils__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_monogrid_js_utils__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var lodash_delay__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! lodash/delay */ "../../node_modules/lodash/delay.js");
/* harmony import */ var lodash_delay__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(lodash_delay__WEBPACK_IMPORTED_MODULE_1__);
//
//
//
//
//
//
//
//
//


/**
 * Instances a `canvas` element that plays a spritesheet
 * from TexturePacker
 *
 * @see [TexturePacker](https://www.codeandweb.com/texturepacker)
 */

/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'SpriteAnimator',
  props: {
    /**
     * PNG image path to spritesheet, must be a valid path or blob string.
     */
    spritesheet: {
      required: true,
      type: String,
      default: ''
    },

    /**
     * The TexturePacker json file, parsed
     */
    json: {
      required: true,
      type: Object,
      default: function _default() {
        return {};
      }
    },

    /**
     * Frames per second for playback
     */
    fps: {
      type: Number,
      default: 30
    },

    /**
     * Signals the spritesheet to start playing as soon as its ready.
     */
    autoplay: {
      type: Boolean,
      default: true
    },

    /**
     * Adds an artificial delay to the autoplay feature, in milliseconds.
     */
    autoplayDelay: {
      type: Number,
      default: function _default() {
        return 0;
      }
    },

    /**
     * Plays the spritesheet in reverse once the end is reached,
     * starts playing forward again after that is done.
     *
     * Must be used in conjuction with `loop`.
     */
    yoyo: {
      type: Boolean,
      default: false
    },

    /**
     * Loops the spritesheet playback.
     */
    loop: {
      type: Boolean,
      default: false
    },

    /**
     * Custom ID for the spritesheet root element.
     */
    id: {
      type: String,
      default: 'vue-sprite'
    }
  },
  watch: {
    /**
     * resets yoyodirection to forward when yoyo is disabled
     */
    yoyo: function yoyo(value) {
      if (!value) {
        this.yoyodirection = 0;
      }
    }
  },
  data: function data() {
    return {
      frames: [],
      length: 0,
      frameIndex: 0,
      currentIndex: 0,
      yoyodirection: 0,
      sprite: null,
      height: 0,
      width: 0,
      now: 0,
      then: 0
    };
  },
  mounted: function mounted() {
    var _this = this;

    this.json.frames.forEach(function (frame) {
      _this.frames.push({
        name: frame.filename,
        x: frame.frame.x,
        y: frame.frame.y,
        w: frame.frame.w,
        h: frame.frame.h
      });
    }); // this.frames.sort((a, b) => a.filename < b.filename)

    this.width = this.frames[0].w;
    this.height = this.frames[0].h;
    this.length = this.frames.length - 1;
  },
  created: function created() {
    var _this2 = this;

    this.$nextTick(function () {
      _this2.sprite = new Image();
      _this2.sprite.src = _this2.spritesheet;

      _this2.sprite.onload = function (_ref) {
        var target = _ref.target;

        _this2.init(target);
      };
    });
  },
  methods: {
    init: function init(img) {
      this.ctx = this.$refs['vue-sprite-canvas'].getContext('2d');
      /**
       * Emitted when the spritesheet is ready to be played
       * (image has been loaded)
       */

      this.$emit('init');

      if (this.autoplay) {
        this.render();
        lodash_delay__WEBPACK_IMPORTED_MODULE_1___default()(this.play, this.autoplayDelay);
      }
    },

    /**
     * Draws the image into the canvas
     */
    render: function render() {
      this.ctx && this.ctx.clearRect(0, 0, this.width, this.height);

      if (this.yoyo && this.currentIndex % this.length === 0 && this.currentIndex) {
        this.yoyodirection = Number(!this.yoyodirection);
      }

      var index = Math.abs(this.currentIndex % this.length - this.length * this.yoyodirection);
      var x = this.frames[index].x;
      var y = this.frames[index].y;
      this.ctx && this.ctx.drawImage(this.sprite, x, y, this.width, this.height, 0, 0, this.width, this.height);
    },

    /**
     * Handles the draw loop.
     */
    doloop: function doloop() {
      this.now = Date.now();
      var delta = this.now - this.then;

      if (delta > 1000 / this.fps) {
        this.then = this.now - delta % (1000 / this.fps);
        this.render();
        this.currentIndex++; // console.log(this.currentIndex)
      }

      if (!this.loop && this.currentIndex >= this.length) {
        _monogrid_js_utils__WEBPACK_IMPORTED_MODULE_0__["RAF"].remove(this.doloop);
        this.currentIndex = 0;
        /**
         * Emitted when the spritesheet ends,
         * never emitted while looping.
         */

        this.$emit('ended');
      }
    },

    /**
     * Stops the playback
     */
    stop: function stop() {
      _monogrid_js_utils__WEBPACK_IMPORTED_MODULE_0__["RAF"].remove(this.doloop);
      this.currentIndex = 0;
      /**
       * Emitted when the spritesheet is
       * explicitly told to stop playing
       */

      this.$emit('stop');
    },

    /**
     * Resumes the playback from optional frame number `from`
     */
    play: function play(from) {
      this.then = Date.now();
      this.currentIndex = Number.isNaN(Number(from)) ? this.currentIndex : from;
      _monogrid_js_utils__WEBPACK_IMPORTED_MODULE_0__["RAF"].add(this.doloop);
      /**
       * Emitted when the spritesheet begins to play
       */

      this.$emit('play');
    }
  }
});

/***/ }),

/***/ "../../node_modules/lodash/_Symbol.js":
/*!**********************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_Symbol.js ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var root = __webpack_require__(/*! ./_root */ "../../node_modules/lodash/_root.js");
/** Built-in value references. */


var _Symbol = root.Symbol;
module.exports = _Symbol;

/***/ }),

/***/ "../../node_modules/lodash/_apply.js":
/*!*********************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_apply.js ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/**
 * A faster alternative to `Function#apply`, this function invokes `func`
 * with the `this` binding of `thisArg` and the arguments of `args`.
 *
 * @private
 * @param {Function} func The function to invoke.
 * @param {*} thisArg The `this` binding of `func`.
 * @param {Array} args The arguments to invoke `func` with.
 * @returns {*} Returns the result of `func`.
 */
function apply(func, thisArg, args) {
  switch (args.length) {
    case 0:
      return func.call(thisArg);

    case 1:
      return func.call(thisArg, args[0]);

    case 2:
      return func.call(thisArg, args[0], args[1]);

    case 3:
      return func.call(thisArg, args[0], args[1], args[2]);
  }

  return func.apply(thisArg, args);
}

module.exports = apply;

/***/ }),

/***/ "../../node_modules/lodash/_baseDelay.js":
/*!*************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_baseDelay.js ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/** Error message constants. */
var FUNC_ERROR_TEXT = 'Expected a function';
/**
 * The base implementation of `_.delay` and `_.defer` which accepts `args`
 * to provide to `func`.
 *
 * @private
 * @param {Function} func The function to delay.
 * @param {number} wait The number of milliseconds to delay invocation.
 * @param {Array} args The arguments to provide to `func`.
 * @returns {number|Object} Returns the timer id or timeout object.
 */

function baseDelay(func, wait, args) {
  if (typeof func != 'function') {
    throw new TypeError(FUNC_ERROR_TEXT);
  }

  return setTimeout(function () {
    func.apply(undefined, args);
  }, wait);
}

module.exports = baseDelay;

/***/ }),

/***/ "../../node_modules/lodash/_baseGetTag.js":
/*!**************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_baseGetTag.js ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var _Symbol = __webpack_require__(/*! ./_Symbol */ "../../node_modules/lodash/_Symbol.js"),
    getRawTag = __webpack_require__(/*! ./_getRawTag */ "../../node_modules/lodash/_getRawTag.js"),
    objectToString = __webpack_require__(/*! ./_objectToString */ "../../node_modules/lodash/_objectToString.js");
/** `Object#toString` result references. */


var nullTag = '[object Null]',
    undefinedTag = '[object Undefined]';
/** Built-in value references. */

var symToStringTag = _Symbol ? _Symbol.toStringTag : undefined;
/**
 * The base implementation of `getTag` without fallbacks for buggy environments.
 *
 * @private
 * @param {*} value The value to query.
 * @returns {string} Returns the `toStringTag`.
 */

function baseGetTag(value) {
  if (value == null) {
    return value === undefined ? undefinedTag : nullTag;
  }

  return symToStringTag && symToStringTag in Object(value) ? getRawTag(value) : objectToString(value);
}

module.exports = baseGetTag;

/***/ }),

/***/ "../../node_modules/lodash/_baseIsNative.js":
/*!****************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_baseIsNative.js ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var isFunction = __webpack_require__(/*! ./isFunction */ "../../node_modules/lodash/isFunction.js"),
    isMasked = __webpack_require__(/*! ./_isMasked */ "../../node_modules/lodash/_isMasked.js"),
    isObject = __webpack_require__(/*! ./isObject */ "../../node_modules/lodash/isObject.js"),
    toSource = __webpack_require__(/*! ./_toSource */ "../../node_modules/lodash/_toSource.js");
/**
 * Used to match `RegExp`
 * [syntax characters](http://ecma-international.org/ecma-262/7.0/#sec-patterns).
 */


var reRegExpChar = /[\\^$.*+?()[\]{}|]/g;
/** Used to detect host constructors (Safari). */

var reIsHostCtor = /^\[object .+?Constructor\]$/;
/** Used for built-in method references. */

var funcProto = Function.prototype,
    objectProto = Object.prototype;
/** Used to resolve the decompiled source of functions. */

var funcToString = funcProto.toString;
/** Used to check objects for own properties. */

var hasOwnProperty = objectProto.hasOwnProperty;
/** Used to detect if a method is native. */

var reIsNative = RegExp('^' + funcToString.call(hasOwnProperty).replace(reRegExpChar, '\\$&').replace(/hasOwnProperty|(function).*?(?=\\\()| for .+?(?=\\\])/g, '$1.*?') + '$');
/**
 * The base implementation of `_.isNative` without bad shim checks.
 *
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a native function,
 *  else `false`.
 */

function baseIsNative(value) {
  if (!isObject(value) || isMasked(value)) {
    return false;
  }

  var pattern = isFunction(value) ? reIsNative : reIsHostCtor;
  return pattern.test(toSource(value));
}

module.exports = baseIsNative;

/***/ }),

/***/ "../../node_modules/lodash/_baseRest.js":
/*!************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_baseRest.js ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var identity = __webpack_require__(/*! ./identity */ "../../node_modules/lodash/identity.js"),
    overRest = __webpack_require__(/*! ./_overRest */ "../../node_modules/lodash/_overRest.js"),
    setToString = __webpack_require__(/*! ./_setToString */ "../../node_modules/lodash/_setToString.js");
/**
 * The base implementation of `_.rest` which doesn't validate or coerce arguments.
 *
 * @private
 * @param {Function} func The function to apply a rest parameter to.
 * @param {number} [start=func.length-1] The start position of the rest parameter.
 * @returns {Function} Returns the new function.
 */


function baseRest(func, start) {
  return setToString(overRest(func, start, identity), func + '');
}

module.exports = baseRest;

/***/ }),

/***/ "../../node_modules/lodash/_baseSetToString.js":
/*!*******************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_baseSetToString.js ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var constant = __webpack_require__(/*! ./constant */ "../../node_modules/lodash/constant.js"),
    defineProperty = __webpack_require__(/*! ./_defineProperty */ "../../node_modules/lodash/_defineProperty.js"),
    identity = __webpack_require__(/*! ./identity */ "../../node_modules/lodash/identity.js");
/**
 * The base implementation of `setToString` without support for hot loop shorting.
 *
 * @private
 * @param {Function} func The function to modify.
 * @param {Function} string The `toString` result.
 * @returns {Function} Returns `func`.
 */


var baseSetToString = !defineProperty ? identity : function (func, string) {
  return defineProperty(func, 'toString', {
    'configurable': true,
    'enumerable': false,
    'value': constant(string),
    'writable': true
  });
};
module.exports = baseSetToString;

/***/ }),

/***/ "../../node_modules/lodash/_coreJsData.js":
/*!**************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_coreJsData.js ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var root = __webpack_require__(/*! ./_root */ "../../node_modules/lodash/_root.js");
/** Used to detect overreaching core-js shims. */


var coreJsData = root['__core-js_shared__'];
module.exports = coreJsData;

/***/ }),

/***/ "../../node_modules/lodash/_defineProperty.js":
/*!******************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_defineProperty.js ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var getNative = __webpack_require__(/*! ./_getNative */ "../../node_modules/lodash/_getNative.js");

var defineProperty = function () {
  try {
    var func = getNative(Object, 'defineProperty');
    func({}, '', {});
    return func;
  } catch (e) {}
}();

module.exports = defineProperty;

/***/ }),

/***/ "../../node_modules/lodash/_freeGlobal.js":
/*!**************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_freeGlobal.js ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {var _typeof = __webpack_require__(/*! @babel/runtime/helpers/typeof */ "../../node_modules/@babel/runtime/helpers/typeof.js");

/** Detect free variable `global` from Node.js. */
var freeGlobal = (typeof global === "undefined" ? "undefined" : _typeof(global)) == 'object' && global && global.Object === Object && global;
module.exports = freeGlobal;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../webpack/buildin/global.js */ "../../node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "../../node_modules/lodash/_getNative.js":
/*!*************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_getNative.js ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var baseIsNative = __webpack_require__(/*! ./_baseIsNative */ "../../node_modules/lodash/_baseIsNative.js"),
    getValue = __webpack_require__(/*! ./_getValue */ "../../node_modules/lodash/_getValue.js");
/**
 * Gets the native function at `key` of `object`.
 *
 * @private
 * @param {Object} object The object to query.
 * @param {string} key The key of the method to get.
 * @returns {*} Returns the function if it's native, else `undefined`.
 */


function getNative(object, key) {
  var value = getValue(object, key);
  return baseIsNative(value) ? value : undefined;
}

module.exports = getNative;

/***/ }),

/***/ "../../node_modules/lodash/_getRawTag.js":
/*!*************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_getRawTag.js ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var _Symbol = __webpack_require__(/*! ./_Symbol */ "../../node_modules/lodash/_Symbol.js");
/** Used for built-in method references. */


var objectProto = Object.prototype;
/** Used to check objects for own properties. */

var hasOwnProperty = objectProto.hasOwnProperty;
/**
 * Used to resolve the
 * [`toStringTag`](http://ecma-international.org/ecma-262/7.0/#sec-object.prototype.tostring)
 * of values.
 */

var nativeObjectToString = objectProto.toString;
/** Built-in value references. */

var symToStringTag = _Symbol ? _Symbol.toStringTag : undefined;
/**
 * A specialized version of `baseGetTag` which ignores `Symbol.toStringTag` values.
 *
 * @private
 * @param {*} value The value to query.
 * @returns {string} Returns the raw `toStringTag`.
 */

function getRawTag(value) {
  var isOwn = hasOwnProperty.call(value, symToStringTag),
      tag = value[symToStringTag];

  try {
    value[symToStringTag] = undefined;
    var unmasked = true;
  } catch (e) {}

  var result = nativeObjectToString.call(value);

  if (unmasked) {
    if (isOwn) {
      value[symToStringTag] = tag;
    } else {
      delete value[symToStringTag];
    }
  }

  return result;
}

module.exports = getRawTag;

/***/ }),

/***/ "../../node_modules/lodash/_getValue.js":
/*!************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_getValue.js ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/**
 * Gets the value at `key` of `object`.
 *
 * @private
 * @param {Object} [object] The object to query.
 * @param {string} key The key of the property to get.
 * @returns {*} Returns the property value.
 */
function getValue(object, key) {
  return object == null ? undefined : object[key];
}

module.exports = getValue;

/***/ }),

/***/ "../../node_modules/lodash/_isMasked.js":
/*!************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_isMasked.js ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var coreJsData = __webpack_require__(/*! ./_coreJsData */ "../../node_modules/lodash/_coreJsData.js");
/** Used to detect methods masquerading as native. */


var maskSrcKey = function () {
  var uid = /[^.]+$/.exec(coreJsData && coreJsData.keys && coreJsData.keys.IE_PROTO || '');
  return uid ? 'Symbol(src)_1.' + uid : '';
}();
/**
 * Checks if `func` has its source masked.
 *
 * @private
 * @param {Function} func The function to check.
 * @returns {boolean} Returns `true` if `func` is masked, else `false`.
 */


function isMasked(func) {
  return !!maskSrcKey && maskSrcKey in func;
}

module.exports = isMasked;

/***/ }),

/***/ "../../node_modules/lodash/_objectToString.js":
/*!******************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_objectToString.js ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/** Used for built-in method references. */
var objectProto = Object.prototype;
/**
 * Used to resolve the
 * [`toStringTag`](http://ecma-international.org/ecma-262/7.0/#sec-object.prototype.tostring)
 * of values.
 */

var nativeObjectToString = objectProto.toString;
/**
 * Converts `value` to a string using `Object.prototype.toString`.
 *
 * @private
 * @param {*} value The value to convert.
 * @returns {string} Returns the converted string.
 */

function objectToString(value) {
  return nativeObjectToString.call(value);
}

module.exports = objectToString;

/***/ }),

/***/ "../../node_modules/lodash/_overRest.js":
/*!************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_overRest.js ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var apply = __webpack_require__(/*! ./_apply */ "../../node_modules/lodash/_apply.js");
/* Built-in method references for those with the same name as other `lodash` methods. */


var nativeMax = Math.max;
/**
 * A specialized version of `baseRest` which transforms the rest array.
 *
 * @private
 * @param {Function} func The function to apply a rest parameter to.
 * @param {number} [start=func.length-1] The start position of the rest parameter.
 * @param {Function} transform The rest array transform.
 * @returns {Function} Returns the new function.
 */

function overRest(func, start, transform) {
  start = nativeMax(start === undefined ? func.length - 1 : start, 0);
  return function () {
    var args = arguments,
        index = -1,
        length = nativeMax(args.length - start, 0),
        array = Array(length);

    while (++index < length) {
      array[index] = args[start + index];
    }

    index = -1;
    var otherArgs = Array(start + 1);

    while (++index < start) {
      otherArgs[index] = args[index];
    }

    otherArgs[start] = transform(array);
    return apply(func, this, otherArgs);
  };
}

module.exports = overRest;

/***/ }),

/***/ "../../node_modules/lodash/_root.js":
/*!********************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_root.js ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var _typeof = __webpack_require__(/*! @babel/runtime/helpers/typeof */ "../../node_modules/@babel/runtime/helpers/typeof.js");

var freeGlobal = __webpack_require__(/*! ./_freeGlobal */ "../../node_modules/lodash/_freeGlobal.js");
/** Detect free variable `self`. */


var freeSelf = (typeof self === "undefined" ? "undefined" : _typeof(self)) == 'object' && self && self.Object === Object && self;
/** Used as a reference to the global object. */

var root = freeGlobal || freeSelf || Function('return this')();
module.exports = root;

/***/ }),

/***/ "../../node_modules/lodash/_setToString.js":
/*!***************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_setToString.js ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var baseSetToString = __webpack_require__(/*! ./_baseSetToString */ "../../node_modules/lodash/_baseSetToString.js"),
    shortOut = __webpack_require__(/*! ./_shortOut */ "../../node_modules/lodash/_shortOut.js");
/**
 * Sets the `toString` method of `func` to return `string`.
 *
 * @private
 * @param {Function} func The function to modify.
 * @param {Function} string The `toString` result.
 * @returns {Function} Returns `func`.
 */


var setToString = shortOut(baseSetToString);
module.exports = setToString;

/***/ }),

/***/ "../../node_modules/lodash/_shortOut.js":
/*!************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_shortOut.js ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/** Used to detect hot functions by number of calls within a span of milliseconds. */
var HOT_COUNT = 800,
    HOT_SPAN = 16;
/* Built-in method references for those with the same name as other `lodash` methods. */

var nativeNow = Date.now;
/**
 * Creates a function that'll short out and invoke `identity` instead
 * of `func` when it's called `HOT_COUNT` or more times in `HOT_SPAN`
 * milliseconds.
 *
 * @private
 * @param {Function} func The function to restrict.
 * @returns {Function} Returns the new shortable function.
 */

function shortOut(func) {
  var count = 0,
      lastCalled = 0;
  return function () {
    var stamp = nativeNow(),
        remaining = HOT_SPAN - (stamp - lastCalled);
    lastCalled = stamp;

    if (remaining > 0) {
      if (++count >= HOT_COUNT) {
        return arguments[0];
      }
    } else {
      count = 0;
    }

    return func.apply(undefined, arguments);
  };
}

module.exports = shortOut;

/***/ }),

/***/ "../../node_modules/lodash/_toSource.js":
/*!************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_toSource.js ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/** Used for built-in method references. */
var funcProto = Function.prototype;
/** Used to resolve the decompiled source of functions. */

var funcToString = funcProto.toString;
/**
 * Converts `func` to its source code.
 *
 * @private
 * @param {Function} func The function to convert.
 * @returns {string} Returns the source code.
 */

function toSource(func) {
  if (func != null) {
    try {
      return funcToString.call(func);
    } catch (e) {}

    try {
      return func + '';
    } catch (e) {}
  }

  return '';
}

module.exports = toSource;

/***/ }),

/***/ "../../node_modules/lodash/constant.js":
/*!***********************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/constant.js ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/**
 * Creates a function that returns `value`.
 *
 * @static
 * @memberOf _
 * @since 2.4.0
 * @category Util
 * @param {*} value The value to return from the new function.
 * @returns {Function} Returns the new constant function.
 * @example
 *
 * var objects = _.times(2, _.constant({ 'a': 1 }));
 *
 * console.log(objects);
 * // => [{ 'a': 1 }, { 'a': 1 }]
 *
 * console.log(objects[0] === objects[1]);
 * // => true
 */
function constant(value) {
  return function () {
    return value;
  };
}

module.exports = constant;

/***/ }),

/***/ "../../node_modules/lodash/delay.js":
/*!********************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/delay.js ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var baseDelay = __webpack_require__(/*! ./_baseDelay */ "../../node_modules/lodash/_baseDelay.js"),
    baseRest = __webpack_require__(/*! ./_baseRest */ "../../node_modules/lodash/_baseRest.js"),
    toNumber = __webpack_require__(/*! ./toNumber */ "../../node_modules/lodash/toNumber.js");
/**
 * Invokes `func` after `wait` milliseconds. Any additional arguments are
 * provided to `func` when it's invoked.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Function
 * @param {Function} func The function to delay.
 * @param {number} wait The number of milliseconds to delay invocation.
 * @param {...*} [args] The arguments to invoke `func` with.
 * @returns {number} Returns the timer id.
 * @example
 *
 * _.delay(function(text) {
 *   console.log(text);
 * }, 1000, 'later');
 * // => Logs 'later' after one second.
 */


var delay = baseRest(function (func, wait, args) {
  return baseDelay(func, toNumber(wait) || 0, args);
});
module.exports = delay;

/***/ }),

/***/ "../../node_modules/lodash/identity.js":
/*!***********************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/identity.js ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/**
 * This method returns the first argument it receives.
 *
 * @static
 * @since 0.1.0
 * @memberOf _
 * @category Util
 * @param {*} value Any value.
 * @returns {*} Returns `value`.
 * @example
 *
 * var object = { 'a': 1 };
 *
 * console.log(_.identity(object) === object);
 * // => true
 */
function identity(value) {
  return value;
}

module.exports = identity;

/***/ }),

/***/ "../../node_modules/lodash/isFunction.js":
/*!*************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/isFunction.js ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var baseGetTag = __webpack_require__(/*! ./_baseGetTag */ "../../node_modules/lodash/_baseGetTag.js"),
    isObject = __webpack_require__(/*! ./isObject */ "../../node_modules/lodash/isObject.js");
/** `Object#toString` result references. */


var asyncTag = '[object AsyncFunction]',
    funcTag = '[object Function]',
    genTag = '[object GeneratorFunction]',
    proxyTag = '[object Proxy]';
/**
 * Checks if `value` is classified as a `Function` object.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a function, else `false`.
 * @example
 *
 * _.isFunction(_);
 * // => true
 *
 * _.isFunction(/abc/);
 * // => false
 */

function isFunction(value) {
  if (!isObject(value)) {
    return false;
  } // The use of `Object#toString` avoids issues with the `typeof` operator
  // in Safari 9 which returns 'object' for typed arrays and other constructors.


  var tag = baseGetTag(value);
  return tag == funcTag || tag == genTag || tag == asyncTag || tag == proxyTag;
}

module.exports = isFunction;

/***/ }),

/***/ "../../node_modules/lodash/isObject.js":
/*!***********************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/isObject.js ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var _typeof = __webpack_require__(/*! @babel/runtime/helpers/typeof */ "../../node_modules/@babel/runtime/helpers/typeof.js");

/**
 * Checks if `value` is the
 * [language type](http://www.ecma-international.org/ecma-262/7.0/#sec-ecmascript-language-types)
 * of `Object`. (e.g. arrays, functions, objects, regexes, `new Number(0)`, and `new String('')`)
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is an object, else `false`.
 * @example
 *
 * _.isObject({});
 * // => true
 *
 * _.isObject([1, 2, 3]);
 * // => true
 *
 * _.isObject(_.noop);
 * // => true
 *
 * _.isObject(null);
 * // => false
 */
function isObject(value) {
  var type = _typeof(value);

  return value != null && (type == 'object' || type == 'function');
}

module.exports = isObject;

/***/ }),

/***/ "../../node_modules/lodash/isObjectLike.js":
/*!***************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/isObjectLike.js ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var _typeof = __webpack_require__(/*! @babel/runtime/helpers/typeof */ "../../node_modules/@babel/runtime/helpers/typeof.js");

/**
 * Checks if `value` is object-like. A value is object-like if it's not `null`
 * and has a `typeof` result of "object".
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is object-like, else `false`.
 * @example
 *
 * _.isObjectLike({});
 * // => true
 *
 * _.isObjectLike([1, 2, 3]);
 * // => true
 *
 * _.isObjectLike(_.noop);
 * // => false
 *
 * _.isObjectLike(null);
 * // => false
 */
function isObjectLike(value) {
  return value != null && _typeof(value) == 'object';
}

module.exports = isObjectLike;

/***/ }),

/***/ "../../node_modules/lodash/isSymbol.js":
/*!***********************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/isSymbol.js ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var _typeof = __webpack_require__(/*! @babel/runtime/helpers/typeof */ "../../node_modules/@babel/runtime/helpers/typeof.js");

var baseGetTag = __webpack_require__(/*! ./_baseGetTag */ "../../node_modules/lodash/_baseGetTag.js"),
    isObjectLike = __webpack_require__(/*! ./isObjectLike */ "../../node_modules/lodash/isObjectLike.js");
/** `Object#toString` result references. */


var symbolTag = '[object Symbol]';
/**
 * Checks if `value` is classified as a `Symbol` primitive or object.
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a symbol, else `false`.
 * @example
 *
 * _.isSymbol(Symbol.iterator);
 * // => true
 *
 * _.isSymbol('abc');
 * // => false
 */

function isSymbol(value) {
  return _typeof(value) == 'symbol' || isObjectLike(value) && baseGetTag(value) == symbolTag;
}

module.exports = isSymbol;

/***/ }),

/***/ "../../node_modules/lodash/toNumber.js":
/*!***********************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/toNumber.js ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var isObject = __webpack_require__(/*! ./isObject */ "../../node_modules/lodash/isObject.js"),
    isSymbol = __webpack_require__(/*! ./isSymbol */ "../../node_modules/lodash/isSymbol.js");
/** Used as references for various `Number` constants. */


var NAN = 0 / 0;
/** Used to match leading and trailing whitespace. */

var reTrim = /^\s+|\s+$/g;
/** Used to detect bad signed hexadecimal string values. */

var reIsBadHex = /^[-+]0x[0-9a-f]+$/i;
/** Used to detect binary string values. */

var reIsBinary = /^0b[01]+$/i;
/** Used to detect octal string values. */

var reIsOctal = /^0o[0-7]+$/i;
/** Built-in method references without a dependency on `root`. */

var freeParseInt = parseInt;
/**
 * Converts `value` to a number.
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to process.
 * @returns {number} Returns the number.
 * @example
 *
 * _.toNumber(3.2);
 * // => 3.2
 *
 * _.toNumber(Number.MIN_VALUE);
 * // => 5e-324
 *
 * _.toNumber(Infinity);
 * // => Infinity
 *
 * _.toNumber('3.2');
 * // => 3.2
 */

function toNumber(value) {
  if (typeof value == 'number') {
    return value;
  }

  if (isSymbol(value)) {
    return NAN;
  }

  if (isObject(value)) {
    var other = typeof value.valueOf == 'function' ? value.valueOf() : value;
    value = isObject(other) ? other + '' : other;
  }

  if (typeof value != 'string') {
    return value === 0 ? value : +value;
  }

  value = value.replace(reTrim, '');
  var isBinary = reIsBinary.test(value);
  return isBinary || reIsOctal.test(value) ? freeParseInt(value.slice(2), isBinary ? 2 : 8) : reIsBadHex.test(value) ? NAN : +value;
}

module.exports = toNumber;

/***/ }),

/***/ "../../node_modules/vue-loader/lib/loaders/templateLoader.js?!../../node_modules/vue-loader/lib/index.js?!./src/components/SpriteAnimator.vue?vue&type=template&id=1d1b44fc&lang=html&":
/*!******************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!D:/Users/dani/Desktop/@monogrid/node_modules/vue-loader/lib??vue-loader-options!./src/components/SpriteAnimator.vue?vue&type=template&id=1d1b44fc&lang=html& ***!
  \******************************************************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("canvas", {
    ref: "vue-sprite-canvas",
    attrs: { id: _vm.id, width: _vm.width, height: _vm.height }
  })
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "../../node_modules/vue-loader/lib/runtime/componentNormalizer.js":
/*!**************************************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/vue-loader/lib/runtime/componentNormalizer.js ***!
  \**************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return normalizeComponent; });
/* globals __VUE_SSR_CONTEXT__ */

// IMPORTANT: Do NOT use ES2015 features in this file (except for modules).
// This module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle.

function normalizeComponent (
  scriptExports,
  render,
  staticRenderFns,
  functionalTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier, /* server only */
  shadowMode /* vue-cli only */
) {
  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (render) {
    options.render = render
    options.staticRenderFns = staticRenderFns
    options._compiled = true
  }

  // functional template
  if (functionalTemplate) {
    options.functional = true
  }

  // scopedId
  if (scopeId) {
    options._scopeId = 'data-v-' + scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = shadowMode
      ? function () {
        injectStyles.call(
          this,
          (options.functional ? this.parent : this).$root.$options.shadowRoot
        )
      }
      : injectStyles
  }

  if (hook) {
    if (options.functional) {
      // for template-only hot-reload because in that case the render fn doesn't
      // go through the normalizer
      options._injectStyles = hook
      // register for functional component in vue file
      var originalRender = options.render
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return originalRender(h, context)
      }
    } else {
      // inject component registration as beforeCreate hook
      var existing = options.beforeCreate
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    }
  }

  return {
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ "../../node_modules/webpack/buildin/global.js":
/*!***********************************!*\
  !*** (webpack)/buildin/global.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var _typeof = __webpack_require__(/*! @babel/runtime/helpers/typeof */ "../../node_modules/@babel/runtime/helpers/typeof.js");

var g; // This works in non-strict mode

g = function () {
  return this;
}();

try {
  // This works if eval is allowed (see CSP)
  g = g || new Function("return this")();
} catch (e) {
  // This works if the window reference is available
  if ((typeof window === "undefined" ? "undefined" : _typeof(window)) === "object") g = window;
} // g can still be undefined, but nothing to do about it...
// We return undefined, instead of nothing here, so it's
// easier to handle this case. if(!global) { ...}


module.exports = g;

/***/ }),

/***/ "./src/components/SpriteAnimator.vue":
/*!*******************************************!*\
  !*** ./src/components/SpriteAnimator.vue ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _SpriteAnimator_vue_vue_type_template_id_1d1b44fc_lang_html___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./SpriteAnimator.vue?vue&type=template&id=1d1b44fc&lang=html& */ "./src/components/SpriteAnimator.vue?vue&type=template&id=1d1b44fc&lang=html&");
/* harmony import */ var _SpriteAnimator_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./SpriteAnimator.vue?vue&type=script&lang=js& */ "./src/components/SpriteAnimator.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "../../node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _SpriteAnimator_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _SpriteAnimator_vue_vue_type_template_id_1d1b44fc_lang_html___WEBPACK_IMPORTED_MODULE_0__["render"],
  _SpriteAnimator_vue_vue_type_template_id_1d1b44fc_lang_html___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "src/components/SpriteAnimator.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./src/components/SpriteAnimator.vue?vue&type=script&lang=js&":
/*!********************************************************************!*\
  !*** ./src/components/SpriteAnimator.vue?vue&type=script&lang=js& ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_loader_lib_index_js_vue_loader_options_SpriteAnimator_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib!../../../../node_modules/vue-loader/lib??vue-loader-options!./SpriteAnimator.vue?vue&type=script&lang=js& */ "../../node_modules/babel-loader/lib/index.js!../../node_modules/vue-loader/lib/index.js?!./src/components/SpriteAnimator.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_node_modules_vue_loader_lib_index_js_vue_loader_options_SpriteAnimator_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./src/components/SpriteAnimator.vue?vue&type=template&id=1d1b44fc&lang=html&":
/*!************************************************************************************!*\
  !*** ./src/components/SpriteAnimator.vue?vue&type=template&id=1d1b44fc&lang=html& ***!
  \************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_SpriteAnimator_vue_vue_type_template_id_1d1b44fc_lang_html___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./SpriteAnimator.vue?vue&type=template&id=1d1b44fc&lang=html& */ "../../node_modules/vue-loader/lib/loaders/templateLoader.js?!../../node_modules/vue-loader/lib/index.js?!./src/components/SpriteAnimator.vue?vue&type=template&id=1d1b44fc&lang=html&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_SpriteAnimator_vue_vue_type_template_id_1d1b44fc_lang_html___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_SpriteAnimator_vue_vue_type_template_id_1d1b44fc_lang_html___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "@monogrid/js-utils":
/*!*************************************!*\
  !*** external "@monogrid/js-utils" ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE__monogrid_js_utils__;

/***/ })

/******/ });
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly92dWUtbGliL3dlYnBhY2svdW5pdmVyc2FsTW9kdWxlRGVmaW5pdGlvbiIsIndlYnBhY2s6Ly92dWUtbGliL3dlYnBhY2svYm9vdHN0cmFwIiwid2VicGFjazovL3Z1ZS1saWIvRDovVXNlcnMvZGFuaS9EZXNrdG9wL0Btb25vZ3JpZC9ub2RlX21vZHVsZXMvQGJhYmVsL3J1bnRpbWUvaGVscGVycy90eXBlb2YuanMiLCJ3ZWJwYWNrOi8vdnVlLWxpYi9zcmMvY29tcG9uZW50cy9TcHJpdGVBbmltYXRvci52dWUiLCJ3ZWJwYWNrOi8vdnVlLWxpYi9EOi9Vc2Vycy9kYW5pL0Rlc2t0b3AvQG1vbm9ncmlkL25vZGVfbW9kdWxlcy9sb2Rhc2gvX1N5bWJvbC5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL2xvZGFzaC9fYXBwbHkuanMiLCJ3ZWJwYWNrOi8vdnVlLWxpYi9EOi9Vc2Vycy9kYW5pL0Rlc2t0b3AvQG1vbm9ncmlkL25vZGVfbW9kdWxlcy9sb2Rhc2gvX2Jhc2VEZWxheS5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL2xvZGFzaC9fYmFzZUdldFRhZy5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL2xvZGFzaC9fYmFzZUlzTmF0aXZlLmpzIiwid2VicGFjazovL3Z1ZS1saWIvRDovVXNlcnMvZGFuaS9EZXNrdG9wL0Btb25vZ3JpZC9ub2RlX21vZHVsZXMvbG9kYXNoL19iYXNlUmVzdC5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL2xvZGFzaC9fYmFzZVNldFRvU3RyaW5nLmpzIiwid2VicGFjazovL3Z1ZS1saWIvRDovVXNlcnMvZGFuaS9EZXNrdG9wL0Btb25vZ3JpZC9ub2RlX21vZHVsZXMvbG9kYXNoL19jb3JlSnNEYXRhLmpzIiwid2VicGFjazovL3Z1ZS1saWIvRDovVXNlcnMvZGFuaS9EZXNrdG9wL0Btb25vZ3JpZC9ub2RlX21vZHVsZXMvbG9kYXNoL19kZWZpbmVQcm9wZXJ0eS5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL2xvZGFzaC9fZnJlZUdsb2JhbC5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL2xvZGFzaC9fZ2V0TmF0aXZlLmpzIiwid2VicGFjazovL3Z1ZS1saWIvRDovVXNlcnMvZGFuaS9EZXNrdG9wL0Btb25vZ3JpZC9ub2RlX21vZHVsZXMvbG9kYXNoL19nZXRSYXdUYWcuanMiLCJ3ZWJwYWNrOi8vdnVlLWxpYi9EOi9Vc2Vycy9kYW5pL0Rlc2t0b3AvQG1vbm9ncmlkL25vZGVfbW9kdWxlcy9sb2Rhc2gvX2dldFZhbHVlLmpzIiwid2VicGFjazovL3Z1ZS1saWIvRDovVXNlcnMvZGFuaS9EZXNrdG9wL0Btb25vZ3JpZC9ub2RlX21vZHVsZXMvbG9kYXNoL19pc01hc2tlZC5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL2xvZGFzaC9fb2JqZWN0VG9TdHJpbmcuanMiLCJ3ZWJwYWNrOi8vdnVlLWxpYi9EOi9Vc2Vycy9kYW5pL0Rlc2t0b3AvQG1vbm9ncmlkL25vZGVfbW9kdWxlcy9sb2Rhc2gvX292ZXJSZXN0LmpzIiwid2VicGFjazovL3Z1ZS1saWIvRDovVXNlcnMvZGFuaS9EZXNrdG9wL0Btb25vZ3JpZC9ub2RlX21vZHVsZXMvbG9kYXNoL19yb290LmpzIiwid2VicGFjazovL3Z1ZS1saWIvRDovVXNlcnMvZGFuaS9EZXNrdG9wL0Btb25vZ3JpZC9ub2RlX21vZHVsZXMvbG9kYXNoL19zZXRUb1N0cmluZy5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL2xvZGFzaC9fc2hvcnRPdXQuanMiLCJ3ZWJwYWNrOi8vdnVlLWxpYi9EOi9Vc2Vycy9kYW5pL0Rlc2t0b3AvQG1vbm9ncmlkL25vZGVfbW9kdWxlcy9sb2Rhc2gvX3RvU291cmNlLmpzIiwid2VicGFjazovL3Z1ZS1saWIvRDovVXNlcnMvZGFuaS9EZXNrdG9wL0Btb25vZ3JpZC9ub2RlX21vZHVsZXMvbG9kYXNoL2NvbnN0YW50LmpzIiwid2VicGFjazovL3Z1ZS1saWIvRDovVXNlcnMvZGFuaS9EZXNrdG9wL0Btb25vZ3JpZC9ub2RlX21vZHVsZXMvbG9kYXNoL2RlbGF5LmpzIiwid2VicGFjazovL3Z1ZS1saWIvRDovVXNlcnMvZGFuaS9EZXNrdG9wL0Btb25vZ3JpZC9ub2RlX21vZHVsZXMvbG9kYXNoL2lkZW50aXR5LmpzIiwid2VicGFjazovL3Z1ZS1saWIvRDovVXNlcnMvZGFuaS9EZXNrdG9wL0Btb25vZ3JpZC9ub2RlX21vZHVsZXMvbG9kYXNoL2lzRnVuY3Rpb24uanMiLCJ3ZWJwYWNrOi8vdnVlLWxpYi9EOi9Vc2Vycy9kYW5pL0Rlc2t0b3AvQG1vbm9ncmlkL25vZGVfbW9kdWxlcy9sb2Rhc2gvaXNPYmplY3QuanMiLCJ3ZWJwYWNrOi8vdnVlLWxpYi9EOi9Vc2Vycy9kYW5pL0Rlc2t0b3AvQG1vbm9ncmlkL25vZGVfbW9kdWxlcy9sb2Rhc2gvaXNPYmplY3RMaWtlLmpzIiwid2VicGFjazovL3Z1ZS1saWIvRDovVXNlcnMvZGFuaS9EZXNrdG9wL0Btb25vZ3JpZC9ub2RlX21vZHVsZXMvbG9kYXNoL2lzU3ltYm9sLmpzIiwid2VicGFjazovL3Z1ZS1saWIvRDovVXNlcnMvZGFuaS9EZXNrdG9wL0Btb25vZ3JpZC9ub2RlX21vZHVsZXMvbG9kYXNoL3RvTnVtYmVyLmpzIiwid2VicGFjazovL3Z1ZS1saWIvLi9zcmMvY29tcG9uZW50cy9TcHJpdGVBbmltYXRvci52dWU/M2Q1MyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3J1bnRpbWUvY29tcG9uZW50Tm9ybWFsaXplci5qcyIsIndlYnBhY2s6Ly92dWUtbGliLyh3ZWJwYWNrKS9idWlsZGluL2dsb2JhbC5qcyIsIndlYnBhY2s6Ly92dWUtbGliLy4vc3JjL2NvbXBvbmVudHMvU3ByaXRlQW5pbWF0b3IudnVlIiwid2VicGFjazovL3Z1ZS1saWIvLi9zcmMvY29tcG9uZW50cy9TcHJpdGVBbmltYXRvci52dWU/ZWU1MCIsIndlYnBhY2s6Ly92dWUtbGliLy4vc3JjL2NvbXBvbmVudHMvU3ByaXRlQW5pbWF0b3IudnVlPzYyNDMiLCJ3ZWJwYWNrOi8vdnVlLWxpYi9leHRlcm5hbCBcIkBtb25vZ3JpZC9qcy11dGlsc1wiIl0sIm5hbWVzIjpbIl90eXBlb2YiLCJvYmoiLCJTeW1ib2wiLCJpdGVyYXRvciIsIm1vZHVsZSIsImV4cG9ydHMiLCJjb25zdHJ1Y3RvciIsInByb3RvdHlwZSIsInJvb3QiLCJyZXF1aXJlIiwiYXBwbHkiLCJmdW5jIiwidGhpc0FyZyIsImFyZ3MiLCJsZW5ndGgiLCJjYWxsIiwiRlVOQ19FUlJPUl9URVhUIiwiYmFzZURlbGF5Iiwid2FpdCIsIlR5cGVFcnJvciIsInNldFRpbWVvdXQiLCJ1bmRlZmluZWQiLCJnZXRSYXdUYWciLCJvYmplY3RUb1N0cmluZyIsIm51bGxUYWciLCJ1bmRlZmluZWRUYWciLCJzeW1Ub1N0cmluZ1RhZyIsInRvU3RyaW5nVGFnIiwiYmFzZUdldFRhZyIsInZhbHVlIiwiT2JqZWN0IiwiaXNGdW5jdGlvbiIsImlzTWFza2VkIiwiaXNPYmplY3QiLCJ0b1NvdXJjZSIsInJlUmVnRXhwQ2hhciIsInJlSXNIb3N0Q3RvciIsImZ1bmNQcm90byIsIkZ1bmN0aW9uIiwib2JqZWN0UHJvdG8iLCJmdW5jVG9TdHJpbmciLCJ0b1N0cmluZyIsImhhc093blByb3BlcnR5IiwicmVJc05hdGl2ZSIsIlJlZ0V4cCIsInJlcGxhY2UiLCJiYXNlSXNOYXRpdmUiLCJwYXR0ZXJuIiwidGVzdCIsImlkZW50aXR5Iiwib3ZlclJlc3QiLCJzZXRUb1N0cmluZyIsImJhc2VSZXN0Iiwic3RhcnQiLCJjb25zdGFudCIsImRlZmluZVByb3BlcnR5IiwiYmFzZVNldFRvU3RyaW5nIiwic3RyaW5nIiwiY29yZUpzRGF0YSIsImdldE5hdGl2ZSIsImUiLCJmcmVlR2xvYmFsIiwiZ2xvYmFsIiwiZ2V0VmFsdWUiLCJvYmplY3QiLCJrZXkiLCJuYXRpdmVPYmplY3RUb1N0cmluZyIsImlzT3duIiwidGFnIiwidW5tYXNrZWQiLCJyZXN1bHQiLCJtYXNrU3JjS2V5IiwidWlkIiwiZXhlYyIsImtleXMiLCJJRV9QUk9UTyIsIm5hdGl2ZU1heCIsIk1hdGgiLCJtYXgiLCJ0cmFuc2Zvcm0iLCJhcmd1bWVudHMiLCJpbmRleCIsImFycmF5IiwiQXJyYXkiLCJvdGhlckFyZ3MiLCJmcmVlU2VsZiIsInNlbGYiLCJzaG9ydE91dCIsIkhPVF9DT1VOVCIsIkhPVF9TUEFOIiwibmF0aXZlTm93IiwiRGF0ZSIsIm5vdyIsImNvdW50IiwibGFzdENhbGxlZCIsInN0YW1wIiwicmVtYWluaW5nIiwidG9OdW1iZXIiLCJkZWxheSIsImFzeW5jVGFnIiwiZnVuY1RhZyIsImdlblRhZyIsInByb3h5VGFnIiwidHlwZSIsImlzT2JqZWN0TGlrZSIsInN5bWJvbFRhZyIsImlzU3ltYm9sIiwiTkFOIiwicmVUcmltIiwicmVJc0JhZEhleCIsInJlSXNCaW5hcnkiLCJyZUlzT2N0YWwiLCJmcmVlUGFyc2VJbnQiLCJwYXJzZUludCIsIm90aGVyIiwidmFsdWVPZiIsImlzQmluYXJ5Iiwic2xpY2UiLCJnIiwid2luZG93Il0sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDO0FBQ0QsTztRQ1ZBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBOzs7UUFHQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0EsMENBQTBDLGdDQUFnQztRQUMxRTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLHdEQUF3RCxrQkFBa0I7UUFDMUU7UUFDQSxpREFBaUQsY0FBYztRQUMvRDs7UUFFQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0EseUNBQXlDLGlDQUFpQztRQUMxRSxnSEFBZ0gsbUJBQW1CLEVBQUU7UUFDckk7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSwyQkFBMkIsMEJBQTBCLEVBQUU7UUFDdkQsaUNBQWlDLGVBQWU7UUFDaEQ7UUFDQTtRQUNBOztRQUVBO1FBQ0Esc0RBQXNELCtEQUErRDs7UUFFckg7UUFDQTs7O1FBR0E7UUFDQTs7Ozs7Ozs7Ozs7O0FDbEZBLFNBQVNBLE9BQVQsQ0FBaUJDLEdBQWpCLEVBQXNCO0FBQ3BCOztBQUVBLE1BQUksT0FBT0MsTUFBUCxLQUFrQixVQUFsQixJQUFnQyxPQUFPQSxNQUFNLENBQUNDLFFBQWQsS0FBMkIsUUFBL0QsRUFBeUU7QUFDdkVDLFVBQU0sQ0FBQ0MsT0FBUCxHQUFpQkwsT0FBTyxHQUFHLFNBQVNBLE9BQVQsQ0FBaUJDLEdBQWpCLEVBQXNCO0FBQy9DLGFBQU8sT0FBT0EsR0FBZDtBQUNELEtBRkQ7QUFHRCxHQUpELE1BSU87QUFDTEcsVUFBTSxDQUFDQyxPQUFQLEdBQWlCTCxPQUFPLEdBQUcsU0FBU0EsT0FBVCxDQUFpQkMsR0FBakIsRUFBc0I7QUFDL0MsYUFBT0EsR0FBRyxJQUFJLE9BQU9DLE1BQVAsS0FBa0IsVUFBekIsSUFBdUNELEdBQUcsQ0FBQ0ssV0FBSixLQUFvQkosTUFBM0QsSUFBcUVELEdBQUcsS0FBS0MsTUFBTSxDQUFDSyxTQUFwRixHQUFnRyxRQUFoRyxHQUEyRyxPQUFPTixHQUF6SDtBQUNELEtBRkQ7QUFHRDs7QUFFRCxTQUFPRCxPQUFPLENBQUNDLEdBQUQsQ0FBZDtBQUNEOztBQUVERyxNQUFNLENBQUNDLE9BQVAsR0FBaUJMLE9BQWpCLEM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDTkE7QUFDQTtBQUNBOzs7Ozs7O0FBTUE7QUFDQSx3QkFEQTtBQUdBO0FBQ0E7OztBQUdBO0FBQ0Esb0JBREE7QUFFQSxrQkFGQTtBQUdBO0FBSEEsS0FKQTs7QUFTQTs7O0FBR0E7QUFDQSxvQkFEQTtBQUVBLGtCQUZBO0FBR0EsYUFIQSxzQkFHQTtBQUNBO0FBQ0E7QUFMQSxLQVpBOztBQW1CQTs7O0FBR0E7QUFDQSxrQkFEQTtBQUVBO0FBRkEsS0F0QkE7O0FBMEJBOzs7QUFHQTtBQUNBLG1CQURBO0FBRUE7QUFGQSxLQTdCQTs7QUFpQ0E7OztBQUdBO0FBQ0Esa0JBREE7QUFFQSxhQUZBLHNCQUVBO0FBQ0E7QUFDQTtBQUpBLEtBcENBOztBQTBDQTs7Ozs7O0FBTUE7QUFDQSxtQkFEQTtBQUVBO0FBRkEsS0FoREE7O0FBb0RBOzs7QUFHQTtBQUNBLG1CQURBO0FBRUE7QUFGQSxLQXZEQTs7QUEyREE7OztBQUdBO0FBQ0Esa0JBREE7QUFFQTtBQUZBO0FBOURBLEdBSEE7QUF1RUE7QUFDQTs7O0FBR0EsUUFKQSxnQkFJQSxLQUpBLEVBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVJBLEdBdkVBO0FBa0ZBLE1BbEZBLGtCQWtGQTtBQUNBO0FBQ0EsZ0JBREE7QUFFQSxlQUZBO0FBR0EsbUJBSEE7QUFJQSxxQkFKQTtBQUtBLHNCQUxBO0FBTUEsa0JBTkE7QUFPQSxlQVBBO0FBUUEsY0FSQTtBQVNBLFlBVEE7QUFVQTtBQVZBO0FBWUEsR0EvRkE7QUFnR0EsU0FoR0EscUJBZ0dBO0FBQUE7O0FBQ0E7QUFDQTtBQUNBLDRCQURBO0FBRUEsd0JBRkE7QUFHQSx3QkFIQTtBQUlBLHdCQUpBO0FBS0E7QUFMQTtBQU9BLEtBUkEsRUFEQSxDQVVBOztBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBOUdBO0FBK0dBLFNBL0dBLHFCQStHQTtBQUFBOztBQUNBO0FBQ0E7QUFDQTs7QUFDQTtBQUFBOztBQUFBO0FBQUE7QUFDQSxLQUpBO0FBS0EsR0FySEE7QUFzSEE7QUFDQSxRQURBLGdCQUNBLEdBREEsRUFDQTtBQUNBO0FBQ0E7Ozs7O0FBSUE7O0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQVpBOztBQWFBOzs7QUFHQSxVQWhCQSxvQkFnQkE7QUFDQTs7QUFDQTtBQUNBO0FBQ0E7O0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQXpCQTs7QUEwQkE7OztBQUdBLFVBN0JBLG9CQTZCQTtBQUNBO0FBQ0E7O0FBQ0E7QUFDQTtBQUNBO0FBQ0EsNEJBSEEsQ0FJQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOzs7OztBQUlBO0FBQ0E7QUFDQSxLQWhEQTs7QUFpREE7OztBQUdBLFFBcERBLGtCQW9EQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7QUFJQTtBQUNBLEtBNURBOztBQTZEQTs7O0FBR0EsUUFoRUEsZ0JBZ0VBLElBaEVBLEVBZ0VBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7QUFHQTtBQUNBO0FBeEVBO0FBdEhBLEc7Ozs7Ozs7Ozs7O0FDbEJBLElBQUlRLElBQUksR0FBR0MsbUJBQU8sQ0FBQyxtREFBRCxDQUFsQjtBQUVBOzs7QUFDQSxJQUFJUCxPQUFNLEdBQUdNLElBQUksQ0FBQ04sTUFBbEI7QUFFQUUsTUFBTSxDQUFDQyxPQUFQLEdBQWlCSCxPQUFqQixDOzs7Ozs7Ozs7OztBQ0xBOzs7Ozs7Ozs7O0FBVUEsU0FBU1EsS0FBVCxDQUFlQyxJQUFmLEVBQXFCQyxPQUFyQixFQUE4QkMsSUFBOUIsRUFBb0M7QUFDbEMsVUFBUUEsSUFBSSxDQUFDQyxNQUFiO0FBQ0UsU0FBSyxDQUFMO0FBQVEsYUFBT0gsSUFBSSxDQUFDSSxJQUFMLENBQVVILE9BQVYsQ0FBUDs7QUFDUixTQUFLLENBQUw7QUFBUSxhQUFPRCxJQUFJLENBQUNJLElBQUwsQ0FBVUgsT0FBVixFQUFtQkMsSUFBSSxDQUFDLENBQUQsQ0FBdkIsQ0FBUDs7QUFDUixTQUFLLENBQUw7QUFBUSxhQUFPRixJQUFJLENBQUNJLElBQUwsQ0FBVUgsT0FBVixFQUFtQkMsSUFBSSxDQUFDLENBQUQsQ0FBdkIsRUFBNEJBLElBQUksQ0FBQyxDQUFELENBQWhDLENBQVA7O0FBQ1IsU0FBSyxDQUFMO0FBQVEsYUFBT0YsSUFBSSxDQUFDSSxJQUFMLENBQVVILE9BQVYsRUFBbUJDLElBQUksQ0FBQyxDQUFELENBQXZCLEVBQTRCQSxJQUFJLENBQUMsQ0FBRCxDQUFoQyxFQUFxQ0EsSUFBSSxDQUFDLENBQUQsQ0FBekMsQ0FBUDtBQUpWOztBQU1BLFNBQU9GLElBQUksQ0FBQ0QsS0FBTCxDQUFXRSxPQUFYLEVBQW9CQyxJQUFwQixDQUFQO0FBQ0Q7O0FBRURULE1BQU0sQ0FBQ0MsT0FBUCxHQUFpQkssS0FBakIsQzs7Ozs7Ozs7Ozs7QUNwQkE7QUFDQSxJQUFJTSxlQUFlLEdBQUcscUJBQXRCO0FBRUE7Ozs7Ozs7Ozs7O0FBVUEsU0FBU0MsU0FBVCxDQUFtQk4sSUFBbkIsRUFBeUJPLElBQXpCLEVBQStCTCxJQUEvQixFQUFxQztBQUNuQyxNQUFJLE9BQU9GLElBQVAsSUFBZSxVQUFuQixFQUErQjtBQUM3QixVQUFNLElBQUlRLFNBQUosQ0FBY0gsZUFBZCxDQUFOO0FBQ0Q7O0FBQ0QsU0FBT0ksVUFBVSxDQUFDLFlBQVc7QUFBRVQsUUFBSSxDQUFDRCxLQUFMLENBQVdXLFNBQVgsRUFBc0JSLElBQXRCO0FBQThCLEdBQTVDLEVBQThDSyxJQUE5QyxDQUFqQjtBQUNEOztBQUVEZCxNQUFNLENBQUNDLE9BQVAsR0FBaUJZLFNBQWpCLEM7Ozs7Ozs7Ozs7O0FDcEJBLElBQUlmLE9BQU0sR0FBR08sbUJBQU8sQ0FBQyx1REFBRCxDQUFwQjtBQUFBLElBQ0lhLFNBQVMsR0FBR2IsbUJBQU8sQ0FBQyw2REFBRCxDQUR2QjtBQUFBLElBRUljLGNBQWMsR0FBR2QsbUJBQU8sQ0FBQyx1RUFBRCxDQUY1QjtBQUlBOzs7QUFDQSxJQUFJZSxPQUFPLEdBQUcsZUFBZDtBQUFBLElBQ0lDLFlBQVksR0FBRyxvQkFEbkI7QUFHQTs7QUFDQSxJQUFJQyxjQUFjLEdBQUd4QixPQUFNLEdBQUdBLE9BQU0sQ0FBQ3lCLFdBQVYsR0FBd0JOLFNBQW5EO0FBRUE7Ozs7Ozs7O0FBT0EsU0FBU08sVUFBVCxDQUFvQkMsS0FBcEIsRUFBMkI7QUFDekIsTUFBSUEsS0FBSyxJQUFJLElBQWIsRUFBbUI7QUFDakIsV0FBT0EsS0FBSyxLQUFLUixTQUFWLEdBQXNCSSxZQUF0QixHQUFxQ0QsT0FBNUM7QUFDRDs7QUFDRCxTQUFRRSxjQUFjLElBQUlBLGNBQWMsSUFBSUksTUFBTSxDQUFDRCxLQUFELENBQTNDLEdBQ0hQLFNBQVMsQ0FBQ08sS0FBRCxDQUROLEdBRUhOLGNBQWMsQ0FBQ00sS0FBRCxDQUZsQjtBQUdEOztBQUVEekIsTUFBTSxDQUFDQyxPQUFQLEdBQWlCdUIsVUFBakIsQzs7Ozs7Ozs7Ozs7QUMzQkEsSUFBSUcsVUFBVSxHQUFHdEIsbUJBQU8sQ0FBQyw2REFBRCxDQUF4QjtBQUFBLElBQ0l1QixRQUFRLEdBQUd2QixtQkFBTyxDQUFDLDJEQUFELENBRHRCO0FBQUEsSUFFSXdCLFFBQVEsR0FBR3hCLG1CQUFPLENBQUMseURBQUQsQ0FGdEI7QUFBQSxJQUdJeUIsUUFBUSxHQUFHekIsbUJBQU8sQ0FBQywyREFBRCxDQUh0QjtBQUtBOzs7Ozs7QUFJQSxJQUFJMEIsWUFBWSxHQUFHLHFCQUFuQjtBQUVBOztBQUNBLElBQUlDLFlBQVksR0FBRyw2QkFBbkI7QUFFQTs7QUFDQSxJQUFJQyxTQUFTLEdBQUdDLFFBQVEsQ0FBQy9CLFNBQXpCO0FBQUEsSUFDSWdDLFdBQVcsR0FBR1QsTUFBTSxDQUFDdkIsU0FEekI7QUFHQTs7QUFDQSxJQUFJaUMsWUFBWSxHQUFHSCxTQUFTLENBQUNJLFFBQTdCO0FBRUE7O0FBQ0EsSUFBSUMsY0FBYyxHQUFHSCxXQUFXLENBQUNHLGNBQWpDO0FBRUE7O0FBQ0EsSUFBSUMsVUFBVSxHQUFHQyxNQUFNLENBQUMsTUFDdEJKLFlBQVksQ0FBQ3pCLElBQWIsQ0FBa0IyQixjQUFsQixFQUFrQ0csT0FBbEMsQ0FBMENWLFlBQTFDLEVBQXdELE1BQXhELEVBQ0NVLE9BREQsQ0FDUyx3REFEVCxFQUNtRSxPQURuRSxDQURzQixHQUV3RCxHQUZ6RCxDQUF2QjtBQUtBOzs7Ozs7Ozs7QUFRQSxTQUFTQyxZQUFULENBQXNCakIsS0FBdEIsRUFBNkI7QUFDM0IsTUFBSSxDQUFDSSxRQUFRLENBQUNKLEtBQUQsQ0FBVCxJQUFvQkcsUUFBUSxDQUFDSCxLQUFELENBQWhDLEVBQXlDO0FBQ3ZDLFdBQU8sS0FBUDtBQUNEOztBQUNELE1BQUlrQixPQUFPLEdBQUdoQixVQUFVLENBQUNGLEtBQUQsQ0FBVixHQUFvQmMsVUFBcEIsR0FBaUNQLFlBQS9DO0FBQ0EsU0FBT1csT0FBTyxDQUFDQyxJQUFSLENBQWFkLFFBQVEsQ0FBQ0wsS0FBRCxDQUFyQixDQUFQO0FBQ0Q7O0FBRUR6QixNQUFNLENBQUNDLE9BQVAsR0FBaUJ5QyxZQUFqQixDOzs7Ozs7Ozs7OztBQzlDQSxJQUFJRyxRQUFRLEdBQUd4QyxtQkFBTyxDQUFDLHlEQUFELENBQXRCO0FBQUEsSUFDSXlDLFFBQVEsR0FBR3pDLG1CQUFPLENBQUMsMkRBQUQsQ0FEdEI7QUFBQSxJQUVJMEMsV0FBVyxHQUFHMUMsbUJBQU8sQ0FBQyxpRUFBRCxDQUZ6QjtBQUlBOzs7Ozs7Ozs7O0FBUUEsU0FBUzJDLFFBQVQsQ0FBa0J6QyxJQUFsQixFQUF3QjBDLEtBQXhCLEVBQStCO0FBQzdCLFNBQU9GLFdBQVcsQ0FBQ0QsUUFBUSxDQUFDdkMsSUFBRCxFQUFPMEMsS0FBUCxFQUFjSixRQUFkLENBQVQsRUFBa0N0QyxJQUFJLEdBQUcsRUFBekMsQ0FBbEI7QUFDRDs7QUFFRFAsTUFBTSxDQUFDQyxPQUFQLEdBQWlCK0MsUUFBakIsQzs7Ozs7Ozs7Ozs7QUNoQkEsSUFBSUUsUUFBUSxHQUFHN0MsbUJBQU8sQ0FBQyx5REFBRCxDQUF0QjtBQUFBLElBQ0k4QyxjQUFjLEdBQUc5QyxtQkFBTyxDQUFDLHVFQUFELENBRDVCO0FBQUEsSUFFSXdDLFFBQVEsR0FBR3hDLG1CQUFPLENBQUMseURBQUQsQ0FGdEI7QUFJQTs7Ozs7Ozs7OztBQVFBLElBQUkrQyxlQUFlLEdBQUcsQ0FBQ0QsY0FBRCxHQUFrQk4sUUFBbEIsR0FBNkIsVUFBU3RDLElBQVQsRUFBZThDLE1BQWYsRUFBdUI7QUFDeEUsU0FBT0YsY0FBYyxDQUFDNUMsSUFBRCxFQUFPLFVBQVAsRUFBbUI7QUFDdEMsb0JBQWdCLElBRHNCO0FBRXRDLGtCQUFjLEtBRndCO0FBR3RDLGFBQVMyQyxRQUFRLENBQUNHLE1BQUQsQ0FIcUI7QUFJdEMsZ0JBQVk7QUFKMEIsR0FBbkIsQ0FBckI7QUFNRCxDQVBEO0FBU0FyRCxNQUFNLENBQUNDLE9BQVAsR0FBaUJtRCxlQUFqQixDOzs7Ozs7Ozs7OztBQ3JCQSxJQUFJaEQsSUFBSSxHQUFHQyxtQkFBTyxDQUFDLG1EQUFELENBQWxCO0FBRUE7OztBQUNBLElBQUlpRCxVQUFVLEdBQUdsRCxJQUFJLENBQUMsb0JBQUQsQ0FBckI7QUFFQUosTUFBTSxDQUFDQyxPQUFQLEdBQWlCcUQsVUFBakIsQzs7Ozs7Ozs7Ozs7QUNMQSxJQUFJQyxTQUFTLEdBQUdsRCxtQkFBTyxDQUFDLDZEQUFELENBQXZCOztBQUVBLElBQUk4QyxjQUFjLEdBQUksWUFBVztBQUMvQixNQUFJO0FBQ0YsUUFBSTVDLElBQUksR0FBR2dELFNBQVMsQ0FBQzdCLE1BQUQsRUFBUyxnQkFBVCxDQUFwQjtBQUNBbkIsUUFBSSxDQUFDLEVBQUQsRUFBSyxFQUFMLEVBQVMsRUFBVCxDQUFKO0FBQ0EsV0FBT0EsSUFBUDtBQUNELEdBSkQsQ0FJRSxPQUFPaUQsQ0FBUCxFQUFVLENBQUU7QUFDZixDQU5xQixFQUF0Qjs7QUFRQXhELE1BQU0sQ0FBQ0MsT0FBUCxHQUFpQmtELGNBQWpCLEM7Ozs7Ozs7Ozs7Ozs7QUNWQTtBQUNBLElBQUlNLFVBQVUsR0FBRyxRQUFPQyxNQUFQLHlDQUFPQSxNQUFQLE1BQWlCLFFBQWpCLElBQTZCQSxNQUE3QixJQUF1Q0EsTUFBTSxDQUFDaEMsTUFBUCxLQUFrQkEsTUFBekQsSUFBbUVnQyxNQUFwRjtBQUVBMUQsTUFBTSxDQUFDQyxPQUFQLEdBQWlCd0QsVUFBakIsQzs7Ozs7Ozs7Ozs7O0FDSEEsSUFBSWYsWUFBWSxHQUFHckMsbUJBQU8sQ0FBQyxtRUFBRCxDQUExQjtBQUFBLElBQ0lzRCxRQUFRLEdBQUd0RCxtQkFBTyxDQUFDLDJEQUFELENBRHRCO0FBR0E7Ozs7Ozs7Ozs7QUFRQSxTQUFTa0QsU0FBVCxDQUFtQkssTUFBbkIsRUFBMkJDLEdBQTNCLEVBQWdDO0FBQzlCLE1BQUlwQyxLQUFLLEdBQUdrQyxRQUFRLENBQUNDLE1BQUQsRUFBU0MsR0FBVCxDQUFwQjtBQUNBLFNBQU9uQixZQUFZLENBQUNqQixLQUFELENBQVosR0FBc0JBLEtBQXRCLEdBQThCUixTQUFyQztBQUNEOztBQUVEakIsTUFBTSxDQUFDQyxPQUFQLEdBQWlCc0QsU0FBakIsQzs7Ozs7Ozs7Ozs7QUNoQkEsSUFBSXpELE9BQU0sR0FBR08sbUJBQU8sQ0FBQyx1REFBRCxDQUFwQjtBQUVBOzs7QUFDQSxJQUFJOEIsV0FBVyxHQUFHVCxNQUFNLENBQUN2QixTQUF6QjtBQUVBOztBQUNBLElBQUltQyxjQUFjLEdBQUdILFdBQVcsQ0FBQ0csY0FBakM7QUFFQTs7Ozs7O0FBS0EsSUFBSXdCLG9CQUFvQixHQUFHM0IsV0FBVyxDQUFDRSxRQUF2QztBQUVBOztBQUNBLElBQUlmLGNBQWMsR0FBR3hCLE9BQU0sR0FBR0EsT0FBTSxDQUFDeUIsV0FBVixHQUF3Qk4sU0FBbkQ7QUFFQTs7Ozs7Ozs7QUFPQSxTQUFTQyxTQUFULENBQW1CTyxLQUFuQixFQUEwQjtBQUN4QixNQUFJc0MsS0FBSyxHQUFHekIsY0FBYyxDQUFDM0IsSUFBZixDQUFvQmMsS0FBcEIsRUFBMkJILGNBQTNCLENBQVo7QUFBQSxNQUNJMEMsR0FBRyxHQUFHdkMsS0FBSyxDQUFDSCxjQUFELENBRGY7O0FBR0EsTUFBSTtBQUNGRyxTQUFLLENBQUNILGNBQUQsQ0FBTCxHQUF3QkwsU0FBeEI7QUFDQSxRQUFJZ0QsUUFBUSxHQUFHLElBQWY7QUFDRCxHQUhELENBR0UsT0FBT1QsQ0FBUCxFQUFVLENBQUU7O0FBRWQsTUFBSVUsTUFBTSxHQUFHSixvQkFBb0IsQ0FBQ25ELElBQXJCLENBQTBCYyxLQUExQixDQUFiOztBQUNBLE1BQUl3QyxRQUFKLEVBQWM7QUFDWixRQUFJRixLQUFKLEVBQVc7QUFDVHRDLFdBQUssQ0FBQ0gsY0FBRCxDQUFMLEdBQXdCMEMsR0FBeEI7QUFDRCxLQUZELE1BRU87QUFDTCxhQUFPdkMsS0FBSyxDQUFDSCxjQUFELENBQVo7QUFDRDtBQUNGOztBQUNELFNBQU80QyxNQUFQO0FBQ0Q7O0FBRURsRSxNQUFNLENBQUNDLE9BQVAsR0FBaUJpQixTQUFqQixDOzs7Ozs7Ozs7OztBQzdDQTs7Ozs7Ozs7QUFRQSxTQUFTeUMsUUFBVCxDQUFrQkMsTUFBbEIsRUFBMEJDLEdBQTFCLEVBQStCO0FBQzdCLFNBQU9ELE1BQU0sSUFBSSxJQUFWLEdBQWlCM0MsU0FBakIsR0FBNkIyQyxNQUFNLENBQUNDLEdBQUQsQ0FBMUM7QUFDRDs7QUFFRDdELE1BQU0sQ0FBQ0MsT0FBUCxHQUFpQjBELFFBQWpCLEM7Ozs7Ozs7Ozs7O0FDWkEsSUFBSUwsVUFBVSxHQUFHakQsbUJBQU8sQ0FBQywrREFBRCxDQUF4QjtBQUVBOzs7QUFDQSxJQUFJOEQsVUFBVSxHQUFJLFlBQVc7QUFDM0IsTUFBSUMsR0FBRyxHQUFHLFNBQVNDLElBQVQsQ0FBY2YsVUFBVSxJQUFJQSxVQUFVLENBQUNnQixJQUF6QixJQUFpQ2hCLFVBQVUsQ0FBQ2dCLElBQVgsQ0FBZ0JDLFFBQWpELElBQTZELEVBQTNFLENBQVY7QUFDQSxTQUFPSCxHQUFHLEdBQUksbUJBQW1CQSxHQUF2QixHQUE4QixFQUF4QztBQUNELENBSGlCLEVBQWxCO0FBS0E7Ozs7Ozs7OztBQU9BLFNBQVN4QyxRQUFULENBQWtCckIsSUFBbEIsRUFBd0I7QUFDdEIsU0FBTyxDQUFDLENBQUM0RCxVQUFGLElBQWlCQSxVQUFVLElBQUk1RCxJQUF0QztBQUNEOztBQUVEUCxNQUFNLENBQUNDLE9BQVAsR0FBaUIyQixRQUFqQixDOzs7Ozs7Ozs7OztBQ25CQTtBQUNBLElBQUlPLFdBQVcsR0FBR1QsTUFBTSxDQUFDdkIsU0FBekI7QUFFQTs7Ozs7O0FBS0EsSUFBSTJELG9CQUFvQixHQUFHM0IsV0FBVyxDQUFDRSxRQUF2QztBQUVBOzs7Ozs7OztBQU9BLFNBQVNsQixjQUFULENBQXdCTSxLQUF4QixFQUErQjtBQUM3QixTQUFPcUMsb0JBQW9CLENBQUNuRCxJQUFyQixDQUEwQmMsS0FBMUIsQ0FBUDtBQUNEOztBQUVEekIsTUFBTSxDQUFDQyxPQUFQLEdBQWlCa0IsY0FBakIsQzs7Ozs7Ozs7Ozs7QUNyQkEsSUFBSWIsS0FBSyxHQUFHRCxtQkFBTyxDQUFDLHFEQUFELENBQW5CO0FBRUE7OztBQUNBLElBQUltRSxTQUFTLEdBQUdDLElBQUksQ0FBQ0MsR0FBckI7QUFFQTs7Ozs7Ozs7OztBQVNBLFNBQVM1QixRQUFULENBQWtCdkMsSUFBbEIsRUFBd0IwQyxLQUF4QixFQUErQjBCLFNBQS9CLEVBQTBDO0FBQ3hDMUIsT0FBSyxHQUFHdUIsU0FBUyxDQUFDdkIsS0FBSyxLQUFLaEMsU0FBVixHQUF1QlYsSUFBSSxDQUFDRyxNQUFMLEdBQWMsQ0FBckMsR0FBMEN1QyxLQUEzQyxFQUFrRCxDQUFsRCxDQUFqQjtBQUNBLFNBQU8sWUFBVztBQUNoQixRQUFJeEMsSUFBSSxHQUFHbUUsU0FBWDtBQUFBLFFBQ0lDLEtBQUssR0FBRyxDQUFDLENBRGI7QUFBQSxRQUVJbkUsTUFBTSxHQUFHOEQsU0FBUyxDQUFDL0QsSUFBSSxDQUFDQyxNQUFMLEdBQWN1QyxLQUFmLEVBQXNCLENBQXRCLENBRnRCO0FBQUEsUUFHSTZCLEtBQUssR0FBR0MsS0FBSyxDQUFDckUsTUFBRCxDQUhqQjs7QUFLQSxXQUFPLEVBQUVtRSxLQUFGLEdBQVVuRSxNQUFqQixFQUF5QjtBQUN2Qm9FLFdBQUssQ0FBQ0QsS0FBRCxDQUFMLEdBQWVwRSxJQUFJLENBQUN3QyxLQUFLLEdBQUc0QixLQUFULENBQW5CO0FBQ0Q7O0FBQ0RBLFNBQUssR0FBRyxDQUFDLENBQVQ7QUFDQSxRQUFJRyxTQUFTLEdBQUdELEtBQUssQ0FBQzlCLEtBQUssR0FBRyxDQUFULENBQXJCOztBQUNBLFdBQU8sRUFBRTRCLEtBQUYsR0FBVTVCLEtBQWpCLEVBQXdCO0FBQ3RCK0IsZUFBUyxDQUFDSCxLQUFELENBQVQsR0FBbUJwRSxJQUFJLENBQUNvRSxLQUFELENBQXZCO0FBQ0Q7O0FBQ0RHLGFBQVMsQ0FBQy9CLEtBQUQsQ0FBVCxHQUFtQjBCLFNBQVMsQ0FBQ0csS0FBRCxDQUE1QjtBQUNBLFdBQU94RSxLQUFLLENBQUNDLElBQUQsRUFBTyxJQUFQLEVBQWF5RSxTQUFiLENBQVo7QUFDRCxHQWhCRDtBQWlCRDs7QUFFRGhGLE1BQU0sQ0FBQ0MsT0FBUCxHQUFpQjZDLFFBQWpCLEM7Ozs7Ozs7Ozs7Ozs7QUNuQ0EsSUFBSVcsVUFBVSxHQUFHcEQsbUJBQU8sQ0FBQywrREFBRCxDQUF4QjtBQUVBOzs7QUFDQSxJQUFJNEUsUUFBUSxHQUFHLFFBQU9DLElBQVAseUNBQU9BLElBQVAsTUFBZSxRQUFmLElBQTJCQSxJQUEzQixJQUFtQ0EsSUFBSSxDQUFDeEQsTUFBTCxLQUFnQkEsTUFBbkQsSUFBNkR3RCxJQUE1RTtBQUVBOztBQUNBLElBQUk5RSxJQUFJLEdBQUdxRCxVQUFVLElBQUl3QixRQUFkLElBQTBCL0MsUUFBUSxDQUFDLGFBQUQsQ0FBUixFQUFyQztBQUVBbEMsTUFBTSxDQUFDQyxPQUFQLEdBQWlCRyxJQUFqQixDOzs7Ozs7Ozs7OztBQ1JBLElBQUlnRCxlQUFlLEdBQUcvQyxtQkFBTyxDQUFDLHlFQUFELENBQTdCO0FBQUEsSUFDSThFLFFBQVEsR0FBRzlFLG1CQUFPLENBQUMsMkRBQUQsQ0FEdEI7QUFHQTs7Ozs7Ozs7OztBQVFBLElBQUkwQyxXQUFXLEdBQUdvQyxRQUFRLENBQUMvQixlQUFELENBQTFCO0FBRUFwRCxNQUFNLENBQUNDLE9BQVAsR0FBaUI4QyxXQUFqQixDOzs7Ozs7Ozs7OztBQ2JBO0FBQ0EsSUFBSXFDLFNBQVMsR0FBRyxHQUFoQjtBQUFBLElBQ0lDLFFBQVEsR0FBRyxFQURmO0FBR0E7O0FBQ0EsSUFBSUMsU0FBUyxHQUFHQyxJQUFJLENBQUNDLEdBQXJCO0FBRUE7Ozs7Ozs7Ozs7QUFTQSxTQUFTTCxRQUFULENBQWtCNUUsSUFBbEIsRUFBd0I7QUFDdEIsTUFBSWtGLEtBQUssR0FBRyxDQUFaO0FBQUEsTUFDSUMsVUFBVSxHQUFHLENBRGpCO0FBR0EsU0FBTyxZQUFXO0FBQ2hCLFFBQUlDLEtBQUssR0FBR0wsU0FBUyxFQUFyQjtBQUFBLFFBQ0lNLFNBQVMsR0FBR1AsUUFBUSxJQUFJTSxLQUFLLEdBQUdELFVBQVosQ0FEeEI7QUFHQUEsY0FBVSxHQUFHQyxLQUFiOztBQUNBLFFBQUlDLFNBQVMsR0FBRyxDQUFoQixFQUFtQjtBQUNqQixVQUFJLEVBQUVILEtBQUYsSUFBV0wsU0FBZixFQUEwQjtBQUN4QixlQUFPUixTQUFTLENBQUMsQ0FBRCxDQUFoQjtBQUNEO0FBQ0YsS0FKRCxNQUlPO0FBQ0xhLFdBQUssR0FBRyxDQUFSO0FBQ0Q7O0FBQ0QsV0FBT2xGLElBQUksQ0FBQ0QsS0FBTCxDQUFXVyxTQUFYLEVBQXNCMkQsU0FBdEIsQ0FBUDtBQUNELEdBYkQ7QUFjRDs7QUFFRDVFLE1BQU0sQ0FBQ0MsT0FBUCxHQUFpQmtGLFFBQWpCLEM7Ozs7Ozs7Ozs7O0FDcENBO0FBQ0EsSUFBSWxELFNBQVMsR0FBR0MsUUFBUSxDQUFDL0IsU0FBekI7QUFFQTs7QUFDQSxJQUFJaUMsWUFBWSxHQUFHSCxTQUFTLENBQUNJLFFBQTdCO0FBRUE7Ozs7Ozs7O0FBT0EsU0FBU1AsUUFBVCxDQUFrQnZCLElBQWxCLEVBQXdCO0FBQ3RCLE1BQUlBLElBQUksSUFBSSxJQUFaLEVBQWtCO0FBQ2hCLFFBQUk7QUFDRixhQUFPNkIsWUFBWSxDQUFDekIsSUFBYixDQUFrQkosSUFBbEIsQ0FBUDtBQUNELEtBRkQsQ0FFRSxPQUFPaUQsQ0FBUCxFQUFVLENBQUU7O0FBQ2QsUUFBSTtBQUNGLGFBQVFqRCxJQUFJLEdBQUcsRUFBZjtBQUNELEtBRkQsQ0FFRSxPQUFPaUQsQ0FBUCxFQUFVLENBQUU7QUFDZjs7QUFDRCxTQUFPLEVBQVA7QUFDRDs7QUFFRHhELE1BQU0sQ0FBQ0MsT0FBUCxHQUFpQjZCLFFBQWpCLEM7Ozs7Ozs7Ozs7O0FDekJBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBbUJBLFNBQVNvQixRQUFULENBQWtCekIsS0FBbEIsRUFBeUI7QUFDdkIsU0FBTyxZQUFXO0FBQ2hCLFdBQU9BLEtBQVA7QUFDRCxHQUZEO0FBR0Q7O0FBRUR6QixNQUFNLENBQUNDLE9BQVAsR0FBaUJpRCxRQUFqQixDOzs7Ozs7Ozs7OztBQ3pCQSxJQUFJckMsU0FBUyxHQUFHUixtQkFBTyxDQUFDLDZEQUFELENBQXZCO0FBQUEsSUFDSTJDLFFBQVEsR0FBRzNDLG1CQUFPLENBQUMsMkRBQUQsQ0FEdEI7QUFBQSxJQUVJd0YsUUFBUSxHQUFHeEYsbUJBQU8sQ0FBQyx5REFBRCxDQUZ0QjtBQUlBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkEsSUFBSXlGLEtBQUssR0FBRzlDLFFBQVEsQ0FBQyxVQUFTekMsSUFBVCxFQUFlTyxJQUFmLEVBQXFCTCxJQUFyQixFQUEyQjtBQUM5QyxTQUFPSSxTQUFTLENBQUNOLElBQUQsRUFBT3NGLFFBQVEsQ0FBQy9FLElBQUQsQ0FBUixJQUFrQixDQUF6QixFQUE0QkwsSUFBNUIsQ0FBaEI7QUFDRCxDQUZtQixDQUFwQjtBQUlBVCxNQUFNLENBQUNDLE9BQVAsR0FBaUI2RixLQUFqQixDOzs7Ozs7Ozs7OztBQzNCQTs7Ozs7Ozs7Ozs7Ozs7OztBQWdCQSxTQUFTakQsUUFBVCxDQUFrQnBCLEtBQWxCLEVBQXlCO0FBQ3ZCLFNBQU9BLEtBQVA7QUFDRDs7QUFFRHpCLE1BQU0sQ0FBQ0MsT0FBUCxHQUFpQjRDLFFBQWpCLEM7Ozs7Ozs7Ozs7O0FDcEJBLElBQUlyQixVQUFVLEdBQUduQixtQkFBTyxDQUFDLCtEQUFELENBQXhCO0FBQUEsSUFDSXdCLFFBQVEsR0FBR3hCLG1CQUFPLENBQUMseURBQUQsQ0FEdEI7QUFHQTs7O0FBQ0EsSUFBSTBGLFFBQVEsR0FBRyx3QkFBZjtBQUFBLElBQ0lDLE9BQU8sR0FBRyxtQkFEZDtBQUFBLElBRUlDLE1BQU0sR0FBRyw0QkFGYjtBQUFBLElBR0lDLFFBQVEsR0FBRyxnQkFIZjtBQUtBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFpQkEsU0FBU3ZFLFVBQVQsQ0FBb0JGLEtBQXBCLEVBQTJCO0FBQ3pCLE1BQUksQ0FBQ0ksUUFBUSxDQUFDSixLQUFELENBQWIsRUFBc0I7QUFDcEIsV0FBTyxLQUFQO0FBQ0QsR0FId0IsQ0FJekI7QUFDQTs7O0FBQ0EsTUFBSXVDLEdBQUcsR0FBR3hDLFVBQVUsQ0FBQ0MsS0FBRCxDQUFwQjtBQUNBLFNBQU91QyxHQUFHLElBQUlnQyxPQUFQLElBQWtCaEMsR0FBRyxJQUFJaUMsTUFBekIsSUFBbUNqQyxHQUFHLElBQUkrQixRQUExQyxJQUFzRC9CLEdBQUcsSUFBSWtDLFFBQXBFO0FBQ0Q7O0FBRURsRyxNQUFNLENBQUNDLE9BQVAsR0FBaUIwQixVQUFqQixDOzs7Ozs7Ozs7Ozs7O0FDcENBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBeUJBLFNBQVNFLFFBQVQsQ0FBa0JKLEtBQWxCLEVBQXlCO0FBQ3ZCLE1BQUkwRSxJQUFJLFdBQVUxRSxLQUFWLENBQVI7O0FBQ0EsU0FBT0EsS0FBSyxJQUFJLElBQVQsS0FBa0IwRSxJQUFJLElBQUksUUFBUixJQUFvQkEsSUFBSSxJQUFJLFVBQTlDLENBQVA7QUFDRDs7QUFFRG5HLE1BQU0sQ0FBQ0MsT0FBUCxHQUFpQjRCLFFBQWpCLEM7Ozs7Ozs7Ozs7Ozs7QUM5QkE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQXdCQSxTQUFTdUUsWUFBVCxDQUFzQjNFLEtBQXRCLEVBQTZCO0FBQzNCLFNBQU9BLEtBQUssSUFBSSxJQUFULElBQWlCLFFBQU9BLEtBQVAsS0FBZ0IsUUFBeEM7QUFDRDs7QUFFRHpCLE1BQU0sQ0FBQ0MsT0FBUCxHQUFpQm1HLFlBQWpCLEM7Ozs7Ozs7Ozs7Ozs7QUM1QkEsSUFBSTVFLFVBQVUsR0FBR25CLG1CQUFPLENBQUMsK0RBQUQsQ0FBeEI7QUFBQSxJQUNJK0YsWUFBWSxHQUFHL0YsbUJBQU8sQ0FBQyxpRUFBRCxDQUQxQjtBQUdBOzs7QUFDQSxJQUFJZ0csU0FBUyxHQUFHLGlCQUFoQjtBQUVBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFpQkEsU0FBU0MsUUFBVCxDQUFrQjdFLEtBQWxCLEVBQXlCO0FBQ3ZCLFNBQU8sUUFBT0EsS0FBUCxLQUFnQixRQUFoQixJQUNKMkUsWUFBWSxDQUFDM0UsS0FBRCxDQUFaLElBQXVCRCxVQUFVLENBQUNDLEtBQUQsQ0FBVixJQUFxQjRFLFNBRC9DO0FBRUQ7O0FBRURyRyxNQUFNLENBQUNDLE9BQVAsR0FBaUJxRyxRQUFqQixDOzs7Ozs7Ozs7OztBQzVCQSxJQUFJekUsUUFBUSxHQUFHeEIsbUJBQU8sQ0FBQyx5REFBRCxDQUF0QjtBQUFBLElBQ0lpRyxRQUFRLEdBQUdqRyxtQkFBTyxDQUFDLHlEQUFELENBRHRCO0FBR0E7OztBQUNBLElBQUlrRyxHQUFHLEdBQUcsSUFBSSxDQUFkO0FBRUE7O0FBQ0EsSUFBSUMsTUFBTSxHQUFHLFlBQWI7QUFFQTs7QUFDQSxJQUFJQyxVQUFVLEdBQUcsb0JBQWpCO0FBRUE7O0FBQ0EsSUFBSUMsVUFBVSxHQUFHLFlBQWpCO0FBRUE7O0FBQ0EsSUFBSUMsU0FBUyxHQUFHLGFBQWhCO0FBRUE7O0FBQ0EsSUFBSUMsWUFBWSxHQUFHQyxRQUFuQjtBQUVBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUF1QkEsU0FBU2hCLFFBQVQsQ0FBa0JwRSxLQUFsQixFQUF5QjtBQUN2QixNQUFJLE9BQU9BLEtBQVAsSUFBZ0IsUUFBcEIsRUFBOEI7QUFDNUIsV0FBT0EsS0FBUDtBQUNEOztBQUNELE1BQUk2RSxRQUFRLENBQUM3RSxLQUFELENBQVosRUFBcUI7QUFDbkIsV0FBTzhFLEdBQVA7QUFDRDs7QUFDRCxNQUFJMUUsUUFBUSxDQUFDSixLQUFELENBQVosRUFBcUI7QUFDbkIsUUFBSXFGLEtBQUssR0FBRyxPQUFPckYsS0FBSyxDQUFDc0YsT0FBYixJQUF3QixVQUF4QixHQUFxQ3RGLEtBQUssQ0FBQ3NGLE9BQU4sRUFBckMsR0FBdUR0RixLQUFuRTtBQUNBQSxTQUFLLEdBQUdJLFFBQVEsQ0FBQ2lGLEtBQUQsQ0FBUixHQUFtQkEsS0FBSyxHQUFHLEVBQTNCLEdBQWlDQSxLQUF6QztBQUNEOztBQUNELE1BQUksT0FBT3JGLEtBQVAsSUFBZ0IsUUFBcEIsRUFBOEI7QUFDNUIsV0FBT0EsS0FBSyxLQUFLLENBQVYsR0FBY0EsS0FBZCxHQUFzQixDQUFDQSxLQUE5QjtBQUNEOztBQUNEQSxPQUFLLEdBQUdBLEtBQUssQ0FBQ2dCLE9BQU4sQ0FBYytELE1BQWQsRUFBc0IsRUFBdEIsQ0FBUjtBQUNBLE1BQUlRLFFBQVEsR0FBR04sVUFBVSxDQUFDOUQsSUFBWCxDQUFnQm5CLEtBQWhCLENBQWY7QUFDQSxTQUFRdUYsUUFBUSxJQUFJTCxTQUFTLENBQUMvRCxJQUFWLENBQWVuQixLQUFmLENBQWIsR0FDSG1GLFlBQVksQ0FBQ25GLEtBQUssQ0FBQ3dGLEtBQU4sQ0FBWSxDQUFaLENBQUQsRUFBaUJELFFBQVEsR0FBRyxDQUFILEdBQU8sQ0FBaEMsQ0FEVCxHQUVGUCxVQUFVLENBQUM3RCxJQUFYLENBQWdCbkIsS0FBaEIsSUFBeUI4RSxHQUF6QixHQUErQixDQUFDOUUsS0FGckM7QUFHRDs7QUFFRHpCLE1BQU0sQ0FBQ0MsT0FBUCxHQUFpQjRGLFFBQWpCLEM7Ozs7Ozs7Ozs7OztBQ2pFQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxZQUFZO0FBQ1osR0FBRztBQUNIO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7Ozs7QUNWQTtBQUFBO0FBQUE7O0FBRUE7QUFDQTtBQUNBOztBQUVlO0FBQ2Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0EseUJBQXlCO0FBQ3pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7O0FDakdBLElBQUlxQixDQUFKLEMsQ0FFQTs7QUFDQUEsQ0FBQyxHQUFJLFlBQVc7QUFDZixTQUFPLElBQVA7QUFDQSxDQUZHLEVBQUo7O0FBSUEsSUFBSTtBQUNIO0FBQ0FBLEdBQUMsR0FBR0EsQ0FBQyxJQUFJLElBQUloRixRQUFKLENBQWEsYUFBYixHQUFUO0FBQ0EsQ0FIRCxDQUdFLE9BQU9zQixDQUFQLEVBQVU7QUFDWDtBQUNBLE1BQUksUUFBTzJELE1BQVAseUNBQU9BLE1BQVAsT0FBa0IsUUFBdEIsRUFBZ0NELENBQUMsR0FBR0MsTUFBSjtBQUNoQyxDLENBRUQ7QUFDQTtBQUNBOzs7QUFFQW5ILE1BQU0sQ0FBQ0MsT0FBUCxHQUFpQmlILENBQWpCLEM7Ozs7Ozs7Ozs7OztBQ25CQTtBQUFBO0FBQUE7QUFBQTtBQUF1RztBQUNyQztBQUNMOzs7QUFHN0Q7QUFDZ0c7QUFDaEcsZ0JBQWdCLDJHQUFVO0FBQzFCLEVBQUUsb0ZBQU07QUFDUixFQUFFLG1HQUFNO0FBQ1IsRUFBRSw0R0FBZTtBQUNqQjtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBLElBQUksS0FBVSxFQUFFLFlBaUJmO0FBQ0Q7QUFDZSxnRjs7Ozs7Ozs7Ozs7O0FDdENmO0FBQUE7QUFBQSx3Q0FBMEwsQ0FBZ0Isa1BBQUcsRUFBQyxDOzs7Ozs7Ozs7Ozs7QUNBOU07QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBOzs7Ozs7Ozs7Ozs7QUNBQSxnRSIsImZpbGUiOiJTcHJpdGVBbmltYXRvci5qcyIsInNvdXJjZXNDb250ZW50IjpbIihmdW5jdGlvbiB3ZWJwYWNrVW5pdmVyc2FsTW9kdWxlRGVmaW5pdGlvbihyb290LCBmYWN0b3J5KSB7XG5cdGlmKHR5cGVvZiBleHBvcnRzID09PSAnb2JqZWN0JyAmJiB0eXBlb2YgbW9kdWxlID09PSAnb2JqZWN0Jylcblx0XHRtb2R1bGUuZXhwb3J0cyA9IGZhY3RvcnkocmVxdWlyZShcIkBtb25vZ3JpZC9qcy11dGlsc1wiKSk7XG5cdGVsc2UgaWYodHlwZW9mIGRlZmluZSA9PT0gJ2Z1bmN0aW9uJyAmJiBkZWZpbmUuYW1kKVxuXHRcdGRlZmluZShcInZ1ZS1saWJcIiwgW1wiQG1vbm9ncmlkL2pzLXV0aWxzXCJdLCBmYWN0b3J5KTtcblx0ZWxzZSBpZih0eXBlb2YgZXhwb3J0cyA9PT0gJ29iamVjdCcpXG5cdFx0ZXhwb3J0c1tcInZ1ZS1saWJcIl0gPSBmYWN0b3J5KHJlcXVpcmUoXCJAbW9ub2dyaWQvanMtdXRpbHNcIikpO1xuXHRlbHNlXG5cdFx0cm9vdFtcInZ1ZS1saWJcIl0gPSBmYWN0b3J5KHJvb3RbXCJAbW9ub2dyaWQvanMtdXRpbHNcIl0pO1xufSkod2luZG93LCBmdW5jdGlvbihfX1dFQlBBQ0tfRVhURVJOQUxfTU9EVUxFX19tb25vZ3JpZF9qc191dGlsc19fKSB7XG5yZXR1cm4gIiwiIFx0Ly8gVGhlIG1vZHVsZSBjYWNoZVxuIFx0dmFyIGluc3RhbGxlZE1vZHVsZXMgPSB7fTtcblxuIFx0Ly8gVGhlIHJlcXVpcmUgZnVuY3Rpb25cbiBcdGZ1bmN0aW9uIF9fd2VicGFja19yZXF1aXJlX18obW9kdWxlSWQpIHtcblxuIFx0XHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcbiBcdFx0aWYoaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0pIHtcbiBcdFx0XHRyZXR1cm4gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0uZXhwb3J0cztcbiBcdFx0fVxuIFx0XHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuIFx0XHR2YXIgbW9kdWxlID0gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0gPSB7XG4gXHRcdFx0aTogbW9kdWxlSWQsXG4gXHRcdFx0bDogZmFsc2UsXG4gXHRcdFx0ZXhwb3J0czoge31cbiBcdFx0fTtcblxuIFx0XHQvLyBFeGVjdXRlIHRoZSBtb2R1bGUgZnVuY3Rpb25cbiBcdFx0bW9kdWxlc1ttb2R1bGVJZF0uY2FsbChtb2R1bGUuZXhwb3J0cywgbW9kdWxlLCBtb2R1bGUuZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXyk7XG5cbiBcdFx0Ly8gRmxhZyB0aGUgbW9kdWxlIGFzIGxvYWRlZFxuIFx0XHRtb2R1bGUubCA9IHRydWU7XG5cbiBcdFx0Ly8gUmV0dXJuIHRoZSBleHBvcnRzIG9mIHRoZSBtb2R1bGVcbiBcdFx0cmV0dXJuIG1vZHVsZS5leHBvcnRzO1xuIFx0fVxuXG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlcyBvYmplY3QgKF9fd2VicGFja19tb2R1bGVzX18pXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm0gPSBtb2R1bGVzO1xuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZSBjYWNoZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5jID0gaW5zdGFsbGVkTW9kdWxlcztcblxuIFx0Ly8gZGVmaW5lIGdldHRlciBmdW5jdGlvbiBmb3IgaGFybW9ueSBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQgPSBmdW5jdGlvbihleHBvcnRzLCBuYW1lLCBnZXR0ZXIpIHtcbiBcdFx0aWYoIV9fd2VicGFja19yZXF1aXJlX18ubyhleHBvcnRzLCBuYW1lKSkge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBuYW1lLCB7IGVudW1lcmFibGU6IHRydWUsIGdldDogZ2V0dGVyIH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBkZWZpbmUgX19lc01vZHVsZSBvbiBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIgPSBmdW5jdGlvbihleHBvcnRzKSB7XG4gXHRcdGlmKHR5cGVvZiBTeW1ib2wgIT09ICd1bmRlZmluZWQnICYmIFN5bWJvbC50b1N0cmluZ1RhZykge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBTeW1ib2wudG9TdHJpbmdUYWcsIHsgdmFsdWU6ICdNb2R1bGUnIH0pO1xuIFx0XHR9XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnX19lc01vZHVsZScsIHsgdmFsdWU6IHRydWUgfSk7XG4gXHR9O1xuXG4gXHQvLyBjcmVhdGUgYSBmYWtlIG5hbWVzcGFjZSBvYmplY3RcbiBcdC8vIG1vZGUgJiAxOiB2YWx1ZSBpcyBhIG1vZHVsZSBpZCwgcmVxdWlyZSBpdFxuIFx0Ly8gbW9kZSAmIDI6IG1lcmdlIGFsbCBwcm9wZXJ0aWVzIG9mIHZhbHVlIGludG8gdGhlIG5zXG4gXHQvLyBtb2RlICYgNDogcmV0dXJuIHZhbHVlIHdoZW4gYWxyZWFkeSBucyBvYmplY3RcbiBcdC8vIG1vZGUgJiA4fDE6IGJlaGF2ZSBsaWtlIHJlcXVpcmVcbiBcdF9fd2VicGFja19yZXF1aXJlX18udCA9IGZ1bmN0aW9uKHZhbHVlLCBtb2RlKSB7XG4gXHRcdGlmKG1vZGUgJiAxKSB2YWx1ZSA9IF9fd2VicGFja19yZXF1aXJlX18odmFsdWUpO1xuIFx0XHRpZihtb2RlICYgOCkgcmV0dXJuIHZhbHVlO1xuIFx0XHRpZigobW9kZSAmIDQpICYmIHR5cGVvZiB2YWx1ZSA9PT0gJ29iamVjdCcgJiYgdmFsdWUgJiYgdmFsdWUuX19lc01vZHVsZSkgcmV0dXJuIHZhbHVlO1xuIFx0XHR2YXIgbnMgPSBPYmplY3QuY3JlYXRlKG51bGwpO1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIobnMpO1xuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkobnMsICdkZWZhdWx0JywgeyBlbnVtZXJhYmxlOiB0cnVlLCB2YWx1ZTogdmFsdWUgfSk7XG4gXHRcdGlmKG1vZGUgJiAyICYmIHR5cGVvZiB2YWx1ZSAhPSAnc3RyaW5nJykgZm9yKHZhciBrZXkgaW4gdmFsdWUpIF9fd2VicGFja19yZXF1aXJlX18uZChucywga2V5LCBmdW5jdGlvbihrZXkpIHsgcmV0dXJuIHZhbHVlW2tleV07IH0uYmluZChudWxsLCBrZXkpKTtcbiBcdFx0cmV0dXJuIG5zO1xuIFx0fTtcblxuIFx0Ly8gZ2V0RGVmYXVsdEV4cG9ydCBmdW5jdGlvbiBmb3IgY29tcGF0aWJpbGl0eSB3aXRoIG5vbi1oYXJtb255IG1vZHVsZXNcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubiA9IGZ1bmN0aW9uKG1vZHVsZSkge1xuIFx0XHR2YXIgZ2V0dGVyID0gbW9kdWxlICYmIG1vZHVsZS5fX2VzTW9kdWxlID9cbiBcdFx0XHRmdW5jdGlvbiBnZXREZWZhdWx0KCkgeyByZXR1cm4gbW9kdWxlWydkZWZhdWx0J107IH0gOlxuIFx0XHRcdGZ1bmN0aW9uIGdldE1vZHVsZUV4cG9ydHMoKSB7IHJldHVybiBtb2R1bGU7IH07XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18uZChnZXR0ZXIsICdhJywgZ2V0dGVyKTtcbiBcdFx0cmV0dXJuIGdldHRlcjtcbiBcdH07XG5cbiBcdC8vIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5vID0gZnVuY3Rpb24ob2JqZWN0LCBwcm9wZXJ0eSkgeyByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iamVjdCwgcHJvcGVydHkpOyB9O1xuXG4gXHQvLyBfX3dlYnBhY2tfcHVibGljX3BhdGhfX1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5wID0gXCJcIjtcblxuXG4gXHQvLyBMb2FkIGVudHJ5IG1vZHVsZSBhbmQgcmV0dXJuIGV4cG9ydHNcbiBcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKF9fd2VicGFja19yZXF1aXJlX18ucyA9IFwiLi9zcmMvY29tcG9uZW50cy9TcHJpdGVBbmltYXRvci52dWVcIik7XG4iLCJmdW5jdGlvbiBfdHlwZW9mKG9iaikge1xuICBcIkBiYWJlbC9oZWxwZXJzIC0gdHlwZW9mXCI7XG5cbiAgaWYgKHR5cGVvZiBTeW1ib2wgPT09IFwiZnVuY3Rpb25cIiAmJiB0eXBlb2YgU3ltYm9sLml0ZXJhdG9yID09PSBcInN5bWJvbFwiKSB7XG4gICAgbW9kdWxlLmV4cG9ydHMgPSBfdHlwZW9mID0gZnVuY3Rpb24gX3R5cGVvZihvYmopIHtcbiAgICAgIHJldHVybiB0eXBlb2Ygb2JqO1xuICAgIH07XG4gIH0gZWxzZSB7XG4gICAgbW9kdWxlLmV4cG9ydHMgPSBfdHlwZW9mID0gZnVuY3Rpb24gX3R5cGVvZihvYmopIHtcbiAgICAgIHJldHVybiBvYmogJiYgdHlwZW9mIFN5bWJvbCA9PT0gXCJmdW5jdGlvblwiICYmIG9iai5jb25zdHJ1Y3RvciA9PT0gU3ltYm9sICYmIG9iaiAhPT0gU3ltYm9sLnByb3RvdHlwZSA/IFwic3ltYm9sXCIgOiB0eXBlb2Ygb2JqO1xuICAgIH07XG4gIH1cblxuICByZXR1cm4gX3R5cGVvZihvYmopO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IF90eXBlb2Y7IiwiPHRlbXBsYXRlIGxhbmc9XCJodG1sXCI+XG4gIDxjYW52YXNcbiAgICA6aWQ9J2lkJ1xuICAgIDp3aWR0aD1cIndpZHRoXCJcbiAgICA6aGVpZ2h0PVwiaGVpZ2h0XCJcbiAgICByZWY9XCJ2dWUtc3ByaXRlLWNhbnZhc1wiXG4gID48L2NhbnZhcz5cbjwvdGVtcGxhdGU+XG5cbjxzY3JpcHQ+XG5pbXBvcnQgeyBSQUYgfSBmcm9tICdAbW9ub2dyaWQvanMtdXRpbHMnXG5pbXBvcnQgZGVsYXkgZnJvbSAnbG9kYXNoL2RlbGF5J1xuLyoqXG4gKiBJbnN0YW5jZXMgYSBgY2FudmFzYCBlbGVtZW50IHRoYXQgcGxheXMgYSBzcHJpdGVzaGVldFxuICogZnJvbSBUZXh0dXJlUGFja2VyXG4gKlxuICogQHNlZSBbVGV4dHVyZVBhY2tlcl0oaHR0cHM6Ly93d3cuY29kZWFuZHdlYi5jb20vdGV4dHVyZXBhY2tlcilcbiAqL1xuZXhwb3J0IGRlZmF1bHQge1xuICBuYW1lOiAnU3ByaXRlQW5pbWF0b3InLFxuXG4gIHByb3BzOiB7XG4gICAgLyoqXG4gICAgICogUE5HIGltYWdlIHBhdGggdG8gc3ByaXRlc2hlZXQsIG11c3QgYmUgYSB2YWxpZCBwYXRoIG9yIGJsb2Igc3RyaW5nLlxuICAgICAqL1xuICAgIHNwcml0ZXNoZWV0OiB7XG4gICAgICByZXF1aXJlZDogdHJ1ZSxcbiAgICAgIHR5cGU6IFN0cmluZyxcbiAgICAgIGRlZmF1bHQ6ICcnXG4gICAgfSxcbiAgICAvKipcbiAgICAgKiBUaGUgVGV4dHVyZVBhY2tlciBqc29uIGZpbGUsIHBhcnNlZFxuICAgICAqL1xuICAgIGpzb246IHtcbiAgICAgIHJlcXVpcmVkOiB0cnVlLFxuICAgICAgdHlwZTogT2JqZWN0LFxuICAgICAgZGVmYXVsdCAoKSB7XG4gICAgICAgIHJldHVybiB7fVxuICAgICAgfVxuICAgIH0sXG4gICAgLyoqXG4gICAgICogRnJhbWVzIHBlciBzZWNvbmQgZm9yIHBsYXliYWNrXG4gICAgICovXG4gICAgZnBzOiB7XG4gICAgICB0eXBlOiBOdW1iZXIsXG4gICAgICBkZWZhdWx0OiAzMFxuICAgIH0sXG4gICAgLyoqXG4gICAgICogU2lnbmFscyB0aGUgc3ByaXRlc2hlZXQgdG8gc3RhcnQgcGxheWluZyBhcyBzb29uIGFzIGl0cyByZWFkeS5cbiAgICAgKi9cbiAgICBhdXRvcGxheToge1xuICAgICAgdHlwZTogQm9vbGVhbixcbiAgICAgIGRlZmF1bHQ6IHRydWVcbiAgICB9LFxuICAgIC8qKlxuICAgICAqIEFkZHMgYW4gYXJ0aWZpY2lhbCBkZWxheSB0byB0aGUgYXV0b3BsYXkgZmVhdHVyZSwgaW4gbWlsbGlzZWNvbmRzLlxuICAgICAqL1xuICAgIGF1dG9wbGF5RGVsYXk6IHtcbiAgICAgIHR5cGU6IE51bWJlcixcbiAgICAgIGRlZmF1bHQgKCkge1xuICAgICAgICByZXR1cm4gMFxuICAgICAgfVxuICAgIH0sXG4gICAgLyoqXG4gICAgICogUGxheXMgdGhlIHNwcml0ZXNoZWV0IGluIHJldmVyc2Ugb25jZSB0aGUgZW5kIGlzIHJlYWNoZWQsXG4gICAgICogc3RhcnRzIHBsYXlpbmcgZm9yd2FyZCBhZ2FpbiBhZnRlciB0aGF0IGlzIGRvbmUuXG4gICAgICpcbiAgICAgKiBNdXN0IGJlIHVzZWQgaW4gY29uanVjdGlvbiB3aXRoIGBsb29wYC5cbiAgICAgKi9cbiAgICB5b3lvOiB7XG4gICAgICB0eXBlOiBCb29sZWFuLFxuICAgICAgZGVmYXVsdDogZmFsc2VcbiAgICB9LFxuICAgIC8qKlxuICAgICAqIExvb3BzIHRoZSBzcHJpdGVzaGVldCBwbGF5YmFjay5cbiAgICAgKi9cbiAgICBsb29wOiB7XG4gICAgICB0eXBlOiBCb29sZWFuLFxuICAgICAgZGVmYXVsdDogZmFsc2VcbiAgICB9LFxuICAgIC8qKlxuICAgICAqIEN1c3RvbSBJRCBmb3IgdGhlIHNwcml0ZXNoZWV0IHJvb3QgZWxlbWVudC5cbiAgICAgKi9cbiAgICBpZDoge1xuICAgICAgdHlwZTogU3RyaW5nLFxuICAgICAgZGVmYXVsdDogJ3Z1ZS1zcHJpdGUnXG4gICAgfVxuICB9LFxuXG4gIHdhdGNoOiB7XG4gICAgLyoqXG4gICAgICogcmVzZXRzIHlveW9kaXJlY3Rpb24gdG8gZm9yd2FyZCB3aGVuIHlveW8gaXMgZGlzYWJsZWRcbiAgICAgKi9cbiAgICB5b3lvICh2YWx1ZSkge1xuICAgICAgaWYgKCF2YWx1ZSkge1xuICAgICAgICB0aGlzLnlveW9kaXJlY3Rpb24gPSAwXG4gICAgICB9XG4gICAgfVxuICB9LFxuXG4gIGRhdGEgKCkge1xuICAgIHJldHVybiB7XG4gICAgICBmcmFtZXM6IFtdLFxuICAgICAgbGVuZ3RoOiAwLFxuICAgICAgZnJhbWVJbmRleDogMCxcbiAgICAgIGN1cnJlbnRJbmRleDogMCxcbiAgICAgIHlveW9kaXJlY3Rpb246IDAsXG4gICAgICBzcHJpdGU6IG51bGwsXG4gICAgICBoZWlnaHQ6IDAsXG4gICAgICB3aWR0aDogMCxcbiAgICAgIG5vdzogMCxcbiAgICAgIHRoZW46IDBcbiAgICB9XG4gIH0sXG4gIG1vdW50ZWQgKCkge1xuICAgIHRoaXMuanNvbi5mcmFtZXMuZm9yRWFjaCgoZnJhbWUpID0+IHtcbiAgICAgIHRoaXMuZnJhbWVzLnB1c2goe1xuICAgICAgICBuYW1lOiBmcmFtZS5maWxlbmFtZSxcbiAgICAgICAgeDogZnJhbWUuZnJhbWUueCxcbiAgICAgICAgeTogZnJhbWUuZnJhbWUueSxcbiAgICAgICAgdzogZnJhbWUuZnJhbWUudyxcbiAgICAgICAgaDogZnJhbWUuZnJhbWUuaFxuICAgICAgfSlcbiAgICB9KVxuICAgIC8vIHRoaXMuZnJhbWVzLnNvcnQoKGEsIGIpID0+IGEuZmlsZW5hbWUgPCBiLmZpbGVuYW1lKVxuICAgIHRoaXMud2lkdGggPSB0aGlzLmZyYW1lc1swXS53XG4gICAgdGhpcy5oZWlnaHQgPSB0aGlzLmZyYW1lc1swXS5oXG4gICAgdGhpcy5sZW5ndGggPSAodGhpcy5mcmFtZXMubGVuZ3RoIC0gMSlcbiAgfSxcbiAgY3JlYXRlZCAoKSB7XG4gICAgdGhpcy4kbmV4dFRpY2soKCkgPT4ge1xuICAgICAgdGhpcy5zcHJpdGUgPSBuZXcgSW1hZ2UoKVxuICAgICAgdGhpcy5zcHJpdGUuc3JjID0gdGhpcy5zcHJpdGVzaGVldFxuICAgICAgdGhpcy5zcHJpdGUub25sb2FkID0gKHsgdGFyZ2V0IH0pID0+IHsgdGhpcy5pbml0KHRhcmdldCkgfVxuICAgIH0pXG4gIH0sXG4gIG1ldGhvZHM6IHtcbiAgICBpbml0IChpbWcpIHtcbiAgICAgIHRoaXMuY3R4ID0gdGhpcy4kcmVmc1sndnVlLXNwcml0ZS1jYW52YXMnXS5nZXRDb250ZXh0KCcyZCcpXG4gICAgICAvKipcbiAgICAgICAqIEVtaXR0ZWQgd2hlbiB0aGUgc3ByaXRlc2hlZXQgaXMgcmVhZHkgdG8gYmUgcGxheWVkXG4gICAgICAgKiAoaW1hZ2UgaGFzIGJlZW4gbG9hZGVkKVxuICAgICAgICovXG4gICAgICB0aGlzLiRlbWl0KCdpbml0JylcbiAgICAgIGlmICh0aGlzLmF1dG9wbGF5KSB7XG4gICAgICAgIHRoaXMucmVuZGVyKClcbiAgICAgICAgZGVsYXkodGhpcy5wbGF5LCB0aGlzLmF1dG9wbGF5RGVsYXkpXG4gICAgICB9XG4gICAgfSxcbiAgICAvKipcbiAgICAgKiBEcmF3cyB0aGUgaW1hZ2UgaW50byB0aGUgY2FudmFzXG4gICAgICovXG4gICAgcmVuZGVyICgpIHtcbiAgICAgIHRoaXMuY3R4ICYmIHRoaXMuY3R4LmNsZWFyUmVjdCgwLCAwLCB0aGlzLndpZHRoLCB0aGlzLmhlaWdodClcbiAgICAgIGlmICh0aGlzLnlveW8gJiYgdGhpcy5jdXJyZW50SW5kZXggJSB0aGlzLmxlbmd0aCA9PT0gMCAmJiB0aGlzLmN1cnJlbnRJbmRleCkge1xuICAgICAgICB0aGlzLnlveW9kaXJlY3Rpb24gPSBOdW1iZXIoIXRoaXMueW95b2RpcmVjdGlvbilcbiAgICAgIH1cbiAgICAgIGNvbnN0IGluZGV4ID0gTWF0aC5hYnMoKHRoaXMuY3VycmVudEluZGV4ICUgdGhpcy5sZW5ndGgpIC0gKHRoaXMubGVuZ3RoICogdGhpcy55b3lvZGlyZWN0aW9uKSlcbiAgICAgIGNvbnN0IHggPSB0aGlzLmZyYW1lc1tpbmRleF0ueFxuICAgICAgY29uc3QgeSA9IHRoaXMuZnJhbWVzW2luZGV4XS55XG4gICAgICB0aGlzLmN0eCAmJiB0aGlzLmN0eC5kcmF3SW1hZ2UodGhpcy5zcHJpdGUsIHgsIHksIHRoaXMud2lkdGgsIHRoaXMuaGVpZ2h0LCAwLCAwLCB0aGlzLndpZHRoLCB0aGlzLmhlaWdodClcbiAgICB9LFxuICAgIC8qKlxuICAgICAqIEhhbmRsZXMgdGhlIGRyYXcgbG9vcC5cbiAgICAgKi9cbiAgICBkb2xvb3AgKCkge1xuICAgICAgdGhpcy5ub3cgPSBEYXRlLm5vdygpXG4gICAgICBjb25zdCBkZWx0YSA9ICh0aGlzLm5vdyAtIHRoaXMudGhlbilcbiAgICAgIGlmIChkZWx0YSA+ICgxMDAwIC8gdGhpcy5mcHMpKSB7XG4gICAgICAgIHRoaXMudGhlbiA9IHRoaXMubm93IC0gKGRlbHRhICUgKDEwMDAgLyB0aGlzLmZwcykpXG4gICAgICAgIHRoaXMucmVuZGVyKClcbiAgICAgICAgdGhpcy5jdXJyZW50SW5kZXgrK1xuICAgICAgICAvLyBjb25zb2xlLmxvZyh0aGlzLmN1cnJlbnRJbmRleClcbiAgICAgIH1cblxuICAgICAgaWYgKCF0aGlzLmxvb3AgJiYgdGhpcy5jdXJyZW50SW5kZXggPj0gdGhpcy5sZW5ndGgpIHtcbiAgICAgICAgUkFGLnJlbW92ZSh0aGlzLmRvbG9vcClcbiAgICAgICAgdGhpcy5jdXJyZW50SW5kZXggPSAwXG4gICAgICAgIC8qKlxuICAgICAgICAgKiBFbWl0dGVkIHdoZW4gdGhlIHNwcml0ZXNoZWV0IGVuZHMsXG4gICAgICAgICAqIG5ldmVyIGVtaXR0ZWQgd2hpbGUgbG9vcGluZy5cbiAgICAgICAgICovXG4gICAgICAgIHRoaXMuJGVtaXQoJ2VuZGVkJylcbiAgICAgIH1cbiAgICB9LFxuICAgIC8qKlxuICAgICAqIFN0b3BzIHRoZSBwbGF5YmFja1xuICAgICAqL1xuICAgIHN0b3AgKCkge1xuICAgICAgUkFGLnJlbW92ZSh0aGlzLmRvbG9vcClcbiAgICAgIHRoaXMuY3VycmVudEluZGV4ID0gMFxuICAgICAgLyoqXG4gICAgICAgKiBFbWl0dGVkIHdoZW4gdGhlIHNwcml0ZXNoZWV0IGlzXG4gICAgICAgKiBleHBsaWNpdGx5IHRvbGQgdG8gc3RvcCBwbGF5aW5nXG4gICAgICAgKi9cbiAgICAgIHRoaXMuJGVtaXQoJ3N0b3AnKVxuICAgIH0sXG4gICAgLyoqXG4gICAgICogUmVzdW1lcyB0aGUgcGxheWJhY2sgZnJvbSBvcHRpb25hbCBmcmFtZSBudW1iZXIgYGZyb21gXG4gICAgICovXG4gICAgcGxheSAoZnJvbSkge1xuICAgICAgdGhpcy50aGVuID0gRGF0ZS5ub3coKVxuICAgICAgdGhpcy5jdXJyZW50SW5kZXggPSBOdW1iZXIuaXNOYU4oTnVtYmVyKGZyb20pKSA/IHRoaXMuY3VycmVudEluZGV4IDogZnJvbVxuICAgICAgUkFGLmFkZCh0aGlzLmRvbG9vcClcbiAgICAgIC8qKlxuICAgICAgICogRW1pdHRlZCB3aGVuIHRoZSBzcHJpdGVzaGVldCBiZWdpbnMgdG8gcGxheVxuICAgICAgICovXG4gICAgICB0aGlzLiRlbWl0KCdwbGF5JylcbiAgICB9XG4gIH1cbn1cbjwvc2NyaXB0PlxuIiwidmFyIHJvb3QgPSByZXF1aXJlKCcuL19yb290Jyk7XG5cbi8qKiBCdWlsdC1pbiB2YWx1ZSByZWZlcmVuY2VzLiAqL1xudmFyIFN5bWJvbCA9IHJvb3QuU3ltYm9sO1xuXG5tb2R1bGUuZXhwb3J0cyA9IFN5bWJvbDtcbiIsIi8qKlxuICogQSBmYXN0ZXIgYWx0ZXJuYXRpdmUgdG8gYEZ1bmN0aW9uI2FwcGx5YCwgdGhpcyBmdW5jdGlvbiBpbnZva2VzIGBmdW5jYFxuICogd2l0aCB0aGUgYHRoaXNgIGJpbmRpbmcgb2YgYHRoaXNBcmdgIGFuZCB0aGUgYXJndW1lbnRzIG9mIGBhcmdzYC5cbiAqXG4gKiBAcHJpdmF0ZVxuICogQHBhcmFtIHtGdW5jdGlvbn0gZnVuYyBUaGUgZnVuY3Rpb24gdG8gaW52b2tlLlxuICogQHBhcmFtIHsqfSB0aGlzQXJnIFRoZSBgdGhpc2AgYmluZGluZyBvZiBgZnVuY2AuXG4gKiBAcGFyYW0ge0FycmF5fSBhcmdzIFRoZSBhcmd1bWVudHMgdG8gaW52b2tlIGBmdW5jYCB3aXRoLlxuICogQHJldHVybnMgeyp9IFJldHVybnMgdGhlIHJlc3VsdCBvZiBgZnVuY2AuXG4gKi9cbmZ1bmN0aW9uIGFwcGx5KGZ1bmMsIHRoaXNBcmcsIGFyZ3MpIHtcbiAgc3dpdGNoIChhcmdzLmxlbmd0aCkge1xuICAgIGNhc2UgMDogcmV0dXJuIGZ1bmMuY2FsbCh0aGlzQXJnKTtcbiAgICBjYXNlIDE6IHJldHVybiBmdW5jLmNhbGwodGhpc0FyZywgYXJnc1swXSk7XG4gICAgY2FzZSAyOiByZXR1cm4gZnVuYy5jYWxsKHRoaXNBcmcsIGFyZ3NbMF0sIGFyZ3NbMV0pO1xuICAgIGNhc2UgMzogcmV0dXJuIGZ1bmMuY2FsbCh0aGlzQXJnLCBhcmdzWzBdLCBhcmdzWzFdLCBhcmdzWzJdKTtcbiAgfVxuICByZXR1cm4gZnVuYy5hcHBseSh0aGlzQXJnLCBhcmdzKTtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBhcHBseTtcbiIsIi8qKiBFcnJvciBtZXNzYWdlIGNvbnN0YW50cy4gKi9cbnZhciBGVU5DX0VSUk9SX1RFWFQgPSAnRXhwZWN0ZWQgYSBmdW5jdGlvbic7XG5cbi8qKlxuICogVGhlIGJhc2UgaW1wbGVtZW50YXRpb24gb2YgYF8uZGVsYXlgIGFuZCBgXy5kZWZlcmAgd2hpY2ggYWNjZXB0cyBgYXJnc2BcbiAqIHRvIHByb3ZpZGUgdG8gYGZ1bmNgLlxuICpcbiAqIEBwcml2YXRlXG4gKiBAcGFyYW0ge0Z1bmN0aW9ufSBmdW5jIFRoZSBmdW5jdGlvbiB0byBkZWxheS5cbiAqIEBwYXJhbSB7bnVtYmVyfSB3YWl0IFRoZSBudW1iZXIgb2YgbWlsbGlzZWNvbmRzIHRvIGRlbGF5IGludm9jYXRpb24uXG4gKiBAcGFyYW0ge0FycmF5fSBhcmdzIFRoZSBhcmd1bWVudHMgdG8gcHJvdmlkZSB0byBgZnVuY2AuXG4gKiBAcmV0dXJucyB7bnVtYmVyfE9iamVjdH0gUmV0dXJucyB0aGUgdGltZXIgaWQgb3IgdGltZW91dCBvYmplY3QuXG4gKi9cbmZ1bmN0aW9uIGJhc2VEZWxheShmdW5jLCB3YWl0LCBhcmdzKSB7XG4gIGlmICh0eXBlb2YgZnVuYyAhPSAnZnVuY3Rpb24nKSB7XG4gICAgdGhyb3cgbmV3IFR5cGVFcnJvcihGVU5DX0VSUk9SX1RFWFQpO1xuICB9XG4gIHJldHVybiBzZXRUaW1lb3V0KGZ1bmN0aW9uKCkgeyBmdW5jLmFwcGx5KHVuZGVmaW5lZCwgYXJncyk7IH0sIHdhaXQpO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IGJhc2VEZWxheTtcbiIsInZhciBTeW1ib2wgPSByZXF1aXJlKCcuL19TeW1ib2wnKSxcbiAgICBnZXRSYXdUYWcgPSByZXF1aXJlKCcuL19nZXRSYXdUYWcnKSxcbiAgICBvYmplY3RUb1N0cmluZyA9IHJlcXVpcmUoJy4vX29iamVjdFRvU3RyaW5nJyk7XG5cbi8qKiBgT2JqZWN0I3RvU3RyaW5nYCByZXN1bHQgcmVmZXJlbmNlcy4gKi9cbnZhciBudWxsVGFnID0gJ1tvYmplY3QgTnVsbF0nLFxuICAgIHVuZGVmaW5lZFRhZyA9ICdbb2JqZWN0IFVuZGVmaW5lZF0nO1xuXG4vKiogQnVpbHQtaW4gdmFsdWUgcmVmZXJlbmNlcy4gKi9cbnZhciBzeW1Ub1N0cmluZ1RhZyA9IFN5bWJvbCA/IFN5bWJvbC50b1N0cmluZ1RhZyA6IHVuZGVmaW5lZDtcblxuLyoqXG4gKiBUaGUgYmFzZSBpbXBsZW1lbnRhdGlvbiBvZiBgZ2V0VGFnYCB3aXRob3V0IGZhbGxiYWNrcyBmb3IgYnVnZ3kgZW52aXJvbm1lbnRzLlxuICpcbiAqIEBwcml2YXRlXG4gKiBAcGFyYW0geyp9IHZhbHVlIFRoZSB2YWx1ZSB0byBxdWVyeS5cbiAqIEByZXR1cm5zIHtzdHJpbmd9IFJldHVybnMgdGhlIGB0b1N0cmluZ1RhZ2AuXG4gKi9cbmZ1bmN0aW9uIGJhc2VHZXRUYWcodmFsdWUpIHtcbiAgaWYgKHZhbHVlID09IG51bGwpIHtcbiAgICByZXR1cm4gdmFsdWUgPT09IHVuZGVmaW5lZCA/IHVuZGVmaW5lZFRhZyA6IG51bGxUYWc7XG4gIH1cbiAgcmV0dXJuIChzeW1Ub1N0cmluZ1RhZyAmJiBzeW1Ub1N0cmluZ1RhZyBpbiBPYmplY3QodmFsdWUpKVxuICAgID8gZ2V0UmF3VGFnKHZhbHVlKVxuICAgIDogb2JqZWN0VG9TdHJpbmcodmFsdWUpO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IGJhc2VHZXRUYWc7XG4iLCJ2YXIgaXNGdW5jdGlvbiA9IHJlcXVpcmUoJy4vaXNGdW5jdGlvbicpLFxuICAgIGlzTWFza2VkID0gcmVxdWlyZSgnLi9faXNNYXNrZWQnKSxcbiAgICBpc09iamVjdCA9IHJlcXVpcmUoJy4vaXNPYmplY3QnKSxcbiAgICB0b1NvdXJjZSA9IHJlcXVpcmUoJy4vX3RvU291cmNlJyk7XG5cbi8qKlxuICogVXNlZCB0byBtYXRjaCBgUmVnRXhwYFxuICogW3N5bnRheCBjaGFyYWN0ZXJzXShodHRwOi8vZWNtYS1pbnRlcm5hdGlvbmFsLm9yZy9lY21hLTI2Mi83LjAvI3NlYy1wYXR0ZXJucykuXG4gKi9cbnZhciByZVJlZ0V4cENoYXIgPSAvW1xcXFxeJC4qKz8oKVtcXF17fXxdL2c7XG5cbi8qKiBVc2VkIHRvIGRldGVjdCBob3N0IGNvbnN0cnVjdG9ycyAoU2FmYXJpKS4gKi9cbnZhciByZUlzSG9zdEN0b3IgPSAvXlxcW29iamVjdCAuKz9Db25zdHJ1Y3RvclxcXSQvO1xuXG4vKiogVXNlZCBmb3IgYnVpbHQtaW4gbWV0aG9kIHJlZmVyZW5jZXMuICovXG52YXIgZnVuY1Byb3RvID0gRnVuY3Rpb24ucHJvdG90eXBlLFxuICAgIG9iamVjdFByb3RvID0gT2JqZWN0LnByb3RvdHlwZTtcblxuLyoqIFVzZWQgdG8gcmVzb2x2ZSB0aGUgZGVjb21waWxlZCBzb3VyY2Ugb2YgZnVuY3Rpb25zLiAqL1xudmFyIGZ1bmNUb1N0cmluZyA9IGZ1bmNQcm90by50b1N0cmluZztcblxuLyoqIFVzZWQgdG8gY2hlY2sgb2JqZWN0cyBmb3Igb3duIHByb3BlcnRpZXMuICovXG52YXIgaGFzT3duUHJvcGVydHkgPSBvYmplY3RQcm90by5oYXNPd25Qcm9wZXJ0eTtcblxuLyoqIFVzZWQgdG8gZGV0ZWN0IGlmIGEgbWV0aG9kIGlzIG5hdGl2ZS4gKi9cbnZhciByZUlzTmF0aXZlID0gUmVnRXhwKCdeJyArXG4gIGZ1bmNUb1N0cmluZy5jYWxsKGhhc093blByb3BlcnR5KS5yZXBsYWNlKHJlUmVnRXhwQ2hhciwgJ1xcXFwkJicpXG4gIC5yZXBsYWNlKC9oYXNPd25Qcm9wZXJ0eXwoZnVuY3Rpb24pLio/KD89XFxcXFxcKCl8IGZvciAuKz8oPz1cXFxcXFxdKS9nLCAnJDEuKj8nKSArICckJ1xuKTtcblxuLyoqXG4gKiBUaGUgYmFzZSBpbXBsZW1lbnRhdGlvbiBvZiBgXy5pc05hdGl2ZWAgd2l0aG91dCBiYWQgc2hpbSBjaGVja3MuXG4gKlxuICogQHByaXZhdGVcbiAqIEBwYXJhbSB7Kn0gdmFsdWUgVGhlIHZhbHVlIHRvIGNoZWNrLlxuICogQHJldHVybnMge2Jvb2xlYW59IFJldHVybnMgYHRydWVgIGlmIGB2YWx1ZWAgaXMgYSBuYXRpdmUgZnVuY3Rpb24sXG4gKiAgZWxzZSBgZmFsc2VgLlxuICovXG5mdW5jdGlvbiBiYXNlSXNOYXRpdmUodmFsdWUpIHtcbiAgaWYgKCFpc09iamVjdCh2YWx1ZSkgfHwgaXNNYXNrZWQodmFsdWUpKSB7XG4gICAgcmV0dXJuIGZhbHNlO1xuICB9XG4gIHZhciBwYXR0ZXJuID0gaXNGdW5jdGlvbih2YWx1ZSkgPyByZUlzTmF0aXZlIDogcmVJc0hvc3RDdG9yO1xuICByZXR1cm4gcGF0dGVybi50ZXN0KHRvU291cmNlKHZhbHVlKSk7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gYmFzZUlzTmF0aXZlO1xuIiwidmFyIGlkZW50aXR5ID0gcmVxdWlyZSgnLi9pZGVudGl0eScpLFxuICAgIG92ZXJSZXN0ID0gcmVxdWlyZSgnLi9fb3ZlclJlc3QnKSxcbiAgICBzZXRUb1N0cmluZyA9IHJlcXVpcmUoJy4vX3NldFRvU3RyaW5nJyk7XG5cbi8qKlxuICogVGhlIGJhc2UgaW1wbGVtZW50YXRpb24gb2YgYF8ucmVzdGAgd2hpY2ggZG9lc24ndCB2YWxpZGF0ZSBvciBjb2VyY2UgYXJndW1lbnRzLlxuICpcbiAqIEBwcml2YXRlXG4gKiBAcGFyYW0ge0Z1bmN0aW9ufSBmdW5jIFRoZSBmdW5jdGlvbiB0byBhcHBseSBhIHJlc3QgcGFyYW1ldGVyIHRvLlxuICogQHBhcmFtIHtudW1iZXJ9IFtzdGFydD1mdW5jLmxlbmd0aC0xXSBUaGUgc3RhcnQgcG9zaXRpb24gb2YgdGhlIHJlc3QgcGFyYW1ldGVyLlxuICogQHJldHVybnMge0Z1bmN0aW9ufSBSZXR1cm5zIHRoZSBuZXcgZnVuY3Rpb24uXG4gKi9cbmZ1bmN0aW9uIGJhc2VSZXN0KGZ1bmMsIHN0YXJ0KSB7XG4gIHJldHVybiBzZXRUb1N0cmluZyhvdmVyUmVzdChmdW5jLCBzdGFydCwgaWRlbnRpdHkpLCBmdW5jICsgJycpO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IGJhc2VSZXN0O1xuIiwidmFyIGNvbnN0YW50ID0gcmVxdWlyZSgnLi9jb25zdGFudCcpLFxuICAgIGRlZmluZVByb3BlcnR5ID0gcmVxdWlyZSgnLi9fZGVmaW5lUHJvcGVydHknKSxcbiAgICBpZGVudGl0eSA9IHJlcXVpcmUoJy4vaWRlbnRpdHknKTtcblxuLyoqXG4gKiBUaGUgYmFzZSBpbXBsZW1lbnRhdGlvbiBvZiBgc2V0VG9TdHJpbmdgIHdpdGhvdXQgc3VwcG9ydCBmb3IgaG90IGxvb3Agc2hvcnRpbmcuXG4gKlxuICogQHByaXZhdGVcbiAqIEBwYXJhbSB7RnVuY3Rpb259IGZ1bmMgVGhlIGZ1bmN0aW9uIHRvIG1vZGlmeS5cbiAqIEBwYXJhbSB7RnVuY3Rpb259IHN0cmluZyBUaGUgYHRvU3RyaW5nYCByZXN1bHQuXG4gKiBAcmV0dXJucyB7RnVuY3Rpb259IFJldHVybnMgYGZ1bmNgLlxuICovXG52YXIgYmFzZVNldFRvU3RyaW5nID0gIWRlZmluZVByb3BlcnR5ID8gaWRlbnRpdHkgOiBmdW5jdGlvbihmdW5jLCBzdHJpbmcpIHtcbiAgcmV0dXJuIGRlZmluZVByb3BlcnR5KGZ1bmMsICd0b1N0cmluZycsIHtcbiAgICAnY29uZmlndXJhYmxlJzogdHJ1ZSxcbiAgICAnZW51bWVyYWJsZSc6IGZhbHNlLFxuICAgICd2YWx1ZSc6IGNvbnN0YW50KHN0cmluZyksXG4gICAgJ3dyaXRhYmxlJzogdHJ1ZVxuICB9KTtcbn07XG5cbm1vZHVsZS5leHBvcnRzID0gYmFzZVNldFRvU3RyaW5nO1xuIiwidmFyIHJvb3QgPSByZXF1aXJlKCcuL19yb290Jyk7XG5cbi8qKiBVc2VkIHRvIGRldGVjdCBvdmVycmVhY2hpbmcgY29yZS1qcyBzaGltcy4gKi9cbnZhciBjb3JlSnNEYXRhID0gcm9vdFsnX19jb3JlLWpzX3NoYXJlZF9fJ107XG5cbm1vZHVsZS5leHBvcnRzID0gY29yZUpzRGF0YTtcbiIsInZhciBnZXROYXRpdmUgPSByZXF1aXJlKCcuL19nZXROYXRpdmUnKTtcblxudmFyIGRlZmluZVByb3BlcnR5ID0gKGZ1bmN0aW9uKCkge1xuICB0cnkge1xuICAgIHZhciBmdW5jID0gZ2V0TmF0aXZlKE9iamVjdCwgJ2RlZmluZVByb3BlcnR5Jyk7XG4gICAgZnVuYyh7fSwgJycsIHt9KTtcbiAgICByZXR1cm4gZnVuYztcbiAgfSBjYXRjaCAoZSkge31cbn0oKSk7XG5cbm1vZHVsZS5leHBvcnRzID0gZGVmaW5lUHJvcGVydHk7XG4iLCIvKiogRGV0ZWN0IGZyZWUgdmFyaWFibGUgYGdsb2JhbGAgZnJvbSBOb2RlLmpzLiAqL1xudmFyIGZyZWVHbG9iYWwgPSB0eXBlb2YgZ2xvYmFsID09ICdvYmplY3QnICYmIGdsb2JhbCAmJiBnbG9iYWwuT2JqZWN0ID09PSBPYmplY3QgJiYgZ2xvYmFsO1xuXG5tb2R1bGUuZXhwb3J0cyA9IGZyZWVHbG9iYWw7XG4iLCJ2YXIgYmFzZUlzTmF0aXZlID0gcmVxdWlyZSgnLi9fYmFzZUlzTmF0aXZlJyksXG4gICAgZ2V0VmFsdWUgPSByZXF1aXJlKCcuL19nZXRWYWx1ZScpO1xuXG4vKipcbiAqIEdldHMgdGhlIG5hdGl2ZSBmdW5jdGlvbiBhdCBga2V5YCBvZiBgb2JqZWN0YC5cbiAqXG4gKiBAcHJpdmF0ZVxuICogQHBhcmFtIHtPYmplY3R9IG9iamVjdCBUaGUgb2JqZWN0IHRvIHF1ZXJ5LlxuICogQHBhcmFtIHtzdHJpbmd9IGtleSBUaGUga2V5IG9mIHRoZSBtZXRob2QgdG8gZ2V0LlxuICogQHJldHVybnMgeyp9IFJldHVybnMgdGhlIGZ1bmN0aW9uIGlmIGl0J3MgbmF0aXZlLCBlbHNlIGB1bmRlZmluZWRgLlxuICovXG5mdW5jdGlvbiBnZXROYXRpdmUob2JqZWN0LCBrZXkpIHtcbiAgdmFyIHZhbHVlID0gZ2V0VmFsdWUob2JqZWN0LCBrZXkpO1xuICByZXR1cm4gYmFzZUlzTmF0aXZlKHZhbHVlKSA/IHZhbHVlIDogdW5kZWZpbmVkO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IGdldE5hdGl2ZTtcbiIsInZhciBTeW1ib2wgPSByZXF1aXJlKCcuL19TeW1ib2wnKTtcblxuLyoqIFVzZWQgZm9yIGJ1aWx0LWluIG1ldGhvZCByZWZlcmVuY2VzLiAqL1xudmFyIG9iamVjdFByb3RvID0gT2JqZWN0LnByb3RvdHlwZTtcblxuLyoqIFVzZWQgdG8gY2hlY2sgb2JqZWN0cyBmb3Igb3duIHByb3BlcnRpZXMuICovXG52YXIgaGFzT3duUHJvcGVydHkgPSBvYmplY3RQcm90by5oYXNPd25Qcm9wZXJ0eTtcblxuLyoqXG4gKiBVc2VkIHRvIHJlc29sdmUgdGhlXG4gKiBbYHRvU3RyaW5nVGFnYF0oaHR0cDovL2VjbWEtaW50ZXJuYXRpb25hbC5vcmcvZWNtYS0yNjIvNy4wLyNzZWMtb2JqZWN0LnByb3RvdHlwZS50b3N0cmluZylcbiAqIG9mIHZhbHVlcy5cbiAqL1xudmFyIG5hdGl2ZU9iamVjdFRvU3RyaW5nID0gb2JqZWN0UHJvdG8udG9TdHJpbmc7XG5cbi8qKiBCdWlsdC1pbiB2YWx1ZSByZWZlcmVuY2VzLiAqL1xudmFyIHN5bVRvU3RyaW5nVGFnID0gU3ltYm9sID8gU3ltYm9sLnRvU3RyaW5nVGFnIDogdW5kZWZpbmVkO1xuXG4vKipcbiAqIEEgc3BlY2lhbGl6ZWQgdmVyc2lvbiBvZiBgYmFzZUdldFRhZ2Agd2hpY2ggaWdub3JlcyBgU3ltYm9sLnRvU3RyaW5nVGFnYCB2YWx1ZXMuXG4gKlxuICogQHByaXZhdGVcbiAqIEBwYXJhbSB7Kn0gdmFsdWUgVGhlIHZhbHVlIHRvIHF1ZXJ5LlxuICogQHJldHVybnMge3N0cmluZ30gUmV0dXJucyB0aGUgcmF3IGB0b1N0cmluZ1RhZ2AuXG4gKi9cbmZ1bmN0aW9uIGdldFJhd1RhZyh2YWx1ZSkge1xuICB2YXIgaXNPd24gPSBoYXNPd25Qcm9wZXJ0eS5jYWxsKHZhbHVlLCBzeW1Ub1N0cmluZ1RhZyksXG4gICAgICB0YWcgPSB2YWx1ZVtzeW1Ub1N0cmluZ1RhZ107XG5cbiAgdHJ5IHtcbiAgICB2YWx1ZVtzeW1Ub1N0cmluZ1RhZ10gPSB1bmRlZmluZWQ7XG4gICAgdmFyIHVubWFza2VkID0gdHJ1ZTtcbiAgfSBjYXRjaCAoZSkge31cblxuICB2YXIgcmVzdWx0ID0gbmF0aXZlT2JqZWN0VG9TdHJpbmcuY2FsbCh2YWx1ZSk7XG4gIGlmICh1bm1hc2tlZCkge1xuICAgIGlmIChpc093bikge1xuICAgICAgdmFsdWVbc3ltVG9TdHJpbmdUYWddID0gdGFnO1xuICAgIH0gZWxzZSB7XG4gICAgICBkZWxldGUgdmFsdWVbc3ltVG9TdHJpbmdUYWddO1xuICAgIH1cbiAgfVxuICByZXR1cm4gcmVzdWx0O1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IGdldFJhd1RhZztcbiIsIi8qKlxuICogR2V0cyB0aGUgdmFsdWUgYXQgYGtleWAgb2YgYG9iamVjdGAuXG4gKlxuICogQHByaXZhdGVcbiAqIEBwYXJhbSB7T2JqZWN0fSBbb2JqZWN0XSBUaGUgb2JqZWN0IHRvIHF1ZXJ5LlxuICogQHBhcmFtIHtzdHJpbmd9IGtleSBUaGUga2V5IG9mIHRoZSBwcm9wZXJ0eSB0byBnZXQuXG4gKiBAcmV0dXJucyB7Kn0gUmV0dXJucyB0aGUgcHJvcGVydHkgdmFsdWUuXG4gKi9cbmZ1bmN0aW9uIGdldFZhbHVlKG9iamVjdCwga2V5KSB7XG4gIHJldHVybiBvYmplY3QgPT0gbnVsbCA/IHVuZGVmaW5lZCA6IG9iamVjdFtrZXldO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IGdldFZhbHVlO1xuIiwidmFyIGNvcmVKc0RhdGEgPSByZXF1aXJlKCcuL19jb3JlSnNEYXRhJyk7XG5cbi8qKiBVc2VkIHRvIGRldGVjdCBtZXRob2RzIG1hc3F1ZXJhZGluZyBhcyBuYXRpdmUuICovXG52YXIgbWFza1NyY0tleSA9IChmdW5jdGlvbigpIHtcbiAgdmFyIHVpZCA9IC9bXi5dKyQvLmV4ZWMoY29yZUpzRGF0YSAmJiBjb3JlSnNEYXRhLmtleXMgJiYgY29yZUpzRGF0YS5rZXlzLklFX1BST1RPIHx8ICcnKTtcbiAgcmV0dXJuIHVpZCA/ICgnU3ltYm9sKHNyYylfMS4nICsgdWlkKSA6ICcnO1xufSgpKTtcblxuLyoqXG4gKiBDaGVja3MgaWYgYGZ1bmNgIGhhcyBpdHMgc291cmNlIG1hc2tlZC5cbiAqXG4gKiBAcHJpdmF0ZVxuICogQHBhcmFtIHtGdW5jdGlvbn0gZnVuYyBUaGUgZnVuY3Rpb24gdG8gY2hlY2suXG4gKiBAcmV0dXJucyB7Ym9vbGVhbn0gUmV0dXJucyBgdHJ1ZWAgaWYgYGZ1bmNgIGlzIG1hc2tlZCwgZWxzZSBgZmFsc2VgLlxuICovXG5mdW5jdGlvbiBpc01hc2tlZChmdW5jKSB7XG4gIHJldHVybiAhIW1hc2tTcmNLZXkgJiYgKG1hc2tTcmNLZXkgaW4gZnVuYyk7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gaXNNYXNrZWQ7XG4iLCIvKiogVXNlZCBmb3IgYnVpbHQtaW4gbWV0aG9kIHJlZmVyZW5jZXMuICovXG52YXIgb2JqZWN0UHJvdG8gPSBPYmplY3QucHJvdG90eXBlO1xuXG4vKipcbiAqIFVzZWQgdG8gcmVzb2x2ZSB0aGVcbiAqIFtgdG9TdHJpbmdUYWdgXShodHRwOi8vZWNtYS1pbnRlcm5hdGlvbmFsLm9yZy9lY21hLTI2Mi83LjAvI3NlYy1vYmplY3QucHJvdG90eXBlLnRvc3RyaW5nKVxuICogb2YgdmFsdWVzLlxuICovXG52YXIgbmF0aXZlT2JqZWN0VG9TdHJpbmcgPSBvYmplY3RQcm90by50b1N0cmluZztcblxuLyoqXG4gKiBDb252ZXJ0cyBgdmFsdWVgIHRvIGEgc3RyaW5nIHVzaW5nIGBPYmplY3QucHJvdG90eXBlLnRvU3RyaW5nYC5cbiAqXG4gKiBAcHJpdmF0ZVxuICogQHBhcmFtIHsqfSB2YWx1ZSBUaGUgdmFsdWUgdG8gY29udmVydC5cbiAqIEByZXR1cm5zIHtzdHJpbmd9IFJldHVybnMgdGhlIGNvbnZlcnRlZCBzdHJpbmcuXG4gKi9cbmZ1bmN0aW9uIG9iamVjdFRvU3RyaW5nKHZhbHVlKSB7XG4gIHJldHVybiBuYXRpdmVPYmplY3RUb1N0cmluZy5jYWxsKHZhbHVlKTtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBvYmplY3RUb1N0cmluZztcbiIsInZhciBhcHBseSA9IHJlcXVpcmUoJy4vX2FwcGx5Jyk7XG5cbi8qIEJ1aWx0LWluIG1ldGhvZCByZWZlcmVuY2VzIGZvciB0aG9zZSB3aXRoIHRoZSBzYW1lIG5hbWUgYXMgb3RoZXIgYGxvZGFzaGAgbWV0aG9kcy4gKi9cbnZhciBuYXRpdmVNYXggPSBNYXRoLm1heDtcblxuLyoqXG4gKiBBIHNwZWNpYWxpemVkIHZlcnNpb24gb2YgYGJhc2VSZXN0YCB3aGljaCB0cmFuc2Zvcm1zIHRoZSByZXN0IGFycmF5LlxuICpcbiAqIEBwcml2YXRlXG4gKiBAcGFyYW0ge0Z1bmN0aW9ufSBmdW5jIFRoZSBmdW5jdGlvbiB0byBhcHBseSBhIHJlc3QgcGFyYW1ldGVyIHRvLlxuICogQHBhcmFtIHtudW1iZXJ9IFtzdGFydD1mdW5jLmxlbmd0aC0xXSBUaGUgc3RhcnQgcG9zaXRpb24gb2YgdGhlIHJlc3QgcGFyYW1ldGVyLlxuICogQHBhcmFtIHtGdW5jdGlvbn0gdHJhbnNmb3JtIFRoZSByZXN0IGFycmF5IHRyYW5zZm9ybS5cbiAqIEByZXR1cm5zIHtGdW5jdGlvbn0gUmV0dXJucyB0aGUgbmV3IGZ1bmN0aW9uLlxuICovXG5mdW5jdGlvbiBvdmVyUmVzdChmdW5jLCBzdGFydCwgdHJhbnNmb3JtKSB7XG4gIHN0YXJ0ID0gbmF0aXZlTWF4KHN0YXJ0ID09PSB1bmRlZmluZWQgPyAoZnVuYy5sZW5ndGggLSAxKSA6IHN0YXJ0LCAwKTtcbiAgcmV0dXJuIGZ1bmN0aW9uKCkge1xuICAgIHZhciBhcmdzID0gYXJndW1lbnRzLFxuICAgICAgICBpbmRleCA9IC0xLFxuICAgICAgICBsZW5ndGggPSBuYXRpdmVNYXgoYXJncy5sZW5ndGggLSBzdGFydCwgMCksXG4gICAgICAgIGFycmF5ID0gQXJyYXkobGVuZ3RoKTtcblxuICAgIHdoaWxlICgrK2luZGV4IDwgbGVuZ3RoKSB7XG4gICAgICBhcnJheVtpbmRleF0gPSBhcmdzW3N0YXJ0ICsgaW5kZXhdO1xuICAgIH1cbiAgICBpbmRleCA9IC0xO1xuICAgIHZhciBvdGhlckFyZ3MgPSBBcnJheShzdGFydCArIDEpO1xuICAgIHdoaWxlICgrK2luZGV4IDwgc3RhcnQpIHtcbiAgICAgIG90aGVyQXJnc1tpbmRleF0gPSBhcmdzW2luZGV4XTtcbiAgICB9XG4gICAgb3RoZXJBcmdzW3N0YXJ0XSA9IHRyYW5zZm9ybShhcnJheSk7XG4gICAgcmV0dXJuIGFwcGx5KGZ1bmMsIHRoaXMsIG90aGVyQXJncyk7XG4gIH07XG59XG5cbm1vZHVsZS5leHBvcnRzID0gb3ZlclJlc3Q7XG4iLCJ2YXIgZnJlZUdsb2JhbCA9IHJlcXVpcmUoJy4vX2ZyZWVHbG9iYWwnKTtcblxuLyoqIERldGVjdCBmcmVlIHZhcmlhYmxlIGBzZWxmYC4gKi9cbnZhciBmcmVlU2VsZiA9IHR5cGVvZiBzZWxmID09ICdvYmplY3QnICYmIHNlbGYgJiYgc2VsZi5PYmplY3QgPT09IE9iamVjdCAmJiBzZWxmO1xuXG4vKiogVXNlZCBhcyBhIHJlZmVyZW5jZSB0byB0aGUgZ2xvYmFsIG9iamVjdC4gKi9cbnZhciByb290ID0gZnJlZUdsb2JhbCB8fCBmcmVlU2VsZiB8fCBGdW5jdGlvbigncmV0dXJuIHRoaXMnKSgpO1xuXG5tb2R1bGUuZXhwb3J0cyA9IHJvb3Q7XG4iLCJ2YXIgYmFzZVNldFRvU3RyaW5nID0gcmVxdWlyZSgnLi9fYmFzZVNldFRvU3RyaW5nJyksXG4gICAgc2hvcnRPdXQgPSByZXF1aXJlKCcuL19zaG9ydE91dCcpO1xuXG4vKipcbiAqIFNldHMgdGhlIGB0b1N0cmluZ2AgbWV0aG9kIG9mIGBmdW5jYCB0byByZXR1cm4gYHN0cmluZ2AuXG4gKlxuICogQHByaXZhdGVcbiAqIEBwYXJhbSB7RnVuY3Rpb259IGZ1bmMgVGhlIGZ1bmN0aW9uIHRvIG1vZGlmeS5cbiAqIEBwYXJhbSB7RnVuY3Rpb259IHN0cmluZyBUaGUgYHRvU3RyaW5nYCByZXN1bHQuXG4gKiBAcmV0dXJucyB7RnVuY3Rpb259IFJldHVybnMgYGZ1bmNgLlxuICovXG52YXIgc2V0VG9TdHJpbmcgPSBzaG9ydE91dChiYXNlU2V0VG9TdHJpbmcpO1xuXG5tb2R1bGUuZXhwb3J0cyA9IHNldFRvU3RyaW5nO1xuIiwiLyoqIFVzZWQgdG8gZGV0ZWN0IGhvdCBmdW5jdGlvbnMgYnkgbnVtYmVyIG9mIGNhbGxzIHdpdGhpbiBhIHNwYW4gb2YgbWlsbGlzZWNvbmRzLiAqL1xudmFyIEhPVF9DT1VOVCA9IDgwMCxcbiAgICBIT1RfU1BBTiA9IDE2O1xuXG4vKiBCdWlsdC1pbiBtZXRob2QgcmVmZXJlbmNlcyBmb3IgdGhvc2Ugd2l0aCB0aGUgc2FtZSBuYW1lIGFzIG90aGVyIGBsb2Rhc2hgIG1ldGhvZHMuICovXG52YXIgbmF0aXZlTm93ID0gRGF0ZS5ub3c7XG5cbi8qKlxuICogQ3JlYXRlcyBhIGZ1bmN0aW9uIHRoYXQnbGwgc2hvcnQgb3V0IGFuZCBpbnZva2UgYGlkZW50aXR5YCBpbnN0ZWFkXG4gKiBvZiBgZnVuY2Agd2hlbiBpdCdzIGNhbGxlZCBgSE9UX0NPVU5UYCBvciBtb3JlIHRpbWVzIGluIGBIT1RfU1BBTmBcbiAqIG1pbGxpc2Vjb25kcy5cbiAqXG4gKiBAcHJpdmF0ZVxuICogQHBhcmFtIHtGdW5jdGlvbn0gZnVuYyBUaGUgZnVuY3Rpb24gdG8gcmVzdHJpY3QuXG4gKiBAcmV0dXJucyB7RnVuY3Rpb259IFJldHVybnMgdGhlIG5ldyBzaG9ydGFibGUgZnVuY3Rpb24uXG4gKi9cbmZ1bmN0aW9uIHNob3J0T3V0KGZ1bmMpIHtcbiAgdmFyIGNvdW50ID0gMCxcbiAgICAgIGxhc3RDYWxsZWQgPSAwO1xuXG4gIHJldHVybiBmdW5jdGlvbigpIHtcbiAgICB2YXIgc3RhbXAgPSBuYXRpdmVOb3coKSxcbiAgICAgICAgcmVtYWluaW5nID0gSE9UX1NQQU4gLSAoc3RhbXAgLSBsYXN0Q2FsbGVkKTtcblxuICAgIGxhc3RDYWxsZWQgPSBzdGFtcDtcbiAgICBpZiAocmVtYWluaW5nID4gMCkge1xuICAgICAgaWYgKCsrY291bnQgPj0gSE9UX0NPVU5UKSB7XG4gICAgICAgIHJldHVybiBhcmd1bWVudHNbMF07XG4gICAgICB9XG4gICAgfSBlbHNlIHtcbiAgICAgIGNvdW50ID0gMDtcbiAgICB9XG4gICAgcmV0dXJuIGZ1bmMuYXBwbHkodW5kZWZpbmVkLCBhcmd1bWVudHMpO1xuICB9O1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IHNob3J0T3V0O1xuIiwiLyoqIFVzZWQgZm9yIGJ1aWx0LWluIG1ldGhvZCByZWZlcmVuY2VzLiAqL1xudmFyIGZ1bmNQcm90byA9IEZ1bmN0aW9uLnByb3RvdHlwZTtcblxuLyoqIFVzZWQgdG8gcmVzb2x2ZSB0aGUgZGVjb21waWxlZCBzb3VyY2Ugb2YgZnVuY3Rpb25zLiAqL1xudmFyIGZ1bmNUb1N0cmluZyA9IGZ1bmNQcm90by50b1N0cmluZztcblxuLyoqXG4gKiBDb252ZXJ0cyBgZnVuY2AgdG8gaXRzIHNvdXJjZSBjb2RlLlxuICpcbiAqIEBwcml2YXRlXG4gKiBAcGFyYW0ge0Z1bmN0aW9ufSBmdW5jIFRoZSBmdW5jdGlvbiB0byBjb252ZXJ0LlxuICogQHJldHVybnMge3N0cmluZ30gUmV0dXJucyB0aGUgc291cmNlIGNvZGUuXG4gKi9cbmZ1bmN0aW9uIHRvU291cmNlKGZ1bmMpIHtcbiAgaWYgKGZ1bmMgIT0gbnVsbCkge1xuICAgIHRyeSB7XG4gICAgICByZXR1cm4gZnVuY1RvU3RyaW5nLmNhbGwoZnVuYyk7XG4gICAgfSBjYXRjaCAoZSkge31cbiAgICB0cnkge1xuICAgICAgcmV0dXJuIChmdW5jICsgJycpO1xuICAgIH0gY2F0Y2ggKGUpIHt9XG4gIH1cbiAgcmV0dXJuICcnO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IHRvU291cmNlO1xuIiwiLyoqXG4gKiBDcmVhdGVzIGEgZnVuY3Rpb24gdGhhdCByZXR1cm5zIGB2YWx1ZWAuXG4gKlxuICogQHN0YXRpY1xuICogQG1lbWJlck9mIF9cbiAqIEBzaW5jZSAyLjQuMFxuICogQGNhdGVnb3J5IFV0aWxcbiAqIEBwYXJhbSB7Kn0gdmFsdWUgVGhlIHZhbHVlIHRvIHJldHVybiBmcm9tIHRoZSBuZXcgZnVuY3Rpb24uXG4gKiBAcmV0dXJucyB7RnVuY3Rpb259IFJldHVybnMgdGhlIG5ldyBjb25zdGFudCBmdW5jdGlvbi5cbiAqIEBleGFtcGxlXG4gKlxuICogdmFyIG9iamVjdHMgPSBfLnRpbWVzKDIsIF8uY29uc3RhbnQoeyAnYSc6IDEgfSkpO1xuICpcbiAqIGNvbnNvbGUubG9nKG9iamVjdHMpO1xuICogLy8gPT4gW3sgJ2EnOiAxIH0sIHsgJ2EnOiAxIH1dXG4gKlxuICogY29uc29sZS5sb2cob2JqZWN0c1swXSA9PT0gb2JqZWN0c1sxXSk7XG4gKiAvLyA9PiB0cnVlXG4gKi9cbmZ1bmN0aW9uIGNvbnN0YW50KHZhbHVlKSB7XG4gIHJldHVybiBmdW5jdGlvbigpIHtcbiAgICByZXR1cm4gdmFsdWU7XG4gIH07XG59XG5cbm1vZHVsZS5leHBvcnRzID0gY29uc3RhbnQ7XG4iLCJ2YXIgYmFzZURlbGF5ID0gcmVxdWlyZSgnLi9fYmFzZURlbGF5JyksXG4gICAgYmFzZVJlc3QgPSByZXF1aXJlKCcuL19iYXNlUmVzdCcpLFxuICAgIHRvTnVtYmVyID0gcmVxdWlyZSgnLi90b051bWJlcicpO1xuXG4vKipcbiAqIEludm9rZXMgYGZ1bmNgIGFmdGVyIGB3YWl0YCBtaWxsaXNlY29uZHMuIEFueSBhZGRpdGlvbmFsIGFyZ3VtZW50cyBhcmVcbiAqIHByb3ZpZGVkIHRvIGBmdW5jYCB3aGVuIGl0J3MgaW52b2tlZC5cbiAqXG4gKiBAc3RhdGljXG4gKiBAbWVtYmVyT2YgX1xuICogQHNpbmNlIDAuMS4wXG4gKiBAY2F0ZWdvcnkgRnVuY3Rpb25cbiAqIEBwYXJhbSB7RnVuY3Rpb259IGZ1bmMgVGhlIGZ1bmN0aW9uIHRvIGRlbGF5LlxuICogQHBhcmFtIHtudW1iZXJ9IHdhaXQgVGhlIG51bWJlciBvZiBtaWxsaXNlY29uZHMgdG8gZGVsYXkgaW52b2NhdGlvbi5cbiAqIEBwYXJhbSB7Li4uKn0gW2FyZ3NdIFRoZSBhcmd1bWVudHMgdG8gaW52b2tlIGBmdW5jYCB3aXRoLlxuICogQHJldHVybnMge251bWJlcn0gUmV0dXJucyB0aGUgdGltZXIgaWQuXG4gKiBAZXhhbXBsZVxuICpcbiAqIF8uZGVsYXkoZnVuY3Rpb24odGV4dCkge1xuICogICBjb25zb2xlLmxvZyh0ZXh0KTtcbiAqIH0sIDEwMDAsICdsYXRlcicpO1xuICogLy8gPT4gTG9ncyAnbGF0ZXInIGFmdGVyIG9uZSBzZWNvbmQuXG4gKi9cbnZhciBkZWxheSA9IGJhc2VSZXN0KGZ1bmN0aW9uKGZ1bmMsIHdhaXQsIGFyZ3MpIHtcbiAgcmV0dXJuIGJhc2VEZWxheShmdW5jLCB0b051bWJlcih3YWl0KSB8fCAwLCBhcmdzKTtcbn0pO1xuXG5tb2R1bGUuZXhwb3J0cyA9IGRlbGF5O1xuIiwiLyoqXG4gKiBUaGlzIG1ldGhvZCByZXR1cm5zIHRoZSBmaXJzdCBhcmd1bWVudCBpdCByZWNlaXZlcy5cbiAqXG4gKiBAc3RhdGljXG4gKiBAc2luY2UgMC4xLjBcbiAqIEBtZW1iZXJPZiBfXG4gKiBAY2F0ZWdvcnkgVXRpbFxuICogQHBhcmFtIHsqfSB2YWx1ZSBBbnkgdmFsdWUuXG4gKiBAcmV0dXJucyB7Kn0gUmV0dXJucyBgdmFsdWVgLlxuICogQGV4YW1wbGVcbiAqXG4gKiB2YXIgb2JqZWN0ID0geyAnYSc6IDEgfTtcbiAqXG4gKiBjb25zb2xlLmxvZyhfLmlkZW50aXR5KG9iamVjdCkgPT09IG9iamVjdCk7XG4gKiAvLyA9PiB0cnVlXG4gKi9cbmZ1bmN0aW9uIGlkZW50aXR5KHZhbHVlKSB7XG4gIHJldHVybiB2YWx1ZTtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBpZGVudGl0eTtcbiIsInZhciBiYXNlR2V0VGFnID0gcmVxdWlyZSgnLi9fYmFzZUdldFRhZycpLFxuICAgIGlzT2JqZWN0ID0gcmVxdWlyZSgnLi9pc09iamVjdCcpO1xuXG4vKiogYE9iamVjdCN0b1N0cmluZ2AgcmVzdWx0IHJlZmVyZW5jZXMuICovXG52YXIgYXN5bmNUYWcgPSAnW29iamVjdCBBc3luY0Z1bmN0aW9uXScsXG4gICAgZnVuY1RhZyA9ICdbb2JqZWN0IEZ1bmN0aW9uXScsXG4gICAgZ2VuVGFnID0gJ1tvYmplY3QgR2VuZXJhdG9yRnVuY3Rpb25dJyxcbiAgICBwcm94eVRhZyA9ICdbb2JqZWN0IFByb3h5XSc7XG5cbi8qKlxuICogQ2hlY2tzIGlmIGB2YWx1ZWAgaXMgY2xhc3NpZmllZCBhcyBhIGBGdW5jdGlvbmAgb2JqZWN0LlxuICpcbiAqIEBzdGF0aWNcbiAqIEBtZW1iZXJPZiBfXG4gKiBAc2luY2UgMC4xLjBcbiAqIEBjYXRlZ29yeSBMYW5nXG4gKiBAcGFyYW0geyp9IHZhbHVlIFRoZSB2YWx1ZSB0byBjaGVjay5cbiAqIEByZXR1cm5zIHtib29sZWFufSBSZXR1cm5zIGB0cnVlYCBpZiBgdmFsdWVgIGlzIGEgZnVuY3Rpb24sIGVsc2UgYGZhbHNlYC5cbiAqIEBleGFtcGxlXG4gKlxuICogXy5pc0Z1bmN0aW9uKF8pO1xuICogLy8gPT4gdHJ1ZVxuICpcbiAqIF8uaXNGdW5jdGlvbigvYWJjLyk7XG4gKiAvLyA9PiBmYWxzZVxuICovXG5mdW5jdGlvbiBpc0Z1bmN0aW9uKHZhbHVlKSB7XG4gIGlmICghaXNPYmplY3QodmFsdWUpKSB7XG4gICAgcmV0dXJuIGZhbHNlO1xuICB9XG4gIC8vIFRoZSB1c2Ugb2YgYE9iamVjdCN0b1N0cmluZ2AgYXZvaWRzIGlzc3VlcyB3aXRoIHRoZSBgdHlwZW9mYCBvcGVyYXRvclxuICAvLyBpbiBTYWZhcmkgOSB3aGljaCByZXR1cm5zICdvYmplY3QnIGZvciB0eXBlZCBhcnJheXMgYW5kIG90aGVyIGNvbnN0cnVjdG9ycy5cbiAgdmFyIHRhZyA9IGJhc2VHZXRUYWcodmFsdWUpO1xuICByZXR1cm4gdGFnID09IGZ1bmNUYWcgfHwgdGFnID09IGdlblRhZyB8fCB0YWcgPT0gYXN5bmNUYWcgfHwgdGFnID09IHByb3h5VGFnO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IGlzRnVuY3Rpb247XG4iLCIvKipcbiAqIENoZWNrcyBpZiBgdmFsdWVgIGlzIHRoZVxuICogW2xhbmd1YWdlIHR5cGVdKGh0dHA6Ly93d3cuZWNtYS1pbnRlcm5hdGlvbmFsLm9yZy9lY21hLTI2Mi83LjAvI3NlYy1lY21hc2NyaXB0LWxhbmd1YWdlLXR5cGVzKVxuICogb2YgYE9iamVjdGAuIChlLmcuIGFycmF5cywgZnVuY3Rpb25zLCBvYmplY3RzLCByZWdleGVzLCBgbmV3IE51bWJlcigwKWAsIGFuZCBgbmV3IFN0cmluZygnJylgKVxuICpcbiAqIEBzdGF0aWNcbiAqIEBtZW1iZXJPZiBfXG4gKiBAc2luY2UgMC4xLjBcbiAqIEBjYXRlZ29yeSBMYW5nXG4gKiBAcGFyYW0geyp9IHZhbHVlIFRoZSB2YWx1ZSB0byBjaGVjay5cbiAqIEByZXR1cm5zIHtib29sZWFufSBSZXR1cm5zIGB0cnVlYCBpZiBgdmFsdWVgIGlzIGFuIG9iamVjdCwgZWxzZSBgZmFsc2VgLlxuICogQGV4YW1wbGVcbiAqXG4gKiBfLmlzT2JqZWN0KHt9KTtcbiAqIC8vID0+IHRydWVcbiAqXG4gKiBfLmlzT2JqZWN0KFsxLCAyLCAzXSk7XG4gKiAvLyA9PiB0cnVlXG4gKlxuICogXy5pc09iamVjdChfLm5vb3ApO1xuICogLy8gPT4gdHJ1ZVxuICpcbiAqIF8uaXNPYmplY3QobnVsbCk7XG4gKiAvLyA9PiBmYWxzZVxuICovXG5mdW5jdGlvbiBpc09iamVjdCh2YWx1ZSkge1xuICB2YXIgdHlwZSA9IHR5cGVvZiB2YWx1ZTtcbiAgcmV0dXJuIHZhbHVlICE9IG51bGwgJiYgKHR5cGUgPT0gJ29iamVjdCcgfHwgdHlwZSA9PSAnZnVuY3Rpb24nKTtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBpc09iamVjdDtcbiIsIi8qKlxuICogQ2hlY2tzIGlmIGB2YWx1ZWAgaXMgb2JqZWN0LWxpa2UuIEEgdmFsdWUgaXMgb2JqZWN0LWxpa2UgaWYgaXQncyBub3QgYG51bGxgXG4gKiBhbmQgaGFzIGEgYHR5cGVvZmAgcmVzdWx0IG9mIFwib2JqZWN0XCIuXG4gKlxuICogQHN0YXRpY1xuICogQG1lbWJlck9mIF9cbiAqIEBzaW5jZSA0LjAuMFxuICogQGNhdGVnb3J5IExhbmdcbiAqIEBwYXJhbSB7Kn0gdmFsdWUgVGhlIHZhbHVlIHRvIGNoZWNrLlxuICogQHJldHVybnMge2Jvb2xlYW59IFJldHVybnMgYHRydWVgIGlmIGB2YWx1ZWAgaXMgb2JqZWN0LWxpa2UsIGVsc2UgYGZhbHNlYC5cbiAqIEBleGFtcGxlXG4gKlxuICogXy5pc09iamVjdExpa2Uoe30pO1xuICogLy8gPT4gdHJ1ZVxuICpcbiAqIF8uaXNPYmplY3RMaWtlKFsxLCAyLCAzXSk7XG4gKiAvLyA9PiB0cnVlXG4gKlxuICogXy5pc09iamVjdExpa2UoXy5ub29wKTtcbiAqIC8vID0+IGZhbHNlXG4gKlxuICogXy5pc09iamVjdExpa2UobnVsbCk7XG4gKiAvLyA9PiBmYWxzZVxuICovXG5mdW5jdGlvbiBpc09iamVjdExpa2UodmFsdWUpIHtcbiAgcmV0dXJuIHZhbHVlICE9IG51bGwgJiYgdHlwZW9mIHZhbHVlID09ICdvYmplY3QnO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IGlzT2JqZWN0TGlrZTtcbiIsInZhciBiYXNlR2V0VGFnID0gcmVxdWlyZSgnLi9fYmFzZUdldFRhZycpLFxuICAgIGlzT2JqZWN0TGlrZSA9IHJlcXVpcmUoJy4vaXNPYmplY3RMaWtlJyk7XG5cbi8qKiBgT2JqZWN0I3RvU3RyaW5nYCByZXN1bHQgcmVmZXJlbmNlcy4gKi9cbnZhciBzeW1ib2xUYWcgPSAnW29iamVjdCBTeW1ib2xdJztcblxuLyoqXG4gKiBDaGVja3MgaWYgYHZhbHVlYCBpcyBjbGFzc2lmaWVkIGFzIGEgYFN5bWJvbGAgcHJpbWl0aXZlIG9yIG9iamVjdC5cbiAqXG4gKiBAc3RhdGljXG4gKiBAbWVtYmVyT2YgX1xuICogQHNpbmNlIDQuMC4wXG4gKiBAY2F0ZWdvcnkgTGFuZ1xuICogQHBhcmFtIHsqfSB2YWx1ZSBUaGUgdmFsdWUgdG8gY2hlY2suXG4gKiBAcmV0dXJucyB7Ym9vbGVhbn0gUmV0dXJucyBgdHJ1ZWAgaWYgYHZhbHVlYCBpcyBhIHN5bWJvbCwgZWxzZSBgZmFsc2VgLlxuICogQGV4YW1wbGVcbiAqXG4gKiBfLmlzU3ltYm9sKFN5bWJvbC5pdGVyYXRvcik7XG4gKiAvLyA9PiB0cnVlXG4gKlxuICogXy5pc1N5bWJvbCgnYWJjJyk7XG4gKiAvLyA9PiBmYWxzZVxuICovXG5mdW5jdGlvbiBpc1N5bWJvbCh2YWx1ZSkge1xuICByZXR1cm4gdHlwZW9mIHZhbHVlID09ICdzeW1ib2wnIHx8XG4gICAgKGlzT2JqZWN0TGlrZSh2YWx1ZSkgJiYgYmFzZUdldFRhZyh2YWx1ZSkgPT0gc3ltYm9sVGFnKTtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBpc1N5bWJvbDtcbiIsInZhciBpc09iamVjdCA9IHJlcXVpcmUoJy4vaXNPYmplY3QnKSxcbiAgICBpc1N5bWJvbCA9IHJlcXVpcmUoJy4vaXNTeW1ib2wnKTtcblxuLyoqIFVzZWQgYXMgcmVmZXJlbmNlcyBmb3IgdmFyaW91cyBgTnVtYmVyYCBjb25zdGFudHMuICovXG52YXIgTkFOID0gMCAvIDA7XG5cbi8qKiBVc2VkIHRvIG1hdGNoIGxlYWRpbmcgYW5kIHRyYWlsaW5nIHdoaXRlc3BhY2UuICovXG52YXIgcmVUcmltID0gL15cXHMrfFxccyskL2c7XG5cbi8qKiBVc2VkIHRvIGRldGVjdCBiYWQgc2lnbmVkIGhleGFkZWNpbWFsIHN0cmluZyB2YWx1ZXMuICovXG52YXIgcmVJc0JhZEhleCA9IC9eWy0rXTB4WzAtOWEtZl0rJC9pO1xuXG4vKiogVXNlZCB0byBkZXRlY3QgYmluYXJ5IHN0cmluZyB2YWx1ZXMuICovXG52YXIgcmVJc0JpbmFyeSA9IC9eMGJbMDFdKyQvaTtcblxuLyoqIFVzZWQgdG8gZGV0ZWN0IG9jdGFsIHN0cmluZyB2YWx1ZXMuICovXG52YXIgcmVJc09jdGFsID0gL14wb1swLTddKyQvaTtcblxuLyoqIEJ1aWx0LWluIG1ldGhvZCByZWZlcmVuY2VzIHdpdGhvdXQgYSBkZXBlbmRlbmN5IG9uIGByb290YC4gKi9cbnZhciBmcmVlUGFyc2VJbnQgPSBwYXJzZUludDtcblxuLyoqXG4gKiBDb252ZXJ0cyBgdmFsdWVgIHRvIGEgbnVtYmVyLlxuICpcbiAqIEBzdGF0aWNcbiAqIEBtZW1iZXJPZiBfXG4gKiBAc2luY2UgNC4wLjBcbiAqIEBjYXRlZ29yeSBMYW5nXG4gKiBAcGFyYW0geyp9IHZhbHVlIFRoZSB2YWx1ZSB0byBwcm9jZXNzLlxuICogQHJldHVybnMge251bWJlcn0gUmV0dXJucyB0aGUgbnVtYmVyLlxuICogQGV4YW1wbGVcbiAqXG4gKiBfLnRvTnVtYmVyKDMuMik7XG4gKiAvLyA9PiAzLjJcbiAqXG4gKiBfLnRvTnVtYmVyKE51bWJlci5NSU5fVkFMVUUpO1xuICogLy8gPT4gNWUtMzI0XG4gKlxuICogXy50b051bWJlcihJbmZpbml0eSk7XG4gKiAvLyA9PiBJbmZpbml0eVxuICpcbiAqIF8udG9OdW1iZXIoJzMuMicpO1xuICogLy8gPT4gMy4yXG4gKi9cbmZ1bmN0aW9uIHRvTnVtYmVyKHZhbHVlKSB7XG4gIGlmICh0eXBlb2YgdmFsdWUgPT0gJ251bWJlcicpIHtcbiAgICByZXR1cm4gdmFsdWU7XG4gIH1cbiAgaWYgKGlzU3ltYm9sKHZhbHVlKSkge1xuICAgIHJldHVybiBOQU47XG4gIH1cbiAgaWYgKGlzT2JqZWN0KHZhbHVlKSkge1xuICAgIHZhciBvdGhlciA9IHR5cGVvZiB2YWx1ZS52YWx1ZU9mID09ICdmdW5jdGlvbicgPyB2YWx1ZS52YWx1ZU9mKCkgOiB2YWx1ZTtcbiAgICB2YWx1ZSA9IGlzT2JqZWN0KG90aGVyKSA/IChvdGhlciArICcnKSA6IG90aGVyO1xuICB9XG4gIGlmICh0eXBlb2YgdmFsdWUgIT0gJ3N0cmluZycpIHtcbiAgICByZXR1cm4gdmFsdWUgPT09IDAgPyB2YWx1ZSA6ICt2YWx1ZTtcbiAgfVxuICB2YWx1ZSA9IHZhbHVlLnJlcGxhY2UocmVUcmltLCAnJyk7XG4gIHZhciBpc0JpbmFyeSA9IHJlSXNCaW5hcnkudGVzdCh2YWx1ZSk7XG4gIHJldHVybiAoaXNCaW5hcnkgfHwgcmVJc09jdGFsLnRlc3QodmFsdWUpKVxuICAgID8gZnJlZVBhcnNlSW50KHZhbHVlLnNsaWNlKDIpLCBpc0JpbmFyeSA/IDIgOiA4KVxuICAgIDogKHJlSXNCYWRIZXgudGVzdCh2YWx1ZSkgPyBOQU4gOiArdmFsdWUpO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IHRvTnVtYmVyO1xuIiwidmFyIHJlbmRlciA9IGZ1bmN0aW9uKCkge1xuICB2YXIgX3ZtID0gdGhpc1xuICB2YXIgX2ggPSBfdm0uJGNyZWF0ZUVsZW1lbnRcbiAgdmFyIF9jID0gX3ZtLl9zZWxmLl9jIHx8IF9oXG4gIHJldHVybiBfYyhcImNhbnZhc1wiLCB7XG4gICAgcmVmOiBcInZ1ZS1zcHJpdGUtY2FudmFzXCIsXG4gICAgYXR0cnM6IHsgaWQ6IF92bS5pZCwgd2lkdGg6IF92bS53aWR0aCwgaGVpZ2h0OiBfdm0uaGVpZ2h0IH1cbiAgfSlcbn1cbnZhciBzdGF0aWNSZW5kZXJGbnMgPSBbXVxucmVuZGVyLl93aXRoU3RyaXBwZWQgPSB0cnVlXG5cbmV4cG9ydCB7IHJlbmRlciwgc3RhdGljUmVuZGVyRm5zIH0iLCIvKiBnbG9iYWxzIF9fVlVFX1NTUl9DT05URVhUX18gKi9cblxuLy8gSU1QT1JUQU5UOiBEbyBOT1QgdXNlIEVTMjAxNSBmZWF0dXJlcyBpbiB0aGlzIGZpbGUgKGV4Y2VwdCBmb3IgbW9kdWxlcykuXG4vLyBUaGlzIG1vZHVsZSBpcyBhIHJ1bnRpbWUgdXRpbGl0eSBmb3IgY2xlYW5lciBjb21wb25lbnQgbW9kdWxlIG91dHB1dCBhbmQgd2lsbFxuLy8gYmUgaW5jbHVkZWQgaW4gdGhlIGZpbmFsIHdlYnBhY2sgdXNlciBidW5kbGUuXG5cbmV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIG5vcm1hbGl6ZUNvbXBvbmVudCAoXG4gIHNjcmlwdEV4cG9ydHMsXG4gIHJlbmRlcixcbiAgc3RhdGljUmVuZGVyRm5zLFxuICBmdW5jdGlvbmFsVGVtcGxhdGUsXG4gIGluamVjdFN0eWxlcyxcbiAgc2NvcGVJZCxcbiAgbW9kdWxlSWRlbnRpZmllciwgLyogc2VydmVyIG9ubHkgKi9cbiAgc2hhZG93TW9kZSAvKiB2dWUtY2xpIG9ubHkgKi9cbikge1xuICAvLyBWdWUuZXh0ZW5kIGNvbnN0cnVjdG9yIGV4cG9ydCBpbnRlcm9wXG4gIHZhciBvcHRpb25zID0gdHlwZW9mIHNjcmlwdEV4cG9ydHMgPT09ICdmdW5jdGlvbidcbiAgICA/IHNjcmlwdEV4cG9ydHMub3B0aW9uc1xuICAgIDogc2NyaXB0RXhwb3J0c1xuXG4gIC8vIHJlbmRlciBmdW5jdGlvbnNcbiAgaWYgKHJlbmRlcikge1xuICAgIG9wdGlvbnMucmVuZGVyID0gcmVuZGVyXG4gICAgb3B0aW9ucy5zdGF0aWNSZW5kZXJGbnMgPSBzdGF0aWNSZW5kZXJGbnNcbiAgICBvcHRpb25zLl9jb21waWxlZCA9IHRydWVcbiAgfVxuXG4gIC8vIGZ1bmN0aW9uYWwgdGVtcGxhdGVcbiAgaWYgKGZ1bmN0aW9uYWxUZW1wbGF0ZSkge1xuICAgIG9wdGlvbnMuZnVuY3Rpb25hbCA9IHRydWVcbiAgfVxuXG4gIC8vIHNjb3BlZElkXG4gIGlmIChzY29wZUlkKSB7XG4gICAgb3B0aW9ucy5fc2NvcGVJZCA9ICdkYXRhLXYtJyArIHNjb3BlSWRcbiAgfVxuXG4gIHZhciBob29rXG4gIGlmIChtb2R1bGVJZGVudGlmaWVyKSB7IC8vIHNlcnZlciBidWlsZFxuICAgIGhvb2sgPSBmdW5jdGlvbiAoY29udGV4dCkge1xuICAgICAgLy8gMi4zIGluamVjdGlvblxuICAgICAgY29udGV4dCA9XG4gICAgICAgIGNvbnRleHQgfHwgLy8gY2FjaGVkIGNhbGxcbiAgICAgICAgKHRoaXMuJHZub2RlICYmIHRoaXMuJHZub2RlLnNzckNvbnRleHQpIHx8IC8vIHN0YXRlZnVsXG4gICAgICAgICh0aGlzLnBhcmVudCAmJiB0aGlzLnBhcmVudC4kdm5vZGUgJiYgdGhpcy5wYXJlbnQuJHZub2RlLnNzckNvbnRleHQpIC8vIGZ1bmN0aW9uYWxcbiAgICAgIC8vIDIuMiB3aXRoIHJ1bkluTmV3Q29udGV4dDogdHJ1ZVxuICAgICAgaWYgKCFjb250ZXh0ICYmIHR5cGVvZiBfX1ZVRV9TU1JfQ09OVEVYVF9fICE9PSAndW5kZWZpbmVkJykge1xuICAgICAgICBjb250ZXh0ID0gX19WVUVfU1NSX0NPTlRFWFRfX1xuICAgICAgfVxuICAgICAgLy8gaW5qZWN0IGNvbXBvbmVudCBzdHlsZXNcbiAgICAgIGlmIChpbmplY3RTdHlsZXMpIHtcbiAgICAgICAgaW5qZWN0U3R5bGVzLmNhbGwodGhpcywgY29udGV4dClcbiAgICAgIH1cbiAgICAgIC8vIHJlZ2lzdGVyIGNvbXBvbmVudCBtb2R1bGUgaWRlbnRpZmllciBmb3IgYXN5bmMgY2h1bmsgaW5mZXJyZW5jZVxuICAgICAgaWYgKGNvbnRleHQgJiYgY29udGV4dC5fcmVnaXN0ZXJlZENvbXBvbmVudHMpIHtcbiAgICAgICAgY29udGV4dC5fcmVnaXN0ZXJlZENvbXBvbmVudHMuYWRkKG1vZHVsZUlkZW50aWZpZXIpXG4gICAgICB9XG4gICAgfVxuICAgIC8vIHVzZWQgYnkgc3NyIGluIGNhc2UgY29tcG9uZW50IGlzIGNhY2hlZCBhbmQgYmVmb3JlQ3JlYXRlXG4gICAgLy8gbmV2ZXIgZ2V0cyBjYWxsZWRcbiAgICBvcHRpb25zLl9zc3JSZWdpc3RlciA9IGhvb2tcbiAgfSBlbHNlIGlmIChpbmplY3RTdHlsZXMpIHtcbiAgICBob29rID0gc2hhZG93TW9kZVxuICAgICAgPyBmdW5jdGlvbiAoKSB7XG4gICAgICAgIGluamVjdFN0eWxlcy5jYWxsKFxuICAgICAgICAgIHRoaXMsXG4gICAgICAgICAgKG9wdGlvbnMuZnVuY3Rpb25hbCA/IHRoaXMucGFyZW50IDogdGhpcykuJHJvb3QuJG9wdGlvbnMuc2hhZG93Um9vdFxuICAgICAgICApXG4gICAgICB9XG4gICAgICA6IGluamVjdFN0eWxlc1xuICB9XG5cbiAgaWYgKGhvb2spIHtcbiAgICBpZiAob3B0aW9ucy5mdW5jdGlvbmFsKSB7XG4gICAgICAvLyBmb3IgdGVtcGxhdGUtb25seSBob3QtcmVsb2FkIGJlY2F1c2UgaW4gdGhhdCBjYXNlIHRoZSByZW5kZXIgZm4gZG9lc24ndFxuICAgICAgLy8gZ28gdGhyb3VnaCB0aGUgbm9ybWFsaXplclxuICAgICAgb3B0aW9ucy5faW5qZWN0U3R5bGVzID0gaG9va1xuICAgICAgLy8gcmVnaXN0ZXIgZm9yIGZ1bmN0aW9uYWwgY29tcG9uZW50IGluIHZ1ZSBmaWxlXG4gICAgICB2YXIgb3JpZ2luYWxSZW5kZXIgPSBvcHRpb25zLnJlbmRlclxuICAgICAgb3B0aW9ucy5yZW5kZXIgPSBmdW5jdGlvbiByZW5kZXJXaXRoU3R5bGVJbmplY3Rpb24gKGgsIGNvbnRleHQpIHtcbiAgICAgICAgaG9vay5jYWxsKGNvbnRleHQpXG4gICAgICAgIHJldHVybiBvcmlnaW5hbFJlbmRlcihoLCBjb250ZXh0KVxuICAgICAgfVxuICAgIH0gZWxzZSB7XG4gICAgICAvLyBpbmplY3QgY29tcG9uZW50IHJlZ2lzdHJhdGlvbiBhcyBiZWZvcmVDcmVhdGUgaG9va1xuICAgICAgdmFyIGV4aXN0aW5nID0gb3B0aW9ucy5iZWZvcmVDcmVhdGVcbiAgICAgIG9wdGlvbnMuYmVmb3JlQ3JlYXRlID0gZXhpc3RpbmdcbiAgICAgICAgPyBbXS5jb25jYXQoZXhpc3RpbmcsIGhvb2spXG4gICAgICAgIDogW2hvb2tdXG4gICAgfVxuICB9XG5cbiAgcmV0dXJuIHtcbiAgICBleHBvcnRzOiBzY3JpcHRFeHBvcnRzLFxuICAgIG9wdGlvbnM6IG9wdGlvbnNcbiAgfVxufVxuIiwidmFyIGc7XG5cbi8vIFRoaXMgd29ya3MgaW4gbm9uLXN0cmljdCBtb2RlXG5nID0gKGZ1bmN0aW9uKCkge1xuXHRyZXR1cm4gdGhpcztcbn0pKCk7XG5cbnRyeSB7XG5cdC8vIFRoaXMgd29ya3MgaWYgZXZhbCBpcyBhbGxvd2VkIChzZWUgQ1NQKVxuXHRnID0gZyB8fCBuZXcgRnVuY3Rpb24oXCJyZXR1cm4gdGhpc1wiKSgpO1xufSBjYXRjaCAoZSkge1xuXHQvLyBUaGlzIHdvcmtzIGlmIHRoZSB3aW5kb3cgcmVmZXJlbmNlIGlzIGF2YWlsYWJsZVxuXHRpZiAodHlwZW9mIHdpbmRvdyA9PT0gXCJvYmplY3RcIikgZyA9IHdpbmRvdztcbn1cblxuLy8gZyBjYW4gc3RpbGwgYmUgdW5kZWZpbmVkLCBidXQgbm90aGluZyB0byBkbyBhYm91dCBpdC4uLlxuLy8gV2UgcmV0dXJuIHVuZGVmaW5lZCwgaW5zdGVhZCBvZiBub3RoaW5nIGhlcmUsIHNvIGl0J3Ncbi8vIGVhc2llciB0byBoYW5kbGUgdGhpcyBjYXNlLiBpZighZ2xvYmFsKSB7IC4uLn1cblxubW9kdWxlLmV4cG9ydHMgPSBnO1xuIiwiaW1wb3J0IHsgcmVuZGVyLCBzdGF0aWNSZW5kZXJGbnMgfSBmcm9tIFwiLi9TcHJpdGVBbmltYXRvci52dWU/dnVlJnR5cGU9dGVtcGxhdGUmaWQ9MWQxYjQ0ZmMmbGFuZz1odG1sJlwiXG5pbXBvcnQgc2NyaXB0IGZyb20gXCIuL1Nwcml0ZUFuaW1hdG9yLnZ1ZT92dWUmdHlwZT1zY3JpcHQmbGFuZz1qcyZcIlxuZXhwb3J0ICogZnJvbSBcIi4vU3ByaXRlQW5pbWF0b3IudnVlP3Z1ZSZ0eXBlPXNjcmlwdCZsYW5nPWpzJlwiXG5cblxuLyogbm9ybWFsaXplIGNvbXBvbmVudCAqL1xuaW1wb3J0IG5vcm1hbGl6ZXIgZnJvbSBcIiEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvcnVudGltZS9jb21wb25lbnROb3JtYWxpemVyLmpzXCJcbnZhciBjb21wb25lbnQgPSBub3JtYWxpemVyKFxuICBzY3JpcHQsXG4gIHJlbmRlcixcbiAgc3RhdGljUmVuZGVyRm5zLFxuICBmYWxzZSxcbiAgbnVsbCxcbiAgbnVsbCxcbiAgbnVsbFxuICBcbilcblxuLyogaG90IHJlbG9hZCAqL1xuaWYgKG1vZHVsZS5ob3QpIHtcbiAgdmFyIGFwaSA9IHJlcXVpcmUoXCJEOlxcXFxVc2Vyc1xcXFxkYW5pXFxcXERlc2t0b3BcXFxcQG1vbm9ncmlkXFxcXG5vZGVfbW9kdWxlc1xcXFx2dWUtaG90LXJlbG9hZC1hcGlcXFxcZGlzdFxcXFxpbmRleC5qc1wiKVxuICBhcGkuaW5zdGFsbChyZXF1aXJlKCd2dWUnKSlcbiAgaWYgKGFwaS5jb21wYXRpYmxlKSB7XG4gICAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICAgIGlmICghYXBpLmlzUmVjb3JkZWQoJzFkMWI0NGZjJykpIHtcbiAgICAgIGFwaS5jcmVhdGVSZWNvcmQoJzFkMWI0NGZjJywgY29tcG9uZW50Lm9wdGlvbnMpXG4gICAgfSBlbHNlIHtcbiAgICAgIGFwaS5yZWxvYWQoJzFkMWI0NGZjJywgY29tcG9uZW50Lm9wdGlvbnMpXG4gICAgfVxuICAgIG1vZHVsZS5ob3QuYWNjZXB0KFwiLi9TcHJpdGVBbmltYXRvci52dWU/dnVlJnR5cGU9dGVtcGxhdGUmaWQ9MWQxYjQ0ZmMmbGFuZz1odG1sJlwiLCBmdW5jdGlvbiAoKSB7XG4gICAgICBhcGkucmVyZW5kZXIoJzFkMWI0NGZjJywge1xuICAgICAgICByZW5kZXI6IHJlbmRlcixcbiAgICAgICAgc3RhdGljUmVuZGVyRm5zOiBzdGF0aWNSZW5kZXJGbnNcbiAgICAgIH0pXG4gICAgfSlcbiAgfVxufVxuY29tcG9uZW50Lm9wdGlvbnMuX19maWxlID0gXCJzcmMvY29tcG9uZW50cy9TcHJpdGVBbmltYXRvci52dWVcIlxuZXhwb3J0IGRlZmF1bHQgY29tcG9uZW50LmV4cG9ydHMiLCJpbXBvcnQgbW9kIGZyb20gXCItIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9iYWJlbC1sb2FkZXIvbGliL2luZGV4LmpzIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9pbmRleC5qcz8/dnVlLWxvYWRlci1vcHRpb25zIS4vU3ByaXRlQW5pbWF0b3IudnVlP3Z1ZSZ0eXBlPXNjcmlwdCZsYW5nPWpzJlwiOyBleHBvcnQgZGVmYXVsdCBtb2Q7IGV4cG9ydCAqIGZyb20gXCItIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9iYWJlbC1sb2FkZXIvbGliL2luZGV4LmpzIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9pbmRleC5qcz8/dnVlLWxvYWRlci1vcHRpb25zIS4vU3ByaXRlQW5pbWF0b3IudnVlP3Z1ZSZ0eXBlPXNjcmlwdCZsYW5nPWpzJlwiIiwiZXhwb3J0ICogZnJvbSBcIi0hLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2xvYWRlcnMvdGVtcGxhdGVMb2FkZXIuanM/P3Z1ZS1sb2FkZXItb3B0aW9ucyEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvaW5kZXguanM/P3Z1ZS1sb2FkZXItb3B0aW9ucyEuL1Nwcml0ZUFuaW1hdG9yLnZ1ZT92dWUmdHlwZT10ZW1wbGF0ZSZpZD0xZDFiNDRmYyZsYW5nPWh0bWwmXCIiLCJtb2R1bGUuZXhwb3J0cyA9IF9fV0VCUEFDS19FWFRFUk5BTF9NT0RVTEVfX21vbm9ncmlkX2pzX3V0aWxzX187Il0sInNvdXJjZVJvb3QiOiIifQ==