(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("vue"));
	else if(typeof define === 'function' && define.amd)
		define("vue-lib", ["vue"], factory);
	else if(typeof exports === 'object')
		exports["vue-lib"] = factory(require("vue"));
	else
		root["vue-lib"] = factory(root["vue"]);
})(window, function(__WEBPACK_EXTERNAL_MODULE_vue__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/components/MarkdownBlock.vue");
/******/ })
/************************************************************************/
/******/ ({

/***/ "../../node_modules/@babel/runtime/helpers/arrayLikeToArray.js":
/*!***********************************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/@babel/runtime/helpers/arrayLikeToArray.js ***!
  \***********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _arrayLikeToArray(arr, len) {
  if (len == null || len > arr.length) len = arr.length;

  for (var i = 0, arr2 = new Array(len); i < len; i++) {
    arr2[i] = arr[i];
  }

  return arr2;
}

module.exports = _arrayLikeToArray;

/***/ }),

/***/ "../../node_modules/@babel/runtime/helpers/arrayWithHoles.js":
/*!*********************************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/@babel/runtime/helpers/arrayWithHoles.js ***!
  \*********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _arrayWithHoles(arr) {
  if (Array.isArray(arr)) return arr;
}

module.exports = _arrayWithHoles;

/***/ }),

/***/ "../../node_modules/@babel/runtime/helpers/iterableToArrayLimit.js":
/*!***************************************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/@babel/runtime/helpers/iterableToArrayLimit.js ***!
  \***************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _iterableToArrayLimit(arr, i) {
  if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return;
  var _arr = [];
  var _n = true;
  var _d = false;
  var _e = undefined;

  try {
    for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) {
      _arr.push(_s.value);

      if (i && _arr.length === i) break;
    }
  } catch (err) {
    _d = true;
    _e = err;
  } finally {
    try {
      if (!_n && _i["return"] != null) _i["return"]();
    } finally {
      if (_d) throw _e;
    }
  }

  return _arr;
}

module.exports = _iterableToArrayLimit;

/***/ }),

/***/ "../../node_modules/@babel/runtime/helpers/nonIterableRest.js":
/*!**********************************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/@babel/runtime/helpers/nonIterableRest.js ***!
  \**********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _nonIterableRest() {
  throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
}

module.exports = _nonIterableRest;

/***/ }),

/***/ "../../node_modules/@babel/runtime/helpers/slicedToArray.js":
/*!********************************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/@babel/runtime/helpers/slicedToArray.js ***!
  \********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var arrayWithHoles = __webpack_require__(/*! ./arrayWithHoles */ "../../node_modules/@babel/runtime/helpers/arrayWithHoles.js");

var iterableToArrayLimit = __webpack_require__(/*! ./iterableToArrayLimit */ "../../node_modules/@babel/runtime/helpers/iterableToArrayLimit.js");

var unsupportedIterableToArray = __webpack_require__(/*! ./unsupportedIterableToArray */ "../../node_modules/@babel/runtime/helpers/unsupportedIterableToArray.js");

var nonIterableRest = __webpack_require__(/*! ./nonIterableRest */ "../../node_modules/@babel/runtime/helpers/nonIterableRest.js");

function _slicedToArray(arr, i) {
  return arrayWithHoles(arr) || iterableToArrayLimit(arr, i) || unsupportedIterableToArray(arr, i) || nonIterableRest();
}

module.exports = _slicedToArray;

/***/ }),

/***/ "../../node_modules/@babel/runtime/helpers/typeof.js":
/*!*************************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/@babel/runtime/helpers/typeof.js ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _typeof(obj) {
  "@babel/helpers - typeof";

  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    module.exports = _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    module.exports = _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}

module.exports = _typeof;

/***/ }),

/***/ "../../node_modules/@babel/runtime/helpers/unsupportedIterableToArray.js":
/*!*********************************************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/@babel/runtime/helpers/unsupportedIterableToArray.js ***!
  \*********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var arrayLikeToArray = __webpack_require__(/*! ./arrayLikeToArray */ "../../node_modules/@babel/runtime/helpers/arrayLikeToArray.js");

function _unsupportedIterableToArray(o, minLen) {
  if (!o) return;
  if (typeof o === "string") return arrayLikeToArray(o, minLen);
  var n = Object.prototype.toString.call(o).slice(8, -1);
  if (n === "Object" && o.constructor) n = o.constructor.name;
  if (n === "Map" || n === "Set") return Array.from(o);
  if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return arrayLikeToArray(o, minLen);
}

module.exports = _unsupportedIterableToArray;

/***/ }),

/***/ "../../node_modules/babel-loader/lib/index.js!../../node_modules/vue-loader/lib/index.js?!./src/components/MarkdownBlock.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************************************************************************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/babel-loader/lib!D:/Users/dani/Desktop/@monogrid/node_modules/vue-loader/lib??vue-loader-options!./src/components/MarkdownBlock.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "vue");
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(vue__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var markdown_it__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! markdown-it */ "../../node_modules/markdown-it/index.js");
/* harmony import */ var markdown_it__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(markdown_it__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var markdown_it_link_attributes__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! markdown-it-link-attributes */ "../../node_modules/markdown-it-link-attributes/index.js");
/* harmony import */ var markdown_it_link_attributes__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(markdown_it_link_attributes__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var markdown_it_custom_block__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! markdown-it-custom-block */ "../../node_modules/markdown-it-custom-block/index.js");
/* harmony import */ var markdown_it_custom_block__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(markdown_it_custom_block__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var markdown_it_container__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! markdown-it-container */ "../../node_modules/markdown-it-container/index.js");
/* harmony import */ var markdown_it_container__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(markdown_it_container__WEBPACK_IMPORTED_MODULE_4__);





/**
 * A component that renders markdown text in Vue using **markdown-it** and some of its plugins.
 */

/* harmony default export */ __webpack_exports__["default"] = ({
  props: {
    /**
     * The text to be displayed, should be formatted in markdown syntax
     */
    text: {
      type: String,
      required: true,
      default: ''
    },

    /**
     * Decides whether block tags are rendered or not, example:
     *
     * input
     *
     * ```md
     * some paragraph
     *
     * some other paragraph
     * ```
     *
     * output with true
     *
     * ```html
     * &lt;div&gt;
     *   &lt;p&gt;some paragraph&lt;/p&gt;
     *   &lt;p&gt;some other paragraph&lt;/p&gt;
     * &lt;/div&gt;
     * ```
     *
     * output with false
     *
     * ```html
     * &lt;div&gt;some paragraph some other paragraph&lt;/div&gt;
     * ```
     * @see [markdown-it inline rendering](https://github.com/markdown-it/markdown-it#simple)
     */
    inline: {
      type: Boolean,
      default: false
    },

    /**
     * The base root tag of this element, can be any tag (div, span, p, etc)
     *
     * Should be a valid html tag.
     */
    tag: {
      type: String,
      default: 'div'
    },

    /**
     * Autoconvert URL-like text to links
     *
     * @see [markdown-it docs](https://github.com/markdown-it/markdown-it#init-with-presets-and-options)
     */
    linkify: {
      type: Boolean,
      default: false
    },

    /**
     * Enable some language-neutral replacement + quotes beautification
     *
     * @see [markdown-it docs](https://github.com/markdown-it/markdown-it#init-with-presets-and-options)
     */
    typographer: {
      type: Boolean,
      default: false
    },

    /**
     * Hyperlinks Attributes, applied to all hyperlinks.
     *
     * This uses **markdown-it-link-attributes**, the object is the configuration for the plugin
     *
     * @see [markdown-it-link-attributes docs](https://github.com/crookedneighbor/markdown-it-link-attributes)
     */
    linksAttributes: {
      type: Object,
      default: function _default() {
        return {
          attrs: {
            target: '_blank',
            rel: 'noopener'
          }
        };
      }
    },

    /**
     * Object containing any number of **markdown-it-custom-block** functions
     *
     * Each function should have:
     *
     * * the name of the custom block as function name
     * * parser for the custom block as the body of the function
     *
     * @see [markdown-it-custom-block docs](https://github.com/posva/markdown-it-custom-block)
     */
    customBlocks: {
      type: Object,
      default: function _default() {
        return {
          'mark': function mark(arg) {
            return "<mark>".concat(arg, "</mark>");
          }
        };
      }
    },

    /**
     * Object containing any number of **markdown-it-container**
     * configuration objects indexed by their container name, like this:
     *
     * |       key      |            value               |
     * |----------------|--------------------------------|
     * | container-name | { validate: ..., render: ... } |
     *
     * markdown-it-container is a plugin for creating block-level custom containers for markdown-it markdown parser.
     *
     * @see [markdown-it-container docs](https://github.com/markdown-it/markdown-it-container)
     */
    customContainers: {
      type: Object,
      default: function _default() {
        return {};
      }
    }
  },
  created: function created() {
    this.buildMd();
  },
  render: function render(createElement) {
    var t = this.getTemplate(this.$props.text, this.$props.inline, this.$props.tag);
    var compiled = vue__WEBPACK_IMPORTED_MODULE_0___default.a.compile(t); // create a new scope for the compiled markdown template

    var scope = {
      _c: this.$parent._c,
      _v: this.$parent._v,
      _m: this.$parent._m,
      _staticTrees: [],
      _renderProxy: this.$parent._renderProxy,
      $options: {
        staticRenderFns: compiled.staticRenderFns
      }
    }; // console.log('rendering markdown')

    return createElement({
      name: 'MarkdownBlock',
      render: compiled.render.bind(scope)
    });
  },
  watch: {
    linkify: function linkify(value) {
      this.buildMd();
      this.$forceUpdate();
    },
    typographer: function typographer(value) {
      this.buildMd();
      this.$forceUpdate();
    },
    linksAttributes: function linksAttributes(value) {
      this.buildMd();
      this.$forceUpdate();
    },
    customBlocks: function customBlocks(value) {
      this.buildMd();
      this.$forceUpdate();
    },
    customContainers: function customContainers(value) {
      this.buildMd();
      this.$forceUpdate();
    }
  },
  methods: {
    buildMd: function buildMd() {
      var _this = this;

      this.md = new markdown_it__WEBPACK_IMPORTED_MODULE_1___default.a({
        linkify: this.linkify,
        typographer: this.typographer
      });
      this.md.use(markdown_it_link_attributes__WEBPACK_IMPORTED_MODULE_2___default.a, this.linksAttributes);
      this.md.use(markdown_it_custom_block__WEBPACK_IMPORTED_MODULE_3___default.a, this.customBlocks);

      if (this.customContainers) {
        Object.keys(this.customContainers).forEach(function (key) {
          _this.md.use(markdown_it_container__WEBPACK_IMPORTED_MODULE_4___default.a, key, _this.customContainers[key]);
        });
      }
    },
    getTemplate: function getTemplate(text) {
      var inline = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
      var rootTag = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 'span';

      // check if root tag exists when inline = true
      if (inline === true && !rootTag) {
        throw new Error('[MarkdownParser] When using inline option you must specify a root tag');
      }

      var templateMarkdown;

      if (inline) {
        // we parse this with markdown
        // and add a root tag of our choice
        templateMarkdown = (rootTag ? '<' + rootTag + '>' : '') + this.md.renderInline(text) + (rootTag ? '</' + rootTag + '>' : '');
      } else {
        // or use this if you have multiline elements or you want a <p> tag as root of a single line
        templateMarkdown = (rootTag ? '<' + rootTag + '>' : '') + this.md.render(text) + (rootTag ? '</' + rootTag + '>' : '');
      } // QUICK HACK:
      // [Else](Something "Optional") usually get compiled to html links
      // so we replace any <a href="Something" title="Optional">Else</a>
      // with <router-link :to="{name: 'Something' params: {Optional}}">Else</router-link>
      // NOTE: this should leave alone links wich contains non-word characters like . or / so external links still work
      // const template = templateMarkdown.replace(/<a href="([\w]+)"[ ]*(title="([^"]*)")?[^>]*>([^<]+)<\/a>/igm, (match, p1, p2, p3, p4) => {
      //   return '<router-link :to="{name: \'' + p1 + '\'' + (p2 ? ', params: {' + p3 + '}' : '') + '}">' + p4 + '</router-link>'
      // })
      // return a new component so that router-links get parsed and rendered


      return templateMarkdown;
    }
  }
});

/***/ }),

/***/ "../../node_modules/entities/lib/maps/entities.json":
/*!************************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/entities/lib/maps/entities.json ***!
  \************************************************************************************/
/*! exports provided: Aacute, aacute, Abreve, abreve, ac, acd, acE, Acirc, acirc, acute, Acy, acy, AElig, aelig, af, Afr, afr, Agrave, agrave, alefsym, aleph, Alpha, alpha, Amacr, amacr, amalg, amp, AMP, andand, And, and, andd, andslope, andv, ang, ange, angle, angmsdaa, angmsdab, angmsdac, angmsdad, angmsdae, angmsdaf, angmsdag, angmsdah, angmsd, angrt, angrtvb, angrtvbd, angsph, angst, angzarr, Aogon, aogon, Aopf, aopf, apacir, ap, apE, ape, apid, apos, ApplyFunction, approx, approxeq, Aring, aring, Ascr, ascr, Assign, ast, asymp, asympeq, Atilde, atilde, Auml, auml, awconint, awint, backcong, backepsilon, backprime, backsim, backsimeq, Backslash, Barv, barvee, barwed, Barwed, barwedge, bbrk, bbrktbrk, bcong, Bcy, bcy, bdquo, becaus, because, Because, bemptyv, bepsi, bernou, Bernoullis, Beta, beta, beth, between, Bfr, bfr, bigcap, bigcirc, bigcup, bigodot, bigoplus, bigotimes, bigsqcup, bigstar, bigtriangledown, bigtriangleup, biguplus, bigvee, bigwedge, bkarow, blacklozenge, blacksquare, blacktriangle, blacktriangledown, blacktriangleleft, blacktriangleright, blank, blk12, blk14, blk34, block, bne, bnequiv, bNot, bnot, Bopf, bopf, bot, bottom, bowtie, boxbox, boxdl, boxdL, boxDl, boxDL, boxdr, boxdR, boxDr, boxDR, boxh, boxH, boxhd, boxHd, boxhD, boxHD, boxhu, boxHu, boxhU, boxHU, boxminus, boxplus, boxtimes, boxul, boxuL, boxUl, boxUL, boxur, boxuR, boxUr, boxUR, boxv, boxV, boxvh, boxvH, boxVh, boxVH, boxvl, boxvL, boxVl, boxVL, boxvr, boxvR, boxVr, boxVR, bprime, breve, Breve, brvbar, bscr, Bscr, bsemi, bsim, bsime, bsolb, bsol, bsolhsub, bull, bullet, bump, bumpE, bumpe, Bumpeq, bumpeq, Cacute, cacute, capand, capbrcup, capcap, cap, Cap, capcup, capdot, CapitalDifferentialD, caps, caret, caron, Cayleys, ccaps, Ccaron, ccaron, Ccedil, ccedil, Ccirc, ccirc, Cconint, ccups, ccupssm, Cdot, cdot, cedil, Cedilla, cemptyv, cent, centerdot, CenterDot, cfr, Cfr, CHcy, chcy, check, checkmark, Chi, chi, circ, circeq, circlearrowleft, circlearrowright, circledast, circledcirc, circleddash, CircleDot, circledR, circledS, CircleMinus, CirclePlus, CircleTimes, cir, cirE, cire, cirfnint, cirmid, cirscir, ClockwiseContourIntegral, CloseCurlyDoubleQuote, CloseCurlyQuote, clubs, clubsuit, colon, Colon, Colone, colone, coloneq, comma, commat, comp, compfn, complement, complexes, cong, congdot, Congruent, conint, Conint, ContourIntegral, copf, Copf, coprod, Coproduct, copy, COPY, copysr, CounterClockwiseContourIntegral, crarr, cross, Cross, Cscr, cscr, csub, csube, csup, csupe, ctdot, cudarrl, cudarrr, cuepr, cuesc, cularr, cularrp, cupbrcap, cupcap, CupCap, cup, Cup, cupcup, cupdot, cupor, cups, curarr, curarrm, curlyeqprec, curlyeqsucc, curlyvee, curlywedge, curren, curvearrowleft, curvearrowright, cuvee, cuwed, cwconint, cwint, cylcty, dagger, Dagger, daleth, darr, Darr, dArr, dash, Dashv, dashv, dbkarow, dblac, Dcaron, dcaron, Dcy, dcy, ddagger, ddarr, DD, dd, DDotrahd, ddotseq, deg, Del, Delta, delta, demptyv, dfisht, Dfr, dfr, dHar, dharl, dharr, DiacriticalAcute, DiacriticalDot, DiacriticalDoubleAcute, DiacriticalGrave, DiacriticalTilde, diam, diamond, Diamond, diamondsuit, diams, die, DifferentialD, digamma, disin, div, divide, divideontimes, divonx, DJcy, djcy, dlcorn, dlcrop, dollar, Dopf, dopf, Dot, dot, DotDot, doteq, doteqdot, DotEqual, dotminus, dotplus, dotsquare, doublebarwedge, DoubleContourIntegral, DoubleDot, DoubleDownArrow, DoubleLeftArrow, DoubleLeftRightArrow, DoubleLeftTee, DoubleLongLeftArrow, DoubleLongLeftRightArrow, DoubleLongRightArrow, DoubleRightArrow, DoubleRightTee, DoubleUpArrow, DoubleUpDownArrow, DoubleVerticalBar, DownArrowBar, downarrow, DownArrow, Downarrow, DownArrowUpArrow, DownBreve, downdownarrows, downharpoonleft, downharpoonright, DownLeftRightVector, DownLeftTeeVector, DownLeftVectorBar, DownLeftVector, DownRightTeeVector, DownRightVectorBar, DownRightVector, DownTeeArrow, DownTee, drbkarow, drcorn, drcrop, Dscr, dscr, DScy, dscy, dsol, Dstrok, dstrok, dtdot, dtri, dtrif, duarr, duhar, dwangle, DZcy, dzcy, dzigrarr, Eacute, eacute, easter, Ecaron, ecaron, Ecirc, ecirc, ecir, ecolon, Ecy, ecy, eDDot, Edot, edot, eDot, ee, efDot, Efr, efr, eg, Egrave, egrave, egs, egsdot, el, Element, elinters, ell, els, elsdot, Emacr, emacr, empty, emptyset, EmptySmallSquare, emptyv, EmptyVerySmallSquare, emsp13, emsp14, emsp, ENG, eng, ensp, Eogon, eogon, Eopf, eopf, epar, eparsl, eplus, epsi, Epsilon, epsilon, epsiv, eqcirc, eqcolon, eqsim, eqslantgtr, eqslantless, Equal, equals, EqualTilde, equest, Equilibrium, equiv, equivDD, eqvparsl, erarr, erDot, escr, Escr, esdot, Esim, esim, Eta, eta, ETH, eth, Euml, euml, euro, excl, exist, Exists, expectation, exponentiale, ExponentialE, fallingdotseq, Fcy, fcy, female, ffilig, fflig, ffllig, Ffr, ffr, filig, FilledSmallSquare, FilledVerySmallSquare, fjlig, flat, fllig, fltns, fnof, Fopf, fopf, forall, ForAll, fork, forkv, Fouriertrf, fpartint, frac12, frac13, frac14, frac15, frac16, frac18, frac23, frac25, frac34, frac35, frac38, frac45, frac56, frac58, frac78, frasl, frown, fscr, Fscr, gacute, Gamma, gamma, Gammad, gammad, gap, Gbreve, gbreve, Gcedil, Gcirc, gcirc, Gcy, gcy, Gdot, gdot, ge, gE, gEl, gel, geq, geqq, geqslant, gescc, ges, gesdot, gesdoto, gesdotol, gesl, gesles, Gfr, gfr, gg, Gg, ggg, gimel, GJcy, gjcy, gla, gl, glE, glj, gnap, gnapprox, gne, gnE, gneq, gneqq, gnsim, Gopf, gopf, grave, GreaterEqual, GreaterEqualLess, GreaterFullEqual, GreaterGreater, GreaterLess, GreaterSlantEqual, GreaterTilde, Gscr, gscr, gsim, gsime, gsiml, gtcc, gtcir, gt, GT, Gt, gtdot, gtlPar, gtquest, gtrapprox, gtrarr, gtrdot, gtreqless, gtreqqless, gtrless, gtrsim, gvertneqq, gvnE, Hacek, hairsp, half, hamilt, HARDcy, hardcy, harrcir, harr, hArr, harrw, Hat, hbar, Hcirc, hcirc, hearts, heartsuit, hellip, hercon, hfr, Hfr, HilbertSpace, hksearow, hkswarow, hoarr, homtht, hookleftarrow, hookrightarrow, hopf, Hopf, horbar, HorizontalLine, hscr, Hscr, hslash, Hstrok, hstrok, HumpDownHump, HumpEqual, hybull, hyphen, Iacute, iacute, ic, Icirc, icirc, Icy, icy, Idot, IEcy, iecy, iexcl, iff, ifr, Ifr, Igrave, igrave, ii, iiiint, iiint, iinfin, iiota, IJlig, ijlig, Imacr, imacr, image, ImaginaryI, imagline, imagpart, imath, Im, imof, imped, Implies, incare, in, infin, infintie, inodot, intcal, int, Int, integers, Integral, intercal, Intersection, intlarhk, intprod, InvisibleComma, InvisibleTimes, IOcy, iocy, Iogon, iogon, Iopf, iopf, Iota, iota, iprod, iquest, iscr, Iscr, isin, isindot, isinE, isins, isinsv, isinv, it, Itilde, itilde, Iukcy, iukcy, Iuml, iuml, Jcirc, jcirc, Jcy, jcy, Jfr, jfr, jmath, Jopf, jopf, Jscr, jscr, Jsercy, jsercy, Jukcy, jukcy, Kappa, kappa, kappav, Kcedil, kcedil, Kcy, kcy, Kfr, kfr, kgreen, KHcy, khcy, KJcy, kjcy, Kopf, kopf, Kscr, kscr, lAarr, Lacute, lacute, laemptyv, lagran, Lambda, lambda, lang, Lang, langd, langle, lap, Laplacetrf, laquo, larrb, larrbfs, larr, Larr, lArr, larrfs, larrhk, larrlp, larrpl, larrsim, larrtl, latail, lAtail, lat, late, lates, lbarr, lBarr, lbbrk, lbrace, lbrack, lbrke, lbrksld, lbrkslu, Lcaron, lcaron, Lcedil, lcedil, lceil, lcub, Lcy, lcy, ldca, ldquo, ldquor, ldrdhar, ldrushar, ldsh, le, lE, LeftAngleBracket, LeftArrowBar, leftarrow, LeftArrow, Leftarrow, LeftArrowRightArrow, leftarrowtail, LeftCeiling, LeftDoubleBracket, LeftDownTeeVector, LeftDownVectorBar, LeftDownVector, LeftFloor, leftharpoondown, leftharpoonup, leftleftarrows, leftrightarrow, LeftRightArrow, Leftrightarrow, leftrightarrows, leftrightharpoons, leftrightsquigarrow, LeftRightVector, LeftTeeArrow, LeftTee, LeftTeeVector, leftthreetimes, LeftTriangleBar, LeftTriangle, LeftTriangleEqual, LeftUpDownVector, LeftUpTeeVector, LeftUpVectorBar, LeftUpVector, LeftVectorBar, LeftVector, lEg, leg, leq, leqq, leqslant, lescc, les, lesdot, lesdoto, lesdotor, lesg, lesges, lessapprox, lessdot, lesseqgtr, lesseqqgtr, LessEqualGreater, LessFullEqual, LessGreater, lessgtr, LessLess, lesssim, LessSlantEqual, LessTilde, lfisht, lfloor, Lfr, lfr, lg, lgE, lHar, lhard, lharu, lharul, lhblk, LJcy, ljcy, llarr, ll, Ll, llcorner, Lleftarrow, llhard, lltri, Lmidot, lmidot, lmoustache, lmoust, lnap, lnapprox, lne, lnE, lneq, lneqq, lnsim, loang, loarr, lobrk, longleftarrow, LongLeftArrow, Longleftarrow, longleftrightarrow, LongLeftRightArrow, Longleftrightarrow, longmapsto, longrightarrow, LongRightArrow, Longrightarrow, looparrowleft, looparrowright, lopar, Lopf, lopf, loplus, lotimes, lowast, lowbar, LowerLeftArrow, LowerRightArrow, loz, lozenge, lozf, lpar, lparlt, lrarr, lrcorner, lrhar, lrhard, lrm, lrtri, lsaquo, lscr, Lscr, lsh, Lsh, lsim, lsime, lsimg, lsqb, lsquo, lsquor, Lstrok, lstrok, ltcc, ltcir, lt, LT, Lt, ltdot, lthree, ltimes, ltlarr, ltquest, ltri, ltrie, ltrif, ltrPar, lurdshar, luruhar, lvertneqq, lvnE, macr, male, malt, maltese, Map, map, mapsto, mapstodown, mapstoleft, mapstoup, marker, mcomma, Mcy, mcy, mdash, mDDot, measuredangle, MediumSpace, Mellintrf, Mfr, mfr, mho, micro, midast, midcir, mid, middot, minusb, minus, minusd, minusdu, MinusPlus, mlcp, mldr, mnplus, models, Mopf, mopf, mp, mscr, Mscr, mstpos, Mu, mu, multimap, mumap, nabla, Nacute, nacute, nang, nap, napE, napid, napos, napprox, natural, naturals, natur, nbsp, nbump, nbumpe, ncap, Ncaron, ncaron, Ncedil, ncedil, ncong, ncongdot, ncup, Ncy, ncy, ndash, nearhk, nearr, neArr, nearrow, ne, nedot, NegativeMediumSpace, NegativeThickSpace, NegativeThinSpace, NegativeVeryThinSpace, nequiv, nesear, nesim, NestedGreaterGreater, NestedLessLess, NewLine, nexist, nexists, Nfr, nfr, ngE, nge, ngeq, ngeqq, ngeqslant, nges, nGg, ngsim, nGt, ngt, ngtr, nGtv, nharr, nhArr, nhpar, ni, nis, nisd, niv, NJcy, njcy, nlarr, nlArr, nldr, nlE, nle, nleftarrow, nLeftarrow, nleftrightarrow, nLeftrightarrow, nleq, nleqq, nleqslant, nles, nless, nLl, nlsim, nLt, nlt, nltri, nltrie, nLtv, nmid, NoBreak, NonBreakingSpace, nopf, Nopf, Not, not, NotCongruent, NotCupCap, NotDoubleVerticalBar, NotElement, NotEqual, NotEqualTilde, NotExists, NotGreater, NotGreaterEqual, NotGreaterFullEqual, NotGreaterGreater, NotGreaterLess, NotGreaterSlantEqual, NotGreaterTilde, NotHumpDownHump, NotHumpEqual, notin, notindot, notinE, notinva, notinvb, notinvc, NotLeftTriangleBar, NotLeftTriangle, NotLeftTriangleEqual, NotLess, NotLessEqual, NotLessGreater, NotLessLess, NotLessSlantEqual, NotLessTilde, NotNestedGreaterGreater, NotNestedLessLess, notni, notniva, notnivb, notnivc, NotPrecedes, NotPrecedesEqual, NotPrecedesSlantEqual, NotReverseElement, NotRightTriangleBar, NotRightTriangle, NotRightTriangleEqual, NotSquareSubset, NotSquareSubsetEqual, NotSquareSuperset, NotSquareSupersetEqual, NotSubset, NotSubsetEqual, NotSucceeds, NotSucceedsEqual, NotSucceedsSlantEqual, NotSucceedsTilde, NotSuperset, NotSupersetEqual, NotTilde, NotTildeEqual, NotTildeFullEqual, NotTildeTilde, NotVerticalBar, nparallel, npar, nparsl, npart, npolint, npr, nprcue, nprec, npreceq, npre, nrarrc, nrarr, nrArr, nrarrw, nrightarrow, nRightarrow, nrtri, nrtrie, nsc, nsccue, nsce, Nscr, nscr, nshortmid, nshortparallel, nsim, nsime, nsimeq, nsmid, nspar, nsqsube, nsqsupe, nsub, nsubE, nsube, nsubset, nsubseteq, nsubseteqq, nsucc, nsucceq, nsup, nsupE, nsupe, nsupset, nsupseteq, nsupseteqq, ntgl, Ntilde, ntilde, ntlg, ntriangleleft, ntrianglelefteq, ntriangleright, ntrianglerighteq, Nu, nu, num, numero, numsp, nvap, nvdash, nvDash, nVdash, nVDash, nvge, nvgt, nvHarr, nvinfin, nvlArr, nvle, nvlt, nvltrie, nvrArr, nvrtrie, nvsim, nwarhk, nwarr, nwArr, nwarrow, nwnear, Oacute, oacute, oast, Ocirc, ocirc, ocir, Ocy, ocy, odash, Odblac, odblac, odiv, odot, odsold, OElig, oelig, ofcir, Ofr, ofr, ogon, Ograve, ograve, ogt, ohbar, ohm, oint, olarr, olcir, olcross, oline, olt, Omacr, omacr, Omega, omega, Omicron, omicron, omid, ominus, Oopf, oopf, opar, OpenCurlyDoubleQuote, OpenCurlyQuote, operp, oplus, orarr, Or, or, ord, order, orderof, ordf, ordm, origof, oror, orslope, orv, oS, Oscr, oscr, Oslash, oslash, osol, Otilde, otilde, otimesas, Otimes, otimes, Ouml, ouml, ovbar, OverBar, OverBrace, OverBracket, OverParenthesis, para, parallel, par, parsim, parsl, part, PartialD, Pcy, pcy, percnt, period, permil, perp, pertenk, Pfr, pfr, Phi, phi, phiv, phmmat, phone, Pi, pi, pitchfork, piv, planck, planckh, plankv, plusacir, plusb, pluscir, plus, plusdo, plusdu, pluse, PlusMinus, plusmn, plussim, plustwo, pm, Poincareplane, pointint, popf, Popf, pound, prap, Pr, pr, prcue, precapprox, prec, preccurlyeq, Precedes, PrecedesEqual, PrecedesSlantEqual, PrecedesTilde, preceq, precnapprox, precneqq, precnsim, pre, prE, precsim, prime, Prime, primes, prnap, prnE, prnsim, prod, Product, profalar, profline, profsurf, prop, Proportional, Proportion, propto, prsim, prurel, Pscr, pscr, Psi, psi, puncsp, Qfr, qfr, qint, qopf, Qopf, qprime, Qscr, qscr, quaternions, quatint, quest, questeq, quot, QUOT, rAarr, race, Racute, racute, radic, raemptyv, rang, Rang, rangd, range, rangle, raquo, rarrap, rarrb, rarrbfs, rarrc, rarr, Rarr, rArr, rarrfs, rarrhk, rarrlp, rarrpl, rarrsim, Rarrtl, rarrtl, rarrw, ratail, rAtail, ratio, rationals, rbarr, rBarr, RBarr, rbbrk, rbrace, rbrack, rbrke, rbrksld, rbrkslu, Rcaron, rcaron, Rcedil, rcedil, rceil, rcub, Rcy, rcy, rdca, rdldhar, rdquo, rdquor, rdsh, real, realine, realpart, reals, Re, rect, reg, REG, ReverseElement, ReverseEquilibrium, ReverseUpEquilibrium, rfisht, rfloor, rfr, Rfr, rHar, rhard, rharu, rharul, Rho, rho, rhov, RightAngleBracket, RightArrowBar, rightarrow, RightArrow, Rightarrow, RightArrowLeftArrow, rightarrowtail, RightCeiling, RightDoubleBracket, RightDownTeeVector, RightDownVectorBar, RightDownVector, RightFloor, rightharpoondown, rightharpoonup, rightleftarrows, rightleftharpoons, rightrightarrows, rightsquigarrow, RightTeeArrow, RightTee, RightTeeVector, rightthreetimes, RightTriangleBar, RightTriangle, RightTriangleEqual, RightUpDownVector, RightUpTeeVector, RightUpVectorBar, RightUpVector, RightVectorBar, RightVector, ring, risingdotseq, rlarr, rlhar, rlm, rmoustache, rmoust, rnmid, roang, roarr, robrk, ropar, ropf, Ropf, roplus, rotimes, RoundImplies, rpar, rpargt, rppolint, rrarr, Rrightarrow, rsaquo, rscr, Rscr, rsh, Rsh, rsqb, rsquo, rsquor, rthree, rtimes, rtri, rtrie, rtrif, rtriltri, RuleDelayed, ruluhar, rx, Sacute, sacute, sbquo, scap, Scaron, scaron, Sc, sc, sccue, sce, scE, Scedil, scedil, Scirc, scirc, scnap, scnE, scnsim, scpolint, scsim, Scy, scy, sdotb, sdot, sdote, searhk, searr, seArr, searrow, sect, semi, seswar, setminus, setmn, sext, Sfr, sfr, sfrown, sharp, SHCHcy, shchcy, SHcy, shcy, ShortDownArrow, ShortLeftArrow, shortmid, shortparallel, ShortRightArrow, ShortUpArrow, shy, Sigma, sigma, sigmaf, sigmav, sim, simdot, sime, simeq, simg, simgE, siml, simlE, simne, simplus, simrarr, slarr, SmallCircle, smallsetminus, smashp, smeparsl, smid, smile, smt, smte, smtes, SOFTcy, softcy, solbar, solb, sol, Sopf, sopf, spades, spadesuit, spar, sqcap, sqcaps, sqcup, sqcups, Sqrt, sqsub, sqsube, sqsubset, sqsubseteq, sqsup, sqsupe, sqsupset, sqsupseteq, square, Square, SquareIntersection, SquareSubset, SquareSubsetEqual, SquareSuperset, SquareSupersetEqual, SquareUnion, squarf, squ, squf, srarr, Sscr, sscr, ssetmn, ssmile, sstarf, Star, star, starf, straightepsilon, straightphi, strns, sub, Sub, subdot, subE, sube, subedot, submult, subnE, subne, subplus, subrarr, subset, Subset, subseteq, subseteqq, SubsetEqual, subsetneq, subsetneqq, subsim, subsub, subsup, succapprox, succ, succcurlyeq, Succeeds, SucceedsEqual, SucceedsSlantEqual, SucceedsTilde, succeq, succnapprox, succneqq, succnsim, succsim, SuchThat, sum, Sum, sung, sup1, sup2, sup3, sup, Sup, supdot, supdsub, supE, supe, supedot, Superset, SupersetEqual, suphsol, suphsub, suplarr, supmult, supnE, supne, supplus, supset, Supset, supseteq, supseteqq, supsetneq, supsetneqq, supsim, supsub, supsup, swarhk, swarr, swArr, swarrow, swnwar, szlig, Tab, target, Tau, tau, tbrk, Tcaron, tcaron, Tcedil, tcedil, Tcy, tcy, tdot, telrec, Tfr, tfr, there4, therefore, Therefore, Theta, theta, thetasym, thetav, thickapprox, thicksim, ThickSpace, ThinSpace, thinsp, thkap, thksim, THORN, thorn, tilde, Tilde, TildeEqual, TildeFullEqual, TildeTilde, timesbar, timesb, times, timesd, tint, toea, topbot, topcir, top, Topf, topf, topfork, tosa, tprime, trade, TRADE, triangle, triangledown, triangleleft, trianglelefteq, triangleq, triangleright, trianglerighteq, tridot, trie, triminus, TripleDot, triplus, trisb, tritime, trpezium, Tscr, tscr, TScy, tscy, TSHcy, tshcy, Tstrok, tstrok, twixt, twoheadleftarrow, twoheadrightarrow, Uacute, uacute, uarr, Uarr, uArr, Uarrocir, Ubrcy, ubrcy, Ubreve, ubreve, Ucirc, ucirc, Ucy, ucy, udarr, Udblac, udblac, udhar, ufisht, Ufr, ufr, Ugrave, ugrave, uHar, uharl, uharr, uhblk, ulcorn, ulcorner, ulcrop, ultri, Umacr, umacr, uml, UnderBar, UnderBrace, UnderBracket, UnderParenthesis, Union, UnionPlus, Uogon, uogon, Uopf, uopf, UpArrowBar, uparrow, UpArrow, Uparrow, UpArrowDownArrow, updownarrow, UpDownArrow, Updownarrow, UpEquilibrium, upharpoonleft, upharpoonright, uplus, UpperLeftArrow, UpperRightArrow, upsi, Upsi, upsih, Upsilon, upsilon, UpTeeArrow, UpTee, upuparrows, urcorn, urcorner, urcrop, Uring, uring, urtri, Uscr, uscr, utdot, Utilde, utilde, utri, utrif, uuarr, Uuml, uuml, uwangle, vangrt, varepsilon, varkappa, varnothing, varphi, varpi, varpropto, varr, vArr, varrho, varsigma, varsubsetneq, varsubsetneqq, varsupsetneq, varsupsetneqq, vartheta, vartriangleleft, vartriangleright, vBar, Vbar, vBarv, Vcy, vcy, vdash, vDash, Vdash, VDash, Vdashl, veebar, vee, Vee, veeeq, vellip, verbar, Verbar, vert, Vert, VerticalBar, VerticalLine, VerticalSeparator, VerticalTilde, VeryThinSpace, Vfr, vfr, vltri, vnsub, vnsup, Vopf, vopf, vprop, vrtri, Vscr, vscr, vsubnE, vsubne, vsupnE, vsupne, Vvdash, vzigzag, Wcirc, wcirc, wedbar, wedge, Wedge, wedgeq, weierp, Wfr, wfr, Wopf, wopf, wp, wr, wreath, Wscr, wscr, xcap, xcirc, xcup, xdtri, Xfr, xfr, xharr, xhArr, Xi, xi, xlarr, xlArr, xmap, xnis, xodot, Xopf, xopf, xoplus, xotime, xrarr, xrArr, Xscr, xscr, xsqcup, xuplus, xutri, xvee, xwedge, Yacute, yacute, YAcy, yacy, Ycirc, ycirc, Ycy, ycy, yen, Yfr, yfr, YIcy, yicy, Yopf, yopf, Yscr, yscr, YUcy, yucy, yuml, Yuml, Zacute, zacute, Zcaron, zcaron, Zcy, zcy, Zdot, zdot, zeetrf, ZeroWidthSpace, Zeta, zeta, zfr, Zfr, ZHcy, zhcy, zigrarr, zopf, Zopf, Zscr, zscr, zwj, zwnj, default */
/***/ (function(module) {

module.exports = JSON.parse("{\"Aacute\":\"Á\",\"aacute\":\"á\",\"Abreve\":\"Ă\",\"abreve\":\"ă\",\"ac\":\"∾\",\"acd\":\"∿\",\"acE\":\"∾̳\",\"Acirc\":\"Â\",\"acirc\":\"â\",\"acute\":\"´\",\"Acy\":\"А\",\"acy\":\"а\",\"AElig\":\"Æ\",\"aelig\":\"æ\",\"af\":\"⁡\",\"Afr\":\"𝔄\",\"afr\":\"𝔞\",\"Agrave\":\"À\",\"agrave\":\"à\",\"alefsym\":\"ℵ\",\"aleph\":\"ℵ\",\"Alpha\":\"Α\",\"alpha\":\"α\",\"Amacr\":\"Ā\",\"amacr\":\"ā\",\"amalg\":\"⨿\",\"amp\":\"&\",\"AMP\":\"&\",\"andand\":\"⩕\",\"And\":\"⩓\",\"and\":\"∧\",\"andd\":\"⩜\",\"andslope\":\"⩘\",\"andv\":\"⩚\",\"ang\":\"∠\",\"ange\":\"⦤\",\"angle\":\"∠\",\"angmsdaa\":\"⦨\",\"angmsdab\":\"⦩\",\"angmsdac\":\"⦪\",\"angmsdad\":\"⦫\",\"angmsdae\":\"⦬\",\"angmsdaf\":\"⦭\",\"angmsdag\":\"⦮\",\"angmsdah\":\"⦯\",\"angmsd\":\"∡\",\"angrt\":\"∟\",\"angrtvb\":\"⊾\",\"angrtvbd\":\"⦝\",\"angsph\":\"∢\",\"angst\":\"Å\",\"angzarr\":\"⍼\",\"Aogon\":\"Ą\",\"aogon\":\"ą\",\"Aopf\":\"𝔸\",\"aopf\":\"𝕒\",\"apacir\":\"⩯\",\"ap\":\"≈\",\"apE\":\"⩰\",\"ape\":\"≊\",\"apid\":\"≋\",\"apos\":\"'\",\"ApplyFunction\":\"⁡\",\"approx\":\"≈\",\"approxeq\":\"≊\",\"Aring\":\"Å\",\"aring\":\"å\",\"Ascr\":\"𝒜\",\"ascr\":\"𝒶\",\"Assign\":\"≔\",\"ast\":\"*\",\"asymp\":\"≈\",\"asympeq\":\"≍\",\"Atilde\":\"Ã\",\"atilde\":\"ã\",\"Auml\":\"Ä\",\"auml\":\"ä\",\"awconint\":\"∳\",\"awint\":\"⨑\",\"backcong\":\"≌\",\"backepsilon\":\"϶\",\"backprime\":\"‵\",\"backsim\":\"∽\",\"backsimeq\":\"⋍\",\"Backslash\":\"∖\",\"Barv\":\"⫧\",\"barvee\":\"⊽\",\"barwed\":\"⌅\",\"Barwed\":\"⌆\",\"barwedge\":\"⌅\",\"bbrk\":\"⎵\",\"bbrktbrk\":\"⎶\",\"bcong\":\"≌\",\"Bcy\":\"Б\",\"bcy\":\"б\",\"bdquo\":\"„\",\"becaus\":\"∵\",\"because\":\"∵\",\"Because\":\"∵\",\"bemptyv\":\"⦰\",\"bepsi\":\"϶\",\"bernou\":\"ℬ\",\"Bernoullis\":\"ℬ\",\"Beta\":\"Β\",\"beta\":\"β\",\"beth\":\"ℶ\",\"between\":\"≬\",\"Bfr\":\"𝔅\",\"bfr\":\"𝔟\",\"bigcap\":\"⋂\",\"bigcirc\":\"◯\",\"bigcup\":\"⋃\",\"bigodot\":\"⨀\",\"bigoplus\":\"⨁\",\"bigotimes\":\"⨂\",\"bigsqcup\":\"⨆\",\"bigstar\":\"★\",\"bigtriangledown\":\"▽\",\"bigtriangleup\":\"△\",\"biguplus\":\"⨄\",\"bigvee\":\"⋁\",\"bigwedge\":\"⋀\",\"bkarow\":\"⤍\",\"blacklozenge\":\"⧫\",\"blacksquare\":\"▪\",\"blacktriangle\":\"▴\",\"blacktriangledown\":\"▾\",\"blacktriangleleft\":\"◂\",\"blacktriangleright\":\"▸\",\"blank\":\"␣\",\"blk12\":\"▒\",\"blk14\":\"░\",\"blk34\":\"▓\",\"block\":\"█\",\"bne\":\"=⃥\",\"bnequiv\":\"≡⃥\",\"bNot\":\"⫭\",\"bnot\":\"⌐\",\"Bopf\":\"𝔹\",\"bopf\":\"𝕓\",\"bot\":\"⊥\",\"bottom\":\"⊥\",\"bowtie\":\"⋈\",\"boxbox\":\"⧉\",\"boxdl\":\"┐\",\"boxdL\":\"╕\",\"boxDl\":\"╖\",\"boxDL\":\"╗\",\"boxdr\":\"┌\",\"boxdR\":\"╒\",\"boxDr\":\"╓\",\"boxDR\":\"╔\",\"boxh\":\"─\",\"boxH\":\"═\",\"boxhd\":\"┬\",\"boxHd\":\"╤\",\"boxhD\":\"╥\",\"boxHD\":\"╦\",\"boxhu\":\"┴\",\"boxHu\":\"╧\",\"boxhU\":\"╨\",\"boxHU\":\"╩\",\"boxminus\":\"⊟\",\"boxplus\":\"⊞\",\"boxtimes\":\"⊠\",\"boxul\":\"┘\",\"boxuL\":\"╛\",\"boxUl\":\"╜\",\"boxUL\":\"╝\",\"boxur\":\"└\",\"boxuR\":\"╘\",\"boxUr\":\"╙\",\"boxUR\":\"╚\",\"boxv\":\"│\",\"boxV\":\"║\",\"boxvh\":\"┼\",\"boxvH\":\"╪\",\"boxVh\":\"╫\",\"boxVH\":\"╬\",\"boxvl\":\"┤\",\"boxvL\":\"╡\",\"boxVl\":\"╢\",\"boxVL\":\"╣\",\"boxvr\":\"├\",\"boxvR\":\"╞\",\"boxVr\":\"╟\",\"boxVR\":\"╠\",\"bprime\":\"‵\",\"breve\":\"˘\",\"Breve\":\"˘\",\"brvbar\":\"¦\",\"bscr\":\"𝒷\",\"Bscr\":\"ℬ\",\"bsemi\":\"⁏\",\"bsim\":\"∽\",\"bsime\":\"⋍\",\"bsolb\":\"⧅\",\"bsol\":\"\\\\\",\"bsolhsub\":\"⟈\",\"bull\":\"•\",\"bullet\":\"•\",\"bump\":\"≎\",\"bumpE\":\"⪮\",\"bumpe\":\"≏\",\"Bumpeq\":\"≎\",\"bumpeq\":\"≏\",\"Cacute\":\"Ć\",\"cacute\":\"ć\",\"capand\":\"⩄\",\"capbrcup\":\"⩉\",\"capcap\":\"⩋\",\"cap\":\"∩\",\"Cap\":\"⋒\",\"capcup\":\"⩇\",\"capdot\":\"⩀\",\"CapitalDifferentialD\":\"ⅅ\",\"caps\":\"∩︀\",\"caret\":\"⁁\",\"caron\":\"ˇ\",\"Cayleys\":\"ℭ\",\"ccaps\":\"⩍\",\"Ccaron\":\"Č\",\"ccaron\":\"č\",\"Ccedil\":\"Ç\",\"ccedil\":\"ç\",\"Ccirc\":\"Ĉ\",\"ccirc\":\"ĉ\",\"Cconint\":\"∰\",\"ccups\":\"⩌\",\"ccupssm\":\"⩐\",\"Cdot\":\"Ċ\",\"cdot\":\"ċ\",\"cedil\":\"¸\",\"Cedilla\":\"¸\",\"cemptyv\":\"⦲\",\"cent\":\"¢\",\"centerdot\":\"·\",\"CenterDot\":\"·\",\"cfr\":\"𝔠\",\"Cfr\":\"ℭ\",\"CHcy\":\"Ч\",\"chcy\":\"ч\",\"check\":\"✓\",\"checkmark\":\"✓\",\"Chi\":\"Χ\",\"chi\":\"χ\",\"circ\":\"ˆ\",\"circeq\":\"≗\",\"circlearrowleft\":\"↺\",\"circlearrowright\":\"↻\",\"circledast\":\"⊛\",\"circledcirc\":\"⊚\",\"circleddash\":\"⊝\",\"CircleDot\":\"⊙\",\"circledR\":\"®\",\"circledS\":\"Ⓢ\",\"CircleMinus\":\"⊖\",\"CirclePlus\":\"⊕\",\"CircleTimes\":\"⊗\",\"cir\":\"○\",\"cirE\":\"⧃\",\"cire\":\"≗\",\"cirfnint\":\"⨐\",\"cirmid\":\"⫯\",\"cirscir\":\"⧂\",\"ClockwiseContourIntegral\":\"∲\",\"CloseCurlyDoubleQuote\":\"”\",\"CloseCurlyQuote\":\"’\",\"clubs\":\"♣\",\"clubsuit\":\"♣\",\"colon\":\":\",\"Colon\":\"∷\",\"Colone\":\"⩴\",\"colone\":\"≔\",\"coloneq\":\"≔\",\"comma\":\",\",\"commat\":\"@\",\"comp\":\"∁\",\"compfn\":\"∘\",\"complement\":\"∁\",\"complexes\":\"ℂ\",\"cong\":\"≅\",\"congdot\":\"⩭\",\"Congruent\":\"≡\",\"conint\":\"∮\",\"Conint\":\"∯\",\"ContourIntegral\":\"∮\",\"copf\":\"𝕔\",\"Copf\":\"ℂ\",\"coprod\":\"∐\",\"Coproduct\":\"∐\",\"copy\":\"©\",\"COPY\":\"©\",\"copysr\":\"℗\",\"CounterClockwiseContourIntegral\":\"∳\",\"crarr\":\"↵\",\"cross\":\"✗\",\"Cross\":\"⨯\",\"Cscr\":\"𝒞\",\"cscr\":\"𝒸\",\"csub\":\"⫏\",\"csube\":\"⫑\",\"csup\":\"⫐\",\"csupe\":\"⫒\",\"ctdot\":\"⋯\",\"cudarrl\":\"⤸\",\"cudarrr\":\"⤵\",\"cuepr\":\"⋞\",\"cuesc\":\"⋟\",\"cularr\":\"↶\",\"cularrp\":\"⤽\",\"cupbrcap\":\"⩈\",\"cupcap\":\"⩆\",\"CupCap\":\"≍\",\"cup\":\"∪\",\"Cup\":\"⋓\",\"cupcup\":\"⩊\",\"cupdot\":\"⊍\",\"cupor\":\"⩅\",\"cups\":\"∪︀\",\"curarr\":\"↷\",\"curarrm\":\"⤼\",\"curlyeqprec\":\"⋞\",\"curlyeqsucc\":\"⋟\",\"curlyvee\":\"⋎\",\"curlywedge\":\"⋏\",\"curren\":\"¤\",\"curvearrowleft\":\"↶\",\"curvearrowright\":\"↷\",\"cuvee\":\"⋎\",\"cuwed\":\"⋏\",\"cwconint\":\"∲\",\"cwint\":\"∱\",\"cylcty\":\"⌭\",\"dagger\":\"†\",\"Dagger\":\"‡\",\"daleth\":\"ℸ\",\"darr\":\"↓\",\"Darr\":\"↡\",\"dArr\":\"⇓\",\"dash\":\"‐\",\"Dashv\":\"⫤\",\"dashv\":\"⊣\",\"dbkarow\":\"⤏\",\"dblac\":\"˝\",\"Dcaron\":\"Ď\",\"dcaron\":\"ď\",\"Dcy\":\"Д\",\"dcy\":\"д\",\"ddagger\":\"‡\",\"ddarr\":\"⇊\",\"DD\":\"ⅅ\",\"dd\":\"ⅆ\",\"DDotrahd\":\"⤑\",\"ddotseq\":\"⩷\",\"deg\":\"°\",\"Del\":\"∇\",\"Delta\":\"Δ\",\"delta\":\"δ\",\"demptyv\":\"⦱\",\"dfisht\":\"⥿\",\"Dfr\":\"𝔇\",\"dfr\":\"𝔡\",\"dHar\":\"⥥\",\"dharl\":\"⇃\",\"dharr\":\"⇂\",\"DiacriticalAcute\":\"´\",\"DiacriticalDot\":\"˙\",\"DiacriticalDoubleAcute\":\"˝\",\"DiacriticalGrave\":\"`\",\"DiacriticalTilde\":\"˜\",\"diam\":\"⋄\",\"diamond\":\"⋄\",\"Diamond\":\"⋄\",\"diamondsuit\":\"♦\",\"diams\":\"♦\",\"die\":\"¨\",\"DifferentialD\":\"ⅆ\",\"digamma\":\"ϝ\",\"disin\":\"⋲\",\"div\":\"÷\",\"divide\":\"÷\",\"divideontimes\":\"⋇\",\"divonx\":\"⋇\",\"DJcy\":\"Ђ\",\"djcy\":\"ђ\",\"dlcorn\":\"⌞\",\"dlcrop\":\"⌍\",\"dollar\":\"$\",\"Dopf\":\"𝔻\",\"dopf\":\"𝕕\",\"Dot\":\"¨\",\"dot\":\"˙\",\"DotDot\":\"⃜\",\"doteq\":\"≐\",\"doteqdot\":\"≑\",\"DotEqual\":\"≐\",\"dotminus\":\"∸\",\"dotplus\":\"∔\",\"dotsquare\":\"⊡\",\"doublebarwedge\":\"⌆\",\"DoubleContourIntegral\":\"∯\",\"DoubleDot\":\"¨\",\"DoubleDownArrow\":\"⇓\",\"DoubleLeftArrow\":\"⇐\",\"DoubleLeftRightArrow\":\"⇔\",\"DoubleLeftTee\":\"⫤\",\"DoubleLongLeftArrow\":\"⟸\",\"DoubleLongLeftRightArrow\":\"⟺\",\"DoubleLongRightArrow\":\"⟹\",\"DoubleRightArrow\":\"⇒\",\"DoubleRightTee\":\"⊨\",\"DoubleUpArrow\":\"⇑\",\"DoubleUpDownArrow\":\"⇕\",\"DoubleVerticalBar\":\"∥\",\"DownArrowBar\":\"⤓\",\"downarrow\":\"↓\",\"DownArrow\":\"↓\",\"Downarrow\":\"⇓\",\"DownArrowUpArrow\":\"⇵\",\"DownBreve\":\"̑\",\"downdownarrows\":\"⇊\",\"downharpoonleft\":\"⇃\",\"downharpoonright\":\"⇂\",\"DownLeftRightVector\":\"⥐\",\"DownLeftTeeVector\":\"⥞\",\"DownLeftVectorBar\":\"⥖\",\"DownLeftVector\":\"↽\",\"DownRightTeeVector\":\"⥟\",\"DownRightVectorBar\":\"⥗\",\"DownRightVector\":\"⇁\",\"DownTeeArrow\":\"↧\",\"DownTee\":\"⊤\",\"drbkarow\":\"⤐\",\"drcorn\":\"⌟\",\"drcrop\":\"⌌\",\"Dscr\":\"𝒟\",\"dscr\":\"𝒹\",\"DScy\":\"Ѕ\",\"dscy\":\"ѕ\",\"dsol\":\"⧶\",\"Dstrok\":\"Đ\",\"dstrok\":\"đ\",\"dtdot\":\"⋱\",\"dtri\":\"▿\",\"dtrif\":\"▾\",\"duarr\":\"⇵\",\"duhar\":\"⥯\",\"dwangle\":\"⦦\",\"DZcy\":\"Џ\",\"dzcy\":\"џ\",\"dzigrarr\":\"⟿\",\"Eacute\":\"É\",\"eacute\":\"é\",\"easter\":\"⩮\",\"Ecaron\":\"Ě\",\"ecaron\":\"ě\",\"Ecirc\":\"Ê\",\"ecirc\":\"ê\",\"ecir\":\"≖\",\"ecolon\":\"≕\",\"Ecy\":\"Э\",\"ecy\":\"э\",\"eDDot\":\"⩷\",\"Edot\":\"Ė\",\"edot\":\"ė\",\"eDot\":\"≑\",\"ee\":\"ⅇ\",\"efDot\":\"≒\",\"Efr\":\"𝔈\",\"efr\":\"𝔢\",\"eg\":\"⪚\",\"Egrave\":\"È\",\"egrave\":\"è\",\"egs\":\"⪖\",\"egsdot\":\"⪘\",\"el\":\"⪙\",\"Element\":\"∈\",\"elinters\":\"⏧\",\"ell\":\"ℓ\",\"els\":\"⪕\",\"elsdot\":\"⪗\",\"Emacr\":\"Ē\",\"emacr\":\"ē\",\"empty\":\"∅\",\"emptyset\":\"∅\",\"EmptySmallSquare\":\"◻\",\"emptyv\":\"∅\",\"EmptyVerySmallSquare\":\"▫\",\"emsp13\":\" \",\"emsp14\":\" \",\"emsp\":\" \",\"ENG\":\"Ŋ\",\"eng\":\"ŋ\",\"ensp\":\" \",\"Eogon\":\"Ę\",\"eogon\":\"ę\",\"Eopf\":\"𝔼\",\"eopf\":\"𝕖\",\"epar\":\"⋕\",\"eparsl\":\"⧣\",\"eplus\":\"⩱\",\"epsi\":\"ε\",\"Epsilon\":\"Ε\",\"epsilon\":\"ε\",\"epsiv\":\"ϵ\",\"eqcirc\":\"≖\",\"eqcolon\":\"≕\",\"eqsim\":\"≂\",\"eqslantgtr\":\"⪖\",\"eqslantless\":\"⪕\",\"Equal\":\"⩵\",\"equals\":\"=\",\"EqualTilde\":\"≂\",\"equest\":\"≟\",\"Equilibrium\":\"⇌\",\"equiv\":\"≡\",\"equivDD\":\"⩸\",\"eqvparsl\":\"⧥\",\"erarr\":\"⥱\",\"erDot\":\"≓\",\"escr\":\"ℯ\",\"Escr\":\"ℰ\",\"esdot\":\"≐\",\"Esim\":\"⩳\",\"esim\":\"≂\",\"Eta\":\"Η\",\"eta\":\"η\",\"ETH\":\"Ð\",\"eth\":\"ð\",\"Euml\":\"Ë\",\"euml\":\"ë\",\"euro\":\"€\",\"excl\":\"!\",\"exist\":\"∃\",\"Exists\":\"∃\",\"expectation\":\"ℰ\",\"exponentiale\":\"ⅇ\",\"ExponentialE\":\"ⅇ\",\"fallingdotseq\":\"≒\",\"Fcy\":\"Ф\",\"fcy\":\"ф\",\"female\":\"♀\",\"ffilig\":\"ﬃ\",\"fflig\":\"ﬀ\",\"ffllig\":\"ﬄ\",\"Ffr\":\"𝔉\",\"ffr\":\"𝔣\",\"filig\":\"ﬁ\",\"FilledSmallSquare\":\"◼\",\"FilledVerySmallSquare\":\"▪\",\"fjlig\":\"fj\",\"flat\":\"♭\",\"fllig\":\"ﬂ\",\"fltns\":\"▱\",\"fnof\":\"ƒ\",\"Fopf\":\"𝔽\",\"fopf\":\"𝕗\",\"forall\":\"∀\",\"ForAll\":\"∀\",\"fork\":\"⋔\",\"forkv\":\"⫙\",\"Fouriertrf\":\"ℱ\",\"fpartint\":\"⨍\",\"frac12\":\"½\",\"frac13\":\"⅓\",\"frac14\":\"¼\",\"frac15\":\"⅕\",\"frac16\":\"⅙\",\"frac18\":\"⅛\",\"frac23\":\"⅔\",\"frac25\":\"⅖\",\"frac34\":\"¾\",\"frac35\":\"⅗\",\"frac38\":\"⅜\",\"frac45\":\"⅘\",\"frac56\":\"⅚\",\"frac58\":\"⅝\",\"frac78\":\"⅞\",\"frasl\":\"⁄\",\"frown\":\"⌢\",\"fscr\":\"𝒻\",\"Fscr\":\"ℱ\",\"gacute\":\"ǵ\",\"Gamma\":\"Γ\",\"gamma\":\"γ\",\"Gammad\":\"Ϝ\",\"gammad\":\"ϝ\",\"gap\":\"⪆\",\"Gbreve\":\"Ğ\",\"gbreve\":\"ğ\",\"Gcedil\":\"Ģ\",\"Gcirc\":\"Ĝ\",\"gcirc\":\"ĝ\",\"Gcy\":\"Г\",\"gcy\":\"г\",\"Gdot\":\"Ġ\",\"gdot\":\"ġ\",\"ge\":\"≥\",\"gE\":\"≧\",\"gEl\":\"⪌\",\"gel\":\"⋛\",\"geq\":\"≥\",\"geqq\":\"≧\",\"geqslant\":\"⩾\",\"gescc\":\"⪩\",\"ges\":\"⩾\",\"gesdot\":\"⪀\",\"gesdoto\":\"⪂\",\"gesdotol\":\"⪄\",\"gesl\":\"⋛︀\",\"gesles\":\"⪔\",\"Gfr\":\"𝔊\",\"gfr\":\"𝔤\",\"gg\":\"≫\",\"Gg\":\"⋙\",\"ggg\":\"⋙\",\"gimel\":\"ℷ\",\"GJcy\":\"Ѓ\",\"gjcy\":\"ѓ\",\"gla\":\"⪥\",\"gl\":\"≷\",\"glE\":\"⪒\",\"glj\":\"⪤\",\"gnap\":\"⪊\",\"gnapprox\":\"⪊\",\"gne\":\"⪈\",\"gnE\":\"≩\",\"gneq\":\"⪈\",\"gneqq\":\"≩\",\"gnsim\":\"⋧\",\"Gopf\":\"𝔾\",\"gopf\":\"𝕘\",\"grave\":\"`\",\"GreaterEqual\":\"≥\",\"GreaterEqualLess\":\"⋛\",\"GreaterFullEqual\":\"≧\",\"GreaterGreater\":\"⪢\",\"GreaterLess\":\"≷\",\"GreaterSlantEqual\":\"⩾\",\"GreaterTilde\":\"≳\",\"Gscr\":\"𝒢\",\"gscr\":\"ℊ\",\"gsim\":\"≳\",\"gsime\":\"⪎\",\"gsiml\":\"⪐\",\"gtcc\":\"⪧\",\"gtcir\":\"⩺\",\"gt\":\">\",\"GT\":\">\",\"Gt\":\"≫\",\"gtdot\":\"⋗\",\"gtlPar\":\"⦕\",\"gtquest\":\"⩼\",\"gtrapprox\":\"⪆\",\"gtrarr\":\"⥸\",\"gtrdot\":\"⋗\",\"gtreqless\":\"⋛\",\"gtreqqless\":\"⪌\",\"gtrless\":\"≷\",\"gtrsim\":\"≳\",\"gvertneqq\":\"≩︀\",\"gvnE\":\"≩︀\",\"Hacek\":\"ˇ\",\"hairsp\":\" \",\"half\":\"½\",\"hamilt\":\"ℋ\",\"HARDcy\":\"Ъ\",\"hardcy\":\"ъ\",\"harrcir\":\"⥈\",\"harr\":\"↔\",\"hArr\":\"⇔\",\"harrw\":\"↭\",\"Hat\":\"^\",\"hbar\":\"ℏ\",\"Hcirc\":\"Ĥ\",\"hcirc\":\"ĥ\",\"hearts\":\"♥\",\"heartsuit\":\"♥\",\"hellip\":\"…\",\"hercon\":\"⊹\",\"hfr\":\"𝔥\",\"Hfr\":\"ℌ\",\"HilbertSpace\":\"ℋ\",\"hksearow\":\"⤥\",\"hkswarow\":\"⤦\",\"hoarr\":\"⇿\",\"homtht\":\"∻\",\"hookleftarrow\":\"↩\",\"hookrightarrow\":\"↪\",\"hopf\":\"𝕙\",\"Hopf\":\"ℍ\",\"horbar\":\"―\",\"HorizontalLine\":\"─\",\"hscr\":\"𝒽\",\"Hscr\":\"ℋ\",\"hslash\":\"ℏ\",\"Hstrok\":\"Ħ\",\"hstrok\":\"ħ\",\"HumpDownHump\":\"≎\",\"HumpEqual\":\"≏\",\"hybull\":\"⁃\",\"hyphen\":\"‐\",\"Iacute\":\"Í\",\"iacute\":\"í\",\"ic\":\"⁣\",\"Icirc\":\"Î\",\"icirc\":\"î\",\"Icy\":\"И\",\"icy\":\"и\",\"Idot\":\"İ\",\"IEcy\":\"Е\",\"iecy\":\"е\",\"iexcl\":\"¡\",\"iff\":\"⇔\",\"ifr\":\"𝔦\",\"Ifr\":\"ℑ\",\"Igrave\":\"Ì\",\"igrave\":\"ì\",\"ii\":\"ⅈ\",\"iiiint\":\"⨌\",\"iiint\":\"∭\",\"iinfin\":\"⧜\",\"iiota\":\"℩\",\"IJlig\":\"Ĳ\",\"ijlig\":\"ĳ\",\"Imacr\":\"Ī\",\"imacr\":\"ī\",\"image\":\"ℑ\",\"ImaginaryI\":\"ⅈ\",\"imagline\":\"ℐ\",\"imagpart\":\"ℑ\",\"imath\":\"ı\",\"Im\":\"ℑ\",\"imof\":\"⊷\",\"imped\":\"Ƶ\",\"Implies\":\"⇒\",\"incare\":\"℅\",\"in\":\"∈\",\"infin\":\"∞\",\"infintie\":\"⧝\",\"inodot\":\"ı\",\"intcal\":\"⊺\",\"int\":\"∫\",\"Int\":\"∬\",\"integers\":\"ℤ\",\"Integral\":\"∫\",\"intercal\":\"⊺\",\"Intersection\":\"⋂\",\"intlarhk\":\"⨗\",\"intprod\":\"⨼\",\"InvisibleComma\":\"⁣\",\"InvisibleTimes\":\"⁢\",\"IOcy\":\"Ё\",\"iocy\":\"ё\",\"Iogon\":\"Į\",\"iogon\":\"į\",\"Iopf\":\"𝕀\",\"iopf\":\"𝕚\",\"Iota\":\"Ι\",\"iota\":\"ι\",\"iprod\":\"⨼\",\"iquest\":\"¿\",\"iscr\":\"𝒾\",\"Iscr\":\"ℐ\",\"isin\":\"∈\",\"isindot\":\"⋵\",\"isinE\":\"⋹\",\"isins\":\"⋴\",\"isinsv\":\"⋳\",\"isinv\":\"∈\",\"it\":\"⁢\",\"Itilde\":\"Ĩ\",\"itilde\":\"ĩ\",\"Iukcy\":\"І\",\"iukcy\":\"і\",\"Iuml\":\"Ï\",\"iuml\":\"ï\",\"Jcirc\":\"Ĵ\",\"jcirc\":\"ĵ\",\"Jcy\":\"Й\",\"jcy\":\"й\",\"Jfr\":\"𝔍\",\"jfr\":\"𝔧\",\"jmath\":\"ȷ\",\"Jopf\":\"𝕁\",\"jopf\":\"𝕛\",\"Jscr\":\"𝒥\",\"jscr\":\"𝒿\",\"Jsercy\":\"Ј\",\"jsercy\":\"ј\",\"Jukcy\":\"Є\",\"jukcy\":\"є\",\"Kappa\":\"Κ\",\"kappa\":\"κ\",\"kappav\":\"ϰ\",\"Kcedil\":\"Ķ\",\"kcedil\":\"ķ\",\"Kcy\":\"К\",\"kcy\":\"к\",\"Kfr\":\"𝔎\",\"kfr\":\"𝔨\",\"kgreen\":\"ĸ\",\"KHcy\":\"Х\",\"khcy\":\"х\",\"KJcy\":\"Ќ\",\"kjcy\":\"ќ\",\"Kopf\":\"𝕂\",\"kopf\":\"𝕜\",\"Kscr\":\"𝒦\",\"kscr\":\"𝓀\",\"lAarr\":\"⇚\",\"Lacute\":\"Ĺ\",\"lacute\":\"ĺ\",\"laemptyv\":\"⦴\",\"lagran\":\"ℒ\",\"Lambda\":\"Λ\",\"lambda\":\"λ\",\"lang\":\"⟨\",\"Lang\":\"⟪\",\"langd\":\"⦑\",\"langle\":\"⟨\",\"lap\":\"⪅\",\"Laplacetrf\":\"ℒ\",\"laquo\":\"«\",\"larrb\":\"⇤\",\"larrbfs\":\"⤟\",\"larr\":\"←\",\"Larr\":\"↞\",\"lArr\":\"⇐\",\"larrfs\":\"⤝\",\"larrhk\":\"↩\",\"larrlp\":\"↫\",\"larrpl\":\"⤹\",\"larrsim\":\"⥳\",\"larrtl\":\"↢\",\"latail\":\"⤙\",\"lAtail\":\"⤛\",\"lat\":\"⪫\",\"late\":\"⪭\",\"lates\":\"⪭︀\",\"lbarr\":\"⤌\",\"lBarr\":\"⤎\",\"lbbrk\":\"❲\",\"lbrace\":\"{\",\"lbrack\":\"[\",\"lbrke\":\"⦋\",\"lbrksld\":\"⦏\",\"lbrkslu\":\"⦍\",\"Lcaron\":\"Ľ\",\"lcaron\":\"ľ\",\"Lcedil\":\"Ļ\",\"lcedil\":\"ļ\",\"lceil\":\"⌈\",\"lcub\":\"{\",\"Lcy\":\"Л\",\"lcy\":\"л\",\"ldca\":\"⤶\",\"ldquo\":\"“\",\"ldquor\":\"„\",\"ldrdhar\":\"⥧\",\"ldrushar\":\"⥋\",\"ldsh\":\"↲\",\"le\":\"≤\",\"lE\":\"≦\",\"LeftAngleBracket\":\"⟨\",\"LeftArrowBar\":\"⇤\",\"leftarrow\":\"←\",\"LeftArrow\":\"←\",\"Leftarrow\":\"⇐\",\"LeftArrowRightArrow\":\"⇆\",\"leftarrowtail\":\"↢\",\"LeftCeiling\":\"⌈\",\"LeftDoubleBracket\":\"⟦\",\"LeftDownTeeVector\":\"⥡\",\"LeftDownVectorBar\":\"⥙\",\"LeftDownVector\":\"⇃\",\"LeftFloor\":\"⌊\",\"leftharpoondown\":\"↽\",\"leftharpoonup\":\"↼\",\"leftleftarrows\":\"⇇\",\"leftrightarrow\":\"↔\",\"LeftRightArrow\":\"↔\",\"Leftrightarrow\":\"⇔\",\"leftrightarrows\":\"⇆\",\"leftrightharpoons\":\"⇋\",\"leftrightsquigarrow\":\"↭\",\"LeftRightVector\":\"⥎\",\"LeftTeeArrow\":\"↤\",\"LeftTee\":\"⊣\",\"LeftTeeVector\":\"⥚\",\"leftthreetimes\":\"⋋\",\"LeftTriangleBar\":\"⧏\",\"LeftTriangle\":\"⊲\",\"LeftTriangleEqual\":\"⊴\",\"LeftUpDownVector\":\"⥑\",\"LeftUpTeeVector\":\"⥠\",\"LeftUpVectorBar\":\"⥘\",\"LeftUpVector\":\"↿\",\"LeftVectorBar\":\"⥒\",\"LeftVector\":\"↼\",\"lEg\":\"⪋\",\"leg\":\"⋚\",\"leq\":\"≤\",\"leqq\":\"≦\",\"leqslant\":\"⩽\",\"lescc\":\"⪨\",\"les\":\"⩽\",\"lesdot\":\"⩿\",\"lesdoto\":\"⪁\",\"lesdotor\":\"⪃\",\"lesg\":\"⋚︀\",\"lesges\":\"⪓\",\"lessapprox\":\"⪅\",\"lessdot\":\"⋖\",\"lesseqgtr\":\"⋚\",\"lesseqqgtr\":\"⪋\",\"LessEqualGreater\":\"⋚\",\"LessFullEqual\":\"≦\",\"LessGreater\":\"≶\",\"lessgtr\":\"≶\",\"LessLess\":\"⪡\",\"lesssim\":\"≲\",\"LessSlantEqual\":\"⩽\",\"LessTilde\":\"≲\",\"lfisht\":\"⥼\",\"lfloor\":\"⌊\",\"Lfr\":\"𝔏\",\"lfr\":\"𝔩\",\"lg\":\"≶\",\"lgE\":\"⪑\",\"lHar\":\"⥢\",\"lhard\":\"↽\",\"lharu\":\"↼\",\"lharul\":\"⥪\",\"lhblk\":\"▄\",\"LJcy\":\"Љ\",\"ljcy\":\"љ\",\"llarr\":\"⇇\",\"ll\":\"≪\",\"Ll\":\"⋘\",\"llcorner\":\"⌞\",\"Lleftarrow\":\"⇚\",\"llhard\":\"⥫\",\"lltri\":\"◺\",\"Lmidot\":\"Ŀ\",\"lmidot\":\"ŀ\",\"lmoustache\":\"⎰\",\"lmoust\":\"⎰\",\"lnap\":\"⪉\",\"lnapprox\":\"⪉\",\"lne\":\"⪇\",\"lnE\":\"≨\",\"lneq\":\"⪇\",\"lneqq\":\"≨\",\"lnsim\":\"⋦\",\"loang\":\"⟬\",\"loarr\":\"⇽\",\"lobrk\":\"⟦\",\"longleftarrow\":\"⟵\",\"LongLeftArrow\":\"⟵\",\"Longleftarrow\":\"⟸\",\"longleftrightarrow\":\"⟷\",\"LongLeftRightArrow\":\"⟷\",\"Longleftrightarrow\":\"⟺\",\"longmapsto\":\"⟼\",\"longrightarrow\":\"⟶\",\"LongRightArrow\":\"⟶\",\"Longrightarrow\":\"⟹\",\"looparrowleft\":\"↫\",\"looparrowright\":\"↬\",\"lopar\":\"⦅\",\"Lopf\":\"𝕃\",\"lopf\":\"𝕝\",\"loplus\":\"⨭\",\"lotimes\":\"⨴\",\"lowast\":\"∗\",\"lowbar\":\"_\",\"LowerLeftArrow\":\"↙\",\"LowerRightArrow\":\"↘\",\"loz\":\"◊\",\"lozenge\":\"◊\",\"lozf\":\"⧫\",\"lpar\":\"(\",\"lparlt\":\"⦓\",\"lrarr\":\"⇆\",\"lrcorner\":\"⌟\",\"lrhar\":\"⇋\",\"lrhard\":\"⥭\",\"lrm\":\"‎\",\"lrtri\":\"⊿\",\"lsaquo\":\"‹\",\"lscr\":\"𝓁\",\"Lscr\":\"ℒ\",\"lsh\":\"↰\",\"Lsh\":\"↰\",\"lsim\":\"≲\",\"lsime\":\"⪍\",\"lsimg\":\"⪏\",\"lsqb\":\"[\",\"lsquo\":\"‘\",\"lsquor\":\"‚\",\"Lstrok\":\"Ł\",\"lstrok\":\"ł\",\"ltcc\":\"⪦\",\"ltcir\":\"⩹\",\"lt\":\"<\",\"LT\":\"<\",\"Lt\":\"≪\",\"ltdot\":\"⋖\",\"lthree\":\"⋋\",\"ltimes\":\"⋉\",\"ltlarr\":\"⥶\",\"ltquest\":\"⩻\",\"ltri\":\"◃\",\"ltrie\":\"⊴\",\"ltrif\":\"◂\",\"ltrPar\":\"⦖\",\"lurdshar\":\"⥊\",\"luruhar\":\"⥦\",\"lvertneqq\":\"≨︀\",\"lvnE\":\"≨︀\",\"macr\":\"¯\",\"male\":\"♂\",\"malt\":\"✠\",\"maltese\":\"✠\",\"Map\":\"⤅\",\"map\":\"↦\",\"mapsto\":\"↦\",\"mapstodown\":\"↧\",\"mapstoleft\":\"↤\",\"mapstoup\":\"↥\",\"marker\":\"▮\",\"mcomma\":\"⨩\",\"Mcy\":\"М\",\"mcy\":\"м\",\"mdash\":\"—\",\"mDDot\":\"∺\",\"measuredangle\":\"∡\",\"MediumSpace\":\" \",\"Mellintrf\":\"ℳ\",\"Mfr\":\"𝔐\",\"mfr\":\"𝔪\",\"mho\":\"℧\",\"micro\":\"µ\",\"midast\":\"*\",\"midcir\":\"⫰\",\"mid\":\"∣\",\"middot\":\"·\",\"minusb\":\"⊟\",\"minus\":\"−\",\"minusd\":\"∸\",\"minusdu\":\"⨪\",\"MinusPlus\":\"∓\",\"mlcp\":\"⫛\",\"mldr\":\"…\",\"mnplus\":\"∓\",\"models\":\"⊧\",\"Mopf\":\"𝕄\",\"mopf\":\"𝕞\",\"mp\":\"∓\",\"mscr\":\"𝓂\",\"Mscr\":\"ℳ\",\"mstpos\":\"∾\",\"Mu\":\"Μ\",\"mu\":\"μ\",\"multimap\":\"⊸\",\"mumap\":\"⊸\",\"nabla\":\"∇\",\"Nacute\":\"Ń\",\"nacute\":\"ń\",\"nang\":\"∠⃒\",\"nap\":\"≉\",\"napE\":\"⩰̸\",\"napid\":\"≋̸\",\"napos\":\"ŉ\",\"napprox\":\"≉\",\"natural\":\"♮\",\"naturals\":\"ℕ\",\"natur\":\"♮\",\"nbsp\":\" \",\"nbump\":\"≎̸\",\"nbumpe\":\"≏̸\",\"ncap\":\"⩃\",\"Ncaron\":\"Ň\",\"ncaron\":\"ň\",\"Ncedil\":\"Ņ\",\"ncedil\":\"ņ\",\"ncong\":\"≇\",\"ncongdot\":\"⩭̸\",\"ncup\":\"⩂\",\"Ncy\":\"Н\",\"ncy\":\"н\",\"ndash\":\"–\",\"nearhk\":\"⤤\",\"nearr\":\"↗\",\"neArr\":\"⇗\",\"nearrow\":\"↗\",\"ne\":\"≠\",\"nedot\":\"≐̸\",\"NegativeMediumSpace\":\"​\",\"NegativeThickSpace\":\"​\",\"NegativeThinSpace\":\"​\",\"NegativeVeryThinSpace\":\"​\",\"nequiv\":\"≢\",\"nesear\":\"⤨\",\"nesim\":\"≂̸\",\"NestedGreaterGreater\":\"≫\",\"NestedLessLess\":\"≪\",\"NewLine\":\"\\n\",\"nexist\":\"∄\",\"nexists\":\"∄\",\"Nfr\":\"𝔑\",\"nfr\":\"𝔫\",\"ngE\":\"≧̸\",\"nge\":\"≱\",\"ngeq\":\"≱\",\"ngeqq\":\"≧̸\",\"ngeqslant\":\"⩾̸\",\"nges\":\"⩾̸\",\"nGg\":\"⋙̸\",\"ngsim\":\"≵\",\"nGt\":\"≫⃒\",\"ngt\":\"≯\",\"ngtr\":\"≯\",\"nGtv\":\"≫̸\",\"nharr\":\"↮\",\"nhArr\":\"⇎\",\"nhpar\":\"⫲\",\"ni\":\"∋\",\"nis\":\"⋼\",\"nisd\":\"⋺\",\"niv\":\"∋\",\"NJcy\":\"Њ\",\"njcy\":\"њ\",\"nlarr\":\"↚\",\"nlArr\":\"⇍\",\"nldr\":\"‥\",\"nlE\":\"≦̸\",\"nle\":\"≰\",\"nleftarrow\":\"↚\",\"nLeftarrow\":\"⇍\",\"nleftrightarrow\":\"↮\",\"nLeftrightarrow\":\"⇎\",\"nleq\":\"≰\",\"nleqq\":\"≦̸\",\"nleqslant\":\"⩽̸\",\"nles\":\"⩽̸\",\"nless\":\"≮\",\"nLl\":\"⋘̸\",\"nlsim\":\"≴\",\"nLt\":\"≪⃒\",\"nlt\":\"≮\",\"nltri\":\"⋪\",\"nltrie\":\"⋬\",\"nLtv\":\"≪̸\",\"nmid\":\"∤\",\"NoBreak\":\"⁠\",\"NonBreakingSpace\":\" \",\"nopf\":\"𝕟\",\"Nopf\":\"ℕ\",\"Not\":\"⫬\",\"not\":\"¬\",\"NotCongruent\":\"≢\",\"NotCupCap\":\"≭\",\"NotDoubleVerticalBar\":\"∦\",\"NotElement\":\"∉\",\"NotEqual\":\"≠\",\"NotEqualTilde\":\"≂̸\",\"NotExists\":\"∄\",\"NotGreater\":\"≯\",\"NotGreaterEqual\":\"≱\",\"NotGreaterFullEqual\":\"≧̸\",\"NotGreaterGreater\":\"≫̸\",\"NotGreaterLess\":\"≹\",\"NotGreaterSlantEqual\":\"⩾̸\",\"NotGreaterTilde\":\"≵\",\"NotHumpDownHump\":\"≎̸\",\"NotHumpEqual\":\"≏̸\",\"notin\":\"∉\",\"notindot\":\"⋵̸\",\"notinE\":\"⋹̸\",\"notinva\":\"∉\",\"notinvb\":\"⋷\",\"notinvc\":\"⋶\",\"NotLeftTriangleBar\":\"⧏̸\",\"NotLeftTriangle\":\"⋪\",\"NotLeftTriangleEqual\":\"⋬\",\"NotLess\":\"≮\",\"NotLessEqual\":\"≰\",\"NotLessGreater\":\"≸\",\"NotLessLess\":\"≪̸\",\"NotLessSlantEqual\":\"⩽̸\",\"NotLessTilde\":\"≴\",\"NotNestedGreaterGreater\":\"⪢̸\",\"NotNestedLessLess\":\"⪡̸\",\"notni\":\"∌\",\"notniva\":\"∌\",\"notnivb\":\"⋾\",\"notnivc\":\"⋽\",\"NotPrecedes\":\"⊀\",\"NotPrecedesEqual\":\"⪯̸\",\"NotPrecedesSlantEqual\":\"⋠\",\"NotReverseElement\":\"∌\",\"NotRightTriangleBar\":\"⧐̸\",\"NotRightTriangle\":\"⋫\",\"NotRightTriangleEqual\":\"⋭\",\"NotSquareSubset\":\"⊏̸\",\"NotSquareSubsetEqual\":\"⋢\",\"NotSquareSuperset\":\"⊐̸\",\"NotSquareSupersetEqual\":\"⋣\",\"NotSubset\":\"⊂⃒\",\"NotSubsetEqual\":\"⊈\",\"NotSucceeds\":\"⊁\",\"NotSucceedsEqual\":\"⪰̸\",\"NotSucceedsSlantEqual\":\"⋡\",\"NotSucceedsTilde\":\"≿̸\",\"NotSuperset\":\"⊃⃒\",\"NotSupersetEqual\":\"⊉\",\"NotTilde\":\"≁\",\"NotTildeEqual\":\"≄\",\"NotTildeFullEqual\":\"≇\",\"NotTildeTilde\":\"≉\",\"NotVerticalBar\":\"∤\",\"nparallel\":\"∦\",\"npar\":\"∦\",\"nparsl\":\"⫽⃥\",\"npart\":\"∂̸\",\"npolint\":\"⨔\",\"npr\":\"⊀\",\"nprcue\":\"⋠\",\"nprec\":\"⊀\",\"npreceq\":\"⪯̸\",\"npre\":\"⪯̸\",\"nrarrc\":\"⤳̸\",\"nrarr\":\"↛\",\"nrArr\":\"⇏\",\"nrarrw\":\"↝̸\",\"nrightarrow\":\"↛\",\"nRightarrow\":\"⇏\",\"nrtri\":\"⋫\",\"nrtrie\":\"⋭\",\"nsc\":\"⊁\",\"nsccue\":\"⋡\",\"nsce\":\"⪰̸\",\"Nscr\":\"𝒩\",\"nscr\":\"𝓃\",\"nshortmid\":\"∤\",\"nshortparallel\":\"∦\",\"nsim\":\"≁\",\"nsime\":\"≄\",\"nsimeq\":\"≄\",\"nsmid\":\"∤\",\"nspar\":\"∦\",\"nsqsube\":\"⋢\",\"nsqsupe\":\"⋣\",\"nsub\":\"⊄\",\"nsubE\":\"⫅̸\",\"nsube\":\"⊈\",\"nsubset\":\"⊂⃒\",\"nsubseteq\":\"⊈\",\"nsubseteqq\":\"⫅̸\",\"nsucc\":\"⊁\",\"nsucceq\":\"⪰̸\",\"nsup\":\"⊅\",\"nsupE\":\"⫆̸\",\"nsupe\":\"⊉\",\"nsupset\":\"⊃⃒\",\"nsupseteq\":\"⊉\",\"nsupseteqq\":\"⫆̸\",\"ntgl\":\"≹\",\"Ntilde\":\"Ñ\",\"ntilde\":\"ñ\",\"ntlg\":\"≸\",\"ntriangleleft\":\"⋪\",\"ntrianglelefteq\":\"⋬\",\"ntriangleright\":\"⋫\",\"ntrianglerighteq\":\"⋭\",\"Nu\":\"Ν\",\"nu\":\"ν\",\"num\":\"#\",\"numero\":\"№\",\"numsp\":\" \",\"nvap\":\"≍⃒\",\"nvdash\":\"⊬\",\"nvDash\":\"⊭\",\"nVdash\":\"⊮\",\"nVDash\":\"⊯\",\"nvge\":\"≥⃒\",\"nvgt\":\">⃒\",\"nvHarr\":\"⤄\",\"nvinfin\":\"⧞\",\"nvlArr\":\"⤂\",\"nvle\":\"≤⃒\",\"nvlt\":\"<⃒\",\"nvltrie\":\"⊴⃒\",\"nvrArr\":\"⤃\",\"nvrtrie\":\"⊵⃒\",\"nvsim\":\"∼⃒\",\"nwarhk\":\"⤣\",\"nwarr\":\"↖\",\"nwArr\":\"⇖\",\"nwarrow\":\"↖\",\"nwnear\":\"⤧\",\"Oacute\":\"Ó\",\"oacute\":\"ó\",\"oast\":\"⊛\",\"Ocirc\":\"Ô\",\"ocirc\":\"ô\",\"ocir\":\"⊚\",\"Ocy\":\"О\",\"ocy\":\"о\",\"odash\":\"⊝\",\"Odblac\":\"Ő\",\"odblac\":\"ő\",\"odiv\":\"⨸\",\"odot\":\"⊙\",\"odsold\":\"⦼\",\"OElig\":\"Œ\",\"oelig\":\"œ\",\"ofcir\":\"⦿\",\"Ofr\":\"𝔒\",\"ofr\":\"𝔬\",\"ogon\":\"˛\",\"Ograve\":\"Ò\",\"ograve\":\"ò\",\"ogt\":\"⧁\",\"ohbar\":\"⦵\",\"ohm\":\"Ω\",\"oint\":\"∮\",\"olarr\":\"↺\",\"olcir\":\"⦾\",\"olcross\":\"⦻\",\"oline\":\"‾\",\"olt\":\"⧀\",\"Omacr\":\"Ō\",\"omacr\":\"ō\",\"Omega\":\"Ω\",\"omega\":\"ω\",\"Omicron\":\"Ο\",\"omicron\":\"ο\",\"omid\":\"⦶\",\"ominus\":\"⊖\",\"Oopf\":\"𝕆\",\"oopf\":\"𝕠\",\"opar\":\"⦷\",\"OpenCurlyDoubleQuote\":\"“\",\"OpenCurlyQuote\":\"‘\",\"operp\":\"⦹\",\"oplus\":\"⊕\",\"orarr\":\"↻\",\"Or\":\"⩔\",\"or\":\"∨\",\"ord\":\"⩝\",\"order\":\"ℴ\",\"orderof\":\"ℴ\",\"ordf\":\"ª\",\"ordm\":\"º\",\"origof\":\"⊶\",\"oror\":\"⩖\",\"orslope\":\"⩗\",\"orv\":\"⩛\",\"oS\":\"Ⓢ\",\"Oscr\":\"𝒪\",\"oscr\":\"ℴ\",\"Oslash\":\"Ø\",\"oslash\":\"ø\",\"osol\":\"⊘\",\"Otilde\":\"Õ\",\"otilde\":\"õ\",\"otimesas\":\"⨶\",\"Otimes\":\"⨷\",\"otimes\":\"⊗\",\"Ouml\":\"Ö\",\"ouml\":\"ö\",\"ovbar\":\"⌽\",\"OverBar\":\"‾\",\"OverBrace\":\"⏞\",\"OverBracket\":\"⎴\",\"OverParenthesis\":\"⏜\",\"para\":\"¶\",\"parallel\":\"∥\",\"par\":\"∥\",\"parsim\":\"⫳\",\"parsl\":\"⫽\",\"part\":\"∂\",\"PartialD\":\"∂\",\"Pcy\":\"П\",\"pcy\":\"п\",\"percnt\":\"%\",\"period\":\".\",\"permil\":\"‰\",\"perp\":\"⊥\",\"pertenk\":\"‱\",\"Pfr\":\"𝔓\",\"pfr\":\"𝔭\",\"Phi\":\"Φ\",\"phi\":\"φ\",\"phiv\":\"ϕ\",\"phmmat\":\"ℳ\",\"phone\":\"☎\",\"Pi\":\"Π\",\"pi\":\"π\",\"pitchfork\":\"⋔\",\"piv\":\"ϖ\",\"planck\":\"ℏ\",\"planckh\":\"ℎ\",\"plankv\":\"ℏ\",\"plusacir\":\"⨣\",\"plusb\":\"⊞\",\"pluscir\":\"⨢\",\"plus\":\"+\",\"plusdo\":\"∔\",\"plusdu\":\"⨥\",\"pluse\":\"⩲\",\"PlusMinus\":\"±\",\"plusmn\":\"±\",\"plussim\":\"⨦\",\"plustwo\":\"⨧\",\"pm\":\"±\",\"Poincareplane\":\"ℌ\",\"pointint\":\"⨕\",\"popf\":\"𝕡\",\"Popf\":\"ℙ\",\"pound\":\"£\",\"prap\":\"⪷\",\"Pr\":\"⪻\",\"pr\":\"≺\",\"prcue\":\"≼\",\"precapprox\":\"⪷\",\"prec\":\"≺\",\"preccurlyeq\":\"≼\",\"Precedes\":\"≺\",\"PrecedesEqual\":\"⪯\",\"PrecedesSlantEqual\":\"≼\",\"PrecedesTilde\":\"≾\",\"preceq\":\"⪯\",\"precnapprox\":\"⪹\",\"precneqq\":\"⪵\",\"precnsim\":\"⋨\",\"pre\":\"⪯\",\"prE\":\"⪳\",\"precsim\":\"≾\",\"prime\":\"′\",\"Prime\":\"″\",\"primes\":\"ℙ\",\"prnap\":\"⪹\",\"prnE\":\"⪵\",\"prnsim\":\"⋨\",\"prod\":\"∏\",\"Product\":\"∏\",\"profalar\":\"⌮\",\"profline\":\"⌒\",\"profsurf\":\"⌓\",\"prop\":\"∝\",\"Proportional\":\"∝\",\"Proportion\":\"∷\",\"propto\":\"∝\",\"prsim\":\"≾\",\"prurel\":\"⊰\",\"Pscr\":\"𝒫\",\"pscr\":\"𝓅\",\"Psi\":\"Ψ\",\"psi\":\"ψ\",\"puncsp\":\" \",\"Qfr\":\"𝔔\",\"qfr\":\"𝔮\",\"qint\":\"⨌\",\"qopf\":\"𝕢\",\"Qopf\":\"ℚ\",\"qprime\":\"⁗\",\"Qscr\":\"𝒬\",\"qscr\":\"𝓆\",\"quaternions\":\"ℍ\",\"quatint\":\"⨖\",\"quest\":\"?\",\"questeq\":\"≟\",\"quot\":\"\\\"\",\"QUOT\":\"\\\"\",\"rAarr\":\"⇛\",\"race\":\"∽̱\",\"Racute\":\"Ŕ\",\"racute\":\"ŕ\",\"radic\":\"√\",\"raemptyv\":\"⦳\",\"rang\":\"⟩\",\"Rang\":\"⟫\",\"rangd\":\"⦒\",\"range\":\"⦥\",\"rangle\":\"⟩\",\"raquo\":\"»\",\"rarrap\":\"⥵\",\"rarrb\":\"⇥\",\"rarrbfs\":\"⤠\",\"rarrc\":\"⤳\",\"rarr\":\"→\",\"Rarr\":\"↠\",\"rArr\":\"⇒\",\"rarrfs\":\"⤞\",\"rarrhk\":\"↪\",\"rarrlp\":\"↬\",\"rarrpl\":\"⥅\",\"rarrsim\":\"⥴\",\"Rarrtl\":\"⤖\",\"rarrtl\":\"↣\",\"rarrw\":\"↝\",\"ratail\":\"⤚\",\"rAtail\":\"⤜\",\"ratio\":\"∶\",\"rationals\":\"ℚ\",\"rbarr\":\"⤍\",\"rBarr\":\"⤏\",\"RBarr\":\"⤐\",\"rbbrk\":\"❳\",\"rbrace\":\"}\",\"rbrack\":\"]\",\"rbrke\":\"⦌\",\"rbrksld\":\"⦎\",\"rbrkslu\":\"⦐\",\"Rcaron\":\"Ř\",\"rcaron\":\"ř\",\"Rcedil\":\"Ŗ\",\"rcedil\":\"ŗ\",\"rceil\":\"⌉\",\"rcub\":\"}\",\"Rcy\":\"Р\",\"rcy\":\"р\",\"rdca\":\"⤷\",\"rdldhar\":\"⥩\",\"rdquo\":\"”\",\"rdquor\":\"”\",\"rdsh\":\"↳\",\"real\":\"ℜ\",\"realine\":\"ℛ\",\"realpart\":\"ℜ\",\"reals\":\"ℝ\",\"Re\":\"ℜ\",\"rect\":\"▭\",\"reg\":\"®\",\"REG\":\"®\",\"ReverseElement\":\"∋\",\"ReverseEquilibrium\":\"⇋\",\"ReverseUpEquilibrium\":\"⥯\",\"rfisht\":\"⥽\",\"rfloor\":\"⌋\",\"rfr\":\"𝔯\",\"Rfr\":\"ℜ\",\"rHar\":\"⥤\",\"rhard\":\"⇁\",\"rharu\":\"⇀\",\"rharul\":\"⥬\",\"Rho\":\"Ρ\",\"rho\":\"ρ\",\"rhov\":\"ϱ\",\"RightAngleBracket\":\"⟩\",\"RightArrowBar\":\"⇥\",\"rightarrow\":\"→\",\"RightArrow\":\"→\",\"Rightarrow\":\"⇒\",\"RightArrowLeftArrow\":\"⇄\",\"rightarrowtail\":\"↣\",\"RightCeiling\":\"⌉\",\"RightDoubleBracket\":\"⟧\",\"RightDownTeeVector\":\"⥝\",\"RightDownVectorBar\":\"⥕\",\"RightDownVector\":\"⇂\",\"RightFloor\":\"⌋\",\"rightharpoondown\":\"⇁\",\"rightharpoonup\":\"⇀\",\"rightleftarrows\":\"⇄\",\"rightleftharpoons\":\"⇌\",\"rightrightarrows\":\"⇉\",\"rightsquigarrow\":\"↝\",\"RightTeeArrow\":\"↦\",\"RightTee\":\"⊢\",\"RightTeeVector\":\"⥛\",\"rightthreetimes\":\"⋌\",\"RightTriangleBar\":\"⧐\",\"RightTriangle\":\"⊳\",\"RightTriangleEqual\":\"⊵\",\"RightUpDownVector\":\"⥏\",\"RightUpTeeVector\":\"⥜\",\"RightUpVectorBar\":\"⥔\",\"RightUpVector\":\"↾\",\"RightVectorBar\":\"⥓\",\"RightVector\":\"⇀\",\"ring\":\"˚\",\"risingdotseq\":\"≓\",\"rlarr\":\"⇄\",\"rlhar\":\"⇌\",\"rlm\":\"‏\",\"rmoustache\":\"⎱\",\"rmoust\":\"⎱\",\"rnmid\":\"⫮\",\"roang\":\"⟭\",\"roarr\":\"⇾\",\"robrk\":\"⟧\",\"ropar\":\"⦆\",\"ropf\":\"𝕣\",\"Ropf\":\"ℝ\",\"roplus\":\"⨮\",\"rotimes\":\"⨵\",\"RoundImplies\":\"⥰\",\"rpar\":\")\",\"rpargt\":\"⦔\",\"rppolint\":\"⨒\",\"rrarr\":\"⇉\",\"Rrightarrow\":\"⇛\",\"rsaquo\":\"›\",\"rscr\":\"𝓇\",\"Rscr\":\"ℛ\",\"rsh\":\"↱\",\"Rsh\":\"↱\",\"rsqb\":\"]\",\"rsquo\":\"’\",\"rsquor\":\"’\",\"rthree\":\"⋌\",\"rtimes\":\"⋊\",\"rtri\":\"▹\",\"rtrie\":\"⊵\",\"rtrif\":\"▸\",\"rtriltri\":\"⧎\",\"RuleDelayed\":\"⧴\",\"ruluhar\":\"⥨\",\"rx\":\"℞\",\"Sacute\":\"Ś\",\"sacute\":\"ś\",\"sbquo\":\"‚\",\"scap\":\"⪸\",\"Scaron\":\"Š\",\"scaron\":\"š\",\"Sc\":\"⪼\",\"sc\":\"≻\",\"sccue\":\"≽\",\"sce\":\"⪰\",\"scE\":\"⪴\",\"Scedil\":\"Ş\",\"scedil\":\"ş\",\"Scirc\":\"Ŝ\",\"scirc\":\"ŝ\",\"scnap\":\"⪺\",\"scnE\":\"⪶\",\"scnsim\":\"⋩\",\"scpolint\":\"⨓\",\"scsim\":\"≿\",\"Scy\":\"С\",\"scy\":\"с\",\"sdotb\":\"⊡\",\"sdot\":\"⋅\",\"sdote\":\"⩦\",\"searhk\":\"⤥\",\"searr\":\"↘\",\"seArr\":\"⇘\",\"searrow\":\"↘\",\"sect\":\"§\",\"semi\":\";\",\"seswar\":\"⤩\",\"setminus\":\"∖\",\"setmn\":\"∖\",\"sext\":\"✶\",\"Sfr\":\"𝔖\",\"sfr\":\"𝔰\",\"sfrown\":\"⌢\",\"sharp\":\"♯\",\"SHCHcy\":\"Щ\",\"shchcy\":\"щ\",\"SHcy\":\"Ш\",\"shcy\":\"ш\",\"ShortDownArrow\":\"↓\",\"ShortLeftArrow\":\"←\",\"shortmid\":\"∣\",\"shortparallel\":\"∥\",\"ShortRightArrow\":\"→\",\"ShortUpArrow\":\"↑\",\"shy\":\"­\",\"Sigma\":\"Σ\",\"sigma\":\"σ\",\"sigmaf\":\"ς\",\"sigmav\":\"ς\",\"sim\":\"∼\",\"simdot\":\"⩪\",\"sime\":\"≃\",\"simeq\":\"≃\",\"simg\":\"⪞\",\"simgE\":\"⪠\",\"siml\":\"⪝\",\"simlE\":\"⪟\",\"simne\":\"≆\",\"simplus\":\"⨤\",\"simrarr\":\"⥲\",\"slarr\":\"←\",\"SmallCircle\":\"∘\",\"smallsetminus\":\"∖\",\"smashp\":\"⨳\",\"smeparsl\":\"⧤\",\"smid\":\"∣\",\"smile\":\"⌣\",\"smt\":\"⪪\",\"smte\":\"⪬\",\"smtes\":\"⪬︀\",\"SOFTcy\":\"Ь\",\"softcy\":\"ь\",\"solbar\":\"⌿\",\"solb\":\"⧄\",\"sol\":\"/\",\"Sopf\":\"𝕊\",\"sopf\":\"𝕤\",\"spades\":\"♠\",\"spadesuit\":\"♠\",\"spar\":\"∥\",\"sqcap\":\"⊓\",\"sqcaps\":\"⊓︀\",\"sqcup\":\"⊔\",\"sqcups\":\"⊔︀\",\"Sqrt\":\"√\",\"sqsub\":\"⊏\",\"sqsube\":\"⊑\",\"sqsubset\":\"⊏\",\"sqsubseteq\":\"⊑\",\"sqsup\":\"⊐\",\"sqsupe\":\"⊒\",\"sqsupset\":\"⊐\",\"sqsupseteq\":\"⊒\",\"square\":\"□\",\"Square\":\"□\",\"SquareIntersection\":\"⊓\",\"SquareSubset\":\"⊏\",\"SquareSubsetEqual\":\"⊑\",\"SquareSuperset\":\"⊐\",\"SquareSupersetEqual\":\"⊒\",\"SquareUnion\":\"⊔\",\"squarf\":\"▪\",\"squ\":\"□\",\"squf\":\"▪\",\"srarr\":\"→\",\"Sscr\":\"𝒮\",\"sscr\":\"𝓈\",\"ssetmn\":\"∖\",\"ssmile\":\"⌣\",\"sstarf\":\"⋆\",\"Star\":\"⋆\",\"star\":\"☆\",\"starf\":\"★\",\"straightepsilon\":\"ϵ\",\"straightphi\":\"ϕ\",\"strns\":\"¯\",\"sub\":\"⊂\",\"Sub\":\"⋐\",\"subdot\":\"⪽\",\"subE\":\"⫅\",\"sube\":\"⊆\",\"subedot\":\"⫃\",\"submult\":\"⫁\",\"subnE\":\"⫋\",\"subne\":\"⊊\",\"subplus\":\"⪿\",\"subrarr\":\"⥹\",\"subset\":\"⊂\",\"Subset\":\"⋐\",\"subseteq\":\"⊆\",\"subseteqq\":\"⫅\",\"SubsetEqual\":\"⊆\",\"subsetneq\":\"⊊\",\"subsetneqq\":\"⫋\",\"subsim\":\"⫇\",\"subsub\":\"⫕\",\"subsup\":\"⫓\",\"succapprox\":\"⪸\",\"succ\":\"≻\",\"succcurlyeq\":\"≽\",\"Succeeds\":\"≻\",\"SucceedsEqual\":\"⪰\",\"SucceedsSlantEqual\":\"≽\",\"SucceedsTilde\":\"≿\",\"succeq\":\"⪰\",\"succnapprox\":\"⪺\",\"succneqq\":\"⪶\",\"succnsim\":\"⋩\",\"succsim\":\"≿\",\"SuchThat\":\"∋\",\"sum\":\"∑\",\"Sum\":\"∑\",\"sung\":\"♪\",\"sup1\":\"¹\",\"sup2\":\"²\",\"sup3\":\"³\",\"sup\":\"⊃\",\"Sup\":\"⋑\",\"supdot\":\"⪾\",\"supdsub\":\"⫘\",\"supE\":\"⫆\",\"supe\":\"⊇\",\"supedot\":\"⫄\",\"Superset\":\"⊃\",\"SupersetEqual\":\"⊇\",\"suphsol\":\"⟉\",\"suphsub\":\"⫗\",\"suplarr\":\"⥻\",\"supmult\":\"⫂\",\"supnE\":\"⫌\",\"supne\":\"⊋\",\"supplus\":\"⫀\",\"supset\":\"⊃\",\"Supset\":\"⋑\",\"supseteq\":\"⊇\",\"supseteqq\":\"⫆\",\"supsetneq\":\"⊋\",\"supsetneqq\":\"⫌\",\"supsim\":\"⫈\",\"supsub\":\"⫔\",\"supsup\":\"⫖\",\"swarhk\":\"⤦\",\"swarr\":\"↙\",\"swArr\":\"⇙\",\"swarrow\":\"↙\",\"swnwar\":\"⤪\",\"szlig\":\"ß\",\"Tab\":\"\\t\",\"target\":\"⌖\",\"Tau\":\"Τ\",\"tau\":\"τ\",\"tbrk\":\"⎴\",\"Tcaron\":\"Ť\",\"tcaron\":\"ť\",\"Tcedil\":\"Ţ\",\"tcedil\":\"ţ\",\"Tcy\":\"Т\",\"tcy\":\"т\",\"tdot\":\"⃛\",\"telrec\":\"⌕\",\"Tfr\":\"𝔗\",\"tfr\":\"𝔱\",\"there4\":\"∴\",\"therefore\":\"∴\",\"Therefore\":\"∴\",\"Theta\":\"Θ\",\"theta\":\"θ\",\"thetasym\":\"ϑ\",\"thetav\":\"ϑ\",\"thickapprox\":\"≈\",\"thicksim\":\"∼\",\"ThickSpace\":\"  \",\"ThinSpace\":\" \",\"thinsp\":\" \",\"thkap\":\"≈\",\"thksim\":\"∼\",\"THORN\":\"Þ\",\"thorn\":\"þ\",\"tilde\":\"˜\",\"Tilde\":\"∼\",\"TildeEqual\":\"≃\",\"TildeFullEqual\":\"≅\",\"TildeTilde\":\"≈\",\"timesbar\":\"⨱\",\"timesb\":\"⊠\",\"times\":\"×\",\"timesd\":\"⨰\",\"tint\":\"∭\",\"toea\":\"⤨\",\"topbot\":\"⌶\",\"topcir\":\"⫱\",\"top\":\"⊤\",\"Topf\":\"𝕋\",\"topf\":\"𝕥\",\"topfork\":\"⫚\",\"tosa\":\"⤩\",\"tprime\":\"‴\",\"trade\":\"™\",\"TRADE\":\"™\",\"triangle\":\"▵\",\"triangledown\":\"▿\",\"triangleleft\":\"◃\",\"trianglelefteq\":\"⊴\",\"triangleq\":\"≜\",\"triangleright\":\"▹\",\"trianglerighteq\":\"⊵\",\"tridot\":\"◬\",\"trie\":\"≜\",\"triminus\":\"⨺\",\"TripleDot\":\"⃛\",\"triplus\":\"⨹\",\"trisb\":\"⧍\",\"tritime\":\"⨻\",\"trpezium\":\"⏢\",\"Tscr\":\"𝒯\",\"tscr\":\"𝓉\",\"TScy\":\"Ц\",\"tscy\":\"ц\",\"TSHcy\":\"Ћ\",\"tshcy\":\"ћ\",\"Tstrok\":\"Ŧ\",\"tstrok\":\"ŧ\",\"twixt\":\"≬\",\"twoheadleftarrow\":\"↞\",\"twoheadrightarrow\":\"↠\",\"Uacute\":\"Ú\",\"uacute\":\"ú\",\"uarr\":\"↑\",\"Uarr\":\"↟\",\"uArr\":\"⇑\",\"Uarrocir\":\"⥉\",\"Ubrcy\":\"Ў\",\"ubrcy\":\"ў\",\"Ubreve\":\"Ŭ\",\"ubreve\":\"ŭ\",\"Ucirc\":\"Û\",\"ucirc\":\"û\",\"Ucy\":\"У\",\"ucy\":\"у\",\"udarr\":\"⇅\",\"Udblac\":\"Ű\",\"udblac\":\"ű\",\"udhar\":\"⥮\",\"ufisht\":\"⥾\",\"Ufr\":\"𝔘\",\"ufr\":\"𝔲\",\"Ugrave\":\"Ù\",\"ugrave\":\"ù\",\"uHar\":\"⥣\",\"uharl\":\"↿\",\"uharr\":\"↾\",\"uhblk\":\"▀\",\"ulcorn\":\"⌜\",\"ulcorner\":\"⌜\",\"ulcrop\":\"⌏\",\"ultri\":\"◸\",\"Umacr\":\"Ū\",\"umacr\":\"ū\",\"uml\":\"¨\",\"UnderBar\":\"_\",\"UnderBrace\":\"⏟\",\"UnderBracket\":\"⎵\",\"UnderParenthesis\":\"⏝\",\"Union\":\"⋃\",\"UnionPlus\":\"⊎\",\"Uogon\":\"Ų\",\"uogon\":\"ų\",\"Uopf\":\"𝕌\",\"uopf\":\"𝕦\",\"UpArrowBar\":\"⤒\",\"uparrow\":\"↑\",\"UpArrow\":\"↑\",\"Uparrow\":\"⇑\",\"UpArrowDownArrow\":\"⇅\",\"updownarrow\":\"↕\",\"UpDownArrow\":\"↕\",\"Updownarrow\":\"⇕\",\"UpEquilibrium\":\"⥮\",\"upharpoonleft\":\"↿\",\"upharpoonright\":\"↾\",\"uplus\":\"⊎\",\"UpperLeftArrow\":\"↖\",\"UpperRightArrow\":\"↗\",\"upsi\":\"υ\",\"Upsi\":\"ϒ\",\"upsih\":\"ϒ\",\"Upsilon\":\"Υ\",\"upsilon\":\"υ\",\"UpTeeArrow\":\"↥\",\"UpTee\":\"⊥\",\"upuparrows\":\"⇈\",\"urcorn\":\"⌝\",\"urcorner\":\"⌝\",\"urcrop\":\"⌎\",\"Uring\":\"Ů\",\"uring\":\"ů\",\"urtri\":\"◹\",\"Uscr\":\"𝒰\",\"uscr\":\"𝓊\",\"utdot\":\"⋰\",\"Utilde\":\"Ũ\",\"utilde\":\"ũ\",\"utri\":\"▵\",\"utrif\":\"▴\",\"uuarr\":\"⇈\",\"Uuml\":\"Ü\",\"uuml\":\"ü\",\"uwangle\":\"⦧\",\"vangrt\":\"⦜\",\"varepsilon\":\"ϵ\",\"varkappa\":\"ϰ\",\"varnothing\":\"∅\",\"varphi\":\"ϕ\",\"varpi\":\"ϖ\",\"varpropto\":\"∝\",\"varr\":\"↕\",\"vArr\":\"⇕\",\"varrho\":\"ϱ\",\"varsigma\":\"ς\",\"varsubsetneq\":\"⊊︀\",\"varsubsetneqq\":\"⫋︀\",\"varsupsetneq\":\"⊋︀\",\"varsupsetneqq\":\"⫌︀\",\"vartheta\":\"ϑ\",\"vartriangleleft\":\"⊲\",\"vartriangleright\":\"⊳\",\"vBar\":\"⫨\",\"Vbar\":\"⫫\",\"vBarv\":\"⫩\",\"Vcy\":\"В\",\"vcy\":\"в\",\"vdash\":\"⊢\",\"vDash\":\"⊨\",\"Vdash\":\"⊩\",\"VDash\":\"⊫\",\"Vdashl\":\"⫦\",\"veebar\":\"⊻\",\"vee\":\"∨\",\"Vee\":\"⋁\",\"veeeq\":\"≚\",\"vellip\":\"⋮\",\"verbar\":\"|\",\"Verbar\":\"‖\",\"vert\":\"|\",\"Vert\":\"‖\",\"VerticalBar\":\"∣\",\"VerticalLine\":\"|\",\"VerticalSeparator\":\"❘\",\"VerticalTilde\":\"≀\",\"VeryThinSpace\":\" \",\"Vfr\":\"𝔙\",\"vfr\":\"𝔳\",\"vltri\":\"⊲\",\"vnsub\":\"⊂⃒\",\"vnsup\":\"⊃⃒\",\"Vopf\":\"𝕍\",\"vopf\":\"𝕧\",\"vprop\":\"∝\",\"vrtri\":\"⊳\",\"Vscr\":\"𝒱\",\"vscr\":\"𝓋\",\"vsubnE\":\"⫋︀\",\"vsubne\":\"⊊︀\",\"vsupnE\":\"⫌︀\",\"vsupne\":\"⊋︀\",\"Vvdash\":\"⊪\",\"vzigzag\":\"⦚\",\"Wcirc\":\"Ŵ\",\"wcirc\":\"ŵ\",\"wedbar\":\"⩟\",\"wedge\":\"∧\",\"Wedge\":\"⋀\",\"wedgeq\":\"≙\",\"weierp\":\"℘\",\"Wfr\":\"𝔚\",\"wfr\":\"𝔴\",\"Wopf\":\"𝕎\",\"wopf\":\"𝕨\",\"wp\":\"℘\",\"wr\":\"≀\",\"wreath\":\"≀\",\"Wscr\":\"𝒲\",\"wscr\":\"𝓌\",\"xcap\":\"⋂\",\"xcirc\":\"◯\",\"xcup\":\"⋃\",\"xdtri\":\"▽\",\"Xfr\":\"𝔛\",\"xfr\":\"𝔵\",\"xharr\":\"⟷\",\"xhArr\":\"⟺\",\"Xi\":\"Ξ\",\"xi\":\"ξ\",\"xlarr\":\"⟵\",\"xlArr\":\"⟸\",\"xmap\":\"⟼\",\"xnis\":\"⋻\",\"xodot\":\"⨀\",\"Xopf\":\"𝕏\",\"xopf\":\"𝕩\",\"xoplus\":\"⨁\",\"xotime\":\"⨂\",\"xrarr\":\"⟶\",\"xrArr\":\"⟹\",\"Xscr\":\"𝒳\",\"xscr\":\"𝓍\",\"xsqcup\":\"⨆\",\"xuplus\":\"⨄\",\"xutri\":\"△\",\"xvee\":\"⋁\",\"xwedge\":\"⋀\",\"Yacute\":\"Ý\",\"yacute\":\"ý\",\"YAcy\":\"Я\",\"yacy\":\"я\",\"Ycirc\":\"Ŷ\",\"ycirc\":\"ŷ\",\"Ycy\":\"Ы\",\"ycy\":\"ы\",\"yen\":\"¥\",\"Yfr\":\"𝔜\",\"yfr\":\"𝔶\",\"YIcy\":\"Ї\",\"yicy\":\"ї\",\"Yopf\":\"𝕐\",\"yopf\":\"𝕪\",\"Yscr\":\"𝒴\",\"yscr\":\"𝓎\",\"YUcy\":\"Ю\",\"yucy\":\"ю\",\"yuml\":\"ÿ\",\"Yuml\":\"Ÿ\",\"Zacute\":\"Ź\",\"zacute\":\"ź\",\"Zcaron\":\"Ž\",\"zcaron\":\"ž\",\"Zcy\":\"З\",\"zcy\":\"з\",\"Zdot\":\"Ż\",\"zdot\":\"ż\",\"zeetrf\":\"ℨ\",\"ZeroWidthSpace\":\"​\",\"Zeta\":\"Ζ\",\"zeta\":\"ζ\",\"zfr\":\"𝔷\",\"Zfr\":\"ℨ\",\"ZHcy\":\"Ж\",\"zhcy\":\"ж\",\"zigrarr\":\"⇝\",\"zopf\":\"𝕫\",\"Zopf\":\"ℤ\",\"Zscr\":\"𝒵\",\"zscr\":\"𝓏\",\"zwj\":\"‍\",\"zwnj\":\"‌\"}");

/***/ }),

/***/ "../../node_modules/linkify-it/index.js":
/*!************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/linkify-it/index.js ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
 ////////////////////////////////////////////////////////////////////////////////
// Helpers
// Merge objects
//

function assign(obj
/*from1, from2, from3, ...*/
) {
  var sources = Array.prototype.slice.call(arguments, 1);
  sources.forEach(function (source) {
    if (!source) {
      return;
    }

    Object.keys(source).forEach(function (key) {
      obj[key] = source[key];
    });
  });
  return obj;
}

function _class(obj) {
  return Object.prototype.toString.call(obj);
}

function isString(obj) {
  return _class(obj) === '[object String]';
}

function isObject(obj) {
  return _class(obj) === '[object Object]';
}

function isRegExp(obj) {
  return _class(obj) === '[object RegExp]';
}

function isFunction(obj) {
  return _class(obj) === '[object Function]';
}

function escapeRE(str) {
  return str.replace(/[.?*+^$[\]\\(){}|-]/g, '\\$&');
} ////////////////////////////////////////////////////////////////////////////////


var defaultOptions = {
  fuzzyLink: true,
  fuzzyEmail: true,
  fuzzyIP: false
};

function isOptionsObj(obj) {
  return Object.keys(obj || {}).reduce(function (acc, k) {
    return acc || defaultOptions.hasOwnProperty(k);
  }, false);
}

var defaultSchemas = {
  'http:': {
    validate: function validate(text, pos, self) {
      var tail = text.slice(pos);

      if (!self.re.http) {
        // compile lazily, because "host"-containing variables can change on tlds update.
        self.re.http = new RegExp('^\\/\\/' + self.re.src_auth + self.re.src_host_port_strict + self.re.src_path, 'i');
      }

      if (self.re.http.test(tail)) {
        return tail.match(self.re.http)[0].length;
      }

      return 0;
    }
  },
  'https:': 'http:',
  'ftp:': 'http:',
  '//': {
    validate: function validate(text, pos, self) {
      var tail = text.slice(pos);

      if (!self.re.no_http) {
        // compile lazily, because "host"-containing variables can change on tlds update.
        self.re.no_http = new RegExp('^' + self.re.src_auth + // Don't allow single-level domains, because of false positives like '//test'
        // with code comments
        '(?:localhost|(?:(?:' + self.re.src_domain + ')\\.)+' + self.re.src_domain_root + ')' + self.re.src_port + self.re.src_host_terminator + self.re.src_path, 'i');
      }

      if (self.re.no_http.test(tail)) {
        // should not be `://` & `///`, that protects from errors in protocol name
        if (pos >= 3 && text[pos - 3] === ':') {
          return 0;
        }

        if (pos >= 3 && text[pos - 3] === '/') {
          return 0;
        }

        return tail.match(self.re.no_http)[0].length;
      }

      return 0;
    }
  },
  'mailto:': {
    validate: function validate(text, pos, self) {
      var tail = text.slice(pos);

      if (!self.re.mailto) {
        self.re.mailto = new RegExp('^' + self.re.src_email_name + '@' + self.re.src_host_strict, 'i');
      }

      if (self.re.mailto.test(tail)) {
        return tail.match(self.re.mailto)[0].length;
      }

      return 0;
    }
  }
};
/*eslint-disable max-len*/
// RE pattern for 2-character tlds (autogenerated by ./support/tlds_2char_gen.js)

var tlds_2ch_src_re = 'a[cdefgilmnoqrstuwxz]|b[abdefghijmnorstvwyz]|c[acdfghiklmnoruvwxyz]|d[ejkmoz]|e[cegrstu]|f[ijkmor]|g[abdefghilmnpqrstuwy]|h[kmnrtu]|i[delmnoqrst]|j[emop]|k[eghimnprwyz]|l[abcikrstuvy]|m[acdeghklmnopqrstuvwxyz]|n[acefgilopruz]|om|p[aefghklmnrstwy]|qa|r[eosuw]|s[abcdeghijklmnortuvxyz]|t[cdfghjklmnortvwz]|u[agksyz]|v[aceginu]|w[fs]|y[et]|z[amw]'; // DON'T try to make PRs with changes. Extend TLDs with LinkifyIt.tlds() instead

var tlds_default = 'biz|com|edu|gov|net|org|pro|web|xxx|aero|asia|coop|info|museum|name|shop|рф'.split('|');
/*eslint-enable max-len*/
////////////////////////////////////////////////////////////////////////////////

function resetScanCache(self) {
  self.__index__ = -1;
  self.__text_cache__ = '';
}

function createValidator(re) {
  return function (text, pos) {
    var tail = text.slice(pos);

    if (re.test(tail)) {
      return tail.match(re)[0].length;
    }

    return 0;
  };
}

function createNormalizer() {
  return function (match, self) {
    self.normalize(match);
  };
} // Schemas compiler. Build regexps.
//


function compile(self) {
  // Load & clone RE patterns.
  var re = self.re = __webpack_require__(/*! ./lib/re */ "../../node_modules/linkify-it/lib/re.js")(self.__opts__); // Define dynamic patterns


  var tlds = self.__tlds__.slice();

  self.onCompile();

  if (!self.__tlds_replaced__) {
    tlds.push(tlds_2ch_src_re);
  }

  tlds.push(re.src_xn);
  re.src_tlds = tlds.join('|');

  function untpl(tpl) {
    return tpl.replace('%TLDS%', re.src_tlds);
  }

  re.email_fuzzy = RegExp(untpl(re.tpl_email_fuzzy), 'i');
  re.link_fuzzy = RegExp(untpl(re.tpl_link_fuzzy), 'i');
  re.link_no_ip_fuzzy = RegExp(untpl(re.tpl_link_no_ip_fuzzy), 'i');
  re.host_fuzzy_test = RegExp(untpl(re.tpl_host_fuzzy_test), 'i'); //
  // Compile each schema
  //

  var aliases = [];
  self.__compiled__ = {}; // Reset compiled data

  function schemaError(name, val) {
    throw new Error('(LinkifyIt) Invalid schema "' + name + '": ' + val);
  }

  Object.keys(self.__schemas__).forEach(function (name) {
    var val = self.__schemas__[name]; // skip disabled methods

    if (val === null) {
      return;
    }

    var compiled = {
      validate: null,
      link: null
    };
    self.__compiled__[name] = compiled;

    if (isObject(val)) {
      if (isRegExp(val.validate)) {
        compiled.validate = createValidator(val.validate);
      } else if (isFunction(val.validate)) {
        compiled.validate = val.validate;
      } else {
        schemaError(name, val);
      }

      if (isFunction(val.normalize)) {
        compiled.normalize = val.normalize;
      } else if (!val.normalize) {
        compiled.normalize = createNormalizer();
      } else {
        schemaError(name, val);
      }

      return;
    }

    if (isString(val)) {
      aliases.push(name);
      return;
    }

    schemaError(name, val);
  }); //
  // Compile postponed aliases
  //

  aliases.forEach(function (alias) {
    if (!self.__compiled__[self.__schemas__[alias]]) {
      // Silently fail on missed schemas to avoid errons on disable.
      // schemaError(alias, self.__schemas__[alias]);
      return;
    }

    self.__compiled__[alias].validate = self.__compiled__[self.__schemas__[alias]].validate;
    self.__compiled__[alias].normalize = self.__compiled__[self.__schemas__[alias]].normalize;
  }); //
  // Fake record for guessed links
  //

  self.__compiled__[''] = {
    validate: null,
    normalize: createNormalizer()
  }; //
  // Build schema condition
  //

  var slist = Object.keys(self.__compiled__).filter(function (name) {
    // Filter disabled & fake schemas
    return name.length > 0 && self.__compiled__[name];
  }).map(escapeRE).join('|'); // (?!_) cause 1.5x slowdown

  self.re.schema_test = RegExp("(^|(?!_)(?:[><\uFF5C]|" + re.src_ZPCc + '))(' + slist + ')', 'i');
  self.re.schema_search = RegExp("(^|(?!_)(?:[><\uFF5C]|" + re.src_ZPCc + '))(' + slist + ')', 'ig');
  self.re.pretest = RegExp('(' + self.re.schema_test.source + ')|(' + self.re.host_fuzzy_test.source + ')|@', 'i'); //
  // Cleanup
  //

  resetScanCache(self);
}
/**
 * class Match
 *
 * Match result. Single element of array, returned by [[LinkifyIt#match]]
 **/


function Match(self, shift) {
  var start = self.__index__,
      end = self.__last_index__,
      text = self.__text_cache__.slice(start, end);
  /**
   * Match#schema -> String
   *
   * Prefix (protocol) for matched string.
   **/


  this.schema = self.__schema__.toLowerCase();
  /**
   * Match#index -> Number
   *
   * First position of matched string.
   **/

  this.index = start + shift;
  /**
   * Match#lastIndex -> Number
   *
   * Next position after matched string.
   **/

  this.lastIndex = end + shift;
  /**
   * Match#raw -> String
   *
   * Matched string.
   **/

  this.raw = text;
  /**
   * Match#text -> String
   *
   * Notmalized text of matched string.
   **/

  this.text = text;
  /**
   * Match#url -> String
   *
   * Normalized url of matched string.
   **/

  this.url = text;
}

function createMatch(self, shift) {
  var match = new Match(self, shift);

  self.__compiled__[match.schema].normalize(match, self);

  return match;
}
/**
 * class LinkifyIt
 **/

/**
 * new LinkifyIt(schemas, options)
 * - schemas (Object): Optional. Additional schemas to validate (prefix/validator)
 * - options (Object): { fuzzyLink|fuzzyEmail|fuzzyIP: true|false }
 *
 * Creates new linkifier instance with optional additional schemas.
 * Can be called without `new` keyword for convenience.
 *
 * By default understands:
 *
 * - `http(s)://...` , `ftp://...`, `mailto:...` & `//...` links
 * - "fuzzy" links and emails (example.com, foo@bar.com).
 *
 * `schemas` is an object, where each key/value describes protocol/rule:
 *
 * - __key__ - link prefix (usually, protocol name with `:` at the end, `skype:`
 *   for example). `linkify-it` makes shure that prefix is not preceeded with
 *   alphanumeric char and symbols. Only whitespaces and punctuation allowed.
 * - __value__ - rule to check tail after link prefix
 *   - _String_ - just alias to existing rule
 *   - _Object_
 *     - _validate_ - validator function (should return matched length on success),
 *       or `RegExp`.
 *     - _normalize_ - optional function to normalize text & url of matched result
 *       (for example, for @twitter mentions).
 *
 * `options`:
 *
 * - __fuzzyLink__ - recognige URL-s without `http(s):` prefix. Default `true`.
 * - __fuzzyIP__ - allow IPs in fuzzy links above. Can conflict with some texts
 *   like version numbers. Default `false`.
 * - __fuzzyEmail__ - recognize emails without `mailto:` prefix.
 *
 **/


function LinkifyIt(schemas, options) {
  if (!(this instanceof LinkifyIt)) {
    return new LinkifyIt(schemas, options);
  }

  if (!options) {
    if (isOptionsObj(schemas)) {
      options = schemas;
      schemas = {};
    }
  }

  this.__opts__ = assign({}, defaultOptions, options); // Cache last tested result. Used to skip repeating steps on next `match` call.

  this.__index__ = -1;
  this.__last_index__ = -1; // Next scan position

  this.__schema__ = '';
  this.__text_cache__ = '';
  this.__schemas__ = assign({}, defaultSchemas, schemas);
  this.__compiled__ = {};
  this.__tlds__ = tlds_default;
  this.__tlds_replaced__ = false;
  this.re = {};
  compile(this);
}
/** chainable
 * LinkifyIt#add(schema, definition)
 * - schema (String): rule name (fixed pattern prefix)
 * - definition (String|RegExp|Object): schema definition
 *
 * Add new rule definition. See constructor description for details.
 **/


LinkifyIt.prototype.add = function add(schema, definition) {
  this.__schemas__[schema] = definition;
  compile(this);
  return this;
};
/** chainable
 * LinkifyIt#set(options)
 * - options (Object): { fuzzyLink|fuzzyEmail|fuzzyIP: true|false }
 *
 * Set recognition options for links without schema.
 **/


LinkifyIt.prototype.set = function set(options) {
  this.__opts__ = assign(this.__opts__, options);
  return this;
};
/**
 * LinkifyIt#test(text) -> Boolean
 *
 * Searches linkifiable pattern and returns `true` on success or `false` on fail.
 **/


LinkifyIt.prototype.test = function test(text) {
  // Reset scan cache
  this.__text_cache__ = text;
  this.__index__ = -1;

  if (!text.length) {
    return false;
  }

  var m, ml, me, len, shift, next, re, tld_pos, at_pos; // try to scan for link with schema - that's the most simple rule

  if (this.re.schema_test.test(text)) {
    re = this.re.schema_search;
    re.lastIndex = 0;

    while ((m = re.exec(text)) !== null) {
      len = this.testSchemaAt(text, m[2], re.lastIndex);

      if (len) {
        this.__schema__ = m[2];
        this.__index__ = m.index + m[1].length;
        this.__last_index__ = m.index + m[0].length + len;
        break;
      }
    }
  }

  if (this.__opts__.fuzzyLink && this.__compiled__['http:']) {
    // guess schemaless links
    tld_pos = text.search(this.re.host_fuzzy_test);

    if (tld_pos >= 0) {
      // if tld is located after found link - no need to check fuzzy pattern
      if (this.__index__ < 0 || tld_pos < this.__index__) {
        if ((ml = text.match(this.__opts__.fuzzyIP ? this.re.link_fuzzy : this.re.link_no_ip_fuzzy)) !== null) {
          shift = ml.index + ml[1].length;

          if (this.__index__ < 0 || shift < this.__index__) {
            this.__schema__ = '';
            this.__index__ = shift;
            this.__last_index__ = ml.index + ml[0].length;
          }
        }
      }
    }
  }

  if (this.__opts__.fuzzyEmail && this.__compiled__['mailto:']) {
    // guess schemaless emails
    at_pos = text.indexOf('@');

    if (at_pos >= 0) {
      // We can't skip this check, because this cases are possible:
      // 192.168.1.1@gmail.com, my.in@example.com
      if ((me = text.match(this.re.email_fuzzy)) !== null) {
        shift = me.index + me[1].length;
        next = me.index + me[0].length;

        if (this.__index__ < 0 || shift < this.__index__ || shift === this.__index__ && next > this.__last_index__) {
          this.__schema__ = 'mailto:';
          this.__index__ = shift;
          this.__last_index__ = next;
        }
      }
    }
  }

  return this.__index__ >= 0;
};
/**
 * LinkifyIt#pretest(text) -> Boolean
 *
 * Very quick check, that can give false positives. Returns true if link MAY BE
 * can exists. Can be used for speed optimization, when you need to check that
 * link NOT exists.
 **/


LinkifyIt.prototype.pretest = function pretest(text) {
  return this.re.pretest.test(text);
};
/**
 * LinkifyIt#testSchemaAt(text, name, position) -> Number
 * - text (String): text to scan
 * - name (String): rule (schema) name
 * - position (Number): text offset to check from
 *
 * Similar to [[LinkifyIt#test]] but checks only specific protocol tail exactly
 * at given position. Returns length of found pattern (0 on fail).
 **/


LinkifyIt.prototype.testSchemaAt = function testSchemaAt(text, schema, pos) {
  // If not supported schema check requested - terminate
  if (!this.__compiled__[schema.toLowerCase()]) {
    return 0;
  }

  return this.__compiled__[schema.toLowerCase()].validate(text, pos, this);
};
/**
 * LinkifyIt#match(text) -> Array|null
 *
 * Returns array of found link descriptions or `null` on fail. We strongly
 * recommend to use [[LinkifyIt#test]] first, for best speed.
 *
 * ##### Result match description
 *
 * - __schema__ - link schema, can be empty for fuzzy links, or `//` for
 *   protocol-neutral  links.
 * - __index__ - offset of matched text
 * - __lastIndex__ - index of next char after mathch end
 * - __raw__ - matched text
 * - __text__ - normalized text
 * - __url__ - link, generated from matched text
 **/


LinkifyIt.prototype.match = function match(text) {
  var shift = 0,
      result = []; // Try to take previous element from cache, if .test() called before

  if (this.__index__ >= 0 && this.__text_cache__ === text) {
    result.push(createMatch(this, shift));
    shift = this.__last_index__;
  } // Cut head if cache was used


  var tail = shift ? text.slice(shift) : text; // Scan string until end reached

  while (this.test(tail)) {
    result.push(createMatch(this, shift));
    tail = tail.slice(this.__last_index__);
    shift += this.__last_index__;
  }

  if (result.length) {
    return result;
  }

  return null;
};
/** chainable
 * LinkifyIt#tlds(list [, keepOld]) -> this
 * - list (Array): list of tlds
 * - keepOld (Boolean): merge with current list if `true` (`false` by default)
 *
 * Load (or merge) new tlds list. Those are user for fuzzy links (without prefix)
 * to avoid false positives. By default this algorythm used:
 *
 * - hostname with any 2-letter root zones are ok.
 * - biz|com|edu|gov|net|org|pro|web|xxx|aero|asia|coop|info|museum|name|shop|рф
 *   are ok.
 * - encoded (`xn--...`) root zones are ok.
 *
 * If list is replaced, then exact match for 2-chars root zones will be checked.
 **/


LinkifyIt.prototype.tlds = function tlds(list, keepOld) {
  list = Array.isArray(list) ? list : [list];

  if (!keepOld) {
    this.__tlds__ = list.slice();
    this.__tlds_replaced__ = true;
    compile(this);
    return this;
  }

  this.__tlds__ = this.__tlds__.concat(list).sort().filter(function (el, idx, arr) {
    return el !== arr[idx - 1];
  }).reverse();
  compile(this);
  return this;
};
/**
 * LinkifyIt#normalize(match)
 *
 * Default normalizer (if schema does not define it's own).
 **/


LinkifyIt.prototype.normalize = function normalize(match) {
  // Do minimal possible changes by default. Need to collect feedback prior
  // to move forward https://github.com/markdown-it/linkify-it/issues/1
  if (!match.schema) {
    match.url = 'http://' + match.url;
  }

  if (match.schema === 'mailto:' && !/^mailto:/i.test(match.url)) {
    match.url = 'mailto:' + match.url;
  }
};
/**
 * LinkifyIt#onCompile()
 *
 * Override to modify basic RegExp-s.
 **/


LinkifyIt.prototype.onCompile = function onCompile() {};

module.exports = LinkifyIt;

/***/ }),

/***/ "../../node_modules/linkify-it/lib/re.js":
/*!*************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/linkify-it/lib/re.js ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = function (opts) {
  var re = {}; // Use direct extract instead of `regenerate` to reduse browserified size

  re.src_Any = __webpack_require__(/*! uc.micro/properties/Any/regex */ "../../node_modules/uc.micro/properties/Any/regex.js").source;
  re.src_Cc = __webpack_require__(/*! uc.micro/categories/Cc/regex */ "../../node_modules/uc.micro/categories/Cc/regex.js").source;
  re.src_Z = __webpack_require__(/*! uc.micro/categories/Z/regex */ "../../node_modules/uc.micro/categories/Z/regex.js").source;
  re.src_P = __webpack_require__(/*! uc.micro/categories/P/regex */ "../../node_modules/uc.micro/categories/P/regex.js").source; // \p{\Z\P\Cc\CF} (white spaces + control + format + punctuation)

  re.src_ZPCc = [re.src_Z, re.src_P, re.src_Cc].join('|'); // \p{\Z\Cc} (white spaces + control)

  re.src_ZCc = [re.src_Z, re.src_Cc].join('|'); // Experimental. List of chars, completely prohibited in links
  // because can separate it from other part of text

  var text_separators = "[><\uFF5C]"; // All possible word characters (everything without punctuation, spaces & controls)
  // Defined via punctuation & spaces to save space
  // Should be something like \p{\L\N\S\M} (\w but without `_`)

  re.src_pseudo_letter = '(?:(?!' + text_separators + '|' + re.src_ZPCc + ')' + re.src_Any + ')'; // The same as abothe but without [0-9]
  // var src_pseudo_letter_non_d = '(?:(?![0-9]|' + src_ZPCc + ')' + src_Any + ')';
  ////////////////////////////////////////////////////////////////////////////////

  re.src_ip4 = '(?:(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)'; // Prohibit any of "@/[]()" in user/pass to avoid wrong domain fetch.

  re.src_auth = '(?:(?:(?!' + re.src_ZCc + '|[@/\\[\\]()]).)+@)?';
  re.src_port = '(?::(?:6(?:[0-4]\\d{3}|5(?:[0-4]\\d{2}|5(?:[0-2]\\d|3[0-5])))|[1-5]?\\d{1,4}))?';
  re.src_host_terminator = '(?=$|' + text_separators + '|' + re.src_ZPCc + ')(?!-|_|:\\d|\\.-|\\.(?!$|' + re.src_ZPCc + '))';
  re.src_path = '(?:' + '[/?#]' + '(?:' + '(?!' + re.src_ZCc + '|' + text_separators + '|[()[\\]{}.,"\'?!\\-]).|' + '\\[(?:(?!' + re.src_ZCc + '|\\]).)*\\]|' + '\\((?:(?!' + re.src_ZCc + '|[)]).)*\\)|' + '\\{(?:(?!' + re.src_ZCc + '|[}]).)*\\}|' + '\\"(?:(?!' + re.src_ZCc + '|["]).)+\\"|' + "\\'(?:(?!" + re.src_ZCc + "|[']).)+\\'|" + "\\'(?=" + re.src_pseudo_letter + '|[-]).|' + // allow `I'm_king` if no pair found
  '\\.{2,4}[a-zA-Z0-9%/]|' + // github has ... in commit range links,
  // google has .... in links (issue #66)
  // Restrict to
  // - english
  // - percent-encoded
  // - parts of file path
  // until more examples found.
  '\\.(?!' + re.src_ZCc + '|[.]).|' + (opts && opts['---'] ? '\\-(?!--(?:[^-]|$))(?:-*)|' // `---` => long dash, terminate
  : '\\-+|') + '\\,(?!' + re.src_ZCc + ').|' + // allow `,,,` in paths
  '\\!(?!' + re.src_ZCc + '|[!]).|' + '\\?(?!' + re.src_ZCc + '|[?]).' + ')+' + '|\\/' + ')?'; // Allow anything in markdown spec, forbid quote (") at the first position
  // because emails enclosed in quotes are far more common

  re.src_email_name = '[\\-;:&=\\+\\$,\\.a-zA-Z0-9_][\\-;:&=\\+\\$,\\"\\.a-zA-Z0-9_]*';
  re.src_xn = 'xn--[a-z0-9\\-]{1,59}'; // More to read about domain names
  // http://serverfault.com/questions/638260/

  re.src_domain_root = // Allow letters & digits (http://test1)
  '(?:' + re.src_xn + '|' + re.src_pseudo_letter + '{1,63}' + ')';
  re.src_domain = '(?:' + re.src_xn + '|' + '(?:' + re.src_pseudo_letter + ')' + '|' + '(?:' + re.src_pseudo_letter + '(?:-|' + re.src_pseudo_letter + '){0,61}' + re.src_pseudo_letter + ')' + ')';
  re.src_host = '(?:' + // Don't need IP check, because digits are already allowed in normal domain names
  //   src_ip4 +
  // '|' +
  '(?:(?:(?:' + re.src_domain + ')\\.)*' + re.src_domain
  /*_root*/
  + ')' + ')';
  re.tpl_host_fuzzy = '(?:' + re.src_ip4 + '|' + '(?:(?:(?:' + re.src_domain + ')\\.)+(?:%TLDS%))' + ')';
  re.tpl_host_no_ip_fuzzy = '(?:(?:(?:' + re.src_domain + ')\\.)+(?:%TLDS%))';
  re.src_host_strict = re.src_host + re.src_host_terminator;
  re.tpl_host_fuzzy_strict = re.tpl_host_fuzzy + re.src_host_terminator;
  re.src_host_port_strict = re.src_host + re.src_port + re.src_host_terminator;
  re.tpl_host_port_fuzzy_strict = re.tpl_host_fuzzy + re.src_port + re.src_host_terminator;
  re.tpl_host_port_no_ip_fuzzy_strict = re.tpl_host_no_ip_fuzzy + re.src_port + re.src_host_terminator; ////////////////////////////////////////////////////////////////////////////////
  // Main rules
  // Rude test fuzzy links by host, for quick deny

  re.tpl_host_fuzzy_test = 'localhost|www\\.|\\.\\d{1,3}\\.|(?:\\.(?:%TLDS%)(?:' + re.src_ZPCc + '|>|$))';
  re.tpl_email_fuzzy = '(^|' + text_separators + '|"|\\(|' + re.src_ZCc + ')' + '(' + re.src_email_name + '@' + re.tpl_host_fuzzy_strict + ')';
  re.tpl_link_fuzzy = // Fuzzy link can't be prepended with .:/\- and non punctuation.
  // but can start with > (markdown blockquote)
  "(^|(?![.:/\\-_@])(?:[$+<=>^`|\uFF5C]|" + re.src_ZPCc + '))' + "((?![$+<=>^`|\uFF5C])" + re.tpl_host_port_fuzzy_strict + re.src_path + ')';
  re.tpl_link_no_ip_fuzzy = // Fuzzy link can't be prepended with .:/\- and non punctuation.
  // but can start with > (markdown blockquote)
  "(^|(?![.:/\\-_@])(?:[$+<=>^`|\uFF5C]|" + re.src_ZPCc + '))' + "((?![$+<=>^`|\uFF5C])" + re.tpl_host_port_no_ip_fuzzy_strict + re.src_path + ')';
  return re;
};

/***/ }),

/***/ "../../node_modules/markdown-it-container/index.js":
/*!***********************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/markdown-it-container/index.js ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
// Process block-level custom containers
//


module.exports = function container_plugin(md, name, options) {
  function validateDefault(params) {
    return params.trim().split(' ', 2)[0] === name;
  }

  function renderDefault(tokens, idx, _options, env, self) {
    // add a class to the opening tag
    if (tokens[idx].nesting === 1) {
      tokens[idx].attrPush(['class', name]);
    }

    return self.renderToken(tokens, idx, _options, env, self);
  }

  options = options || {};
  var min_markers = 3,
      marker_str = options.marker || ':',
      marker_char = marker_str.charCodeAt(0),
      marker_len = marker_str.length,
      validate = options.validate || validateDefault,
      render = options.render || renderDefault;

  function container(state, startLine, endLine, silent) {
    var pos,
        nextLine,
        marker_count,
        markup,
        params,
        token,
        old_parent,
        old_line_max,
        auto_closed = false,
        start = state.bMarks[startLine] + state.tShift[startLine],
        max = state.eMarks[startLine]; // Check out the first character quickly,
    // this should filter out most of non-containers
    //

    if (marker_char !== state.src.charCodeAt(start)) {
      return false;
    } // Check out the rest of the marker string
    //


    for (pos = start + 1; pos <= max; pos++) {
      if (marker_str[(pos - start) % marker_len] !== state.src[pos]) {
        break;
      }
    }

    marker_count = Math.floor((pos - start) / marker_len);

    if (marker_count < min_markers) {
      return false;
    }

    pos -= (pos - start) % marker_len;
    markup = state.src.slice(start, pos);
    params = state.src.slice(pos, max);

    if (!validate(params)) {
      return false;
    } // Since start is found, we can report success here in validation mode
    //


    if (silent) {
      return true;
    } // Search for the end of the block
    //


    nextLine = startLine;

    for (;;) {
      nextLine++;

      if (nextLine >= endLine) {
        // unclosed block should be autoclosed by end of document.
        // also block seems to be autoclosed by end of parent
        break;
      }

      start = state.bMarks[nextLine] + state.tShift[nextLine];
      max = state.eMarks[nextLine];

      if (start < max && state.sCount[nextLine] < state.blkIndent) {
        // non-empty line with negative indent should stop the list:
        // - ```
        //  test
        break;
      }

      if (marker_char !== state.src.charCodeAt(start)) {
        continue;
      }

      if (state.sCount[nextLine] - state.blkIndent >= 4) {
        // closing fence should be indented less than 4 spaces
        continue;
      }

      for (pos = start + 1; pos <= max; pos++) {
        if (marker_str[(pos - start) % marker_len] !== state.src[pos]) {
          break;
        }
      } // closing code fence must be at least as long as the opening one


      if (Math.floor((pos - start) / marker_len) < marker_count) {
        continue;
      } // make sure tail has spaces only


      pos -= (pos - start) % marker_len;
      pos = state.skipSpaces(pos);

      if (pos < max) {
        continue;
      } // found!


      auto_closed = true;
      break;
    }

    old_parent = state.parentType;
    old_line_max = state.lineMax;
    state.parentType = 'container'; // this will prevent lazy continuations from ever going past our end marker

    state.lineMax = nextLine;
    token = state.push('container_' + name + '_open', 'div', 1);
    token.markup = markup;
    token.block = true;
    token.info = params;
    token.map = [startLine, nextLine];
    state.md.block.tokenize(state, startLine + 1, nextLine);
    token = state.push('container_' + name + '_close', 'div', -1);
    token.markup = state.src.slice(start, pos);
    token.block = true;
    state.parentType = old_parent;
    state.lineMax = old_line_max;
    state.line = nextLine + (auto_closed ? 1 : 0);
    return true;
  }

  md.block.ruler.before('fence', 'container_' + name, container, {
    alt: ['paragraph', 'reference', 'blockquote', 'list']
  });
  md.renderer.rules['container_' + name + '_open'] = render;
  md.renderer.rules['container_' + name + '_close'] = render;
};

/***/ }),

/***/ "../../node_modules/markdown-it-custom-block/index.js":
/*!**************************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/markdown-it-custom-block/index.js ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var _slicedToArray = __webpack_require__(/*! @babel/runtime/helpers/slicedToArray */ "../../node_modules/@babel/runtime/helpers/slicedToArray.js");

var embedRE = /@\[([\w-]+)\]\((.+)\)/im;

module.exports = function plugin(md, options) {
  md.renderer.rules.custom = function tokenizeBlock(tokens, idx) {
    var _tokens$idx$info = tokens[idx].info,
        tag = _tokens$idx$info.tag,
        arg = _tokens$idx$info.arg;
    if (!tag) return '';
    return options[tag](arg) + '\n';
  };

  md.block.ruler.before('fence', 'custom', function customEmbed(state, startLine, endLine, silent) {
    var startPos = state.bMarks[startLine] + state.tShift[startLine];
    var maxPos = state.eMarks[startLine];
    var block = state.src.slice(startPos, maxPos);
    var pointer = {
      line: startLine,
      pos: startPos
    }; // XXX wtf

    if (startLine !== 0) {
      var prevLineStartPos = state.bMarks[startLine - 1] + state.tShift[startLine - 1];
      var prevLineMaxPos = state.eMarks[startLine - 1];
      if (prevLineMaxPos > prevLineStartPos) return false;
    } // Check if it's @[tag](arg)


    if (state.src.charCodeAt(pointer.pos) !== 0x40
    /* @ */
    || state.src.charCodeAt(pointer.pos + 1) !== 0x5B
    /* [ */
    ) {
        return false;
      }

    var match = embedRE.exec(block);

    if (!match || match.length < 3) {
      return false;
    }

    var _match = _slicedToArray(match, 3),
        all = _match[0],
        tag = _match[1],
        arg = _match[2];

    pointer.pos += all.length; // Block embed must be at end of input or the next line must be blank.
    // TODO something can be done here to make it work without blank lines

    if (endLine !== pointer.line + 1) {
      var nextLineStartPos = state.bMarks[pointer.line + 1] + state.tShift[pointer.line + 1];
      var nextLineMaxPos = state.eMarks[pointer.line + 1];
      if (nextLineMaxPos > nextLineStartPos) return false;
    }

    if (pointer.line >= endLine) return false;

    if (!silent) {
      var token = state.push('custom', 'div', 0);
      token.markup = state.src.slice(startPos, pointer.pos);
      token.info = {
        arg: arg,
        tag: tag
      };
      token.block = true;
      token.map = [startLine, pointer.line + 1];
      state.line = pointer.line + 1;
    }

    return true;
  }, {
    alt: ['paragraph', 'reference', 'blockquote', 'list']
  });
};

/***/ }),

/***/ "../../node_modules/markdown-it-link-attributes/index.js":
/*!*****************************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/markdown-it-link-attributes/index.js ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
 // Adapted from https://github.com/markdown-it/markdown-it/blob/fbc6b0fed563ba7c00557ab638fd19752f8e759d/docs/architecture.md

function findFirstMatchingConfig(link, configs) {
  var i, config;
  var href = link.attrs[link.attrIndex('href')][1];

  for (i = 0; i < configs.length; ++i) {
    config = configs[i]; // if there is no pattern, config matches for all links
    // otherwise, only return config if href matches the pattern set

    if (!config.pattern || new RegExp(config.pattern).test(href)) {
      return config;
    }
  }
}

function applyAttributes(idx, tokens, attributes) {
  Object.keys(attributes).forEach(function (attr) {
    var attrIndex;
    var value = attributes[attr];

    if (attr === 'className') {
      // when dealing with applying classes
      // programatically, some programmers
      // may prefer to use the className syntax
      attr = 'class';
    }

    attrIndex = tokens[idx].attrIndex(attr);

    if (attrIndex < 0) {
      // attr doesn't exist, add new attribute
      tokens[idx].attrPush([attr, value]);
    } else {
      // attr already exists, overwrite it
      tokens[idx].attrs[attrIndex][1] = value; // replace value of existing attr
    }
  });
}

function markdownitLinkAttributes(md, configs) {
  if (!configs) {
    configs = [];
  } else {
    configs = Array.isArray(configs) ? configs : [configs];
  }

  Object.freeze(configs);
  var defaultRender = md.renderer.rules.link_open || this.defaultRender;

  md.renderer.rules.link_open = function (tokens, idx, options, env, self) {
    var config = findFirstMatchingConfig(tokens[idx], configs);
    var attributes = config && config.attrs;

    if (attributes) {
      applyAttributes(idx, tokens, attributes);
    } // pass token to default renderer.


    return defaultRender(tokens, idx, options, env, self);
  };
}

markdownitLinkAttributes.defaultRender = function (tokens, idx, options, env, self) {
  return self.renderToken(tokens, idx, options);
};

module.exports = markdownitLinkAttributes;

/***/ }),

/***/ "../../node_modules/markdown-it/index.js":
/*!*************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/markdown-it/index.js ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = __webpack_require__(/*! ./lib/ */ "../../node_modules/markdown-it/lib/index.js");

/***/ }),

/***/ "../../node_modules/markdown-it/lib/common/entities.js":
/*!***************************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/markdown-it/lib/common/entities.js ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
// HTML5 entities map: { name -> utf16string }
//

/*eslint quotes:0*/

module.exports = __webpack_require__(/*! entities/lib/maps/entities.json */ "../../node_modules/entities/lib/maps/entities.json");

/***/ }),

/***/ "../../node_modules/markdown-it/lib/common/html_blocks.js":
/*!******************************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/markdown-it/lib/common/html_blocks.js ***!
  \******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
// List of valid html blocks names, accorting to commonmark spec
// http://jgm.github.io/CommonMark/spec.html#html-blocks


module.exports = ['address', 'article', 'aside', 'base', 'basefont', 'blockquote', 'body', 'caption', 'center', 'col', 'colgroup', 'dd', 'details', 'dialog', 'dir', 'div', 'dl', 'dt', 'fieldset', 'figcaption', 'figure', 'footer', 'form', 'frame', 'frameset', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'head', 'header', 'hr', 'html', 'iframe', 'legend', 'li', 'link', 'main', 'menu', 'menuitem', 'meta', 'nav', 'noframes', 'ol', 'optgroup', 'option', 'p', 'param', 'section', 'source', 'summary', 'table', 'tbody', 'td', 'tfoot', 'th', 'thead', 'title', 'tr', 'track', 'ul'];

/***/ }),

/***/ "../../node_modules/markdown-it/lib/common/html_re.js":
/*!**************************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/markdown-it/lib/common/html_re.js ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
// Regexps to match html elements


var attr_name = '[a-zA-Z_:][a-zA-Z0-9:._-]*';
var unquoted = '[^"\'=<>`\\x00-\\x20]+';
var single_quoted = "'[^']*'";
var double_quoted = '"[^"]*"';
var attr_value = '(?:' + unquoted + '|' + single_quoted + '|' + double_quoted + ')';
var attribute = '(?:\\s+' + attr_name + '(?:\\s*=\\s*' + attr_value + ')?)';
var open_tag = '<[A-Za-z][A-Za-z0-9\\-]*' + attribute + '*\\s*\\/?>';
var close_tag = '<\\/[A-Za-z][A-Za-z0-9\\-]*\\s*>';
var comment = '<!---->|<!--(?:-?[^>-])(?:-?[^-])*-->';
var processing = '<[?].*?[?]>';
var declaration = '<![A-Z]+\\s+[^>]*>';
var cdata = '<!\\[CDATA\\[[\\s\\S]*?\\]\\]>';
var HTML_TAG_RE = new RegExp('^(?:' + open_tag + '|' + close_tag + '|' + comment + '|' + processing + '|' + declaration + '|' + cdata + ')');
var HTML_OPEN_CLOSE_TAG_RE = new RegExp('^(?:' + open_tag + '|' + close_tag + ')');
module.exports.HTML_TAG_RE = HTML_TAG_RE;
module.exports.HTML_OPEN_CLOSE_TAG_RE = HTML_OPEN_CLOSE_TAG_RE;

/***/ }),

/***/ "../../node_modules/markdown-it/lib/common/utils.js":
/*!************************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/markdown-it/lib/common/utils.js ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
// Utilities
//


var _typeof = __webpack_require__(/*! @babel/runtime/helpers/typeof */ "../../node_modules/@babel/runtime/helpers/typeof.js");

function _class(obj) {
  return Object.prototype.toString.call(obj);
}

function isString(obj) {
  return _class(obj) === '[object String]';
}

var _hasOwnProperty = Object.prototype.hasOwnProperty;

function has(object, key) {
  return _hasOwnProperty.call(object, key);
} // Merge objects
//


function assign(obj
/*from1, from2, from3, ...*/
) {
  var sources = Array.prototype.slice.call(arguments, 1);
  sources.forEach(function (source) {
    if (!source) {
      return;
    }

    if (_typeof(source) !== 'object') {
      throw new TypeError(source + 'must be object');
    }

    Object.keys(source).forEach(function (key) {
      obj[key] = source[key];
    });
  });
  return obj;
} // Remove element from array and put another array at those position.
// Useful for some operations with tokens


function arrayReplaceAt(src, pos, newElements) {
  return [].concat(src.slice(0, pos), newElements, src.slice(pos + 1));
} ////////////////////////////////////////////////////////////////////////////////


function isValidEntityCode(c) {
  /*eslint no-bitwise:0*/
  // broken sequence
  if (c >= 0xD800 && c <= 0xDFFF) {
    return false;
  } // never used


  if (c >= 0xFDD0 && c <= 0xFDEF) {
    return false;
  }

  if ((c & 0xFFFF) === 0xFFFF || (c & 0xFFFF) === 0xFFFE) {
    return false;
  } // control codes


  if (c >= 0x00 && c <= 0x08) {
    return false;
  }

  if (c === 0x0B) {
    return false;
  }

  if (c >= 0x0E && c <= 0x1F) {
    return false;
  }

  if (c >= 0x7F && c <= 0x9F) {
    return false;
  } // out of range


  if (c > 0x10FFFF) {
    return false;
  }

  return true;
}

function fromCodePoint(c) {
  /*eslint no-bitwise:0*/
  if (c > 0xffff) {
    c -= 0x10000;
    var surrogate1 = 0xd800 + (c >> 10),
        surrogate2 = 0xdc00 + (c & 0x3ff);
    return String.fromCharCode(surrogate1, surrogate2);
  }

  return String.fromCharCode(c);
}

var UNESCAPE_MD_RE = /\\([!"#$%&'()*+,\-.\/:;<=>?@[\\\]^_`{|}~])/g;
var ENTITY_RE = /&([a-z#][a-z0-9]{1,31});/gi;
var UNESCAPE_ALL_RE = new RegExp(UNESCAPE_MD_RE.source + '|' + ENTITY_RE.source, 'gi');
var DIGITAL_ENTITY_TEST_RE = /^#((?:x[a-f0-9]{1,8}|[0-9]{1,8}))/i;

var entities = __webpack_require__(/*! ./entities */ "../../node_modules/markdown-it/lib/common/entities.js");

function replaceEntityPattern(match, name) {
  var code = 0;

  if (has(entities, name)) {
    return entities[name];
  }

  if (name.charCodeAt(0) === 0x23
  /* # */
  && DIGITAL_ENTITY_TEST_RE.test(name)) {
    code = name[1].toLowerCase() === 'x' ? parseInt(name.slice(2), 16) : parseInt(name.slice(1), 10);

    if (isValidEntityCode(code)) {
      return fromCodePoint(code);
    }
  }

  return match;
}
/*function replaceEntities(str) {
  if (str.indexOf('&') < 0) { return str; }

  return str.replace(ENTITY_RE, replaceEntityPattern);
}*/


function unescapeMd(str) {
  if (str.indexOf('\\') < 0) {
    return str;
  }

  return str.replace(UNESCAPE_MD_RE, '$1');
}

function unescapeAll(str) {
  if (str.indexOf('\\') < 0 && str.indexOf('&') < 0) {
    return str;
  }

  return str.replace(UNESCAPE_ALL_RE, function (match, escaped, entity) {
    if (escaped) {
      return escaped;
    }

    return replaceEntityPattern(match, entity);
  });
} ////////////////////////////////////////////////////////////////////////////////


var HTML_ESCAPE_TEST_RE = /[&<>"]/;
var HTML_ESCAPE_REPLACE_RE = /[&<>"]/g;
var HTML_REPLACEMENTS = {
  '&': '&amp;',
  '<': '&lt;',
  '>': '&gt;',
  '"': '&quot;'
};

function replaceUnsafeChar(ch) {
  return HTML_REPLACEMENTS[ch];
}

function escapeHtml(str) {
  if (HTML_ESCAPE_TEST_RE.test(str)) {
    return str.replace(HTML_ESCAPE_REPLACE_RE, replaceUnsafeChar);
  }

  return str;
} ////////////////////////////////////////////////////////////////////////////////


var REGEXP_ESCAPE_RE = /[.?*+^$[\]\\(){}|-]/g;

function escapeRE(str) {
  return str.replace(REGEXP_ESCAPE_RE, '\\$&');
} ////////////////////////////////////////////////////////////////////////////////


function isSpace(code) {
  switch (code) {
    case 0x09:
    case 0x20:
      return true;
  }

  return false;
} // Zs (unicode class) || [\t\f\v\r\n]


function isWhiteSpace(code) {
  if (code >= 0x2000 && code <= 0x200A) {
    return true;
  }

  switch (code) {
    case 0x09: // \t

    case 0x0A: // \n

    case 0x0B: // \v

    case 0x0C: // \f

    case 0x0D: // \r

    case 0x20:
    case 0xA0:
    case 0x1680:
    case 0x202F:
    case 0x205F:
    case 0x3000:
      return true;
  }

  return false;
} ////////////////////////////////////////////////////////////////////////////////

/*eslint-disable max-len*/


var UNICODE_PUNCT_RE = __webpack_require__(/*! uc.micro/categories/P/regex */ "../../node_modules/uc.micro/categories/P/regex.js"); // Currently without astral characters support.


function isPunctChar(ch) {
  return UNICODE_PUNCT_RE.test(ch);
} // Markdown ASCII punctuation characters.
//
// !, ", #, $, %, &, ', (, ), *, +, ,, -, ., /, :, ;, <, =, >, ?, @, [, \, ], ^, _, `, {, |, }, or ~
// http://spec.commonmark.org/0.15/#ascii-punctuation-character
//
// Don't confuse with unicode punctuation !!! It lacks some chars in ascii range.
//


function isMdAsciiPunct(ch) {
  switch (ch) {
    case 0x21
    /* ! */
    :
    case 0x22
    /* " */
    :
    case 0x23
    /* # */
    :
    case 0x24
    /* $ */
    :
    case 0x25
    /* % */
    :
    case 0x26
    /* & */
    :
    case 0x27
    /* ' */
    :
    case 0x28
    /* ( */
    :
    case 0x29
    /* ) */
    :
    case 0x2A
    /* * */
    :
    case 0x2B
    /* + */
    :
    case 0x2C
    /* , */
    :
    case 0x2D
    /* - */
    :
    case 0x2E
    /* . */
    :
    case 0x2F
    /* / */
    :
    case 0x3A
    /* : */
    :
    case 0x3B
    /* ; */
    :
    case 0x3C
    /* < */
    :
    case 0x3D
    /* = */
    :
    case 0x3E
    /* > */
    :
    case 0x3F
    /* ? */
    :
    case 0x40
    /* @ */
    :
    case 0x5B
    /* [ */
    :
    case 0x5C
    /* \ */
    :
    case 0x5D
    /* ] */
    :
    case 0x5E
    /* ^ */
    :
    case 0x5F
    /* _ */
    :
    case 0x60
    /* ` */
    :
    case 0x7B
    /* { */
    :
    case 0x7C
    /* | */
    :
    case 0x7D
    /* } */
    :
    case 0x7E
    /* ~ */
    :
      return true;

    default:
      return false;
  }
} // Hepler to unify [reference labels].
//


function normalizeReference(str) {
  // Trim and collapse whitespace
  //
  str = str.trim().replace(/\s+/g, ' '); // In node v10 'ẞ'.toLowerCase() === 'Ṿ', which is presumed to be a bug
  // fixed in v12 (couldn't find any details).
  //
  // So treat this one as a special case
  // (remove this when node v10 is no longer supported).
  //

  if ('ẞ'.toLowerCase() === 'Ṿ') {
    str = str.replace(/ẞ/g, 'ß');
  } // .toLowerCase().toUpperCase() should get rid of all differences
  // between letter variants.
  //
  // Simple .toLowerCase() doesn't normalize 125 code points correctly,
  // and .toUpperCase doesn't normalize 6 of them (list of exceptions:
  // İ, ϴ, ẞ, Ω, K, Å - those are already uppercased, but have differently
  // uppercased versions).
  //
  // Here's an example showing how it happens. Lets take greek letter omega:
  // uppercase U+0398 (Θ), U+03f4 (ϴ) and lowercase U+03b8 (θ), U+03d1 (ϑ)
  //
  // Unicode entries:
  // 0398;GREEK CAPITAL LETTER THETA;Lu;0;L;;;;;N;;;;03B8;
  // 03B8;GREEK SMALL LETTER THETA;Ll;0;L;;;;;N;;;0398;;0398
  // 03D1;GREEK THETA SYMBOL;Ll;0;L;<compat> 03B8;;;;N;GREEK SMALL LETTER SCRIPT THETA;;0398;;0398
  // 03F4;GREEK CAPITAL THETA SYMBOL;Lu;0;L;<compat> 0398;;;;N;;;;03B8;
  //
  // Case-insensitive comparison should treat all of them as equivalent.
  //
  // But .toLowerCase() doesn't change ϑ (it's already lowercase),
  // and .toUpperCase() doesn't change ϴ (already uppercase).
  //
  // Applying first lower then upper case normalizes any character:
  // '\u0398\u03f4\u03b8\u03d1'.toLowerCase().toUpperCase() === '\u0398\u0398\u0398\u0398'
  //
  // Note: this is equivalent to unicode case folding; unicode normalization
  // is a different step that is not required here.
  //
  // Final result should be uppercased, because it's later stored in an object
  // (this avoid a conflict with Object.prototype members,
  // most notably, `__proto__`)
  //


  return str.toLowerCase().toUpperCase();
} ////////////////////////////////////////////////////////////////////////////////
// Re-export libraries commonly used in both markdown-it and its plugins,
// so plugins won't have to depend on them explicitly, which reduces their
// bundled size (e.g. a browser build).
//


exports.lib = {};
exports.lib.mdurl = __webpack_require__(/*! mdurl */ "../../node_modules/mdurl/index.js");
exports.lib.ucmicro = __webpack_require__(/*! uc.micro */ "../../node_modules/uc.micro/index.js");
exports.assign = assign;
exports.isString = isString;
exports.has = has;
exports.unescapeMd = unescapeMd;
exports.unescapeAll = unescapeAll;
exports.isValidEntityCode = isValidEntityCode;
exports.fromCodePoint = fromCodePoint; // exports.replaceEntities     = replaceEntities;

exports.escapeHtml = escapeHtml;
exports.arrayReplaceAt = arrayReplaceAt;
exports.isSpace = isSpace;
exports.isWhiteSpace = isWhiteSpace;
exports.isMdAsciiPunct = isMdAsciiPunct;
exports.isPunctChar = isPunctChar;
exports.escapeRE = escapeRE;
exports.normalizeReference = normalizeReference;

/***/ }),

/***/ "../../node_modules/markdown-it/lib/helpers/index.js":
/*!*************************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/markdown-it/lib/helpers/index.js ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
// Just a shortcut for bulk export


exports.parseLinkLabel = __webpack_require__(/*! ./parse_link_label */ "../../node_modules/markdown-it/lib/helpers/parse_link_label.js");
exports.parseLinkDestination = __webpack_require__(/*! ./parse_link_destination */ "../../node_modules/markdown-it/lib/helpers/parse_link_destination.js");
exports.parseLinkTitle = __webpack_require__(/*! ./parse_link_title */ "../../node_modules/markdown-it/lib/helpers/parse_link_title.js");

/***/ }),

/***/ "../../node_modules/markdown-it/lib/helpers/parse_link_destination.js":
/*!******************************************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/markdown-it/lib/helpers/parse_link_destination.js ***!
  \******************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
// Parse link destination
//


var unescapeAll = __webpack_require__(/*! ../common/utils */ "../../node_modules/markdown-it/lib/common/utils.js").unescapeAll;

module.exports = function parseLinkDestination(str, pos, max) {
  var code,
      level,
      lines = 0,
      start = pos,
      result = {
    ok: false,
    pos: 0,
    lines: 0,
    str: ''
  };

  if (str.charCodeAt(pos) === 0x3C
  /* < */
  ) {
      pos++;

      while (pos < max) {
        code = str.charCodeAt(pos);

        if (code === 0x0A
        /* \n */
        ) {
            return result;
          }

        if (code === 0x3E
        /* > */
        ) {
            result.pos = pos + 1;
            result.str = unescapeAll(str.slice(start + 1, pos));
            result.ok = true;
            return result;
          }

        if (code === 0x5C
        /* \ */
        && pos + 1 < max) {
          pos += 2;
          continue;
        }

        pos++;
      } // no closing '>'


      return result;
    } // this should be ... } else { ... branch


  level = 0;

  while (pos < max) {
    code = str.charCodeAt(pos);

    if (code === 0x20) {
      break;
    } // ascii control characters


    if (code < 0x20 || code === 0x7F) {
      break;
    }

    if (code === 0x5C
    /* \ */
    && pos + 1 < max) {
      pos += 2;
      continue;
    }

    if (code === 0x28
    /* ( */
    ) {
        level++;
      }

    if (code === 0x29
    /* ) */
    ) {
        if (level === 0) {
          break;
        }

        level--;
      }

    pos++;
  }

  if (start === pos) {
    return result;
  }

  if (level !== 0) {
    return result;
  }

  result.str = unescapeAll(str.slice(start, pos));
  result.lines = lines;
  result.pos = pos;
  result.ok = true;
  return result;
};

/***/ }),

/***/ "../../node_modules/markdown-it/lib/helpers/parse_link_label.js":
/*!************************************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/markdown-it/lib/helpers/parse_link_label.js ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
// Parse link label
//
// this function assumes that first character ("[") already matches;
// returns the end of the label
//


module.exports = function parseLinkLabel(state, start, disableNested) {
  var level,
      found,
      marker,
      prevPos,
      labelEnd = -1,
      max = state.posMax,
      oldPos = state.pos;
  state.pos = start + 1;
  level = 1;

  while (state.pos < max) {
    marker = state.src.charCodeAt(state.pos);

    if (marker === 0x5D
    /* ] */
    ) {
        level--;

        if (level === 0) {
          found = true;
          break;
        }
      }

    prevPos = state.pos;
    state.md.inline.skipToken(state);

    if (marker === 0x5B
    /* [ */
    ) {
        if (prevPos === state.pos - 1) {
          // increase level if we find text `[`, which is not a part of any token
          level++;
        } else if (disableNested) {
          state.pos = oldPos;
          return -1;
        }
      }
  }

  if (found) {
    labelEnd = state.pos;
  } // restore old state


  state.pos = oldPos;
  return labelEnd;
};

/***/ }),

/***/ "../../node_modules/markdown-it/lib/helpers/parse_link_title.js":
/*!************************************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/markdown-it/lib/helpers/parse_link_title.js ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
// Parse link title
//


var unescapeAll = __webpack_require__(/*! ../common/utils */ "../../node_modules/markdown-it/lib/common/utils.js").unescapeAll;

module.exports = function parseLinkTitle(str, pos, max) {
  var code,
      marker,
      lines = 0,
      start = pos,
      result = {
    ok: false,
    pos: 0,
    lines: 0,
    str: ''
  };

  if (pos >= max) {
    return result;
  }

  marker = str.charCodeAt(pos);

  if (marker !== 0x22
  /* " */
  && marker !== 0x27
  /* ' */
  && marker !== 0x28
  /* ( */
  ) {
      return result;
    }

  pos++; // if opening marker is "(", switch it to closing marker ")"

  if (marker === 0x28) {
    marker = 0x29;
  }

  while (pos < max) {
    code = str.charCodeAt(pos);

    if (code === marker) {
      result.pos = pos + 1;
      result.lines = lines;
      result.str = unescapeAll(str.slice(start + 1, pos));
      result.ok = true;
      return result;
    } else if (code === 0x0A) {
      lines++;
    } else if (code === 0x5C
    /* \ */
    && pos + 1 < max) {
      pos++;

      if (str.charCodeAt(pos) === 0x0A) {
        lines++;
      }
    }

    pos++;
  }

  return result;
};

/***/ }),

/***/ "../../node_modules/markdown-it/lib/index.js":
/*!*****************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/markdown-it/lib/index.js ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
// Main parser class


var utils = __webpack_require__(/*! ./common/utils */ "../../node_modules/markdown-it/lib/common/utils.js");

var helpers = __webpack_require__(/*! ./helpers */ "../../node_modules/markdown-it/lib/helpers/index.js");

var Renderer = __webpack_require__(/*! ./renderer */ "../../node_modules/markdown-it/lib/renderer.js");

var ParserCore = __webpack_require__(/*! ./parser_core */ "../../node_modules/markdown-it/lib/parser_core.js");

var ParserBlock = __webpack_require__(/*! ./parser_block */ "../../node_modules/markdown-it/lib/parser_block.js");

var ParserInline = __webpack_require__(/*! ./parser_inline */ "../../node_modules/markdown-it/lib/parser_inline.js");

var LinkifyIt = __webpack_require__(/*! linkify-it */ "../../node_modules/linkify-it/index.js");

var mdurl = __webpack_require__(/*! mdurl */ "../../node_modules/mdurl/index.js");

var punycode = __webpack_require__(/*! punycode */ "../../node_modules/node-libs-browser/node_modules/punycode/punycode.js");

var config = {
  'default': __webpack_require__(/*! ./presets/default */ "../../node_modules/markdown-it/lib/presets/default.js"),
  zero: __webpack_require__(/*! ./presets/zero */ "../../node_modules/markdown-it/lib/presets/zero.js"),
  commonmark: __webpack_require__(/*! ./presets/commonmark */ "../../node_modules/markdown-it/lib/presets/commonmark.js")
}; ////////////////////////////////////////////////////////////////////////////////
//
// This validator can prohibit more than really needed to prevent XSS. It's a
// tradeoff to keep code simple and to be secure by default.
//
// If you need different setup - override validator method as you wish. Or
// replace it with dummy function and use external sanitizer.
//

var BAD_PROTO_RE = /^(vbscript|javascript|file|data):/;
var GOOD_DATA_RE = /^data:image\/(gif|png|jpeg|webp);/;

function validateLink(url) {
  // url should be normalized at this point, and existing entities are decoded
  var str = url.trim().toLowerCase();
  return BAD_PROTO_RE.test(str) ? GOOD_DATA_RE.test(str) ? true : false : true;
} ////////////////////////////////////////////////////////////////////////////////


var RECODE_HOSTNAME_FOR = ['http:', 'https:', 'mailto:'];

function normalizeLink(url) {
  var parsed = mdurl.parse(url, true);

  if (parsed.hostname) {
    // Encode hostnames in urls like:
    // `http://host/`, `https://host/`, `mailto:user@host`, `//host/`
    //
    // We don't encode unknown schemas, because it's likely that we encode
    // something we shouldn't (e.g. `skype:name` treated as `skype:host`)
    //
    if (!parsed.protocol || RECODE_HOSTNAME_FOR.indexOf(parsed.protocol) >= 0) {
      try {
        parsed.hostname = punycode.toASCII(parsed.hostname);
      } catch (er) {
        /**/
      }
    }
  }

  return mdurl.encode(mdurl.format(parsed));
}

function normalizeLinkText(url) {
  var parsed = mdurl.parse(url, true);

  if (parsed.hostname) {
    // Encode hostnames in urls like:
    // `http://host/`, `https://host/`, `mailto:user@host`, `//host/`
    //
    // We don't encode unknown schemas, because it's likely that we encode
    // something we shouldn't (e.g. `skype:name` treated as `skype:host`)
    //
    if (!parsed.protocol || RECODE_HOSTNAME_FOR.indexOf(parsed.protocol) >= 0) {
      try {
        parsed.hostname = punycode.toUnicode(parsed.hostname);
      } catch (er) {
        /**/
      }
    }
  }

  return mdurl.decode(mdurl.format(parsed));
}
/**
 * class MarkdownIt
 *
 * Main parser/renderer class.
 *
 * ##### Usage
 *
 * ```javascript
 * // node.js, "classic" way:
 * var MarkdownIt = require('markdown-it'),
 *     md = new MarkdownIt();
 * var result = md.render('# markdown-it rulezz!');
 *
 * // node.js, the same, but with sugar:
 * var md = require('markdown-it')();
 * var result = md.render('# markdown-it rulezz!');
 *
 * // browser without AMD, added to "window" on script load
 * // Note, there are no dash.
 * var md = window.markdownit();
 * var result = md.render('# markdown-it rulezz!');
 * ```
 *
 * Single line rendering, without paragraph wrap:
 *
 * ```javascript
 * var md = require('markdown-it')();
 * var result = md.renderInline('__markdown-it__ rulezz!');
 * ```
 **/

/**
 * new MarkdownIt([presetName, options])
 * - presetName (String): optional, `commonmark` / `zero`
 * - options (Object)
 *
 * Creates parser instanse with given config. Can be called without `new`.
 *
 * ##### presetName
 *
 * MarkdownIt provides named presets as a convenience to quickly
 * enable/disable active syntax rules and options for common use cases.
 *
 * - ["commonmark"](https://github.com/markdown-it/markdown-it/blob/master/lib/presets/commonmark.js) -
 *   configures parser to strict [CommonMark](http://commonmark.org/) mode.
 * - [default](https://github.com/markdown-it/markdown-it/blob/master/lib/presets/default.js) -
 *   similar to GFM, used when no preset name given. Enables all available rules,
 *   but still without html, typographer & autolinker.
 * - ["zero"](https://github.com/markdown-it/markdown-it/blob/master/lib/presets/zero.js) -
 *   all rules disabled. Useful to quickly setup your config via `.enable()`.
 *   For example, when you need only `bold` and `italic` markup and nothing else.
 *
 * ##### options:
 *
 * - __html__ - `false`. Set `true` to enable HTML tags in source. Be careful!
 *   That's not safe! You may need external sanitizer to protect output from XSS.
 *   It's better to extend features via plugins, instead of enabling HTML.
 * - __xhtmlOut__ - `false`. Set `true` to add '/' when closing single tags
 *   (`<br />`). This is needed only for full CommonMark compatibility. In real
 *   world you will need HTML output.
 * - __breaks__ - `false`. Set `true` to convert `\n` in paragraphs into `<br>`.
 * - __langPrefix__ - `language-`. CSS language class prefix for fenced blocks.
 *   Can be useful for external highlighters.
 * - __linkify__ - `false`. Set `true` to autoconvert URL-like text to links.
 * - __typographer__  - `false`. Set `true` to enable [some language-neutral
 *   replacement](https://github.com/markdown-it/markdown-it/blob/master/lib/rules_core/replacements.js) +
 *   quotes beautification (smartquotes).
 * - __quotes__ - `“”‘’`, String or Array. Double + single quotes replacement
 *   pairs, when typographer enabled and smartquotes on. For example, you can
 *   use `'«»„“'` for Russian, `'„“‚‘'` for German, and
 *   `['«\xA0', '\xA0»', '‹\xA0', '\xA0›']` for French (including nbsp).
 * - __highlight__ - `null`. Highlighter function for fenced code blocks.
 *   Highlighter `function (str, lang)` should return escaped HTML. It can also
 *   return empty string if the source was not changed and should be escaped
 *   externaly. If result starts with <pre... internal wrapper is skipped.
 *
 * ##### Example
 *
 * ```javascript
 * // commonmark mode
 * var md = require('markdown-it')('commonmark');
 *
 * // default mode
 * var md = require('markdown-it')();
 *
 * // enable everything
 * var md = require('markdown-it')({
 *   html: true,
 *   linkify: true,
 *   typographer: true
 * });
 * ```
 *
 * ##### Syntax highlighting
 *
 * ```js
 * var hljs = require('highlight.js') // https://highlightjs.org/
 *
 * var md = require('markdown-it')({
 *   highlight: function (str, lang) {
 *     if (lang && hljs.getLanguage(lang)) {
 *       try {
 *         return hljs.highlight(lang, str, true).value;
 *       } catch (__) {}
 *     }
 *
 *     return ''; // use external default escaping
 *   }
 * });
 * ```
 *
 * Or with full wrapper override (if you need assign class to `<pre>`):
 *
 * ```javascript
 * var hljs = require('highlight.js') // https://highlightjs.org/
 *
 * // Actual default values
 * var md = require('markdown-it')({
 *   highlight: function (str, lang) {
 *     if (lang && hljs.getLanguage(lang)) {
 *       try {
 *         return '<pre class="hljs"><code>' +
 *                hljs.highlight(lang, str, true).value +
 *                '</code></pre>';
 *       } catch (__) {}
 *     }
 *
 *     return '<pre class="hljs"><code>' + md.utils.escapeHtml(str) + '</code></pre>';
 *   }
 * });
 * ```
 *
 **/


function MarkdownIt(presetName, options) {
  if (!(this instanceof MarkdownIt)) {
    return new MarkdownIt(presetName, options);
  }

  if (!options) {
    if (!utils.isString(presetName)) {
      options = presetName || {};
      presetName = 'default';
    }
  }
  /**
   * MarkdownIt#inline -> ParserInline
   *
   * Instance of [[ParserInline]]. You may need it to add new rules when
   * writing plugins. For simple rules control use [[MarkdownIt.disable]] and
   * [[MarkdownIt.enable]].
   **/


  this.inline = new ParserInline();
  /**
   * MarkdownIt#block -> ParserBlock
   *
   * Instance of [[ParserBlock]]. You may need it to add new rules when
   * writing plugins. For simple rules control use [[MarkdownIt.disable]] and
   * [[MarkdownIt.enable]].
   **/

  this.block = new ParserBlock();
  /**
   * MarkdownIt#core -> Core
   *
   * Instance of [[Core]] chain executor. You may need it to add new rules when
   * writing plugins. For simple rules control use [[MarkdownIt.disable]] and
   * [[MarkdownIt.enable]].
   **/

  this.core = new ParserCore();
  /**
   * MarkdownIt#renderer -> Renderer
   *
   * Instance of [[Renderer]]. Use it to modify output look. Or to add rendering
   * rules for new token types, generated by plugins.
   *
   * ##### Example
   *
   * ```javascript
   * var md = require('markdown-it')();
   *
   * function myToken(tokens, idx, options, env, self) {
   *   //...
   *   return result;
   * };
   *
   * md.renderer.rules['my_token'] = myToken
   * ```
   *
   * See [[Renderer]] docs and [source code](https://github.com/markdown-it/markdown-it/blob/master/lib/renderer.js).
   **/

  this.renderer = new Renderer();
  /**
   * MarkdownIt#linkify -> LinkifyIt
   *
   * [linkify-it](https://github.com/markdown-it/linkify-it) instance.
   * Used by [linkify](https://github.com/markdown-it/markdown-it/blob/master/lib/rules_core/linkify.js)
   * rule.
   **/

  this.linkify = new LinkifyIt();
  /**
   * MarkdownIt#validateLink(url) -> Boolean
   *
   * Link validation function. CommonMark allows too much in links. By default
   * we disable `javascript:`, `vbscript:`, `file:` schemas, and almost all `data:...` schemas
   * except some embedded image types.
   *
   * You can change this behaviour:
   *
   * ```javascript
   * var md = require('markdown-it')();
   * // enable everything
   * md.validateLink = function () { return true; }
   * ```
   **/

  this.validateLink = validateLink;
  /**
   * MarkdownIt#normalizeLink(url) -> String
   *
   * Function used to encode link url to a machine-readable format,
   * which includes url-encoding, punycode, etc.
   **/

  this.normalizeLink = normalizeLink;
  /**
   * MarkdownIt#normalizeLinkText(url) -> String
   *
   * Function used to decode link url to a human-readable format`
   **/

  this.normalizeLinkText = normalizeLinkText; // Expose utils & helpers for easy acces from plugins

  /**
   * MarkdownIt#utils -> utils
   *
   * Assorted utility functions, useful to write plugins. See details
   * [here](https://github.com/markdown-it/markdown-it/blob/master/lib/common/utils.js).
   **/

  this.utils = utils;
  /**
   * MarkdownIt#helpers -> helpers
   *
   * Link components parser functions, useful to write plugins. See details
   * [here](https://github.com/markdown-it/markdown-it/blob/master/lib/helpers).
   **/

  this.helpers = utils.assign({}, helpers);
  this.options = {};
  this.configure(presetName);

  if (options) {
    this.set(options);
  }
}
/** chainable
 * MarkdownIt.set(options)
 *
 * Set parser options (in the same format as in constructor). Probably, you
 * will never need it, but you can change options after constructor call.
 *
 * ##### Example
 *
 * ```javascript
 * var md = require('markdown-it')()
 *             .set({ html: true, breaks: true })
 *             .set({ typographer, true });
 * ```
 *
 * __Note:__ To achieve the best possible performance, don't modify a
 * `markdown-it` instance options on the fly. If you need multiple configurations
 * it's best to create multiple instances and initialize each with separate
 * config.
 **/


MarkdownIt.prototype.set = function (options) {
  utils.assign(this.options, options);
  return this;
};
/** chainable, internal
 * MarkdownIt.configure(presets)
 *
 * Batch load of all options and compenent settings. This is internal method,
 * and you probably will not need it. But if you with - see available presets
 * and data structure [here](https://github.com/markdown-it/markdown-it/tree/master/lib/presets)
 *
 * We strongly recommend to use presets instead of direct config loads. That
 * will give better compatibility with next versions.
 **/


MarkdownIt.prototype.configure = function (presets) {
  var self = this,
      presetName;

  if (utils.isString(presets)) {
    presetName = presets;
    presets = config[presetName];

    if (!presets) {
      throw new Error('Wrong `markdown-it` preset "' + presetName + '", check name');
    }
  }

  if (!presets) {
    throw new Error('Wrong `markdown-it` preset, can\'t be empty');
  }

  if (presets.options) {
    self.set(presets.options);
  }

  if (presets.components) {
    Object.keys(presets.components).forEach(function (name) {
      if (presets.components[name].rules) {
        self[name].ruler.enableOnly(presets.components[name].rules);
      }

      if (presets.components[name].rules2) {
        self[name].ruler2.enableOnly(presets.components[name].rules2);
      }
    });
  }

  return this;
};
/** chainable
 * MarkdownIt.enable(list, ignoreInvalid)
 * - list (String|Array): rule name or list of rule names to enable
 * - ignoreInvalid (Boolean): set `true` to ignore errors when rule not found.
 *
 * Enable list or rules. It will automatically find appropriate components,
 * containing rules with given names. If rule not found, and `ignoreInvalid`
 * not set - throws exception.
 *
 * ##### Example
 *
 * ```javascript
 * var md = require('markdown-it')()
 *             .enable(['sub', 'sup'])
 *             .disable('smartquotes');
 * ```
 **/


MarkdownIt.prototype.enable = function (list, ignoreInvalid) {
  var result = [];

  if (!Array.isArray(list)) {
    list = [list];
  }

  ['core', 'block', 'inline'].forEach(function (chain) {
    result = result.concat(this[chain].ruler.enable(list, true));
  }, this);
  result = result.concat(this.inline.ruler2.enable(list, true));
  var missed = list.filter(function (name) {
    return result.indexOf(name) < 0;
  });

  if (missed.length && !ignoreInvalid) {
    throw new Error('MarkdownIt. Failed to enable unknown rule(s): ' + missed);
  }

  return this;
};
/** chainable
 * MarkdownIt.disable(list, ignoreInvalid)
 * - list (String|Array): rule name or list of rule names to disable.
 * - ignoreInvalid (Boolean): set `true` to ignore errors when rule not found.
 *
 * The same as [[MarkdownIt.enable]], but turn specified rules off.
 **/


MarkdownIt.prototype.disable = function (list, ignoreInvalid) {
  var result = [];

  if (!Array.isArray(list)) {
    list = [list];
  }

  ['core', 'block', 'inline'].forEach(function (chain) {
    result = result.concat(this[chain].ruler.disable(list, true));
  }, this);
  result = result.concat(this.inline.ruler2.disable(list, true));
  var missed = list.filter(function (name) {
    return result.indexOf(name) < 0;
  });

  if (missed.length && !ignoreInvalid) {
    throw new Error('MarkdownIt. Failed to disable unknown rule(s): ' + missed);
  }

  return this;
};
/** chainable
 * MarkdownIt.use(plugin, params)
 *
 * Load specified plugin with given params into current parser instance.
 * It's just a sugar to call `plugin(md, params)` with curring.
 *
 * ##### Example
 *
 * ```javascript
 * var iterator = require('markdown-it-for-inline');
 * var md = require('markdown-it')()
 *             .use(iterator, 'foo_replace', 'text', function (tokens, idx) {
 *               tokens[idx].content = tokens[idx].content.replace(/foo/g, 'bar');
 *             });
 * ```
 **/


MarkdownIt.prototype.use = function (plugin
/*, params, ... */
) {
  var args = [this].concat(Array.prototype.slice.call(arguments, 1));
  plugin.apply(plugin, args);
  return this;
};
/** internal
 * MarkdownIt.parse(src, env) -> Array
 * - src (String): source string
 * - env (Object): environment sandbox
 *
 * Parse input string and returns list of block tokens (special token type
 * "inline" will contain list of inline tokens). You should not call this
 * method directly, until you write custom renderer (for example, to produce
 * AST).
 *
 * `env` is used to pass data between "distributed" rules and return additional
 * metadata like reference info, needed for the renderer. It also can be used to
 * inject data in specific cases. Usually, you will be ok to pass `{}`,
 * and then pass updated object to renderer.
 **/


MarkdownIt.prototype.parse = function (src, env) {
  if (typeof src !== 'string') {
    throw new Error('Input data should be a String');
  }

  var state = new this.core.State(src, this, env);
  this.core.process(state);
  return state.tokens;
};
/**
 * MarkdownIt.render(src [, env]) -> String
 * - src (String): source string
 * - env (Object): environment sandbox
 *
 * Render markdown string into html. It does all magic for you :).
 *
 * `env` can be used to inject additional metadata (`{}` by default).
 * But you will not need it with high probability. See also comment
 * in [[MarkdownIt.parse]].
 **/


MarkdownIt.prototype.render = function (src, env) {
  env = env || {};
  return this.renderer.render(this.parse(src, env), this.options, env);
};
/** internal
 * MarkdownIt.parseInline(src, env) -> Array
 * - src (String): source string
 * - env (Object): environment sandbox
 *
 * The same as [[MarkdownIt.parse]] but skip all block rules. It returns the
 * block tokens list with the single `inline` element, containing parsed inline
 * tokens in `children` property. Also updates `env` object.
 **/


MarkdownIt.prototype.parseInline = function (src, env) {
  var state = new this.core.State(src, this, env);
  state.inlineMode = true;
  this.core.process(state);
  return state.tokens;
};
/**
 * MarkdownIt.renderInline(src [, env]) -> String
 * - src (String): source string
 * - env (Object): environment sandbox
 *
 * Similar to [[MarkdownIt.render]] but for single paragraph content. Result
 * will NOT be wrapped into `<p>` tags.
 **/


MarkdownIt.prototype.renderInline = function (src, env) {
  env = env || {};
  return this.renderer.render(this.parseInline(src, env), this.options, env);
};

module.exports = MarkdownIt;

/***/ }),

/***/ "../../node_modules/markdown-it/lib/parser_block.js":
/*!************************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/markdown-it/lib/parser_block.js ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/** internal
 * class ParserBlock
 *
 * Block-level tokenizer.
 **/


var Ruler = __webpack_require__(/*! ./ruler */ "../../node_modules/markdown-it/lib/ruler.js");

var _rules = [// First 2 params - rule name & source. Secondary array - list of rules,
// which can be terminated by this one.
['table', __webpack_require__(/*! ./rules_block/table */ "../../node_modules/markdown-it/lib/rules_block/table.js"), ['paragraph', 'reference']], ['code', __webpack_require__(/*! ./rules_block/code */ "../../node_modules/markdown-it/lib/rules_block/code.js")], ['fence', __webpack_require__(/*! ./rules_block/fence */ "../../node_modules/markdown-it/lib/rules_block/fence.js"), ['paragraph', 'reference', 'blockquote', 'list']], ['blockquote', __webpack_require__(/*! ./rules_block/blockquote */ "../../node_modules/markdown-it/lib/rules_block/blockquote.js"), ['paragraph', 'reference', 'blockquote', 'list']], ['hr', __webpack_require__(/*! ./rules_block/hr */ "../../node_modules/markdown-it/lib/rules_block/hr.js"), ['paragraph', 'reference', 'blockquote', 'list']], ['list', __webpack_require__(/*! ./rules_block/list */ "../../node_modules/markdown-it/lib/rules_block/list.js"), ['paragraph', 'reference', 'blockquote']], ['reference', __webpack_require__(/*! ./rules_block/reference */ "../../node_modules/markdown-it/lib/rules_block/reference.js")], ['heading', __webpack_require__(/*! ./rules_block/heading */ "../../node_modules/markdown-it/lib/rules_block/heading.js"), ['paragraph', 'reference', 'blockquote']], ['lheading', __webpack_require__(/*! ./rules_block/lheading */ "../../node_modules/markdown-it/lib/rules_block/lheading.js")], ['html_block', __webpack_require__(/*! ./rules_block/html_block */ "../../node_modules/markdown-it/lib/rules_block/html_block.js"), ['paragraph', 'reference', 'blockquote']], ['paragraph', __webpack_require__(/*! ./rules_block/paragraph */ "../../node_modules/markdown-it/lib/rules_block/paragraph.js")]];
/**
 * new ParserBlock()
 **/

function ParserBlock() {
  /**
   * ParserBlock#ruler -> Ruler
   *
   * [[Ruler]] instance. Keep configuration of block rules.
   **/
  this.ruler = new Ruler();

  for (var i = 0; i < _rules.length; i++) {
    this.ruler.push(_rules[i][0], _rules[i][1], {
      alt: (_rules[i][2] || []).slice()
    });
  }
} // Generate tokens for input range
//


ParserBlock.prototype.tokenize = function (state, startLine, endLine) {
  var ok,
      i,
      rules = this.ruler.getRules(''),
      len = rules.length,
      line = startLine,
      hasEmptyLines = false,
      maxNesting = state.md.options.maxNesting;

  while (line < endLine) {
    state.line = line = state.skipEmptyLines(line);

    if (line >= endLine) {
      break;
    } // Termination condition for nested calls.
    // Nested calls currently used for blockquotes & lists


    if (state.sCount[line] < state.blkIndent) {
      break;
    } // If nesting level exceeded - skip tail to the end. That's not ordinary
    // situation and we should not care about content.


    if (state.level >= maxNesting) {
      state.line = endLine;
      break;
    } // Try all possible rules.
    // On success, rule should:
    //
    // - update `state.line`
    // - update `state.tokens`
    // - return true


    for (i = 0; i < len; i++) {
      ok = rules[i](state, line, endLine, false);

      if (ok) {
        break;
      }
    } // set state.tight if we had an empty line before current tag
    // i.e. latest empty line should not count


    state.tight = !hasEmptyLines; // paragraph might "eat" one newline after it in nested lists

    if (state.isEmpty(state.line - 1)) {
      hasEmptyLines = true;
    }

    line = state.line;

    if (line < endLine && state.isEmpty(line)) {
      hasEmptyLines = true;
      line++;
      state.line = line;
    }
  }
};
/**
 * ParserBlock.parse(str, md, env, outTokens)
 *
 * Process input string and push block tokens into `outTokens`
 **/


ParserBlock.prototype.parse = function (src, md, env, outTokens) {
  var state;

  if (!src) {
    return;
  }

  state = new this.State(src, md, env, outTokens);
  this.tokenize(state, state.line, state.lineMax);
};

ParserBlock.prototype.State = __webpack_require__(/*! ./rules_block/state_block */ "../../node_modules/markdown-it/lib/rules_block/state_block.js");
module.exports = ParserBlock;

/***/ }),

/***/ "../../node_modules/markdown-it/lib/parser_core.js":
/*!***********************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/markdown-it/lib/parser_core.js ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/** internal
 * class Core
 *
 * Top-level rules executor. Glues block/inline parsers and does intermediate
 * transformations.
 **/


var Ruler = __webpack_require__(/*! ./ruler */ "../../node_modules/markdown-it/lib/ruler.js");

var _rules = [['normalize', __webpack_require__(/*! ./rules_core/normalize */ "../../node_modules/markdown-it/lib/rules_core/normalize.js")], ['block', __webpack_require__(/*! ./rules_core/block */ "../../node_modules/markdown-it/lib/rules_core/block.js")], ['inline', __webpack_require__(/*! ./rules_core/inline */ "../../node_modules/markdown-it/lib/rules_core/inline.js")], ['linkify', __webpack_require__(/*! ./rules_core/linkify */ "../../node_modules/markdown-it/lib/rules_core/linkify.js")], ['replacements', __webpack_require__(/*! ./rules_core/replacements */ "../../node_modules/markdown-it/lib/rules_core/replacements.js")], ['smartquotes', __webpack_require__(/*! ./rules_core/smartquotes */ "../../node_modules/markdown-it/lib/rules_core/smartquotes.js")]];
/**
 * new Core()
 **/

function Core() {
  /**
   * Core#ruler -> Ruler
   *
   * [[Ruler]] instance. Keep configuration of core rules.
   **/
  this.ruler = new Ruler();

  for (var i = 0; i < _rules.length; i++) {
    this.ruler.push(_rules[i][0], _rules[i][1]);
  }
}
/**
 * Core.process(state)
 *
 * Executes core chain rules.
 **/


Core.prototype.process = function (state) {
  var i, l, rules;
  rules = this.ruler.getRules('');

  for (i = 0, l = rules.length; i < l; i++) {
    rules[i](state);
  }
};

Core.prototype.State = __webpack_require__(/*! ./rules_core/state_core */ "../../node_modules/markdown-it/lib/rules_core/state_core.js");
module.exports = Core;

/***/ }),

/***/ "../../node_modules/markdown-it/lib/parser_inline.js":
/*!*************************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/markdown-it/lib/parser_inline.js ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/** internal
 * class ParserInline
 *
 * Tokenizes paragraph content.
 **/


var Ruler = __webpack_require__(/*! ./ruler */ "../../node_modules/markdown-it/lib/ruler.js"); ////////////////////////////////////////////////////////////////////////////////
// Parser rules


var _rules = [['text', __webpack_require__(/*! ./rules_inline/text */ "../../node_modules/markdown-it/lib/rules_inline/text.js")], ['newline', __webpack_require__(/*! ./rules_inline/newline */ "../../node_modules/markdown-it/lib/rules_inline/newline.js")], ['escape', __webpack_require__(/*! ./rules_inline/escape */ "../../node_modules/markdown-it/lib/rules_inline/escape.js")], ['backticks', __webpack_require__(/*! ./rules_inline/backticks */ "../../node_modules/markdown-it/lib/rules_inline/backticks.js")], ['strikethrough', __webpack_require__(/*! ./rules_inline/strikethrough */ "../../node_modules/markdown-it/lib/rules_inline/strikethrough.js").tokenize], ['emphasis', __webpack_require__(/*! ./rules_inline/emphasis */ "../../node_modules/markdown-it/lib/rules_inline/emphasis.js").tokenize], ['link', __webpack_require__(/*! ./rules_inline/link */ "../../node_modules/markdown-it/lib/rules_inline/link.js")], ['image', __webpack_require__(/*! ./rules_inline/image */ "../../node_modules/markdown-it/lib/rules_inline/image.js")], ['autolink', __webpack_require__(/*! ./rules_inline/autolink */ "../../node_modules/markdown-it/lib/rules_inline/autolink.js")], ['html_inline', __webpack_require__(/*! ./rules_inline/html_inline */ "../../node_modules/markdown-it/lib/rules_inline/html_inline.js")], ['entity', __webpack_require__(/*! ./rules_inline/entity */ "../../node_modules/markdown-it/lib/rules_inline/entity.js")]];
var _rules2 = [['balance_pairs', __webpack_require__(/*! ./rules_inline/balance_pairs */ "../../node_modules/markdown-it/lib/rules_inline/balance_pairs.js")], ['strikethrough', __webpack_require__(/*! ./rules_inline/strikethrough */ "../../node_modules/markdown-it/lib/rules_inline/strikethrough.js").postProcess], ['emphasis', __webpack_require__(/*! ./rules_inline/emphasis */ "../../node_modules/markdown-it/lib/rules_inline/emphasis.js").postProcess], ['text_collapse', __webpack_require__(/*! ./rules_inline/text_collapse */ "../../node_modules/markdown-it/lib/rules_inline/text_collapse.js")]];
/**
 * new ParserInline()
 **/

function ParserInline() {
  var i;
  /**
   * ParserInline#ruler -> Ruler
   *
   * [[Ruler]] instance. Keep configuration of inline rules.
   **/

  this.ruler = new Ruler();

  for (i = 0; i < _rules.length; i++) {
    this.ruler.push(_rules[i][0], _rules[i][1]);
  }
  /**
   * ParserInline#ruler2 -> Ruler
   *
   * [[Ruler]] instance. Second ruler used for post-processing
   * (e.g. in emphasis-like rules).
   **/


  this.ruler2 = new Ruler();

  for (i = 0; i < _rules2.length; i++) {
    this.ruler2.push(_rules2[i][0], _rules2[i][1]);
  }
} // Skip single token by running all rules in validation mode;
// returns `true` if any rule reported success
//


ParserInline.prototype.skipToken = function (state) {
  var ok,
      i,
      pos = state.pos,
      rules = this.ruler.getRules(''),
      len = rules.length,
      maxNesting = state.md.options.maxNesting,
      cache = state.cache;

  if (typeof cache[pos] !== 'undefined') {
    state.pos = cache[pos];
    return;
  }

  if (state.level < maxNesting) {
    for (i = 0; i < len; i++) {
      // Increment state.level and decrement it later to limit recursion.
      // It's harmless to do here, because no tokens are created. But ideally,
      // we'd need a separate private state variable for this purpose.
      //
      state.level++;
      ok = rules[i](state, true);
      state.level--;

      if (ok) {
        break;
      }
    }
  } else {
    // Too much nesting, just skip until the end of the paragraph.
    //
    // NOTE: this will cause links to behave incorrectly in the following case,
    //       when an amount of `[` is exactly equal to `maxNesting + 1`:
    //
    //       [[[[[[[[[[[[[[[[[[[[[foo]()
    //
    // TODO: remove this workaround when CM standard will allow nested links
    //       (we can replace it by preventing links from being parsed in
    //       validation mode)
    //
    state.pos = state.posMax;
  }

  if (!ok) {
    state.pos++;
  }

  cache[pos] = state.pos;
}; // Generate tokens for input range
//


ParserInline.prototype.tokenize = function (state) {
  var ok,
      i,
      rules = this.ruler.getRules(''),
      len = rules.length,
      end = state.posMax,
      maxNesting = state.md.options.maxNesting;

  while (state.pos < end) {
    // Try all possible rules.
    // On success, rule should:
    //
    // - update `state.pos`
    // - update `state.tokens`
    // - return true
    if (state.level < maxNesting) {
      for (i = 0; i < len; i++) {
        ok = rules[i](state, false);

        if (ok) {
          break;
        }
      }
    }

    if (ok) {
      if (state.pos >= end) {
        break;
      }

      continue;
    }

    state.pending += state.src[state.pos++];
  }

  if (state.pending) {
    state.pushPending();
  }
};
/**
 * ParserInline.parse(str, md, env, outTokens)
 *
 * Process input string and push inline tokens into `outTokens`
 **/


ParserInline.prototype.parse = function (str, md, env, outTokens) {
  var i, rules, len;
  var state = new this.State(str, md, env, outTokens);
  this.tokenize(state);
  rules = this.ruler2.getRules('');
  len = rules.length;

  for (i = 0; i < len; i++) {
    rules[i](state);
  }
};

ParserInline.prototype.State = __webpack_require__(/*! ./rules_inline/state_inline */ "../../node_modules/markdown-it/lib/rules_inline/state_inline.js");
module.exports = ParserInline;

/***/ }),

/***/ "../../node_modules/markdown-it/lib/presets/commonmark.js":
/*!******************************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/markdown-it/lib/presets/commonmark.js ***!
  \******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
// Commonmark default options


module.exports = {
  options: {
    html: true,
    // Enable HTML tags in source
    xhtmlOut: true,
    // Use '/' to close single tags (<br />)
    breaks: false,
    // Convert '\n' in paragraphs into <br>
    langPrefix: 'language-',
    // CSS language prefix for fenced blocks
    linkify: false,
    // autoconvert URL-like texts to links
    // Enable some language-neutral replacements + quotes beautification
    typographer: false,
    // Double + single quotes replacement pairs, when typographer enabled,
    // and smartquotes on. Could be either a String or an Array.
    //
    // For example, you can use '«»„“' for Russian, '„“‚‘' for German,
    // and ['«\xA0', '\xA0»', '‹\xA0', '\xA0›'] for French (including nbsp).
    quotes: "\u201C\u201D\u2018\u2019",

    /* “”‘’ */
    // Highlighter function. Should return escaped HTML,
    // or '' if the source string is not changed and should be escaped externaly.
    // If result starts with <pre... internal wrapper is skipped.
    //
    // function (/*str, lang*/) { return ''; }
    //
    highlight: null,
    maxNesting: 20 // Internal protection, recursion limit

  },
  components: {
    core: {
      rules: ['normalize', 'block', 'inline']
    },
    block: {
      rules: ['blockquote', 'code', 'fence', 'heading', 'hr', 'html_block', 'lheading', 'list', 'reference', 'paragraph']
    },
    inline: {
      rules: ['autolink', 'backticks', 'emphasis', 'entity', 'escape', 'html_inline', 'image', 'link', 'newline', 'text'],
      rules2: ['balance_pairs', 'emphasis', 'text_collapse']
    }
  }
};

/***/ }),

/***/ "../../node_modules/markdown-it/lib/presets/default.js":
/*!***************************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/markdown-it/lib/presets/default.js ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
// markdown-it default options


module.exports = {
  options: {
    html: false,
    // Enable HTML tags in source
    xhtmlOut: false,
    // Use '/' to close single tags (<br />)
    breaks: false,
    // Convert '\n' in paragraphs into <br>
    langPrefix: 'language-',
    // CSS language prefix for fenced blocks
    linkify: false,
    // autoconvert URL-like texts to links
    // Enable some language-neutral replacements + quotes beautification
    typographer: false,
    // Double + single quotes replacement pairs, when typographer enabled,
    // and smartquotes on. Could be either a String or an Array.
    //
    // For example, you can use '«»„“' for Russian, '„“‚‘' for German,
    // and ['«\xA0', '\xA0»', '‹\xA0', '\xA0›'] for French (including nbsp).
    quotes: "\u201C\u201D\u2018\u2019",

    /* “”‘’ */
    // Highlighter function. Should return escaped HTML,
    // or '' if the source string is not changed and should be escaped externaly.
    // If result starts with <pre... internal wrapper is skipped.
    //
    // function (/*str, lang*/) { return ''; }
    //
    highlight: null,
    maxNesting: 100 // Internal protection, recursion limit

  },
  components: {
    core: {},
    block: {},
    inline: {}
  }
};

/***/ }),

/***/ "../../node_modules/markdown-it/lib/presets/zero.js":
/*!************************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/markdown-it/lib/presets/zero.js ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
// "Zero" preset, with nothing enabled. Useful for manual configuring of simple
// modes. For example, to parse bold/italic only.


module.exports = {
  options: {
    html: false,
    // Enable HTML tags in source
    xhtmlOut: false,
    // Use '/' to close single tags (<br />)
    breaks: false,
    // Convert '\n' in paragraphs into <br>
    langPrefix: 'language-',
    // CSS language prefix for fenced blocks
    linkify: false,
    // autoconvert URL-like texts to links
    // Enable some language-neutral replacements + quotes beautification
    typographer: false,
    // Double + single quotes replacement pairs, when typographer enabled,
    // and smartquotes on. Could be either a String or an Array.
    //
    // For example, you can use '«»„“' for Russian, '„“‚‘' for German,
    // and ['«\xA0', '\xA0»', '‹\xA0', '\xA0›'] for French (including nbsp).
    quotes: "\u201C\u201D\u2018\u2019",

    /* “”‘’ */
    // Highlighter function. Should return escaped HTML,
    // or '' if the source string is not changed and should be escaped externaly.
    // If result starts with <pre... internal wrapper is skipped.
    //
    // function (/*str, lang*/) { return ''; }
    //
    highlight: null,
    maxNesting: 20 // Internal protection, recursion limit

  },
  components: {
    core: {
      rules: ['normalize', 'block', 'inline']
    },
    block: {
      rules: ['paragraph']
    },
    inline: {
      rules: ['text'],
      rules2: ['balance_pairs', 'text_collapse']
    }
  }
};

/***/ }),

/***/ "../../node_modules/markdown-it/lib/renderer.js":
/*!********************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/markdown-it/lib/renderer.js ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/**
 * class Renderer
 *
 * Generates HTML from parsed token stream. Each instance has independent
 * copy of rules. Those can be rewritten with ease. Also, you can add new
 * rules if you create plugin and adds new token types.
 **/


var assign = __webpack_require__(/*! ./common/utils */ "../../node_modules/markdown-it/lib/common/utils.js").assign;

var unescapeAll = __webpack_require__(/*! ./common/utils */ "../../node_modules/markdown-it/lib/common/utils.js").unescapeAll;

var escapeHtml = __webpack_require__(/*! ./common/utils */ "../../node_modules/markdown-it/lib/common/utils.js").escapeHtml; ////////////////////////////////////////////////////////////////////////////////


var default_rules = {};

default_rules.code_inline = function (tokens, idx, options, env, slf) {
  var token = tokens[idx];
  return '<code' + slf.renderAttrs(token) + '>' + escapeHtml(tokens[idx].content) + '</code>';
};

default_rules.code_block = function (tokens, idx, options, env, slf) {
  var token = tokens[idx];
  return '<pre' + slf.renderAttrs(token) + '><code>' + escapeHtml(tokens[idx].content) + '</code></pre>\n';
};

default_rules.fence = function (tokens, idx, options, env, slf) {
  var token = tokens[idx],
      info = token.info ? unescapeAll(token.info).trim() : '',
      langName = '',
      highlighted,
      i,
      tmpAttrs,
      tmpToken;

  if (info) {
    langName = info.split(/\s+/g)[0];
  }

  if (options.highlight) {
    highlighted = options.highlight(token.content, langName) || escapeHtml(token.content);
  } else {
    highlighted = escapeHtml(token.content);
  }

  if (highlighted.indexOf('<pre') === 0) {
    return highlighted + '\n';
  } // If language exists, inject class gently, without modifying original token.
  // May be, one day we will add .clone() for token and simplify this part, but
  // now we prefer to keep things local.


  if (info) {
    i = token.attrIndex('class');
    tmpAttrs = token.attrs ? token.attrs.slice() : [];

    if (i < 0) {
      tmpAttrs.push(['class', options.langPrefix + langName]);
    } else {
      tmpAttrs[i][1] += ' ' + options.langPrefix + langName;
    } // Fake token just to render attributes


    tmpToken = {
      attrs: tmpAttrs
    };
    return '<pre><code' + slf.renderAttrs(tmpToken) + '>' + highlighted + '</code></pre>\n';
  }

  return '<pre><code' + slf.renderAttrs(token) + '>' + highlighted + '</code></pre>\n';
};

default_rules.image = function (tokens, idx, options, env, slf) {
  var token = tokens[idx]; // "alt" attr MUST be set, even if empty. Because it's mandatory and
  // should be placed on proper position for tests.
  //
  // Replace content with actual value

  token.attrs[token.attrIndex('alt')][1] = slf.renderInlineAsText(token.children, options, env);
  return slf.renderToken(tokens, idx, options);
};

default_rules.hardbreak = function (tokens, idx, options
/*, env */
) {
  return options.xhtmlOut ? '<br />\n' : '<br>\n';
};

default_rules.softbreak = function (tokens, idx, options
/*, env */
) {
  return options.breaks ? options.xhtmlOut ? '<br />\n' : '<br>\n' : '\n';
};

default_rules.text = function (tokens, idx
/*, options, env */
) {
  return escapeHtml(tokens[idx].content);
};

default_rules.html_block = function (tokens, idx
/*, options, env */
) {
  return tokens[idx].content;
};

default_rules.html_inline = function (tokens, idx
/*, options, env */
) {
  return tokens[idx].content;
};
/**
 * new Renderer()
 *
 * Creates new [[Renderer]] instance and fill [[Renderer#rules]] with defaults.
 **/


function Renderer() {
  /**
   * Renderer#rules -> Object
   *
   * Contains render rules for tokens. Can be updated and extended.
   *
   * ##### Example
   *
   * ```javascript
   * var md = require('markdown-it')();
   *
   * md.renderer.rules.strong_open  = function () { return '<b>'; };
   * md.renderer.rules.strong_close = function () { return '</b>'; };
   *
   * var result = md.renderInline(...);
   * ```
   *
   * Each rule is called as independent static function with fixed signature:
   *
   * ```javascript
   * function my_token_render(tokens, idx, options, env, renderer) {
   *   // ...
   *   return renderedHTML;
   * }
   * ```
   *
   * See [source code](https://github.com/markdown-it/markdown-it/blob/master/lib/renderer.js)
   * for more details and examples.
   **/
  this.rules = assign({}, default_rules);
}
/**
 * Renderer.renderAttrs(token) -> String
 *
 * Render token attributes to string.
 **/


Renderer.prototype.renderAttrs = function renderAttrs(token) {
  var i, l, result;

  if (!token.attrs) {
    return '';
  }

  result = '';

  for (i = 0, l = token.attrs.length; i < l; i++) {
    result += ' ' + escapeHtml(token.attrs[i][0]) + '="' + escapeHtml(token.attrs[i][1]) + '"';
  }

  return result;
};
/**
 * Renderer.renderToken(tokens, idx, options) -> String
 * - tokens (Array): list of tokens
 * - idx (Numbed): token index to render
 * - options (Object): params of parser instance
 *
 * Default token renderer. Can be overriden by custom function
 * in [[Renderer#rules]].
 **/


Renderer.prototype.renderToken = function renderToken(tokens, idx, options) {
  var nextToken,
      result = '',
      needLf = false,
      token = tokens[idx]; // Tight list paragraphs

  if (token.hidden) {
    return '';
  } // Insert a newline between hidden paragraph and subsequent opening
  // block-level tag.
  //
  // For example, here we should insert a newline before blockquote:
  //  - a
  //    >
  //


  if (token.block && token.nesting !== -1 && idx && tokens[idx - 1].hidden) {
    result += '\n';
  } // Add token name, e.g. `<img`


  result += (token.nesting === -1 ? '</' : '<') + token.tag; // Encode attributes, e.g. `<img src="foo"`

  result += this.renderAttrs(token); // Add a slash for self-closing tags, e.g. `<img src="foo" /`

  if (token.nesting === 0 && options.xhtmlOut) {
    result += ' /';
  } // Check if we need to add a newline after this tag


  if (token.block) {
    needLf = true;

    if (token.nesting === 1) {
      if (idx + 1 < tokens.length) {
        nextToken = tokens[idx + 1];

        if (nextToken.type === 'inline' || nextToken.hidden) {
          // Block-level tag containing an inline tag.
          //
          needLf = false;
        } else if (nextToken.nesting === -1 && nextToken.tag === token.tag) {
          // Opening tag + closing tag of the same type. E.g. `<li></li>`.
          //
          needLf = false;
        }
      }
    }
  }

  result += needLf ? '>\n' : '>';
  return result;
};
/**
 * Renderer.renderInline(tokens, options, env) -> String
 * - tokens (Array): list on block tokens to renter
 * - options (Object): params of parser instance
 * - env (Object): additional data from parsed input (references, for example)
 *
 * The same as [[Renderer.render]], but for single token of `inline` type.
 **/


Renderer.prototype.renderInline = function (tokens, options, env) {
  var type,
      result = '',
      rules = this.rules;

  for (var i = 0, len = tokens.length; i < len; i++) {
    type = tokens[i].type;

    if (typeof rules[type] !== 'undefined') {
      result += rules[type](tokens, i, options, env, this);
    } else {
      result += this.renderToken(tokens, i, options);
    }
  }

  return result;
};
/** internal
 * Renderer.renderInlineAsText(tokens, options, env) -> String
 * - tokens (Array): list on block tokens to renter
 * - options (Object): params of parser instance
 * - env (Object): additional data from parsed input (references, for example)
 *
 * Special kludge for image `alt` attributes to conform CommonMark spec.
 * Don't try to use it! Spec requires to show `alt` content with stripped markup,
 * instead of simple escaping.
 **/


Renderer.prototype.renderInlineAsText = function (tokens, options, env) {
  var result = '';

  for (var i = 0, len = tokens.length; i < len; i++) {
    if (tokens[i].type === 'text') {
      result += tokens[i].content;
    } else if (tokens[i].type === 'image') {
      result += this.renderInlineAsText(tokens[i].children, options, env);
    }
  }

  return result;
};
/**
 * Renderer.render(tokens, options, env) -> String
 * - tokens (Array): list on block tokens to renter
 * - options (Object): params of parser instance
 * - env (Object): additional data from parsed input (references, for example)
 *
 * Takes token stream and generates HTML. Probably, you will never need to call
 * this method directly.
 **/


Renderer.prototype.render = function (tokens, options, env) {
  var i,
      len,
      type,
      result = '',
      rules = this.rules;

  for (i = 0, len = tokens.length; i < len; i++) {
    type = tokens[i].type;

    if (type === 'inline') {
      result += this.renderInline(tokens[i].children, options, env);
    } else if (typeof rules[type] !== 'undefined') {
      result += rules[tokens[i].type](tokens, i, options, env, this);
    } else {
      result += this.renderToken(tokens, i, options, env);
    }
  }

  return result;
};

module.exports = Renderer;

/***/ }),

/***/ "../../node_modules/markdown-it/lib/ruler.js":
/*!*****************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/markdown-it/lib/ruler.js ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/**
 * class Ruler
 *
 * Helper class, used by [[MarkdownIt#core]], [[MarkdownIt#block]] and
 * [[MarkdownIt#inline]] to manage sequences of functions (rules):
 *
 * - keep rules in defined order
 * - assign the name to each rule
 * - enable/disable rules
 * - add/replace rules
 * - allow assign rules to additional named chains (in the same)
 * - cacheing lists of active rules
 *
 * You will not need use this class directly until write plugins. For simple
 * rules control use [[MarkdownIt.disable]], [[MarkdownIt.enable]] and
 * [[MarkdownIt.use]].
 **/

/**
 * new Ruler()
 **/

function Ruler() {
  // List of added rules. Each element is:
  //
  // {
  //   name: XXX,
  //   enabled: Boolean,
  //   fn: Function(),
  //   alt: [ name2, name3 ]
  // }
  //
  this.__rules__ = []; // Cached rule chains.
  //
  // First level - chain name, '' for default.
  // Second level - diginal anchor for fast filtering by charcodes.
  //

  this.__cache__ = null;
} ////////////////////////////////////////////////////////////////////////////////
// Helper methods, should not be used directly
// Find rule index by name
//


Ruler.prototype.__find__ = function (name) {
  for (var i = 0; i < this.__rules__.length; i++) {
    if (this.__rules__[i].name === name) {
      return i;
    }
  }

  return -1;
}; // Build rules lookup cache
//


Ruler.prototype.__compile__ = function () {
  var self = this;
  var chains = ['']; // collect unique names

  self.__rules__.forEach(function (rule) {
    if (!rule.enabled) {
      return;
    }

    rule.alt.forEach(function (altName) {
      if (chains.indexOf(altName) < 0) {
        chains.push(altName);
      }
    });
  });

  self.__cache__ = {};
  chains.forEach(function (chain) {
    self.__cache__[chain] = [];

    self.__rules__.forEach(function (rule) {
      if (!rule.enabled) {
        return;
      }

      if (chain && rule.alt.indexOf(chain) < 0) {
        return;
      }

      self.__cache__[chain].push(rule.fn);
    });
  });
};
/**
 * Ruler.at(name, fn [, options])
 * - name (String): rule name to replace.
 * - fn (Function): new rule function.
 * - options (Object): new rule options (not mandatory).
 *
 * Replace rule by name with new function & options. Throws error if name not
 * found.
 *
 * ##### Options:
 *
 * - __alt__ - array with names of "alternate" chains.
 *
 * ##### Example
 *
 * Replace existing typographer replacement rule with new one:
 *
 * ```javascript
 * var md = require('markdown-it')();
 *
 * md.core.ruler.at('replacements', function replace(state) {
 *   //...
 * });
 * ```
 **/


Ruler.prototype.at = function (name, fn, options) {
  var index = this.__find__(name);

  var opt = options || {};

  if (index === -1) {
    throw new Error('Parser rule not found: ' + name);
  }

  this.__rules__[index].fn = fn;
  this.__rules__[index].alt = opt.alt || [];
  this.__cache__ = null;
};
/**
 * Ruler.before(beforeName, ruleName, fn [, options])
 * - beforeName (String): new rule will be added before this one.
 * - ruleName (String): name of added rule.
 * - fn (Function): rule function.
 * - options (Object): rule options (not mandatory).
 *
 * Add new rule to chain before one with given name. See also
 * [[Ruler.after]], [[Ruler.push]].
 *
 * ##### Options:
 *
 * - __alt__ - array with names of "alternate" chains.
 *
 * ##### Example
 *
 * ```javascript
 * var md = require('markdown-it')();
 *
 * md.block.ruler.before('paragraph', 'my_rule', function replace(state) {
 *   //...
 * });
 * ```
 **/


Ruler.prototype.before = function (beforeName, ruleName, fn, options) {
  var index = this.__find__(beforeName);

  var opt = options || {};

  if (index === -1) {
    throw new Error('Parser rule not found: ' + beforeName);
  }

  this.__rules__.splice(index, 0, {
    name: ruleName,
    enabled: true,
    fn: fn,
    alt: opt.alt || []
  });

  this.__cache__ = null;
};
/**
 * Ruler.after(afterName, ruleName, fn [, options])
 * - afterName (String): new rule will be added after this one.
 * - ruleName (String): name of added rule.
 * - fn (Function): rule function.
 * - options (Object): rule options (not mandatory).
 *
 * Add new rule to chain after one with given name. See also
 * [[Ruler.before]], [[Ruler.push]].
 *
 * ##### Options:
 *
 * - __alt__ - array with names of "alternate" chains.
 *
 * ##### Example
 *
 * ```javascript
 * var md = require('markdown-it')();
 *
 * md.inline.ruler.after('text', 'my_rule', function replace(state) {
 *   //...
 * });
 * ```
 **/


Ruler.prototype.after = function (afterName, ruleName, fn, options) {
  var index = this.__find__(afterName);

  var opt = options || {};

  if (index === -1) {
    throw new Error('Parser rule not found: ' + afterName);
  }

  this.__rules__.splice(index + 1, 0, {
    name: ruleName,
    enabled: true,
    fn: fn,
    alt: opt.alt || []
  });

  this.__cache__ = null;
};
/**
 * Ruler.push(ruleName, fn [, options])
 * - ruleName (String): name of added rule.
 * - fn (Function): rule function.
 * - options (Object): rule options (not mandatory).
 *
 * Push new rule to the end of chain. See also
 * [[Ruler.before]], [[Ruler.after]].
 *
 * ##### Options:
 *
 * - __alt__ - array with names of "alternate" chains.
 *
 * ##### Example
 *
 * ```javascript
 * var md = require('markdown-it')();
 *
 * md.core.ruler.push('my_rule', function replace(state) {
 *   //...
 * });
 * ```
 **/


Ruler.prototype.push = function (ruleName, fn, options) {
  var opt = options || {};

  this.__rules__.push({
    name: ruleName,
    enabled: true,
    fn: fn,
    alt: opt.alt || []
  });

  this.__cache__ = null;
};
/**
 * Ruler.enable(list [, ignoreInvalid]) -> Array
 * - list (String|Array): list of rule names to enable.
 * - ignoreInvalid (Boolean): set `true` to ignore errors when rule not found.
 *
 * Enable rules with given names. If any rule name not found - throw Error.
 * Errors can be disabled by second param.
 *
 * Returns list of found rule names (if no exception happened).
 *
 * See also [[Ruler.disable]], [[Ruler.enableOnly]].
 **/


Ruler.prototype.enable = function (list, ignoreInvalid) {
  if (!Array.isArray(list)) {
    list = [list];
  }

  var result = []; // Search by name and enable

  list.forEach(function (name) {
    var idx = this.__find__(name);

    if (idx < 0) {
      if (ignoreInvalid) {
        return;
      }

      throw new Error('Rules manager: invalid rule name ' + name);
    }

    this.__rules__[idx].enabled = true;
    result.push(name);
  }, this);
  this.__cache__ = null;
  return result;
};
/**
 * Ruler.enableOnly(list [, ignoreInvalid])
 * - list (String|Array): list of rule names to enable (whitelist).
 * - ignoreInvalid (Boolean): set `true` to ignore errors when rule not found.
 *
 * Enable rules with given names, and disable everything else. If any rule name
 * not found - throw Error. Errors can be disabled by second param.
 *
 * See also [[Ruler.disable]], [[Ruler.enable]].
 **/


Ruler.prototype.enableOnly = function (list, ignoreInvalid) {
  if (!Array.isArray(list)) {
    list = [list];
  }

  this.__rules__.forEach(function (rule) {
    rule.enabled = false;
  });

  this.enable(list, ignoreInvalid);
};
/**
 * Ruler.disable(list [, ignoreInvalid]) -> Array
 * - list (String|Array): list of rule names to disable.
 * - ignoreInvalid (Boolean): set `true` to ignore errors when rule not found.
 *
 * Disable rules with given names. If any rule name not found - throw Error.
 * Errors can be disabled by second param.
 *
 * Returns list of found rule names (if no exception happened).
 *
 * See also [[Ruler.enable]], [[Ruler.enableOnly]].
 **/


Ruler.prototype.disable = function (list, ignoreInvalid) {
  if (!Array.isArray(list)) {
    list = [list];
  }

  var result = []; // Search by name and disable

  list.forEach(function (name) {
    var idx = this.__find__(name);

    if (idx < 0) {
      if (ignoreInvalid) {
        return;
      }

      throw new Error('Rules manager: invalid rule name ' + name);
    }

    this.__rules__[idx].enabled = false;
    result.push(name);
  }, this);
  this.__cache__ = null;
  return result;
};
/**
 * Ruler.getRules(chainName) -> Array
 *
 * Return array of active functions (rules) for given chain name. It analyzes
 * rules configuration, compiles caches if not exists and returns result.
 *
 * Default chain name is `''` (empty string). It can't be skipped. That's
 * done intentionally, to keep signature monomorphic for high speed.
 **/


Ruler.prototype.getRules = function (chainName) {
  if (this.__cache__ === null) {
    this.__compile__();
  } // Chain can be empty, if rules disabled. But we still have to return Array.


  return this.__cache__[chainName] || [];
};

module.exports = Ruler;

/***/ }),

/***/ "../../node_modules/markdown-it/lib/rules_block/blockquote.js":
/*!**********************************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/markdown-it/lib/rules_block/blockquote.js ***!
  \**********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
// Block quotes


var isSpace = __webpack_require__(/*! ../common/utils */ "../../node_modules/markdown-it/lib/common/utils.js").isSpace;

module.exports = function blockquote(state, startLine, endLine, silent) {
  var adjustTab,
      ch,
      i,
      initial,
      l,
      lastLineEmpty,
      lines,
      nextLine,
      offset,
      oldBMarks,
      oldBSCount,
      oldIndent,
      oldParentType,
      oldSCount,
      oldTShift,
      spaceAfterMarker,
      terminate,
      terminatorRules,
      token,
      wasOutdented,
      oldLineMax = state.lineMax,
      pos = state.bMarks[startLine] + state.tShift[startLine],
      max = state.eMarks[startLine]; // if it's indented more than 3 spaces, it should be a code block

  if (state.sCount[startLine] - state.blkIndent >= 4) {
    return false;
  } // check the block quote marker


  if (state.src.charCodeAt(pos++) !== 0x3E
  /* > */
  ) {
      return false;
    } // we know that it's going to be a valid blockquote,
  // so no point trying to find the end of it in silent mode


  if (silent) {
    return true;
  } // skip spaces after ">" and re-calculate offset


  initial = offset = state.sCount[startLine] + pos - (state.bMarks[startLine] + state.tShift[startLine]); // skip one optional space after '>'

  if (state.src.charCodeAt(pos) === 0x20
  /* space */
  ) {
      // ' >   test '
      //     ^ -- position start of line here:
      pos++;
      initial++;
      offset++;
      adjustTab = false;
      spaceAfterMarker = true;
    } else if (state.src.charCodeAt(pos) === 0x09
  /* tab */
  ) {
      spaceAfterMarker = true;

      if ((state.bsCount[startLine] + offset) % 4 === 3) {
        // '  >\t  test '
        //       ^ -- position start of line here (tab has width===1)
        pos++;
        initial++;
        offset++;
        adjustTab = false;
      } else {
        // ' >\t  test '
        //    ^ -- position start of line here + shift bsCount slightly
        //         to make extra space appear
        adjustTab = true;
      }
    } else {
    spaceAfterMarker = false;
  }

  oldBMarks = [state.bMarks[startLine]];
  state.bMarks[startLine] = pos;

  while (pos < max) {
    ch = state.src.charCodeAt(pos);

    if (isSpace(ch)) {
      if (ch === 0x09) {
        offset += 4 - (offset + state.bsCount[startLine] + (adjustTab ? 1 : 0)) % 4;
      } else {
        offset++;
      }
    } else {
      break;
    }

    pos++;
  }

  oldBSCount = [state.bsCount[startLine]];
  state.bsCount[startLine] = state.sCount[startLine] + 1 + (spaceAfterMarker ? 1 : 0);
  lastLineEmpty = pos >= max;
  oldSCount = [state.sCount[startLine]];
  state.sCount[startLine] = offset - initial;
  oldTShift = [state.tShift[startLine]];
  state.tShift[startLine] = pos - state.bMarks[startLine];
  terminatorRules = state.md.block.ruler.getRules('blockquote');
  oldParentType = state.parentType;
  state.parentType = 'blockquote';
  wasOutdented = false; // Search the end of the block
  //
  // Block ends with either:
  //  1. an empty line outside:
  //     ```
  //     > test
  //
  //     ```
  //  2. an empty line inside:
  //     ```
  //     >
  //     test
  //     ```
  //  3. another tag:
  //     ```
  //     > test
  //      - - -
  //     ```

  for (nextLine = startLine + 1; nextLine < endLine; nextLine++) {
    // check if it's outdented, i.e. it's inside list item and indented
    // less than said list item:
    //
    // ```
    // 1. anything
    //    > current blockquote
    // 2. checking this line
    // ```
    if (state.sCount[nextLine] < state.blkIndent) wasOutdented = true;
    pos = state.bMarks[nextLine] + state.tShift[nextLine];
    max = state.eMarks[nextLine];

    if (pos >= max) {
      // Case 1: line is not inside the blockquote, and this line is empty.
      break;
    }

    if (state.src.charCodeAt(pos++) === 0x3E
    /* > */
    && !wasOutdented) {
      // This line is inside the blockquote.
      // skip spaces after ">" and re-calculate offset
      initial = offset = state.sCount[nextLine] + pos - (state.bMarks[nextLine] + state.tShift[nextLine]); // skip one optional space after '>'

      if (state.src.charCodeAt(pos) === 0x20
      /* space */
      ) {
          // ' >   test '
          //     ^ -- position start of line here:
          pos++;
          initial++;
          offset++;
          adjustTab = false;
          spaceAfterMarker = true;
        } else if (state.src.charCodeAt(pos) === 0x09
      /* tab */
      ) {
          spaceAfterMarker = true;

          if ((state.bsCount[nextLine] + offset) % 4 === 3) {
            // '  >\t  test '
            //       ^ -- position start of line here (tab has width===1)
            pos++;
            initial++;
            offset++;
            adjustTab = false;
          } else {
            // ' >\t  test '
            //    ^ -- position start of line here + shift bsCount slightly
            //         to make extra space appear
            adjustTab = true;
          }
        } else {
        spaceAfterMarker = false;
      }

      oldBMarks.push(state.bMarks[nextLine]);
      state.bMarks[nextLine] = pos;

      while (pos < max) {
        ch = state.src.charCodeAt(pos);

        if (isSpace(ch)) {
          if (ch === 0x09) {
            offset += 4 - (offset + state.bsCount[nextLine] + (adjustTab ? 1 : 0)) % 4;
          } else {
            offset++;
          }
        } else {
          break;
        }

        pos++;
      }

      lastLineEmpty = pos >= max;
      oldBSCount.push(state.bsCount[nextLine]);
      state.bsCount[nextLine] = state.sCount[nextLine] + 1 + (spaceAfterMarker ? 1 : 0);
      oldSCount.push(state.sCount[nextLine]);
      state.sCount[nextLine] = offset - initial;
      oldTShift.push(state.tShift[nextLine]);
      state.tShift[nextLine] = pos - state.bMarks[nextLine];
      continue;
    } // Case 2: line is not inside the blockquote, and the last line was empty.


    if (lastLineEmpty) {
      break;
    } // Case 3: another tag found.


    terminate = false;

    for (i = 0, l = terminatorRules.length; i < l; i++) {
      if (terminatorRules[i](state, nextLine, endLine, true)) {
        terminate = true;
        break;
      }
    }

    if (terminate) {
      // Quirk to enforce "hard termination mode" for paragraphs;
      // normally if you call `tokenize(state, startLine, nextLine)`,
      // paragraphs will look below nextLine for paragraph continuation,
      // but if blockquote is terminated by another tag, they shouldn't
      state.lineMax = nextLine;

      if (state.blkIndent !== 0) {
        // state.blkIndent was non-zero, we now set it to zero,
        // so we need to re-calculate all offsets to appear as
        // if indent wasn't changed
        oldBMarks.push(state.bMarks[nextLine]);
        oldBSCount.push(state.bsCount[nextLine]);
        oldTShift.push(state.tShift[nextLine]);
        oldSCount.push(state.sCount[nextLine]);
        state.sCount[nextLine] -= state.blkIndent;
      }

      break;
    }

    oldBMarks.push(state.bMarks[nextLine]);
    oldBSCount.push(state.bsCount[nextLine]);
    oldTShift.push(state.tShift[nextLine]);
    oldSCount.push(state.sCount[nextLine]); // A negative indentation means that this is a paragraph continuation
    //

    state.sCount[nextLine] = -1;
  }

  oldIndent = state.blkIndent;
  state.blkIndent = 0;
  token = state.push('blockquote_open', 'blockquote', 1);
  token.markup = '>';
  token.map = lines = [startLine, 0];
  state.md.block.tokenize(state, startLine, nextLine);
  token = state.push('blockquote_close', 'blockquote', -1);
  token.markup = '>';
  state.lineMax = oldLineMax;
  state.parentType = oldParentType;
  lines[1] = state.line; // Restore original tShift; this might not be necessary since the parser
  // has already been here, but just to make sure we can do that.

  for (i = 0; i < oldTShift.length; i++) {
    state.bMarks[i + startLine] = oldBMarks[i];
    state.tShift[i + startLine] = oldTShift[i];
    state.sCount[i + startLine] = oldSCount[i];
    state.bsCount[i + startLine] = oldBSCount[i];
  }

  state.blkIndent = oldIndent;
  return true;
};

/***/ }),

/***/ "../../node_modules/markdown-it/lib/rules_block/code.js":
/*!****************************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/markdown-it/lib/rules_block/code.js ***!
  \****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
// Code block (4 spaces padded)


module.exports = function code(state, startLine, endLine
/*, silent*/
) {
  var nextLine, last, token;

  if (state.sCount[startLine] - state.blkIndent < 4) {
    return false;
  }

  last = nextLine = startLine + 1;

  while (nextLine < endLine) {
    if (state.isEmpty(nextLine)) {
      nextLine++;
      continue;
    }

    if (state.sCount[nextLine] - state.blkIndent >= 4) {
      nextLine++;
      last = nextLine;
      continue;
    }

    break;
  }

  state.line = last;
  token = state.push('code_block', 'code', 0);
  token.content = state.getLines(startLine, last, 4 + state.blkIndent, true);
  token.map = [startLine, state.line];
  return true;
};

/***/ }),

/***/ "../../node_modules/markdown-it/lib/rules_block/fence.js":
/*!*****************************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/markdown-it/lib/rules_block/fence.js ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
// fences (``` lang, ~~~ lang)


module.exports = function fence(state, startLine, endLine, silent) {
  var marker,
      len,
      params,
      nextLine,
      mem,
      token,
      markup,
      haveEndMarker = false,
      pos = state.bMarks[startLine] + state.tShift[startLine],
      max = state.eMarks[startLine]; // if it's indented more than 3 spaces, it should be a code block

  if (state.sCount[startLine] - state.blkIndent >= 4) {
    return false;
  }

  if (pos + 3 > max) {
    return false;
  }

  marker = state.src.charCodeAt(pos);

  if (marker !== 0x7E
  /* ~ */
  && marker !== 0x60
  /* ` */
  ) {
      return false;
    } // scan marker length


  mem = pos;
  pos = state.skipChars(pos, marker);
  len = pos - mem;

  if (len < 3) {
    return false;
  }

  markup = state.src.slice(mem, pos);
  params = state.src.slice(pos, max);

  if (marker === 0x60
  /* ` */
  ) {
      if (params.indexOf(String.fromCharCode(marker)) >= 0) {
        return false;
      }
    } // Since start is found, we can report success here in validation mode


  if (silent) {
    return true;
  } // search end of block


  nextLine = startLine;

  for (;;) {
    nextLine++;

    if (nextLine >= endLine) {
      // unclosed block should be autoclosed by end of document.
      // also block seems to be autoclosed by end of parent
      break;
    }

    pos = mem = state.bMarks[nextLine] + state.tShift[nextLine];
    max = state.eMarks[nextLine];

    if (pos < max && state.sCount[nextLine] < state.blkIndent) {
      // non-empty line with negative indent should stop the list:
      // - ```
      //  test
      break;
    }

    if (state.src.charCodeAt(pos) !== marker) {
      continue;
    }

    if (state.sCount[nextLine] - state.blkIndent >= 4) {
      // closing fence should be indented less than 4 spaces
      continue;
    }

    pos = state.skipChars(pos, marker); // closing code fence must be at least as long as the opening one

    if (pos - mem < len) {
      continue;
    } // make sure tail has spaces only


    pos = state.skipSpaces(pos);

    if (pos < max) {
      continue;
    }

    haveEndMarker = true; // found!

    break;
  } // If a fence has heading spaces, they should be removed from its inner block


  len = state.sCount[startLine];
  state.line = nextLine + (haveEndMarker ? 1 : 0);
  token = state.push('fence', 'code', 0);
  token.info = params;
  token.content = state.getLines(startLine + 1, nextLine, len, true);
  token.markup = markup;
  token.map = [startLine, state.line];
  return true;
};

/***/ }),

/***/ "../../node_modules/markdown-it/lib/rules_block/heading.js":
/*!*******************************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/markdown-it/lib/rules_block/heading.js ***!
  \*******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
// heading (#, ##, ...)


var isSpace = __webpack_require__(/*! ../common/utils */ "../../node_modules/markdown-it/lib/common/utils.js").isSpace;

module.exports = function heading(state, startLine, endLine, silent) {
  var ch,
      level,
      tmp,
      token,
      pos = state.bMarks[startLine] + state.tShift[startLine],
      max = state.eMarks[startLine]; // if it's indented more than 3 spaces, it should be a code block

  if (state.sCount[startLine] - state.blkIndent >= 4) {
    return false;
  }

  ch = state.src.charCodeAt(pos);

  if (ch !== 0x23
  /* # */
  || pos >= max) {
    return false;
  } // count heading level


  level = 1;
  ch = state.src.charCodeAt(++pos);

  while (ch === 0x23
  /* # */
  && pos < max && level <= 6) {
    level++;
    ch = state.src.charCodeAt(++pos);
  }

  if (level > 6 || pos < max && !isSpace(ch)) {
    return false;
  }

  if (silent) {
    return true;
  } // Let's cut tails like '    ###  ' from the end of string


  max = state.skipSpacesBack(max, pos);
  tmp = state.skipCharsBack(max, 0x23, pos); // #

  if (tmp > pos && isSpace(state.src.charCodeAt(tmp - 1))) {
    max = tmp;
  }

  state.line = startLine + 1;
  token = state.push('heading_open', 'h' + String(level), 1);
  token.markup = '########'.slice(0, level);
  token.map = [startLine, state.line];
  token = state.push('inline', '', 0);
  token.content = state.src.slice(pos, max).trim();
  token.map = [startLine, state.line];
  token.children = [];
  token = state.push('heading_close', 'h' + String(level), -1);
  token.markup = '########'.slice(0, level);
  return true;
};

/***/ }),

/***/ "../../node_modules/markdown-it/lib/rules_block/hr.js":
/*!**************************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/markdown-it/lib/rules_block/hr.js ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
// Horizontal rule


var isSpace = __webpack_require__(/*! ../common/utils */ "../../node_modules/markdown-it/lib/common/utils.js").isSpace;

module.exports = function hr(state, startLine, endLine, silent) {
  var marker,
      cnt,
      ch,
      token,
      pos = state.bMarks[startLine] + state.tShift[startLine],
      max = state.eMarks[startLine]; // if it's indented more than 3 spaces, it should be a code block

  if (state.sCount[startLine] - state.blkIndent >= 4) {
    return false;
  }

  marker = state.src.charCodeAt(pos++); // Check hr marker

  if (marker !== 0x2A
  /* * */
  && marker !== 0x2D
  /* - */
  && marker !== 0x5F
  /* _ */
  ) {
      return false;
    } // markers can be mixed with spaces, but there should be at least 3 of them


  cnt = 1;

  while (pos < max) {
    ch = state.src.charCodeAt(pos++);

    if (ch !== marker && !isSpace(ch)) {
      return false;
    }

    if (ch === marker) {
      cnt++;
    }
  }

  if (cnt < 3) {
    return false;
  }

  if (silent) {
    return true;
  }

  state.line = startLine + 1;
  token = state.push('hr', 'hr', 0);
  token.map = [startLine, state.line];
  token.markup = Array(cnt + 1).join(String.fromCharCode(marker));
  return true;
};

/***/ }),

/***/ "../../node_modules/markdown-it/lib/rules_block/html_block.js":
/*!**********************************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/markdown-it/lib/rules_block/html_block.js ***!
  \**********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
// HTML block


var block_names = __webpack_require__(/*! ../common/html_blocks */ "../../node_modules/markdown-it/lib/common/html_blocks.js");

var HTML_OPEN_CLOSE_TAG_RE = __webpack_require__(/*! ../common/html_re */ "../../node_modules/markdown-it/lib/common/html_re.js").HTML_OPEN_CLOSE_TAG_RE; // An array of opening and corresponding closing sequences for html tags,
// last argument defines whether it can terminate a paragraph or not
//


var HTML_SEQUENCES = [[/^<(script|pre|style)(?=(\s|>|$))/i, /<\/(script|pre|style)>/i, true], [/^<!--/, /-->/, true], [/^<\?/, /\?>/, true], [/^<![A-Z]/, />/, true], [/^<!\[CDATA\[/, /\]\]>/, true], [new RegExp('^</?(' + block_names.join('|') + ')(?=(\\s|/?>|$))', 'i'), /^$/, true], [new RegExp(HTML_OPEN_CLOSE_TAG_RE.source + '\\s*$'), /^$/, false]];

module.exports = function html_block(state, startLine, endLine, silent) {
  var i,
      nextLine,
      token,
      lineText,
      pos = state.bMarks[startLine] + state.tShift[startLine],
      max = state.eMarks[startLine]; // if it's indented more than 3 spaces, it should be a code block

  if (state.sCount[startLine] - state.blkIndent >= 4) {
    return false;
  }

  if (!state.md.options.html) {
    return false;
  }

  if (state.src.charCodeAt(pos) !== 0x3C
  /* < */
  ) {
      return false;
    }

  lineText = state.src.slice(pos, max);

  for (i = 0; i < HTML_SEQUENCES.length; i++) {
    if (HTML_SEQUENCES[i][0].test(lineText)) {
      break;
    }
  }

  if (i === HTML_SEQUENCES.length) {
    return false;
  }

  if (silent) {
    // true if this sequence can be a terminator, false otherwise
    return HTML_SEQUENCES[i][2];
  }

  nextLine = startLine + 1; // If we are here - we detected HTML block.
  // Let's roll down till block end.

  if (!HTML_SEQUENCES[i][1].test(lineText)) {
    for (; nextLine < endLine; nextLine++) {
      if (state.sCount[nextLine] < state.blkIndent) {
        break;
      }

      pos = state.bMarks[nextLine] + state.tShift[nextLine];
      max = state.eMarks[nextLine];
      lineText = state.src.slice(pos, max);

      if (HTML_SEQUENCES[i][1].test(lineText)) {
        if (lineText.length !== 0) {
          nextLine++;
        }

        break;
      }
    }
  }

  state.line = nextLine;
  token = state.push('html_block', '', 0);
  token.map = [startLine, nextLine];
  token.content = state.getLines(startLine, nextLine, state.blkIndent, true);
  return true;
};

/***/ }),

/***/ "../../node_modules/markdown-it/lib/rules_block/lheading.js":
/*!********************************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/markdown-it/lib/rules_block/lheading.js ***!
  \********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
// lheading (---, ===)


module.exports = function lheading(state, startLine, endLine
/*, silent*/
) {
  var content,
      terminate,
      i,
      l,
      token,
      pos,
      max,
      level,
      marker,
      nextLine = startLine + 1,
      oldParentType,
      terminatorRules = state.md.block.ruler.getRules('paragraph'); // if it's indented more than 3 spaces, it should be a code block

  if (state.sCount[startLine] - state.blkIndent >= 4) {
    return false;
  }

  oldParentType = state.parentType;
  state.parentType = 'paragraph'; // use paragraph to match terminatorRules
  // jump line-by-line until empty one or EOF

  for (; nextLine < endLine && !state.isEmpty(nextLine); nextLine++) {
    // this would be a code block normally, but after paragraph
    // it's considered a lazy continuation regardless of what's there
    if (state.sCount[nextLine] - state.blkIndent > 3) {
      continue;
    } //
    // Check for underline in setext header
    //


    if (state.sCount[nextLine] >= state.blkIndent) {
      pos = state.bMarks[nextLine] + state.tShift[nextLine];
      max = state.eMarks[nextLine];

      if (pos < max) {
        marker = state.src.charCodeAt(pos);

        if (marker === 0x2D
        /* - */
        || marker === 0x3D
        /* = */
        ) {
            pos = state.skipChars(pos, marker);
            pos = state.skipSpaces(pos);

            if (pos >= max) {
              level = marker === 0x3D
              /* = */
              ? 1 : 2;
              break;
            }
          }
      }
    } // quirk for blockquotes, this line should already be checked by that rule


    if (state.sCount[nextLine] < 0) {
      continue;
    } // Some tags can terminate paragraph without empty line.


    terminate = false;

    for (i = 0, l = terminatorRules.length; i < l; i++) {
      if (terminatorRules[i](state, nextLine, endLine, true)) {
        terminate = true;
        break;
      }
    }

    if (terminate) {
      break;
    }
  }

  if (!level) {
    // Didn't find valid underline
    return false;
  }

  content = state.getLines(startLine, nextLine, state.blkIndent, false).trim();
  state.line = nextLine + 1;
  token = state.push('heading_open', 'h' + String(level), 1);
  token.markup = String.fromCharCode(marker);
  token.map = [startLine, state.line];
  token = state.push('inline', '', 0);
  token.content = content;
  token.map = [startLine, state.line - 1];
  token.children = [];
  token = state.push('heading_close', 'h' + String(level), -1);
  token.markup = String.fromCharCode(marker);
  state.parentType = oldParentType;
  return true;
};

/***/ }),

/***/ "../../node_modules/markdown-it/lib/rules_block/list.js":
/*!****************************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/markdown-it/lib/rules_block/list.js ***!
  \****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
// Lists


var isSpace = __webpack_require__(/*! ../common/utils */ "../../node_modules/markdown-it/lib/common/utils.js").isSpace; // Search `[-+*][\n ]`, returns next pos after marker on success
// or -1 on fail.


function skipBulletListMarker(state, startLine) {
  var marker, pos, max, ch;
  pos = state.bMarks[startLine] + state.tShift[startLine];
  max = state.eMarks[startLine];
  marker = state.src.charCodeAt(pos++); // Check bullet

  if (marker !== 0x2A
  /* * */
  && marker !== 0x2D
  /* - */
  && marker !== 0x2B
  /* + */
  ) {
      return -1;
    }

  if (pos < max) {
    ch = state.src.charCodeAt(pos);

    if (!isSpace(ch)) {
      // " -test " - is not a list item
      return -1;
    }
  }

  return pos;
} // Search `\d+[.)][\n ]`, returns next pos after marker on success
// or -1 on fail.


function skipOrderedListMarker(state, startLine) {
  var ch,
      start = state.bMarks[startLine] + state.tShift[startLine],
      pos = start,
      max = state.eMarks[startLine]; // List marker should have at least 2 chars (digit + dot)

  if (pos + 1 >= max) {
    return -1;
  }

  ch = state.src.charCodeAt(pos++);

  if (ch < 0x30
  /* 0 */
  || ch > 0x39
  /* 9 */
  ) {
      return -1;
    }

  for (;;) {
    // EOL -> fail
    if (pos >= max) {
      return -1;
    }

    ch = state.src.charCodeAt(pos++);

    if (ch >= 0x30
    /* 0 */
    && ch <= 0x39
    /* 9 */
    ) {
        // List marker should have no more than 9 digits
        // (prevents integer overflow in browsers)
        if (pos - start >= 10) {
          return -1;
        }

        continue;
      } // found valid marker


    if (ch === 0x29
    /* ) */
    || ch === 0x2e
    /* . */
    ) {
        break;
      }

    return -1;
  }

  if (pos < max) {
    ch = state.src.charCodeAt(pos);

    if (!isSpace(ch)) {
      // " 1.test " - is not a list item
      return -1;
    }
  }

  return pos;
}

function markTightParagraphs(state, idx) {
  var i,
      l,
      level = state.level + 2;

  for (i = idx + 2, l = state.tokens.length - 2; i < l; i++) {
    if (state.tokens[i].level === level && state.tokens[i].type === 'paragraph_open') {
      state.tokens[i + 2].hidden = true;
      state.tokens[i].hidden = true;
      i += 2;
    }
  }
}

module.exports = function list(state, startLine, endLine, silent) {
  var ch,
      contentStart,
      i,
      indent,
      indentAfterMarker,
      initial,
      isOrdered,
      itemLines,
      l,
      listLines,
      listTokIdx,
      markerCharCode,
      markerValue,
      max,
      nextLine,
      offset,
      oldListIndent,
      oldParentType,
      oldSCount,
      oldTShift,
      oldTight,
      pos,
      posAfterMarker,
      prevEmptyEnd,
      start,
      terminate,
      terminatorRules,
      token,
      isTerminatingParagraph = false,
      tight = true; // if it's indented more than 3 spaces, it should be a code block

  if (state.sCount[startLine] - state.blkIndent >= 4) {
    return false;
  } // Special case:
  //  - item 1
  //   - item 2
  //    - item 3
  //     - item 4
  //      - this one is a paragraph continuation


  if (state.listIndent >= 0 && state.sCount[startLine] - state.listIndent >= 4 && state.sCount[startLine] < state.blkIndent) {
    return false;
  } // limit conditions when list can interrupt
  // a paragraph (validation mode only)


  if (silent && state.parentType === 'paragraph') {
    // Next list item should still terminate previous list item;
    //
    // This code can fail if plugins use blkIndent as well as lists,
    // but I hope the spec gets fixed long before that happens.
    //
    if (state.tShift[startLine] >= state.blkIndent) {
      isTerminatingParagraph = true;
    }
  } // Detect list type and position after marker


  if ((posAfterMarker = skipOrderedListMarker(state, startLine)) >= 0) {
    isOrdered = true;
    start = state.bMarks[startLine] + state.tShift[startLine];
    markerValue = Number(state.src.substr(start, posAfterMarker - start - 1)); // If we're starting a new ordered list right after
    // a paragraph, it should start with 1.

    if (isTerminatingParagraph && markerValue !== 1) return false;
  } else if ((posAfterMarker = skipBulletListMarker(state, startLine)) >= 0) {
    isOrdered = false;
  } else {
    return false;
  } // If we're starting a new unordered list right after
  // a paragraph, first line should not be empty.


  if (isTerminatingParagraph) {
    if (state.skipSpaces(posAfterMarker) >= state.eMarks[startLine]) return false;
  } // We should terminate list on style change. Remember first one to compare.


  markerCharCode = state.src.charCodeAt(posAfterMarker - 1); // For validation mode we can terminate immediately

  if (silent) {
    return true;
  } // Start list


  listTokIdx = state.tokens.length;

  if (isOrdered) {
    token = state.push('ordered_list_open', 'ol', 1);

    if (markerValue !== 1) {
      token.attrs = [['start', markerValue]];
    }
  } else {
    token = state.push('bullet_list_open', 'ul', 1);
  }

  token.map = listLines = [startLine, 0];
  token.markup = String.fromCharCode(markerCharCode); //
  // Iterate list items
  //

  nextLine = startLine;
  prevEmptyEnd = false;
  terminatorRules = state.md.block.ruler.getRules('list');
  oldParentType = state.parentType;
  state.parentType = 'list';

  while (nextLine < endLine) {
    pos = posAfterMarker;
    max = state.eMarks[nextLine];
    initial = offset = state.sCount[nextLine] + posAfterMarker - (state.bMarks[startLine] + state.tShift[startLine]);

    while (pos < max) {
      ch = state.src.charCodeAt(pos);

      if (ch === 0x09) {
        offset += 4 - (offset + state.bsCount[nextLine]) % 4;
      } else if (ch === 0x20) {
        offset++;
      } else {
        break;
      }

      pos++;
    }

    contentStart = pos;

    if (contentStart >= max) {
      // trimming space in "-    \n  3" case, indent is 1 here
      indentAfterMarker = 1;
    } else {
      indentAfterMarker = offset - initial;
    } // If we have more than 4 spaces, the indent is 1
    // (the rest is just indented code block)


    if (indentAfterMarker > 4) {
      indentAfterMarker = 1;
    } // "  -  test"
    //  ^^^^^ - calculating total length of this thing


    indent = initial + indentAfterMarker; // Run subparser & write tokens

    token = state.push('list_item_open', 'li', 1);
    token.markup = String.fromCharCode(markerCharCode);
    token.map = itemLines = [startLine, 0]; // change current state, then restore it after parser subcall

    oldTight = state.tight;
    oldTShift = state.tShift[startLine];
    oldSCount = state.sCount[startLine]; //  - example list
    // ^ listIndent position will be here
    //   ^ blkIndent position will be here
    //

    oldListIndent = state.listIndent;
    state.listIndent = state.blkIndent;
    state.blkIndent = indent;
    state.tight = true;
    state.tShift[startLine] = contentStart - state.bMarks[startLine];
    state.sCount[startLine] = offset;

    if (contentStart >= max && state.isEmpty(startLine + 1)) {
      // workaround for this case
      // (list item is empty, list terminates before "foo"):
      // ~~~~~~~~
      //   -
      //
      //     foo
      // ~~~~~~~~
      state.line = Math.min(state.line + 2, endLine);
    } else {
      state.md.block.tokenize(state, startLine, endLine, true);
    } // If any of list item is tight, mark list as tight


    if (!state.tight || prevEmptyEnd) {
      tight = false;
    } // Item become loose if finish with empty line,
    // but we should filter last element, because it means list finish


    prevEmptyEnd = state.line - startLine > 1 && state.isEmpty(state.line - 1);
    state.blkIndent = state.listIndent;
    state.listIndent = oldListIndent;
    state.tShift[startLine] = oldTShift;
    state.sCount[startLine] = oldSCount;
    state.tight = oldTight;
    token = state.push('list_item_close', 'li', -1);
    token.markup = String.fromCharCode(markerCharCode);
    nextLine = startLine = state.line;
    itemLines[1] = nextLine;
    contentStart = state.bMarks[startLine];

    if (nextLine >= endLine) {
      break;
    } //
    // Try to check if list is terminated or continued.
    //


    if (state.sCount[nextLine] < state.blkIndent) {
      break;
    } // if it's indented more than 3 spaces, it should be a code block


    if (state.sCount[startLine] - state.blkIndent >= 4) {
      break;
    } // fail if terminating block found


    terminate = false;

    for (i = 0, l = terminatorRules.length; i < l; i++) {
      if (terminatorRules[i](state, nextLine, endLine, true)) {
        terminate = true;
        break;
      }
    }

    if (terminate) {
      break;
    } // fail if list has another type


    if (isOrdered) {
      posAfterMarker = skipOrderedListMarker(state, nextLine);

      if (posAfterMarker < 0) {
        break;
      }
    } else {
      posAfterMarker = skipBulletListMarker(state, nextLine);

      if (posAfterMarker < 0) {
        break;
      }
    }

    if (markerCharCode !== state.src.charCodeAt(posAfterMarker - 1)) {
      break;
    }
  } // Finalize list


  if (isOrdered) {
    token = state.push('ordered_list_close', 'ol', -1);
  } else {
    token = state.push('bullet_list_close', 'ul', -1);
  }

  token.markup = String.fromCharCode(markerCharCode);
  listLines[1] = nextLine;
  state.line = nextLine;
  state.parentType = oldParentType; // mark paragraphs tight if needed

  if (tight) {
    markTightParagraphs(state, listTokIdx);
  }

  return true;
};

/***/ }),

/***/ "../../node_modules/markdown-it/lib/rules_block/paragraph.js":
/*!*********************************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/markdown-it/lib/rules_block/paragraph.js ***!
  \*********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
// Paragraph


module.exports = function paragraph(state, startLine
/*, endLine*/
) {
  var content,
      terminate,
      i,
      l,
      token,
      oldParentType,
      nextLine = startLine + 1,
      terminatorRules = state.md.block.ruler.getRules('paragraph'),
      endLine = state.lineMax;
  oldParentType = state.parentType;
  state.parentType = 'paragraph'; // jump line-by-line until empty one or EOF

  for (; nextLine < endLine && !state.isEmpty(nextLine); nextLine++) {
    // this would be a code block normally, but after paragraph
    // it's considered a lazy continuation regardless of what's there
    if (state.sCount[nextLine] - state.blkIndent > 3) {
      continue;
    } // quirk for blockquotes, this line should already be checked by that rule


    if (state.sCount[nextLine] < 0) {
      continue;
    } // Some tags can terminate paragraph without empty line.


    terminate = false;

    for (i = 0, l = terminatorRules.length; i < l; i++) {
      if (terminatorRules[i](state, nextLine, endLine, true)) {
        terminate = true;
        break;
      }
    }

    if (terminate) {
      break;
    }
  }

  content = state.getLines(startLine, nextLine, state.blkIndent, false).trim();
  state.line = nextLine;
  token = state.push('paragraph_open', 'p', 1);
  token.map = [startLine, state.line];
  token = state.push('inline', '', 0);
  token.content = content;
  token.map = [startLine, state.line];
  token.children = [];
  token = state.push('paragraph_close', 'p', -1);
  state.parentType = oldParentType;
  return true;
};

/***/ }),

/***/ "../../node_modules/markdown-it/lib/rules_block/reference.js":
/*!*********************************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/markdown-it/lib/rules_block/reference.js ***!
  \*********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var normalizeReference = __webpack_require__(/*! ../common/utils */ "../../node_modules/markdown-it/lib/common/utils.js").normalizeReference;

var isSpace = __webpack_require__(/*! ../common/utils */ "../../node_modules/markdown-it/lib/common/utils.js").isSpace;

module.exports = function reference(state, startLine, _endLine, silent) {
  var ch,
      destEndPos,
      destEndLineNo,
      endLine,
      href,
      i,
      l,
      label,
      labelEnd,
      oldParentType,
      res,
      start,
      str,
      terminate,
      terminatorRules,
      title,
      lines = 0,
      pos = state.bMarks[startLine] + state.tShift[startLine],
      max = state.eMarks[startLine],
      nextLine = startLine + 1; // if it's indented more than 3 spaces, it should be a code block

  if (state.sCount[startLine] - state.blkIndent >= 4) {
    return false;
  }

  if (state.src.charCodeAt(pos) !== 0x5B
  /* [ */
  ) {
      return false;
    } // Simple check to quickly interrupt scan on [link](url) at the start of line.
  // Can be useful on practice: https://github.com/markdown-it/markdown-it/issues/54


  while (++pos < max) {
    if (state.src.charCodeAt(pos) === 0x5D
    /* ] */
    && state.src.charCodeAt(pos - 1) !== 0x5C
    /* \ */
    ) {
        if (pos + 1 === max) {
          return false;
        }

        if (state.src.charCodeAt(pos + 1) !== 0x3A
        /* : */
        ) {
            return false;
          }

        break;
      }
  }

  endLine = state.lineMax; // jump line-by-line until empty one or EOF

  terminatorRules = state.md.block.ruler.getRules('reference');
  oldParentType = state.parentType;
  state.parentType = 'reference';

  for (; nextLine < endLine && !state.isEmpty(nextLine); nextLine++) {
    // this would be a code block normally, but after paragraph
    // it's considered a lazy continuation regardless of what's there
    if (state.sCount[nextLine] - state.blkIndent > 3) {
      continue;
    } // quirk for blockquotes, this line should already be checked by that rule


    if (state.sCount[nextLine] < 0) {
      continue;
    } // Some tags can terminate paragraph without empty line.


    terminate = false;

    for (i = 0, l = terminatorRules.length; i < l; i++) {
      if (terminatorRules[i](state, nextLine, endLine, true)) {
        terminate = true;
        break;
      }
    }

    if (terminate) {
      break;
    }
  }

  str = state.getLines(startLine, nextLine, state.blkIndent, false).trim();
  max = str.length;

  for (pos = 1; pos < max; pos++) {
    ch = str.charCodeAt(pos);

    if (ch === 0x5B
    /* [ */
    ) {
        return false;
      } else if (ch === 0x5D
    /* ] */
    ) {
        labelEnd = pos;
        break;
      } else if (ch === 0x0A
    /* \n */
    ) {
        lines++;
      } else if (ch === 0x5C
    /* \ */
    ) {
        pos++;

        if (pos < max && str.charCodeAt(pos) === 0x0A) {
          lines++;
        }
      }
  }

  if (labelEnd < 0 || str.charCodeAt(labelEnd + 1) !== 0x3A
  /* : */
  ) {
      return false;
    } // [label]:   destination   'title'
  //         ^^^ skip optional whitespace here


  for (pos = labelEnd + 2; pos < max; pos++) {
    ch = str.charCodeAt(pos);

    if (ch === 0x0A) {
      lines++;
    } else if (isSpace(ch)) {
      /*eslint no-empty:0*/
    } else {
      break;
    }
  } // [label]:   destination   'title'
  //            ^^^^^^^^^^^ parse this


  res = state.md.helpers.parseLinkDestination(str, pos, max);

  if (!res.ok) {
    return false;
  }

  href = state.md.normalizeLink(res.str);

  if (!state.md.validateLink(href)) {
    return false;
  }

  pos = res.pos;
  lines += res.lines; // save cursor state, we could require to rollback later

  destEndPos = pos;
  destEndLineNo = lines; // [label]:   destination   'title'
  //                       ^^^ skipping those spaces

  start = pos;

  for (; pos < max; pos++) {
    ch = str.charCodeAt(pos);

    if (ch === 0x0A) {
      lines++;
    } else if (isSpace(ch)) {
      /*eslint no-empty:0*/
    } else {
      break;
    }
  } // [label]:   destination   'title'
  //                          ^^^^^^^ parse this


  res = state.md.helpers.parseLinkTitle(str, pos, max);

  if (pos < max && start !== pos && res.ok) {
    title = res.str;
    pos = res.pos;
    lines += res.lines;
  } else {
    title = '';
    pos = destEndPos;
    lines = destEndLineNo;
  } // skip trailing spaces until the rest of the line


  while (pos < max) {
    ch = str.charCodeAt(pos);

    if (!isSpace(ch)) {
      break;
    }

    pos++;
  }

  if (pos < max && str.charCodeAt(pos) !== 0x0A) {
    if (title) {
      // garbage at the end of the line after title,
      // but it could still be a valid reference if we roll back
      title = '';
      pos = destEndPos;
      lines = destEndLineNo;

      while (pos < max) {
        ch = str.charCodeAt(pos);

        if (!isSpace(ch)) {
          break;
        }

        pos++;
      }
    }
  }

  if (pos < max && str.charCodeAt(pos) !== 0x0A) {
    // garbage at the end of the line
    return false;
  }

  label = normalizeReference(str.slice(1, labelEnd));

  if (!label) {
    // CommonMark 0.20 disallows empty labels
    return false;
  } // Reference can not terminate anything. This check is for safety only.

  /*istanbul ignore if*/


  if (silent) {
    return true;
  }

  if (typeof state.env.references === 'undefined') {
    state.env.references = {};
  }

  if (typeof state.env.references[label] === 'undefined') {
    state.env.references[label] = {
      title: title,
      href: href
    };
  }

  state.parentType = oldParentType;
  state.line = startLine + lines + 1;
  return true;
};

/***/ }),

/***/ "../../node_modules/markdown-it/lib/rules_block/state_block.js":
/*!***********************************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/markdown-it/lib/rules_block/state_block.js ***!
  \***********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
// Parser state class


var Token = __webpack_require__(/*! ../token */ "../../node_modules/markdown-it/lib/token.js");

var isSpace = __webpack_require__(/*! ../common/utils */ "../../node_modules/markdown-it/lib/common/utils.js").isSpace;

function StateBlock(src, md, env, tokens) {
  var ch, s, start, pos, len, indent, offset, indent_found;
  this.src = src; // link to parser instance

  this.md = md;
  this.env = env; //
  // Internal state vartiables
  //

  this.tokens = tokens;
  this.bMarks = []; // line begin offsets for fast jumps

  this.eMarks = []; // line end offsets for fast jumps

  this.tShift = []; // offsets of the first non-space characters (tabs not expanded)

  this.sCount = []; // indents for each line (tabs expanded)
  // An amount of virtual spaces (tabs expanded) between beginning
  // of each line (bMarks) and real beginning of that line.
  //
  // It exists only as a hack because blockquotes override bMarks
  // losing information in the process.
  //
  // It's used only when expanding tabs, you can think about it as
  // an initial tab length, e.g. bsCount=21 applied to string `\t123`
  // means first tab should be expanded to 4-21%4 === 3 spaces.
  //

  this.bsCount = []; // block parser variables

  this.blkIndent = 0; // required block content indent (for example, if we are
  // inside a list, it would be positioned after list marker)

  this.line = 0; // line index in src

  this.lineMax = 0; // lines count

  this.tight = false; // loose/tight mode for lists

  this.ddIndent = -1; // indent of the current dd block (-1 if there isn't any)

  this.listIndent = -1; // indent of the current list block (-1 if there isn't any)
  // can be 'blockquote', 'list', 'root', 'paragraph' or 'reference'
  // used in lists to determine if they interrupt a paragraph

  this.parentType = 'root';
  this.level = 0; // renderer

  this.result = ''; // Create caches
  // Generate markers.

  s = this.src;
  indent_found = false;

  for (start = pos = indent = offset = 0, len = s.length; pos < len; pos++) {
    ch = s.charCodeAt(pos);

    if (!indent_found) {
      if (isSpace(ch)) {
        indent++;

        if (ch === 0x09) {
          offset += 4 - offset % 4;
        } else {
          offset++;
        }

        continue;
      } else {
        indent_found = true;
      }
    }

    if (ch === 0x0A || pos === len - 1) {
      if (ch !== 0x0A) {
        pos++;
      }

      this.bMarks.push(start);
      this.eMarks.push(pos);
      this.tShift.push(indent);
      this.sCount.push(offset);
      this.bsCount.push(0);
      indent_found = false;
      indent = 0;
      offset = 0;
      start = pos + 1;
    }
  } // Push fake entry to simplify cache bounds checks


  this.bMarks.push(s.length);
  this.eMarks.push(s.length);
  this.tShift.push(0);
  this.sCount.push(0);
  this.bsCount.push(0);
  this.lineMax = this.bMarks.length - 1; // don't count last fake line
} // Push new token to "stream".
//


StateBlock.prototype.push = function (type, tag, nesting) {
  var token = new Token(type, tag, nesting);
  token.block = true;
  if (nesting < 0) this.level--; // closing tag

  token.level = this.level;
  if (nesting > 0) this.level++; // opening tag

  this.tokens.push(token);
  return token;
};

StateBlock.prototype.isEmpty = function isEmpty(line) {
  return this.bMarks[line] + this.tShift[line] >= this.eMarks[line];
};

StateBlock.prototype.skipEmptyLines = function skipEmptyLines(from) {
  for (var max = this.lineMax; from < max; from++) {
    if (this.bMarks[from] + this.tShift[from] < this.eMarks[from]) {
      break;
    }
  }

  return from;
}; // Skip spaces from given position.


StateBlock.prototype.skipSpaces = function skipSpaces(pos) {
  var ch;

  for (var max = this.src.length; pos < max; pos++) {
    ch = this.src.charCodeAt(pos);

    if (!isSpace(ch)) {
      break;
    }
  }

  return pos;
}; // Skip spaces from given position in reverse.


StateBlock.prototype.skipSpacesBack = function skipSpacesBack(pos, min) {
  if (pos <= min) {
    return pos;
  }

  while (pos > min) {
    if (!isSpace(this.src.charCodeAt(--pos))) {
      return pos + 1;
    }
  }

  return pos;
}; // Skip char codes from given position


StateBlock.prototype.skipChars = function skipChars(pos, code) {
  for (var max = this.src.length; pos < max; pos++) {
    if (this.src.charCodeAt(pos) !== code) {
      break;
    }
  }

  return pos;
}; // Skip char codes reverse from given position - 1


StateBlock.prototype.skipCharsBack = function skipCharsBack(pos, code, min) {
  if (pos <= min) {
    return pos;
  }

  while (pos > min) {
    if (code !== this.src.charCodeAt(--pos)) {
      return pos + 1;
    }
  }

  return pos;
}; // cut lines range from source.


StateBlock.prototype.getLines = function getLines(begin, end, indent, keepLastLF) {
  var i,
      lineIndent,
      ch,
      first,
      last,
      queue,
      lineStart,
      line = begin;

  if (begin >= end) {
    return '';
  }

  queue = new Array(end - begin);

  for (i = 0; line < end; line++, i++) {
    lineIndent = 0;
    lineStart = first = this.bMarks[line];

    if (line + 1 < end || keepLastLF) {
      // No need for bounds check because we have fake entry on tail.
      last = this.eMarks[line] + 1;
    } else {
      last = this.eMarks[line];
    }

    while (first < last && lineIndent < indent) {
      ch = this.src.charCodeAt(first);

      if (isSpace(ch)) {
        if (ch === 0x09) {
          lineIndent += 4 - (lineIndent + this.bsCount[line]) % 4;
        } else {
          lineIndent++;
        }
      } else if (first - lineStart < this.tShift[line]) {
        // patched tShift masked characters to look like spaces (blockquotes, list markers)
        lineIndent++;
      } else {
        break;
      }

      first++;
    }

    if (lineIndent > indent) {
      // partially expanding tabs in code blocks, e.g '\t\tfoobar'
      // with indent=2 becomes '  \tfoobar'
      queue[i] = new Array(lineIndent - indent + 1).join(' ') + this.src.slice(first, last);
    } else {
      queue[i] = this.src.slice(first, last);
    }
  }

  return queue.join('');
}; // re-export Token class to use in block rules


StateBlock.prototype.Token = Token;
module.exports = StateBlock;

/***/ }),

/***/ "../../node_modules/markdown-it/lib/rules_block/table.js":
/*!*****************************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/markdown-it/lib/rules_block/table.js ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
// GFM table, non-standard


var isSpace = __webpack_require__(/*! ../common/utils */ "../../node_modules/markdown-it/lib/common/utils.js").isSpace;

function getLine(state, line) {
  var pos = state.bMarks[line] + state.blkIndent,
      max = state.eMarks[line];
  return state.src.substr(pos, max - pos);
}

function escapedSplit(str) {
  var result = [],
      pos = 0,
      max = str.length,
      ch,
      escapes = 0,
      lastPos = 0,
      backTicked = false,
      lastBackTick = 0;
  ch = str.charCodeAt(pos);

  while (pos < max) {
    if (ch === 0x60
    /* ` */
    ) {
        if (backTicked) {
          // make \` close code sequence, but not open it;
          // the reason is: `\` is correct code block
          backTicked = false;
          lastBackTick = pos;
        } else if (escapes % 2 === 0) {
          backTicked = true;
          lastBackTick = pos;
        }
      } else if (ch === 0x7c
    /* | */
    && escapes % 2 === 0 && !backTicked) {
      result.push(str.substring(lastPos, pos));
      lastPos = pos + 1;
    }

    if (ch === 0x5c
    /* \ */
    ) {
        escapes++;
      } else {
      escapes = 0;
    }

    pos++; // If there was an un-closed backtick, go back to just after
    // the last backtick, but as if it was a normal character

    if (pos === max && backTicked) {
      backTicked = false;
      pos = lastBackTick + 1;
    }

    ch = str.charCodeAt(pos);
  }

  result.push(str.substring(lastPos));
  return result;
}

module.exports = function table(state, startLine, endLine, silent) {
  var ch, lineText, pos, i, nextLine, columns, columnCount, token, aligns, t, tableLines, tbodyLines; // should have at least two lines

  if (startLine + 2 > endLine) {
    return false;
  }

  nextLine = startLine + 1;

  if (state.sCount[nextLine] < state.blkIndent) {
    return false;
  } // if it's indented more than 3 spaces, it should be a code block


  if (state.sCount[nextLine] - state.blkIndent >= 4) {
    return false;
  } // first character of the second line should be '|', '-', ':',
  // and no other characters are allowed but spaces;
  // basically, this is the equivalent of /^[-:|][-:|\s]*$/ regexp


  pos = state.bMarks[nextLine] + state.tShift[nextLine];

  if (pos >= state.eMarks[nextLine]) {
    return false;
  }

  ch = state.src.charCodeAt(pos++);

  if (ch !== 0x7C
  /* | */
  && ch !== 0x2D
  /* - */
  && ch !== 0x3A
  /* : */
  ) {
      return false;
    }

  while (pos < state.eMarks[nextLine]) {
    ch = state.src.charCodeAt(pos);

    if (ch !== 0x7C
    /* | */
    && ch !== 0x2D
    /* - */
    && ch !== 0x3A
    /* : */
    && !isSpace(ch)) {
      return false;
    }

    pos++;
  }

  lineText = getLine(state, startLine + 1);
  columns = lineText.split('|');
  aligns = [];

  for (i = 0; i < columns.length; i++) {
    t = columns[i].trim();

    if (!t) {
      // allow empty columns before and after table, but not in between columns;
      // e.g. allow ` |---| `, disallow ` ---||--- `
      if (i === 0 || i === columns.length - 1) {
        continue;
      } else {
        return false;
      }
    }

    if (!/^:?-+:?$/.test(t)) {
      return false;
    }

    if (t.charCodeAt(t.length - 1) === 0x3A
    /* : */
    ) {
        aligns.push(t.charCodeAt(0) === 0x3A
        /* : */
        ? 'center' : 'right');
      } else if (t.charCodeAt(0) === 0x3A
    /* : */
    ) {
        aligns.push('left');
      } else {
      aligns.push('');
    }
  }

  lineText = getLine(state, startLine).trim();

  if (lineText.indexOf('|') === -1) {
    return false;
  }

  if (state.sCount[startLine] - state.blkIndent >= 4) {
    return false;
  }

  columns = escapedSplit(lineText.replace(/^\||\|$/g, '')); // header row will define an amount of columns in the entire table,
  // and align row shouldn't be smaller than that (the rest of the rows can)

  columnCount = columns.length;

  if (columnCount > aligns.length) {
    return false;
  }

  if (silent) {
    return true;
  }

  token = state.push('table_open', 'table', 1);
  token.map = tableLines = [startLine, 0];
  token = state.push('thead_open', 'thead', 1);
  token.map = [startLine, startLine + 1];
  token = state.push('tr_open', 'tr', 1);
  token.map = [startLine, startLine + 1];

  for (i = 0; i < columns.length; i++) {
    token = state.push('th_open', 'th', 1);
    token.map = [startLine, startLine + 1];

    if (aligns[i]) {
      token.attrs = [['style', 'text-align:' + aligns[i]]];
    }

    token = state.push('inline', '', 0);
    token.content = columns[i].trim();
    token.map = [startLine, startLine + 1];
    token.children = [];
    token = state.push('th_close', 'th', -1);
  }

  token = state.push('tr_close', 'tr', -1);
  token = state.push('thead_close', 'thead', -1);
  token = state.push('tbody_open', 'tbody', 1);
  token.map = tbodyLines = [startLine + 2, 0];

  for (nextLine = startLine + 2; nextLine < endLine; nextLine++) {
    if (state.sCount[nextLine] < state.blkIndent) {
      break;
    }

    lineText = getLine(state, nextLine).trim();

    if (lineText.indexOf('|') === -1) {
      break;
    }

    if (state.sCount[nextLine] - state.blkIndent >= 4) {
      break;
    }

    columns = escapedSplit(lineText.replace(/^\||\|$/g, ''));
    token = state.push('tr_open', 'tr', 1);

    for (i = 0; i < columnCount; i++) {
      token = state.push('td_open', 'td', 1);

      if (aligns[i]) {
        token.attrs = [['style', 'text-align:' + aligns[i]]];
      }

      token = state.push('inline', '', 0);
      token.content = columns[i] ? columns[i].trim() : '';
      token.children = [];
      token = state.push('td_close', 'td', -1);
    }

    token = state.push('tr_close', 'tr', -1);
  }

  token = state.push('tbody_close', 'tbody', -1);
  token = state.push('table_close', 'table', -1);
  tableLines[1] = tbodyLines[1] = nextLine;
  state.line = nextLine;
  return true;
};

/***/ }),

/***/ "../../node_modules/markdown-it/lib/rules_core/block.js":
/*!****************************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/markdown-it/lib/rules_core/block.js ***!
  \****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = function block(state) {
  var token;

  if (state.inlineMode) {
    token = new state.Token('inline', '', 0);
    token.content = state.src;
    token.map = [0, 1];
    token.children = [];
    state.tokens.push(token);
  } else {
    state.md.block.parse(state.src, state.md, state.env, state.tokens);
  }
};

/***/ }),

/***/ "../../node_modules/markdown-it/lib/rules_core/inline.js":
/*!*****************************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/markdown-it/lib/rules_core/inline.js ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = function inline(state) {
  var tokens = state.tokens,
      tok,
      i,
      l; // Parse inlines

  for (i = 0, l = tokens.length; i < l; i++) {
    tok = tokens[i];

    if (tok.type === 'inline') {
      state.md.inline.parse(tok.content, state.md, state.env, tok.children);
    }
  }
};

/***/ }),

/***/ "../../node_modules/markdown-it/lib/rules_core/linkify.js":
/*!******************************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/markdown-it/lib/rules_core/linkify.js ***!
  \******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
// Replace link-like texts with link nodes.
//
// Currently restricted by `md.validateLink()` to http/https/ftp
//


var arrayReplaceAt = __webpack_require__(/*! ../common/utils */ "../../node_modules/markdown-it/lib/common/utils.js").arrayReplaceAt;

function isLinkOpen(str) {
  return /^<a[>\s]/i.test(str);
}

function isLinkClose(str) {
  return /^<\/a\s*>/i.test(str);
}

module.exports = function linkify(state) {
  var i,
      j,
      l,
      tokens,
      token,
      currentToken,
      nodes,
      ln,
      text,
      pos,
      lastPos,
      level,
      htmlLinkLevel,
      url,
      fullUrl,
      urlText,
      blockTokens = state.tokens,
      links;

  if (!state.md.options.linkify) {
    return;
  }

  for (j = 0, l = blockTokens.length; j < l; j++) {
    if (blockTokens[j].type !== 'inline' || !state.md.linkify.pretest(blockTokens[j].content)) {
      continue;
    }

    tokens = blockTokens[j].children;
    htmlLinkLevel = 0; // We scan from the end, to keep position when new tags added.
    // Use reversed logic in links start/end match

    for (i = tokens.length - 1; i >= 0; i--) {
      currentToken = tokens[i]; // Skip content of markdown links

      if (currentToken.type === 'link_close') {
        i--;

        while (tokens[i].level !== currentToken.level && tokens[i].type !== 'link_open') {
          i--;
        }

        continue;
      } // Skip content of html tag links


      if (currentToken.type === 'html_inline') {
        if (isLinkOpen(currentToken.content) && htmlLinkLevel > 0) {
          htmlLinkLevel--;
        }

        if (isLinkClose(currentToken.content)) {
          htmlLinkLevel++;
        }
      }

      if (htmlLinkLevel > 0) {
        continue;
      }

      if (currentToken.type === 'text' && state.md.linkify.test(currentToken.content)) {
        text = currentToken.content;
        links = state.md.linkify.match(text); // Now split string to nodes

        nodes = [];
        level = currentToken.level;
        lastPos = 0;

        for (ln = 0; ln < links.length; ln++) {
          url = links[ln].url;
          fullUrl = state.md.normalizeLink(url);

          if (!state.md.validateLink(fullUrl)) {
            continue;
          }

          urlText = links[ln].text; // Linkifier might send raw hostnames like "example.com", where url
          // starts with domain name. So we prepend http:// in those cases,
          // and remove it afterwards.
          //

          if (!links[ln].schema) {
            urlText = state.md.normalizeLinkText('http://' + urlText).replace(/^http:\/\//, '');
          } else if (links[ln].schema === 'mailto:' && !/^mailto:/i.test(urlText)) {
            urlText = state.md.normalizeLinkText('mailto:' + urlText).replace(/^mailto:/, '');
          } else {
            urlText = state.md.normalizeLinkText(urlText);
          }

          pos = links[ln].index;

          if (pos > lastPos) {
            token = new state.Token('text', '', 0);
            token.content = text.slice(lastPos, pos);
            token.level = level;
            nodes.push(token);
          }

          token = new state.Token('link_open', 'a', 1);
          token.attrs = [['href', fullUrl]];
          token.level = level++;
          token.markup = 'linkify';
          token.info = 'auto';
          nodes.push(token);
          token = new state.Token('text', '', 0);
          token.content = urlText;
          token.level = level;
          nodes.push(token);
          token = new state.Token('link_close', 'a', -1);
          token.level = --level;
          token.markup = 'linkify';
          token.info = 'auto';
          nodes.push(token);
          lastPos = links[ln].lastIndex;
        }

        if (lastPos < text.length) {
          token = new state.Token('text', '', 0);
          token.content = text.slice(lastPos);
          token.level = level;
          nodes.push(token);
        } // replace current node


        blockTokens[j].children = tokens = arrayReplaceAt(tokens, i, nodes);
      }
    }
  }
};

/***/ }),

/***/ "../../node_modules/markdown-it/lib/rules_core/normalize.js":
/*!********************************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/markdown-it/lib/rules_core/normalize.js ***!
  \********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
// Normalize input string
 // https://spec.commonmark.org/0.29/#line-ending

var NEWLINES_RE = /\r\n?|\n/g;
var NULL_RE = /\0/g;

module.exports = function normalize(state) {
  var str; // Normalize newlines

  str = state.src.replace(NEWLINES_RE, '\n'); // Replace NULL characters

  str = str.replace(NULL_RE, "\uFFFD");
  state.src = str;
};

/***/ }),

/***/ "../../node_modules/markdown-it/lib/rules_core/replacements.js":
/*!***********************************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/markdown-it/lib/rules_core/replacements.js ***!
  \***********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
// Simple typographic replacements
//
// (c) (C) → ©
// (tm) (TM) → ™
// (r) (R) → ®
// +- → ±
// (p) (P) -> §
// ... → … (also ?.... → ?.., !.... → !..)
// ???????? → ???, !!!!! → !!!, `,,` → `,`
// -- → &ndash;, --- → &mdash;
//
 // TODO:
// - fractionals 1/2, 1/4, 3/4 -> ½, ¼, ¾
// - miltiplication 2 x 4 -> 2 × 4

var RARE_RE = /\+-|\.\.|\?\?\?\?|!!!!|,,|--/; // Workaround for phantomjs - need regex without /g flag,
// or root check will fail every second time

var SCOPED_ABBR_TEST_RE = /\((c|tm|r|p)\)/i;
var SCOPED_ABBR_RE = /\((c|tm|r|p)\)/ig;
var SCOPED_ABBR = {
  c: '©',
  r: '®',
  p: '§',
  tm: '™'
};

function replaceFn(match, name) {
  return SCOPED_ABBR[name.toLowerCase()];
}

function replace_scoped(inlineTokens) {
  var i,
      token,
      inside_autolink = 0;

  for (i = inlineTokens.length - 1; i >= 0; i--) {
    token = inlineTokens[i];

    if (token.type === 'text' && !inside_autolink) {
      token.content = token.content.replace(SCOPED_ABBR_RE, replaceFn);
    }

    if (token.type === 'link_open' && token.info === 'auto') {
      inside_autolink--;
    }

    if (token.type === 'link_close' && token.info === 'auto') {
      inside_autolink++;
    }
  }
}

function replace_rare(inlineTokens) {
  var i,
      token,
      inside_autolink = 0;

  for (i = inlineTokens.length - 1; i >= 0; i--) {
    token = inlineTokens[i];

    if (token.type === 'text' && !inside_autolink) {
      if (RARE_RE.test(token.content)) {
        token.content = token.content.replace(/\+-/g, '±') // .., ..., ....... -> …
        // but ?..... & !..... -> ?.. & !..
        .replace(/\.{2,}/g, '…').replace(/([?!])…/g, '$1..').replace(/([?!]){4,}/g, '$1$1$1').replace(/,{2,}/g, ',') // em-dash
        .replace(/(^|[^-])---([^-]|$)/mg, "$1\u2014$2") // en-dash
        .replace(/(^|\s)--(\s|$)/mg, "$1\u2013$2").replace(/(^|[^-\s])--([^-\s]|$)/mg, "$1\u2013$2");
      }
    }

    if (token.type === 'link_open' && token.info === 'auto') {
      inside_autolink--;
    }

    if (token.type === 'link_close' && token.info === 'auto') {
      inside_autolink++;
    }
  }
}

module.exports = function replace(state) {
  var blkIdx;

  if (!state.md.options.typographer) {
    return;
  }

  for (blkIdx = state.tokens.length - 1; blkIdx >= 0; blkIdx--) {
    if (state.tokens[blkIdx].type !== 'inline') {
      continue;
    }

    if (SCOPED_ABBR_TEST_RE.test(state.tokens[blkIdx].content)) {
      replace_scoped(state.tokens[blkIdx].children);
    }

    if (RARE_RE.test(state.tokens[blkIdx].content)) {
      replace_rare(state.tokens[blkIdx].children);
    }
  }
};

/***/ }),

/***/ "../../node_modules/markdown-it/lib/rules_core/smartquotes.js":
/*!**********************************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/markdown-it/lib/rules_core/smartquotes.js ***!
  \**********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
// Convert straight quotation marks to typographic ones
//


var isWhiteSpace = __webpack_require__(/*! ../common/utils */ "../../node_modules/markdown-it/lib/common/utils.js").isWhiteSpace;

var isPunctChar = __webpack_require__(/*! ../common/utils */ "../../node_modules/markdown-it/lib/common/utils.js").isPunctChar;

var isMdAsciiPunct = __webpack_require__(/*! ../common/utils */ "../../node_modules/markdown-it/lib/common/utils.js").isMdAsciiPunct;

var QUOTE_TEST_RE = /['"]/;
var QUOTE_RE = /['"]/g;
var APOSTROPHE = "\u2019";
/* ’ */

function replaceAt(str, index, ch) {
  return str.substr(0, index) + ch + str.substr(index + 1);
}

function process_inlines(tokens, state) {
  var i, token, text, t, pos, max, thisLevel, item, lastChar, nextChar, isLastPunctChar, isNextPunctChar, isLastWhiteSpace, isNextWhiteSpace, canOpen, canClose, j, isSingle, stack, openQuote, closeQuote;
  stack = [];

  for (i = 0; i < tokens.length; i++) {
    token = tokens[i];
    thisLevel = tokens[i].level;

    for (j = stack.length - 1; j >= 0; j--) {
      if (stack[j].level <= thisLevel) {
        break;
      }
    }

    stack.length = j + 1;

    if (token.type !== 'text') {
      continue;
    }

    text = token.content;
    pos = 0;
    max = text.length;
    /*eslint no-labels:0,block-scoped-var:0*/

    OUTER: while (pos < max) {
      QUOTE_RE.lastIndex = pos;
      t = QUOTE_RE.exec(text);

      if (!t) {
        break;
      }

      canOpen = canClose = true;
      pos = t.index + 1;
      isSingle = t[0] === "'"; // Find previous character,
      // default to space if it's the beginning of the line
      //

      lastChar = 0x20;

      if (t.index - 1 >= 0) {
        lastChar = text.charCodeAt(t.index - 1);
      } else {
        for (j = i - 1; j >= 0; j--) {
          if (tokens[j].type === 'softbreak' || tokens[j].type === 'hardbreak') break; // lastChar defaults to 0x20

          if (tokens[j].type !== 'text') continue;
          lastChar = tokens[j].content.charCodeAt(tokens[j].content.length - 1);
          break;
        }
      } // Find next character,
      // default to space if it's the end of the line
      //


      nextChar = 0x20;

      if (pos < max) {
        nextChar = text.charCodeAt(pos);
      } else {
        for (j = i + 1; j < tokens.length; j++) {
          if (tokens[j].type === 'softbreak' || tokens[j].type === 'hardbreak') break; // nextChar defaults to 0x20

          if (tokens[j].type !== 'text') continue;
          nextChar = tokens[j].content.charCodeAt(0);
          break;
        }
      }

      isLastPunctChar = isMdAsciiPunct(lastChar) || isPunctChar(String.fromCharCode(lastChar));
      isNextPunctChar = isMdAsciiPunct(nextChar) || isPunctChar(String.fromCharCode(nextChar));
      isLastWhiteSpace = isWhiteSpace(lastChar);
      isNextWhiteSpace = isWhiteSpace(nextChar);

      if (isNextWhiteSpace) {
        canOpen = false;
      } else if (isNextPunctChar) {
        if (!(isLastWhiteSpace || isLastPunctChar)) {
          canOpen = false;
        }
      }

      if (isLastWhiteSpace) {
        canClose = false;
      } else if (isLastPunctChar) {
        if (!(isNextWhiteSpace || isNextPunctChar)) {
          canClose = false;
        }
      }

      if (nextChar === 0x22
      /* " */
      && t[0] === '"') {
        if (lastChar >= 0x30
        /* 0 */
        && lastChar <= 0x39
        /* 9 */
        ) {
            // special case: 1"" - count first quote as an inch
            canClose = canOpen = false;
          }
      }

      if (canOpen && canClose) {
        // treat this as the middle of the word
        canOpen = false;
        canClose = isNextPunctChar;
      }

      if (!canOpen && !canClose) {
        // middle of word
        if (isSingle) {
          token.content = replaceAt(token.content, t.index, APOSTROPHE);
        }

        continue;
      }

      if (canClose) {
        // this could be a closing quote, rewind the stack to get a match
        for (j = stack.length - 1; j >= 0; j--) {
          item = stack[j];

          if (stack[j].level < thisLevel) {
            break;
          }

          if (item.single === isSingle && stack[j].level === thisLevel) {
            item = stack[j];

            if (isSingle) {
              openQuote = state.md.options.quotes[2];
              closeQuote = state.md.options.quotes[3];
            } else {
              openQuote = state.md.options.quotes[0];
              closeQuote = state.md.options.quotes[1];
            } // replace token.content *before* tokens[item.token].content,
            // because, if they are pointing at the same token, replaceAt
            // could mess up indices when quote length != 1


            token.content = replaceAt(token.content, t.index, closeQuote);
            tokens[item.token].content = replaceAt(tokens[item.token].content, item.pos, openQuote);
            pos += closeQuote.length - 1;

            if (item.token === i) {
              pos += openQuote.length - 1;
            }

            text = token.content;
            max = text.length;
            stack.length = j;
            continue OUTER;
          }
        }
      }

      if (canOpen) {
        stack.push({
          token: i,
          pos: t.index,
          single: isSingle,
          level: thisLevel
        });
      } else if (canClose && isSingle) {
        token.content = replaceAt(token.content, t.index, APOSTROPHE);
      }
    }
  }
}

module.exports = function smartquotes(state) {
  /*eslint max-depth:0*/
  var blkIdx;

  if (!state.md.options.typographer) {
    return;
  }

  for (blkIdx = state.tokens.length - 1; blkIdx >= 0; blkIdx--) {
    if (state.tokens[blkIdx].type !== 'inline' || !QUOTE_TEST_RE.test(state.tokens[blkIdx].content)) {
      continue;
    }

    process_inlines(state.tokens[blkIdx].children, state);
  }
};

/***/ }),

/***/ "../../node_modules/markdown-it/lib/rules_core/state_core.js":
/*!*********************************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/markdown-it/lib/rules_core/state_core.js ***!
  \*********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
// Core state object
//


var Token = __webpack_require__(/*! ../token */ "../../node_modules/markdown-it/lib/token.js");

function StateCore(src, md, env) {
  this.src = src;
  this.env = env;
  this.tokens = [];
  this.inlineMode = false;
  this.md = md; // link to parser instance
} // re-export Token class to use in core rules


StateCore.prototype.Token = Token;
module.exports = StateCore;

/***/ }),

/***/ "../../node_modules/markdown-it/lib/rules_inline/autolink.js":
/*!*********************************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/markdown-it/lib/rules_inline/autolink.js ***!
  \*********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
// Process autolinks '<protocol:...>'

/*eslint max-len:0*/

var EMAIL_RE = /^<([a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*)>/;
var AUTOLINK_RE = /^<([a-zA-Z][a-zA-Z0-9+.\-]{1,31}):([^<>\x00-\x20]*)>/;

module.exports = function autolink(state, silent) {
  var tail,
      linkMatch,
      emailMatch,
      url,
      fullUrl,
      token,
      pos = state.pos;

  if (state.src.charCodeAt(pos) !== 0x3C
  /* < */
  ) {
      return false;
    }

  tail = state.src.slice(pos);

  if (tail.indexOf('>') < 0) {
    return false;
  }

  if (AUTOLINK_RE.test(tail)) {
    linkMatch = tail.match(AUTOLINK_RE);
    url = linkMatch[0].slice(1, -1);
    fullUrl = state.md.normalizeLink(url);

    if (!state.md.validateLink(fullUrl)) {
      return false;
    }

    if (!silent) {
      token = state.push('link_open', 'a', 1);
      token.attrs = [['href', fullUrl]];
      token.markup = 'autolink';
      token.info = 'auto';
      token = state.push('text', '', 0);
      token.content = state.md.normalizeLinkText(url);
      token = state.push('link_close', 'a', -1);
      token.markup = 'autolink';
      token.info = 'auto';
    }

    state.pos += linkMatch[0].length;
    return true;
  }

  if (EMAIL_RE.test(tail)) {
    emailMatch = tail.match(EMAIL_RE);
    url = emailMatch[0].slice(1, -1);
    fullUrl = state.md.normalizeLink('mailto:' + url);

    if (!state.md.validateLink(fullUrl)) {
      return false;
    }

    if (!silent) {
      token = state.push('link_open', 'a', 1);
      token.attrs = [['href', fullUrl]];
      token.markup = 'autolink';
      token.info = 'auto';
      token = state.push('text', '', 0);
      token.content = state.md.normalizeLinkText(url);
      token = state.push('link_close', 'a', -1);
      token.markup = 'autolink';
      token.info = 'auto';
    }

    state.pos += emailMatch[0].length;
    return true;
  }

  return false;
};

/***/ }),

/***/ "../../node_modules/markdown-it/lib/rules_inline/backticks.js":
/*!**********************************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/markdown-it/lib/rules_inline/backticks.js ***!
  \**********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
// Parse backticks


module.exports = function backtick(state, silent) {
  var start,
      max,
      marker,
      matchStart,
      matchEnd,
      token,
      pos = state.pos,
      ch = state.src.charCodeAt(pos);

  if (ch !== 0x60
  /* ` */
  ) {
      return false;
    }

  start = pos;
  pos++;
  max = state.posMax;

  while (pos < max && state.src.charCodeAt(pos) === 0x60
  /* ` */
  ) {
    pos++;
  }

  marker = state.src.slice(start, pos);
  matchStart = matchEnd = pos;

  while ((matchStart = state.src.indexOf('`', matchEnd)) !== -1) {
    matchEnd = matchStart + 1;

    while (matchEnd < max && state.src.charCodeAt(matchEnd) === 0x60
    /* ` */
    ) {
      matchEnd++;
    }

    if (matchEnd - matchStart === marker.length) {
      if (!silent) {
        token = state.push('code_inline', 'code', 0);
        token.markup = marker;
        token.content = state.src.slice(pos, matchStart).replace(/\n/g, ' ').replace(/^ (.+) $/, '$1');
      }

      state.pos = matchEnd;
      return true;
    }
  }

  if (!silent) {
    state.pending += marker;
  }

  state.pos += marker.length;
  return true;
};

/***/ }),

/***/ "../../node_modules/markdown-it/lib/rules_inline/balance_pairs.js":
/*!**************************************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/markdown-it/lib/rules_inline/balance_pairs.js ***!
  \**************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
// For each opening emphasis-like marker find a matching closing one
//


function processDelimiters(state, delimiters) {
  var closerIdx,
      openerIdx,
      closer,
      opener,
      minOpenerIdx,
      newMinOpenerIdx,
      isOddMatch,
      lastJump,
      openersBottom = {},
      max = delimiters.length;

  for (closerIdx = 0; closerIdx < max; closerIdx++) {
    closer = delimiters[closerIdx]; // Length is only used for emphasis-specific "rule of 3",
    // if it's not defined (in strikethrough or 3rd party plugins),
    // we can default it to 0 to disable those checks.
    //

    closer.length = closer.length || 0;
    if (!closer.close) continue; // Previously calculated lower bounds (previous fails)
    // for each marker and each delimiter length modulo 3.

    if (!openersBottom.hasOwnProperty(closer.marker)) {
      openersBottom[closer.marker] = [-1, -1, -1];
    }

    minOpenerIdx = openersBottom[closer.marker][closer.length % 3];
    newMinOpenerIdx = -1;
    openerIdx = closerIdx - closer.jump - 1;

    for (; openerIdx > minOpenerIdx; openerIdx -= opener.jump + 1) {
      opener = delimiters[openerIdx];
      if (opener.marker !== closer.marker) continue;
      if (newMinOpenerIdx === -1) newMinOpenerIdx = openerIdx;

      if (opener.open && opener.end < 0 && opener.level === closer.level) {
        isOddMatch = false; // from spec:
        //
        // If one of the delimiters can both open and close emphasis, then the
        // sum of the lengths of the delimiter runs containing the opening and
        // closing delimiters must not be a multiple of 3 unless both lengths
        // are multiples of 3.
        //

        if (opener.close || closer.open) {
          if ((opener.length + closer.length) % 3 === 0) {
            if (opener.length % 3 !== 0 || closer.length % 3 !== 0) {
              isOddMatch = true;
            }
          }
        }

        if (!isOddMatch) {
          // If previous delimiter cannot be an opener, we can safely skip
          // the entire sequence in future checks. This is required to make
          // sure algorithm has linear complexity (see *_*_*_*_*_... case).
          //
          lastJump = openerIdx > 0 && !delimiters[openerIdx - 1].open ? delimiters[openerIdx - 1].jump + 1 : 0;
          closer.jump = closerIdx - openerIdx + lastJump;
          closer.open = false;
          opener.end = closerIdx;
          opener.jump = lastJump;
          opener.close = false;
          newMinOpenerIdx = -1;
          break;
        }
      }
    }

    if (newMinOpenerIdx !== -1) {
      // If match for this delimiter run failed, we want to set lower bound for
      // future lookups. This is required to make sure algorithm has linear
      // complexity.
      //
      // See details here:
      // https://github.com/commonmark/cmark/issues/178#issuecomment-270417442
      //
      openersBottom[closer.marker][(closer.length || 0) % 3] = newMinOpenerIdx;
    }
  }
}

module.exports = function link_pairs(state) {
  var curr,
      tokens_meta = state.tokens_meta,
      max = state.tokens_meta.length;
  processDelimiters(state, state.delimiters);

  for (curr = 0; curr < max; curr++) {
    if (tokens_meta[curr] && tokens_meta[curr].delimiters) {
      processDelimiters(state, tokens_meta[curr].delimiters);
    }
  }
};

/***/ }),

/***/ "../../node_modules/markdown-it/lib/rules_inline/emphasis.js":
/*!*********************************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/markdown-it/lib/rules_inline/emphasis.js ***!
  \*********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
// Process *this* and _that_
//
 // Insert each marker as a separate text token, and add it to delimiter list
//

module.exports.tokenize = function emphasis(state, silent) {
  var i,
      scanned,
      token,
      start = state.pos,
      marker = state.src.charCodeAt(start);

  if (silent) {
    return false;
  }

  if (marker !== 0x5F
  /* _ */
  && marker !== 0x2A
  /* * */
  ) {
      return false;
    }

  scanned = state.scanDelims(state.pos, marker === 0x2A);

  for (i = 0; i < scanned.length; i++) {
    token = state.push('text', '', 0);
    token.content = String.fromCharCode(marker);
    state.delimiters.push({
      // Char code of the starting marker (number).
      //
      marker: marker,
      // Total length of these series of delimiters.
      //
      length: scanned.length,
      // An amount of characters before this one that's equivalent to
      // current one. In plain English: if this delimiter does not open
      // an emphasis, neither do previous `jump` characters.
      //
      // Used to skip sequences like "*****" in one step, for 1st asterisk
      // value will be 0, for 2nd it's 1 and so on.
      //
      jump: i,
      // A position of the token this delimiter corresponds to.
      //
      token: state.tokens.length - 1,
      // If this delimiter is matched as a valid opener, `end` will be
      // equal to its position, otherwise it's `-1`.
      //
      end: -1,
      // Boolean flags that determine if this delimiter could open or close
      // an emphasis.
      //
      open: scanned.can_open,
      close: scanned.can_close
    });
  }

  state.pos += scanned.length;
  return true;
};

function postProcess(state, delimiters) {
  var i,
      startDelim,
      endDelim,
      token,
      ch,
      isStrong,
      max = delimiters.length;

  for (i = max - 1; i >= 0; i--) {
    startDelim = delimiters[i];

    if (startDelim.marker !== 0x5F
    /* _ */
    && startDelim.marker !== 0x2A
    /* * */
    ) {
        continue;
      } // Process only opening markers


    if (startDelim.end === -1) {
      continue;
    }

    endDelim = delimiters[startDelim.end]; // If the previous delimiter has the same marker and is adjacent to this one,
    // merge those into one strong delimiter.
    //
    // `<em><em>whatever</em></em>` -> `<strong>whatever</strong>`
    //

    isStrong = i > 0 && delimiters[i - 1].end === startDelim.end + 1 && delimiters[i - 1].token === startDelim.token - 1 && delimiters[startDelim.end + 1].token === endDelim.token + 1 && delimiters[i - 1].marker === startDelim.marker;
    ch = String.fromCharCode(startDelim.marker);
    token = state.tokens[startDelim.token];
    token.type = isStrong ? 'strong_open' : 'em_open';
    token.tag = isStrong ? 'strong' : 'em';
    token.nesting = 1;
    token.markup = isStrong ? ch + ch : ch;
    token.content = '';
    token = state.tokens[endDelim.token];
    token.type = isStrong ? 'strong_close' : 'em_close';
    token.tag = isStrong ? 'strong' : 'em';
    token.nesting = -1;
    token.markup = isStrong ? ch + ch : ch;
    token.content = '';

    if (isStrong) {
      state.tokens[delimiters[i - 1].token].content = '';
      state.tokens[delimiters[startDelim.end + 1].token].content = '';
      i--;
    }
  }
} // Walk through delimiter list and replace text tokens with tags
//


module.exports.postProcess = function emphasis(state) {
  var curr,
      tokens_meta = state.tokens_meta,
      max = state.tokens_meta.length;
  postProcess(state, state.delimiters);

  for (curr = 0; curr < max; curr++) {
    if (tokens_meta[curr] && tokens_meta[curr].delimiters) {
      postProcess(state, tokens_meta[curr].delimiters);
    }
  }
};

/***/ }),

/***/ "../../node_modules/markdown-it/lib/rules_inline/entity.js":
/*!*******************************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/markdown-it/lib/rules_inline/entity.js ***!
  \*******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
// Process html entity - &#123;, &#xAF;, &quot;, ...


var entities = __webpack_require__(/*! ../common/entities */ "../../node_modules/markdown-it/lib/common/entities.js");

var has = __webpack_require__(/*! ../common/utils */ "../../node_modules/markdown-it/lib/common/utils.js").has;

var isValidEntityCode = __webpack_require__(/*! ../common/utils */ "../../node_modules/markdown-it/lib/common/utils.js").isValidEntityCode;

var fromCodePoint = __webpack_require__(/*! ../common/utils */ "../../node_modules/markdown-it/lib/common/utils.js").fromCodePoint;

var DIGITAL_RE = /^&#((?:x[a-f0-9]{1,6}|[0-9]{1,7}));/i;
var NAMED_RE = /^&([a-z][a-z0-9]{1,31});/i;

module.exports = function entity(state, silent) {
  var ch,
      code,
      match,
      pos = state.pos,
      max = state.posMax;

  if (state.src.charCodeAt(pos) !== 0x26
  /* & */
  ) {
      return false;
    }

  if (pos + 1 < max) {
    ch = state.src.charCodeAt(pos + 1);

    if (ch === 0x23
    /* # */
    ) {
        match = state.src.slice(pos).match(DIGITAL_RE);

        if (match) {
          if (!silent) {
            code = match[1][0].toLowerCase() === 'x' ? parseInt(match[1].slice(1), 16) : parseInt(match[1], 10);
            state.pending += isValidEntityCode(code) ? fromCodePoint(code) : fromCodePoint(0xFFFD);
          }

          state.pos += match[0].length;
          return true;
        }
      } else {
      match = state.src.slice(pos).match(NAMED_RE);

      if (match) {
        if (has(entities, match[1])) {
          if (!silent) {
            state.pending += entities[match[1]];
          }

          state.pos += match[0].length;
          return true;
        }
      }
    }
  }

  if (!silent) {
    state.pending += '&';
  }

  state.pos++;
  return true;
};

/***/ }),

/***/ "../../node_modules/markdown-it/lib/rules_inline/escape.js":
/*!*******************************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/markdown-it/lib/rules_inline/escape.js ***!
  \*******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
// Process escaped chars and hardbreaks


var isSpace = __webpack_require__(/*! ../common/utils */ "../../node_modules/markdown-it/lib/common/utils.js").isSpace;

var ESCAPED = [];

for (var i = 0; i < 256; i++) {
  ESCAPED.push(0);
}

'\\!"#$%&\'()*+,./:;<=>?@[]^_`{|}~-'.split('').forEach(function (ch) {
  ESCAPED[ch.charCodeAt(0)] = 1;
});

module.exports = function escape(state, silent) {
  var ch,
      pos = state.pos,
      max = state.posMax;

  if (state.src.charCodeAt(pos) !== 0x5C
  /* \ */
  ) {
      return false;
    }

  pos++;

  if (pos < max) {
    ch = state.src.charCodeAt(pos);

    if (ch < 256 && ESCAPED[ch] !== 0) {
      if (!silent) {
        state.pending += state.src[pos];
      }

      state.pos += 2;
      return true;
    }

    if (ch === 0x0A) {
      if (!silent) {
        state.push('hardbreak', 'br', 0);
      }

      pos++; // skip leading whitespaces from next line

      while (pos < max) {
        ch = state.src.charCodeAt(pos);

        if (!isSpace(ch)) {
          break;
        }

        pos++;
      }

      state.pos = pos;
      return true;
    }
  }

  if (!silent) {
    state.pending += '\\';
  }

  state.pos++;
  return true;
};

/***/ }),

/***/ "../../node_modules/markdown-it/lib/rules_inline/html_inline.js":
/*!************************************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/markdown-it/lib/rules_inline/html_inline.js ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
// Process html tags


var HTML_TAG_RE = __webpack_require__(/*! ../common/html_re */ "../../node_modules/markdown-it/lib/common/html_re.js").HTML_TAG_RE;

function isLetter(ch) {
  /*eslint no-bitwise:0*/
  var lc = ch | 0x20; // to lower case

  return lc >= 0x61
  /* a */
  && lc <= 0x7a
  /* z */
  ;
}

module.exports = function html_inline(state, silent) {
  var ch,
      match,
      max,
      token,
      pos = state.pos;

  if (!state.md.options.html) {
    return false;
  } // Check start


  max = state.posMax;

  if (state.src.charCodeAt(pos) !== 0x3C
  /* < */
  || pos + 2 >= max) {
    return false;
  } // Quick fail on second char


  ch = state.src.charCodeAt(pos + 1);

  if (ch !== 0x21
  /* ! */
  && ch !== 0x3F
  /* ? */
  && ch !== 0x2F
  /* / */
  && !isLetter(ch)) {
    return false;
  }

  match = state.src.slice(pos).match(HTML_TAG_RE);

  if (!match) {
    return false;
  }

  if (!silent) {
    token = state.push('html_inline', '', 0);
    token.content = state.src.slice(pos, pos + match[0].length);
  }

  state.pos += match[0].length;
  return true;
};

/***/ }),

/***/ "../../node_modules/markdown-it/lib/rules_inline/image.js":
/*!******************************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/markdown-it/lib/rules_inline/image.js ***!
  \******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
// Process ![image](<src> "title")


var normalizeReference = __webpack_require__(/*! ../common/utils */ "../../node_modules/markdown-it/lib/common/utils.js").normalizeReference;

var isSpace = __webpack_require__(/*! ../common/utils */ "../../node_modules/markdown-it/lib/common/utils.js").isSpace;

module.exports = function image(state, silent) {
  var attrs,
      code,
      content,
      label,
      labelEnd,
      labelStart,
      pos,
      ref,
      res,
      title,
      token,
      tokens,
      start,
      href = '',
      oldPos = state.pos,
      max = state.posMax;

  if (state.src.charCodeAt(state.pos) !== 0x21
  /* ! */
  ) {
      return false;
    }

  if (state.src.charCodeAt(state.pos + 1) !== 0x5B
  /* [ */
  ) {
      return false;
    }

  labelStart = state.pos + 2;
  labelEnd = state.md.helpers.parseLinkLabel(state, state.pos + 1, false); // parser failed to find ']', so it's not a valid link

  if (labelEnd < 0) {
    return false;
  }

  pos = labelEnd + 1;

  if (pos < max && state.src.charCodeAt(pos) === 0x28
  /* ( */
  ) {
      //
      // Inline link
      //
      // [link](  <href>  "title"  )
      //        ^^ skipping these spaces
      pos++;

      for (; pos < max; pos++) {
        code = state.src.charCodeAt(pos);

        if (!isSpace(code) && code !== 0x0A) {
          break;
        }
      }

      if (pos >= max) {
        return false;
      } // [link](  <href>  "title"  )
      //          ^^^^^^ parsing link destination


      start = pos;
      res = state.md.helpers.parseLinkDestination(state.src, pos, state.posMax);

      if (res.ok) {
        href = state.md.normalizeLink(res.str);

        if (state.md.validateLink(href)) {
          pos = res.pos;
        } else {
          href = '';
        }
      } // [link](  <href>  "title"  )
      //                ^^ skipping these spaces


      start = pos;

      for (; pos < max; pos++) {
        code = state.src.charCodeAt(pos);

        if (!isSpace(code) && code !== 0x0A) {
          break;
        }
      } // [link](  <href>  "title"  )
      //                  ^^^^^^^ parsing link title


      res = state.md.helpers.parseLinkTitle(state.src, pos, state.posMax);

      if (pos < max && start !== pos && res.ok) {
        title = res.str;
        pos = res.pos; // [link](  <href>  "title"  )
        //                         ^^ skipping these spaces

        for (; pos < max; pos++) {
          code = state.src.charCodeAt(pos);

          if (!isSpace(code) && code !== 0x0A) {
            break;
          }
        }
      } else {
        title = '';
      }

      if (pos >= max || state.src.charCodeAt(pos) !== 0x29
      /* ) */
      ) {
          state.pos = oldPos;
          return false;
        }

      pos++;
    } else {
    //
    // Link reference
    //
    if (typeof state.env.references === 'undefined') {
      return false;
    }

    if (pos < max && state.src.charCodeAt(pos) === 0x5B
    /* [ */
    ) {
        start = pos + 1;
        pos = state.md.helpers.parseLinkLabel(state, pos);

        if (pos >= 0) {
          label = state.src.slice(start, pos++);
        } else {
          pos = labelEnd + 1;
        }
      } else {
      pos = labelEnd + 1;
    } // covers label === '' and label === undefined
    // (collapsed reference link and shortcut reference link respectively)


    if (!label) {
      label = state.src.slice(labelStart, labelEnd);
    }

    ref = state.env.references[normalizeReference(label)];

    if (!ref) {
      state.pos = oldPos;
      return false;
    }

    href = ref.href;
    title = ref.title;
  } //
  // We found the end of the link, and know for a fact it's a valid link;
  // so all that's left to do is to call tokenizer.
  //


  if (!silent) {
    content = state.src.slice(labelStart, labelEnd);
    state.md.inline.parse(content, state.md, state.env, tokens = []);
    token = state.push('image', 'img', 0);
    token.attrs = attrs = [['src', href], ['alt', '']];
    token.children = tokens;
    token.content = content;

    if (title) {
      attrs.push(['title', title]);
    }
  }

  state.pos = pos;
  state.posMax = max;
  return true;
};

/***/ }),

/***/ "../../node_modules/markdown-it/lib/rules_inline/link.js":
/*!*****************************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/markdown-it/lib/rules_inline/link.js ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
// Process [link](<to> "stuff")


var normalizeReference = __webpack_require__(/*! ../common/utils */ "../../node_modules/markdown-it/lib/common/utils.js").normalizeReference;

var isSpace = __webpack_require__(/*! ../common/utils */ "../../node_modules/markdown-it/lib/common/utils.js").isSpace;

module.exports = function link(state, silent) {
  var attrs,
      code,
      label,
      labelEnd,
      labelStart,
      pos,
      res,
      ref,
      title,
      token,
      href = '',
      oldPos = state.pos,
      max = state.posMax,
      start = state.pos,
      parseReference = true;

  if (state.src.charCodeAt(state.pos) !== 0x5B
  /* [ */
  ) {
      return false;
    }

  labelStart = state.pos + 1;
  labelEnd = state.md.helpers.parseLinkLabel(state, state.pos, true); // parser failed to find ']', so it's not a valid link

  if (labelEnd < 0) {
    return false;
  }

  pos = labelEnd + 1;

  if (pos < max && state.src.charCodeAt(pos) === 0x28
  /* ( */
  ) {
      //
      // Inline link
      //
      // might have found a valid shortcut link, disable reference parsing
      parseReference = false; // [link](  <href>  "title"  )
      //        ^^ skipping these spaces

      pos++;

      for (; pos < max; pos++) {
        code = state.src.charCodeAt(pos);

        if (!isSpace(code) && code !== 0x0A) {
          break;
        }
      }

      if (pos >= max) {
        return false;
      } // [link](  <href>  "title"  )
      //          ^^^^^^ parsing link destination


      start = pos;
      res = state.md.helpers.parseLinkDestination(state.src, pos, state.posMax);

      if (res.ok) {
        href = state.md.normalizeLink(res.str);

        if (state.md.validateLink(href)) {
          pos = res.pos;
        } else {
          href = '';
        }
      } // [link](  <href>  "title"  )
      //                ^^ skipping these spaces


      start = pos;

      for (; pos < max; pos++) {
        code = state.src.charCodeAt(pos);

        if (!isSpace(code) && code !== 0x0A) {
          break;
        }
      } // [link](  <href>  "title"  )
      //                  ^^^^^^^ parsing link title


      res = state.md.helpers.parseLinkTitle(state.src, pos, state.posMax);

      if (pos < max && start !== pos && res.ok) {
        title = res.str;
        pos = res.pos; // [link](  <href>  "title"  )
        //                         ^^ skipping these spaces

        for (; pos < max; pos++) {
          code = state.src.charCodeAt(pos);

          if (!isSpace(code) && code !== 0x0A) {
            break;
          }
        }
      } else {
        title = '';
      }

      if (pos >= max || state.src.charCodeAt(pos) !== 0x29
      /* ) */
      ) {
          // parsing a valid shortcut link failed, fallback to reference
          parseReference = true;
        }

      pos++;
    }

  if (parseReference) {
    //
    // Link reference
    //
    if (typeof state.env.references === 'undefined') {
      return false;
    }

    if (pos < max && state.src.charCodeAt(pos) === 0x5B
    /* [ */
    ) {
        start = pos + 1;
        pos = state.md.helpers.parseLinkLabel(state, pos);

        if (pos >= 0) {
          label = state.src.slice(start, pos++);
        } else {
          pos = labelEnd + 1;
        }
      } else {
      pos = labelEnd + 1;
    } // covers label === '' and label === undefined
    // (collapsed reference link and shortcut reference link respectively)


    if (!label) {
      label = state.src.slice(labelStart, labelEnd);
    }

    ref = state.env.references[normalizeReference(label)];

    if (!ref) {
      state.pos = oldPos;
      return false;
    }

    href = ref.href;
    title = ref.title;
  } //
  // We found the end of the link, and know for a fact it's a valid link;
  // so all that's left to do is to call tokenizer.
  //


  if (!silent) {
    state.pos = labelStart;
    state.posMax = labelEnd;
    token = state.push('link_open', 'a', 1);
    token.attrs = attrs = [['href', href]];

    if (title) {
      attrs.push(['title', title]);
    }

    state.md.inline.tokenize(state);
    token = state.push('link_close', 'a', -1);
  }

  state.pos = pos;
  state.posMax = max;
  return true;
};

/***/ }),

/***/ "../../node_modules/markdown-it/lib/rules_inline/newline.js":
/*!********************************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/markdown-it/lib/rules_inline/newline.js ***!
  \********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
// Proceess '\n'


var isSpace = __webpack_require__(/*! ../common/utils */ "../../node_modules/markdown-it/lib/common/utils.js").isSpace;

module.exports = function newline(state, silent) {
  var pmax,
      max,
      pos = state.pos;

  if (state.src.charCodeAt(pos) !== 0x0A
  /* \n */
  ) {
      return false;
    }

  pmax = state.pending.length - 1;
  max = state.posMax; // '  \n' -> hardbreak
  // Lookup in pending chars is bad practice! Don't copy to other rules!
  // Pending string is stored in concat mode, indexed lookups will cause
  // convertion to flat mode.

  if (!silent) {
    if (pmax >= 0 && state.pending.charCodeAt(pmax) === 0x20) {
      if (pmax >= 1 && state.pending.charCodeAt(pmax - 1) === 0x20) {
        state.pending = state.pending.replace(/ +$/, '');
        state.push('hardbreak', 'br', 0);
      } else {
        state.pending = state.pending.slice(0, -1);
        state.push('softbreak', 'br', 0);
      }
    } else {
      state.push('softbreak', 'br', 0);
    }
  }

  pos++; // skip heading spaces for next line

  while (pos < max && isSpace(state.src.charCodeAt(pos))) {
    pos++;
  }

  state.pos = pos;
  return true;
};

/***/ }),

/***/ "../../node_modules/markdown-it/lib/rules_inline/state_inline.js":
/*!*************************************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/markdown-it/lib/rules_inline/state_inline.js ***!
  \*************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
// Inline parser state


var Token = __webpack_require__(/*! ../token */ "../../node_modules/markdown-it/lib/token.js");

var isWhiteSpace = __webpack_require__(/*! ../common/utils */ "../../node_modules/markdown-it/lib/common/utils.js").isWhiteSpace;

var isPunctChar = __webpack_require__(/*! ../common/utils */ "../../node_modules/markdown-it/lib/common/utils.js").isPunctChar;

var isMdAsciiPunct = __webpack_require__(/*! ../common/utils */ "../../node_modules/markdown-it/lib/common/utils.js").isMdAsciiPunct;

function StateInline(src, md, env, outTokens) {
  this.src = src;
  this.env = env;
  this.md = md;
  this.tokens = outTokens;
  this.tokens_meta = Array(outTokens.length);
  this.pos = 0;
  this.posMax = this.src.length;
  this.level = 0;
  this.pending = '';
  this.pendingLevel = 0; // Stores { start: end } pairs. Useful for backtrack
  // optimization of pairs parse (emphasis, strikes).

  this.cache = {}; // List of emphasis-like delimiters for current tag

  this.delimiters = []; // Stack of delimiter lists for upper level tags

  this._prev_delimiters = [];
} // Flush pending text
//


StateInline.prototype.pushPending = function () {
  var token = new Token('text', '', 0);
  token.content = this.pending;
  token.level = this.pendingLevel;
  this.tokens.push(token);
  this.pending = '';
  return token;
}; // Push new token to "stream".
// If pending text exists - flush it as text token
//


StateInline.prototype.push = function (type, tag, nesting) {
  if (this.pending) {
    this.pushPending();
  }

  var token = new Token(type, tag, nesting);
  var token_meta = null;

  if (nesting < 0) {
    // closing tag
    this.level--;
    this.delimiters = this._prev_delimiters.pop();
  }

  token.level = this.level;

  if (nesting > 0) {
    // opening tag
    this.level++;

    this._prev_delimiters.push(this.delimiters);

    this.delimiters = [];
    token_meta = {
      delimiters: this.delimiters
    };
  }

  this.pendingLevel = this.level;
  this.tokens.push(token);
  this.tokens_meta.push(token_meta);
  return token;
}; // Scan a sequence of emphasis-like markers, and determine whether
// it can start an emphasis sequence or end an emphasis sequence.
//
//  - start - position to scan from (it should point at a valid marker);
//  - canSplitWord - determine if these markers can be found inside a word
//


StateInline.prototype.scanDelims = function (start, canSplitWord) {
  var pos = start,
      lastChar,
      nextChar,
      count,
      can_open,
      can_close,
      isLastWhiteSpace,
      isLastPunctChar,
      isNextWhiteSpace,
      isNextPunctChar,
      left_flanking = true,
      right_flanking = true,
      max = this.posMax,
      marker = this.src.charCodeAt(start); // treat beginning of the line as a whitespace

  lastChar = start > 0 ? this.src.charCodeAt(start - 1) : 0x20;

  while (pos < max && this.src.charCodeAt(pos) === marker) {
    pos++;
  }

  count = pos - start; // treat end of the line as a whitespace

  nextChar = pos < max ? this.src.charCodeAt(pos) : 0x20;
  isLastPunctChar = isMdAsciiPunct(lastChar) || isPunctChar(String.fromCharCode(lastChar));
  isNextPunctChar = isMdAsciiPunct(nextChar) || isPunctChar(String.fromCharCode(nextChar));
  isLastWhiteSpace = isWhiteSpace(lastChar);
  isNextWhiteSpace = isWhiteSpace(nextChar);

  if (isNextWhiteSpace) {
    left_flanking = false;
  } else if (isNextPunctChar) {
    if (!(isLastWhiteSpace || isLastPunctChar)) {
      left_flanking = false;
    }
  }

  if (isLastWhiteSpace) {
    right_flanking = false;
  } else if (isLastPunctChar) {
    if (!(isNextWhiteSpace || isNextPunctChar)) {
      right_flanking = false;
    }
  }

  if (!canSplitWord) {
    can_open = left_flanking && (!right_flanking || isLastPunctChar);
    can_close = right_flanking && (!left_flanking || isNextPunctChar);
  } else {
    can_open = left_flanking;
    can_close = right_flanking;
  }

  return {
    can_open: can_open,
    can_close: can_close,
    length: count
  };
}; // re-export Token class to use in block rules


StateInline.prototype.Token = Token;
module.exports = StateInline;

/***/ }),

/***/ "../../node_modules/markdown-it/lib/rules_inline/strikethrough.js":
/*!**************************************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/markdown-it/lib/rules_inline/strikethrough.js ***!
  \**************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
// ~~strike through~~
//
 // Insert each marker as a separate text token, and add it to delimiter list
//

module.exports.tokenize = function strikethrough(state, silent) {
  var i,
      scanned,
      token,
      len,
      ch,
      start = state.pos,
      marker = state.src.charCodeAt(start);

  if (silent) {
    return false;
  }

  if (marker !== 0x7E
  /* ~ */
  ) {
      return false;
    }

  scanned = state.scanDelims(state.pos, true);
  len = scanned.length;
  ch = String.fromCharCode(marker);

  if (len < 2) {
    return false;
  }

  if (len % 2) {
    token = state.push('text', '', 0);
    token.content = ch;
    len--;
  }

  for (i = 0; i < len; i += 2) {
    token = state.push('text', '', 0);
    token.content = ch + ch;
    state.delimiters.push({
      marker: marker,
      length: 0,
      // disable "rule of 3" length checks meant for emphasis
      jump: i,
      token: state.tokens.length - 1,
      end: -1,
      open: scanned.can_open,
      close: scanned.can_close
    });
  }

  state.pos += scanned.length;
  return true;
};

function postProcess(state, delimiters) {
  var i,
      j,
      startDelim,
      endDelim,
      token,
      loneMarkers = [],
      max = delimiters.length;

  for (i = 0; i < max; i++) {
    startDelim = delimiters[i];

    if (startDelim.marker !== 0x7E
    /* ~ */
    ) {
        continue;
      }

    if (startDelim.end === -1) {
      continue;
    }

    endDelim = delimiters[startDelim.end];
    token = state.tokens[startDelim.token];
    token.type = 's_open';
    token.tag = 's';
    token.nesting = 1;
    token.markup = '~~';
    token.content = '';
    token = state.tokens[endDelim.token];
    token.type = 's_close';
    token.tag = 's';
    token.nesting = -1;
    token.markup = '~~';
    token.content = '';

    if (state.tokens[endDelim.token - 1].type === 'text' && state.tokens[endDelim.token - 1].content === '~') {
      loneMarkers.push(endDelim.token - 1);
    }
  } // If a marker sequence has an odd number of characters, it's splitted
  // like this: `~~~~~` -> `~` + `~~` + `~~`, leaving one marker at the
  // start of the sequence.
  //
  // So, we have to move all those markers after subsequent s_close tags.
  //


  while (loneMarkers.length) {
    i = loneMarkers.pop();
    j = i + 1;

    while (j < state.tokens.length && state.tokens[j].type === 's_close') {
      j++;
    }

    j--;

    if (i !== j) {
      token = state.tokens[j];
      state.tokens[j] = state.tokens[i];
      state.tokens[i] = token;
    }
  }
} // Walk through delimiter list and replace text tokens with tags
//


module.exports.postProcess = function strikethrough(state) {
  var curr,
      tokens_meta = state.tokens_meta,
      max = state.tokens_meta.length;
  postProcess(state, state.delimiters);

  for (curr = 0; curr < max; curr++) {
    if (tokens_meta[curr] && tokens_meta[curr].delimiters) {
      postProcess(state, tokens_meta[curr].delimiters);
    }
  }
};

/***/ }),

/***/ "../../node_modules/markdown-it/lib/rules_inline/text.js":
/*!*****************************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/markdown-it/lib/rules_inline/text.js ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
// Skip text characters for text token, place those to pending buffer
// and increment current pos
 // Rule to skip pure text
// '{}$%@~+=:' reserved for extentions
// !, ", #, $, %, &, ', (, ), *, +, ,, -, ., /, :, ;, <, =, >, ?, @, [, \, ], ^, _, `, {, |, }, or ~
// !!!! Don't confuse with "Markdown ASCII Punctuation" chars
// http://spec.commonmark.org/0.15/#ascii-punctuation-character

function isTerminatorChar(ch) {
  switch (ch) {
    case 0x0A
    /* \n */
    :
    case 0x21
    /* ! */
    :
    case 0x23
    /* # */
    :
    case 0x24
    /* $ */
    :
    case 0x25
    /* % */
    :
    case 0x26
    /* & */
    :
    case 0x2A
    /* * */
    :
    case 0x2B
    /* + */
    :
    case 0x2D
    /* - */
    :
    case 0x3A
    /* : */
    :
    case 0x3C
    /* < */
    :
    case 0x3D
    /* = */
    :
    case 0x3E
    /* > */
    :
    case 0x40
    /* @ */
    :
    case 0x5B
    /* [ */
    :
    case 0x5C
    /* \ */
    :
    case 0x5D
    /* ] */
    :
    case 0x5E
    /* ^ */
    :
    case 0x5F
    /* _ */
    :
    case 0x60
    /* ` */
    :
    case 0x7B
    /* { */
    :
    case 0x7D
    /* } */
    :
    case 0x7E
    /* ~ */
    :
      return true;

    default:
      return false;
  }
}

module.exports = function text(state, silent) {
  var pos = state.pos;

  while (pos < state.posMax && !isTerminatorChar(state.src.charCodeAt(pos))) {
    pos++;
  }

  if (pos === state.pos) {
    return false;
  }

  if (!silent) {
    state.pending += state.src.slice(state.pos, pos);
  }

  state.pos = pos;
  return true;
}; // Alternative implementation, for memory.
//
// It costs 10% of performance, but allows extend terminators list, if place it
// to `ParcerInline` property. Probably, will switch to it sometime, such
// flexibility required.

/*
var TERMINATOR_RE = /[\n!#$%&*+\-:<=>@[\\\]^_`{}~]/;

module.exports = function text(state, silent) {
  var pos = state.pos,
      idx = state.src.slice(pos).search(TERMINATOR_RE);

  // first char is terminator -> empty text
  if (idx === 0) { return false; }

  // no terminator -> text till end of string
  if (idx < 0) {
    if (!silent) { state.pending += state.src.slice(pos); }
    state.pos = state.src.length;
    return true;
  }

  if (!silent) { state.pending += state.src.slice(pos, pos + idx); }

  state.pos += idx;

  return true;
};*/

/***/ }),

/***/ "../../node_modules/markdown-it/lib/rules_inline/text_collapse.js":
/*!**************************************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/markdown-it/lib/rules_inline/text_collapse.js ***!
  \**************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
// Clean up tokens after emphasis and strikethrough postprocessing:
// merge adjacent text nodes into one and re-calculate all token levels
//
// This is necessary because initially emphasis delimiter markers (*, _, ~)
// are treated as their own separate text tokens. Then emphasis rule either
// leaves them as text (needed to merge with adjacent text) or turns them
// into opening/closing tags (which messes up levels inside).
//


module.exports = function text_collapse(state) {
  var curr,
      last,
      level = 0,
      tokens = state.tokens,
      max = state.tokens.length;

  for (curr = last = 0; curr < max; curr++) {
    // re-calculate levels after emphasis/strikethrough turns some text nodes
    // into opening/closing tags
    if (tokens[curr].nesting < 0) level--; // closing tag

    tokens[curr].level = level;
    if (tokens[curr].nesting > 0) level++; // opening tag

    if (tokens[curr].type === 'text' && curr + 1 < max && tokens[curr + 1].type === 'text') {
      // collapse two adjacent text nodes
      tokens[curr + 1].content = tokens[curr].content + tokens[curr + 1].content;
    } else {
      if (curr !== last) {
        tokens[last] = tokens[curr];
      }

      last++;
    }
  }

  if (curr !== last) {
    tokens.length = last;
  }
};

/***/ }),

/***/ "../../node_modules/markdown-it/lib/token.js":
/*!*****************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/markdown-it/lib/token.js ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
// Token class

/**
 * class Token
 **/

/**
 * new Token(type, tag, nesting)
 *
 * Create new token and fill passed properties.
 **/

function Token(type, tag, nesting) {
  /**
   * Token#type -> String
   *
   * Type of the token (string, e.g. "paragraph_open")
   **/
  this.type = type;
  /**
   * Token#tag -> String
   *
   * html tag name, e.g. "p"
   **/

  this.tag = tag;
  /**
   * Token#attrs -> Array
   *
   * Html attributes. Format: `[ [ name1, value1 ], [ name2, value2 ] ]`
   **/

  this.attrs = null;
  /**
   * Token#map -> Array
   *
   * Source map info. Format: `[ line_begin, line_end ]`
   **/

  this.map = null;
  /**
   * Token#nesting -> Number
   *
   * Level change (number in {-1, 0, 1} set), where:
   *
   * -  `1` means the tag is opening
   * -  `0` means the tag is self-closing
   * - `-1` means the tag is closing
   **/

  this.nesting = nesting;
  /**
   * Token#level -> Number
   *
   * nesting level, the same as `state.level`
   **/

  this.level = 0;
  /**
   * Token#children -> Array
   *
   * An array of child nodes (inline and img tokens)
   **/

  this.children = null;
  /**
   * Token#content -> String
   *
   * In a case of self-closing tag (code, html, fence, etc.),
   * it has contents of this tag.
   **/

  this.content = '';
  /**
   * Token#markup -> String
   *
   * '*' or '_' for emphasis, fence string for fence, etc.
   **/

  this.markup = '';
  /**
   * Token#info -> String
   *
   * fence infostring
   **/

  this.info = '';
  /**
   * Token#meta -> Object
   *
   * A place for plugins to store an arbitrary data
   **/

  this.meta = null;
  /**
   * Token#block -> Boolean
   *
   * True for block-level tokens, false for inline tokens.
   * Used in renderer to calculate line breaks
   **/

  this.block = false;
  /**
   * Token#hidden -> Boolean
   *
   * If it's true, ignore this element when rendering. Used for tight lists
   * to hide paragraphs.
   **/

  this.hidden = false;
}
/**
 * Token.attrIndex(name) -> Number
 *
 * Search attribute index by name.
 **/


Token.prototype.attrIndex = function attrIndex(name) {
  var attrs, i, len;

  if (!this.attrs) {
    return -1;
  }

  attrs = this.attrs;

  for (i = 0, len = attrs.length; i < len; i++) {
    if (attrs[i][0] === name) {
      return i;
    }
  }

  return -1;
};
/**
 * Token.attrPush(attrData)
 *
 * Add `[ name, value ]` attribute to list. Init attrs if necessary
 **/


Token.prototype.attrPush = function attrPush(attrData) {
  if (this.attrs) {
    this.attrs.push(attrData);
  } else {
    this.attrs = [attrData];
  }
};
/**
 * Token.attrSet(name, value)
 *
 * Set `name` attribute to `value`. Override old value if exists.
 **/


Token.prototype.attrSet = function attrSet(name, value) {
  var idx = this.attrIndex(name),
      attrData = [name, value];

  if (idx < 0) {
    this.attrPush(attrData);
  } else {
    this.attrs[idx] = attrData;
  }
};
/**
 * Token.attrGet(name)
 *
 * Get the value of attribute `name`, or null if it does not exist.
 **/


Token.prototype.attrGet = function attrGet(name) {
  var idx = this.attrIndex(name),
      value = null;

  if (idx >= 0) {
    value = this.attrs[idx][1];
  }

  return value;
};
/**
 * Token.attrJoin(name, value)
 *
 * Join value to existing attribute via space. Or create new attribute if not
 * exists. Useful to operate with token classes.
 **/


Token.prototype.attrJoin = function attrJoin(name, value) {
  var idx = this.attrIndex(name);

  if (idx < 0) {
    this.attrPush([name, value]);
  } else {
    this.attrs[idx][1] = this.attrs[idx][1] + ' ' + value;
  }
};

module.exports = Token;

/***/ }),

/***/ "../../node_modules/mdurl/decode.js":
/*!********************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/mdurl/decode.js ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* eslint-disable no-bitwise */

var decodeCache = {};

function getDecodeCache(exclude) {
  var i,
      ch,
      cache = decodeCache[exclude];

  if (cache) {
    return cache;
  }

  cache = decodeCache[exclude] = [];

  for (i = 0; i < 128; i++) {
    ch = String.fromCharCode(i);
    cache.push(ch);
  }

  for (i = 0; i < exclude.length; i++) {
    ch = exclude.charCodeAt(i);
    cache[ch] = '%' + ('0' + ch.toString(16).toUpperCase()).slice(-2);
  }

  return cache;
} // Decode percent-encoded string.
//


function decode(string, exclude) {
  var cache;

  if (typeof exclude !== 'string') {
    exclude = decode.defaultChars;
  }

  cache = getDecodeCache(exclude);
  return string.replace(/(%[a-f0-9]{2})+/gi, function (seq) {
    var i,
        l,
        b1,
        b2,
        b3,
        b4,
        chr,
        result = '';

    for (i = 0, l = seq.length; i < l; i += 3) {
      b1 = parseInt(seq.slice(i + 1, i + 3), 16);

      if (b1 < 0x80) {
        result += cache[b1];
        continue;
      }

      if ((b1 & 0xE0) === 0xC0 && i + 3 < l) {
        // 110xxxxx 10xxxxxx
        b2 = parseInt(seq.slice(i + 4, i + 6), 16);

        if ((b2 & 0xC0) === 0x80) {
          chr = b1 << 6 & 0x7C0 | b2 & 0x3F;

          if (chr < 0x80) {
            result += "\uFFFD\uFFFD";
          } else {
            result += String.fromCharCode(chr);
          }

          i += 3;
          continue;
        }
      }

      if ((b1 & 0xF0) === 0xE0 && i + 6 < l) {
        // 1110xxxx 10xxxxxx 10xxxxxx
        b2 = parseInt(seq.slice(i + 4, i + 6), 16);
        b3 = parseInt(seq.slice(i + 7, i + 9), 16);

        if ((b2 & 0xC0) === 0x80 && (b3 & 0xC0) === 0x80) {
          chr = b1 << 12 & 0xF000 | b2 << 6 & 0xFC0 | b3 & 0x3F;

          if (chr < 0x800 || chr >= 0xD800 && chr <= 0xDFFF) {
            result += "\uFFFD\uFFFD\uFFFD";
          } else {
            result += String.fromCharCode(chr);
          }

          i += 6;
          continue;
        }
      }

      if ((b1 & 0xF8) === 0xF0 && i + 9 < l) {
        // 111110xx 10xxxxxx 10xxxxxx 10xxxxxx
        b2 = parseInt(seq.slice(i + 4, i + 6), 16);
        b3 = parseInt(seq.slice(i + 7, i + 9), 16);
        b4 = parseInt(seq.slice(i + 10, i + 12), 16);

        if ((b2 & 0xC0) === 0x80 && (b3 & 0xC0) === 0x80 && (b4 & 0xC0) === 0x80) {
          chr = b1 << 18 & 0x1C0000 | b2 << 12 & 0x3F000 | b3 << 6 & 0xFC0 | b4 & 0x3F;

          if (chr < 0x10000 || chr > 0x10FFFF) {
            result += "\uFFFD\uFFFD\uFFFD\uFFFD";
          } else {
            chr -= 0x10000;
            result += String.fromCharCode(0xD800 + (chr >> 10), 0xDC00 + (chr & 0x3FF));
          }

          i += 9;
          continue;
        }
      }

      result += "\uFFFD";
    }

    return result;
  });
}

decode.defaultChars = ';/?:@&=+$,#';
decode.componentChars = '';
module.exports = decode;

/***/ }),

/***/ "../../node_modules/mdurl/encode.js":
/*!********************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/mdurl/encode.js ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var encodeCache = {}; // Create a lookup array where anything but characters in `chars` string
// and alphanumeric chars is percent-encoded.
//

function getEncodeCache(exclude) {
  var i,
      ch,
      cache = encodeCache[exclude];

  if (cache) {
    return cache;
  }

  cache = encodeCache[exclude] = [];

  for (i = 0; i < 128; i++) {
    ch = String.fromCharCode(i);

    if (/^[0-9a-z]$/i.test(ch)) {
      // always allow unencoded alphanumeric characters
      cache.push(ch);
    } else {
      cache.push('%' + ('0' + i.toString(16).toUpperCase()).slice(-2));
    }
  }

  for (i = 0; i < exclude.length; i++) {
    cache[exclude.charCodeAt(i)] = exclude[i];
  }

  return cache;
} // Encode unsafe characters with percent-encoding, skipping already
// encoded sequences.
//
//  - string       - string to encode
//  - exclude      - list of characters to ignore (in addition to a-zA-Z0-9)
//  - keepEscaped  - don't encode '%' in a correct escape sequence (default: true)
//


function encode(string, exclude, keepEscaped) {
  var i,
      l,
      code,
      nextCode,
      cache,
      result = '';

  if (typeof exclude !== 'string') {
    // encode(string, keepEscaped)
    keepEscaped = exclude;
    exclude = encode.defaultChars;
  }

  if (typeof keepEscaped === 'undefined') {
    keepEscaped = true;
  }

  cache = getEncodeCache(exclude);

  for (i = 0, l = string.length; i < l; i++) {
    code = string.charCodeAt(i);

    if (keepEscaped && code === 0x25
    /* % */
    && i + 2 < l) {
      if (/^[0-9a-f]{2}$/i.test(string.slice(i + 1, i + 3))) {
        result += string.slice(i, i + 3);
        i += 2;
        continue;
      }
    }

    if (code < 128) {
      result += cache[code];
      continue;
    }

    if (code >= 0xD800 && code <= 0xDFFF) {
      if (code >= 0xD800 && code <= 0xDBFF && i + 1 < l) {
        nextCode = string.charCodeAt(i + 1);

        if (nextCode >= 0xDC00 && nextCode <= 0xDFFF) {
          result += encodeURIComponent(string[i] + string[i + 1]);
          i++;
          continue;
        }
      }

      result += '%EF%BF%BD';
      continue;
    }

    result += encodeURIComponent(string[i]);
  }

  return result;
}

encode.defaultChars = ";/?:@&=+$,-_.!~*'()#";
encode.componentChars = "-_.!~*'()";
module.exports = encode;

/***/ }),

/***/ "../../node_modules/mdurl/format.js":
/*!********************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/mdurl/format.js ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = function format(url) {
  var result = '';
  result += url.protocol || '';
  result += url.slashes ? '//' : '';
  result += url.auth ? url.auth + '@' : '';

  if (url.hostname && url.hostname.indexOf(':') !== -1) {
    // ipv6 address
    result += '[' + url.hostname + ']';
  } else {
    result += url.hostname || '';
  }

  result += url.port ? ':' + url.port : '';
  result += url.pathname || '';
  result += url.search || '';
  result += url.hash || '';
  return result;
};

/***/ }),

/***/ "../../node_modules/mdurl/index.js":
/*!*******************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/mdurl/index.js ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports.encode = __webpack_require__(/*! ./encode */ "../../node_modules/mdurl/encode.js");
module.exports.decode = __webpack_require__(/*! ./decode */ "../../node_modules/mdurl/decode.js");
module.exports.format = __webpack_require__(/*! ./format */ "../../node_modules/mdurl/format.js");
module.exports.parse = __webpack_require__(/*! ./parse */ "../../node_modules/mdurl/parse.js");

/***/ }),

/***/ "../../node_modules/mdurl/parse.js":
/*!*******************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/mdurl/parse.js ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
// Copyright Joyent, Inc. and other Node contributors.
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the
// following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
// USE OR OTHER DEALINGS IN THE SOFTWARE.
 //
// Changes from joyent/node:
//
// 1. No leading slash in paths,
//    e.g. in `url.parse('http://foo?bar')` pathname is ``, not `/`
//
// 2. Backslashes are not replaced with slashes,
//    so `http:\\example.org\` is treated like a relative path
//
// 3. Trailing colon is treated like a part of the path,
//    i.e. in `http://example.org:foo` pathname is `:foo`
//
// 4. Nothing is URL-encoded in the resulting object,
//    (in joyent/node some chars in auth and paths are encoded)
//
// 5. `url.parse()` does not have `parseQueryString` argument
//
// 6. Removed extraneous result properties: `host`, `path`, `query`, etc.,
//    which can be constructed using other parts of the url.
//

function Url() {
  this.protocol = null;
  this.slashes = null;
  this.auth = null;
  this.port = null;
  this.hostname = null;
  this.hash = null;
  this.search = null;
  this.pathname = null;
} // Reference: RFC 3986, RFC 1808, RFC 2396
// define these here so at least they only have to be
// compiled once on the first module load.


var protocolPattern = /^([a-z0-9.+-]+:)/i,
    portPattern = /:[0-9]*$/,
    // Special case for a simple path URL
simplePathPattern = /^(\/\/?(?!\/)[^\?\s]*)(\?[^\s]*)?$/,
    // RFC 2396: characters reserved for delimiting URLs.
// We actually just auto-escape these.
delims = ['<', '>', '"', '`', ' ', '\r', '\n', '\t'],
    // RFC 2396: characters not allowed for various reasons.
unwise = ['{', '}', '|', '\\', '^', '`'].concat(delims),
    // Allowed by RFCs, but cause of XSS attacks.  Always escape these.
autoEscape = ['\''].concat(unwise),
    // Characters that are never ever allowed in a hostname.
// Note that any invalid chars are also handled, but these
// are the ones that are *expected* to be seen, so we fast-path
// them.
nonHostChars = ['%', '/', '?', ';', '#'].concat(autoEscape),
    hostEndingChars = ['/', '?', '#'],
    hostnameMaxLen = 255,
    hostnamePartPattern = /^[+a-z0-9A-Z_-]{0,63}$/,
    hostnamePartStart = /^([+a-z0-9A-Z_-]{0,63})(.*)$/,
    // protocols that can allow "unsafe" and "unwise" chars.

/* eslint-disable no-script-url */
// protocols that never have a hostname.
hostlessProtocol = {
  'javascript': true,
  'javascript:': true
},
    // protocols that always contain a // bit.
slashedProtocol = {
  'http': true,
  'https': true,
  'ftp': true,
  'gopher': true,
  'file': true,
  'http:': true,
  'https:': true,
  'ftp:': true,
  'gopher:': true,
  'file:': true
};
/* eslint-enable no-script-url */

function urlParse(url, slashesDenoteHost) {
  if (url && url instanceof Url) {
    return url;
  }

  var u = new Url();
  u.parse(url, slashesDenoteHost);
  return u;
}

Url.prototype.parse = function (url, slashesDenoteHost) {
  var i,
      l,
      lowerProto,
      hec,
      slashes,
      rest = url; // trim before proceeding.
  // This is to support parse stuff like "  http://foo.com  \n"

  rest = rest.trim();

  if (!slashesDenoteHost && url.split('#').length === 1) {
    // Try fast path regexp
    var simplePath = simplePathPattern.exec(rest);

    if (simplePath) {
      this.pathname = simplePath[1];

      if (simplePath[2]) {
        this.search = simplePath[2];
      }

      return this;
    }
  }

  var proto = protocolPattern.exec(rest);

  if (proto) {
    proto = proto[0];
    lowerProto = proto.toLowerCase();
    this.protocol = proto;
    rest = rest.substr(proto.length);
  } // figure out if it's got a host
  // user@server is *always* interpreted as a hostname, and url
  // resolution will treat //foo/bar as host=foo,path=bar because that's
  // how the browser resolves relative URLs.


  if (slashesDenoteHost || proto || rest.match(/^\/\/[^@\/]+@[^@\/]+/)) {
    slashes = rest.substr(0, 2) === '//';

    if (slashes && !(proto && hostlessProtocol[proto])) {
      rest = rest.substr(2);
      this.slashes = true;
    }
  }

  if (!hostlessProtocol[proto] && (slashes || proto && !slashedProtocol[proto])) {
    // there's a hostname.
    // the first instance of /, ?, ;, or # ends the host.
    //
    // If there is an @ in the hostname, then non-host chars *are* allowed
    // to the left of the last @ sign, unless some host-ending character
    // comes *before* the @-sign.
    // URLs are obnoxious.
    //
    // ex:
    // http://a@b@c/ => user:a@b host:c
    // http://a@b?@c => user:a host:c path:/?@c
    // v0.12 TODO(isaacs): This is not quite how Chrome does things.
    // Review our test case against browsers more comprehensively.
    // find the first instance of any hostEndingChars
    var hostEnd = -1;

    for (i = 0; i < hostEndingChars.length; i++) {
      hec = rest.indexOf(hostEndingChars[i]);

      if (hec !== -1 && (hostEnd === -1 || hec < hostEnd)) {
        hostEnd = hec;
      }
    } // at this point, either we have an explicit point where the
    // auth portion cannot go past, or the last @ char is the decider.


    var auth, atSign;

    if (hostEnd === -1) {
      // atSign can be anywhere.
      atSign = rest.lastIndexOf('@');
    } else {
      // atSign must be in auth portion.
      // http://a@b/c@d => host:b auth:a path:/c@d
      atSign = rest.lastIndexOf('@', hostEnd);
    } // Now we have a portion which is definitely the auth.
    // Pull that off.


    if (atSign !== -1) {
      auth = rest.slice(0, atSign);
      rest = rest.slice(atSign + 1);
      this.auth = auth;
    } // the host is the remaining to the left of the first non-host char


    hostEnd = -1;

    for (i = 0; i < nonHostChars.length; i++) {
      hec = rest.indexOf(nonHostChars[i]);

      if (hec !== -1 && (hostEnd === -1 || hec < hostEnd)) {
        hostEnd = hec;
      }
    } // if we still have not hit it, then the entire thing is a host.


    if (hostEnd === -1) {
      hostEnd = rest.length;
    }

    if (rest[hostEnd - 1] === ':') {
      hostEnd--;
    }

    var host = rest.slice(0, hostEnd);
    rest = rest.slice(hostEnd); // pull out port.

    this.parseHost(host); // we've indicated that there is a hostname,
    // so even if it's empty, it has to be present.

    this.hostname = this.hostname || ''; // if hostname begins with [ and ends with ]
    // assume that it's an IPv6 address.

    var ipv6Hostname = this.hostname[0] === '[' && this.hostname[this.hostname.length - 1] === ']'; // validate a little.

    if (!ipv6Hostname) {
      var hostparts = this.hostname.split(/\./);

      for (i = 0, l = hostparts.length; i < l; i++) {
        var part = hostparts[i];

        if (!part) {
          continue;
        }

        if (!part.match(hostnamePartPattern)) {
          var newpart = '';

          for (var j = 0, k = part.length; j < k; j++) {
            if (part.charCodeAt(j) > 127) {
              // we replace non-ASCII char with a temporary placeholder
              // we need this to make sure size of hostname is not
              // broken by replacing non-ASCII by nothing
              newpart += 'x';
            } else {
              newpart += part[j];
            }
          } // we test again with ASCII char only


          if (!newpart.match(hostnamePartPattern)) {
            var validParts = hostparts.slice(0, i);
            var notHost = hostparts.slice(i + 1);
            var bit = part.match(hostnamePartStart);

            if (bit) {
              validParts.push(bit[1]);
              notHost.unshift(bit[2]);
            }

            if (notHost.length) {
              rest = notHost.join('.') + rest;
            }

            this.hostname = validParts.join('.');
            break;
          }
        }
      }
    }

    if (this.hostname.length > hostnameMaxLen) {
      this.hostname = '';
    } // strip [ and ] from the hostname
    // the host field still retains them, though


    if (ipv6Hostname) {
      this.hostname = this.hostname.substr(1, this.hostname.length - 2);
    }
  } // chop off from the tail first.


  var hash = rest.indexOf('#');

  if (hash !== -1) {
    // got a fragment string.
    this.hash = rest.substr(hash);
    rest = rest.slice(0, hash);
  }

  var qm = rest.indexOf('?');

  if (qm !== -1) {
    this.search = rest.substr(qm);
    rest = rest.slice(0, qm);
  }

  if (rest) {
    this.pathname = rest;
  }

  if (slashedProtocol[lowerProto] && this.hostname && !this.pathname) {
    this.pathname = '';
  }

  return this;
};

Url.prototype.parseHost = function (host) {
  var port = portPattern.exec(host);

  if (port) {
    port = port[0];

    if (port !== ':') {
      this.port = port.substr(1);
    }

    host = host.substr(0, host.length - port.length);
  }

  if (host) {
    this.hostname = host;
  }
};

module.exports = urlParse;

/***/ }),

/***/ "../../node_modules/node-libs-browser/node_modules/punycode/punycode.js":
/*!********************************************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/node-libs-browser/node_modules/punycode/punycode.js ***!
  \********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(module, global) {var __WEBPACK_AMD_DEFINE_RESULT__;var _typeof = __webpack_require__(/*! @babel/runtime/helpers/typeof */ "../../node_modules/@babel/runtime/helpers/typeof.js");

/*! https://mths.be/punycode v1.4.1 by @mathias */
;

(function (root) {
  /** Detect free variables */
  var freeExports = ( false ? undefined : _typeof(exports)) == 'object' && exports && !exports.nodeType && exports;
  var freeModule = ( false ? undefined : _typeof(module)) == 'object' && module && !module.nodeType && module;
  var freeGlobal = (typeof global === "undefined" ? "undefined" : _typeof(global)) == 'object' && global;

  if (freeGlobal.global === freeGlobal || freeGlobal.window === freeGlobal || freeGlobal.self === freeGlobal) {
    root = freeGlobal;
  }
  /**
   * The `punycode` object.
   * @name punycode
   * @type Object
   */


  var punycode,

  /** Highest positive signed 32-bit float value */
  maxInt = 2147483647,
      // aka. 0x7FFFFFFF or 2^31-1

  /** Bootstring parameters */
  base = 36,
      tMin = 1,
      tMax = 26,
      skew = 38,
      damp = 700,
      initialBias = 72,
      initialN = 128,
      // 0x80
  delimiter = '-',
      // '\x2D'

  /** Regular expressions */
  regexPunycode = /^xn--/,
      regexNonASCII = /[^\x20-\x7E]/,
      // unprintable ASCII chars + non-ASCII chars
  regexSeparators = /[\x2E\u3002\uFF0E\uFF61]/g,
      // RFC 3490 separators

  /** Error messages */
  errors = {
    'overflow': 'Overflow: input needs wider integers to process',
    'not-basic': 'Illegal input >= 0x80 (not a basic code point)',
    'invalid-input': 'Invalid input'
  },

  /** Convenience shortcuts */
  baseMinusTMin = base - tMin,
      floor = Math.floor,
      stringFromCharCode = String.fromCharCode,

  /** Temporary variable */
  key;
  /*--------------------------------------------------------------------------*/

  /**
   * A generic error utility function.
   * @private
   * @param {String} type The error type.
   * @returns {Error} Throws a `RangeError` with the applicable error message.
   */

  function error(type) {
    throw new RangeError(errors[type]);
  }
  /**
   * A generic `Array#map` utility function.
   * @private
   * @param {Array} array The array to iterate over.
   * @param {Function} callback The function that gets called for every array
   * item.
   * @returns {Array} A new array of values returned by the callback function.
   */


  function map(array, fn) {
    var length = array.length;
    var result = [];

    while (length--) {
      result[length] = fn(array[length]);
    }

    return result;
  }
  /**
   * A simple `Array#map`-like wrapper to work with domain name strings or email
   * addresses.
   * @private
   * @param {String} domain The domain name or email address.
   * @param {Function} callback The function that gets called for every
   * character.
   * @returns {Array} A new string of characters returned by the callback
   * function.
   */


  function mapDomain(string, fn) {
    var parts = string.split('@');
    var result = '';

    if (parts.length > 1) {
      // In email addresses, only the domain name should be punycoded. Leave
      // the local part (i.e. everything up to `@`) intact.
      result = parts[0] + '@';
      string = parts[1];
    } // Avoid `split(regex)` for IE8 compatibility. See #17.


    string = string.replace(regexSeparators, '\x2E');
    var labels = string.split('.');
    var encoded = map(labels, fn).join('.');
    return result + encoded;
  }
  /**
   * Creates an array containing the numeric code points of each Unicode
   * character in the string. While JavaScript uses UCS-2 internally,
   * this function will convert a pair of surrogate halves (each of which
   * UCS-2 exposes as separate characters) into a single code point,
   * matching UTF-16.
   * @see `punycode.ucs2.encode`
   * @see <https://mathiasbynens.be/notes/javascript-encoding>
   * @memberOf punycode.ucs2
   * @name decode
   * @param {String} string The Unicode input string (UCS-2).
   * @returns {Array} The new array of code points.
   */


  function ucs2decode(string) {
    var output = [],
        counter = 0,
        length = string.length,
        value,
        extra;

    while (counter < length) {
      value = string.charCodeAt(counter++);

      if (value >= 0xD800 && value <= 0xDBFF && counter < length) {
        // high surrogate, and there is a next character
        extra = string.charCodeAt(counter++);

        if ((extra & 0xFC00) == 0xDC00) {
          // low surrogate
          output.push(((value & 0x3FF) << 10) + (extra & 0x3FF) + 0x10000);
        } else {
          // unmatched surrogate; only append this code unit, in case the next
          // code unit is the high surrogate of a surrogate pair
          output.push(value);
          counter--;
        }
      } else {
        output.push(value);
      }
    }

    return output;
  }
  /**
   * Creates a string based on an array of numeric code points.
   * @see `punycode.ucs2.decode`
   * @memberOf punycode.ucs2
   * @name encode
   * @param {Array} codePoints The array of numeric code points.
   * @returns {String} The new Unicode string (UCS-2).
   */


  function ucs2encode(array) {
    return map(array, function (value) {
      var output = '';

      if (value > 0xFFFF) {
        value -= 0x10000;
        output += stringFromCharCode(value >>> 10 & 0x3FF | 0xD800);
        value = 0xDC00 | value & 0x3FF;
      }

      output += stringFromCharCode(value);
      return output;
    }).join('');
  }
  /**
   * Converts a basic code point into a digit/integer.
   * @see `digitToBasic()`
   * @private
   * @param {Number} codePoint The basic numeric code point value.
   * @returns {Number} The numeric value of a basic code point (for use in
   * representing integers) in the range `0` to `base - 1`, or `base` if
   * the code point does not represent a value.
   */


  function basicToDigit(codePoint) {
    if (codePoint - 48 < 10) {
      return codePoint - 22;
    }

    if (codePoint - 65 < 26) {
      return codePoint - 65;
    }

    if (codePoint - 97 < 26) {
      return codePoint - 97;
    }

    return base;
  }
  /**
   * Converts a digit/integer into a basic code point.
   * @see `basicToDigit()`
   * @private
   * @param {Number} digit The numeric value of a basic code point.
   * @returns {Number} The basic code point whose value (when used for
   * representing integers) is `digit`, which needs to be in the range
   * `0` to `base - 1`. If `flag` is non-zero, the uppercase form is
   * used; else, the lowercase form is used. The behavior is undefined
   * if `flag` is non-zero and `digit` has no uppercase form.
   */


  function digitToBasic(digit, flag) {
    //  0..25 map to ASCII a..z or A..Z
    // 26..35 map to ASCII 0..9
    return digit + 22 + 75 * (digit < 26) - ((flag != 0) << 5);
  }
  /**
   * Bias adaptation function as per section 3.4 of RFC 3492.
   * https://tools.ietf.org/html/rfc3492#section-3.4
   * @private
   */


  function adapt(delta, numPoints, firstTime) {
    var k = 0;
    delta = firstTime ? floor(delta / damp) : delta >> 1;
    delta += floor(delta / numPoints);

    for (;
    /* no initialization */
    delta > baseMinusTMin * tMax >> 1; k += base) {
      delta = floor(delta / baseMinusTMin);
    }

    return floor(k + (baseMinusTMin + 1) * delta / (delta + skew));
  }
  /**
   * Converts a Punycode string of ASCII-only symbols to a string of Unicode
   * symbols.
   * @memberOf punycode
   * @param {String} input The Punycode string of ASCII-only symbols.
   * @returns {String} The resulting string of Unicode symbols.
   */


  function decode(input) {
    // Don't use UCS-2
    var output = [],
        inputLength = input.length,
        out,
        i = 0,
        n = initialN,
        bias = initialBias,
        basic,
        j,
        index,
        oldi,
        w,
        k,
        digit,
        t,

    /** Cached calculation results */
    baseMinusT; // Handle the basic code points: let `basic` be the number of input code
    // points before the last delimiter, or `0` if there is none, then copy
    // the first basic code points to the output.

    basic = input.lastIndexOf(delimiter);

    if (basic < 0) {
      basic = 0;
    }

    for (j = 0; j < basic; ++j) {
      // if it's not a basic code point
      if (input.charCodeAt(j) >= 0x80) {
        error('not-basic');
      }

      output.push(input.charCodeAt(j));
    } // Main decoding loop: start just after the last delimiter if any basic code
    // points were copied; start at the beginning otherwise.


    for (index = basic > 0 ? basic + 1 : 0; index < inputLength;)
    /* no final expression */
    {
      // `index` is the index of the next character to be consumed.
      // Decode a generalized variable-length integer into `delta`,
      // which gets added to `i`. The overflow checking is easier
      // if we increase `i` as we go, then subtract off its starting
      // value at the end to obtain `delta`.
      for (oldi = i, w = 1, k = base;;
      /* no condition */
      k += base) {
        if (index >= inputLength) {
          error('invalid-input');
        }

        digit = basicToDigit(input.charCodeAt(index++));

        if (digit >= base || digit > floor((maxInt - i) / w)) {
          error('overflow');
        }

        i += digit * w;
        t = k <= bias ? tMin : k >= bias + tMax ? tMax : k - bias;

        if (digit < t) {
          break;
        }

        baseMinusT = base - t;

        if (w > floor(maxInt / baseMinusT)) {
          error('overflow');
        }

        w *= baseMinusT;
      }

      out = output.length + 1;
      bias = adapt(i - oldi, out, oldi == 0); // `i` was supposed to wrap around from `out` to `0`,
      // incrementing `n` each time, so we'll fix that now:

      if (floor(i / out) > maxInt - n) {
        error('overflow');
      }

      n += floor(i / out);
      i %= out; // Insert `n` at position `i` of the output

      output.splice(i++, 0, n);
    }

    return ucs2encode(output);
  }
  /**
   * Converts a string of Unicode symbols (e.g. a domain name label) to a
   * Punycode string of ASCII-only symbols.
   * @memberOf punycode
   * @param {String} input The string of Unicode symbols.
   * @returns {String} The resulting Punycode string of ASCII-only symbols.
   */


  function encode(input) {
    var n,
        delta,
        handledCPCount,
        basicLength,
        bias,
        j,
        m,
        q,
        k,
        t,
        currentValue,
        output = [],

    /** `inputLength` will hold the number of code points in `input`. */
    inputLength,

    /** Cached calculation results */
    handledCPCountPlusOne,
        baseMinusT,
        qMinusT; // Convert the input in UCS-2 to Unicode

    input = ucs2decode(input); // Cache the length

    inputLength = input.length; // Initialize the state

    n = initialN;
    delta = 0;
    bias = initialBias; // Handle the basic code points

    for (j = 0; j < inputLength; ++j) {
      currentValue = input[j];

      if (currentValue < 0x80) {
        output.push(stringFromCharCode(currentValue));
      }
    }

    handledCPCount = basicLength = output.length; // `handledCPCount` is the number of code points that have been handled;
    // `basicLength` is the number of basic code points.
    // Finish the basic string - if it is not empty - with a delimiter

    if (basicLength) {
      output.push(delimiter);
    } // Main encoding loop:


    while (handledCPCount < inputLength) {
      // All non-basic code points < n have been handled already. Find the next
      // larger one:
      for (m = maxInt, j = 0; j < inputLength; ++j) {
        currentValue = input[j];

        if (currentValue >= n && currentValue < m) {
          m = currentValue;
        }
      } // Increase `delta` enough to advance the decoder's <n,i> state to <m,0>,
      // but guard against overflow


      handledCPCountPlusOne = handledCPCount + 1;

      if (m - n > floor((maxInt - delta) / handledCPCountPlusOne)) {
        error('overflow');
      }

      delta += (m - n) * handledCPCountPlusOne;
      n = m;

      for (j = 0; j < inputLength; ++j) {
        currentValue = input[j];

        if (currentValue < n && ++delta > maxInt) {
          error('overflow');
        }

        if (currentValue == n) {
          // Represent delta as a generalized variable-length integer
          for (q = delta, k = base;;
          /* no condition */
          k += base) {
            t = k <= bias ? tMin : k >= bias + tMax ? tMax : k - bias;

            if (q < t) {
              break;
            }

            qMinusT = q - t;
            baseMinusT = base - t;
            output.push(stringFromCharCode(digitToBasic(t + qMinusT % baseMinusT, 0)));
            q = floor(qMinusT / baseMinusT);
          }

          output.push(stringFromCharCode(digitToBasic(q, 0)));
          bias = adapt(delta, handledCPCountPlusOne, handledCPCount == basicLength);
          delta = 0;
          ++handledCPCount;
        }
      }

      ++delta;
      ++n;
    }

    return output.join('');
  }
  /**
   * Converts a Punycode string representing a domain name or an email address
   * to Unicode. Only the Punycoded parts of the input will be converted, i.e.
   * it doesn't matter if you call it on a string that has already been
   * converted to Unicode.
   * @memberOf punycode
   * @param {String} input The Punycoded domain name or email address to
   * convert to Unicode.
   * @returns {String} The Unicode representation of the given Punycode
   * string.
   */


  function toUnicode(input) {
    return mapDomain(input, function (string) {
      return regexPunycode.test(string) ? decode(string.slice(4).toLowerCase()) : string;
    });
  }
  /**
   * Converts a Unicode string representing a domain name or an email address to
   * Punycode. Only the non-ASCII parts of the domain name will be converted,
   * i.e. it doesn't matter if you call it with a domain that's already in
   * ASCII.
   * @memberOf punycode
   * @param {String} input The domain name or email address to convert, as a
   * Unicode string.
   * @returns {String} The Punycode representation of the given domain name or
   * email address.
   */


  function toASCII(input) {
    return mapDomain(input, function (string) {
      return regexNonASCII.test(string) ? 'xn--' + encode(string) : string;
    });
  }
  /*--------------------------------------------------------------------------*/

  /** Define the public API */


  punycode = {
    /**
     * A string representing the current Punycode.js version number.
     * @memberOf punycode
     * @type String
     */
    'version': '1.4.1',

    /**
     * An object of methods to convert from JavaScript's internal character
     * representation (UCS-2) to Unicode code points, and back.
     * @see <https://mathiasbynens.be/notes/javascript-encoding>
     * @memberOf punycode
     * @type Object
     */
    'ucs2': {
      'decode': ucs2decode,
      'encode': ucs2encode
    },
    'decode': decode,
    'encode': encode,
    'toASCII': toASCII,
    'toUnicode': toUnicode
  };
  /** Expose `punycode` */
  // Some AMD build optimizers, like r.js, check for specific condition patterns
  // like the following:

  if ( true && _typeof(__webpack_require__(/*! !webpack amd options */ "../../node_modules/webpack/buildin/amd-options.js")) == 'object' && __webpack_require__(/*! !webpack amd options */ "../../node_modules/webpack/buildin/amd-options.js")) {
    !(__WEBPACK_AMD_DEFINE_RESULT__ = (function () {
      return punycode;
    }).call(exports, __webpack_require__, exports, module),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
  } else if (freeExports && freeModule) {
    if (module.exports == freeExports) {
      // in Node.js, io.js, or RingoJS v0.8.0+
      freeModule.exports = punycode;
    } else {
      // in Narwhal or RingoJS v0.7.0-
      for (key in punycode) {
        punycode.hasOwnProperty(key) && (freeExports[key] = punycode[key]);
      }
    }
  } else {
    // in Rhino or a web browser
    root.punycode = punycode;
  }
})(this);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../webpack/buildin/module.js */ "../../node_modules/webpack/buildin/module.js")(module), __webpack_require__(/*! ./../../../webpack/buildin/global.js */ "../../node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "../../node_modules/uc.micro/categories/Cc/regex.js":
/*!************************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/uc.micro/categories/Cc/regex.js ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = /[\0-\x1F\x7F-\x9F]/;

/***/ }),

/***/ "../../node_modules/uc.micro/categories/Cf/regex.js":
/*!************************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/uc.micro/categories/Cf/regex.js ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = /[\xAD\u0600-\u0605\u061C\u06DD\u070F\u08E2\u180E\u200B-\u200F\u202A-\u202E\u2060-\u2064\u2066-\u206F\uFEFF\uFFF9-\uFFFB]|\uD804[\uDCBD\uDCCD]|\uD82F[\uDCA0-\uDCA3]|\uD834[\uDD73-\uDD7A]|\uDB40[\uDC01\uDC20-\uDC7F]/;

/***/ }),

/***/ "../../node_modules/uc.micro/categories/P/regex.js":
/*!***********************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/uc.micro/categories/P/regex.js ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = /[!-#%-\*,-\/:;\?@\[-\]_\{\}\xA1\xA7\xAB\xB6\xB7\xBB\xBF\u037E\u0387\u055A-\u055F\u0589\u058A\u05BE\u05C0\u05C3\u05C6\u05F3\u05F4\u0609\u060A\u060C\u060D\u061B\u061E\u061F\u066A-\u066D\u06D4\u0700-\u070D\u07F7-\u07F9\u0830-\u083E\u085E\u0964\u0965\u0970\u09FD\u0A76\u0AF0\u0C84\u0DF4\u0E4F\u0E5A\u0E5B\u0F04-\u0F12\u0F14\u0F3A-\u0F3D\u0F85\u0FD0-\u0FD4\u0FD9\u0FDA\u104A-\u104F\u10FB\u1360-\u1368\u1400\u166D\u166E\u169B\u169C\u16EB-\u16ED\u1735\u1736\u17D4-\u17D6\u17D8-\u17DA\u1800-\u180A\u1944\u1945\u1A1E\u1A1F\u1AA0-\u1AA6\u1AA8-\u1AAD\u1B5A-\u1B60\u1BFC-\u1BFF\u1C3B-\u1C3F\u1C7E\u1C7F\u1CC0-\u1CC7\u1CD3\u2010-\u2027\u2030-\u2043\u2045-\u2051\u2053-\u205E\u207D\u207E\u208D\u208E\u2308-\u230B\u2329\u232A\u2768-\u2775\u27C5\u27C6\u27E6-\u27EF\u2983-\u2998\u29D8-\u29DB\u29FC\u29FD\u2CF9-\u2CFC\u2CFE\u2CFF\u2D70\u2E00-\u2E2E\u2E30-\u2E4E\u3001-\u3003\u3008-\u3011\u3014-\u301F\u3030\u303D\u30A0\u30FB\uA4FE\uA4FF\uA60D-\uA60F\uA673\uA67E\uA6F2-\uA6F7\uA874-\uA877\uA8CE\uA8CF\uA8F8-\uA8FA\uA8FC\uA92E\uA92F\uA95F\uA9C1-\uA9CD\uA9DE\uA9DF\uAA5C-\uAA5F\uAADE\uAADF\uAAF0\uAAF1\uABEB\uFD3E\uFD3F\uFE10-\uFE19\uFE30-\uFE52\uFE54-\uFE61\uFE63\uFE68\uFE6A\uFE6B\uFF01-\uFF03\uFF05-\uFF0A\uFF0C-\uFF0F\uFF1A\uFF1B\uFF1F\uFF20\uFF3B-\uFF3D\uFF3F\uFF5B\uFF5D\uFF5F-\uFF65]|\uD800[\uDD00-\uDD02\uDF9F\uDFD0]|\uD801\uDD6F|\uD802[\uDC57\uDD1F\uDD3F\uDE50-\uDE58\uDE7F\uDEF0-\uDEF6\uDF39-\uDF3F\uDF99-\uDF9C]|\uD803[\uDF55-\uDF59]|\uD804[\uDC47-\uDC4D\uDCBB\uDCBC\uDCBE-\uDCC1\uDD40-\uDD43\uDD74\uDD75\uDDC5-\uDDC8\uDDCD\uDDDB\uDDDD-\uDDDF\uDE38-\uDE3D\uDEA9]|\uD805[\uDC4B-\uDC4F\uDC5B\uDC5D\uDCC6\uDDC1-\uDDD7\uDE41-\uDE43\uDE60-\uDE6C\uDF3C-\uDF3E]|\uD806[\uDC3B\uDE3F-\uDE46\uDE9A-\uDE9C\uDE9E-\uDEA2]|\uD807[\uDC41-\uDC45\uDC70\uDC71\uDEF7\uDEF8]|\uD809[\uDC70-\uDC74]|\uD81A[\uDE6E\uDE6F\uDEF5\uDF37-\uDF3B\uDF44]|\uD81B[\uDE97-\uDE9A]|\uD82F\uDC9F|\uD836[\uDE87-\uDE8B]|\uD83A[\uDD5E\uDD5F]/;

/***/ }),

/***/ "../../node_modules/uc.micro/categories/Z/regex.js":
/*!***********************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/uc.micro/categories/Z/regex.js ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = /[ \xA0\u1680\u2000-\u200A\u2028\u2029\u202F\u205F\u3000]/;

/***/ }),

/***/ "../../node_modules/uc.micro/index.js":
/*!**********************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/uc.micro/index.js ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.Any = __webpack_require__(/*! ./properties/Any/regex */ "../../node_modules/uc.micro/properties/Any/regex.js");
exports.Cc = __webpack_require__(/*! ./categories/Cc/regex */ "../../node_modules/uc.micro/categories/Cc/regex.js");
exports.Cf = __webpack_require__(/*! ./categories/Cf/regex */ "../../node_modules/uc.micro/categories/Cf/regex.js");
exports.P = __webpack_require__(/*! ./categories/P/regex */ "../../node_modules/uc.micro/categories/P/regex.js");
exports.Z = __webpack_require__(/*! ./categories/Z/regex */ "../../node_modules/uc.micro/categories/Z/regex.js");

/***/ }),

/***/ "../../node_modules/uc.micro/properties/Any/regex.js":
/*!*************************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/uc.micro/properties/Any/regex.js ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = /[\0-\uD7FF\uE000-\uFFFF]|[\uD800-\uDBFF][\uDC00-\uDFFF]|[\uD800-\uDBFF](?![\uDC00-\uDFFF])|(?:[^\uD800-\uDBFF]|^)[\uDC00-\uDFFF]/;

/***/ }),

/***/ "../../node_modules/vue-loader/lib/runtime/componentNormalizer.js":
/*!**************************************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/vue-loader/lib/runtime/componentNormalizer.js ***!
  \**************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return normalizeComponent; });
/* globals __VUE_SSR_CONTEXT__ */

// IMPORTANT: Do NOT use ES2015 features in this file (except for modules).
// This module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle.

function normalizeComponent (
  scriptExports,
  render,
  staticRenderFns,
  functionalTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier, /* server only */
  shadowMode /* vue-cli only */
) {
  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (render) {
    options.render = render
    options.staticRenderFns = staticRenderFns
    options._compiled = true
  }

  // functional template
  if (functionalTemplate) {
    options.functional = true
  }

  // scopedId
  if (scopeId) {
    options._scopeId = 'data-v-' + scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = shadowMode
      ? function () {
        injectStyles.call(
          this,
          (options.functional ? this.parent : this).$root.$options.shadowRoot
        )
      }
      : injectStyles
  }

  if (hook) {
    if (options.functional) {
      // for template-only hot-reload because in that case the render fn doesn't
      // go through the normalizer
      options._injectStyles = hook
      // register for functional component in vue file
      var originalRender = options.render
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return originalRender(h, context)
      }
    } else {
      // inject component registration as beforeCreate hook
      var existing = options.beforeCreate
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    }
  }

  return {
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ "../../node_modules/webpack/buildin/amd-options.js":
/*!****************************************!*\
  !*** (webpack)/buildin/amd-options.js ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/* WEBPACK VAR INJECTION */(function(__webpack_amd_options__) {/* globals __webpack_amd_options__ */
module.exports = __webpack_amd_options__;

/* WEBPACK VAR INJECTION */}.call(this, {}))

/***/ }),

/***/ "../../node_modules/webpack/buildin/global.js":
/*!***********************************!*\
  !*** (webpack)/buildin/global.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var _typeof = __webpack_require__(/*! @babel/runtime/helpers/typeof */ "../../node_modules/@babel/runtime/helpers/typeof.js");

var g; // This works in non-strict mode

g = function () {
  return this;
}();

try {
  // This works if eval is allowed (see CSP)
  g = g || new Function("return this")();
} catch (e) {
  // This works if the window reference is available
  if ((typeof window === "undefined" ? "undefined" : _typeof(window)) === "object") g = window;
} // g can still be undefined, but nothing to do about it...
// We return undefined, instead of nothing here, so it's
// easier to handle this case. if(!global) { ...}


module.exports = g;

/***/ }),

/***/ "../../node_modules/webpack/buildin/module.js":
/*!***********************************!*\
  !*** (webpack)/buildin/module.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = function (module) {
  if (!module.webpackPolyfill) {
    module.deprecate = function () {};

    module.paths = []; // module.parent = undefined by default

    if (!module.children) module.children = [];
    Object.defineProperty(module, "loaded", {
      enumerable: true,
      get: function get() {
        return module.l;
      }
    });
    Object.defineProperty(module, "id", {
      enumerable: true,
      get: function get() {
        return module.i;
      }
    });
    module.webpackPolyfill = 1;
  }

  return module;
};

/***/ }),

/***/ "./src/components/MarkdownBlock.vue":
/*!******************************************!*\
  !*** ./src/components/MarkdownBlock.vue ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _MarkdownBlock_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./MarkdownBlock.vue?vue&type=script&lang=js& */ "./src/components/MarkdownBlock.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "../../node_modules/vue-loader/lib/runtime/componentNormalizer.js");
var render, staticRenderFns




/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__["default"])(
  _MarkdownBlock_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"],
  render,
  staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "src/components/MarkdownBlock.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./src/components/MarkdownBlock.vue?vue&type=script&lang=js&":
/*!*******************************************************************!*\
  !*** ./src/components/MarkdownBlock.vue?vue&type=script&lang=js& ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_loader_lib_index_js_vue_loader_options_MarkdownBlock_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib!../../../../node_modules/vue-loader/lib??vue-loader-options!./MarkdownBlock.vue?vue&type=script&lang=js& */ "../../node_modules/babel-loader/lib/index.js!../../node_modules/vue-loader/lib/index.js?!./src/components/MarkdownBlock.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_node_modules_vue_loader_lib_index_js_vue_loader_options_MarkdownBlock_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "vue":
/*!**********************!*\
  !*** external "vue" ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_vue__;

/***/ })

/******/ });
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly92dWUtbGliL3dlYnBhY2svdW5pdmVyc2FsTW9kdWxlRGVmaW5pdGlvbiIsIndlYnBhY2s6Ly92dWUtbGliL3dlYnBhY2svYm9vdHN0cmFwIiwid2VicGFjazovL3Z1ZS1saWIvRDovVXNlcnMvZGFuaS9EZXNrdG9wL0Btb25vZ3JpZC9ub2RlX21vZHVsZXMvQGJhYmVsL3J1bnRpbWUvaGVscGVycy9hcnJheUxpa2VUb0FycmF5LmpzIiwid2VicGFjazovL3Z1ZS1saWIvRDovVXNlcnMvZGFuaS9EZXNrdG9wL0Btb25vZ3JpZC9ub2RlX21vZHVsZXMvQGJhYmVsL3J1bnRpbWUvaGVscGVycy9hcnJheVdpdGhIb2xlcy5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL0BiYWJlbC9ydW50aW1lL2hlbHBlcnMvaXRlcmFibGVUb0FycmF5TGltaXQuanMiLCJ3ZWJwYWNrOi8vdnVlLWxpYi9EOi9Vc2Vycy9kYW5pL0Rlc2t0b3AvQG1vbm9ncmlkL25vZGVfbW9kdWxlcy9AYmFiZWwvcnVudGltZS9oZWxwZXJzL25vbkl0ZXJhYmxlUmVzdC5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL0BiYWJlbC9ydW50aW1lL2hlbHBlcnMvc2xpY2VkVG9BcnJheS5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL0BiYWJlbC9ydW50aW1lL2hlbHBlcnMvdHlwZW9mLmpzIiwid2VicGFjazovL3Z1ZS1saWIvRDovVXNlcnMvZGFuaS9EZXNrdG9wL0Btb25vZ3JpZC9ub2RlX21vZHVsZXMvQGJhYmVsL3J1bnRpbWUvaGVscGVycy91bnN1cHBvcnRlZEl0ZXJhYmxlVG9BcnJheS5qcyIsIndlYnBhY2s6Ly92dWUtbGliL3NyYy9jb21wb25lbnRzL01hcmtkb3duQmxvY2sudnVlIiwid2VicGFjazovL3Z1ZS1saWIvRDovVXNlcnMvZGFuaS9EZXNrdG9wL0Btb25vZ3JpZC9ub2RlX21vZHVsZXMvbGlua2lmeS1pdC9pbmRleC5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL2xpbmtpZnktaXQvbGliL3JlLmpzIiwid2VicGFjazovL3Z1ZS1saWIvRDovVXNlcnMvZGFuaS9EZXNrdG9wL0Btb25vZ3JpZC9ub2RlX21vZHVsZXMvbWFya2Rvd24taXQtY29udGFpbmVyL2luZGV4LmpzIiwid2VicGFjazovL3Z1ZS1saWIvRDovVXNlcnMvZGFuaS9EZXNrdG9wL0Btb25vZ3JpZC9ub2RlX21vZHVsZXMvbWFya2Rvd24taXQtY3VzdG9tLWJsb2NrL2luZGV4LmpzIiwid2VicGFjazovL3Z1ZS1saWIvRDovVXNlcnMvZGFuaS9EZXNrdG9wL0Btb25vZ3JpZC9ub2RlX21vZHVsZXMvbWFya2Rvd24taXQtbGluay1hdHRyaWJ1dGVzL2luZGV4LmpzIiwid2VicGFjazovL3Z1ZS1saWIvRDovVXNlcnMvZGFuaS9EZXNrdG9wL0Btb25vZ3JpZC9ub2RlX21vZHVsZXMvbWFya2Rvd24taXQvaW5kZXguanMiLCJ3ZWJwYWNrOi8vdnVlLWxpYi9EOi9Vc2Vycy9kYW5pL0Rlc2t0b3AvQG1vbm9ncmlkL25vZGVfbW9kdWxlcy9tYXJrZG93bi1pdC9saWIvY29tbW9uL2VudGl0aWVzLmpzIiwid2VicGFjazovL3Z1ZS1saWIvRDovVXNlcnMvZGFuaS9EZXNrdG9wL0Btb25vZ3JpZC9ub2RlX21vZHVsZXMvbWFya2Rvd24taXQvbGliL2NvbW1vbi9odG1sX2Jsb2Nrcy5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL21hcmtkb3duLWl0L2xpYi9jb21tb24vaHRtbF9yZS5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL21hcmtkb3duLWl0L2xpYi9jb21tb24vdXRpbHMuanMiLCJ3ZWJwYWNrOi8vdnVlLWxpYi9EOi9Vc2Vycy9kYW5pL0Rlc2t0b3AvQG1vbm9ncmlkL25vZGVfbW9kdWxlcy9tYXJrZG93bi1pdC9saWIvaGVscGVycy9pbmRleC5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL21hcmtkb3duLWl0L2xpYi9oZWxwZXJzL3BhcnNlX2xpbmtfZGVzdGluYXRpb24uanMiLCJ3ZWJwYWNrOi8vdnVlLWxpYi9EOi9Vc2Vycy9kYW5pL0Rlc2t0b3AvQG1vbm9ncmlkL25vZGVfbW9kdWxlcy9tYXJrZG93bi1pdC9saWIvaGVscGVycy9wYXJzZV9saW5rX2xhYmVsLmpzIiwid2VicGFjazovL3Z1ZS1saWIvRDovVXNlcnMvZGFuaS9EZXNrdG9wL0Btb25vZ3JpZC9ub2RlX21vZHVsZXMvbWFya2Rvd24taXQvbGliL2hlbHBlcnMvcGFyc2VfbGlua190aXRsZS5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL21hcmtkb3duLWl0L2xpYi9pbmRleC5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL21hcmtkb3duLWl0L2xpYi9wYXJzZXJfYmxvY2suanMiLCJ3ZWJwYWNrOi8vdnVlLWxpYi9EOi9Vc2Vycy9kYW5pL0Rlc2t0b3AvQG1vbm9ncmlkL25vZGVfbW9kdWxlcy9tYXJrZG93bi1pdC9saWIvcGFyc2VyX2NvcmUuanMiLCJ3ZWJwYWNrOi8vdnVlLWxpYi9EOi9Vc2Vycy9kYW5pL0Rlc2t0b3AvQG1vbm9ncmlkL25vZGVfbW9kdWxlcy9tYXJrZG93bi1pdC9saWIvcGFyc2VyX2lubGluZS5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL21hcmtkb3duLWl0L2xpYi9wcmVzZXRzL2NvbW1vbm1hcmsuanMiLCJ3ZWJwYWNrOi8vdnVlLWxpYi9EOi9Vc2Vycy9kYW5pL0Rlc2t0b3AvQG1vbm9ncmlkL25vZGVfbW9kdWxlcy9tYXJrZG93bi1pdC9saWIvcHJlc2V0cy9kZWZhdWx0LmpzIiwid2VicGFjazovL3Z1ZS1saWIvRDovVXNlcnMvZGFuaS9EZXNrdG9wL0Btb25vZ3JpZC9ub2RlX21vZHVsZXMvbWFya2Rvd24taXQvbGliL3ByZXNldHMvemVyby5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL21hcmtkb3duLWl0L2xpYi9yZW5kZXJlci5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL21hcmtkb3duLWl0L2xpYi9ydWxlci5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL21hcmtkb3duLWl0L2xpYi9ydWxlc19ibG9jay9ibG9ja3F1b3RlLmpzIiwid2VicGFjazovL3Z1ZS1saWIvRDovVXNlcnMvZGFuaS9EZXNrdG9wL0Btb25vZ3JpZC9ub2RlX21vZHVsZXMvbWFya2Rvd24taXQvbGliL3J1bGVzX2Jsb2NrL2NvZGUuanMiLCJ3ZWJwYWNrOi8vdnVlLWxpYi9EOi9Vc2Vycy9kYW5pL0Rlc2t0b3AvQG1vbm9ncmlkL25vZGVfbW9kdWxlcy9tYXJrZG93bi1pdC9saWIvcnVsZXNfYmxvY2svZmVuY2UuanMiLCJ3ZWJwYWNrOi8vdnVlLWxpYi9EOi9Vc2Vycy9kYW5pL0Rlc2t0b3AvQG1vbm9ncmlkL25vZGVfbW9kdWxlcy9tYXJrZG93bi1pdC9saWIvcnVsZXNfYmxvY2svaGVhZGluZy5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL21hcmtkb3duLWl0L2xpYi9ydWxlc19ibG9jay9oci5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL21hcmtkb3duLWl0L2xpYi9ydWxlc19ibG9jay9odG1sX2Jsb2NrLmpzIiwid2VicGFjazovL3Z1ZS1saWIvRDovVXNlcnMvZGFuaS9EZXNrdG9wL0Btb25vZ3JpZC9ub2RlX21vZHVsZXMvbWFya2Rvd24taXQvbGliL3J1bGVzX2Jsb2NrL2xoZWFkaW5nLmpzIiwid2VicGFjazovL3Z1ZS1saWIvRDovVXNlcnMvZGFuaS9EZXNrdG9wL0Btb25vZ3JpZC9ub2RlX21vZHVsZXMvbWFya2Rvd24taXQvbGliL3J1bGVzX2Jsb2NrL2xpc3QuanMiLCJ3ZWJwYWNrOi8vdnVlLWxpYi9EOi9Vc2Vycy9kYW5pL0Rlc2t0b3AvQG1vbm9ncmlkL25vZGVfbW9kdWxlcy9tYXJrZG93bi1pdC9saWIvcnVsZXNfYmxvY2svcGFyYWdyYXBoLmpzIiwid2VicGFjazovL3Z1ZS1saWIvRDovVXNlcnMvZGFuaS9EZXNrdG9wL0Btb25vZ3JpZC9ub2RlX21vZHVsZXMvbWFya2Rvd24taXQvbGliL3J1bGVzX2Jsb2NrL3JlZmVyZW5jZS5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL21hcmtkb3duLWl0L2xpYi9ydWxlc19ibG9jay9zdGF0ZV9ibG9jay5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL21hcmtkb3duLWl0L2xpYi9ydWxlc19ibG9jay90YWJsZS5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL21hcmtkb3duLWl0L2xpYi9ydWxlc19jb3JlL2Jsb2NrLmpzIiwid2VicGFjazovL3Z1ZS1saWIvRDovVXNlcnMvZGFuaS9EZXNrdG9wL0Btb25vZ3JpZC9ub2RlX21vZHVsZXMvbWFya2Rvd24taXQvbGliL3J1bGVzX2NvcmUvaW5saW5lLmpzIiwid2VicGFjazovL3Z1ZS1saWIvRDovVXNlcnMvZGFuaS9EZXNrdG9wL0Btb25vZ3JpZC9ub2RlX21vZHVsZXMvbWFya2Rvd24taXQvbGliL3J1bGVzX2NvcmUvbGlua2lmeS5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL21hcmtkb3duLWl0L2xpYi9ydWxlc19jb3JlL25vcm1hbGl6ZS5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL21hcmtkb3duLWl0L2xpYi9ydWxlc19jb3JlL3JlcGxhY2VtZW50cy5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL21hcmtkb3duLWl0L2xpYi9ydWxlc19jb3JlL3NtYXJ0cXVvdGVzLmpzIiwid2VicGFjazovL3Z1ZS1saWIvRDovVXNlcnMvZGFuaS9EZXNrdG9wL0Btb25vZ3JpZC9ub2RlX21vZHVsZXMvbWFya2Rvd24taXQvbGliL3J1bGVzX2NvcmUvc3RhdGVfY29yZS5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL21hcmtkb3duLWl0L2xpYi9ydWxlc19pbmxpbmUvYXV0b2xpbmsuanMiLCJ3ZWJwYWNrOi8vdnVlLWxpYi9EOi9Vc2Vycy9kYW5pL0Rlc2t0b3AvQG1vbm9ncmlkL25vZGVfbW9kdWxlcy9tYXJrZG93bi1pdC9saWIvcnVsZXNfaW5saW5lL2JhY2t0aWNrcy5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL21hcmtkb3duLWl0L2xpYi9ydWxlc19pbmxpbmUvYmFsYW5jZV9wYWlycy5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL21hcmtkb3duLWl0L2xpYi9ydWxlc19pbmxpbmUvZW1waGFzaXMuanMiLCJ3ZWJwYWNrOi8vdnVlLWxpYi9EOi9Vc2Vycy9kYW5pL0Rlc2t0b3AvQG1vbm9ncmlkL25vZGVfbW9kdWxlcy9tYXJrZG93bi1pdC9saWIvcnVsZXNfaW5saW5lL2VudGl0eS5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL21hcmtkb3duLWl0L2xpYi9ydWxlc19pbmxpbmUvZXNjYXBlLmpzIiwid2VicGFjazovL3Z1ZS1saWIvRDovVXNlcnMvZGFuaS9EZXNrdG9wL0Btb25vZ3JpZC9ub2RlX21vZHVsZXMvbWFya2Rvd24taXQvbGliL3J1bGVzX2lubGluZS9odG1sX2lubGluZS5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL21hcmtkb3duLWl0L2xpYi9ydWxlc19pbmxpbmUvaW1hZ2UuanMiLCJ3ZWJwYWNrOi8vdnVlLWxpYi9EOi9Vc2Vycy9kYW5pL0Rlc2t0b3AvQG1vbm9ncmlkL25vZGVfbW9kdWxlcy9tYXJrZG93bi1pdC9saWIvcnVsZXNfaW5saW5lL2xpbmsuanMiLCJ3ZWJwYWNrOi8vdnVlLWxpYi9EOi9Vc2Vycy9kYW5pL0Rlc2t0b3AvQG1vbm9ncmlkL25vZGVfbW9kdWxlcy9tYXJrZG93bi1pdC9saWIvcnVsZXNfaW5saW5lL25ld2xpbmUuanMiLCJ3ZWJwYWNrOi8vdnVlLWxpYi9EOi9Vc2Vycy9kYW5pL0Rlc2t0b3AvQG1vbm9ncmlkL25vZGVfbW9kdWxlcy9tYXJrZG93bi1pdC9saWIvcnVsZXNfaW5saW5lL3N0YXRlX2lubGluZS5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL21hcmtkb3duLWl0L2xpYi9ydWxlc19pbmxpbmUvc3RyaWtldGhyb3VnaC5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL21hcmtkb3duLWl0L2xpYi9ydWxlc19pbmxpbmUvdGV4dC5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL21hcmtkb3duLWl0L2xpYi9ydWxlc19pbmxpbmUvdGV4dF9jb2xsYXBzZS5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL21hcmtkb3duLWl0L2xpYi90b2tlbi5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL21kdXJsL2RlY29kZS5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL21kdXJsL2VuY29kZS5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL21kdXJsL2Zvcm1hdC5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL21kdXJsL2luZGV4LmpzIiwid2VicGFjazovL3Z1ZS1saWIvRDovVXNlcnMvZGFuaS9EZXNrdG9wL0Btb25vZ3JpZC9ub2RlX21vZHVsZXMvbWR1cmwvcGFyc2UuanMiLCJ3ZWJwYWNrOi8vdnVlLWxpYi9EOi9Vc2Vycy9kYW5pL0Rlc2t0b3AvQG1vbm9ncmlkL25vZGVfbW9kdWxlcy9ub2RlLWxpYnMtYnJvd3Nlci9ub2RlX21vZHVsZXMvcHVueWNvZGUvcHVueWNvZGUuanMiLCJ3ZWJwYWNrOi8vdnVlLWxpYi9EOi9Vc2Vycy9kYW5pL0Rlc2t0b3AvQG1vbm9ncmlkL25vZGVfbW9kdWxlcy91Yy5taWNyby9jYXRlZ29yaWVzL0NjL3JlZ2V4LmpzIiwid2VicGFjazovL3Z1ZS1saWIvRDovVXNlcnMvZGFuaS9EZXNrdG9wL0Btb25vZ3JpZC9ub2RlX21vZHVsZXMvdWMubWljcm8vY2F0ZWdvcmllcy9DZi9yZWdleC5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL3VjLm1pY3JvL2NhdGVnb3JpZXMvUC9yZWdleC5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL3VjLm1pY3JvL2NhdGVnb3JpZXMvWi9yZWdleC5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL3VjLm1pY3JvL2luZGV4LmpzIiwid2VicGFjazovL3Z1ZS1saWIvRDovVXNlcnMvZGFuaS9EZXNrdG9wL0Btb25vZ3JpZC9ub2RlX21vZHVsZXMvdWMubWljcm8vcHJvcGVydGllcy9BbnkvcmVnZXguanMiLCJ3ZWJwYWNrOi8vdnVlLWxpYi9EOi9Vc2Vycy9kYW5pL0Rlc2t0b3AvQG1vbm9ncmlkL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9ydW50aW1lL2NvbXBvbmVudE5vcm1hbGl6ZXIuanMiLCJ3ZWJwYWNrOi8vdnVlLWxpYi8od2VicGFjaykvYnVpbGRpbi9hbWQtb3B0aW9ucy5qcyIsIndlYnBhY2s6Ly92dWUtbGliLyh3ZWJwYWNrKS9idWlsZGluL2dsb2JhbC5qcyIsIndlYnBhY2s6Ly92dWUtbGliLyh3ZWJwYWNrKS9idWlsZGluL21vZHVsZS5qcyIsIndlYnBhY2s6Ly92dWUtbGliLy4vc3JjL2NvbXBvbmVudHMvTWFya2Rvd25CbG9jay52dWUiLCJ3ZWJwYWNrOi8vdnVlLWxpYi8uL3NyYy9jb21wb25lbnRzL01hcmtkb3duQmxvY2sudnVlPzc3YzUiLCJ3ZWJwYWNrOi8vdnVlLWxpYi9leHRlcm5hbCBcInZ1ZVwiIl0sIm5hbWVzIjpbIl9hcnJheUxpa2VUb0FycmF5IiwiYXJyIiwibGVuIiwibGVuZ3RoIiwiaSIsImFycjIiLCJBcnJheSIsIm1vZHVsZSIsImV4cG9ydHMiLCJfYXJyYXlXaXRoSG9sZXMiLCJpc0FycmF5IiwiX2l0ZXJhYmxlVG9BcnJheUxpbWl0IiwiU3ltYm9sIiwiaXRlcmF0b3IiLCJPYmplY3QiLCJfYXJyIiwiX24iLCJfZCIsIl9lIiwidW5kZWZpbmVkIiwiX2kiLCJfcyIsIm5leHQiLCJkb25lIiwicHVzaCIsInZhbHVlIiwiZXJyIiwiX25vbkl0ZXJhYmxlUmVzdCIsIlR5cGVFcnJvciIsImFycmF5V2l0aEhvbGVzIiwicmVxdWlyZSIsIml0ZXJhYmxlVG9BcnJheUxpbWl0IiwidW5zdXBwb3J0ZWRJdGVyYWJsZVRvQXJyYXkiLCJub25JdGVyYWJsZVJlc3QiLCJfc2xpY2VkVG9BcnJheSIsIl90eXBlb2YiLCJvYmoiLCJjb25zdHJ1Y3RvciIsInByb3RvdHlwZSIsImFycmF5TGlrZVRvQXJyYXkiLCJfdW5zdXBwb3J0ZWRJdGVyYWJsZVRvQXJyYXkiLCJvIiwibWluTGVuIiwibiIsInRvU3RyaW5nIiwiY2FsbCIsInNsaWNlIiwibmFtZSIsImZyb20iLCJ0ZXN0IiwiYXNzaWduIiwic291cmNlcyIsImFyZ3VtZW50cyIsImZvckVhY2giLCJzb3VyY2UiLCJrZXlzIiwia2V5IiwiX2NsYXNzIiwiaXNTdHJpbmciLCJpc09iamVjdCIsImlzUmVnRXhwIiwiaXNGdW5jdGlvbiIsImVzY2FwZVJFIiwic3RyIiwicmVwbGFjZSIsImRlZmF1bHRPcHRpb25zIiwiZnV6enlMaW5rIiwiZnV6enlFbWFpbCIsImZ1enp5SVAiLCJpc09wdGlvbnNPYmoiLCJyZWR1Y2UiLCJhY2MiLCJrIiwiaGFzT3duUHJvcGVydHkiLCJkZWZhdWx0U2NoZW1hcyIsInZhbGlkYXRlIiwidGV4dCIsInBvcyIsInNlbGYiLCJ0YWlsIiwicmUiLCJodHRwIiwiUmVnRXhwIiwic3JjX2F1dGgiLCJzcmNfaG9zdF9wb3J0X3N0cmljdCIsInNyY19wYXRoIiwibWF0Y2giLCJub19odHRwIiwic3JjX2RvbWFpbiIsInNyY19kb21haW5fcm9vdCIsInNyY19wb3J0Iiwic3JjX2hvc3RfdGVybWluYXRvciIsIm1haWx0byIsInNyY19lbWFpbF9uYW1lIiwic3JjX2hvc3Rfc3RyaWN0IiwidGxkc18yY2hfc3JjX3JlIiwidGxkc19kZWZhdWx0Iiwic3BsaXQiLCJyZXNldFNjYW5DYWNoZSIsIl9faW5kZXhfXyIsIl9fdGV4dF9jYWNoZV9fIiwiY3JlYXRlVmFsaWRhdG9yIiwiY3JlYXRlTm9ybWFsaXplciIsIm5vcm1hbGl6ZSIsImNvbXBpbGUiLCJfX29wdHNfXyIsInRsZHMiLCJfX3RsZHNfXyIsIm9uQ29tcGlsZSIsIl9fdGxkc19yZXBsYWNlZF9fIiwic3JjX3huIiwic3JjX3RsZHMiLCJqb2luIiwidW50cGwiLCJ0cGwiLCJlbWFpbF9mdXp6eSIsInRwbF9lbWFpbF9mdXp6eSIsImxpbmtfZnV6enkiLCJ0cGxfbGlua19mdXp6eSIsImxpbmtfbm9faXBfZnV6enkiLCJ0cGxfbGlua19ub19pcF9mdXp6eSIsImhvc3RfZnV6enlfdGVzdCIsInRwbF9ob3N0X2Z1enp5X3Rlc3QiLCJhbGlhc2VzIiwiX19jb21waWxlZF9fIiwic2NoZW1hRXJyb3IiLCJ2YWwiLCJFcnJvciIsIl9fc2NoZW1hc19fIiwiY29tcGlsZWQiLCJsaW5rIiwiYWxpYXMiLCJzbGlzdCIsImZpbHRlciIsIm1hcCIsInNjaGVtYV90ZXN0Iiwic3JjX1pQQ2MiLCJzY2hlbWFfc2VhcmNoIiwicHJldGVzdCIsIk1hdGNoIiwic2hpZnQiLCJzdGFydCIsImVuZCIsIl9fbGFzdF9pbmRleF9fIiwic2NoZW1hIiwiX19zY2hlbWFfXyIsInRvTG93ZXJDYXNlIiwiaW5kZXgiLCJsYXN0SW5kZXgiLCJyYXciLCJ1cmwiLCJjcmVhdGVNYXRjaCIsIkxpbmtpZnlJdCIsInNjaGVtYXMiLCJvcHRpb25zIiwiYWRkIiwiZGVmaW5pdGlvbiIsInNldCIsIm0iLCJtbCIsIm1lIiwidGxkX3BvcyIsImF0X3BvcyIsImV4ZWMiLCJ0ZXN0U2NoZW1hQXQiLCJzZWFyY2giLCJpbmRleE9mIiwicmVzdWx0IiwibGlzdCIsImtlZXBPbGQiLCJjb25jYXQiLCJzb3J0IiwiZWwiLCJpZHgiLCJyZXZlcnNlIiwib3B0cyIsInNyY19BbnkiLCJzcmNfQ2MiLCJzcmNfWiIsInNyY19QIiwic3JjX1pDYyIsInRleHRfc2VwYXJhdG9ycyIsInNyY19wc2V1ZG9fbGV0dGVyIiwic3JjX2lwNCIsInNyY19ob3N0IiwidHBsX2hvc3RfZnV6enkiLCJ0cGxfaG9zdF9ub19pcF9mdXp6eSIsInRwbF9ob3N0X2Z1enp5X3N0cmljdCIsInRwbF9ob3N0X3BvcnRfZnV6enlfc3RyaWN0IiwidHBsX2hvc3RfcG9ydF9ub19pcF9mdXp6eV9zdHJpY3QiLCJjb250YWluZXJfcGx1Z2luIiwibWQiLCJ2YWxpZGF0ZURlZmF1bHQiLCJwYXJhbXMiLCJ0cmltIiwicmVuZGVyRGVmYXVsdCIsInRva2VucyIsIl9vcHRpb25zIiwiZW52IiwibmVzdGluZyIsImF0dHJQdXNoIiwicmVuZGVyVG9rZW4iLCJtaW5fbWFya2VycyIsIm1hcmtlcl9zdHIiLCJtYXJrZXIiLCJtYXJrZXJfY2hhciIsImNoYXJDb2RlQXQiLCJtYXJrZXJfbGVuIiwicmVuZGVyIiwiY29udGFpbmVyIiwic3RhdGUiLCJzdGFydExpbmUiLCJlbmRMaW5lIiwic2lsZW50IiwibmV4dExpbmUiLCJtYXJrZXJfY291bnQiLCJtYXJrdXAiLCJ0b2tlbiIsIm9sZF9wYXJlbnQiLCJvbGRfbGluZV9tYXgiLCJhdXRvX2Nsb3NlZCIsImJNYXJrcyIsInRTaGlmdCIsIm1heCIsImVNYXJrcyIsInNyYyIsIk1hdGgiLCJmbG9vciIsInNDb3VudCIsImJsa0luZGVudCIsInNraXBTcGFjZXMiLCJwYXJlbnRUeXBlIiwibGluZU1heCIsImJsb2NrIiwiaW5mbyIsInRva2VuaXplIiwibGluZSIsInJ1bGVyIiwiYmVmb3JlIiwiYWx0IiwicmVuZGVyZXIiLCJydWxlcyIsImVtYmVkUkUiLCJwbHVnaW4iLCJjdXN0b20iLCJ0b2tlbml6ZUJsb2NrIiwidGFnIiwiYXJnIiwiY3VzdG9tRW1iZWQiLCJzdGFydFBvcyIsIm1heFBvcyIsInBvaW50ZXIiLCJwcmV2TGluZVN0YXJ0UG9zIiwicHJldkxpbmVNYXhQb3MiLCJhbGwiLCJuZXh0TGluZVN0YXJ0UG9zIiwibmV4dExpbmVNYXhQb3MiLCJmaW5kRmlyc3RNYXRjaGluZ0NvbmZpZyIsImNvbmZpZ3MiLCJjb25maWciLCJocmVmIiwiYXR0cnMiLCJhdHRySW5kZXgiLCJwYXR0ZXJuIiwiYXBwbHlBdHRyaWJ1dGVzIiwiYXR0cmlidXRlcyIsImF0dHIiLCJtYXJrZG93bml0TGlua0F0dHJpYnV0ZXMiLCJmcmVlemUiLCJkZWZhdWx0UmVuZGVyIiwibGlua19vcGVuIiwiYXR0cl9uYW1lIiwidW5xdW90ZWQiLCJzaW5nbGVfcXVvdGVkIiwiZG91YmxlX3F1b3RlZCIsImF0dHJfdmFsdWUiLCJhdHRyaWJ1dGUiLCJvcGVuX3RhZyIsImNsb3NlX3RhZyIsImNvbW1lbnQiLCJwcm9jZXNzaW5nIiwiZGVjbGFyYXRpb24iLCJjZGF0YSIsIkhUTUxfVEFHX1JFIiwiSFRNTF9PUEVOX0NMT1NFX1RBR19SRSIsIl9oYXNPd25Qcm9wZXJ0eSIsImhhcyIsIm9iamVjdCIsImFycmF5UmVwbGFjZUF0IiwibmV3RWxlbWVudHMiLCJpc1ZhbGlkRW50aXR5Q29kZSIsImMiLCJmcm9tQ29kZVBvaW50Iiwic3Vycm9nYXRlMSIsInN1cnJvZ2F0ZTIiLCJTdHJpbmciLCJmcm9tQ2hhckNvZGUiLCJVTkVTQ0FQRV9NRF9SRSIsIkVOVElUWV9SRSIsIlVORVNDQVBFX0FMTF9SRSIsIkRJR0lUQUxfRU5USVRZX1RFU1RfUkUiLCJlbnRpdGllcyIsInJlcGxhY2VFbnRpdHlQYXR0ZXJuIiwiY29kZSIsInBhcnNlSW50IiwidW5lc2NhcGVNZCIsInVuZXNjYXBlQWxsIiwiZXNjYXBlZCIsImVudGl0eSIsIkhUTUxfRVNDQVBFX1RFU1RfUkUiLCJIVE1MX0VTQ0FQRV9SRVBMQUNFX1JFIiwiSFRNTF9SRVBMQUNFTUVOVFMiLCJyZXBsYWNlVW5zYWZlQ2hhciIsImNoIiwiZXNjYXBlSHRtbCIsIlJFR0VYUF9FU0NBUEVfUkUiLCJpc1NwYWNlIiwiaXNXaGl0ZVNwYWNlIiwiVU5JQ09ERV9QVU5DVF9SRSIsImlzUHVuY3RDaGFyIiwiaXNNZEFzY2lpUHVuY3QiLCJub3JtYWxpemVSZWZlcmVuY2UiLCJ0b1VwcGVyQ2FzZSIsImxpYiIsIm1kdXJsIiwidWNtaWNybyIsInBhcnNlTGlua0xhYmVsIiwicGFyc2VMaW5rRGVzdGluYXRpb24iLCJwYXJzZUxpbmtUaXRsZSIsImxldmVsIiwibGluZXMiLCJvayIsImRpc2FibGVOZXN0ZWQiLCJmb3VuZCIsInByZXZQb3MiLCJsYWJlbEVuZCIsInBvc01heCIsIm9sZFBvcyIsImlubGluZSIsInNraXBUb2tlbiIsInV0aWxzIiwiaGVscGVycyIsIlJlbmRlcmVyIiwiUGFyc2VyQ29yZSIsIlBhcnNlckJsb2NrIiwiUGFyc2VySW5saW5lIiwicHVueWNvZGUiLCJ6ZXJvIiwiY29tbW9ubWFyayIsIkJBRF9QUk9UT19SRSIsIkdPT0RfREFUQV9SRSIsInZhbGlkYXRlTGluayIsIlJFQ09ERV9IT1NUTkFNRV9GT1IiLCJub3JtYWxpemVMaW5rIiwicGFyc2VkIiwicGFyc2UiLCJob3N0bmFtZSIsInByb3RvY29sIiwidG9BU0NJSSIsImVyIiwiZW5jb2RlIiwiZm9ybWF0Iiwibm9ybWFsaXplTGlua1RleHQiLCJ0b1VuaWNvZGUiLCJkZWNvZGUiLCJNYXJrZG93bkl0IiwicHJlc2V0TmFtZSIsImNvcmUiLCJsaW5raWZ5IiwiY29uZmlndXJlIiwicHJlc2V0cyIsImNvbXBvbmVudHMiLCJlbmFibGVPbmx5IiwicnVsZXMyIiwicnVsZXIyIiwiZW5hYmxlIiwiaWdub3JlSW52YWxpZCIsImNoYWluIiwibWlzc2VkIiwiZGlzYWJsZSIsInVzZSIsImFyZ3MiLCJhcHBseSIsIlN0YXRlIiwicHJvY2VzcyIsInBhcnNlSW5saW5lIiwiaW5saW5lTW9kZSIsInJlbmRlcklubGluZSIsIlJ1bGVyIiwiX3J1bGVzIiwiZ2V0UnVsZXMiLCJoYXNFbXB0eUxpbmVzIiwibWF4TmVzdGluZyIsInNraXBFbXB0eUxpbmVzIiwidGlnaHQiLCJpc0VtcHR5Iiwib3V0VG9rZW5zIiwiQ29yZSIsImwiLCJfcnVsZXMyIiwicG9zdFByb2Nlc3MiLCJjYWNoZSIsInBlbmRpbmciLCJwdXNoUGVuZGluZyIsImh0bWwiLCJ4aHRtbE91dCIsImJyZWFrcyIsImxhbmdQcmVmaXgiLCJ0eXBvZ3JhcGhlciIsInF1b3RlcyIsImhpZ2hsaWdodCIsImRlZmF1bHRfcnVsZXMiLCJjb2RlX2lubGluZSIsInNsZiIsInJlbmRlckF0dHJzIiwiY29udGVudCIsImNvZGVfYmxvY2siLCJmZW5jZSIsImxhbmdOYW1lIiwiaGlnaGxpZ2h0ZWQiLCJ0bXBBdHRycyIsInRtcFRva2VuIiwiaW1hZ2UiLCJyZW5kZXJJbmxpbmVBc1RleHQiLCJjaGlsZHJlbiIsImhhcmRicmVhayIsInNvZnRicmVhayIsImh0bWxfYmxvY2siLCJodG1sX2lubGluZSIsIm5leHRUb2tlbiIsIm5lZWRMZiIsImhpZGRlbiIsInR5cGUiLCJfX3J1bGVzX18iLCJfX2NhY2hlX18iLCJfX2ZpbmRfXyIsIl9fY29tcGlsZV9fIiwiY2hhaW5zIiwicnVsZSIsImVuYWJsZWQiLCJhbHROYW1lIiwiZm4iLCJhdCIsIm9wdCIsImJlZm9yZU5hbWUiLCJydWxlTmFtZSIsInNwbGljZSIsImFmdGVyIiwiYWZ0ZXJOYW1lIiwiY2hhaW5OYW1lIiwiYmxvY2txdW90ZSIsImFkanVzdFRhYiIsImluaXRpYWwiLCJsYXN0TGluZUVtcHR5Iiwib2Zmc2V0Iiwib2xkQk1hcmtzIiwib2xkQlNDb3VudCIsIm9sZEluZGVudCIsIm9sZFBhcmVudFR5cGUiLCJvbGRTQ291bnQiLCJvbGRUU2hpZnQiLCJzcGFjZUFmdGVyTWFya2VyIiwidGVybWluYXRlIiwidGVybWluYXRvclJ1bGVzIiwid2FzT3V0ZGVudGVkIiwib2xkTGluZU1heCIsImJzQ291bnQiLCJsYXN0IiwiZ2V0TGluZXMiLCJtZW0iLCJoYXZlRW5kTWFya2VyIiwic2tpcENoYXJzIiwiaGVhZGluZyIsInRtcCIsInNraXBTcGFjZXNCYWNrIiwic2tpcENoYXJzQmFjayIsImhyIiwiY250IiwiYmxvY2tfbmFtZXMiLCJIVE1MX1NFUVVFTkNFUyIsImxpbmVUZXh0IiwibGhlYWRpbmciLCJza2lwQnVsbGV0TGlzdE1hcmtlciIsInNraXBPcmRlcmVkTGlzdE1hcmtlciIsIm1hcmtUaWdodFBhcmFncmFwaHMiLCJjb250ZW50U3RhcnQiLCJpbmRlbnQiLCJpbmRlbnRBZnRlck1hcmtlciIsImlzT3JkZXJlZCIsIml0ZW1MaW5lcyIsImxpc3RMaW5lcyIsImxpc3RUb2tJZHgiLCJtYXJrZXJDaGFyQ29kZSIsIm1hcmtlclZhbHVlIiwib2xkTGlzdEluZGVudCIsIm9sZFRpZ2h0IiwicG9zQWZ0ZXJNYXJrZXIiLCJwcmV2RW1wdHlFbmQiLCJpc1Rlcm1pbmF0aW5nUGFyYWdyYXBoIiwibGlzdEluZGVudCIsIk51bWJlciIsInN1YnN0ciIsIm1pbiIsInBhcmFncmFwaCIsInJlZmVyZW5jZSIsIl9lbmRMaW5lIiwiZGVzdEVuZFBvcyIsImRlc3RFbmRMaW5lTm8iLCJsYWJlbCIsInJlcyIsInRpdGxlIiwicmVmZXJlbmNlcyIsIlRva2VuIiwiU3RhdGVCbG9jayIsInMiLCJpbmRlbnRfZm91bmQiLCJkZEluZGVudCIsImJlZ2luIiwia2VlcExhc3RMRiIsImxpbmVJbmRlbnQiLCJmaXJzdCIsInF1ZXVlIiwibGluZVN0YXJ0IiwiZ2V0TGluZSIsImVzY2FwZWRTcGxpdCIsImVzY2FwZXMiLCJsYXN0UG9zIiwiYmFja1RpY2tlZCIsImxhc3RCYWNrVGljayIsInN1YnN0cmluZyIsInRhYmxlIiwiY29sdW1ucyIsImNvbHVtbkNvdW50IiwiYWxpZ25zIiwidCIsInRhYmxlTGluZXMiLCJ0Ym9keUxpbmVzIiwidG9rIiwiaXNMaW5rT3BlbiIsImlzTGlua0Nsb3NlIiwiaiIsImN1cnJlbnRUb2tlbiIsIm5vZGVzIiwibG4iLCJodG1sTGlua0xldmVsIiwiZnVsbFVybCIsInVybFRleHQiLCJibG9ja1Rva2VucyIsImxpbmtzIiwiTkVXTElORVNfUkUiLCJOVUxMX1JFIiwiUkFSRV9SRSIsIlNDT1BFRF9BQkJSX1RFU1RfUkUiLCJTQ09QRURfQUJCUl9SRSIsIlNDT1BFRF9BQkJSIiwiciIsInAiLCJ0bSIsInJlcGxhY2VGbiIsInJlcGxhY2Vfc2NvcGVkIiwiaW5saW5lVG9rZW5zIiwiaW5zaWRlX2F1dG9saW5rIiwicmVwbGFjZV9yYXJlIiwiYmxrSWR4IiwiUVVPVEVfVEVTVF9SRSIsIlFVT1RFX1JFIiwiQVBPU1RST1BIRSIsInJlcGxhY2VBdCIsInByb2Nlc3NfaW5saW5lcyIsInRoaXNMZXZlbCIsIml0ZW0iLCJsYXN0Q2hhciIsIm5leHRDaGFyIiwiaXNMYXN0UHVuY3RDaGFyIiwiaXNOZXh0UHVuY3RDaGFyIiwiaXNMYXN0V2hpdGVTcGFjZSIsImlzTmV4dFdoaXRlU3BhY2UiLCJjYW5PcGVuIiwiY2FuQ2xvc2UiLCJpc1NpbmdsZSIsInN0YWNrIiwib3BlblF1b3RlIiwiY2xvc2VRdW90ZSIsIk9VVEVSIiwic2luZ2xlIiwic21hcnRxdW90ZXMiLCJTdGF0ZUNvcmUiLCJFTUFJTF9SRSIsIkFVVE9MSU5LX1JFIiwiYXV0b2xpbmsiLCJsaW5rTWF0Y2giLCJlbWFpbE1hdGNoIiwiYmFja3RpY2siLCJtYXRjaFN0YXJ0IiwibWF0Y2hFbmQiLCJwcm9jZXNzRGVsaW1pdGVycyIsImRlbGltaXRlcnMiLCJjbG9zZXJJZHgiLCJvcGVuZXJJZHgiLCJjbG9zZXIiLCJvcGVuZXIiLCJtaW5PcGVuZXJJZHgiLCJuZXdNaW5PcGVuZXJJZHgiLCJpc09kZE1hdGNoIiwibGFzdEp1bXAiLCJvcGVuZXJzQm90dG9tIiwiY2xvc2UiLCJqdW1wIiwib3BlbiIsImxpbmtfcGFpcnMiLCJjdXJyIiwidG9rZW5zX21ldGEiLCJlbXBoYXNpcyIsInNjYW5uZWQiLCJzY2FuRGVsaW1zIiwiY2FuX29wZW4iLCJjYW5fY2xvc2UiLCJzdGFydERlbGltIiwiZW5kRGVsaW0iLCJpc1N0cm9uZyIsIkRJR0lUQUxfUkUiLCJOQU1FRF9SRSIsIkVTQ0FQRUQiLCJlc2NhcGUiLCJpc0xldHRlciIsImxjIiwibGFiZWxTdGFydCIsInJlZiIsInBhcnNlUmVmZXJlbmNlIiwibmV3bGluZSIsInBtYXgiLCJTdGF0ZUlubGluZSIsInBlbmRpbmdMZXZlbCIsIl9wcmV2X2RlbGltaXRlcnMiLCJ0b2tlbl9tZXRhIiwicG9wIiwiY2FuU3BsaXRXb3JkIiwiY291bnQiLCJsZWZ0X2ZsYW5raW5nIiwicmlnaHRfZmxhbmtpbmciLCJzdHJpa2V0aHJvdWdoIiwibG9uZU1hcmtlcnMiLCJpc1Rlcm1pbmF0b3JDaGFyIiwidGV4dF9jb2xsYXBzZSIsIm1ldGEiLCJhdHRyRGF0YSIsImF0dHJTZXQiLCJhdHRyR2V0IiwiYXR0ckpvaW4iLCJkZWNvZGVDYWNoZSIsImdldERlY29kZUNhY2hlIiwiZXhjbHVkZSIsInN0cmluZyIsImRlZmF1bHRDaGFycyIsInNlcSIsImIxIiwiYjIiLCJiMyIsImI0IiwiY2hyIiwiY29tcG9uZW50Q2hhcnMiLCJlbmNvZGVDYWNoZSIsImdldEVuY29kZUNhY2hlIiwia2VlcEVzY2FwZWQiLCJuZXh0Q29kZSIsImVuY29kZVVSSUNvbXBvbmVudCIsInNsYXNoZXMiLCJhdXRoIiwicG9ydCIsInBhdGhuYW1lIiwiaGFzaCIsIlVybCIsInByb3RvY29sUGF0dGVybiIsInBvcnRQYXR0ZXJuIiwic2ltcGxlUGF0aFBhdHRlcm4iLCJkZWxpbXMiLCJ1bndpc2UiLCJhdXRvRXNjYXBlIiwibm9uSG9zdENoYXJzIiwiaG9zdEVuZGluZ0NoYXJzIiwiaG9zdG5hbWVNYXhMZW4iLCJob3N0bmFtZVBhcnRQYXR0ZXJuIiwiaG9zdG5hbWVQYXJ0U3RhcnQiLCJob3N0bGVzc1Byb3RvY29sIiwic2xhc2hlZFByb3RvY29sIiwidXJsUGFyc2UiLCJzbGFzaGVzRGVub3RlSG9zdCIsInUiLCJsb3dlclByb3RvIiwiaGVjIiwicmVzdCIsInNpbXBsZVBhdGgiLCJwcm90byIsImhvc3RFbmQiLCJhdFNpZ24iLCJsYXN0SW5kZXhPZiIsImhvc3QiLCJwYXJzZUhvc3QiLCJpcHY2SG9zdG5hbWUiLCJob3N0cGFydHMiLCJwYXJ0IiwibmV3cGFydCIsInZhbGlkUGFydHMiLCJub3RIb3N0IiwiYml0IiwidW5zaGlmdCIsInFtIiwicm9vdCIsImZyZWVFeHBvcnRzIiwibm9kZVR5cGUiLCJmcmVlTW9kdWxlIiwiZnJlZUdsb2JhbCIsImdsb2JhbCIsIndpbmRvdyIsIm1heEludCIsImJhc2UiLCJ0TWluIiwidE1heCIsInNrZXciLCJkYW1wIiwiaW5pdGlhbEJpYXMiLCJpbml0aWFsTiIsImRlbGltaXRlciIsInJlZ2V4UHVueWNvZGUiLCJyZWdleE5vbkFTQ0lJIiwicmVnZXhTZXBhcmF0b3JzIiwiZXJyb3JzIiwiYmFzZU1pbnVzVE1pbiIsInN0cmluZ0Zyb21DaGFyQ29kZSIsImVycm9yIiwiUmFuZ2VFcnJvciIsImFycmF5IiwibWFwRG9tYWluIiwicGFydHMiLCJsYWJlbHMiLCJlbmNvZGVkIiwidWNzMmRlY29kZSIsIm91dHB1dCIsImNvdW50ZXIiLCJleHRyYSIsInVjczJlbmNvZGUiLCJiYXNpY1RvRGlnaXQiLCJjb2RlUG9pbnQiLCJkaWdpdFRvQmFzaWMiLCJkaWdpdCIsImZsYWciLCJhZGFwdCIsImRlbHRhIiwibnVtUG9pbnRzIiwiZmlyc3RUaW1lIiwiaW5wdXQiLCJpbnB1dExlbmd0aCIsIm91dCIsImJpYXMiLCJiYXNpYyIsIm9sZGkiLCJ3IiwiYmFzZU1pbnVzVCIsImhhbmRsZWRDUENvdW50IiwiYmFzaWNMZW5ndGgiLCJxIiwiY3VycmVudFZhbHVlIiwiaGFuZGxlZENQQ291bnRQbHVzT25lIiwicU1pbnVzVCIsImRlZmluZSIsIkFueSIsIkNjIiwiQ2YiLCJQIiwiWiIsImciLCJGdW5jdGlvbiIsImUiLCJ3ZWJwYWNrUG9seWZpbGwiLCJkZXByZWNhdGUiLCJwYXRocyIsImRlZmluZVByb3BlcnR5IiwiZW51bWVyYWJsZSIsImdldCJdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQztBQUNELE87UUNWQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTs7O1FBR0E7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLDBDQUEwQyxnQ0FBZ0M7UUFDMUU7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSx3REFBd0Qsa0JBQWtCO1FBQzFFO1FBQ0EsaURBQWlELGNBQWM7UUFDL0Q7O1FBRUE7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBLHlDQUF5QyxpQ0FBaUM7UUFDMUUsZ0hBQWdILG1CQUFtQixFQUFFO1FBQ3JJO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0EsMkJBQTJCLDBCQUEwQixFQUFFO1FBQ3ZELGlDQUFpQyxlQUFlO1FBQ2hEO1FBQ0E7UUFDQTs7UUFFQTtRQUNBLHNEQUFzRCwrREFBK0Q7O1FBRXJIO1FBQ0E7OztRQUdBO1FBQ0E7Ozs7Ozs7Ozs7OztBQ2xGQSxTQUFTQSxpQkFBVCxDQUEyQkMsR0FBM0IsRUFBZ0NDLEdBQWhDLEVBQXFDO0FBQ25DLE1BQUlBLEdBQUcsSUFBSSxJQUFQLElBQWVBLEdBQUcsR0FBR0QsR0FBRyxDQUFDRSxNQUE3QixFQUFxQ0QsR0FBRyxHQUFHRCxHQUFHLENBQUNFLE1BQVY7O0FBRXJDLE9BQUssSUFBSUMsQ0FBQyxHQUFHLENBQVIsRUFBV0MsSUFBSSxHQUFHLElBQUlDLEtBQUosQ0FBVUosR0FBVixDQUF2QixFQUF1Q0UsQ0FBQyxHQUFHRixHQUEzQyxFQUFnREUsQ0FBQyxFQUFqRCxFQUFxRDtBQUNuREMsUUFBSSxDQUFDRCxDQUFELENBQUosR0FBVUgsR0FBRyxDQUFDRyxDQUFELENBQWI7QUFDRDs7QUFFRCxTQUFPQyxJQUFQO0FBQ0Q7O0FBRURFLE1BQU0sQ0FBQ0MsT0FBUCxHQUFpQlIsaUJBQWpCLEM7Ozs7Ozs7Ozs7O0FDVkEsU0FBU1MsZUFBVCxDQUF5QlIsR0FBekIsRUFBOEI7QUFDNUIsTUFBSUssS0FBSyxDQUFDSSxPQUFOLENBQWNULEdBQWQsQ0FBSixFQUF3QixPQUFPQSxHQUFQO0FBQ3pCOztBQUVETSxNQUFNLENBQUNDLE9BQVAsR0FBaUJDLGVBQWpCLEM7Ozs7Ozs7Ozs7O0FDSkEsU0FBU0UscUJBQVQsQ0FBK0JWLEdBQS9CLEVBQW9DRyxDQUFwQyxFQUF1QztBQUNyQyxNQUFJLE9BQU9RLE1BQVAsS0FBa0IsV0FBbEIsSUFBaUMsRUFBRUEsTUFBTSxDQUFDQyxRQUFQLElBQW1CQyxNQUFNLENBQUNiLEdBQUQsQ0FBM0IsQ0FBckMsRUFBd0U7QUFDeEUsTUFBSWMsSUFBSSxHQUFHLEVBQVg7QUFDQSxNQUFJQyxFQUFFLEdBQUcsSUFBVDtBQUNBLE1BQUlDLEVBQUUsR0FBRyxLQUFUO0FBQ0EsTUFBSUMsRUFBRSxHQUFHQyxTQUFUOztBQUVBLE1BQUk7QUFDRixTQUFLLElBQUlDLEVBQUUsR0FBR25CLEdBQUcsQ0FBQ1csTUFBTSxDQUFDQyxRQUFSLENBQUgsRUFBVCxFQUFpQ1EsRUFBdEMsRUFBMEMsRUFBRUwsRUFBRSxHQUFHLENBQUNLLEVBQUUsR0FBR0QsRUFBRSxDQUFDRSxJQUFILEVBQU4sRUFBaUJDLElBQXhCLENBQTFDLEVBQXlFUCxFQUFFLEdBQUcsSUFBOUUsRUFBb0Y7QUFDbEZELFVBQUksQ0FBQ1MsSUFBTCxDQUFVSCxFQUFFLENBQUNJLEtBQWI7O0FBRUEsVUFBSXJCLENBQUMsSUFBSVcsSUFBSSxDQUFDWixNQUFMLEtBQWdCQyxDQUF6QixFQUE0QjtBQUM3QjtBQUNGLEdBTkQsQ0FNRSxPQUFPc0IsR0FBUCxFQUFZO0FBQ1pULE1BQUUsR0FBRyxJQUFMO0FBQ0FDLE1BQUUsR0FBR1EsR0FBTDtBQUNELEdBVEQsU0FTVTtBQUNSLFFBQUk7QUFDRixVQUFJLENBQUNWLEVBQUQsSUFBT0ksRUFBRSxDQUFDLFFBQUQsQ0FBRixJQUFnQixJQUEzQixFQUFpQ0EsRUFBRSxDQUFDLFFBQUQsQ0FBRjtBQUNsQyxLQUZELFNBRVU7QUFDUixVQUFJSCxFQUFKLEVBQVEsTUFBTUMsRUFBTjtBQUNUO0FBQ0Y7O0FBRUQsU0FBT0gsSUFBUDtBQUNEOztBQUVEUixNQUFNLENBQUNDLE9BQVAsR0FBaUJHLHFCQUFqQixDOzs7Ozs7Ozs7OztBQzNCQSxTQUFTZ0IsZ0JBQVQsR0FBNEI7QUFDMUIsUUFBTSxJQUFJQyxTQUFKLENBQWMsMklBQWQsQ0FBTjtBQUNEOztBQUVEckIsTUFBTSxDQUFDQyxPQUFQLEdBQWlCbUIsZ0JBQWpCLEM7Ozs7Ozs7Ozs7O0FDSkEsSUFBSUUsY0FBYyxHQUFHQyxtQkFBTyxDQUFDLHFGQUFELENBQTVCOztBQUVBLElBQUlDLG9CQUFvQixHQUFHRCxtQkFBTyxDQUFDLGlHQUFELENBQWxDOztBQUVBLElBQUlFLDBCQUEwQixHQUFHRixtQkFBTyxDQUFDLDZHQUFELENBQXhDOztBQUVBLElBQUlHLGVBQWUsR0FBR0gsbUJBQU8sQ0FBQyx1RkFBRCxDQUE3Qjs7QUFFQSxTQUFTSSxjQUFULENBQXdCakMsR0FBeEIsRUFBNkJHLENBQTdCLEVBQWdDO0FBQzlCLFNBQU95QixjQUFjLENBQUM1QixHQUFELENBQWQsSUFBdUI4QixvQkFBb0IsQ0FBQzlCLEdBQUQsRUFBTUcsQ0FBTixDQUEzQyxJQUF1RDRCLDBCQUEwQixDQUFDL0IsR0FBRCxFQUFNRyxDQUFOLENBQWpGLElBQTZGNkIsZUFBZSxFQUFuSDtBQUNEOztBQUVEMUIsTUFBTSxDQUFDQyxPQUFQLEdBQWlCMEIsY0FBakIsQzs7Ozs7Ozs7Ozs7QUNaQSxTQUFTQyxPQUFULENBQWlCQyxHQUFqQixFQUFzQjtBQUNwQjs7QUFFQSxNQUFJLE9BQU94QixNQUFQLEtBQWtCLFVBQWxCLElBQWdDLE9BQU9BLE1BQU0sQ0FBQ0MsUUFBZCxLQUEyQixRQUEvRCxFQUF5RTtBQUN2RU4sVUFBTSxDQUFDQyxPQUFQLEdBQWlCMkIsT0FBTyxHQUFHLFNBQVNBLE9BQVQsQ0FBaUJDLEdBQWpCLEVBQXNCO0FBQy9DLGFBQU8sT0FBT0EsR0FBZDtBQUNELEtBRkQ7QUFHRCxHQUpELE1BSU87QUFDTDdCLFVBQU0sQ0FBQ0MsT0FBUCxHQUFpQjJCLE9BQU8sR0FBRyxTQUFTQSxPQUFULENBQWlCQyxHQUFqQixFQUFzQjtBQUMvQyxhQUFPQSxHQUFHLElBQUksT0FBT3hCLE1BQVAsS0FBa0IsVUFBekIsSUFBdUN3QixHQUFHLENBQUNDLFdBQUosS0FBb0J6QixNQUEzRCxJQUFxRXdCLEdBQUcsS0FBS3hCLE1BQU0sQ0FBQzBCLFNBQXBGLEdBQWdHLFFBQWhHLEdBQTJHLE9BQU9GLEdBQXpIO0FBQ0QsS0FGRDtBQUdEOztBQUVELFNBQU9ELE9BQU8sQ0FBQ0MsR0FBRCxDQUFkO0FBQ0Q7O0FBRUQ3QixNQUFNLENBQUNDLE9BQVAsR0FBaUIyQixPQUFqQixDOzs7Ozs7Ozs7OztBQ2hCQSxJQUFJSSxnQkFBZ0IsR0FBR1QsbUJBQU8sQ0FBQyx5RkFBRCxDQUE5Qjs7QUFFQSxTQUFTVSwyQkFBVCxDQUFxQ0MsQ0FBckMsRUFBd0NDLE1BQXhDLEVBQWdEO0FBQzlDLE1BQUksQ0FBQ0QsQ0FBTCxFQUFRO0FBQ1IsTUFBSSxPQUFPQSxDQUFQLEtBQWEsUUFBakIsRUFBMkIsT0FBT0YsZ0JBQWdCLENBQUNFLENBQUQsRUFBSUMsTUFBSixDQUF2QjtBQUMzQixNQUFJQyxDQUFDLEdBQUc3QixNQUFNLENBQUN3QixTQUFQLENBQWlCTSxRQUFqQixDQUEwQkMsSUFBMUIsQ0FBK0JKLENBQS9CLEVBQWtDSyxLQUFsQyxDQUF3QyxDQUF4QyxFQUEyQyxDQUFDLENBQTVDLENBQVI7QUFDQSxNQUFJSCxDQUFDLEtBQUssUUFBTixJQUFrQkYsQ0FBQyxDQUFDSixXQUF4QixFQUFxQ00sQ0FBQyxHQUFHRixDQUFDLENBQUNKLFdBQUYsQ0FBY1UsSUFBbEI7QUFDckMsTUFBSUosQ0FBQyxLQUFLLEtBQU4sSUFBZUEsQ0FBQyxLQUFLLEtBQXpCLEVBQWdDLE9BQU9yQyxLQUFLLENBQUMwQyxJQUFOLENBQVdQLENBQVgsQ0FBUDtBQUNoQyxNQUFJRSxDQUFDLEtBQUssV0FBTixJQUFxQiwyQ0FBMkNNLElBQTNDLENBQWdETixDQUFoRCxDQUF6QixFQUE2RSxPQUFPSixnQkFBZ0IsQ0FBQ0UsQ0FBRCxFQUFJQyxNQUFKLENBQXZCO0FBQzlFOztBQUVEbkMsTUFBTSxDQUFDQyxPQUFQLEdBQWlCZ0MsMkJBQWpCLEM7Ozs7Ozs7Ozs7OztBQ1ZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7QUFHQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0Esa0JBREE7QUFFQSxvQkFGQTtBQUdBO0FBSEEsS0FKQTs7QUFTQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBMkJBO0FBQ0EsbUJBREE7QUFFQTtBQUZBLEtBcENBOztBQXdDQTs7Ozs7QUFLQTtBQUNBLGtCQURBO0FBRUE7QUFGQSxLQTdDQTs7QUFpREE7Ozs7O0FBS0E7QUFDQSxtQkFEQTtBQUVBO0FBRkEsS0F0REE7O0FBMERBOzs7OztBQUtBO0FBQ0EsbUJBREE7QUFFQTtBQUZBLEtBL0RBOztBQW1FQTs7Ozs7OztBQU9BO0FBQ0Esa0JBREE7QUFFQSxhQUZBLHNCQUVBO0FBQ0E7QUFDQTtBQUNBLDRCQURBO0FBRUE7QUFGQTtBQURBO0FBTUE7QUFUQSxLQTFFQTs7QUFxRkE7Ozs7Ozs7Ozs7QUFVQTtBQUNBLGtCQURBO0FBRUEsYUFGQSxzQkFFQTtBQUNBO0FBQ0EsZ0JBREEsZ0JBQ0EsR0FEQSxFQUNBO0FBQ0E7QUFDQTtBQUhBO0FBS0E7QUFSQSxLQS9GQTs7QUF5R0E7Ozs7Ozs7Ozs7OztBQVlBO0FBQ0Esa0JBREE7QUFFQSxhQUZBLHNCQUVBO0FBQ0E7QUFDQTtBQUpBO0FBckhBLEdBREE7QUE4SEEsU0E5SEEscUJBOEhBO0FBQ0E7QUFDQSxHQWhJQTtBQWtJQSxRQWxJQSxrQkFrSUEsYUFsSUEsRUFrSUE7QUFDQTtBQUNBLHlFQUZBLENBSUE7O0FBQ0E7QUFDQSx5QkFEQTtBQUVBLHlCQUZBO0FBR0EseUJBSEE7QUFJQSxzQkFKQTtBQUtBLDZDQUxBO0FBTUE7QUFDQTtBQURBO0FBTkEsTUFMQSxDQWVBOztBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0EsR0FuSkE7QUFxSkE7QUFDQSxXQURBLG1CQUNBLEtBREEsRUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUpBO0FBS0EsZUFMQSx1QkFLQSxLQUxBLEVBS0E7QUFDQTtBQUNBO0FBQ0EsS0FSQTtBQVNBLG1CQVRBLDJCQVNBLEtBVEEsRUFTQTtBQUNBO0FBQ0E7QUFDQSxLQVpBO0FBYUEsZ0JBYkEsd0JBYUEsS0FiQSxFQWFBO0FBQ0E7QUFDQTtBQUNBLEtBaEJBO0FBaUJBLG9CQWpCQSw0QkFpQkEsS0FqQkEsRUFpQkE7QUFDQTtBQUNBO0FBQ0E7QUFwQkEsR0FySkE7QUE0S0E7QUFDQSxXQURBLHFCQUNBO0FBQUE7O0FBQ0E7QUFDQSw2QkFEQTtBQUVBO0FBRkE7QUFJQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLFNBRkE7QUFHQTtBQUNBLEtBZEE7QUFlQSxlQWZBLHVCQWVBLElBZkEsRUFlQTtBQUFBO0FBQUE7O0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBQ0E7O0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUpBLE1BSUE7QUFDQTtBQUNBO0FBQ0EsT0FiQSxDQWVBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0E7QUFDQTtBQXpDQTtBQTVLQSxHOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztDQ05BO0FBQ0E7QUFFQTtBQUNBOztBQUNBLFNBQVNVLE1BQVQsQ0FBZ0JkO0FBQUk7QUFBcEIsRUFBa0Q7QUFDaEQsTUFBSWUsT0FBTyxHQUFHN0MsS0FBSyxDQUFDZ0MsU0FBTixDQUFnQlEsS0FBaEIsQ0FBc0JELElBQXRCLENBQTJCTyxTQUEzQixFQUFzQyxDQUF0QyxDQUFkO0FBRUFELFNBQU8sQ0FBQ0UsT0FBUixDQUFnQixVQUFVQyxNQUFWLEVBQWtCO0FBQ2hDLFFBQUksQ0FBQ0EsTUFBTCxFQUFhO0FBQUU7QUFBUzs7QUFFeEJ4QyxVQUFNLENBQUN5QyxJQUFQLENBQVlELE1BQVosRUFBb0JELE9BQXBCLENBQTRCLFVBQVVHLEdBQVYsRUFBZTtBQUN6Q3BCLFNBQUcsQ0FBQ29CLEdBQUQsQ0FBSCxHQUFXRixNQUFNLENBQUNFLEdBQUQsQ0FBakI7QUFDRCxLQUZEO0FBR0QsR0FORDtBQVFBLFNBQU9wQixHQUFQO0FBQ0Q7O0FBRUQsU0FBU3FCLE1BQVQsQ0FBZ0JyQixHQUFoQixFQUFxQjtBQUFFLFNBQU90QixNQUFNLENBQUN3QixTQUFQLENBQWlCTSxRQUFqQixDQUEwQkMsSUFBMUIsQ0FBK0JULEdBQS9CLENBQVA7QUFBNkM7O0FBQ3BFLFNBQVNzQixRQUFULENBQWtCdEIsR0FBbEIsRUFBdUI7QUFBRSxTQUFPcUIsTUFBTSxDQUFDckIsR0FBRCxDQUFOLEtBQWdCLGlCQUF2QjtBQUEyQzs7QUFDcEUsU0FBU3VCLFFBQVQsQ0FBa0J2QixHQUFsQixFQUF1QjtBQUFFLFNBQU9xQixNQUFNLENBQUNyQixHQUFELENBQU4sS0FBZ0IsaUJBQXZCO0FBQTJDOztBQUNwRSxTQUFTd0IsUUFBVCxDQUFrQnhCLEdBQWxCLEVBQXVCO0FBQUUsU0FBT3FCLE1BQU0sQ0FBQ3JCLEdBQUQsQ0FBTixLQUFnQixpQkFBdkI7QUFBMkM7O0FBQ3BFLFNBQVN5QixVQUFULENBQW9CekIsR0FBcEIsRUFBeUI7QUFBRSxTQUFPcUIsTUFBTSxDQUFDckIsR0FBRCxDQUFOLEtBQWdCLG1CQUF2QjtBQUE2Qzs7QUFHeEUsU0FBUzBCLFFBQVQsQ0FBa0JDLEdBQWxCLEVBQXVCO0FBQUUsU0FBT0EsR0FBRyxDQUFDQyxPQUFKLENBQVksc0JBQVosRUFBb0MsTUFBcEMsQ0FBUDtBQUFxRCxDLENBRTlFOzs7QUFHQSxJQUFJQyxjQUFjLEdBQUc7QUFDbkJDLFdBQVMsRUFBRSxJQURRO0FBRW5CQyxZQUFVLEVBQUUsSUFGTztBQUduQkMsU0FBTyxFQUFFO0FBSFUsQ0FBckI7O0FBT0EsU0FBU0MsWUFBVCxDQUFzQmpDLEdBQXRCLEVBQTJCO0FBQ3pCLFNBQU90QixNQUFNLENBQUN5QyxJQUFQLENBQVluQixHQUFHLElBQUksRUFBbkIsRUFBdUJrQyxNQUF2QixDQUE4QixVQUFVQyxHQUFWLEVBQWVDLENBQWYsRUFBa0I7QUFDckQsV0FBT0QsR0FBRyxJQUFJTixjQUFjLENBQUNRLGNBQWYsQ0FBOEJELENBQTlCLENBQWQ7QUFDRCxHQUZNLEVBRUosS0FGSSxDQUFQO0FBR0Q7O0FBR0QsSUFBSUUsY0FBYyxHQUFHO0FBQ25CLFdBQVM7QUFDUEMsWUFBUSxFQUFFLGtCQUFVQyxJQUFWLEVBQWdCQyxHQUFoQixFQUFxQkMsSUFBckIsRUFBMkI7QUFDbkMsVUFBSUMsSUFBSSxHQUFHSCxJQUFJLENBQUM5QixLQUFMLENBQVcrQixHQUFYLENBQVg7O0FBRUEsVUFBSSxDQUFDQyxJQUFJLENBQUNFLEVBQUwsQ0FBUUMsSUFBYixFQUFtQjtBQUNqQjtBQUNBSCxZQUFJLENBQUNFLEVBQUwsQ0FBUUMsSUFBUixHQUFnQixJQUFJQyxNQUFKLENBQ2QsWUFBWUosSUFBSSxDQUFDRSxFQUFMLENBQVFHLFFBQXBCLEdBQStCTCxJQUFJLENBQUNFLEVBQUwsQ0FBUUksb0JBQXZDLEdBQThETixJQUFJLENBQUNFLEVBQUwsQ0FBUUssUUFEeEQsRUFDa0UsR0FEbEUsQ0FBaEI7QUFHRDs7QUFDRCxVQUFJUCxJQUFJLENBQUNFLEVBQUwsQ0FBUUMsSUFBUixDQUFhaEMsSUFBYixDQUFrQjhCLElBQWxCLENBQUosRUFBNkI7QUFDM0IsZUFBT0EsSUFBSSxDQUFDTyxLQUFMLENBQVdSLElBQUksQ0FBQ0UsRUFBTCxDQUFRQyxJQUFuQixFQUF5QixDQUF6QixFQUE0QjlFLE1BQW5DO0FBQ0Q7O0FBQ0QsYUFBTyxDQUFQO0FBQ0Q7QUFkTSxHQURVO0FBaUJuQixZQUFXLE9BakJRO0FBa0JuQixVQUFXLE9BbEJRO0FBbUJuQixRQUFXO0FBQ1R3RSxZQUFRLEVBQUUsa0JBQVVDLElBQVYsRUFBZ0JDLEdBQWhCLEVBQXFCQyxJQUFyQixFQUEyQjtBQUNuQyxVQUFJQyxJQUFJLEdBQUdILElBQUksQ0FBQzlCLEtBQUwsQ0FBVytCLEdBQVgsQ0FBWDs7QUFFQSxVQUFJLENBQUNDLElBQUksQ0FBQ0UsRUFBTCxDQUFRTyxPQUFiLEVBQXNCO0FBQ3RCO0FBQ0VULFlBQUksQ0FBQ0UsRUFBTCxDQUFRTyxPQUFSLEdBQW1CLElBQUlMLE1BQUosQ0FDakIsTUFDQUosSUFBSSxDQUFDRSxFQUFMLENBQVFHLFFBRFIsR0FFQTtBQUNBO0FBQ0EsNkJBSkEsR0FJd0JMLElBQUksQ0FBQ0UsRUFBTCxDQUFRUSxVQUpoQyxHQUk2QyxRQUo3QyxHQUl3RFYsSUFBSSxDQUFDRSxFQUFMLENBQVFTLGVBSmhFLEdBSWtGLEdBSmxGLEdBS0FYLElBQUksQ0FBQ0UsRUFBTCxDQUFRVSxRQUxSLEdBTUFaLElBQUksQ0FBQ0UsRUFBTCxDQUFRVyxtQkFOUixHQU9BYixJQUFJLENBQUNFLEVBQUwsQ0FBUUssUUFSUyxFQVVqQixHQVZpQixDQUFuQjtBQVlEOztBQUVELFVBQUlQLElBQUksQ0FBQ0UsRUFBTCxDQUFRTyxPQUFSLENBQWdCdEMsSUFBaEIsQ0FBcUI4QixJQUFyQixDQUFKLEVBQWdDO0FBQzlCO0FBQ0EsWUFBSUYsR0FBRyxJQUFJLENBQVAsSUFBWUQsSUFBSSxDQUFDQyxHQUFHLEdBQUcsQ0FBUCxDQUFKLEtBQWtCLEdBQWxDLEVBQXVDO0FBQUUsaUJBQU8sQ0FBUDtBQUFXOztBQUNwRCxZQUFJQSxHQUFHLElBQUksQ0FBUCxJQUFZRCxJQUFJLENBQUNDLEdBQUcsR0FBRyxDQUFQLENBQUosS0FBa0IsR0FBbEMsRUFBdUM7QUFBRSxpQkFBTyxDQUFQO0FBQVc7O0FBQ3BELGVBQU9FLElBQUksQ0FBQ08sS0FBTCxDQUFXUixJQUFJLENBQUNFLEVBQUwsQ0FBUU8sT0FBbkIsRUFBNEIsQ0FBNUIsRUFBK0JwRixNQUF0QztBQUNEOztBQUNELGFBQU8sQ0FBUDtBQUNEO0FBM0JRLEdBbkJRO0FBZ0RuQixhQUFXO0FBQ1R3RSxZQUFRLEVBQUUsa0JBQVVDLElBQVYsRUFBZ0JDLEdBQWhCLEVBQXFCQyxJQUFyQixFQUEyQjtBQUNuQyxVQUFJQyxJQUFJLEdBQUdILElBQUksQ0FBQzlCLEtBQUwsQ0FBVytCLEdBQVgsQ0FBWDs7QUFFQSxVQUFJLENBQUNDLElBQUksQ0FBQ0UsRUFBTCxDQUFRWSxNQUFiLEVBQXFCO0FBQ25CZCxZQUFJLENBQUNFLEVBQUwsQ0FBUVksTUFBUixHQUFrQixJQUFJVixNQUFKLENBQ2hCLE1BQU1KLElBQUksQ0FBQ0UsRUFBTCxDQUFRYSxjQUFkLEdBQStCLEdBQS9CLEdBQXFDZixJQUFJLENBQUNFLEVBQUwsQ0FBUWMsZUFEN0IsRUFDOEMsR0FEOUMsQ0FBbEI7QUFHRDs7QUFDRCxVQUFJaEIsSUFBSSxDQUFDRSxFQUFMLENBQVFZLE1BQVIsQ0FBZTNDLElBQWYsQ0FBb0I4QixJQUFwQixDQUFKLEVBQStCO0FBQzdCLGVBQU9BLElBQUksQ0FBQ08sS0FBTCxDQUFXUixJQUFJLENBQUNFLEVBQUwsQ0FBUVksTUFBbkIsRUFBMkIsQ0FBM0IsRUFBOEJ6RixNQUFyQztBQUNEOztBQUNELGFBQU8sQ0FBUDtBQUNEO0FBYlE7QUFoRFEsQ0FBckI7QUFpRUE7QUFFQTs7QUFDQSxJQUFJNEYsZUFBZSxHQUFHLHlWQUF0QixDLENBRUE7O0FBQ0EsSUFBSUMsWUFBWSxHQUFHLDhFQUE4RUMsS0FBOUUsQ0FBb0YsR0FBcEYsQ0FBbkI7QUFFQTtBQUVBOztBQUVBLFNBQVNDLGNBQVQsQ0FBd0JwQixJQUF4QixFQUE4QjtBQUM1QkEsTUFBSSxDQUFDcUIsU0FBTCxHQUFpQixDQUFDLENBQWxCO0FBQ0FyQixNQUFJLENBQUNzQixjQUFMLEdBQXdCLEVBQXhCO0FBQ0Q7O0FBRUQsU0FBU0MsZUFBVCxDQUF5QnJCLEVBQXpCLEVBQTZCO0FBQzNCLFNBQU8sVUFBVUosSUFBVixFQUFnQkMsR0FBaEIsRUFBcUI7QUFDMUIsUUFBSUUsSUFBSSxHQUFHSCxJQUFJLENBQUM5QixLQUFMLENBQVcrQixHQUFYLENBQVg7O0FBRUEsUUFBSUcsRUFBRSxDQUFDL0IsSUFBSCxDQUFROEIsSUFBUixDQUFKLEVBQW1CO0FBQ2pCLGFBQU9BLElBQUksQ0FBQ08sS0FBTCxDQUFXTixFQUFYLEVBQWUsQ0FBZixFQUFrQjdFLE1BQXpCO0FBQ0Q7O0FBQ0QsV0FBTyxDQUFQO0FBQ0QsR0FQRDtBQVFEOztBQUVELFNBQVNtRyxnQkFBVCxHQUE0QjtBQUMxQixTQUFPLFVBQVVoQixLQUFWLEVBQWlCUixJQUFqQixFQUF1QjtBQUM1QkEsUUFBSSxDQUFDeUIsU0FBTCxDQUFlakIsS0FBZjtBQUNELEdBRkQ7QUFHRCxDLENBRUQ7QUFDQTs7O0FBQ0EsU0FBU2tCLE9BQVQsQ0FBaUIxQixJQUFqQixFQUF1QjtBQUVyQjtBQUNBLE1BQUlFLEVBQUUsR0FBR0YsSUFBSSxDQUFDRSxFQUFMLEdBQVVsRCxtQkFBTyxDQUFDLHlEQUFELENBQVAsQ0FBb0JnRCxJQUFJLENBQUMyQixRQUF6QixDQUFuQixDQUhxQixDQUtyQjs7O0FBQ0EsTUFBSUMsSUFBSSxHQUFHNUIsSUFBSSxDQUFDNkIsUUFBTCxDQUFjN0QsS0FBZCxFQUFYOztBQUVBZ0MsTUFBSSxDQUFDOEIsU0FBTDs7QUFFQSxNQUFJLENBQUM5QixJQUFJLENBQUMrQixpQkFBVixFQUE2QjtBQUMzQkgsUUFBSSxDQUFDbEYsSUFBTCxDQUFVdUUsZUFBVjtBQUNEOztBQUNEVyxNQUFJLENBQUNsRixJQUFMLENBQVV3RCxFQUFFLENBQUM4QixNQUFiO0FBRUE5QixJQUFFLENBQUMrQixRQUFILEdBQWNMLElBQUksQ0FBQ00sSUFBTCxDQUFVLEdBQVYsQ0FBZDs7QUFFQSxXQUFTQyxLQUFULENBQWVDLEdBQWYsRUFBb0I7QUFBRSxXQUFPQSxHQUFHLENBQUNsRCxPQUFKLENBQVksUUFBWixFQUFzQmdCLEVBQUUsQ0FBQytCLFFBQXpCLENBQVA7QUFBNEM7O0FBRWxFL0IsSUFBRSxDQUFDbUMsV0FBSCxHQUFzQmpDLE1BQU0sQ0FBQytCLEtBQUssQ0FBQ2pDLEVBQUUsQ0FBQ29DLGVBQUosQ0FBTixFQUE0QixHQUE1QixDQUE1QjtBQUNBcEMsSUFBRSxDQUFDcUMsVUFBSCxHQUFzQm5DLE1BQU0sQ0FBQytCLEtBQUssQ0FBQ2pDLEVBQUUsQ0FBQ3NDLGNBQUosQ0FBTixFQUEyQixHQUEzQixDQUE1QjtBQUNBdEMsSUFBRSxDQUFDdUMsZ0JBQUgsR0FBc0JyQyxNQUFNLENBQUMrQixLQUFLLENBQUNqQyxFQUFFLENBQUN3QyxvQkFBSixDQUFOLEVBQWlDLEdBQWpDLENBQTVCO0FBQ0F4QyxJQUFFLENBQUN5QyxlQUFILEdBQXNCdkMsTUFBTSxDQUFDK0IsS0FBSyxDQUFDakMsRUFBRSxDQUFDMEMsbUJBQUosQ0FBTixFQUFnQyxHQUFoQyxDQUE1QixDQXRCcUIsQ0F3QnJCO0FBQ0E7QUFDQTs7QUFFQSxNQUFJQyxPQUFPLEdBQUcsRUFBZDtBQUVBN0MsTUFBSSxDQUFDOEMsWUFBTCxHQUFvQixFQUFwQixDQTlCcUIsQ0E4Qkc7O0FBRXhCLFdBQVNDLFdBQVQsQ0FBcUI5RSxJQUFyQixFQUEyQitFLEdBQTNCLEVBQWdDO0FBQzlCLFVBQU0sSUFBSUMsS0FBSixDQUFVLGlDQUFpQ2hGLElBQWpDLEdBQXdDLEtBQXhDLEdBQWdEK0UsR0FBMUQsQ0FBTjtBQUNEOztBQUVEaEgsUUFBTSxDQUFDeUMsSUFBUCxDQUFZdUIsSUFBSSxDQUFDa0QsV0FBakIsRUFBOEIzRSxPQUE5QixDQUFzQyxVQUFVTixJQUFWLEVBQWdCO0FBQ3BELFFBQUkrRSxHQUFHLEdBQUdoRCxJQUFJLENBQUNrRCxXQUFMLENBQWlCakYsSUFBakIsQ0FBVixDQURvRCxDQUdwRDs7QUFDQSxRQUFJK0UsR0FBRyxLQUFLLElBQVosRUFBa0I7QUFBRTtBQUFTOztBQUU3QixRQUFJRyxRQUFRLEdBQUc7QUFBRXRELGNBQVEsRUFBRSxJQUFaO0FBQWtCdUQsVUFBSSxFQUFFO0FBQXhCLEtBQWY7QUFFQXBELFFBQUksQ0FBQzhDLFlBQUwsQ0FBa0I3RSxJQUFsQixJQUEwQmtGLFFBQTFCOztBQUVBLFFBQUl0RSxRQUFRLENBQUNtRSxHQUFELENBQVosRUFBbUI7QUFDakIsVUFBSWxFLFFBQVEsQ0FBQ2tFLEdBQUcsQ0FBQ25ELFFBQUwsQ0FBWixFQUE0QjtBQUMxQnNELGdCQUFRLENBQUN0RCxRQUFULEdBQW9CMEIsZUFBZSxDQUFDeUIsR0FBRyxDQUFDbkQsUUFBTCxDQUFuQztBQUNELE9BRkQsTUFFTyxJQUFJZCxVQUFVLENBQUNpRSxHQUFHLENBQUNuRCxRQUFMLENBQWQsRUFBOEI7QUFDbkNzRCxnQkFBUSxDQUFDdEQsUUFBVCxHQUFvQm1ELEdBQUcsQ0FBQ25ELFFBQXhCO0FBQ0QsT0FGTSxNQUVBO0FBQ0xrRCxtQkFBVyxDQUFDOUUsSUFBRCxFQUFPK0UsR0FBUCxDQUFYO0FBQ0Q7O0FBRUQsVUFBSWpFLFVBQVUsQ0FBQ2lFLEdBQUcsQ0FBQ3ZCLFNBQUwsQ0FBZCxFQUErQjtBQUM3QjBCLGdCQUFRLENBQUMxQixTQUFULEdBQXFCdUIsR0FBRyxDQUFDdkIsU0FBekI7QUFDRCxPQUZELE1BRU8sSUFBSSxDQUFDdUIsR0FBRyxDQUFDdkIsU0FBVCxFQUFvQjtBQUN6QjBCLGdCQUFRLENBQUMxQixTQUFULEdBQXFCRCxnQkFBZ0IsRUFBckM7QUFDRCxPQUZNLE1BRUE7QUFDTHVCLG1CQUFXLENBQUM5RSxJQUFELEVBQU8rRSxHQUFQLENBQVg7QUFDRDs7QUFFRDtBQUNEOztBQUVELFFBQUlwRSxRQUFRLENBQUNvRSxHQUFELENBQVosRUFBbUI7QUFDakJILGFBQU8sQ0FBQ25HLElBQVIsQ0FBYXVCLElBQWI7QUFDQTtBQUNEOztBQUVEOEUsZUFBVyxDQUFDOUUsSUFBRCxFQUFPK0UsR0FBUCxDQUFYO0FBQ0QsR0FwQ0QsRUFwQ3FCLENBMEVyQjtBQUNBO0FBQ0E7O0FBRUFILFNBQU8sQ0FBQ3RFLE9BQVIsQ0FBZ0IsVUFBVThFLEtBQVYsRUFBaUI7QUFDL0IsUUFBSSxDQUFDckQsSUFBSSxDQUFDOEMsWUFBTCxDQUFrQjlDLElBQUksQ0FBQ2tELFdBQUwsQ0FBaUJHLEtBQWpCLENBQWxCLENBQUwsRUFBaUQ7QUFDL0M7QUFDQTtBQUNBO0FBQ0Q7O0FBRURyRCxRQUFJLENBQUM4QyxZQUFMLENBQWtCTyxLQUFsQixFQUF5QnhELFFBQXpCLEdBQ0VHLElBQUksQ0FBQzhDLFlBQUwsQ0FBa0I5QyxJQUFJLENBQUNrRCxXQUFMLENBQWlCRyxLQUFqQixDQUFsQixFQUEyQ3hELFFBRDdDO0FBRUFHLFFBQUksQ0FBQzhDLFlBQUwsQ0FBa0JPLEtBQWxCLEVBQXlCNUIsU0FBekIsR0FDRXpCLElBQUksQ0FBQzhDLFlBQUwsQ0FBa0I5QyxJQUFJLENBQUNrRCxXQUFMLENBQWlCRyxLQUFqQixDQUFsQixFQUEyQzVCLFNBRDdDO0FBRUQsR0FYRCxFQTlFcUIsQ0EyRnJCO0FBQ0E7QUFDQTs7QUFDQXpCLE1BQUksQ0FBQzhDLFlBQUwsQ0FBa0IsRUFBbEIsSUFBd0I7QUFBRWpELFlBQVEsRUFBRSxJQUFaO0FBQWtCNEIsYUFBUyxFQUFFRCxnQkFBZ0I7QUFBN0MsR0FBeEIsQ0E5RnFCLENBZ0dyQjtBQUNBO0FBQ0E7O0FBQ0EsTUFBSThCLEtBQUssR0FBR3RILE1BQU0sQ0FBQ3lDLElBQVAsQ0FBWXVCLElBQUksQ0FBQzhDLFlBQWpCLEVBQ1NTLE1BRFQsQ0FDZ0IsVUFBVXRGLElBQVYsRUFBZ0I7QUFDdEI7QUFDQSxXQUFPQSxJQUFJLENBQUM1QyxNQUFMLEdBQWMsQ0FBZCxJQUFtQjJFLElBQUksQ0FBQzhDLFlBQUwsQ0FBa0I3RSxJQUFsQixDQUExQjtBQUNELEdBSlQsRUFLU3VGLEdBTFQsQ0FLYXhFLFFBTGIsRUFNU2tELElBTlQsQ0FNYyxHQU5kLENBQVosQ0FuR3FCLENBMEdyQjs7QUFDQWxDLE1BQUksQ0FBQ0UsRUFBTCxDQUFRdUQsV0FBUixHQUF3QnJELE1BQU0sQ0FBQywyQkFBMkJGLEVBQUUsQ0FBQ3dELFFBQTlCLEdBQXlDLEtBQXpDLEdBQWlESixLQUFqRCxHQUF5RCxHQUExRCxFQUErRCxHQUEvRCxDQUE5QjtBQUNBdEQsTUFBSSxDQUFDRSxFQUFMLENBQVF5RCxhQUFSLEdBQXdCdkQsTUFBTSxDQUFDLDJCQUEyQkYsRUFBRSxDQUFDd0QsUUFBOUIsR0FBeUMsS0FBekMsR0FBaURKLEtBQWpELEdBQXlELEdBQTFELEVBQStELElBQS9ELENBQTlCO0FBRUF0RCxNQUFJLENBQUNFLEVBQUwsQ0FBUTBELE9BQVIsR0FBa0J4RCxNQUFNLENBQ3RCLE1BQU1KLElBQUksQ0FBQ0UsRUFBTCxDQUFRdUQsV0FBUixDQUFvQmpGLE1BQTFCLEdBQW1DLEtBQW5DLEdBQTJDd0IsSUFBSSxDQUFDRSxFQUFMLENBQVF5QyxlQUFSLENBQXdCbkUsTUFBbkUsR0FBNEUsS0FEdEQsRUFFdEIsR0FGc0IsQ0FBeEIsQ0E5R3FCLENBbUhyQjtBQUNBO0FBQ0E7O0FBRUE0QyxnQkFBYyxDQUFDcEIsSUFBRCxDQUFkO0FBQ0Q7QUFFRDs7Ozs7OztBQUtBLFNBQVM2RCxLQUFULENBQWU3RCxJQUFmLEVBQXFCOEQsS0FBckIsRUFBNEI7QUFDMUIsTUFBSUMsS0FBSyxHQUFHL0QsSUFBSSxDQUFDcUIsU0FBakI7QUFBQSxNQUNJMkMsR0FBRyxHQUFLaEUsSUFBSSxDQUFDaUUsY0FEakI7QUFBQSxNQUVJbkUsSUFBSSxHQUFJRSxJQUFJLENBQUNzQixjQUFMLENBQW9CdEQsS0FBcEIsQ0FBMEIrRixLQUExQixFQUFpQ0MsR0FBakMsQ0FGWjtBQUlBOzs7Ozs7O0FBS0EsT0FBS0UsTUFBTCxHQUFpQmxFLElBQUksQ0FBQ21FLFVBQUwsQ0FBZ0JDLFdBQWhCLEVBQWpCO0FBQ0E7Ozs7OztBQUtBLE9BQUtDLEtBQUwsR0FBaUJOLEtBQUssR0FBR0QsS0FBekI7QUFDQTs7Ozs7O0FBS0EsT0FBS1EsU0FBTCxHQUFpQk4sR0FBRyxHQUFHRixLQUF2QjtBQUNBOzs7Ozs7QUFLQSxPQUFLUyxHQUFMLEdBQWlCekUsSUFBakI7QUFDQTs7Ozs7O0FBS0EsT0FBS0EsSUFBTCxHQUFpQkEsSUFBakI7QUFDQTs7Ozs7O0FBS0EsT0FBSzBFLEdBQUwsR0FBaUIxRSxJQUFqQjtBQUNEOztBQUVELFNBQVMyRSxXQUFULENBQXFCekUsSUFBckIsRUFBMkI4RCxLQUEzQixFQUFrQztBQUNoQyxNQUFJdEQsS0FBSyxHQUFHLElBQUlxRCxLQUFKLENBQVU3RCxJQUFWLEVBQWdCOEQsS0FBaEIsQ0FBWjs7QUFFQTlELE1BQUksQ0FBQzhDLFlBQUwsQ0FBa0J0QyxLQUFLLENBQUMwRCxNQUF4QixFQUFnQ3pDLFNBQWhDLENBQTBDakIsS0FBMUMsRUFBaURSLElBQWpEOztBQUVBLFNBQU9RLEtBQVA7QUFDRDtBQUdEOzs7O0FBSUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWtDQSxTQUFTa0UsU0FBVCxDQUFtQkMsT0FBbkIsRUFBNEJDLE9BQTVCLEVBQXFDO0FBQ25DLE1BQUksRUFBRSxnQkFBZ0JGLFNBQWxCLENBQUosRUFBa0M7QUFDaEMsV0FBTyxJQUFJQSxTQUFKLENBQWNDLE9BQWQsRUFBdUJDLE9BQXZCLENBQVA7QUFDRDs7QUFFRCxNQUFJLENBQUNBLE9BQUwsRUFBYztBQUNaLFFBQUlyRixZQUFZLENBQUNvRixPQUFELENBQWhCLEVBQTJCO0FBQ3pCQyxhQUFPLEdBQUdELE9BQVY7QUFDQUEsYUFBTyxHQUFHLEVBQVY7QUFDRDtBQUNGOztBQUVELE9BQUtoRCxRQUFMLEdBQTBCdkQsTUFBTSxDQUFDLEVBQUQsRUFBS2UsY0FBTCxFQUFxQnlGLE9BQXJCLENBQWhDLENBWm1DLENBY25DOztBQUNBLE9BQUt2RCxTQUFMLEdBQTBCLENBQUMsQ0FBM0I7QUFDQSxPQUFLNEMsY0FBTCxHQUEwQixDQUFDLENBQTNCLENBaEJtQyxDQWdCTDs7QUFDOUIsT0FBS0UsVUFBTCxHQUEwQixFQUExQjtBQUNBLE9BQUs3QyxjQUFMLEdBQTBCLEVBQTFCO0FBRUEsT0FBSzRCLFdBQUwsR0FBMEI5RSxNQUFNLENBQUMsRUFBRCxFQUFLd0IsY0FBTCxFQUFxQitFLE9BQXJCLENBQWhDO0FBQ0EsT0FBSzdCLFlBQUwsR0FBMEIsRUFBMUI7QUFFQSxPQUFLakIsUUFBTCxHQUEwQlgsWUFBMUI7QUFDQSxPQUFLYSxpQkFBTCxHQUEwQixLQUExQjtBQUVBLE9BQUs3QixFQUFMLEdBQVUsRUFBVjtBQUVBd0IsU0FBTyxDQUFDLElBQUQsQ0FBUDtBQUNEO0FBR0Q7Ozs7Ozs7OztBQU9BZ0QsU0FBUyxDQUFDbEgsU0FBVixDQUFvQnFILEdBQXBCLEdBQTBCLFNBQVNBLEdBQVQsQ0FBYVgsTUFBYixFQUFxQlksVUFBckIsRUFBaUM7QUFDekQsT0FBSzVCLFdBQUwsQ0FBaUJnQixNQUFqQixJQUEyQlksVUFBM0I7QUFDQXBELFNBQU8sQ0FBQyxJQUFELENBQVA7QUFDQSxTQUFPLElBQVA7QUFDRCxDQUpEO0FBT0E7Ozs7Ozs7O0FBTUFnRCxTQUFTLENBQUNsSCxTQUFWLENBQW9CdUgsR0FBcEIsR0FBMEIsU0FBU0EsR0FBVCxDQUFhSCxPQUFiLEVBQXNCO0FBQzlDLE9BQUtqRCxRQUFMLEdBQWdCdkQsTUFBTSxDQUFDLEtBQUt1RCxRQUFOLEVBQWdCaUQsT0FBaEIsQ0FBdEI7QUFDQSxTQUFPLElBQVA7QUFDRCxDQUhEO0FBTUE7Ozs7Ozs7QUFLQUYsU0FBUyxDQUFDbEgsU0FBVixDQUFvQlcsSUFBcEIsR0FBMkIsU0FBU0EsSUFBVCxDQUFjMkIsSUFBZCxFQUFvQjtBQUM3QztBQUNBLE9BQUt3QixjQUFMLEdBQXNCeEIsSUFBdEI7QUFDQSxPQUFLdUIsU0FBTCxHQUFzQixDQUFDLENBQXZCOztBQUVBLE1BQUksQ0FBQ3ZCLElBQUksQ0FBQ3pFLE1BQVYsRUFBa0I7QUFBRSxXQUFPLEtBQVA7QUFBZTs7QUFFbkMsTUFBSTJKLENBQUosRUFBT0MsRUFBUCxFQUFXQyxFQUFYLEVBQWU5SixHQUFmLEVBQW9CMEksS0FBcEIsRUFBMkJ0SCxJQUEzQixFQUFpQzBELEVBQWpDLEVBQXFDaUYsT0FBckMsRUFBOENDLE1BQTlDLENBUDZDLENBUzdDOztBQUNBLE1BQUksS0FBS2xGLEVBQUwsQ0FBUXVELFdBQVIsQ0FBb0J0RixJQUFwQixDQUF5QjJCLElBQXpCLENBQUosRUFBb0M7QUFDbENJLE1BQUUsR0FBRyxLQUFLQSxFQUFMLENBQVF5RCxhQUFiO0FBQ0F6RCxNQUFFLENBQUNvRSxTQUFILEdBQWUsQ0FBZjs7QUFDQSxXQUFPLENBQUNVLENBQUMsR0FBRzlFLEVBQUUsQ0FBQ21GLElBQUgsQ0FBUXZGLElBQVIsQ0FBTCxNQUF3QixJQUEvQixFQUFxQztBQUNuQzFFLFNBQUcsR0FBRyxLQUFLa0ssWUFBTCxDQUFrQnhGLElBQWxCLEVBQXdCa0YsQ0FBQyxDQUFDLENBQUQsQ0FBekIsRUFBOEI5RSxFQUFFLENBQUNvRSxTQUFqQyxDQUFOOztBQUNBLFVBQUlsSixHQUFKLEVBQVM7QUFDUCxhQUFLK0ksVUFBTCxHQUFzQmEsQ0FBQyxDQUFDLENBQUQsQ0FBdkI7QUFDQSxhQUFLM0QsU0FBTCxHQUFzQjJELENBQUMsQ0FBQ1gsS0FBRixHQUFVVyxDQUFDLENBQUMsQ0FBRCxDQUFELENBQUszSixNQUFyQztBQUNBLGFBQUs0SSxjQUFMLEdBQXNCZSxDQUFDLENBQUNYLEtBQUYsR0FBVVcsQ0FBQyxDQUFDLENBQUQsQ0FBRCxDQUFLM0osTUFBZixHQUF3QkQsR0FBOUM7QUFDQTtBQUNEO0FBQ0Y7QUFDRjs7QUFFRCxNQUFJLEtBQUt1RyxRQUFMLENBQWN2QyxTQUFkLElBQTJCLEtBQUswRCxZQUFMLENBQWtCLE9BQWxCLENBQS9CLEVBQTJEO0FBQ3pEO0FBQ0FxQyxXQUFPLEdBQUdyRixJQUFJLENBQUN5RixNQUFMLENBQVksS0FBS3JGLEVBQUwsQ0FBUXlDLGVBQXBCLENBQVY7O0FBQ0EsUUFBSXdDLE9BQU8sSUFBSSxDQUFmLEVBQWtCO0FBQ2hCO0FBQ0EsVUFBSSxLQUFLOUQsU0FBTCxHQUFpQixDQUFqQixJQUFzQjhELE9BQU8sR0FBRyxLQUFLOUQsU0FBekMsRUFBb0Q7QUFDbEQsWUFBSSxDQUFDNEQsRUFBRSxHQUFHbkYsSUFBSSxDQUFDVSxLQUFMLENBQVcsS0FBS21CLFFBQUwsQ0FBY3JDLE9BQWQsR0FBd0IsS0FBS1ksRUFBTCxDQUFRcUMsVUFBaEMsR0FBNkMsS0FBS3JDLEVBQUwsQ0FBUXVDLGdCQUFoRSxDQUFOLE1BQTZGLElBQWpHLEVBQXVHO0FBRXJHcUIsZUFBSyxHQUFHbUIsRUFBRSxDQUFDWixLQUFILEdBQVdZLEVBQUUsQ0FBQyxDQUFELENBQUYsQ0FBTTVKLE1BQXpCOztBQUVBLGNBQUksS0FBS2dHLFNBQUwsR0FBaUIsQ0FBakIsSUFBc0J5QyxLQUFLLEdBQUcsS0FBS3pDLFNBQXZDLEVBQWtEO0FBQ2hELGlCQUFLOEMsVUFBTCxHQUFzQixFQUF0QjtBQUNBLGlCQUFLOUMsU0FBTCxHQUFzQnlDLEtBQXRCO0FBQ0EsaUJBQUtHLGNBQUwsR0FBc0JnQixFQUFFLENBQUNaLEtBQUgsR0FBV1ksRUFBRSxDQUFDLENBQUQsQ0FBRixDQUFNNUosTUFBdkM7QUFDRDtBQUNGO0FBQ0Y7QUFDRjtBQUNGOztBQUVELE1BQUksS0FBS3NHLFFBQUwsQ0FBY3RDLFVBQWQsSUFBNEIsS0FBS3lELFlBQUwsQ0FBa0IsU0FBbEIsQ0FBaEMsRUFBOEQ7QUFDNUQ7QUFDQXNDLFVBQU0sR0FBR3RGLElBQUksQ0FBQzBGLE9BQUwsQ0FBYSxHQUFiLENBQVQ7O0FBQ0EsUUFBSUosTUFBTSxJQUFJLENBQWQsRUFBaUI7QUFDZjtBQUNBO0FBQ0EsVUFBSSxDQUFDRixFQUFFLEdBQUdwRixJQUFJLENBQUNVLEtBQUwsQ0FBVyxLQUFLTixFQUFMLENBQVFtQyxXQUFuQixDQUFOLE1BQTJDLElBQS9DLEVBQXFEO0FBRW5EeUIsYUFBSyxHQUFHb0IsRUFBRSxDQUFDYixLQUFILEdBQVdhLEVBQUUsQ0FBQyxDQUFELENBQUYsQ0FBTTdKLE1BQXpCO0FBQ0FtQixZQUFJLEdBQUkwSSxFQUFFLENBQUNiLEtBQUgsR0FBV2EsRUFBRSxDQUFDLENBQUQsQ0FBRixDQUFNN0osTUFBekI7O0FBRUEsWUFBSSxLQUFLZ0csU0FBTCxHQUFpQixDQUFqQixJQUFzQnlDLEtBQUssR0FBRyxLQUFLekMsU0FBbkMsSUFDQ3lDLEtBQUssS0FBSyxLQUFLekMsU0FBZixJQUE0QjdFLElBQUksR0FBRyxLQUFLeUgsY0FEN0MsRUFDOEQ7QUFDNUQsZUFBS0UsVUFBTCxHQUFzQixTQUF0QjtBQUNBLGVBQUs5QyxTQUFMLEdBQXNCeUMsS0FBdEI7QUFDQSxlQUFLRyxjQUFMLEdBQXNCekgsSUFBdEI7QUFDRDtBQUNGO0FBQ0Y7QUFDRjs7QUFFRCxTQUFPLEtBQUs2RSxTQUFMLElBQWtCLENBQXpCO0FBQ0QsQ0FsRUQ7QUFxRUE7Ozs7Ozs7OztBQU9BcUQsU0FBUyxDQUFDbEgsU0FBVixDQUFvQm9HLE9BQXBCLEdBQThCLFNBQVNBLE9BQVQsQ0FBaUI5RCxJQUFqQixFQUF1QjtBQUNuRCxTQUFPLEtBQUtJLEVBQUwsQ0FBUTBELE9BQVIsQ0FBZ0J6RixJQUFoQixDQUFxQjJCLElBQXJCLENBQVA7QUFDRCxDQUZEO0FBS0E7Ozs7Ozs7Ozs7O0FBU0E0RSxTQUFTLENBQUNsSCxTQUFWLENBQW9COEgsWUFBcEIsR0FBbUMsU0FBU0EsWUFBVCxDQUFzQnhGLElBQXRCLEVBQTRCb0UsTUFBNUIsRUFBb0NuRSxHQUFwQyxFQUF5QztBQUMxRTtBQUNBLE1BQUksQ0FBQyxLQUFLK0MsWUFBTCxDQUFrQm9CLE1BQU0sQ0FBQ0UsV0FBUCxFQUFsQixDQUFMLEVBQThDO0FBQzVDLFdBQU8sQ0FBUDtBQUNEOztBQUNELFNBQU8sS0FBS3RCLFlBQUwsQ0FBa0JvQixNQUFNLENBQUNFLFdBQVAsRUFBbEIsRUFBd0N2RSxRQUF4QyxDQUFpREMsSUFBakQsRUFBdURDLEdBQXZELEVBQTRELElBQTVELENBQVA7QUFDRCxDQU5EO0FBU0E7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWdCQTJFLFNBQVMsQ0FBQ2xILFNBQVYsQ0FBb0JnRCxLQUFwQixHQUE0QixTQUFTQSxLQUFULENBQWVWLElBQWYsRUFBcUI7QUFDL0MsTUFBSWdFLEtBQUssR0FBRyxDQUFaO0FBQUEsTUFBZTJCLE1BQU0sR0FBRyxFQUF4QixDQUQrQyxDQUcvQzs7QUFDQSxNQUFJLEtBQUtwRSxTQUFMLElBQWtCLENBQWxCLElBQXVCLEtBQUtDLGNBQUwsS0FBd0J4QixJQUFuRCxFQUF5RDtBQUN2RDJGLFVBQU0sQ0FBQy9JLElBQVAsQ0FBWStILFdBQVcsQ0FBQyxJQUFELEVBQU9YLEtBQVAsQ0FBdkI7QUFDQUEsU0FBSyxHQUFHLEtBQUtHLGNBQWI7QUFDRCxHQVA4QyxDQVMvQzs7O0FBQ0EsTUFBSWhFLElBQUksR0FBRzZELEtBQUssR0FBR2hFLElBQUksQ0FBQzlCLEtBQUwsQ0FBVzhGLEtBQVgsQ0FBSCxHQUF1QmhFLElBQXZDLENBVitDLENBWS9DOztBQUNBLFNBQU8sS0FBSzNCLElBQUwsQ0FBVThCLElBQVYsQ0FBUCxFQUF3QjtBQUN0QndGLFVBQU0sQ0FBQy9JLElBQVAsQ0FBWStILFdBQVcsQ0FBQyxJQUFELEVBQU9YLEtBQVAsQ0FBdkI7QUFFQTdELFFBQUksR0FBR0EsSUFBSSxDQUFDakMsS0FBTCxDQUFXLEtBQUtpRyxjQUFoQixDQUFQO0FBQ0FILFNBQUssSUFBSSxLQUFLRyxjQUFkO0FBQ0Q7O0FBRUQsTUFBSXdCLE1BQU0sQ0FBQ3BLLE1BQVgsRUFBbUI7QUFDakIsV0FBT29LLE1BQVA7QUFDRDs7QUFFRCxTQUFPLElBQVA7QUFDRCxDQXpCRDtBQTRCQTs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFlQWYsU0FBUyxDQUFDbEgsU0FBVixDQUFvQm9FLElBQXBCLEdBQTJCLFNBQVNBLElBQVQsQ0FBYzhELElBQWQsRUFBb0JDLE9BQXBCLEVBQTZCO0FBQ3RERCxNQUFJLEdBQUdsSyxLQUFLLENBQUNJLE9BQU4sQ0FBYzhKLElBQWQsSUFBc0JBLElBQXRCLEdBQTZCLENBQUVBLElBQUYsQ0FBcEM7O0FBRUEsTUFBSSxDQUFDQyxPQUFMLEVBQWM7QUFDWixTQUFLOUQsUUFBTCxHQUFnQjZELElBQUksQ0FBQzFILEtBQUwsRUFBaEI7QUFDQSxTQUFLK0QsaUJBQUwsR0FBeUIsSUFBekI7QUFDQUwsV0FBTyxDQUFDLElBQUQsQ0FBUDtBQUNBLFdBQU8sSUFBUDtBQUNEOztBQUVELE9BQUtHLFFBQUwsR0FBZ0IsS0FBS0EsUUFBTCxDQUFjK0QsTUFBZCxDQUFxQkYsSUFBckIsRUFDaUJHLElBRGpCLEdBRWlCdEMsTUFGakIsQ0FFd0IsVUFBVXVDLEVBQVYsRUFBY0MsR0FBZCxFQUFtQjVLLEdBQW5CLEVBQXdCO0FBQzlCLFdBQU8ySyxFQUFFLEtBQUszSyxHQUFHLENBQUM0SyxHQUFHLEdBQUcsQ0FBUCxDQUFqQjtBQUNELEdBSmpCLEVBS2lCQyxPQUxqQixFQUFoQjtBQU9BdEUsU0FBTyxDQUFDLElBQUQsQ0FBUDtBQUNBLFNBQU8sSUFBUDtBQUNELENBbkJEO0FBcUJBOzs7Ozs7O0FBS0FnRCxTQUFTLENBQUNsSCxTQUFWLENBQW9CaUUsU0FBcEIsR0FBZ0MsU0FBU0EsU0FBVCxDQUFtQmpCLEtBQW5CLEVBQTBCO0FBRXhEO0FBQ0E7QUFFQSxNQUFJLENBQUNBLEtBQUssQ0FBQzBELE1BQVgsRUFBbUI7QUFBRTFELFNBQUssQ0FBQ2dFLEdBQU4sR0FBWSxZQUFZaEUsS0FBSyxDQUFDZ0UsR0FBOUI7QUFBb0M7O0FBRXpELE1BQUloRSxLQUFLLENBQUMwRCxNQUFOLEtBQWlCLFNBQWpCLElBQThCLENBQUMsWUFBWS9GLElBQVosQ0FBaUJxQyxLQUFLLENBQUNnRSxHQUF2QixDQUFuQyxFQUFnRTtBQUM5RGhFLFNBQUssQ0FBQ2dFLEdBQU4sR0FBWSxZQUFZaEUsS0FBSyxDQUFDZ0UsR0FBOUI7QUFDRDtBQUNGLENBVkQ7QUFhQTs7Ozs7OztBQUtBRSxTQUFTLENBQUNsSCxTQUFWLENBQW9Cc0UsU0FBcEIsR0FBZ0MsU0FBU0EsU0FBVCxHQUFxQixDQUNwRCxDQUREOztBQUlBckcsTUFBTSxDQUFDQyxPQUFQLEdBQWlCZ0osU0FBakIsQzs7Ozs7Ozs7Ozs7O0FDM25CYTs7QUFHYmpKLE1BQU0sQ0FBQ0MsT0FBUCxHQUFpQixVQUFVdUssSUFBVixFQUFnQjtBQUMvQixNQUFJL0YsRUFBRSxHQUFHLEVBQVQsQ0FEK0IsQ0FHL0I7O0FBQ0FBLElBQUUsQ0FBQ2dHLE9BQUgsR0FBYWxKLG1CQUFPLENBQUMsMEZBQUQsQ0FBUCxDQUF5Q3dCLE1BQXREO0FBQ0EwQixJQUFFLENBQUNpRyxNQUFILEdBQWFuSixtQkFBTyxDQUFDLHdGQUFELENBQVAsQ0FBd0N3QixNQUFyRDtBQUNBMEIsSUFBRSxDQUFDa0csS0FBSCxHQUFhcEosbUJBQU8sQ0FBQyxzRkFBRCxDQUFQLENBQXVDd0IsTUFBcEQ7QUFDQTBCLElBQUUsQ0FBQ21HLEtBQUgsR0FBYXJKLG1CQUFPLENBQUMsc0ZBQUQsQ0FBUCxDQUF1Q3dCLE1BQXBELENBUCtCLENBUy9COztBQUNBMEIsSUFBRSxDQUFDd0QsUUFBSCxHQUFjLENBQUV4RCxFQUFFLENBQUNrRyxLQUFMLEVBQVlsRyxFQUFFLENBQUNtRyxLQUFmLEVBQXNCbkcsRUFBRSxDQUFDaUcsTUFBekIsRUFBa0NqRSxJQUFsQyxDQUF1QyxHQUF2QyxDQUFkLENBVitCLENBWS9COztBQUNBaEMsSUFBRSxDQUFDb0csT0FBSCxHQUFhLENBQUVwRyxFQUFFLENBQUNrRyxLQUFMLEVBQVlsRyxFQUFFLENBQUNpRyxNQUFmLEVBQXdCakUsSUFBeEIsQ0FBNkIsR0FBN0IsQ0FBYixDQWIrQixDQWUvQjtBQUNBOztBQUNBLE1BQUlxRSxlQUFlLEdBQUcsWUFBdEIsQ0FqQitCLENBbUIvQjtBQUNBO0FBQ0E7O0FBQ0FyRyxJQUFFLENBQUNzRyxpQkFBSCxHQUE2QixXQUFXRCxlQUFYLEdBQTZCLEdBQTdCLEdBQW1DckcsRUFBRSxDQUFDd0QsUUFBdEMsR0FBaUQsR0FBakQsR0FBdUR4RCxFQUFFLENBQUNnRyxPQUExRCxHQUFvRSxHQUFqRyxDQXRCK0IsQ0F1Qi9CO0FBQ0E7QUFFQTs7QUFFQWhHLElBQUUsQ0FBQ3VHLE9BQUgsR0FFRSx3RkFGRixDQTVCK0IsQ0FnQy9COztBQUNBdkcsSUFBRSxDQUFDRyxRQUFILEdBQWlCLGNBQWNILEVBQUUsQ0FBQ29HLE9BQWpCLEdBQTJCLHNCQUE1QztBQUVBcEcsSUFBRSxDQUFDVSxRQUFILEdBRUUsaUZBRkY7QUFJQVYsSUFBRSxDQUFDVyxtQkFBSCxHQUVFLFVBQVUwRixlQUFWLEdBQTRCLEdBQTVCLEdBQWtDckcsRUFBRSxDQUFDd0QsUUFBckMsR0FBZ0QsNEJBQWhELEdBQStFeEQsRUFBRSxDQUFDd0QsUUFBbEYsR0FBNkYsSUFGL0Y7QUFJQXhELElBQUUsQ0FBQ0ssUUFBSCxHQUVFLFFBQ0UsT0FERixHQUVJLEtBRkosR0FHTSxLQUhOLEdBR2NMLEVBQUUsQ0FBQ29HLE9BSGpCLEdBRzJCLEdBSDNCLEdBR2lDQyxlQUhqQyxHQUdtRCwwQkFIbkQsR0FJTSxXQUpOLEdBSW9CckcsRUFBRSxDQUFDb0csT0FKdkIsR0FJaUMsY0FKakMsR0FLTSxXQUxOLEdBS29CcEcsRUFBRSxDQUFDb0csT0FMdkIsR0FLaUMsY0FMakMsR0FNTSxXQU5OLEdBTW9CcEcsRUFBRSxDQUFDb0csT0FOdkIsR0FNaUMsY0FOakMsR0FPTSxXQVBOLEdBT29CcEcsRUFBRSxDQUFDb0csT0FQdkIsR0FPaUMsY0FQakMsR0FRTSxXQVJOLEdBUW9CcEcsRUFBRSxDQUFDb0csT0FSdkIsR0FRaUMsY0FSakMsR0FTTSxRQVROLEdBU2lCcEcsRUFBRSxDQUFDc0csaUJBVHBCLEdBU3dDLFNBVHhDLEdBU3FEO0FBQy9DLDBCQVZOLEdBVWlDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQzNCLFVBakJOLEdBaUJpQnRHLEVBQUUsQ0FBQ29HLE9BakJwQixHQWlCOEIsU0FqQjlCLElBa0JPTCxJQUFJLElBQUlBLElBQUksQ0FBQyxLQUFELENBQVosR0FDQyw0QkFERCxDQUM4QjtBQUQ5QixJQUdDLE9BckJSLElBdUJNLFFBdkJOLEdBdUJpQi9GLEVBQUUsQ0FBQ29HLE9BdkJwQixHQXVCOEIsS0F2QjlCLEdBdUIyQztBQUNyQyxVQXhCTixHQXdCaUJwRyxFQUFFLENBQUNvRyxPQXhCcEIsR0F3QjhCLFNBeEI5QixHQXlCTSxRQXpCTixHQXlCaUJwRyxFQUFFLENBQUNvRyxPQXpCcEIsR0F5QjhCLFFBekI5QixHQTBCSSxJQTFCSixHQTJCRSxNQTNCRixHQTRCQSxJQTlCRixDQTNDK0IsQ0EyRS9CO0FBQ0E7O0FBQ0FwRyxJQUFFLENBQUNhLGNBQUgsR0FFRSxnRUFGRjtBQUlBYixJQUFFLENBQUM4QixNQUFILEdBRUUsdUJBRkYsQ0FqRitCLENBcUYvQjtBQUNBOztBQUVBOUIsSUFBRSxDQUFDUyxlQUFILEdBRUU7QUFDQSxVQUNFVCxFQUFFLENBQUM4QixNQURMLEdBRUUsR0FGRixHQUdFOUIsRUFBRSxDQUFDc0csaUJBSEwsR0FHeUIsUUFIekIsR0FJQSxHQVBGO0FBU0F0RyxJQUFFLENBQUNRLFVBQUgsR0FFRSxRQUNFUixFQUFFLENBQUM4QixNQURMLEdBRUUsR0FGRixHQUdFLEtBSEYsR0FHVTlCLEVBQUUsQ0FBQ3NHLGlCQUhiLEdBR2lDLEdBSGpDLEdBSUUsR0FKRixHQUtFLEtBTEYsR0FLVXRHLEVBQUUsQ0FBQ3NHLGlCQUxiLEdBS2lDLE9BTGpDLEdBSzJDdEcsRUFBRSxDQUFDc0csaUJBTDlDLEdBS2tFLFNBTGxFLEdBSzhFdEcsRUFBRSxDQUFDc0csaUJBTGpGLEdBS3FHLEdBTHJHLEdBTUEsR0FSRjtBQVVBdEcsSUFBRSxDQUFDd0csUUFBSCxHQUVFLFFBQ0E7QUFDQTtBQUNBO0FBQ0UsYUFKRixHQUlnQnhHLEVBQUUsQ0FBQ1EsVUFKbkIsR0FJZ0MsUUFKaEMsR0FJMkNSLEVBQUUsQ0FBQ1E7QUFBVTtBQUp4RCxJQUlvRSxHQUpwRSxHQUtBLEdBUEY7QUFTQVIsSUFBRSxDQUFDeUcsY0FBSCxHQUVFLFFBQ0V6RyxFQUFFLENBQUN1RyxPQURMLEdBRUEsR0FGQSxHQUdFLFdBSEYsR0FHZ0J2RyxFQUFFLENBQUNRLFVBSG5CLEdBR2dDLG1CQUhoQyxHQUlBLEdBTkY7QUFRQVIsSUFBRSxDQUFDMEcsb0JBQUgsR0FFRSxjQUFjMUcsRUFBRSxDQUFDUSxVQUFqQixHQUE4QixtQkFGaEM7QUFJQVIsSUFBRSxDQUFDYyxlQUFILEdBRUVkLEVBQUUsQ0FBQ3dHLFFBQUgsR0FBY3hHLEVBQUUsQ0FBQ1csbUJBRm5CO0FBSUFYLElBQUUsQ0FBQzJHLHFCQUFILEdBRUUzRyxFQUFFLENBQUN5RyxjQUFILEdBQW9CekcsRUFBRSxDQUFDVyxtQkFGekI7QUFJQVgsSUFBRSxDQUFDSSxvQkFBSCxHQUVFSixFQUFFLENBQUN3RyxRQUFILEdBQWN4RyxFQUFFLENBQUNVLFFBQWpCLEdBQTRCVixFQUFFLENBQUNXLG1CQUZqQztBQUlBWCxJQUFFLENBQUM0RywwQkFBSCxHQUVFNUcsRUFBRSxDQUFDeUcsY0FBSCxHQUFvQnpHLEVBQUUsQ0FBQ1UsUUFBdkIsR0FBa0NWLEVBQUUsQ0FBQ1csbUJBRnZDO0FBSUFYLElBQUUsQ0FBQzZHLGdDQUFILEdBRUU3RyxFQUFFLENBQUMwRyxvQkFBSCxHQUEwQjFHLEVBQUUsQ0FBQ1UsUUFBN0IsR0FBd0NWLEVBQUUsQ0FBQ1csbUJBRjdDLENBaEorQixDQXFKL0I7QUFDQTtBQUVBOztBQUNBWCxJQUFFLENBQUMwQyxtQkFBSCxHQUVFLHdEQUF3RDFDLEVBQUUsQ0FBQ3dELFFBQTNELEdBQXNFLFFBRnhFO0FBSUF4RCxJQUFFLENBQUNvQyxlQUFILEdBRUksUUFBUWlFLGVBQVIsR0FBMEIsU0FBMUIsR0FBc0NyRyxFQUFFLENBQUNvRyxPQUF6QyxHQUFtRCxHQUFuRCxHQUNBLEdBREEsR0FDTXBHLEVBQUUsQ0FBQ2EsY0FEVCxHQUMwQixHQUQxQixHQUNnQ2IsRUFBRSxDQUFDMkcscUJBRG5DLEdBQzJELEdBSC9EO0FBS0EzRyxJQUFFLENBQUNzQyxjQUFILEdBQ0k7QUFDQTtBQUNBLDRDQUEwQ3RDLEVBQUUsQ0FBQ3dELFFBQTdDLEdBQXdELElBQXhELEdBQ0EsdUJBREEsR0FDMEJ4RCxFQUFFLENBQUM0RywwQkFEN0IsR0FDMEQ1RyxFQUFFLENBQUNLLFFBRDdELEdBQ3dFLEdBSjVFO0FBTUFMLElBQUUsQ0FBQ3dDLG9CQUFILEdBQ0k7QUFDQTtBQUNBLDRDQUEwQ3hDLEVBQUUsQ0FBQ3dELFFBQTdDLEdBQXdELElBQXhELEdBQ0EsdUJBREEsR0FDMEJ4RCxFQUFFLENBQUM2RyxnQ0FEN0IsR0FDZ0U3RyxFQUFFLENBQUNLLFFBRG5FLEdBQzhFLEdBSmxGO0FBTUEsU0FBT0wsRUFBUDtBQUNELENBL0tELEM7Ozs7Ozs7Ozs7OztBQ0hBO0FBQ0E7QUFDYTs7QUFHYnpFLE1BQU0sQ0FBQ0MsT0FBUCxHQUFpQixTQUFTc0wsZ0JBQVQsQ0FBMEJDLEVBQTFCLEVBQThCaEosSUFBOUIsRUFBb0MyRyxPQUFwQyxFQUE2QztBQUU1RCxXQUFTc0MsZUFBVCxDQUF5QkMsTUFBekIsRUFBaUM7QUFDL0IsV0FBT0EsTUFBTSxDQUFDQyxJQUFQLEdBQWNqRyxLQUFkLENBQW9CLEdBQXBCLEVBQXlCLENBQXpCLEVBQTRCLENBQTVCLE1BQW1DbEQsSUFBMUM7QUFDRDs7QUFFRCxXQUFTb0osYUFBVCxDQUF1QkMsTUFBdkIsRUFBK0J2QixHQUEvQixFQUFvQ3dCLFFBQXBDLEVBQThDQyxHQUE5QyxFQUFtRHhILElBQW5ELEVBQXlEO0FBRXZEO0FBQ0EsUUFBSXNILE1BQU0sQ0FBQ3ZCLEdBQUQsQ0FBTixDQUFZMEIsT0FBWixLQUF3QixDQUE1QixFQUErQjtBQUM3QkgsWUFBTSxDQUFDdkIsR0FBRCxDQUFOLENBQVkyQixRQUFaLENBQXFCLENBQUUsT0FBRixFQUFXekosSUFBWCxDQUFyQjtBQUNEOztBQUVELFdBQU8rQixJQUFJLENBQUMySCxXQUFMLENBQWlCTCxNQUFqQixFQUF5QnZCLEdBQXpCLEVBQThCd0IsUUFBOUIsRUFBd0NDLEdBQXhDLEVBQTZDeEgsSUFBN0MsQ0FBUDtBQUNEOztBQUVENEUsU0FBTyxHQUFHQSxPQUFPLElBQUksRUFBckI7QUFFQSxNQUFJZ0QsV0FBVyxHQUFHLENBQWxCO0FBQUEsTUFDSUMsVUFBVSxHQUFJakQsT0FBTyxDQUFDa0QsTUFBUixJQUFrQixHQURwQztBQUFBLE1BRUlDLFdBQVcsR0FBR0YsVUFBVSxDQUFDRyxVQUFYLENBQXNCLENBQXRCLENBRmxCO0FBQUEsTUFHSUMsVUFBVSxHQUFJSixVQUFVLENBQUN4TSxNQUg3QjtBQUFBLE1BSUl3RSxRQUFRLEdBQU0rRSxPQUFPLENBQUMvRSxRQUFSLElBQW9CcUgsZUFKdEM7QUFBQSxNQUtJZ0IsTUFBTSxHQUFRdEQsT0FBTyxDQUFDc0QsTUFBUixJQUFrQmIsYUFMcEM7O0FBT0EsV0FBU2MsU0FBVCxDQUFtQkMsS0FBbkIsRUFBMEJDLFNBQTFCLEVBQXFDQyxPQUFyQyxFQUE4Q0MsTUFBOUMsRUFBc0Q7QUFDcEQsUUFBSXhJLEdBQUo7QUFBQSxRQUFTeUksUUFBVDtBQUFBLFFBQW1CQyxZQUFuQjtBQUFBLFFBQWlDQyxNQUFqQztBQUFBLFFBQXlDdkIsTUFBekM7QUFBQSxRQUFpRHdCLEtBQWpEO0FBQUEsUUFDSUMsVUFESjtBQUFBLFFBQ2dCQyxZQURoQjtBQUFBLFFBRUlDLFdBQVcsR0FBRyxLQUZsQjtBQUFBLFFBR0kvRSxLQUFLLEdBQUdxRSxLQUFLLENBQUNXLE1BQU4sQ0FBYVYsU0FBYixJQUEwQkQsS0FBSyxDQUFDWSxNQUFOLENBQWFYLFNBQWIsQ0FIdEM7QUFBQSxRQUlJWSxHQUFHLEdBQUdiLEtBQUssQ0FBQ2MsTUFBTixDQUFhYixTQUFiLENBSlYsQ0FEb0QsQ0FPcEQ7QUFDQTtBQUNBOztBQUNBLFFBQUlOLFdBQVcsS0FBS0ssS0FBSyxDQUFDZSxHQUFOLENBQVVuQixVQUFWLENBQXFCakUsS0FBckIsQ0FBcEIsRUFBaUQ7QUFBRSxhQUFPLEtBQVA7QUFBZSxLQVZkLENBWXBEO0FBQ0E7OztBQUNBLFNBQUtoRSxHQUFHLEdBQUdnRSxLQUFLLEdBQUcsQ0FBbkIsRUFBc0JoRSxHQUFHLElBQUlrSixHQUE3QixFQUFrQ2xKLEdBQUcsRUFBckMsRUFBeUM7QUFDdkMsVUFBSThILFVBQVUsQ0FBQyxDQUFDOUgsR0FBRyxHQUFHZ0UsS0FBUCxJQUFnQmtFLFVBQWpCLENBQVYsS0FBMkNHLEtBQUssQ0FBQ2UsR0FBTixDQUFVcEosR0FBVixDQUEvQyxFQUErRDtBQUM3RDtBQUNEO0FBQ0Y7O0FBRUQwSSxnQkFBWSxHQUFHVyxJQUFJLENBQUNDLEtBQUwsQ0FBVyxDQUFDdEosR0FBRyxHQUFHZ0UsS0FBUCxJQUFnQmtFLFVBQTNCLENBQWY7O0FBQ0EsUUFBSVEsWUFBWSxHQUFHYixXQUFuQixFQUFnQztBQUFFLGFBQU8sS0FBUDtBQUFlOztBQUNqRDdILE9BQUcsSUFBSSxDQUFDQSxHQUFHLEdBQUdnRSxLQUFQLElBQWdCa0UsVUFBdkI7QUFFQVMsVUFBTSxHQUFHTixLQUFLLENBQUNlLEdBQU4sQ0FBVW5MLEtBQVYsQ0FBZ0IrRixLQUFoQixFQUF1QmhFLEdBQXZCLENBQVQ7QUFDQW9ILFVBQU0sR0FBR2lCLEtBQUssQ0FBQ2UsR0FBTixDQUFVbkwsS0FBVixDQUFnQitCLEdBQWhCLEVBQXFCa0osR0FBckIsQ0FBVDs7QUFDQSxRQUFJLENBQUNwSixRQUFRLENBQUNzSCxNQUFELENBQWIsRUFBdUI7QUFBRSxhQUFPLEtBQVA7QUFBZSxLQTFCWSxDQTRCcEQ7QUFDQTs7O0FBQ0EsUUFBSW9CLE1BQUosRUFBWTtBQUFFLGFBQU8sSUFBUDtBQUFjLEtBOUJ3QixDQWdDcEQ7QUFDQTs7O0FBQ0FDLFlBQVEsR0FBR0gsU0FBWDs7QUFFQSxhQUFTO0FBQ1BHLGNBQVE7O0FBQ1IsVUFBSUEsUUFBUSxJQUFJRixPQUFoQixFQUF5QjtBQUN2QjtBQUNBO0FBQ0E7QUFDRDs7QUFFRHZFLFdBQUssR0FBR3FFLEtBQUssQ0FBQ1csTUFBTixDQUFhUCxRQUFiLElBQXlCSixLQUFLLENBQUNZLE1BQU4sQ0FBYVIsUUFBYixDQUFqQztBQUNBUyxTQUFHLEdBQUdiLEtBQUssQ0FBQ2MsTUFBTixDQUFhVixRQUFiLENBQU47O0FBRUEsVUFBSXpFLEtBQUssR0FBR2tGLEdBQVIsSUFBZWIsS0FBSyxDQUFDa0IsTUFBTixDQUFhZCxRQUFiLElBQXlCSixLQUFLLENBQUNtQixTQUFsRCxFQUE2RDtBQUMzRDtBQUNBO0FBQ0E7QUFDQTtBQUNEOztBQUVELFVBQUl4QixXQUFXLEtBQUtLLEtBQUssQ0FBQ2UsR0FBTixDQUFVbkIsVUFBVixDQUFxQmpFLEtBQXJCLENBQXBCLEVBQWlEO0FBQUU7QUFBVzs7QUFFOUQsVUFBSXFFLEtBQUssQ0FBQ2tCLE1BQU4sQ0FBYWQsUUFBYixJQUF5QkosS0FBSyxDQUFDbUIsU0FBL0IsSUFBNEMsQ0FBaEQsRUFBbUQ7QUFDakQ7QUFDQTtBQUNEOztBQUVELFdBQUt4SixHQUFHLEdBQUdnRSxLQUFLLEdBQUcsQ0FBbkIsRUFBc0JoRSxHQUFHLElBQUlrSixHQUE3QixFQUFrQ2xKLEdBQUcsRUFBckMsRUFBeUM7QUFDdkMsWUFBSThILFVBQVUsQ0FBQyxDQUFDOUgsR0FBRyxHQUFHZ0UsS0FBUCxJQUFnQmtFLFVBQWpCLENBQVYsS0FBMkNHLEtBQUssQ0FBQ2UsR0FBTixDQUFVcEosR0FBVixDQUEvQyxFQUErRDtBQUM3RDtBQUNEO0FBQ0YsT0E3Qk0sQ0ErQlA7OztBQUNBLFVBQUlxSixJQUFJLENBQUNDLEtBQUwsQ0FBVyxDQUFDdEosR0FBRyxHQUFHZ0UsS0FBUCxJQUFnQmtFLFVBQTNCLElBQXlDUSxZQUE3QyxFQUEyRDtBQUFFO0FBQVcsT0FoQ2pFLENBa0NQOzs7QUFDQTFJLFNBQUcsSUFBSSxDQUFDQSxHQUFHLEdBQUdnRSxLQUFQLElBQWdCa0UsVUFBdkI7QUFDQWxJLFNBQUcsR0FBR3FJLEtBQUssQ0FBQ29CLFVBQU4sQ0FBaUJ6SixHQUFqQixDQUFOOztBQUVBLFVBQUlBLEdBQUcsR0FBR2tKLEdBQVYsRUFBZTtBQUFFO0FBQVcsT0F0Q3JCLENBd0NQOzs7QUFDQUgsaUJBQVcsR0FBRyxJQUFkO0FBQ0E7QUFDRDs7QUFFREYsY0FBVSxHQUFHUixLQUFLLENBQUNxQixVQUFuQjtBQUNBWixnQkFBWSxHQUFHVCxLQUFLLENBQUNzQixPQUFyQjtBQUNBdEIsU0FBSyxDQUFDcUIsVUFBTixHQUFtQixXQUFuQixDQW5Gb0QsQ0FxRnBEOztBQUNBckIsU0FBSyxDQUFDc0IsT0FBTixHQUFnQmxCLFFBQWhCO0FBRUFHLFNBQUssR0FBVVAsS0FBSyxDQUFDMUwsSUFBTixDQUFXLGVBQWV1QixJQUFmLEdBQXNCLE9BQWpDLEVBQTBDLEtBQTFDLEVBQWlELENBQWpELENBQWY7QUFDQTBLLFNBQUssQ0FBQ0QsTUFBTixHQUFlQSxNQUFmO0FBQ0FDLFNBQUssQ0FBQ2dCLEtBQU4sR0FBZSxJQUFmO0FBQ0FoQixTQUFLLENBQUNpQixJQUFOLEdBQWV6QyxNQUFmO0FBQ0F3QixTQUFLLENBQUNuRixHQUFOLEdBQWUsQ0FBRTZFLFNBQUYsRUFBYUcsUUFBYixDQUFmO0FBRUFKLFNBQUssQ0FBQ25CLEVBQU4sQ0FBUzBDLEtBQVQsQ0FBZUUsUUFBZixDQUF3QnpCLEtBQXhCLEVBQStCQyxTQUFTLEdBQUcsQ0FBM0MsRUFBOENHLFFBQTlDO0FBRUFHLFNBQUssR0FBVVAsS0FBSyxDQUFDMUwsSUFBTixDQUFXLGVBQWV1QixJQUFmLEdBQXNCLFFBQWpDLEVBQTJDLEtBQTNDLEVBQWtELENBQUMsQ0FBbkQsQ0FBZjtBQUNBMEssU0FBSyxDQUFDRCxNQUFOLEdBQWVOLEtBQUssQ0FBQ2UsR0FBTixDQUFVbkwsS0FBVixDQUFnQitGLEtBQWhCLEVBQXVCaEUsR0FBdkIsQ0FBZjtBQUNBNEksU0FBSyxDQUFDZ0IsS0FBTixHQUFlLElBQWY7QUFFQXZCLFNBQUssQ0FBQ3FCLFVBQU4sR0FBbUJiLFVBQW5CO0FBQ0FSLFNBQUssQ0FBQ3NCLE9BQU4sR0FBZ0JiLFlBQWhCO0FBQ0FULFNBQUssQ0FBQzBCLElBQU4sR0FBYXRCLFFBQVEsSUFBSU0sV0FBVyxHQUFHLENBQUgsR0FBTyxDQUF0QixDQUFyQjtBQUVBLFdBQU8sSUFBUDtBQUNEOztBQUVEN0IsSUFBRSxDQUFDMEMsS0FBSCxDQUFTSSxLQUFULENBQWVDLE1BQWYsQ0FBc0IsT0FBdEIsRUFBK0IsZUFBZS9MLElBQTlDLEVBQW9Ea0ssU0FBcEQsRUFBK0Q7QUFDN0Q4QixPQUFHLEVBQUUsQ0FBRSxXQUFGLEVBQWUsV0FBZixFQUE0QixZQUE1QixFQUEwQyxNQUExQztBQUR3RCxHQUEvRDtBQUdBaEQsSUFBRSxDQUFDaUQsUUFBSCxDQUFZQyxLQUFaLENBQWtCLGVBQWVsTSxJQUFmLEdBQXNCLE9BQXhDLElBQW1EaUssTUFBbkQ7QUFDQWpCLElBQUUsQ0FBQ2lELFFBQUgsQ0FBWUMsS0FBWixDQUFrQixlQUFlbE0sSUFBZixHQUFzQixRQUF4QyxJQUFvRGlLLE1BQXBEO0FBQ0QsQ0F6SUQsQzs7Ozs7Ozs7Ozs7OztBQ0xBLElBQU1rQyxPQUFPLEdBQUcseUJBQWhCOztBQUVBM08sTUFBTSxDQUFDQyxPQUFQLEdBQWlCLFNBQVMyTyxNQUFULENBQWlCcEQsRUFBakIsRUFBcUJyQyxPQUFyQixFQUE4QjtBQUM3Q3FDLElBQUUsQ0FBQ2lELFFBQUgsQ0FBWUMsS0FBWixDQUFrQkcsTUFBbEIsR0FBMkIsU0FBU0MsYUFBVCxDQUF3QmpELE1BQXhCLEVBQWdDdkIsR0FBaEMsRUFBcUM7QUFBQSwyQkFDekN1QixNQUFNLENBQUN2QixHQUFELENBQU4sQ0FBWTZELElBRDZCO0FBQUEsUUFDdERZLEdBRHNELG9CQUN0REEsR0FEc0Q7QUFBQSxRQUNqREMsR0FEaUQsb0JBQ2pEQSxHQURpRDtBQUU5RCxRQUFJLENBQUNELEdBQUwsRUFBVSxPQUFPLEVBQVA7QUFDVixXQUFPNUYsT0FBTyxDQUFDNEYsR0FBRCxDQUFQLENBQWFDLEdBQWIsSUFBb0IsSUFBM0I7QUFDRCxHQUpEOztBQU1BeEQsSUFBRSxDQUFDMEMsS0FBSCxDQUFTSSxLQUFULENBQWVDLE1BQWYsQ0FDRSxPQURGLEVBRUUsUUFGRixFQUdFLFNBQVNVLFdBQVQsQ0FBc0J0QyxLQUF0QixFQUE2QkMsU0FBN0IsRUFBd0NDLE9BQXhDLEVBQWlEQyxNQUFqRCxFQUF5RDtBQUN2RCxRQUFJb0MsUUFBUSxHQUFHdkMsS0FBSyxDQUFDVyxNQUFOLENBQWFWLFNBQWIsSUFBMEJELEtBQUssQ0FBQ1ksTUFBTixDQUFhWCxTQUFiLENBQXpDO0FBQ0EsUUFBSXVDLE1BQU0sR0FBR3hDLEtBQUssQ0FBQ2MsTUFBTixDQUFhYixTQUFiLENBQWI7QUFDQSxRQUFNc0IsS0FBSyxHQUFHdkIsS0FBSyxDQUFDZSxHQUFOLENBQVVuTCxLQUFWLENBQWdCMk0sUUFBaEIsRUFBMEJDLE1BQTFCLENBQWQ7QUFDQSxRQUFJQyxPQUFPLEdBQUc7QUFBRWYsVUFBSSxFQUFFekIsU0FBUjtBQUFtQnRJLFNBQUcsRUFBRTRLO0FBQXhCLEtBQWQsQ0FKdUQsQ0FNdkQ7O0FBQ0EsUUFBSXRDLFNBQVMsS0FBSyxDQUFsQixFQUFxQjtBQUNuQixVQUFJeUMsZ0JBQWdCLEdBQUcxQyxLQUFLLENBQUNXLE1BQU4sQ0FBYVYsU0FBUyxHQUFHLENBQXpCLElBQ2pCRCxLQUFLLENBQUNZLE1BQU4sQ0FBYVgsU0FBUyxHQUFHLENBQXpCLENBRE47QUFFQSxVQUFJMEMsY0FBYyxHQUFHM0MsS0FBSyxDQUFDYyxNQUFOLENBQWFiLFNBQVMsR0FBRyxDQUF6QixDQUFyQjtBQUNBLFVBQUkwQyxjQUFjLEdBQUdELGdCQUFyQixFQUF1QyxPQUFPLEtBQVA7QUFDeEMsS0Fac0QsQ0FjdkQ7OztBQUNBLFFBQUkxQyxLQUFLLENBQUNlLEdBQU4sQ0FBVW5CLFVBQVYsQ0FBcUI2QyxPQUFPLENBQUM5SyxHQUE3QixNQUFzQztBQUFJO0FBQTFDLE9BQ0FxSSxLQUFLLENBQUNlLEdBQU4sQ0FBVW5CLFVBQVYsQ0FBcUI2QyxPQUFPLENBQUM5SyxHQUFSLEdBQWMsQ0FBbkMsTUFBMEM7QUFBSTtBQURsRCxNQUMyRDtBQUN6RCxlQUFPLEtBQVA7QUFDRDs7QUFFRCxRQUFNUyxLQUFLLEdBQUc0SixPQUFPLENBQUMvRSxJQUFSLENBQWFzRSxLQUFiLENBQWQ7O0FBRUEsUUFBSSxDQUFDbkosS0FBRCxJQUFVQSxLQUFLLENBQUNuRixNQUFOLEdBQWUsQ0FBN0IsRUFBZ0M7QUFDOUIsYUFBTyxLQUFQO0FBQ0Q7O0FBeEJzRCxnQ0EwQi9CbUYsS0ExQitCO0FBQUEsUUEwQmhEd0ssR0ExQmdEO0FBQUEsUUEwQjNDUixHQTFCMkM7QUFBQSxRQTBCdENDLEdBMUJzQzs7QUE0QnZESSxXQUFPLENBQUM5SyxHQUFSLElBQWVpTCxHQUFHLENBQUMzUCxNQUFuQixDQTVCdUQsQ0E4QnZEO0FBQ0E7O0FBQ0EsUUFBSWlOLE9BQU8sS0FBS3VDLE9BQU8sQ0FBQ2YsSUFBUixHQUFlLENBQS9CLEVBQWtDO0FBQ2hDLFVBQUltQixnQkFBZ0IsR0FBRzdDLEtBQUssQ0FBQ1csTUFBTixDQUFhOEIsT0FBTyxDQUFDZixJQUFSLEdBQWUsQ0FBNUIsSUFBaUMxQixLQUFLLENBQUNZLE1BQU4sQ0FBYTZCLE9BQU8sQ0FBQ2YsSUFBUixHQUFlLENBQTVCLENBQXhEO0FBQ0EsVUFBSW9CLGNBQWMsR0FBRzlDLEtBQUssQ0FBQ2MsTUFBTixDQUFhMkIsT0FBTyxDQUFDZixJQUFSLEdBQWUsQ0FBNUIsQ0FBckI7QUFDQSxVQUFJb0IsY0FBYyxHQUFHRCxnQkFBckIsRUFBdUMsT0FBTyxLQUFQO0FBQ3hDOztBQUVELFFBQUlKLE9BQU8sQ0FBQ2YsSUFBUixJQUFnQnhCLE9BQXBCLEVBQTZCLE9BQU8sS0FBUDs7QUFFN0IsUUFBSSxDQUFDQyxNQUFMLEVBQWE7QUFDWCxVQUFJSSxLQUFLLEdBQUdQLEtBQUssQ0FBQzFMLElBQU4sQ0FBVyxRQUFYLEVBQXFCLEtBQXJCLEVBQTRCLENBQTVCLENBQVo7QUFDQWlNLFdBQUssQ0FBQ0QsTUFBTixHQUFlTixLQUFLLENBQUNlLEdBQU4sQ0FBVW5MLEtBQVYsQ0FBZ0IyTSxRQUFoQixFQUEwQkUsT0FBTyxDQUFDOUssR0FBbEMsQ0FBZjtBQUNBNEksV0FBSyxDQUFDaUIsSUFBTixHQUFhO0FBQUVhLFdBQUcsRUFBSEEsR0FBRjtBQUFPRCxXQUFHLEVBQUhBO0FBQVAsT0FBYjtBQUNBN0IsV0FBSyxDQUFDZ0IsS0FBTixHQUFjLElBQWQ7QUFDQWhCLFdBQUssQ0FBQ25GLEdBQU4sR0FBWSxDQUFFNkUsU0FBRixFQUFhd0MsT0FBTyxDQUFDZixJQUFSLEdBQWUsQ0FBNUIsQ0FBWjtBQUNBMUIsV0FBSyxDQUFDMEIsSUFBTixHQUFhZSxPQUFPLENBQUNmLElBQVIsR0FBZSxDQUE1QjtBQUNEOztBQUVELFdBQU8sSUFBUDtBQUNELEdBckRILEVBc0RFO0FBQUVHLE9BQUcsRUFBRSxDQUFFLFdBQUYsRUFBZSxXQUFmLEVBQTRCLFlBQTVCLEVBQTBDLE1BQTFDO0FBQVAsR0F0REY7QUF3REQsQ0EvREQsQzs7Ozs7Ozs7Ozs7O0NDQUE7O0FBRUEsU0FBU2tCLHVCQUFULENBQWtDL0gsSUFBbEMsRUFBd0NnSSxPQUF4QyxFQUFpRDtBQUMvQyxNQUFJOVAsQ0FBSixFQUFPK1AsTUFBUDtBQUNBLE1BQUlDLElBQUksR0FBR2xJLElBQUksQ0FBQ21JLEtBQUwsQ0FBV25JLElBQUksQ0FBQ29JLFNBQUwsQ0FBZSxNQUFmLENBQVgsRUFBbUMsQ0FBbkMsQ0FBWDs7QUFFQSxPQUFLbFEsQ0FBQyxHQUFHLENBQVQsRUFBWUEsQ0FBQyxHQUFHOFAsT0FBTyxDQUFDL1AsTUFBeEIsRUFBZ0MsRUFBRUMsQ0FBbEMsRUFBcUM7QUFDbkMrUCxVQUFNLEdBQUdELE9BQU8sQ0FBQzlQLENBQUQsQ0FBaEIsQ0FEbUMsQ0FHbkM7QUFDQTs7QUFDQSxRQUFJLENBQUMrUCxNQUFNLENBQUNJLE9BQVIsSUFBbUIsSUFBSXJMLE1BQUosQ0FBV2lMLE1BQU0sQ0FBQ0ksT0FBbEIsRUFBMkJ0TixJQUEzQixDQUFnQ21OLElBQWhDLENBQXZCLEVBQThEO0FBQzVELGFBQU9ELE1BQVA7QUFDRDtBQUNGO0FBQ0Y7O0FBRUQsU0FBU0ssZUFBVCxDQUEwQjNGLEdBQTFCLEVBQStCdUIsTUFBL0IsRUFBdUNxRSxVQUF2QyxFQUFtRDtBQUNqRDNQLFFBQU0sQ0FBQ3lDLElBQVAsQ0FBWWtOLFVBQVosRUFBd0JwTixPQUF4QixDQUFnQyxVQUFVcU4sSUFBVixFQUFnQjtBQUM5QyxRQUFJSixTQUFKO0FBQ0EsUUFBSTdPLEtBQUssR0FBR2dQLFVBQVUsQ0FBQ0MsSUFBRCxDQUF0Qjs7QUFFQSxRQUFJQSxJQUFJLEtBQUssV0FBYixFQUEwQjtBQUN4QjtBQUNBO0FBQ0E7QUFDQUEsVUFBSSxHQUFHLE9BQVA7QUFDRDs7QUFFREosYUFBUyxHQUFHbEUsTUFBTSxDQUFDdkIsR0FBRCxDQUFOLENBQVl5RixTQUFaLENBQXNCSSxJQUF0QixDQUFaOztBQUVBLFFBQUlKLFNBQVMsR0FBRyxDQUFoQixFQUFtQjtBQUFFO0FBQ25CbEUsWUFBTSxDQUFDdkIsR0FBRCxDQUFOLENBQVkyQixRQUFaLENBQXFCLENBQUNrRSxJQUFELEVBQU9qUCxLQUFQLENBQXJCO0FBQ0QsS0FGRCxNQUVPO0FBQUU7QUFDUDJLLFlBQU0sQ0FBQ3ZCLEdBQUQsQ0FBTixDQUFZd0YsS0FBWixDQUFrQkMsU0FBbEIsRUFBNkIsQ0FBN0IsSUFBa0M3TyxLQUFsQyxDQURLLENBQ21DO0FBQ3pDO0FBQ0YsR0FsQkQ7QUFtQkQ7O0FBRUQsU0FBU2tQLHdCQUFULENBQW1DNUUsRUFBbkMsRUFBdUNtRSxPQUF2QyxFQUFnRDtBQUM5QyxNQUFJLENBQUNBLE9BQUwsRUFBYztBQUNaQSxXQUFPLEdBQUcsRUFBVjtBQUNELEdBRkQsTUFFTztBQUNMQSxXQUFPLEdBQUc1UCxLQUFLLENBQUNJLE9BQU4sQ0FBY3dQLE9BQWQsSUFBeUJBLE9BQXpCLEdBQW1DLENBQUNBLE9BQUQsQ0FBN0M7QUFDRDs7QUFFRHBQLFFBQU0sQ0FBQzhQLE1BQVAsQ0FBY1YsT0FBZDtBQUVBLE1BQUlXLGFBQWEsR0FBRzlFLEVBQUUsQ0FBQ2lELFFBQUgsQ0FBWUMsS0FBWixDQUFrQjZCLFNBQWxCLElBQStCLEtBQUtELGFBQXhEOztBQUVBOUUsSUFBRSxDQUFDaUQsUUFBSCxDQUFZQyxLQUFaLENBQWtCNkIsU0FBbEIsR0FBOEIsVUFBVTFFLE1BQVYsRUFBa0J2QixHQUFsQixFQUF1Qm5CLE9BQXZCLEVBQWdDNEMsR0FBaEMsRUFBcUN4SCxJQUFyQyxFQUEyQztBQUN2RSxRQUFJcUwsTUFBTSxHQUFHRix1QkFBdUIsQ0FBQzdELE1BQU0sQ0FBQ3ZCLEdBQUQsQ0FBUCxFQUFjcUYsT0FBZCxDQUFwQztBQUNBLFFBQUlPLFVBQVUsR0FBR04sTUFBTSxJQUFJQSxNQUFNLENBQUNFLEtBQWxDOztBQUVBLFFBQUlJLFVBQUosRUFBZ0I7QUFDZEQscUJBQWUsQ0FBQzNGLEdBQUQsRUFBTXVCLE1BQU4sRUFBY3FFLFVBQWQsQ0FBZjtBQUNELEtBTnNFLENBUXZFOzs7QUFDQSxXQUFPSSxhQUFhLENBQUN6RSxNQUFELEVBQVN2QixHQUFULEVBQWNuQixPQUFkLEVBQXVCNEMsR0FBdkIsRUFBNEJ4SCxJQUE1QixDQUFwQjtBQUNELEdBVkQ7QUFXRDs7QUFFRDZMLHdCQUF3QixDQUFDRSxhQUF6QixHQUF5QyxVQUFVekUsTUFBVixFQUFrQnZCLEdBQWxCLEVBQXVCbkIsT0FBdkIsRUFBZ0M0QyxHQUFoQyxFQUFxQ3hILElBQXJDLEVBQTJDO0FBQ2xGLFNBQU9BLElBQUksQ0FBQzJILFdBQUwsQ0FBaUJMLE1BQWpCLEVBQXlCdkIsR0FBekIsRUFBOEJuQixPQUE5QixDQUFQO0FBQ0QsQ0FGRDs7QUFJQW5KLE1BQU0sQ0FBQ0MsT0FBUCxHQUFpQm1RLHdCQUFqQixDOzs7Ozs7Ozs7Ozs7QUNyRWE7O0FBR2JwUSxNQUFNLENBQUNDLE9BQVAsR0FBaUJzQixtQkFBTyxDQUFDLDJEQUFELENBQXhCLEM7Ozs7Ozs7Ozs7OztBQ0hBO0FBQ0E7QUFDYTtBQUViOztBQUNBdkIsTUFBTSxDQUFDQyxPQUFQLEdBQWlCc0IsbUJBQU8sQ0FBQywyRkFBRCxDQUF4QixDOzs7Ozs7Ozs7Ozs7QUNMQTtBQUNBO0FBRWE7O0FBR2J2QixNQUFNLENBQUNDLE9BQVAsR0FBaUIsQ0FDZixTQURlLEVBRWYsU0FGZSxFQUdmLE9BSGUsRUFJZixNQUplLEVBS2YsVUFMZSxFQU1mLFlBTmUsRUFPZixNQVBlLEVBUWYsU0FSZSxFQVNmLFFBVGUsRUFVZixLQVZlLEVBV2YsVUFYZSxFQVlmLElBWmUsRUFhZixTQWJlLEVBY2YsUUFkZSxFQWVmLEtBZmUsRUFnQmYsS0FoQmUsRUFpQmYsSUFqQmUsRUFrQmYsSUFsQmUsRUFtQmYsVUFuQmUsRUFvQmYsWUFwQmUsRUFxQmYsUUFyQmUsRUFzQmYsUUF0QmUsRUF1QmYsTUF2QmUsRUF3QmYsT0F4QmUsRUF5QmYsVUF6QmUsRUEwQmYsSUExQmUsRUEyQmYsSUEzQmUsRUE0QmYsSUE1QmUsRUE2QmYsSUE3QmUsRUE4QmYsSUE5QmUsRUErQmYsSUEvQmUsRUFnQ2YsTUFoQ2UsRUFpQ2YsUUFqQ2UsRUFrQ2YsSUFsQ2UsRUFtQ2YsTUFuQ2UsRUFvQ2YsUUFwQ2UsRUFxQ2YsUUFyQ2UsRUFzQ2YsSUF0Q2UsRUF1Q2YsTUF2Q2UsRUF3Q2YsTUF4Q2UsRUF5Q2YsTUF6Q2UsRUEwQ2YsVUExQ2UsRUEyQ2YsTUEzQ2UsRUE0Q2YsS0E1Q2UsRUE2Q2YsVUE3Q2UsRUE4Q2YsSUE5Q2UsRUErQ2YsVUEvQ2UsRUFnRGYsUUFoRGUsRUFpRGYsR0FqRGUsRUFrRGYsT0FsRGUsRUFtRGYsU0FuRGUsRUFvRGYsUUFwRGUsRUFxRGYsU0FyRGUsRUFzRGYsT0F0RGUsRUF1RGYsT0F2RGUsRUF3RGYsSUF4RGUsRUF5RGYsT0F6RGUsRUEwRGYsSUExRGUsRUEyRGYsT0EzRGUsRUE0RGYsT0E1RGUsRUE2RGYsSUE3RGUsRUE4RGYsT0E5RGUsRUErRGYsSUEvRGUsQ0FBakIsQzs7Ozs7Ozs7Ozs7O0FDTkE7QUFFYTs7QUFFYixJQUFJdVEsU0FBUyxHQUFPLDRCQUFwQjtBQUVBLElBQUlDLFFBQVEsR0FBUSx3QkFBcEI7QUFDQSxJQUFJQyxhQUFhLEdBQUcsU0FBcEI7QUFDQSxJQUFJQyxhQUFhLEdBQUcsU0FBcEI7QUFFQSxJQUFJQyxVQUFVLEdBQUksUUFBUUgsUUFBUixHQUFtQixHQUFuQixHQUF5QkMsYUFBekIsR0FBeUMsR0FBekMsR0FBK0NDLGFBQS9DLEdBQStELEdBQWpGO0FBRUEsSUFBSUUsU0FBUyxHQUFLLFlBQVlMLFNBQVosR0FBd0IsY0FBeEIsR0FBeUNJLFVBQXpDLEdBQXNELEtBQXhFO0FBRUEsSUFBSUUsUUFBUSxHQUFNLDZCQUE2QkQsU0FBN0IsR0FBeUMsWUFBM0Q7QUFFQSxJQUFJRSxTQUFTLEdBQUssa0NBQWxCO0FBQ0EsSUFBSUMsT0FBTyxHQUFPLHVDQUFsQjtBQUNBLElBQUlDLFVBQVUsR0FBSSxhQUFsQjtBQUNBLElBQUlDLFdBQVcsR0FBRyxvQkFBbEI7QUFDQSxJQUFJQyxLQUFLLEdBQVMsZ0NBQWxCO0FBRUEsSUFBSUMsV0FBVyxHQUFHLElBQUl6TSxNQUFKLENBQVcsU0FBU21NLFFBQVQsR0FBb0IsR0FBcEIsR0FBMEJDLFNBQTFCLEdBQXNDLEdBQXRDLEdBQTRDQyxPQUE1QyxHQUNMLEdBREssR0FDQ0MsVUFERCxHQUNjLEdBRGQsR0FDb0JDLFdBRHBCLEdBQ2tDLEdBRGxDLEdBQ3dDQyxLQUR4QyxHQUNnRCxHQUQzRCxDQUFsQjtBQUVBLElBQUlFLHNCQUFzQixHQUFHLElBQUkxTSxNQUFKLENBQVcsU0FBU21NLFFBQVQsR0FBb0IsR0FBcEIsR0FBMEJDLFNBQTFCLEdBQXNDLEdBQWpELENBQTdCO0FBRUEvUSxNQUFNLENBQUNDLE9BQVAsQ0FBZW1SLFdBQWYsR0FBNkJBLFdBQTdCO0FBQ0FwUixNQUFNLENBQUNDLE9BQVAsQ0FBZW9SLHNCQUFmLEdBQXdDQSxzQkFBeEMsQzs7Ozs7Ozs7Ozs7O0FDM0JBO0FBQ0E7QUFDYTs7OztBQUdiLFNBQVNuTyxNQUFULENBQWdCckIsR0FBaEIsRUFBcUI7QUFBRSxTQUFPdEIsTUFBTSxDQUFDd0IsU0FBUCxDQUFpQk0sUUFBakIsQ0FBMEJDLElBQTFCLENBQStCVCxHQUEvQixDQUFQO0FBQTZDOztBQUVwRSxTQUFTc0IsUUFBVCxDQUFrQnRCLEdBQWxCLEVBQXVCO0FBQUUsU0FBT3FCLE1BQU0sQ0FBQ3JCLEdBQUQsQ0FBTixLQUFnQixpQkFBdkI7QUFBMkM7O0FBRXBFLElBQUl5UCxlQUFlLEdBQUcvUSxNQUFNLENBQUN3QixTQUFQLENBQWlCbUMsY0FBdkM7O0FBRUEsU0FBU3FOLEdBQVQsQ0FBYUMsTUFBYixFQUFxQnZPLEdBQXJCLEVBQTBCO0FBQ3hCLFNBQU9xTyxlQUFlLENBQUNoUCxJQUFoQixDQUFxQmtQLE1BQXJCLEVBQTZCdk8sR0FBN0IsQ0FBUDtBQUNELEMsQ0FFRDtBQUNBOzs7QUFDQSxTQUFTTixNQUFULENBQWdCZDtBQUFJO0FBQXBCLEVBQWtEO0FBQ2hELE1BQUllLE9BQU8sR0FBRzdDLEtBQUssQ0FBQ2dDLFNBQU4sQ0FBZ0JRLEtBQWhCLENBQXNCRCxJQUF0QixDQUEyQk8sU0FBM0IsRUFBc0MsQ0FBdEMsQ0FBZDtBQUVBRCxTQUFPLENBQUNFLE9BQVIsQ0FBZ0IsVUFBVUMsTUFBVixFQUFrQjtBQUNoQyxRQUFJLENBQUNBLE1BQUwsRUFBYTtBQUFFO0FBQVM7O0FBRXhCLFFBQUksUUFBT0EsTUFBUCxNQUFrQixRQUF0QixFQUFnQztBQUM5QixZQUFNLElBQUkxQixTQUFKLENBQWMwQixNQUFNLEdBQUcsZ0JBQXZCLENBQU47QUFDRDs7QUFFRHhDLFVBQU0sQ0FBQ3lDLElBQVAsQ0FBWUQsTUFBWixFQUFvQkQsT0FBcEIsQ0FBNEIsVUFBVUcsR0FBVixFQUFlO0FBQ3pDcEIsU0FBRyxDQUFDb0IsR0FBRCxDQUFILEdBQVdGLE1BQU0sQ0FBQ0UsR0FBRCxDQUFqQjtBQUNELEtBRkQ7QUFHRCxHQVZEO0FBWUEsU0FBT3BCLEdBQVA7QUFDRCxDLENBRUQ7QUFDQTs7O0FBQ0EsU0FBUzRQLGNBQVQsQ0FBd0IvRCxHQUF4QixFQUE2QnBKLEdBQTdCLEVBQWtDb04sV0FBbEMsRUFBK0M7QUFDN0MsU0FBTyxHQUFHdkgsTUFBSCxDQUFVdUQsR0FBRyxDQUFDbkwsS0FBSixDQUFVLENBQVYsRUFBYStCLEdBQWIsQ0FBVixFQUE2Qm9OLFdBQTdCLEVBQTBDaEUsR0FBRyxDQUFDbkwsS0FBSixDQUFVK0IsR0FBRyxHQUFHLENBQWhCLENBQTFDLENBQVA7QUFDRCxDLENBRUQ7OztBQUVBLFNBQVNxTixpQkFBVCxDQUEyQkMsQ0FBM0IsRUFBOEI7QUFDNUI7QUFDQTtBQUNBLE1BQUlBLENBQUMsSUFBSSxNQUFMLElBQWVBLENBQUMsSUFBSSxNQUF4QixFQUFnQztBQUFFLFdBQU8sS0FBUDtBQUFlLEdBSHJCLENBSTVCOzs7QUFDQSxNQUFJQSxDQUFDLElBQUksTUFBTCxJQUFlQSxDQUFDLElBQUksTUFBeEIsRUFBZ0M7QUFBRSxXQUFPLEtBQVA7QUFBZTs7QUFDakQsTUFBSSxDQUFDQSxDQUFDLEdBQUcsTUFBTCxNQUFpQixNQUFqQixJQUEyQixDQUFDQSxDQUFDLEdBQUcsTUFBTCxNQUFpQixNQUFoRCxFQUF3RDtBQUFFLFdBQU8sS0FBUDtBQUFlLEdBTjdDLENBTzVCOzs7QUFDQSxNQUFJQSxDQUFDLElBQUksSUFBTCxJQUFhQSxDQUFDLElBQUksSUFBdEIsRUFBNEI7QUFBRSxXQUFPLEtBQVA7QUFBZTs7QUFDN0MsTUFBSUEsQ0FBQyxLQUFLLElBQVYsRUFBZ0I7QUFBRSxXQUFPLEtBQVA7QUFBZTs7QUFDakMsTUFBSUEsQ0FBQyxJQUFJLElBQUwsSUFBYUEsQ0FBQyxJQUFJLElBQXRCLEVBQTRCO0FBQUUsV0FBTyxLQUFQO0FBQWU7O0FBQzdDLE1BQUlBLENBQUMsSUFBSSxJQUFMLElBQWFBLENBQUMsSUFBSSxJQUF0QixFQUE0QjtBQUFFLFdBQU8sS0FBUDtBQUFlLEdBWGpCLENBWTVCOzs7QUFDQSxNQUFJQSxDQUFDLEdBQUcsUUFBUixFQUFrQjtBQUFFLFdBQU8sS0FBUDtBQUFlOztBQUNuQyxTQUFPLElBQVA7QUFDRDs7QUFFRCxTQUFTQyxhQUFULENBQXVCRCxDQUF2QixFQUEwQjtBQUN4QjtBQUNBLE1BQUlBLENBQUMsR0FBRyxNQUFSLEVBQWdCO0FBQ2RBLEtBQUMsSUFBSSxPQUFMO0FBQ0EsUUFBSUUsVUFBVSxHQUFHLFVBQVVGLENBQUMsSUFBSSxFQUFmLENBQWpCO0FBQUEsUUFDSUcsVUFBVSxHQUFHLFVBQVVILENBQUMsR0FBRyxLQUFkLENBRGpCO0FBR0EsV0FBT0ksTUFBTSxDQUFDQyxZQUFQLENBQW9CSCxVQUFwQixFQUFnQ0MsVUFBaEMsQ0FBUDtBQUNEOztBQUNELFNBQU9DLE1BQU0sQ0FBQ0MsWUFBUCxDQUFvQkwsQ0FBcEIsQ0FBUDtBQUNEOztBQUdELElBQUlNLGNBQWMsR0FBSSw2Q0FBdEI7QUFDQSxJQUFJQyxTQUFTLEdBQVMsNEJBQXRCO0FBQ0EsSUFBSUMsZUFBZSxHQUFHLElBQUl6TixNQUFKLENBQVd1TixjQUFjLENBQUNuUCxNQUFmLEdBQXdCLEdBQXhCLEdBQThCb1AsU0FBUyxDQUFDcFAsTUFBbkQsRUFBMkQsSUFBM0QsQ0FBdEI7QUFFQSxJQUFJc1Asc0JBQXNCLEdBQUcsb0NBQTdCOztBQUVBLElBQUlDLFFBQVEsR0FBRy9RLG1CQUFPLENBQUMseUVBQUQsQ0FBdEI7O0FBRUEsU0FBU2dSLG9CQUFULENBQThCeE4sS0FBOUIsRUFBcUN2QyxJQUFyQyxFQUEyQztBQUN6QyxNQUFJZ1EsSUFBSSxHQUFHLENBQVg7O0FBRUEsTUFBSWpCLEdBQUcsQ0FBQ2UsUUFBRCxFQUFXOVAsSUFBWCxDQUFQLEVBQXlCO0FBQ3ZCLFdBQU84UCxRQUFRLENBQUM5UCxJQUFELENBQWY7QUFDRDs7QUFFRCxNQUFJQSxJQUFJLENBQUMrSixVQUFMLENBQWdCLENBQWhCLE1BQXVCO0FBQUk7QUFBM0IsS0FBc0M4RixzQkFBc0IsQ0FBQzNQLElBQXZCLENBQTRCRixJQUE1QixDQUExQyxFQUE2RTtBQUMzRWdRLFFBQUksR0FBR2hRLElBQUksQ0FBQyxDQUFELENBQUosQ0FBUW1HLFdBQVIsT0FBMEIsR0FBMUIsR0FDTDhKLFFBQVEsQ0FBQ2pRLElBQUksQ0FBQ0QsS0FBTCxDQUFXLENBQVgsQ0FBRCxFQUFnQixFQUFoQixDQURILEdBQ3lCa1EsUUFBUSxDQUFDalEsSUFBSSxDQUFDRCxLQUFMLENBQVcsQ0FBWCxDQUFELEVBQWdCLEVBQWhCLENBRHhDOztBQUdBLFFBQUlvUCxpQkFBaUIsQ0FBQ2EsSUFBRCxDQUFyQixFQUE2QjtBQUMzQixhQUFPWCxhQUFhLENBQUNXLElBQUQsQ0FBcEI7QUFDRDtBQUNGOztBQUVELFNBQU96TixLQUFQO0FBQ0Q7QUFFRDs7Ozs7OztBQU1BLFNBQVMyTixVQUFULENBQW9CbFAsR0FBcEIsRUFBeUI7QUFDdkIsTUFBSUEsR0FBRyxDQUFDdUcsT0FBSixDQUFZLElBQVosSUFBb0IsQ0FBeEIsRUFBMkI7QUFBRSxXQUFPdkcsR0FBUDtBQUFhOztBQUMxQyxTQUFPQSxHQUFHLENBQUNDLE9BQUosQ0FBWXlPLGNBQVosRUFBNEIsSUFBNUIsQ0FBUDtBQUNEOztBQUVELFNBQVNTLFdBQVQsQ0FBcUJuUCxHQUFyQixFQUEwQjtBQUN4QixNQUFJQSxHQUFHLENBQUN1RyxPQUFKLENBQVksSUFBWixJQUFvQixDQUFwQixJQUF5QnZHLEdBQUcsQ0FBQ3VHLE9BQUosQ0FBWSxHQUFaLElBQW1CLENBQWhELEVBQW1EO0FBQUUsV0FBT3ZHLEdBQVA7QUFBYTs7QUFFbEUsU0FBT0EsR0FBRyxDQUFDQyxPQUFKLENBQVkyTyxlQUFaLEVBQTZCLFVBQVVyTixLQUFWLEVBQWlCNk4sT0FBakIsRUFBMEJDLE1BQTFCLEVBQWtDO0FBQ3BFLFFBQUlELE9BQUosRUFBYTtBQUFFLGFBQU9BLE9BQVA7QUFBaUI7O0FBQ2hDLFdBQU9MLG9CQUFvQixDQUFDeE4sS0FBRCxFQUFROE4sTUFBUixDQUEzQjtBQUNELEdBSE0sQ0FBUDtBQUlELEMsQ0FFRDs7O0FBRUEsSUFBSUMsbUJBQW1CLEdBQUcsUUFBMUI7QUFDQSxJQUFJQyxzQkFBc0IsR0FBRyxTQUE3QjtBQUNBLElBQUlDLGlCQUFpQixHQUFHO0FBQ3RCLE9BQUssT0FEaUI7QUFFdEIsT0FBSyxNQUZpQjtBQUd0QixPQUFLLE1BSGlCO0FBSXRCLE9BQUs7QUFKaUIsQ0FBeEI7O0FBT0EsU0FBU0MsaUJBQVQsQ0FBMkJDLEVBQTNCLEVBQStCO0FBQzdCLFNBQU9GLGlCQUFpQixDQUFDRSxFQUFELENBQXhCO0FBQ0Q7O0FBRUQsU0FBU0MsVUFBVCxDQUFvQjNQLEdBQXBCLEVBQXlCO0FBQ3ZCLE1BQUlzUCxtQkFBbUIsQ0FBQ3BRLElBQXBCLENBQXlCYyxHQUF6QixDQUFKLEVBQW1DO0FBQ2pDLFdBQU9BLEdBQUcsQ0FBQ0MsT0FBSixDQUFZc1Asc0JBQVosRUFBb0NFLGlCQUFwQyxDQUFQO0FBQ0Q7O0FBQ0QsU0FBT3pQLEdBQVA7QUFDRCxDLENBRUQ7OztBQUVBLElBQUk0UCxnQkFBZ0IsR0FBRyxzQkFBdkI7O0FBRUEsU0FBUzdQLFFBQVQsQ0FBa0JDLEdBQWxCLEVBQXVCO0FBQ3JCLFNBQU9BLEdBQUcsQ0FBQ0MsT0FBSixDQUFZMlAsZ0JBQVosRUFBOEIsTUFBOUIsQ0FBUDtBQUNELEMsQ0FFRDs7O0FBRUEsU0FBU0MsT0FBVCxDQUFpQmIsSUFBakIsRUFBdUI7QUFDckIsVUFBUUEsSUFBUjtBQUNFLFNBQUssSUFBTDtBQUNBLFNBQUssSUFBTDtBQUNFLGFBQU8sSUFBUDtBQUhKOztBQUtBLFNBQU8sS0FBUDtBQUNELEMsQ0FFRDs7O0FBQ0EsU0FBU2MsWUFBVCxDQUFzQmQsSUFBdEIsRUFBNEI7QUFDMUIsTUFBSUEsSUFBSSxJQUFJLE1BQVIsSUFBa0JBLElBQUksSUFBSSxNQUE5QixFQUFzQztBQUFFLFdBQU8sSUFBUDtBQUFjOztBQUN0RCxVQUFRQSxJQUFSO0FBQ0UsU0FBSyxJQUFMLENBREYsQ0FDYTs7QUFDWCxTQUFLLElBQUwsQ0FGRixDQUVhOztBQUNYLFNBQUssSUFBTCxDQUhGLENBR2E7O0FBQ1gsU0FBSyxJQUFMLENBSkYsQ0FJYTs7QUFDWCxTQUFLLElBQUwsQ0FMRixDQUthOztBQUNYLFNBQUssSUFBTDtBQUNBLFNBQUssSUFBTDtBQUNBLFNBQUssTUFBTDtBQUNBLFNBQUssTUFBTDtBQUNBLFNBQUssTUFBTDtBQUNBLFNBQUssTUFBTDtBQUNFLGFBQU8sSUFBUDtBQVpKOztBQWNBLFNBQU8sS0FBUDtBQUNELEMsQ0FFRDs7QUFFQTs7O0FBQ0EsSUFBSWUsZ0JBQWdCLEdBQUdoUyxtQkFBTyxDQUFDLHNGQUFELENBQTlCLEMsQ0FFQTs7O0FBQ0EsU0FBU2lTLFdBQVQsQ0FBcUJOLEVBQXJCLEVBQXlCO0FBQ3ZCLFNBQU9LLGdCQUFnQixDQUFDN1EsSUFBakIsQ0FBc0J3USxFQUF0QixDQUFQO0FBQ0QsQyxDQUdEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDQSxTQUFTTyxjQUFULENBQXdCUCxFQUF4QixFQUE0QjtBQUMxQixVQUFRQSxFQUFSO0FBQ0UsU0FBSztBQUFJO0FBQVQ7QUFDQSxTQUFLO0FBQUk7QUFBVDtBQUNBLFNBQUs7QUFBSTtBQUFUO0FBQ0EsU0FBSztBQUFJO0FBQVQ7QUFDQSxTQUFLO0FBQUk7QUFBVDtBQUNBLFNBQUs7QUFBSTtBQUFUO0FBQ0EsU0FBSztBQUFJO0FBQVQ7QUFDQSxTQUFLO0FBQUk7QUFBVDtBQUNBLFNBQUs7QUFBSTtBQUFUO0FBQ0EsU0FBSztBQUFJO0FBQVQ7QUFDQSxTQUFLO0FBQUk7QUFBVDtBQUNBLFNBQUs7QUFBSTtBQUFUO0FBQ0EsU0FBSztBQUFJO0FBQVQ7QUFDQSxTQUFLO0FBQUk7QUFBVDtBQUNBLFNBQUs7QUFBSTtBQUFUO0FBQ0EsU0FBSztBQUFJO0FBQVQ7QUFDQSxTQUFLO0FBQUk7QUFBVDtBQUNBLFNBQUs7QUFBSTtBQUFUO0FBQ0EsU0FBSztBQUFJO0FBQVQ7QUFDQSxTQUFLO0FBQUk7QUFBVDtBQUNBLFNBQUs7QUFBSTtBQUFUO0FBQ0EsU0FBSztBQUFJO0FBQVQ7QUFDQSxTQUFLO0FBQUk7QUFBVDtBQUNBLFNBQUs7QUFBSTtBQUFUO0FBQ0EsU0FBSztBQUFJO0FBQVQ7QUFDQSxTQUFLO0FBQUk7QUFBVDtBQUNBLFNBQUs7QUFBSTtBQUFUO0FBQ0EsU0FBSztBQUFJO0FBQVQ7QUFDQSxTQUFLO0FBQUk7QUFBVDtBQUNBLFNBQUs7QUFBSTtBQUFUO0FBQ0EsU0FBSztBQUFJO0FBQVQ7QUFDQSxTQUFLO0FBQUk7QUFBVDtBQUNFLGFBQU8sSUFBUDs7QUFDRjtBQUNFLGFBQU8sS0FBUDtBQW5DSjtBQXFDRCxDLENBRUQ7QUFDQTs7O0FBQ0EsU0FBU1Esa0JBQVQsQ0FBNEJsUSxHQUE1QixFQUFpQztBQUMvQjtBQUNBO0FBQ0FBLEtBQUcsR0FBR0EsR0FBRyxDQUFDbUksSUFBSixHQUFXbEksT0FBWCxDQUFtQixNQUFuQixFQUEyQixHQUEzQixDQUFOLENBSCtCLENBSy9CO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFDQSxNQUFJLElBQUlrRixXQUFKLE9BQXNCLEdBQTFCLEVBQStCO0FBQzdCbkYsT0FBRyxHQUFHQSxHQUFHLENBQUNDLE9BQUosQ0FBWSxJQUFaLEVBQWtCLEdBQWxCLENBQU47QUFDRCxHQWI4QixDQWUvQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDQSxTQUFPRCxHQUFHLENBQUNtRixXQUFKLEdBQWtCZ0wsV0FBbEIsRUFBUDtBQUNELEMsQ0FFRDtBQUVBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDQTFULE9BQU8sQ0FBQzJULEdBQVIsR0FBOEIsRUFBOUI7QUFDQTNULE9BQU8sQ0FBQzJULEdBQVIsQ0FBWUMsS0FBWixHQUE4QnRTLG1CQUFPLENBQUMsZ0RBQUQsQ0FBckM7QUFDQXRCLE9BQU8sQ0FBQzJULEdBQVIsQ0FBWUUsT0FBWixHQUE4QnZTLG1CQUFPLENBQUMsc0RBQUQsQ0FBckM7QUFFQXRCLE9BQU8sQ0FBQzBDLE1BQVIsR0FBOEJBLE1BQTlCO0FBQ0ExQyxPQUFPLENBQUNrRCxRQUFSLEdBQThCQSxRQUE5QjtBQUNBbEQsT0FBTyxDQUFDc1IsR0FBUixHQUE4QkEsR0FBOUI7QUFDQXRSLE9BQU8sQ0FBQ3lTLFVBQVIsR0FBOEJBLFVBQTlCO0FBQ0F6UyxPQUFPLENBQUMwUyxXQUFSLEdBQThCQSxXQUE5QjtBQUNBMVMsT0FBTyxDQUFDMFIsaUJBQVIsR0FBOEJBLGlCQUE5QjtBQUNBMVIsT0FBTyxDQUFDNFIsYUFBUixHQUE4QkEsYUFBOUIsQyxDQUNBOztBQUNBNVIsT0FBTyxDQUFDa1QsVUFBUixHQUE4QkEsVUFBOUI7QUFDQWxULE9BQU8sQ0FBQ3dSLGNBQVIsR0FBOEJBLGNBQTlCO0FBQ0F4UixPQUFPLENBQUNvVCxPQUFSLEdBQThCQSxPQUE5QjtBQUNBcFQsT0FBTyxDQUFDcVQsWUFBUixHQUE4QkEsWUFBOUI7QUFDQXJULE9BQU8sQ0FBQ3dULGNBQVIsR0FBOEJBLGNBQTlCO0FBQ0F4VCxPQUFPLENBQUN1VCxXQUFSLEdBQThCQSxXQUE5QjtBQUNBdlQsT0FBTyxDQUFDc0QsUUFBUixHQUE4QkEsUUFBOUI7QUFDQXRELE9BQU8sQ0FBQ3lULGtCQUFSLEdBQThCQSxrQkFBOUIsQzs7Ozs7Ozs7Ozs7O0FDNVRBO0FBQ2E7O0FBR2J6VCxPQUFPLENBQUM4VCxjQUFSLEdBQStCeFMsbUJBQU8sQ0FBQywwRkFBRCxDQUF0QztBQUNBdEIsT0FBTyxDQUFDK1Qsb0JBQVIsR0FBK0J6UyxtQkFBTyxDQUFDLHNHQUFELENBQXRDO0FBQ0F0QixPQUFPLENBQUNnVSxjQUFSLEdBQStCMVMsbUJBQU8sQ0FBQywwRkFBRCxDQUF0QyxDOzs7Ozs7Ozs7Ozs7QUNOQTtBQUNBO0FBQ2E7O0FBR2IsSUFBSW9SLFdBQVcsR0FBR3BSLG1CQUFPLENBQUMsMkVBQUQsQ0FBUCxDQUEyQm9SLFdBQTdDOztBQUdBM1MsTUFBTSxDQUFDQyxPQUFQLEdBQWlCLFNBQVMrVCxvQkFBVCxDQUE4QnhRLEdBQTlCLEVBQW1DYyxHQUFuQyxFQUF3Q2tKLEdBQXhDLEVBQTZDO0FBQzVELE1BQUlnRixJQUFKO0FBQUEsTUFBVTBCLEtBQVY7QUFBQSxNQUNJQyxLQUFLLEdBQUcsQ0FEWjtBQUFBLE1BRUk3TCxLQUFLLEdBQUdoRSxHQUZaO0FBQUEsTUFHSTBGLE1BQU0sR0FBRztBQUNQb0ssTUFBRSxFQUFFLEtBREc7QUFFUDlQLE9BQUcsRUFBRSxDQUZFO0FBR1A2UCxTQUFLLEVBQUUsQ0FIQTtBQUlQM1EsT0FBRyxFQUFFO0FBSkUsR0FIYjs7QUFVQSxNQUFJQSxHQUFHLENBQUMrSSxVQUFKLENBQWVqSSxHQUFmLE1BQXdCO0FBQUs7QUFBakMsSUFBMEM7QUFDeENBLFNBQUc7O0FBQ0gsYUFBT0EsR0FBRyxHQUFHa0osR0FBYixFQUFrQjtBQUNoQmdGLFlBQUksR0FBR2hQLEdBQUcsQ0FBQytJLFVBQUosQ0FBZWpJLEdBQWYsQ0FBUDs7QUFDQSxZQUFJa08sSUFBSSxLQUFLO0FBQUs7QUFBbEIsVUFBNEI7QUFBRSxtQkFBT3hJLE1BQVA7QUFBZ0I7O0FBQzlDLFlBQUl3SSxJQUFJLEtBQUs7QUFBSztBQUFsQixVQUEyQjtBQUN6QnhJLGtCQUFNLENBQUMxRixHQUFQLEdBQWFBLEdBQUcsR0FBRyxDQUFuQjtBQUNBMEYsa0JBQU0sQ0FBQ3hHLEdBQVAsR0FBYW1QLFdBQVcsQ0FBQ25QLEdBQUcsQ0FBQ2pCLEtBQUosQ0FBVStGLEtBQUssR0FBRyxDQUFsQixFQUFxQmhFLEdBQXJCLENBQUQsQ0FBeEI7QUFDQTBGLGtCQUFNLENBQUNvSyxFQUFQLEdBQVksSUFBWjtBQUNBLG1CQUFPcEssTUFBUDtBQUNEOztBQUNELFlBQUl3SSxJQUFJLEtBQUs7QUFBSztBQUFkLFdBQXlCbE8sR0FBRyxHQUFHLENBQU4sR0FBVWtKLEdBQXZDLEVBQTRDO0FBQzFDbEosYUFBRyxJQUFJLENBQVA7QUFDQTtBQUNEOztBQUVEQSxXQUFHO0FBQ0osT0FqQnVDLENBbUJ4Qzs7O0FBQ0EsYUFBTzBGLE1BQVA7QUFDRCxLQWhDMkQsQ0FrQzVEOzs7QUFFQWtLLE9BQUssR0FBRyxDQUFSOztBQUNBLFNBQU81UCxHQUFHLEdBQUdrSixHQUFiLEVBQWtCO0FBQ2hCZ0YsUUFBSSxHQUFHaFAsR0FBRyxDQUFDK0ksVUFBSixDQUFlakksR0FBZixDQUFQOztBQUVBLFFBQUlrTyxJQUFJLEtBQUssSUFBYixFQUFtQjtBQUFFO0FBQVEsS0FIYixDQUtoQjs7O0FBQ0EsUUFBSUEsSUFBSSxHQUFHLElBQVAsSUFBZUEsSUFBSSxLQUFLLElBQTVCLEVBQWtDO0FBQUU7QUFBUTs7QUFFNUMsUUFBSUEsSUFBSSxLQUFLO0FBQUs7QUFBZCxPQUF5QmxPLEdBQUcsR0FBRyxDQUFOLEdBQVVrSixHQUF2QyxFQUE0QztBQUMxQ2xKLFNBQUcsSUFBSSxDQUFQO0FBQ0E7QUFDRDs7QUFFRCxRQUFJa08sSUFBSSxLQUFLO0FBQUs7QUFBbEIsTUFBMkI7QUFDekIwQixhQUFLO0FBQ047O0FBRUQsUUFBSTFCLElBQUksS0FBSztBQUFLO0FBQWxCLE1BQTJCO0FBQ3pCLFlBQUkwQixLQUFLLEtBQUssQ0FBZCxFQUFpQjtBQUFFO0FBQVE7O0FBQzNCQSxhQUFLO0FBQ047O0FBRUQ1UCxPQUFHO0FBQ0o7O0FBRUQsTUFBSWdFLEtBQUssS0FBS2hFLEdBQWQsRUFBbUI7QUFBRSxXQUFPMEYsTUFBUDtBQUFnQjs7QUFDckMsTUFBSWtLLEtBQUssS0FBSyxDQUFkLEVBQWlCO0FBQUUsV0FBT2xLLE1BQVA7QUFBZ0I7O0FBRW5DQSxRQUFNLENBQUN4RyxHQUFQLEdBQWFtUCxXQUFXLENBQUNuUCxHQUFHLENBQUNqQixLQUFKLENBQVUrRixLQUFWLEVBQWlCaEUsR0FBakIsQ0FBRCxDQUF4QjtBQUNBMEYsUUFBTSxDQUFDbUssS0FBUCxHQUFlQSxLQUFmO0FBQ0FuSyxRQUFNLENBQUMxRixHQUFQLEdBQWFBLEdBQWI7QUFDQTBGLFFBQU0sQ0FBQ29LLEVBQVAsR0FBWSxJQUFaO0FBQ0EsU0FBT3BLLE1BQVA7QUFDRCxDQXRFRCxDOzs7Ozs7Ozs7Ozs7QUNSQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ2E7O0FBRWJoSyxNQUFNLENBQUNDLE9BQVAsR0FBaUIsU0FBUzhULGNBQVQsQ0FBd0JwSCxLQUF4QixFQUErQnJFLEtBQS9CLEVBQXNDK0wsYUFBdEMsRUFBcUQ7QUFDcEUsTUFBSUgsS0FBSjtBQUFBLE1BQVdJLEtBQVg7QUFBQSxNQUFrQmpJLE1BQWxCO0FBQUEsTUFBMEJrSSxPQUExQjtBQUFBLE1BQ0lDLFFBQVEsR0FBRyxDQUFDLENBRGhCO0FBQUEsTUFFSWhILEdBQUcsR0FBR2IsS0FBSyxDQUFDOEgsTUFGaEI7QUFBQSxNQUdJQyxNQUFNLEdBQUcvSCxLQUFLLENBQUNySSxHQUhuQjtBQUtBcUksT0FBSyxDQUFDckksR0FBTixHQUFZZ0UsS0FBSyxHQUFHLENBQXBCO0FBQ0E0TCxPQUFLLEdBQUcsQ0FBUjs7QUFFQSxTQUFPdkgsS0FBSyxDQUFDckksR0FBTixHQUFZa0osR0FBbkIsRUFBd0I7QUFDdEJuQixVQUFNLEdBQUdNLEtBQUssQ0FBQ2UsR0FBTixDQUFVbkIsVUFBVixDQUFxQkksS0FBSyxDQUFDckksR0FBM0IsQ0FBVDs7QUFDQSxRQUFJK0gsTUFBTSxLQUFLO0FBQUs7QUFBcEIsTUFBNkI7QUFDM0I2SCxhQUFLOztBQUNMLFlBQUlBLEtBQUssS0FBSyxDQUFkLEVBQWlCO0FBQ2ZJLGVBQUssR0FBRyxJQUFSO0FBQ0E7QUFDRDtBQUNGOztBQUVEQyxXQUFPLEdBQUc1SCxLQUFLLENBQUNySSxHQUFoQjtBQUNBcUksU0FBSyxDQUFDbkIsRUFBTixDQUFTbUosTUFBVCxDQUFnQkMsU0FBaEIsQ0FBMEJqSSxLQUExQjs7QUFDQSxRQUFJTixNQUFNLEtBQUs7QUFBSztBQUFwQixNQUE2QjtBQUMzQixZQUFJa0ksT0FBTyxLQUFLNUgsS0FBSyxDQUFDckksR0FBTixHQUFZLENBQTVCLEVBQStCO0FBQzdCO0FBQ0E0UCxlQUFLO0FBQ04sU0FIRCxNQUdPLElBQUlHLGFBQUosRUFBbUI7QUFDeEIxSCxlQUFLLENBQUNySSxHQUFOLEdBQVlvUSxNQUFaO0FBQ0EsaUJBQU8sQ0FBQyxDQUFSO0FBQ0Q7QUFDRjtBQUNGOztBQUVELE1BQUlKLEtBQUosRUFBVztBQUNURSxZQUFRLEdBQUc3SCxLQUFLLENBQUNySSxHQUFqQjtBQUNELEdBbENtRSxDQW9DcEU7OztBQUNBcUksT0FBSyxDQUFDckksR0FBTixHQUFZb1EsTUFBWjtBQUVBLFNBQU9GLFFBQVA7QUFDRCxDQXhDRCxDOzs7Ozs7Ozs7Ozs7QUNQQTtBQUNBO0FBQ2E7O0FBR2IsSUFBSTdCLFdBQVcsR0FBR3BSLG1CQUFPLENBQUMsMkVBQUQsQ0FBUCxDQUEyQm9SLFdBQTdDOztBQUdBM1MsTUFBTSxDQUFDQyxPQUFQLEdBQWlCLFNBQVNnVSxjQUFULENBQXdCelEsR0FBeEIsRUFBNkJjLEdBQTdCLEVBQWtDa0osR0FBbEMsRUFBdUM7QUFDdEQsTUFBSWdGLElBQUo7QUFBQSxNQUNJbkcsTUFESjtBQUFBLE1BRUk4SCxLQUFLLEdBQUcsQ0FGWjtBQUFBLE1BR0k3TCxLQUFLLEdBQUdoRSxHQUhaO0FBQUEsTUFJSTBGLE1BQU0sR0FBRztBQUNQb0ssTUFBRSxFQUFFLEtBREc7QUFFUDlQLE9BQUcsRUFBRSxDQUZFO0FBR1A2UCxTQUFLLEVBQUUsQ0FIQTtBQUlQM1EsT0FBRyxFQUFFO0FBSkUsR0FKYjs7QUFXQSxNQUFJYyxHQUFHLElBQUlrSixHQUFYLEVBQWdCO0FBQUUsV0FBT3hELE1BQVA7QUFBZ0I7O0FBRWxDcUMsUUFBTSxHQUFHN0ksR0FBRyxDQUFDK0ksVUFBSixDQUFlakksR0FBZixDQUFUOztBQUVBLE1BQUkrSCxNQUFNLEtBQUs7QUFBSztBQUFoQixLQUEyQkEsTUFBTSxLQUFLO0FBQUs7QUFBM0MsS0FBc0RBLE1BQU0sS0FBSztBQUFLO0FBQTFFLElBQW1GO0FBQUUsYUFBT3JDLE1BQVA7QUFBZ0I7O0FBRXJHMUYsS0FBRyxHQWxCbUQsQ0FvQnREOztBQUNBLE1BQUkrSCxNQUFNLEtBQUssSUFBZixFQUFxQjtBQUFFQSxVQUFNLEdBQUcsSUFBVDtBQUFnQjs7QUFFdkMsU0FBTy9ILEdBQUcsR0FBR2tKLEdBQWIsRUFBa0I7QUFDaEJnRixRQUFJLEdBQUdoUCxHQUFHLENBQUMrSSxVQUFKLENBQWVqSSxHQUFmLENBQVA7O0FBQ0EsUUFBSWtPLElBQUksS0FBS25HLE1BQWIsRUFBcUI7QUFDbkJyQyxZQUFNLENBQUMxRixHQUFQLEdBQWFBLEdBQUcsR0FBRyxDQUFuQjtBQUNBMEYsWUFBTSxDQUFDbUssS0FBUCxHQUFlQSxLQUFmO0FBQ0FuSyxZQUFNLENBQUN4RyxHQUFQLEdBQWFtUCxXQUFXLENBQUNuUCxHQUFHLENBQUNqQixLQUFKLENBQVUrRixLQUFLLEdBQUcsQ0FBbEIsRUFBcUJoRSxHQUFyQixDQUFELENBQXhCO0FBQ0EwRixZQUFNLENBQUNvSyxFQUFQLEdBQVksSUFBWjtBQUNBLGFBQU9wSyxNQUFQO0FBQ0QsS0FORCxNQU1PLElBQUl3SSxJQUFJLEtBQUssSUFBYixFQUFtQjtBQUN4QjJCLFdBQUs7QUFDTixLQUZNLE1BRUEsSUFBSTNCLElBQUksS0FBSztBQUFLO0FBQWQsT0FBeUJsTyxHQUFHLEdBQUcsQ0FBTixHQUFVa0osR0FBdkMsRUFBNEM7QUFDakRsSixTQUFHOztBQUNILFVBQUlkLEdBQUcsQ0FBQytJLFVBQUosQ0FBZWpJLEdBQWYsTUFBd0IsSUFBNUIsRUFBa0M7QUFDaEM2UCxhQUFLO0FBQ047QUFDRjs7QUFFRDdQLE9BQUc7QUFDSjs7QUFFRCxTQUFPMEYsTUFBUDtBQUNELENBNUNELEM7Ozs7Ozs7Ozs7OztBQ1JBO0FBRWE7O0FBR2IsSUFBSTZLLEtBQUssR0FBVXRULG1CQUFPLENBQUMsMEVBQUQsQ0FBMUI7O0FBQ0EsSUFBSXVULE9BQU8sR0FBUXZULG1CQUFPLENBQUMsc0VBQUQsQ0FBMUI7O0FBQ0EsSUFBSXdULFFBQVEsR0FBT3hULG1CQUFPLENBQUMsa0VBQUQsQ0FBMUI7O0FBQ0EsSUFBSXlULFVBQVUsR0FBS3pULG1CQUFPLENBQUMsd0VBQUQsQ0FBMUI7O0FBQ0EsSUFBSTBULFdBQVcsR0FBSTFULG1CQUFPLENBQUMsMEVBQUQsQ0FBMUI7O0FBQ0EsSUFBSTJULFlBQVksR0FBRzNULG1CQUFPLENBQUMsNEVBQUQsQ0FBMUI7O0FBQ0EsSUFBSTBILFNBQVMsR0FBTTFILG1CQUFPLENBQUMsMERBQUQsQ0FBMUI7O0FBQ0EsSUFBSXNTLEtBQUssR0FBVXRTLG1CQUFPLENBQUMsZ0RBQUQsQ0FBMUI7O0FBQ0EsSUFBSTRULFFBQVEsR0FBTzVULG1CQUFPLENBQUMsd0ZBQUQsQ0FBMUI7O0FBR0EsSUFBSXFPLE1BQU0sR0FBRztBQUNYLGFBQVdyTyxtQkFBTyxDQUFDLGdGQUFELENBRFA7QUFFWDZULE1BQUksRUFBRTdULG1CQUFPLENBQUMsMEVBQUQsQ0FGRjtBQUdYOFQsWUFBVSxFQUFFOVQsbUJBQU8sQ0FBQyxzRkFBRDtBQUhSLENBQWIsQyxDQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsSUFBSStULFlBQVksR0FBRyxtQ0FBbkI7QUFDQSxJQUFJQyxZQUFZLEdBQUcsbUNBQW5COztBQUVBLFNBQVNDLFlBQVQsQ0FBc0J6TSxHQUF0QixFQUEyQjtBQUN6QjtBQUNBLE1BQUl2RixHQUFHLEdBQUd1RixHQUFHLENBQUM0QyxJQUFKLEdBQVdoRCxXQUFYLEVBQVY7QUFFQSxTQUFPMk0sWUFBWSxDQUFDNVMsSUFBYixDQUFrQmMsR0FBbEIsSUFBMEIrUixZQUFZLENBQUM3UyxJQUFiLENBQWtCYyxHQUFsQixJQUF5QixJQUF6QixHQUFnQyxLQUExRCxHQUFtRSxJQUExRTtBQUNELEMsQ0FFRDs7O0FBR0EsSUFBSWlTLG1CQUFtQixHQUFHLENBQUUsT0FBRixFQUFXLFFBQVgsRUFBcUIsU0FBckIsQ0FBMUI7O0FBRUEsU0FBU0MsYUFBVCxDQUF1QjNNLEdBQXZCLEVBQTRCO0FBQzFCLE1BQUk0TSxNQUFNLEdBQUc5QixLQUFLLENBQUMrQixLQUFOLENBQVk3TSxHQUFaLEVBQWlCLElBQWpCLENBQWI7O0FBRUEsTUFBSTRNLE1BQU0sQ0FBQ0UsUUFBWCxFQUFxQjtBQUNuQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxRQUFJLENBQUNGLE1BQU0sQ0FBQ0csUUFBUixJQUFvQkwsbUJBQW1CLENBQUMxTCxPQUFwQixDQUE0QjRMLE1BQU0sQ0FBQ0csUUFBbkMsS0FBZ0QsQ0FBeEUsRUFBMkU7QUFDekUsVUFBSTtBQUNGSCxjQUFNLENBQUNFLFFBQVAsR0FBa0JWLFFBQVEsQ0FBQ1ksT0FBVCxDQUFpQkosTUFBTSxDQUFDRSxRQUF4QixDQUFsQjtBQUNELE9BRkQsQ0FFRSxPQUFPRyxFQUFQLEVBQVc7QUFBRTtBQUFNO0FBQ3RCO0FBQ0Y7O0FBRUQsU0FBT25DLEtBQUssQ0FBQ29DLE1BQU4sQ0FBYXBDLEtBQUssQ0FBQ3FDLE1BQU4sQ0FBYVAsTUFBYixDQUFiLENBQVA7QUFDRDs7QUFFRCxTQUFTUSxpQkFBVCxDQUEyQnBOLEdBQTNCLEVBQWdDO0FBQzlCLE1BQUk0TSxNQUFNLEdBQUc5QixLQUFLLENBQUMrQixLQUFOLENBQVk3TSxHQUFaLEVBQWlCLElBQWpCLENBQWI7O0FBRUEsTUFBSTRNLE1BQU0sQ0FBQ0UsUUFBWCxFQUFxQjtBQUNuQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxRQUFJLENBQUNGLE1BQU0sQ0FBQ0csUUFBUixJQUFvQkwsbUJBQW1CLENBQUMxTCxPQUFwQixDQUE0QjRMLE1BQU0sQ0FBQ0csUUFBbkMsS0FBZ0QsQ0FBeEUsRUFBMkU7QUFDekUsVUFBSTtBQUNGSCxjQUFNLENBQUNFLFFBQVAsR0FBa0JWLFFBQVEsQ0FBQ2lCLFNBQVQsQ0FBbUJULE1BQU0sQ0FBQ0UsUUFBMUIsQ0FBbEI7QUFDRCxPQUZELENBRUUsT0FBT0csRUFBUCxFQUFXO0FBQUU7QUFBTTtBQUN0QjtBQUNGOztBQUVELFNBQU9uQyxLQUFLLENBQUN3QyxNQUFOLENBQWF4QyxLQUFLLENBQUNxQyxNQUFOLENBQWFQLE1BQWIsQ0FBYixDQUFQO0FBQ0Q7QUFHRDs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQStCQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFzR0EsU0FBU1csVUFBVCxDQUFvQkMsVUFBcEIsRUFBZ0NwTixPQUFoQyxFQUF5QztBQUN2QyxNQUFJLEVBQUUsZ0JBQWdCbU4sVUFBbEIsQ0FBSixFQUFtQztBQUNqQyxXQUFPLElBQUlBLFVBQUosQ0FBZUMsVUFBZixFQUEyQnBOLE9BQTNCLENBQVA7QUFDRDs7QUFFRCxNQUFJLENBQUNBLE9BQUwsRUFBYztBQUNaLFFBQUksQ0FBQzBMLEtBQUssQ0FBQzFSLFFBQU4sQ0FBZW9ULFVBQWYsQ0FBTCxFQUFpQztBQUMvQnBOLGFBQU8sR0FBR29OLFVBQVUsSUFBSSxFQUF4QjtBQUNBQSxnQkFBVSxHQUFHLFNBQWI7QUFDRDtBQUNGO0FBRUQ7Ozs7Ozs7OztBQU9BLE9BQUs1QixNQUFMLEdBQWMsSUFBSU8sWUFBSixFQUFkO0FBRUE7Ozs7Ozs7O0FBT0EsT0FBS2hILEtBQUwsR0FBYSxJQUFJK0csV0FBSixFQUFiO0FBRUE7Ozs7Ozs7O0FBT0EsT0FBS3VCLElBQUwsR0FBWSxJQUFJeEIsVUFBSixFQUFaO0FBRUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFxQkEsT0FBS3ZHLFFBQUwsR0FBZ0IsSUFBSXNHLFFBQUosRUFBaEI7QUFFQTs7Ozs7Ozs7QUFPQSxPQUFLMEIsT0FBTCxHQUFlLElBQUl4TixTQUFKLEVBQWY7QUFFQTs7Ozs7Ozs7Ozs7Ozs7OztBQWVBLE9BQUt1TSxZQUFMLEdBQW9CQSxZQUFwQjtBQUVBOzs7Ozs7O0FBTUEsT0FBS0UsYUFBTCxHQUFxQkEsYUFBckI7QUFFQTs7Ozs7O0FBS0EsT0FBS1MsaUJBQUwsR0FBeUJBLGlCQUF6QixDQXJHdUMsQ0F3R3ZDOztBQUVBOzs7Ozs7O0FBTUEsT0FBS3RCLEtBQUwsR0FBYUEsS0FBYjtBQUVBOzs7Ozs7O0FBTUEsT0FBS0MsT0FBTCxHQUFlRCxLQUFLLENBQUNsUyxNQUFOLENBQWEsRUFBYixFQUFpQm1TLE9BQWpCLENBQWY7QUFHQSxPQUFLM0wsT0FBTCxHQUFlLEVBQWY7QUFDQSxPQUFLdU4sU0FBTCxDQUFlSCxVQUFmOztBQUVBLE1BQUlwTixPQUFKLEVBQWE7QUFBRSxTQUFLRyxHQUFMLENBQVNILE9BQVQ7QUFBb0I7QUFDcEM7QUFHRDs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBbUJBbU4sVUFBVSxDQUFDdlUsU0FBWCxDQUFxQnVILEdBQXJCLEdBQTJCLFVBQVVILE9BQVYsRUFBbUI7QUFDNUMwTCxPQUFLLENBQUNsUyxNQUFOLENBQWEsS0FBS3dHLE9BQWxCLEVBQTJCQSxPQUEzQjtBQUNBLFNBQU8sSUFBUDtBQUNELENBSEQ7QUFNQTs7Ozs7Ozs7Ozs7O0FBVUFtTixVQUFVLENBQUN2VSxTQUFYLENBQXFCMlUsU0FBckIsR0FBaUMsVUFBVUMsT0FBVixFQUFtQjtBQUNsRCxNQUFJcFMsSUFBSSxHQUFHLElBQVg7QUFBQSxNQUFpQmdTLFVBQWpCOztBQUVBLE1BQUkxQixLQUFLLENBQUMxUixRQUFOLENBQWV3VCxPQUFmLENBQUosRUFBNkI7QUFDM0JKLGNBQVUsR0FBR0ksT0FBYjtBQUNBQSxXQUFPLEdBQUcvRyxNQUFNLENBQUMyRyxVQUFELENBQWhCOztBQUNBLFFBQUksQ0FBQ0ksT0FBTCxFQUFjO0FBQUUsWUFBTSxJQUFJblAsS0FBSixDQUFVLGlDQUFpQytPLFVBQWpDLEdBQThDLGVBQXhELENBQU47QUFBaUY7QUFDbEc7O0FBRUQsTUFBSSxDQUFDSSxPQUFMLEVBQWM7QUFBRSxVQUFNLElBQUluUCxLQUFKLENBQVUsNkNBQVYsQ0FBTjtBQUFpRTs7QUFFakYsTUFBSW1QLE9BQU8sQ0FBQ3hOLE9BQVosRUFBcUI7QUFBRTVFLFFBQUksQ0FBQytFLEdBQUwsQ0FBU3FOLE9BQU8sQ0FBQ3hOLE9BQWpCO0FBQTRCOztBQUVuRCxNQUFJd04sT0FBTyxDQUFDQyxVQUFaLEVBQXdCO0FBQ3RCclcsVUFBTSxDQUFDeUMsSUFBUCxDQUFZMlQsT0FBTyxDQUFDQyxVQUFwQixFQUFnQzlULE9BQWhDLENBQXdDLFVBQVVOLElBQVYsRUFBZ0I7QUFDdEQsVUFBSW1VLE9BQU8sQ0FBQ0MsVUFBUixDQUFtQnBVLElBQW5CLEVBQXlCa00sS0FBN0IsRUFBb0M7QUFDbENuSyxZQUFJLENBQUMvQixJQUFELENBQUosQ0FBVzhMLEtBQVgsQ0FBaUJ1SSxVQUFqQixDQUE0QkYsT0FBTyxDQUFDQyxVQUFSLENBQW1CcFUsSUFBbkIsRUFBeUJrTSxLQUFyRDtBQUNEOztBQUNELFVBQUlpSSxPQUFPLENBQUNDLFVBQVIsQ0FBbUJwVSxJQUFuQixFQUF5QnNVLE1BQTdCLEVBQXFDO0FBQ25DdlMsWUFBSSxDQUFDL0IsSUFBRCxDQUFKLENBQVd1VSxNQUFYLENBQWtCRixVQUFsQixDQUE2QkYsT0FBTyxDQUFDQyxVQUFSLENBQW1CcFUsSUFBbkIsRUFBeUJzVSxNQUF0RDtBQUNEO0FBQ0YsS0FQRDtBQVFEOztBQUNELFNBQU8sSUFBUDtBQUNELENBeEJEO0FBMkJBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBUixVQUFVLENBQUN2VSxTQUFYLENBQXFCaVYsTUFBckIsR0FBOEIsVUFBVS9NLElBQVYsRUFBZ0JnTixhQUFoQixFQUErQjtBQUMzRCxNQUFJak4sTUFBTSxHQUFHLEVBQWI7O0FBRUEsTUFBSSxDQUFDakssS0FBSyxDQUFDSSxPQUFOLENBQWM4SixJQUFkLENBQUwsRUFBMEI7QUFBRUEsUUFBSSxHQUFHLENBQUVBLElBQUYsQ0FBUDtBQUFrQjs7QUFFOUMsR0FBRSxNQUFGLEVBQVUsT0FBVixFQUFtQixRQUFuQixFQUE4Qm5ILE9BQTlCLENBQXNDLFVBQVVvVSxLQUFWLEVBQWlCO0FBQ3JEbE4sVUFBTSxHQUFHQSxNQUFNLENBQUNHLE1BQVAsQ0FBYyxLQUFLK00sS0FBTCxFQUFZNUksS0FBWixDQUFrQjBJLE1BQWxCLENBQXlCL00sSUFBekIsRUFBK0IsSUFBL0IsQ0FBZCxDQUFUO0FBQ0QsR0FGRCxFQUVHLElBRkg7QUFJQUQsUUFBTSxHQUFHQSxNQUFNLENBQUNHLE1BQVAsQ0FBYyxLQUFLd0ssTUFBTCxDQUFZb0MsTUFBWixDQUFtQkMsTUFBbkIsQ0FBMEIvTSxJQUExQixFQUFnQyxJQUFoQyxDQUFkLENBQVQ7QUFFQSxNQUFJa04sTUFBTSxHQUFHbE4sSUFBSSxDQUFDbkMsTUFBTCxDQUFZLFVBQVV0RixJQUFWLEVBQWdCO0FBQUUsV0FBT3dILE1BQU0sQ0FBQ0QsT0FBUCxDQUFldkgsSUFBZixJQUF1QixDQUE5QjtBQUFrQyxHQUFoRSxDQUFiOztBQUVBLE1BQUkyVSxNQUFNLENBQUN2WCxNQUFQLElBQWlCLENBQUNxWCxhQUF0QixFQUFxQztBQUNuQyxVQUFNLElBQUl6UCxLQUFKLENBQVUsbURBQW1EMlAsTUFBN0QsQ0FBTjtBQUNEOztBQUVELFNBQU8sSUFBUDtBQUNELENBbEJEO0FBcUJBOzs7Ozs7Ozs7QUFPQWIsVUFBVSxDQUFDdlUsU0FBWCxDQUFxQnFWLE9BQXJCLEdBQStCLFVBQVVuTixJQUFWLEVBQWdCZ04sYUFBaEIsRUFBK0I7QUFDNUQsTUFBSWpOLE1BQU0sR0FBRyxFQUFiOztBQUVBLE1BQUksQ0FBQ2pLLEtBQUssQ0FBQ0ksT0FBTixDQUFjOEosSUFBZCxDQUFMLEVBQTBCO0FBQUVBLFFBQUksR0FBRyxDQUFFQSxJQUFGLENBQVA7QUFBa0I7O0FBRTlDLEdBQUUsTUFBRixFQUFVLE9BQVYsRUFBbUIsUUFBbkIsRUFBOEJuSCxPQUE5QixDQUFzQyxVQUFVb1UsS0FBVixFQUFpQjtBQUNyRGxOLFVBQU0sR0FBR0EsTUFBTSxDQUFDRyxNQUFQLENBQWMsS0FBSytNLEtBQUwsRUFBWTVJLEtBQVosQ0FBa0I4SSxPQUFsQixDQUEwQm5OLElBQTFCLEVBQWdDLElBQWhDLENBQWQsQ0FBVDtBQUNELEdBRkQsRUFFRyxJQUZIO0FBSUFELFFBQU0sR0FBR0EsTUFBTSxDQUFDRyxNQUFQLENBQWMsS0FBS3dLLE1BQUwsQ0FBWW9DLE1BQVosQ0FBbUJLLE9BQW5CLENBQTJCbk4sSUFBM0IsRUFBaUMsSUFBakMsQ0FBZCxDQUFUO0FBRUEsTUFBSWtOLE1BQU0sR0FBR2xOLElBQUksQ0FBQ25DLE1BQUwsQ0FBWSxVQUFVdEYsSUFBVixFQUFnQjtBQUFFLFdBQU93SCxNQUFNLENBQUNELE9BQVAsQ0FBZXZILElBQWYsSUFBdUIsQ0FBOUI7QUFBa0MsR0FBaEUsQ0FBYjs7QUFFQSxNQUFJMlUsTUFBTSxDQUFDdlgsTUFBUCxJQUFpQixDQUFDcVgsYUFBdEIsRUFBcUM7QUFDbkMsVUFBTSxJQUFJelAsS0FBSixDQUFVLG9EQUFvRDJQLE1BQTlELENBQU47QUFDRDs7QUFDRCxTQUFPLElBQVA7QUFDRCxDQWpCRDtBQW9CQTs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBZ0JBYixVQUFVLENBQUN2VSxTQUFYLENBQXFCc1YsR0FBckIsR0FBMkIsVUFBVXpJO0FBQU87QUFBakIsRUFBcUM7QUFDOUQsTUFBSTBJLElBQUksR0FBRyxDQUFFLElBQUYsRUFBU25OLE1BQVQsQ0FBZ0JwSyxLQUFLLENBQUNnQyxTQUFOLENBQWdCUSxLQUFoQixDQUFzQkQsSUFBdEIsQ0FBMkJPLFNBQTNCLEVBQXNDLENBQXRDLENBQWhCLENBQVg7QUFDQStMLFFBQU0sQ0FBQzJJLEtBQVAsQ0FBYTNJLE1BQWIsRUFBcUIwSSxJQUFyQjtBQUNBLFNBQU8sSUFBUDtBQUNELENBSkQ7QUFPQTs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFlQWhCLFVBQVUsQ0FBQ3ZVLFNBQVgsQ0FBcUI2VCxLQUFyQixHQUE2QixVQUFVbEksR0FBVixFQUFlM0IsR0FBZixFQUFvQjtBQUMvQyxNQUFJLE9BQU8yQixHQUFQLEtBQWUsUUFBbkIsRUFBNkI7QUFDM0IsVUFBTSxJQUFJbEcsS0FBSixDQUFVLCtCQUFWLENBQU47QUFDRDs7QUFFRCxNQUFJbUYsS0FBSyxHQUFHLElBQUksS0FBSzZKLElBQUwsQ0FBVWdCLEtBQWQsQ0FBb0I5SixHQUFwQixFQUF5QixJQUF6QixFQUErQjNCLEdBQS9CLENBQVo7QUFFQSxPQUFLeUssSUFBTCxDQUFVaUIsT0FBVixDQUFrQjlLLEtBQWxCO0FBRUEsU0FBT0EsS0FBSyxDQUFDZCxNQUFiO0FBQ0QsQ0FWRDtBQWFBOzs7Ozs7Ozs7Ozs7O0FBV0F5SyxVQUFVLENBQUN2VSxTQUFYLENBQXFCMEssTUFBckIsR0FBOEIsVUFBVWlCLEdBQVYsRUFBZTNCLEdBQWYsRUFBb0I7QUFDaERBLEtBQUcsR0FBR0EsR0FBRyxJQUFJLEVBQWI7QUFFQSxTQUFPLEtBQUswQyxRQUFMLENBQWNoQyxNQUFkLENBQXFCLEtBQUttSixLQUFMLENBQVdsSSxHQUFYLEVBQWdCM0IsR0FBaEIsQ0FBckIsRUFBMkMsS0FBSzVDLE9BQWhELEVBQXlENEMsR0FBekQsQ0FBUDtBQUNELENBSkQ7QUFPQTs7Ozs7Ozs7Ozs7QUFTQXVLLFVBQVUsQ0FBQ3ZVLFNBQVgsQ0FBcUIyVixXQUFyQixHQUFtQyxVQUFVaEssR0FBVixFQUFlM0IsR0FBZixFQUFvQjtBQUNyRCxNQUFJWSxLQUFLLEdBQUcsSUFBSSxLQUFLNkosSUFBTCxDQUFVZ0IsS0FBZCxDQUFvQjlKLEdBQXBCLEVBQXlCLElBQXpCLEVBQStCM0IsR0FBL0IsQ0FBWjtBQUVBWSxPQUFLLENBQUNnTCxVQUFOLEdBQW1CLElBQW5CO0FBQ0EsT0FBS25CLElBQUwsQ0FBVWlCLE9BQVYsQ0FBa0I5SyxLQUFsQjtBQUVBLFNBQU9BLEtBQUssQ0FBQ2QsTUFBYjtBQUNELENBUEQ7QUFVQTs7Ozs7Ozs7OztBQVFBeUssVUFBVSxDQUFDdlUsU0FBWCxDQUFxQjZWLFlBQXJCLEdBQW9DLFVBQVVsSyxHQUFWLEVBQWUzQixHQUFmLEVBQW9CO0FBQ3REQSxLQUFHLEdBQUdBLEdBQUcsSUFBSSxFQUFiO0FBRUEsU0FBTyxLQUFLMEMsUUFBTCxDQUFjaEMsTUFBZCxDQUFxQixLQUFLaUwsV0FBTCxDQUFpQmhLLEdBQWpCLEVBQXNCM0IsR0FBdEIsQ0FBckIsRUFBaUQsS0FBSzVDLE9BQXRELEVBQStENEMsR0FBL0QsQ0FBUDtBQUNELENBSkQ7O0FBT0EvTCxNQUFNLENBQUNDLE9BQVAsR0FBaUJxVyxVQUFqQixDOzs7Ozs7Ozs7Ozs7QUNwa0JBOzs7OztBQUthOztBQUdiLElBQUl1QixLQUFLLEdBQWF0VyxtQkFBTyxDQUFDLDREQUFELENBQTdCOztBQUdBLElBQUl1VyxNQUFNLEdBQUcsQ0FDWDtBQUNBO0FBQ0EsQ0FBRSxPQUFGLEVBQWdCdlcsbUJBQU8sQ0FBQyxvRkFBRCxDQUF2QixFQUFxRCxDQUFFLFdBQUYsRUFBZSxXQUFmLENBQXJELENBSFcsRUFJWCxDQUFFLE1BQUYsRUFBZ0JBLG1CQUFPLENBQUMsa0ZBQUQsQ0FBdkIsQ0FKVyxFQUtYLENBQUUsT0FBRixFQUFnQkEsbUJBQU8sQ0FBQyxvRkFBRCxDQUF2QixFQUFxRCxDQUFFLFdBQUYsRUFBZSxXQUFmLEVBQTRCLFlBQTVCLEVBQTBDLE1BQTFDLENBQXJELENBTFcsRUFNWCxDQUFFLFlBQUYsRUFBZ0JBLG1CQUFPLENBQUMsOEZBQUQsQ0FBdkIsRUFBcUQsQ0FBRSxXQUFGLEVBQWUsV0FBZixFQUE0QixZQUE1QixFQUEwQyxNQUExQyxDQUFyRCxDQU5XLEVBT1gsQ0FBRSxJQUFGLEVBQWdCQSxtQkFBTyxDQUFDLDhFQUFELENBQXZCLEVBQXFELENBQUUsV0FBRixFQUFlLFdBQWYsRUFBNEIsWUFBNUIsRUFBMEMsTUFBMUMsQ0FBckQsQ0FQVyxFQVFYLENBQUUsTUFBRixFQUFnQkEsbUJBQU8sQ0FBQyxrRkFBRCxDQUF2QixFQUFxRCxDQUFFLFdBQUYsRUFBZSxXQUFmLEVBQTRCLFlBQTVCLENBQXJELENBUlcsRUFTWCxDQUFFLFdBQUYsRUFBZ0JBLG1CQUFPLENBQUMsNEZBQUQsQ0FBdkIsQ0FUVyxFQVVYLENBQUUsU0FBRixFQUFnQkEsbUJBQU8sQ0FBQyx3RkFBRCxDQUF2QixFQUFxRCxDQUFFLFdBQUYsRUFBZSxXQUFmLEVBQTRCLFlBQTVCLENBQXJELENBVlcsRUFXWCxDQUFFLFVBQUYsRUFBZ0JBLG1CQUFPLENBQUMsMEZBQUQsQ0FBdkIsQ0FYVyxFQVlYLENBQUUsWUFBRixFQUFnQkEsbUJBQU8sQ0FBQyw4RkFBRCxDQUF2QixFQUFxRCxDQUFFLFdBQUYsRUFBZSxXQUFmLEVBQTRCLFlBQTVCLENBQXJELENBWlcsRUFhWCxDQUFFLFdBQUYsRUFBZ0JBLG1CQUFPLENBQUMsNEZBQUQsQ0FBdkIsQ0FiVyxDQUFiO0FBaUJBOzs7O0FBR0EsU0FBUzBULFdBQVQsR0FBdUI7QUFDckI7Ozs7O0FBS0EsT0FBSzNHLEtBQUwsR0FBYSxJQUFJdUosS0FBSixFQUFiOztBQUVBLE9BQUssSUFBSWhZLENBQUMsR0FBRyxDQUFiLEVBQWdCQSxDQUFDLEdBQUdpWSxNQUFNLENBQUNsWSxNQUEzQixFQUFtQ0MsQ0FBQyxFQUFwQyxFQUF3QztBQUN0QyxTQUFLeU8sS0FBTCxDQUFXck4sSUFBWCxDQUFnQjZXLE1BQU0sQ0FBQ2pZLENBQUQsQ0FBTixDQUFVLENBQVYsQ0FBaEIsRUFBOEJpWSxNQUFNLENBQUNqWSxDQUFELENBQU4sQ0FBVSxDQUFWLENBQTlCLEVBQTRDO0FBQUUyTyxTQUFHLEVBQUUsQ0FBQ3NKLE1BQU0sQ0FBQ2pZLENBQUQsQ0FBTixDQUFVLENBQVYsS0FBZ0IsRUFBakIsRUFBcUIwQyxLQUFyQjtBQUFQLEtBQTVDO0FBQ0Q7QUFDRixDLENBR0Q7QUFDQTs7O0FBQ0EwUyxXQUFXLENBQUNsVCxTQUFaLENBQXNCcU0sUUFBdEIsR0FBaUMsVUFBVXpCLEtBQVYsRUFBaUJDLFNBQWpCLEVBQTRCQyxPQUE1QixFQUFxQztBQUNwRSxNQUFJdUgsRUFBSjtBQUFBLE1BQVF2VSxDQUFSO0FBQUEsTUFDSTZPLEtBQUssR0FBRyxLQUFLSixLQUFMLENBQVd5SixRQUFYLENBQW9CLEVBQXBCLENBRFo7QUFBQSxNQUVJcFksR0FBRyxHQUFHK08sS0FBSyxDQUFDOU8sTUFGaEI7QUFBQSxNQUdJeU8sSUFBSSxHQUFHekIsU0FIWDtBQUFBLE1BSUlvTCxhQUFhLEdBQUcsS0FKcEI7QUFBQSxNQUtJQyxVQUFVLEdBQUd0TCxLQUFLLENBQUNuQixFQUFOLENBQVNyQyxPQUFULENBQWlCOE8sVUFMbEM7O0FBT0EsU0FBTzVKLElBQUksR0FBR3hCLE9BQWQsRUFBdUI7QUFDckJGLFNBQUssQ0FBQzBCLElBQU4sR0FBYUEsSUFBSSxHQUFHMUIsS0FBSyxDQUFDdUwsY0FBTixDQUFxQjdKLElBQXJCLENBQXBCOztBQUNBLFFBQUlBLElBQUksSUFBSXhCLE9BQVosRUFBcUI7QUFBRTtBQUFRLEtBRlYsQ0FJckI7QUFDQTs7O0FBQ0EsUUFBSUYsS0FBSyxDQUFDa0IsTUFBTixDQUFhUSxJQUFiLElBQXFCMUIsS0FBSyxDQUFDbUIsU0FBL0IsRUFBMEM7QUFBRTtBQUFRLEtBTi9CLENBUXJCO0FBQ0E7OztBQUNBLFFBQUluQixLQUFLLENBQUN1SCxLQUFOLElBQWUrRCxVQUFuQixFQUErQjtBQUM3QnRMLFdBQUssQ0FBQzBCLElBQU4sR0FBYXhCLE9BQWI7QUFDQTtBQUNELEtBYm9CLENBZXJCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBRUEsU0FBS2hOLENBQUMsR0FBRyxDQUFULEVBQVlBLENBQUMsR0FBR0YsR0FBaEIsRUFBcUJFLENBQUMsRUFBdEIsRUFBMEI7QUFDeEJ1VSxRQUFFLEdBQUcxRixLQUFLLENBQUM3TyxDQUFELENBQUwsQ0FBUzhNLEtBQVQsRUFBZ0IwQixJQUFoQixFQUFzQnhCLE9BQXRCLEVBQStCLEtBQS9CLENBQUw7O0FBQ0EsVUFBSXVILEVBQUosRUFBUTtBQUFFO0FBQVE7QUFDbkIsS0F6Qm9CLENBMkJyQjtBQUNBOzs7QUFDQXpILFNBQUssQ0FBQ3dMLEtBQU4sR0FBYyxDQUFDSCxhQUFmLENBN0JxQixDQStCckI7O0FBQ0EsUUFBSXJMLEtBQUssQ0FBQ3lMLE9BQU4sQ0FBY3pMLEtBQUssQ0FBQzBCLElBQU4sR0FBYSxDQUEzQixDQUFKLEVBQW1DO0FBQ2pDMkosbUJBQWEsR0FBRyxJQUFoQjtBQUNEOztBQUVEM0osUUFBSSxHQUFHMUIsS0FBSyxDQUFDMEIsSUFBYjs7QUFFQSxRQUFJQSxJQUFJLEdBQUd4QixPQUFQLElBQWtCRixLQUFLLENBQUN5TCxPQUFOLENBQWMvSixJQUFkLENBQXRCLEVBQTJDO0FBQ3pDMkosbUJBQWEsR0FBRyxJQUFoQjtBQUNBM0osVUFBSTtBQUNKMUIsV0FBSyxDQUFDMEIsSUFBTixHQUFhQSxJQUFiO0FBQ0Q7QUFDRjtBQUNGLENBcEREO0FBdURBOzs7Ozs7O0FBS0E0RyxXQUFXLENBQUNsVCxTQUFaLENBQXNCNlQsS0FBdEIsR0FBOEIsVUFBVWxJLEdBQVYsRUFBZWxDLEVBQWYsRUFBbUJPLEdBQW5CLEVBQXdCc00sU0FBeEIsRUFBbUM7QUFDL0QsTUFBSTFMLEtBQUo7O0FBRUEsTUFBSSxDQUFDZSxHQUFMLEVBQVU7QUFBRTtBQUFTOztBQUVyQmYsT0FBSyxHQUFHLElBQUksS0FBSzZLLEtBQVQsQ0FBZTlKLEdBQWYsRUFBb0JsQyxFQUFwQixFQUF3Qk8sR0FBeEIsRUFBNkJzTSxTQUE3QixDQUFSO0FBRUEsT0FBS2pLLFFBQUwsQ0FBY3pCLEtBQWQsRUFBcUJBLEtBQUssQ0FBQzBCLElBQTNCLEVBQWlDMUIsS0FBSyxDQUFDc0IsT0FBdkM7QUFDRCxDQVJEOztBQVdBZ0gsV0FBVyxDQUFDbFQsU0FBWixDQUFzQnlWLEtBQXRCLEdBQThCalcsbUJBQU8sQ0FBQyxnR0FBRCxDQUFyQztBQUdBdkIsTUFBTSxDQUFDQyxPQUFQLEdBQWlCZ1YsV0FBakIsQzs7Ozs7Ozs7Ozs7O0FDekhBOzs7Ozs7QUFNYTs7QUFHYixJQUFJNEMsS0FBSyxHQUFJdFcsbUJBQU8sQ0FBQyw0REFBRCxDQUFwQjs7QUFHQSxJQUFJdVcsTUFBTSxHQUFHLENBQ1gsQ0FBRSxXQUFGLEVBQW9CdlcsbUJBQU8sQ0FBQywwRkFBRCxDQUEzQixDQURXLEVBRVgsQ0FBRSxPQUFGLEVBQW9CQSxtQkFBTyxDQUFDLGtGQUFELENBQTNCLENBRlcsRUFHWCxDQUFFLFFBQUYsRUFBb0JBLG1CQUFPLENBQUMsb0ZBQUQsQ0FBM0IsQ0FIVyxFQUlYLENBQUUsU0FBRixFQUFvQkEsbUJBQU8sQ0FBQyxzRkFBRCxDQUEzQixDQUpXLEVBS1gsQ0FBRSxjQUFGLEVBQW9CQSxtQkFBTyxDQUFDLGdHQUFELENBQTNCLENBTFcsRUFNWCxDQUFFLGFBQUYsRUFBb0JBLG1CQUFPLENBQUMsOEZBQUQsQ0FBM0IsQ0FOVyxDQUFiO0FBVUE7Ozs7QUFHQSxTQUFTK1csSUFBVCxHQUFnQjtBQUNkOzs7OztBQUtBLE9BQUtoSyxLQUFMLEdBQWEsSUFBSXVKLEtBQUosRUFBYjs7QUFFQSxPQUFLLElBQUloWSxDQUFDLEdBQUcsQ0FBYixFQUFnQkEsQ0FBQyxHQUFHaVksTUFBTSxDQUFDbFksTUFBM0IsRUFBbUNDLENBQUMsRUFBcEMsRUFBd0M7QUFDdEMsU0FBS3lPLEtBQUwsQ0FBV3JOLElBQVgsQ0FBZ0I2VyxNQUFNLENBQUNqWSxDQUFELENBQU4sQ0FBVSxDQUFWLENBQWhCLEVBQThCaVksTUFBTSxDQUFDalksQ0FBRCxDQUFOLENBQVUsQ0FBVixDQUE5QjtBQUNEO0FBQ0Y7QUFHRDs7Ozs7OztBQUtBeVksSUFBSSxDQUFDdlcsU0FBTCxDQUFlMFYsT0FBZixHQUF5QixVQUFVOUssS0FBVixFQUFpQjtBQUN4QyxNQUFJOU0sQ0FBSixFQUFPMFksQ0FBUCxFQUFVN0osS0FBVjtBQUVBQSxPQUFLLEdBQUcsS0FBS0osS0FBTCxDQUFXeUosUUFBWCxDQUFvQixFQUFwQixDQUFSOztBQUVBLE9BQUtsWSxDQUFDLEdBQUcsQ0FBSixFQUFPMFksQ0FBQyxHQUFHN0osS0FBSyxDQUFDOU8sTUFBdEIsRUFBOEJDLENBQUMsR0FBRzBZLENBQWxDLEVBQXFDMVksQ0FBQyxFQUF0QyxFQUEwQztBQUN4QzZPLFNBQUssQ0FBQzdPLENBQUQsQ0FBTCxDQUFTOE0sS0FBVDtBQUNEO0FBQ0YsQ0FSRDs7QUFVQTJMLElBQUksQ0FBQ3ZXLFNBQUwsQ0FBZXlWLEtBQWYsR0FBdUJqVyxtQkFBTyxDQUFDLDRGQUFELENBQTlCO0FBR0F2QixNQUFNLENBQUNDLE9BQVAsR0FBaUJxWSxJQUFqQixDOzs7Ozs7Ozs7Ozs7QUN6REE7Ozs7O0FBS2E7O0FBR2IsSUFBSVQsS0FBSyxHQUFhdFcsbUJBQU8sQ0FBQyw0REFBRCxDQUE3QixDLENBR0E7QUFDQTs7O0FBRUEsSUFBSXVXLE1BQU0sR0FBRyxDQUNYLENBQUUsTUFBRixFQUFxQnZXLG1CQUFPLENBQUMsb0ZBQUQsQ0FBNUIsQ0FEVyxFQUVYLENBQUUsU0FBRixFQUFxQkEsbUJBQU8sQ0FBQywwRkFBRCxDQUE1QixDQUZXLEVBR1gsQ0FBRSxRQUFGLEVBQXFCQSxtQkFBTyxDQUFDLHdGQUFELENBQTVCLENBSFcsRUFJWCxDQUFFLFdBQUYsRUFBcUJBLG1CQUFPLENBQUMsOEZBQUQsQ0FBNUIsQ0FKVyxFQUtYLENBQUUsZUFBRixFQUFxQkEsbUJBQU8sQ0FBQyxzR0FBRCxDQUFQLENBQXdDNk0sUUFBN0QsQ0FMVyxFQU1YLENBQUUsVUFBRixFQUFxQjdNLG1CQUFPLENBQUMsNEZBQUQsQ0FBUCxDQUFtQzZNLFFBQXhELENBTlcsRUFPWCxDQUFFLE1BQUYsRUFBcUI3TSxtQkFBTyxDQUFDLG9GQUFELENBQTVCLENBUFcsRUFRWCxDQUFFLE9BQUYsRUFBcUJBLG1CQUFPLENBQUMsc0ZBQUQsQ0FBNUIsQ0FSVyxFQVNYLENBQUUsVUFBRixFQUFxQkEsbUJBQU8sQ0FBQyw0RkFBRCxDQUE1QixDQVRXLEVBVVgsQ0FBRSxhQUFGLEVBQXFCQSxtQkFBTyxDQUFDLGtHQUFELENBQTVCLENBVlcsRUFXWCxDQUFFLFFBQUYsRUFBcUJBLG1CQUFPLENBQUMsd0ZBQUQsQ0FBNUIsQ0FYVyxDQUFiO0FBY0EsSUFBSWlYLE9BQU8sR0FBRyxDQUNaLENBQUUsZUFBRixFQUFxQmpYLG1CQUFPLENBQUMsc0dBQUQsQ0FBNUIsQ0FEWSxFQUVaLENBQUUsZUFBRixFQUFxQkEsbUJBQU8sQ0FBQyxzR0FBRCxDQUFQLENBQXdDa1gsV0FBN0QsQ0FGWSxFQUdaLENBQUUsVUFBRixFQUFxQmxYLG1CQUFPLENBQUMsNEZBQUQsQ0FBUCxDQUFtQ2tYLFdBQXhELENBSFksRUFJWixDQUFFLGVBQUYsRUFBcUJsWCxtQkFBTyxDQUFDLHNHQUFELENBQTVCLENBSlksQ0FBZDtBQVFBOzs7O0FBR0EsU0FBUzJULFlBQVQsR0FBd0I7QUFDdEIsTUFBSXJWLENBQUo7QUFFQTs7Ozs7O0FBS0EsT0FBS3lPLEtBQUwsR0FBYSxJQUFJdUosS0FBSixFQUFiOztBQUVBLE9BQUtoWSxDQUFDLEdBQUcsQ0FBVCxFQUFZQSxDQUFDLEdBQUdpWSxNQUFNLENBQUNsWSxNQUF2QixFQUErQkMsQ0FBQyxFQUFoQyxFQUFvQztBQUNsQyxTQUFLeU8sS0FBTCxDQUFXck4sSUFBWCxDQUFnQjZXLE1BQU0sQ0FBQ2pZLENBQUQsQ0FBTixDQUFVLENBQVYsQ0FBaEIsRUFBOEJpWSxNQUFNLENBQUNqWSxDQUFELENBQU4sQ0FBVSxDQUFWLENBQTlCO0FBQ0Q7QUFFRDs7Ozs7Ozs7QUFNQSxPQUFLa1gsTUFBTCxHQUFjLElBQUljLEtBQUosRUFBZDs7QUFFQSxPQUFLaFksQ0FBQyxHQUFHLENBQVQsRUFBWUEsQ0FBQyxHQUFHMlksT0FBTyxDQUFDNVksTUFBeEIsRUFBZ0NDLENBQUMsRUFBakMsRUFBcUM7QUFDbkMsU0FBS2tYLE1BQUwsQ0FBWTlWLElBQVosQ0FBaUJ1WCxPQUFPLENBQUMzWSxDQUFELENBQVAsQ0FBVyxDQUFYLENBQWpCLEVBQWdDMlksT0FBTyxDQUFDM1ksQ0FBRCxDQUFQLENBQVcsQ0FBWCxDQUFoQztBQUNEO0FBQ0YsQyxDQUdEO0FBQ0E7QUFDQTs7O0FBQ0FxVixZQUFZLENBQUNuVCxTQUFiLENBQXVCNlMsU0FBdkIsR0FBbUMsVUFBVWpJLEtBQVYsRUFBaUI7QUFDbEQsTUFBSXlILEVBQUo7QUFBQSxNQUFRdlUsQ0FBUjtBQUFBLE1BQVd5RSxHQUFHLEdBQUdxSSxLQUFLLENBQUNySSxHQUF2QjtBQUFBLE1BQ0lvSyxLQUFLLEdBQUcsS0FBS0osS0FBTCxDQUFXeUosUUFBWCxDQUFvQixFQUFwQixDQURaO0FBQUEsTUFFSXBZLEdBQUcsR0FBRytPLEtBQUssQ0FBQzlPLE1BRmhCO0FBQUEsTUFHSXFZLFVBQVUsR0FBR3RMLEtBQUssQ0FBQ25CLEVBQU4sQ0FBU3JDLE9BQVQsQ0FBaUI4TyxVQUhsQztBQUFBLE1BSUlTLEtBQUssR0FBRy9MLEtBQUssQ0FBQytMLEtBSmxCOztBQU9BLE1BQUksT0FBT0EsS0FBSyxDQUFDcFUsR0FBRCxDQUFaLEtBQXNCLFdBQTFCLEVBQXVDO0FBQ3JDcUksU0FBSyxDQUFDckksR0FBTixHQUFZb1UsS0FBSyxDQUFDcFUsR0FBRCxDQUFqQjtBQUNBO0FBQ0Q7O0FBRUQsTUFBSXFJLEtBQUssQ0FBQ3VILEtBQU4sR0FBYytELFVBQWxCLEVBQThCO0FBQzVCLFNBQUtwWSxDQUFDLEdBQUcsQ0FBVCxFQUFZQSxDQUFDLEdBQUdGLEdBQWhCLEVBQXFCRSxDQUFDLEVBQXRCLEVBQTBCO0FBQ3hCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E4TSxXQUFLLENBQUN1SCxLQUFOO0FBQ0FFLFFBQUUsR0FBRzFGLEtBQUssQ0FBQzdPLENBQUQsQ0FBTCxDQUFTOE0sS0FBVCxFQUFnQixJQUFoQixDQUFMO0FBQ0FBLFdBQUssQ0FBQ3VILEtBQU47O0FBRUEsVUFBSUUsRUFBSixFQUFRO0FBQUU7QUFBUTtBQUNuQjtBQUNGLEdBWkQsTUFZTztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQXpILFNBQUssQ0FBQ3JJLEdBQU4sR0FBWXFJLEtBQUssQ0FBQzhILE1BQWxCO0FBQ0Q7O0FBRUQsTUFBSSxDQUFDTCxFQUFMLEVBQVM7QUFBRXpILFNBQUssQ0FBQ3JJLEdBQU47QUFBYzs7QUFDekJvVSxPQUFLLENBQUNwVSxHQUFELENBQUwsR0FBYXFJLEtBQUssQ0FBQ3JJLEdBQW5CO0FBQ0QsQ0ExQ0QsQyxDQTZDQTtBQUNBOzs7QUFDQTRRLFlBQVksQ0FBQ25ULFNBQWIsQ0FBdUJxTSxRQUF2QixHQUFrQyxVQUFVekIsS0FBVixFQUFpQjtBQUNqRCxNQUFJeUgsRUFBSjtBQUFBLE1BQVF2VSxDQUFSO0FBQUEsTUFDSTZPLEtBQUssR0FBRyxLQUFLSixLQUFMLENBQVd5SixRQUFYLENBQW9CLEVBQXBCLENBRFo7QUFBQSxNQUVJcFksR0FBRyxHQUFHK08sS0FBSyxDQUFDOU8sTUFGaEI7QUFBQSxNQUdJMkksR0FBRyxHQUFHb0UsS0FBSyxDQUFDOEgsTUFIaEI7QUFBQSxNQUlJd0QsVUFBVSxHQUFHdEwsS0FBSyxDQUFDbkIsRUFBTixDQUFTckMsT0FBVCxDQUFpQjhPLFVBSmxDOztBQU1BLFNBQU90TCxLQUFLLENBQUNySSxHQUFOLEdBQVlpRSxHQUFuQixFQUF3QjtBQUN0QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQSxRQUFJb0UsS0FBSyxDQUFDdUgsS0FBTixHQUFjK0QsVUFBbEIsRUFBOEI7QUFDNUIsV0FBS3BZLENBQUMsR0FBRyxDQUFULEVBQVlBLENBQUMsR0FBR0YsR0FBaEIsRUFBcUJFLENBQUMsRUFBdEIsRUFBMEI7QUFDeEJ1VSxVQUFFLEdBQUcxRixLQUFLLENBQUM3TyxDQUFELENBQUwsQ0FBUzhNLEtBQVQsRUFBZ0IsS0FBaEIsQ0FBTDs7QUFDQSxZQUFJeUgsRUFBSixFQUFRO0FBQUU7QUFBUTtBQUNuQjtBQUNGOztBQUVELFFBQUlBLEVBQUosRUFBUTtBQUNOLFVBQUl6SCxLQUFLLENBQUNySSxHQUFOLElBQWFpRSxHQUFqQixFQUFzQjtBQUFFO0FBQVE7O0FBQ2hDO0FBQ0Q7O0FBRURvRSxTQUFLLENBQUNnTSxPQUFOLElBQWlCaE0sS0FBSyxDQUFDZSxHQUFOLENBQVVmLEtBQUssQ0FBQ3JJLEdBQU4sRUFBVixDQUFqQjtBQUNEOztBQUVELE1BQUlxSSxLQUFLLENBQUNnTSxPQUFWLEVBQW1CO0FBQ2pCaE0sU0FBSyxDQUFDaU0sV0FBTjtBQUNEO0FBQ0YsQ0FqQ0Q7QUFvQ0E7Ozs7Ozs7QUFLQTFELFlBQVksQ0FBQ25ULFNBQWIsQ0FBdUI2VCxLQUF2QixHQUErQixVQUFVcFMsR0FBVixFQUFlZ0ksRUFBZixFQUFtQk8sR0FBbkIsRUFBd0JzTSxTQUF4QixFQUFtQztBQUNoRSxNQUFJeFksQ0FBSixFQUFPNk8sS0FBUCxFQUFjL08sR0FBZDtBQUNBLE1BQUlnTixLQUFLLEdBQUcsSUFBSSxLQUFLNkssS0FBVCxDQUFlaFUsR0FBZixFQUFvQmdJLEVBQXBCLEVBQXdCTyxHQUF4QixFQUE2QnNNLFNBQTdCLENBQVo7QUFFQSxPQUFLakssUUFBTCxDQUFjekIsS0FBZDtBQUVBK0IsT0FBSyxHQUFHLEtBQUtxSSxNQUFMLENBQVlnQixRQUFaLENBQXFCLEVBQXJCLENBQVI7QUFDQXBZLEtBQUcsR0FBRytPLEtBQUssQ0FBQzlPLE1BQVo7O0FBRUEsT0FBS0MsQ0FBQyxHQUFHLENBQVQsRUFBWUEsQ0FBQyxHQUFHRixHQUFoQixFQUFxQkUsQ0FBQyxFQUF0QixFQUEwQjtBQUN4QjZPLFNBQUssQ0FBQzdPLENBQUQsQ0FBTCxDQUFTOE0sS0FBVDtBQUNEO0FBQ0YsQ0FaRDs7QUFlQXVJLFlBQVksQ0FBQ25ULFNBQWIsQ0FBdUJ5VixLQUF2QixHQUErQmpXLG1CQUFPLENBQUMsb0dBQUQsQ0FBdEM7QUFHQXZCLE1BQU0sQ0FBQ0MsT0FBUCxHQUFpQmlWLFlBQWpCLEM7Ozs7Ozs7Ozs7OztBQ2hMQTtBQUVhOztBQUdibFYsTUFBTSxDQUFDQyxPQUFQLEdBQWlCO0FBQ2ZrSixTQUFPLEVBQUU7QUFDUDBQLFFBQUksRUFBVSxJQURQO0FBQ3FCO0FBQzVCQyxZQUFRLEVBQU0sSUFGUDtBQUVxQjtBQUM1QkMsVUFBTSxFQUFRLEtBSFA7QUFHcUI7QUFDNUJDLGNBQVUsRUFBSSxXQUpQO0FBSXFCO0FBQzVCdkMsV0FBTyxFQUFPLEtBTFA7QUFLcUI7QUFFNUI7QUFDQXdDLGVBQVcsRUFBRyxLQVJQO0FBVVA7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBQyxVQUFNLEVBQUUsMEJBZkQ7O0FBZTZCO0FBRXBDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBQyxhQUFTLEVBQUUsSUF2Qko7QUF5QlBsQixjQUFVLEVBQUksRUF6QlAsQ0F5QnFCOztBQXpCckIsR0FETTtBQTZCZnJCLFlBQVUsRUFBRTtBQUVWSixRQUFJLEVBQUU7QUFDSjlILFdBQUssRUFBRSxDQUNMLFdBREssRUFFTCxPQUZLLEVBR0wsUUFISztBQURILEtBRkk7QUFVVlIsU0FBSyxFQUFFO0FBQ0xRLFdBQUssRUFBRSxDQUNMLFlBREssRUFFTCxNQUZLLEVBR0wsT0FISyxFQUlMLFNBSkssRUFLTCxJQUxLLEVBTUwsWUFOSyxFQU9MLFVBUEssRUFRTCxNQVJLLEVBU0wsV0FUSyxFQVVMLFdBVks7QUFERixLQVZHO0FBeUJWaUcsVUFBTSxFQUFFO0FBQ05qRyxXQUFLLEVBQUUsQ0FDTCxVQURLLEVBRUwsV0FGSyxFQUdMLFVBSEssRUFJTCxRQUpLLEVBS0wsUUFMSyxFQU1MLGFBTkssRUFPTCxPQVBLLEVBUUwsTUFSSyxFQVNMLFNBVEssRUFVTCxNQVZLLENBREQ7QUFhTm9JLFlBQU0sRUFBRSxDQUNOLGVBRE0sRUFFTixVQUZNLEVBR04sZUFITTtBQWJGO0FBekJFO0FBN0JHLENBQWpCLEM7Ozs7Ozs7Ozs7OztBQ0xBO0FBRWE7O0FBR2I5VyxNQUFNLENBQUNDLE9BQVAsR0FBaUI7QUFDZmtKLFNBQU8sRUFBRTtBQUNQMFAsUUFBSSxFQUFVLEtBRFA7QUFDcUI7QUFDNUJDLFlBQVEsRUFBTSxLQUZQO0FBRXFCO0FBQzVCQyxVQUFNLEVBQVEsS0FIUDtBQUdxQjtBQUM1QkMsY0FBVSxFQUFJLFdBSlA7QUFJcUI7QUFDNUJ2QyxXQUFPLEVBQU8sS0FMUDtBQUtxQjtBQUU1QjtBQUNBd0MsZUFBVyxFQUFHLEtBUlA7QUFVUDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0FDLFVBQU0sRUFBRSwwQkFmRDs7QUFlNkI7QUFFcEM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0FDLGFBQVMsRUFBRSxJQXZCSjtBQXlCUGxCLGNBQVUsRUFBSSxHQXpCUCxDQXlCc0I7O0FBekJ0QixHQURNO0FBNkJmckIsWUFBVSxFQUFFO0FBRVZKLFFBQUksRUFBRSxFQUZJO0FBR1Z0SSxTQUFLLEVBQUUsRUFIRztBQUlWeUcsVUFBTSxFQUFFO0FBSkU7QUE3QkcsQ0FBakIsQzs7Ozs7Ozs7Ozs7O0FDTEE7QUFDQTtBQUVhOztBQUdiM1UsTUFBTSxDQUFDQyxPQUFQLEdBQWlCO0FBQ2ZrSixTQUFPLEVBQUU7QUFDUDBQLFFBQUksRUFBVSxLQURQO0FBQ3FCO0FBQzVCQyxZQUFRLEVBQU0sS0FGUDtBQUVxQjtBQUM1QkMsVUFBTSxFQUFRLEtBSFA7QUFHcUI7QUFDNUJDLGNBQVUsRUFBSSxXQUpQO0FBSXFCO0FBQzVCdkMsV0FBTyxFQUFPLEtBTFA7QUFLcUI7QUFFNUI7QUFDQXdDLGVBQVcsRUFBRyxLQVJQO0FBVVA7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBQyxVQUFNLEVBQUUsMEJBZkQ7O0FBZTZCO0FBRXBDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBQyxhQUFTLEVBQUUsSUF2Qko7QUF5QlBsQixjQUFVLEVBQUksRUF6QlAsQ0F5QnFCOztBQXpCckIsR0FETTtBQTZCZnJCLFlBQVUsRUFBRTtBQUVWSixRQUFJLEVBQUU7QUFDSjlILFdBQUssRUFBRSxDQUNMLFdBREssRUFFTCxPQUZLLEVBR0wsUUFISztBQURILEtBRkk7QUFVVlIsU0FBSyxFQUFFO0FBQ0xRLFdBQUssRUFBRSxDQUNMLFdBREs7QUFERixLQVZHO0FBZ0JWaUcsVUFBTSxFQUFFO0FBQ05qRyxXQUFLLEVBQUUsQ0FDTCxNQURLLENBREQ7QUFJTm9JLFlBQU0sRUFBRSxDQUNOLGVBRE0sRUFFTixlQUZNO0FBSkY7QUFoQkU7QUE3QkcsQ0FBakIsQzs7Ozs7Ozs7Ozs7O0FDTkE7Ozs7Ozs7QUFPYTs7QUFHYixJQUFJblUsTUFBTSxHQUFZcEIsbUJBQU8sQ0FBQywwRUFBRCxDQUFQLENBQTBCb0IsTUFBaEQ7O0FBQ0EsSUFBSWdRLFdBQVcsR0FBT3BSLG1CQUFPLENBQUMsMEVBQUQsQ0FBUCxDQUEwQm9SLFdBQWhEOztBQUNBLElBQUlRLFVBQVUsR0FBUTVSLG1CQUFPLENBQUMsMEVBQUQsQ0FBUCxDQUEwQjRSLFVBQWhELEMsQ0FHQTs7O0FBRUEsSUFBSWlHLGFBQWEsR0FBRyxFQUFwQjs7QUFHQUEsYUFBYSxDQUFDQyxXQUFkLEdBQTRCLFVBQVV4TixNQUFWLEVBQWtCdkIsR0FBbEIsRUFBdUJuQixPQUF2QixFQUFnQzRDLEdBQWhDLEVBQXFDdU4sR0FBckMsRUFBMEM7QUFDcEUsTUFBSXBNLEtBQUssR0FBR3JCLE1BQU0sQ0FBQ3ZCLEdBQUQsQ0FBbEI7QUFFQSxTQUFRLFVBQVVnUCxHQUFHLENBQUNDLFdBQUosQ0FBZ0JyTSxLQUFoQixDQUFWLEdBQW1DLEdBQW5DLEdBQ0FpRyxVQUFVLENBQUN0SCxNQUFNLENBQUN2QixHQUFELENBQU4sQ0FBWWtQLE9BQWIsQ0FEVixHQUVBLFNBRlI7QUFHRCxDQU5EOztBQVNBSixhQUFhLENBQUNLLFVBQWQsR0FBMkIsVUFBVTVOLE1BQVYsRUFBa0J2QixHQUFsQixFQUF1Qm5CLE9BQXZCLEVBQWdDNEMsR0FBaEMsRUFBcUN1TixHQUFyQyxFQUEwQztBQUNuRSxNQUFJcE0sS0FBSyxHQUFHckIsTUFBTSxDQUFDdkIsR0FBRCxDQUFsQjtBQUVBLFNBQVEsU0FBU2dQLEdBQUcsQ0FBQ0MsV0FBSixDQUFnQnJNLEtBQWhCLENBQVQsR0FBa0MsU0FBbEMsR0FDQWlHLFVBQVUsQ0FBQ3RILE1BQU0sQ0FBQ3ZCLEdBQUQsQ0FBTixDQUFZa1AsT0FBYixDQURWLEdBRUEsaUJBRlI7QUFHRCxDQU5EOztBQVNBSixhQUFhLENBQUNNLEtBQWQsR0FBc0IsVUFBVTdOLE1BQVYsRUFBa0J2QixHQUFsQixFQUF1Qm5CLE9BQXZCLEVBQWdDNEMsR0FBaEMsRUFBcUN1TixHQUFyQyxFQUEwQztBQUM5RCxNQUFJcE0sS0FBSyxHQUFHckIsTUFBTSxDQUFDdkIsR0FBRCxDQUFsQjtBQUFBLE1BQ0k2RCxJQUFJLEdBQUdqQixLQUFLLENBQUNpQixJQUFOLEdBQWF3RSxXQUFXLENBQUN6RixLQUFLLENBQUNpQixJQUFQLENBQVgsQ0FBd0J4QyxJQUF4QixFQUFiLEdBQThDLEVBRHpEO0FBQUEsTUFFSWdPLFFBQVEsR0FBRyxFQUZmO0FBQUEsTUFHSUMsV0FISjtBQUFBLE1BR2lCL1osQ0FIakI7QUFBQSxNQUdvQmdhLFFBSHBCO0FBQUEsTUFHOEJDLFFBSDlCOztBQUtBLE1BQUkzTCxJQUFKLEVBQVU7QUFDUndMLFlBQVEsR0FBR3hMLElBQUksQ0FBQ3pJLEtBQUwsQ0FBVyxNQUFYLEVBQW1CLENBQW5CLENBQVg7QUFDRDs7QUFFRCxNQUFJeUQsT0FBTyxDQUFDZ1EsU0FBWixFQUF1QjtBQUNyQlMsZUFBVyxHQUFHelEsT0FBTyxDQUFDZ1EsU0FBUixDQUFrQmpNLEtBQUssQ0FBQ3NNLE9BQXhCLEVBQWlDRyxRQUFqQyxLQUE4Q3hHLFVBQVUsQ0FBQ2pHLEtBQUssQ0FBQ3NNLE9BQVAsQ0FBdEU7QUFDRCxHQUZELE1BRU87QUFDTEksZUFBVyxHQUFHekcsVUFBVSxDQUFDakcsS0FBSyxDQUFDc00sT0FBUCxDQUF4QjtBQUNEOztBQUVELE1BQUlJLFdBQVcsQ0FBQzdQLE9BQVosQ0FBb0IsTUFBcEIsTUFBZ0MsQ0FBcEMsRUFBdUM7QUFDckMsV0FBTzZQLFdBQVcsR0FBRyxJQUFyQjtBQUNELEdBbEI2RCxDQW9COUQ7QUFDQTtBQUNBOzs7QUFDQSxNQUFJekwsSUFBSixFQUFVO0FBQ1J0TyxLQUFDLEdBQVVxTixLQUFLLENBQUM2QyxTQUFOLENBQWdCLE9BQWhCLENBQVg7QUFDQThKLFlBQVEsR0FBRzNNLEtBQUssQ0FBQzRDLEtBQU4sR0FBYzVDLEtBQUssQ0FBQzRDLEtBQU4sQ0FBWXZOLEtBQVosRUFBZCxHQUFvQyxFQUEvQzs7QUFFQSxRQUFJMUMsQ0FBQyxHQUFHLENBQVIsRUFBVztBQUNUZ2EsY0FBUSxDQUFDNVksSUFBVCxDQUFjLENBQUUsT0FBRixFQUFXa0ksT0FBTyxDQUFDNlAsVUFBUixHQUFxQlcsUUFBaEMsQ0FBZDtBQUNELEtBRkQsTUFFTztBQUNMRSxjQUFRLENBQUNoYSxDQUFELENBQVIsQ0FBWSxDQUFaLEtBQWtCLE1BQU1zSixPQUFPLENBQUM2UCxVQUFkLEdBQTJCVyxRQUE3QztBQUNELEtBUk8sQ0FVUjs7O0FBQ0FHLFlBQVEsR0FBRztBQUNUaEssV0FBSyxFQUFFK0o7QUFERSxLQUFYO0FBSUEsV0FBUSxlQUFlUCxHQUFHLENBQUNDLFdBQUosQ0FBZ0JPLFFBQWhCLENBQWYsR0FBMkMsR0FBM0MsR0FDQUYsV0FEQSxHQUVBLGlCQUZSO0FBR0Q7O0FBR0QsU0FBUSxlQUFlTixHQUFHLENBQUNDLFdBQUosQ0FBZ0JyTSxLQUFoQixDQUFmLEdBQXdDLEdBQXhDLEdBQ0EwTSxXQURBLEdBRUEsaUJBRlI7QUFHRCxDQS9DRDs7QUFrREFSLGFBQWEsQ0FBQ1csS0FBZCxHQUFzQixVQUFVbE8sTUFBVixFQUFrQnZCLEdBQWxCLEVBQXVCbkIsT0FBdkIsRUFBZ0M0QyxHQUFoQyxFQUFxQ3VOLEdBQXJDLEVBQTBDO0FBQzlELE1BQUlwTSxLQUFLLEdBQUdyQixNQUFNLENBQUN2QixHQUFELENBQWxCLENBRDhELENBRzlEO0FBQ0E7QUFDQTtBQUNBOztBQUVBNEMsT0FBSyxDQUFDNEMsS0FBTixDQUFZNUMsS0FBSyxDQUFDNkMsU0FBTixDQUFnQixLQUFoQixDQUFaLEVBQW9DLENBQXBDLElBQ0V1SixHQUFHLENBQUNVLGtCQUFKLENBQXVCOU0sS0FBSyxDQUFDK00sUUFBN0IsRUFBdUM5USxPQUF2QyxFQUFnRDRDLEdBQWhELENBREY7QUFHQSxTQUFPdU4sR0FBRyxDQUFDcE4sV0FBSixDQUFnQkwsTUFBaEIsRUFBd0J2QixHQUF4QixFQUE2Qm5CLE9BQTdCLENBQVA7QUFDRCxDQVpEOztBQWVBaVEsYUFBYSxDQUFDYyxTQUFkLEdBQTBCLFVBQVVyTyxNQUFWLEVBQWtCdkIsR0FBbEIsRUFBdUJuQjtBQUFRO0FBQS9CLEVBQTJDO0FBQ25FLFNBQU9BLE9BQU8sQ0FBQzJQLFFBQVIsR0FBbUIsVUFBbkIsR0FBZ0MsUUFBdkM7QUFDRCxDQUZEOztBQUdBTSxhQUFhLENBQUNlLFNBQWQsR0FBMEIsVUFBVXRPLE1BQVYsRUFBa0J2QixHQUFsQixFQUF1Qm5CO0FBQVE7QUFBL0IsRUFBMkM7QUFDbkUsU0FBT0EsT0FBTyxDQUFDNFAsTUFBUixHQUFrQjVQLE9BQU8sQ0FBQzJQLFFBQVIsR0FBbUIsVUFBbkIsR0FBZ0MsUUFBbEQsR0FBOEQsSUFBckU7QUFDRCxDQUZEOztBQUtBTSxhQUFhLENBQUMvVSxJQUFkLEdBQXFCLFVBQVV3SCxNQUFWLEVBQWtCdkI7QUFBSTtBQUF0QixFQUEyQztBQUM5RCxTQUFPNkksVUFBVSxDQUFDdEgsTUFBTSxDQUFDdkIsR0FBRCxDQUFOLENBQVlrUCxPQUFiLENBQWpCO0FBQ0QsQ0FGRDs7QUFLQUosYUFBYSxDQUFDZ0IsVUFBZCxHQUEyQixVQUFVdk8sTUFBVixFQUFrQnZCO0FBQUk7QUFBdEIsRUFBMkM7QUFDcEUsU0FBT3VCLE1BQU0sQ0FBQ3ZCLEdBQUQsQ0FBTixDQUFZa1AsT0FBbkI7QUFDRCxDQUZEOztBQUdBSixhQUFhLENBQUNpQixXQUFkLEdBQTRCLFVBQVV4TyxNQUFWLEVBQWtCdkI7QUFBSTtBQUF0QixFQUEyQztBQUNyRSxTQUFPdUIsTUFBTSxDQUFDdkIsR0FBRCxDQUFOLENBQVlrUCxPQUFuQjtBQUNELENBRkQ7QUFLQTs7Ozs7OztBQUtBLFNBQVN6RSxRQUFULEdBQW9CO0FBRWxCOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBNEJBLE9BQUtyRyxLQUFMLEdBQWEvTCxNQUFNLENBQUMsRUFBRCxFQUFLeVcsYUFBTCxDQUFuQjtBQUNEO0FBR0Q7Ozs7Ozs7QUFLQXJFLFFBQVEsQ0FBQ2hULFNBQVQsQ0FBbUJ3WCxXQUFuQixHQUFpQyxTQUFTQSxXQUFULENBQXFCck0sS0FBckIsRUFBNEI7QUFDM0QsTUFBSXJOLENBQUosRUFBTzBZLENBQVAsRUFBVXZPLE1BQVY7O0FBRUEsTUFBSSxDQUFDa0QsS0FBSyxDQUFDNEMsS0FBWCxFQUFrQjtBQUFFLFdBQU8sRUFBUDtBQUFZOztBQUVoQzlGLFFBQU0sR0FBRyxFQUFUOztBQUVBLE9BQUtuSyxDQUFDLEdBQUcsQ0FBSixFQUFPMFksQ0FBQyxHQUFHckwsS0FBSyxDQUFDNEMsS0FBTixDQUFZbFEsTUFBNUIsRUFBb0NDLENBQUMsR0FBRzBZLENBQXhDLEVBQTJDMVksQ0FBQyxFQUE1QyxFQUFnRDtBQUM5Q21LLFVBQU0sSUFBSSxNQUFNbUosVUFBVSxDQUFDakcsS0FBSyxDQUFDNEMsS0FBTixDQUFZalEsQ0FBWixFQUFlLENBQWYsQ0FBRCxDQUFoQixHQUFzQyxJQUF0QyxHQUE2Q3NULFVBQVUsQ0FBQ2pHLEtBQUssQ0FBQzRDLEtBQU4sQ0FBWWpRLENBQVosRUFBZSxDQUFmLENBQUQsQ0FBdkQsR0FBNkUsR0FBdkY7QUFDRDs7QUFFRCxTQUFPbUssTUFBUDtBQUNELENBWkQ7QUFlQTs7Ozs7Ozs7Ozs7QUFTQStLLFFBQVEsQ0FBQ2hULFNBQVQsQ0FBbUJtSyxXQUFuQixHQUFpQyxTQUFTQSxXQUFULENBQXFCTCxNQUFyQixFQUE2QnZCLEdBQTdCLEVBQWtDbkIsT0FBbEMsRUFBMkM7QUFDMUUsTUFBSW1SLFNBQUo7QUFBQSxNQUNJdFEsTUFBTSxHQUFHLEVBRGI7QUFBQSxNQUVJdVEsTUFBTSxHQUFHLEtBRmI7QUFBQSxNQUdJck4sS0FBSyxHQUFHckIsTUFBTSxDQUFDdkIsR0FBRCxDQUhsQixDQUQwRSxDQU0xRTs7QUFDQSxNQUFJNEMsS0FBSyxDQUFDc04sTUFBVixFQUFrQjtBQUNoQixXQUFPLEVBQVA7QUFDRCxHQVR5RSxDQVcxRTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0EsTUFBSXROLEtBQUssQ0FBQ2dCLEtBQU4sSUFBZWhCLEtBQUssQ0FBQ2xCLE9BQU4sS0FBa0IsQ0FBQyxDQUFsQyxJQUF1QzFCLEdBQXZDLElBQThDdUIsTUFBTSxDQUFDdkIsR0FBRyxHQUFHLENBQVAsQ0FBTixDQUFnQmtRLE1BQWxFLEVBQTBFO0FBQ3hFeFEsVUFBTSxJQUFJLElBQVY7QUFDRCxHQXBCeUUsQ0FzQjFFOzs7QUFDQUEsUUFBTSxJQUFJLENBQUNrRCxLQUFLLENBQUNsQixPQUFOLEtBQWtCLENBQUMsQ0FBbkIsR0FBdUIsSUFBdkIsR0FBOEIsR0FBL0IsSUFBc0NrQixLQUFLLENBQUM2QixHQUF0RCxDQXZCMEUsQ0F5QjFFOztBQUNBL0UsUUFBTSxJQUFJLEtBQUt1UCxXQUFMLENBQWlCck0sS0FBakIsQ0FBVixDQTFCMEUsQ0E0QjFFOztBQUNBLE1BQUlBLEtBQUssQ0FBQ2xCLE9BQU4sS0FBa0IsQ0FBbEIsSUFBdUI3QyxPQUFPLENBQUMyUCxRQUFuQyxFQUE2QztBQUMzQzlPLFVBQU0sSUFBSSxJQUFWO0FBQ0QsR0EvQnlFLENBaUMxRTs7O0FBQ0EsTUFBSWtELEtBQUssQ0FBQ2dCLEtBQVYsRUFBaUI7QUFDZnFNLFVBQU0sR0FBRyxJQUFUOztBQUVBLFFBQUlyTixLQUFLLENBQUNsQixPQUFOLEtBQWtCLENBQXRCLEVBQXlCO0FBQ3ZCLFVBQUkxQixHQUFHLEdBQUcsQ0FBTixHQUFVdUIsTUFBTSxDQUFDak0sTUFBckIsRUFBNkI7QUFDM0IwYSxpQkFBUyxHQUFHek8sTUFBTSxDQUFDdkIsR0FBRyxHQUFHLENBQVAsQ0FBbEI7O0FBRUEsWUFBSWdRLFNBQVMsQ0FBQ0csSUFBVixLQUFtQixRQUFuQixJQUErQkgsU0FBUyxDQUFDRSxNQUE3QyxFQUFxRDtBQUNuRDtBQUNBO0FBQ0FELGdCQUFNLEdBQUcsS0FBVDtBQUVELFNBTEQsTUFLTyxJQUFJRCxTQUFTLENBQUN0TyxPQUFWLEtBQXNCLENBQUMsQ0FBdkIsSUFBNEJzTyxTQUFTLENBQUN2TCxHQUFWLEtBQWtCN0IsS0FBSyxDQUFDNkIsR0FBeEQsRUFBNkQ7QUFDbEU7QUFDQTtBQUNBd0wsZ0JBQU0sR0FBRyxLQUFUO0FBQ0Q7QUFDRjtBQUNGO0FBQ0Y7O0FBRUR2USxRQUFNLElBQUl1USxNQUFNLEdBQUcsS0FBSCxHQUFXLEdBQTNCO0FBRUEsU0FBT3ZRLE1BQVA7QUFDRCxDQTFERDtBQTZEQTs7Ozs7Ozs7OztBQVFBK0ssUUFBUSxDQUFDaFQsU0FBVCxDQUFtQjZWLFlBQW5CLEdBQWtDLFVBQVUvTCxNQUFWLEVBQWtCMUMsT0FBbEIsRUFBMkI0QyxHQUEzQixFQUFnQztBQUNoRSxNQUFJME8sSUFBSjtBQUFBLE1BQ0l6USxNQUFNLEdBQUcsRUFEYjtBQUFBLE1BRUkwRSxLQUFLLEdBQUcsS0FBS0EsS0FGakI7O0FBSUEsT0FBSyxJQUFJN08sQ0FBQyxHQUFHLENBQVIsRUFBV0YsR0FBRyxHQUFHa00sTUFBTSxDQUFDak0sTUFBN0IsRUFBcUNDLENBQUMsR0FBR0YsR0FBekMsRUFBOENFLENBQUMsRUFBL0MsRUFBbUQ7QUFDakQ0YSxRQUFJLEdBQUc1TyxNQUFNLENBQUNoTSxDQUFELENBQU4sQ0FBVTRhLElBQWpCOztBQUVBLFFBQUksT0FBTy9MLEtBQUssQ0FBQytMLElBQUQsQ0FBWixLQUF1QixXQUEzQixFQUF3QztBQUN0Q3pRLFlBQU0sSUFBSTBFLEtBQUssQ0FBQytMLElBQUQsQ0FBTCxDQUFZNU8sTUFBWixFQUFvQmhNLENBQXBCLEVBQXVCc0osT0FBdkIsRUFBZ0M0QyxHQUFoQyxFQUFxQyxJQUFyQyxDQUFWO0FBQ0QsS0FGRCxNQUVPO0FBQ0wvQixZQUFNLElBQUksS0FBS2tDLFdBQUwsQ0FBaUJMLE1BQWpCLEVBQXlCaE0sQ0FBekIsRUFBNEJzSixPQUE1QixDQUFWO0FBQ0Q7QUFDRjs7QUFFRCxTQUFPYSxNQUFQO0FBQ0QsQ0FoQkQ7QUFtQkE7Ozs7Ozs7Ozs7OztBQVVBK0ssUUFBUSxDQUFDaFQsU0FBVCxDQUFtQmlZLGtCQUFuQixHQUF3QyxVQUFVbk8sTUFBVixFQUFrQjFDLE9BQWxCLEVBQTJCNEMsR0FBM0IsRUFBZ0M7QUFDdEUsTUFBSS9CLE1BQU0sR0FBRyxFQUFiOztBQUVBLE9BQUssSUFBSW5LLENBQUMsR0FBRyxDQUFSLEVBQVdGLEdBQUcsR0FBR2tNLE1BQU0sQ0FBQ2pNLE1BQTdCLEVBQXFDQyxDQUFDLEdBQUdGLEdBQXpDLEVBQThDRSxDQUFDLEVBQS9DLEVBQW1EO0FBQ2pELFFBQUlnTSxNQUFNLENBQUNoTSxDQUFELENBQU4sQ0FBVTRhLElBQVYsS0FBbUIsTUFBdkIsRUFBK0I7QUFDN0J6USxZQUFNLElBQUk2QixNQUFNLENBQUNoTSxDQUFELENBQU4sQ0FBVTJaLE9BQXBCO0FBQ0QsS0FGRCxNQUVPLElBQUkzTixNQUFNLENBQUNoTSxDQUFELENBQU4sQ0FBVTRhLElBQVYsS0FBbUIsT0FBdkIsRUFBZ0M7QUFDckN6USxZQUFNLElBQUksS0FBS2dRLGtCQUFMLENBQXdCbk8sTUFBTSxDQUFDaE0sQ0FBRCxDQUFOLENBQVVvYSxRQUFsQyxFQUE0QzlRLE9BQTVDLEVBQXFENEMsR0FBckQsQ0FBVjtBQUNEO0FBQ0Y7O0FBRUQsU0FBTy9CLE1BQVA7QUFDRCxDQVpEO0FBZUE7Ozs7Ozs7Ozs7O0FBU0ErSyxRQUFRLENBQUNoVCxTQUFULENBQW1CMEssTUFBbkIsR0FBNEIsVUFBVVosTUFBVixFQUFrQjFDLE9BQWxCLEVBQTJCNEMsR0FBM0IsRUFBZ0M7QUFDMUQsTUFBSWxNLENBQUo7QUFBQSxNQUFPRixHQUFQO0FBQUEsTUFBWThhLElBQVo7QUFBQSxNQUNJelEsTUFBTSxHQUFHLEVBRGI7QUFBQSxNQUVJMEUsS0FBSyxHQUFHLEtBQUtBLEtBRmpCOztBQUlBLE9BQUs3TyxDQUFDLEdBQUcsQ0FBSixFQUFPRixHQUFHLEdBQUdrTSxNQUFNLENBQUNqTSxNQUF6QixFQUFpQ0MsQ0FBQyxHQUFHRixHQUFyQyxFQUEwQ0UsQ0FBQyxFQUEzQyxFQUErQztBQUM3QzRhLFFBQUksR0FBRzVPLE1BQU0sQ0FBQ2hNLENBQUQsQ0FBTixDQUFVNGEsSUFBakI7O0FBRUEsUUFBSUEsSUFBSSxLQUFLLFFBQWIsRUFBdUI7QUFDckJ6USxZQUFNLElBQUksS0FBSzROLFlBQUwsQ0FBa0IvTCxNQUFNLENBQUNoTSxDQUFELENBQU4sQ0FBVW9hLFFBQTVCLEVBQXNDOVEsT0FBdEMsRUFBK0M0QyxHQUEvQyxDQUFWO0FBQ0QsS0FGRCxNQUVPLElBQUksT0FBTzJDLEtBQUssQ0FBQytMLElBQUQsQ0FBWixLQUF1QixXQUEzQixFQUF3QztBQUM3Q3pRLFlBQU0sSUFBSTBFLEtBQUssQ0FBQzdDLE1BQU0sQ0FBQ2hNLENBQUQsQ0FBTixDQUFVNGEsSUFBWCxDQUFMLENBQXNCNU8sTUFBdEIsRUFBOEJoTSxDQUE5QixFQUFpQ3NKLE9BQWpDLEVBQTBDNEMsR0FBMUMsRUFBK0MsSUFBL0MsQ0FBVjtBQUNELEtBRk0sTUFFQTtBQUNML0IsWUFBTSxJQUFJLEtBQUtrQyxXQUFMLENBQWlCTCxNQUFqQixFQUF5QmhNLENBQXpCLEVBQTRCc0osT0FBNUIsRUFBcUM0QyxHQUFyQyxDQUFWO0FBQ0Q7QUFDRjs7QUFFRCxTQUFPL0IsTUFBUDtBQUNELENBbEJEOztBQW9CQWhLLE1BQU0sQ0FBQ0MsT0FBUCxHQUFpQjhVLFFBQWpCLEM7Ozs7Ozs7Ozs7OztBQzlVQTs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFpQmE7QUFHYjs7OztBQUdBLFNBQVM4QyxLQUFULEdBQWlCO0FBQ2Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBSzZDLFNBQUwsR0FBaUIsRUFBakIsQ0FWZSxDQVlmO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBQ0EsT0FBS0MsU0FBTCxHQUFpQixJQUFqQjtBQUNELEMsQ0FFRDtBQUNBO0FBR0E7QUFDQTs7O0FBQ0E5QyxLQUFLLENBQUM5VixTQUFOLENBQWdCNlksUUFBaEIsR0FBMkIsVUFBVXBZLElBQVYsRUFBZ0I7QUFDekMsT0FBSyxJQUFJM0MsQ0FBQyxHQUFHLENBQWIsRUFBZ0JBLENBQUMsR0FBRyxLQUFLNmEsU0FBTCxDQUFlOWEsTUFBbkMsRUFBMkNDLENBQUMsRUFBNUMsRUFBZ0Q7QUFDOUMsUUFBSSxLQUFLNmEsU0FBTCxDQUFlN2EsQ0FBZixFQUFrQjJDLElBQWxCLEtBQTJCQSxJQUEvQixFQUFxQztBQUNuQyxhQUFPM0MsQ0FBUDtBQUNEO0FBQ0Y7O0FBQ0QsU0FBTyxDQUFDLENBQVI7QUFDRCxDQVBELEMsQ0FVQTtBQUNBOzs7QUFDQWdZLEtBQUssQ0FBQzlWLFNBQU4sQ0FBZ0I4WSxXQUFoQixHQUE4QixZQUFZO0FBQ3hDLE1BQUl0VyxJQUFJLEdBQUcsSUFBWDtBQUNBLE1BQUl1VyxNQUFNLEdBQUcsQ0FBRSxFQUFGLENBQWIsQ0FGd0MsQ0FJeEM7O0FBQ0F2VyxNQUFJLENBQUNtVyxTQUFMLENBQWU1WCxPQUFmLENBQXVCLFVBQVVpWSxJQUFWLEVBQWdCO0FBQ3JDLFFBQUksQ0FBQ0EsSUFBSSxDQUFDQyxPQUFWLEVBQW1CO0FBQUU7QUFBUzs7QUFFOUJELFFBQUksQ0FBQ3ZNLEdBQUwsQ0FBUzFMLE9BQVQsQ0FBaUIsVUFBVW1ZLE9BQVYsRUFBbUI7QUFDbEMsVUFBSUgsTUFBTSxDQUFDL1EsT0FBUCxDQUFla1IsT0FBZixJQUEwQixDQUE5QixFQUFpQztBQUMvQkgsY0FBTSxDQUFDN1osSUFBUCxDQUFZZ2EsT0FBWjtBQUNEO0FBQ0YsS0FKRDtBQUtELEdBUkQ7O0FBVUExVyxNQUFJLENBQUNvVyxTQUFMLEdBQWlCLEVBQWpCO0FBRUFHLFFBQU0sQ0FBQ2hZLE9BQVAsQ0FBZSxVQUFVb1UsS0FBVixFQUFpQjtBQUM5QjNTLFFBQUksQ0FBQ29XLFNBQUwsQ0FBZXpELEtBQWYsSUFBd0IsRUFBeEI7O0FBQ0EzUyxRQUFJLENBQUNtVyxTQUFMLENBQWU1WCxPQUFmLENBQXVCLFVBQVVpWSxJQUFWLEVBQWdCO0FBQ3JDLFVBQUksQ0FBQ0EsSUFBSSxDQUFDQyxPQUFWLEVBQW1CO0FBQUU7QUFBUzs7QUFFOUIsVUFBSTlELEtBQUssSUFBSTZELElBQUksQ0FBQ3ZNLEdBQUwsQ0FBU3pFLE9BQVQsQ0FBaUJtTixLQUFqQixJQUEwQixDQUF2QyxFQUEwQztBQUFFO0FBQVM7O0FBRXJEM1MsVUFBSSxDQUFDb1csU0FBTCxDQUFlekQsS0FBZixFQUFzQmpXLElBQXRCLENBQTJCOFosSUFBSSxDQUFDRyxFQUFoQztBQUNELEtBTkQ7QUFPRCxHQVREO0FBVUQsQ0EzQkQ7QUE4QkE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQXlCQXJELEtBQUssQ0FBQzlWLFNBQU4sQ0FBZ0JvWixFQUFoQixHQUFxQixVQUFVM1ksSUFBVixFQUFnQjBZLEVBQWhCLEVBQW9CL1IsT0FBcEIsRUFBNkI7QUFDaEQsTUFBSVAsS0FBSyxHQUFHLEtBQUtnUyxRQUFMLENBQWNwWSxJQUFkLENBQVo7O0FBQ0EsTUFBSTRZLEdBQUcsR0FBR2pTLE9BQU8sSUFBSSxFQUFyQjs7QUFFQSxNQUFJUCxLQUFLLEtBQUssQ0FBQyxDQUFmLEVBQWtCO0FBQUUsVUFBTSxJQUFJcEIsS0FBSixDQUFVLDRCQUE0QmhGLElBQXRDLENBQU47QUFBb0Q7O0FBRXhFLE9BQUtrWSxTQUFMLENBQWU5UixLQUFmLEVBQXNCc1MsRUFBdEIsR0FBMkJBLEVBQTNCO0FBQ0EsT0FBS1IsU0FBTCxDQUFlOVIsS0FBZixFQUFzQjRGLEdBQXRCLEdBQTRCNE0sR0FBRyxDQUFDNU0sR0FBSixJQUFXLEVBQXZDO0FBQ0EsT0FBS21NLFNBQUwsR0FBaUIsSUFBakI7QUFDRCxDQVREO0FBWUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBd0JBOUMsS0FBSyxDQUFDOVYsU0FBTixDQUFnQndNLE1BQWhCLEdBQXlCLFVBQVU4TSxVQUFWLEVBQXNCQyxRQUF0QixFQUFnQ0osRUFBaEMsRUFBb0MvUixPQUFwQyxFQUE2QztBQUNwRSxNQUFJUCxLQUFLLEdBQUcsS0FBS2dTLFFBQUwsQ0FBY1MsVUFBZCxDQUFaOztBQUNBLE1BQUlELEdBQUcsR0FBR2pTLE9BQU8sSUFBSSxFQUFyQjs7QUFFQSxNQUFJUCxLQUFLLEtBQUssQ0FBQyxDQUFmLEVBQWtCO0FBQUUsVUFBTSxJQUFJcEIsS0FBSixDQUFVLDRCQUE0QjZULFVBQXRDLENBQU47QUFBMEQ7O0FBRTlFLE9BQUtYLFNBQUwsQ0FBZWEsTUFBZixDQUFzQjNTLEtBQXRCLEVBQTZCLENBQTdCLEVBQWdDO0FBQzlCcEcsUUFBSSxFQUFFOFksUUFEd0I7QUFFOUJOLFdBQU8sRUFBRSxJQUZxQjtBQUc5QkUsTUFBRSxFQUFFQSxFQUgwQjtBQUk5QjFNLE9BQUcsRUFBRTRNLEdBQUcsQ0FBQzVNLEdBQUosSUFBVztBQUpjLEdBQWhDOztBQU9BLE9BQUttTSxTQUFMLEdBQWlCLElBQWpCO0FBQ0QsQ0FkRDtBQWlCQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUF3QkE5QyxLQUFLLENBQUM5VixTQUFOLENBQWdCeVosS0FBaEIsR0FBd0IsVUFBVUMsU0FBVixFQUFxQkgsUUFBckIsRUFBK0JKLEVBQS9CLEVBQW1DL1IsT0FBbkMsRUFBNEM7QUFDbEUsTUFBSVAsS0FBSyxHQUFHLEtBQUtnUyxRQUFMLENBQWNhLFNBQWQsQ0FBWjs7QUFDQSxNQUFJTCxHQUFHLEdBQUdqUyxPQUFPLElBQUksRUFBckI7O0FBRUEsTUFBSVAsS0FBSyxLQUFLLENBQUMsQ0FBZixFQUFrQjtBQUFFLFVBQU0sSUFBSXBCLEtBQUosQ0FBVSw0QkFBNEJpVSxTQUF0QyxDQUFOO0FBQXlEOztBQUU3RSxPQUFLZixTQUFMLENBQWVhLE1BQWYsQ0FBc0IzUyxLQUFLLEdBQUcsQ0FBOUIsRUFBaUMsQ0FBakMsRUFBb0M7QUFDbENwRyxRQUFJLEVBQUU4WSxRQUQ0QjtBQUVsQ04sV0FBTyxFQUFFLElBRnlCO0FBR2xDRSxNQUFFLEVBQUVBLEVBSDhCO0FBSWxDMU0sT0FBRyxFQUFFNE0sR0FBRyxDQUFDNU0sR0FBSixJQUFXO0FBSmtCLEdBQXBDOztBQU9BLE9BQUttTSxTQUFMLEdBQWlCLElBQWpCO0FBQ0QsQ0FkRDtBQWdCQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQXVCQTlDLEtBQUssQ0FBQzlWLFNBQU4sQ0FBZ0JkLElBQWhCLEdBQXVCLFVBQVVxYSxRQUFWLEVBQW9CSixFQUFwQixFQUF3Qi9SLE9BQXhCLEVBQWlDO0FBQ3RELE1BQUlpUyxHQUFHLEdBQUdqUyxPQUFPLElBQUksRUFBckI7O0FBRUEsT0FBS3VSLFNBQUwsQ0FBZXpaLElBQWYsQ0FBb0I7QUFDbEJ1QixRQUFJLEVBQUU4WSxRQURZO0FBRWxCTixXQUFPLEVBQUUsSUFGUztBQUdsQkUsTUFBRSxFQUFFQSxFQUhjO0FBSWxCMU0sT0FBRyxFQUFFNE0sR0FBRyxDQUFDNU0sR0FBSixJQUFXO0FBSkUsR0FBcEI7O0FBT0EsT0FBS21NLFNBQUwsR0FBaUIsSUFBakI7QUFDRCxDQVhEO0FBY0E7Ozs7Ozs7Ozs7Ozs7O0FBWUE5QyxLQUFLLENBQUM5VixTQUFOLENBQWdCaVYsTUFBaEIsR0FBeUIsVUFBVS9NLElBQVYsRUFBZ0JnTixhQUFoQixFQUErQjtBQUN0RCxNQUFJLENBQUNsWCxLQUFLLENBQUNJLE9BQU4sQ0FBYzhKLElBQWQsQ0FBTCxFQUEwQjtBQUFFQSxRQUFJLEdBQUcsQ0FBRUEsSUFBRixDQUFQO0FBQWtCOztBQUU5QyxNQUFJRCxNQUFNLEdBQUcsRUFBYixDQUhzRCxDQUt0RDs7QUFDQUMsTUFBSSxDQUFDbkgsT0FBTCxDQUFhLFVBQVVOLElBQVYsRUFBZ0I7QUFDM0IsUUFBSThILEdBQUcsR0FBRyxLQUFLc1EsUUFBTCxDQUFjcFksSUFBZCxDQUFWOztBQUVBLFFBQUk4SCxHQUFHLEdBQUcsQ0FBVixFQUFhO0FBQ1gsVUFBSTJNLGFBQUosRUFBbUI7QUFBRTtBQUFTOztBQUM5QixZQUFNLElBQUl6UCxLQUFKLENBQVUsc0NBQXNDaEYsSUFBaEQsQ0FBTjtBQUNEOztBQUNELFNBQUtrWSxTQUFMLENBQWVwUSxHQUFmLEVBQW9CMFEsT0FBcEIsR0FBOEIsSUFBOUI7QUFDQWhSLFVBQU0sQ0FBQy9JLElBQVAsQ0FBWXVCLElBQVo7QUFDRCxHQVRELEVBU0csSUFUSDtBQVdBLE9BQUttWSxTQUFMLEdBQWlCLElBQWpCO0FBQ0EsU0FBTzNRLE1BQVA7QUFDRCxDQW5CRDtBQXNCQTs7Ozs7Ozs7Ozs7O0FBVUE2TixLQUFLLENBQUM5VixTQUFOLENBQWdCOFUsVUFBaEIsR0FBNkIsVUFBVTVNLElBQVYsRUFBZ0JnTixhQUFoQixFQUErQjtBQUMxRCxNQUFJLENBQUNsWCxLQUFLLENBQUNJLE9BQU4sQ0FBYzhKLElBQWQsQ0FBTCxFQUEwQjtBQUFFQSxRQUFJLEdBQUcsQ0FBRUEsSUFBRixDQUFQO0FBQWtCOztBQUU5QyxPQUFLeVEsU0FBTCxDQUFlNVgsT0FBZixDQUF1QixVQUFVaVksSUFBVixFQUFnQjtBQUFFQSxRQUFJLENBQUNDLE9BQUwsR0FBZSxLQUFmO0FBQXVCLEdBQWhFOztBQUVBLE9BQUtoRSxNQUFMLENBQVkvTSxJQUFaLEVBQWtCZ04sYUFBbEI7QUFDRCxDQU5EO0FBU0E7Ozs7Ozs7Ozs7Ozs7O0FBWUFZLEtBQUssQ0FBQzlWLFNBQU4sQ0FBZ0JxVixPQUFoQixHQUEwQixVQUFVbk4sSUFBVixFQUFnQmdOLGFBQWhCLEVBQStCO0FBQ3ZELE1BQUksQ0FBQ2xYLEtBQUssQ0FBQ0ksT0FBTixDQUFjOEosSUFBZCxDQUFMLEVBQTBCO0FBQUVBLFFBQUksR0FBRyxDQUFFQSxJQUFGLENBQVA7QUFBa0I7O0FBRTlDLE1BQUlELE1BQU0sR0FBRyxFQUFiLENBSHVELENBS3ZEOztBQUNBQyxNQUFJLENBQUNuSCxPQUFMLENBQWEsVUFBVU4sSUFBVixFQUFnQjtBQUMzQixRQUFJOEgsR0FBRyxHQUFHLEtBQUtzUSxRQUFMLENBQWNwWSxJQUFkLENBQVY7O0FBRUEsUUFBSThILEdBQUcsR0FBRyxDQUFWLEVBQWE7QUFDWCxVQUFJMk0sYUFBSixFQUFtQjtBQUFFO0FBQVM7O0FBQzlCLFlBQU0sSUFBSXpQLEtBQUosQ0FBVSxzQ0FBc0NoRixJQUFoRCxDQUFOO0FBQ0Q7O0FBQ0QsU0FBS2tZLFNBQUwsQ0FBZXBRLEdBQWYsRUFBb0IwUSxPQUFwQixHQUE4QixLQUE5QjtBQUNBaFIsVUFBTSxDQUFDL0ksSUFBUCxDQUFZdUIsSUFBWjtBQUNELEdBVEQsRUFTRyxJQVRIO0FBV0EsT0FBS21ZLFNBQUwsR0FBaUIsSUFBakI7QUFDQSxTQUFPM1EsTUFBUDtBQUNELENBbkJEO0FBc0JBOzs7Ozs7Ozs7OztBQVNBNk4sS0FBSyxDQUFDOVYsU0FBTixDQUFnQmdXLFFBQWhCLEdBQTJCLFVBQVUyRCxTQUFWLEVBQXFCO0FBQzlDLE1BQUksS0FBS2YsU0FBTCxLQUFtQixJQUF2QixFQUE2QjtBQUMzQixTQUFLRSxXQUFMO0FBQ0QsR0FINkMsQ0FLOUM7OztBQUNBLFNBQU8sS0FBS0YsU0FBTCxDQUFlZSxTQUFmLEtBQTZCLEVBQXBDO0FBQ0QsQ0FQRDs7QUFTQTFiLE1BQU0sQ0FBQ0MsT0FBUCxHQUFpQjRYLEtBQWpCLEM7Ozs7Ozs7Ozs7OztBQy9WQTtBQUVhOztBQUViLElBQUl4RSxPQUFPLEdBQUc5UixtQkFBTyxDQUFDLDJFQUFELENBQVAsQ0FBMkI4UixPQUF6Qzs7QUFHQXJULE1BQU0sQ0FBQ0MsT0FBUCxHQUFpQixTQUFTMGIsVUFBVCxDQUFvQmhQLEtBQXBCLEVBQTJCQyxTQUEzQixFQUFzQ0MsT0FBdEMsRUFBK0NDLE1BQS9DLEVBQXVEO0FBQ3RFLE1BQUk4TyxTQUFKO0FBQUEsTUFDSTFJLEVBREo7QUFBQSxNQUVJclQsQ0FGSjtBQUFBLE1BR0lnYyxPQUhKO0FBQUEsTUFJSXRELENBSko7QUFBQSxNQUtJdUQsYUFMSjtBQUFBLE1BTUkzSCxLQU5KO0FBQUEsTUFPSXBILFFBUEo7QUFBQSxNQVFJZ1AsTUFSSjtBQUFBLE1BU0lDLFNBVEo7QUFBQSxNQVVJQyxVQVZKO0FBQUEsTUFXSUMsU0FYSjtBQUFBLE1BWUlDLGFBWko7QUFBQSxNQWFJQyxTQWJKO0FBQUEsTUFjSUMsU0FkSjtBQUFBLE1BZUlDLGdCQWZKO0FBQUEsTUFnQklDLFNBaEJKO0FBQUEsTUFpQklDLGVBakJKO0FBQUEsTUFrQkl0UCxLQWxCSjtBQUFBLE1BbUJJdVAsWUFuQko7QUFBQSxNQW9CSUMsVUFBVSxHQUFHL1AsS0FBSyxDQUFDc0IsT0FwQnZCO0FBQUEsTUFxQkkzSixHQUFHLEdBQUdxSSxLQUFLLENBQUNXLE1BQU4sQ0FBYVYsU0FBYixJQUEwQkQsS0FBSyxDQUFDWSxNQUFOLENBQWFYLFNBQWIsQ0FyQnBDO0FBQUEsTUFzQklZLEdBQUcsR0FBR2IsS0FBSyxDQUFDYyxNQUFOLENBQWFiLFNBQWIsQ0F0QlYsQ0FEc0UsQ0F5QnRFOztBQUNBLE1BQUlELEtBQUssQ0FBQ2tCLE1BQU4sQ0FBYWpCLFNBQWIsSUFBMEJELEtBQUssQ0FBQ21CLFNBQWhDLElBQTZDLENBQWpELEVBQW9EO0FBQUUsV0FBTyxLQUFQO0FBQWUsR0ExQkMsQ0E0QnRFOzs7QUFDQSxNQUFJbkIsS0FBSyxDQUFDZSxHQUFOLENBQVVuQixVQUFWLENBQXFCakksR0FBRyxFQUF4QixNQUFnQztBQUFJO0FBQXhDLElBQWlEO0FBQUUsYUFBTyxLQUFQO0FBQWUsS0E3QkksQ0ErQnRFO0FBQ0E7OztBQUNBLE1BQUl3SSxNQUFKLEVBQVk7QUFBRSxXQUFPLElBQVA7QUFBYyxHQWpDMEMsQ0FtQ3RFOzs7QUFDQStPLFNBQU8sR0FBR0UsTUFBTSxHQUFHcFAsS0FBSyxDQUFDa0IsTUFBTixDQUFhakIsU0FBYixJQUEwQnRJLEdBQTFCLElBQWlDcUksS0FBSyxDQUFDVyxNQUFOLENBQWFWLFNBQWIsSUFBMEJELEtBQUssQ0FBQ1ksTUFBTixDQUFhWCxTQUFiLENBQTNELENBQW5CLENBcENzRSxDQXNDdEU7O0FBQ0EsTUFBSUQsS0FBSyxDQUFDZSxHQUFOLENBQVVuQixVQUFWLENBQXFCakksR0FBckIsTUFBOEI7QUFBSztBQUF2QyxJQUFvRDtBQUNsRDtBQUNBO0FBQ0FBLFNBQUc7QUFDSHVYLGFBQU87QUFDUEUsWUFBTTtBQUNOSCxlQUFTLEdBQUcsS0FBWjtBQUNBVSxzQkFBZ0IsR0FBRyxJQUFuQjtBQUNELEtBUkQsTUFRTyxJQUFJM1AsS0FBSyxDQUFDZSxHQUFOLENBQVVuQixVQUFWLENBQXFCakksR0FBckIsTUFBOEI7QUFBSztBQUF2QyxJQUFrRDtBQUN2RGdZLHNCQUFnQixHQUFHLElBQW5COztBQUVBLFVBQUksQ0FBQzNQLEtBQUssQ0FBQ2dRLE9BQU4sQ0FBYy9QLFNBQWQsSUFBMkJtUCxNQUE1QixJQUFzQyxDQUF0QyxLQUE0QyxDQUFoRCxFQUFtRDtBQUNqRDtBQUNBO0FBQ0F6WCxXQUFHO0FBQ0h1WCxlQUFPO0FBQ1BFLGNBQU07QUFDTkgsaUJBQVMsR0FBRyxLQUFaO0FBQ0QsT0FQRCxNQU9PO0FBQ0w7QUFDQTtBQUNBO0FBQ0FBLGlCQUFTLEdBQUcsSUFBWjtBQUNEO0FBQ0YsS0FoQk0sTUFnQkE7QUFDTFUsb0JBQWdCLEdBQUcsS0FBbkI7QUFDRDs7QUFFRE4sV0FBUyxHQUFHLENBQUVyUCxLQUFLLENBQUNXLE1BQU4sQ0FBYVYsU0FBYixDQUFGLENBQVo7QUFDQUQsT0FBSyxDQUFDVyxNQUFOLENBQWFWLFNBQWIsSUFBMEJ0SSxHQUExQjs7QUFFQSxTQUFPQSxHQUFHLEdBQUdrSixHQUFiLEVBQWtCO0FBQ2hCMEYsTUFBRSxHQUFHdkcsS0FBSyxDQUFDZSxHQUFOLENBQVVuQixVQUFWLENBQXFCakksR0FBckIsQ0FBTDs7QUFFQSxRQUFJK08sT0FBTyxDQUFDSCxFQUFELENBQVgsRUFBaUI7QUFDZixVQUFJQSxFQUFFLEtBQUssSUFBWCxFQUFpQjtBQUNmNkksY0FBTSxJQUFJLElBQUksQ0FBQ0EsTUFBTSxHQUFHcFAsS0FBSyxDQUFDZ1EsT0FBTixDQUFjL1AsU0FBZCxDQUFULElBQXFDZ1AsU0FBUyxHQUFHLENBQUgsR0FBTyxDQUFyRCxDQUFELElBQTRELENBQTFFO0FBQ0QsT0FGRCxNQUVPO0FBQ0xHLGNBQU07QUFDUDtBQUNGLEtBTkQsTUFNTztBQUNMO0FBQ0Q7O0FBRUR6WCxPQUFHO0FBQ0o7O0FBRUQyWCxZQUFVLEdBQUcsQ0FBRXRQLEtBQUssQ0FBQ2dRLE9BQU4sQ0FBYy9QLFNBQWQsQ0FBRixDQUFiO0FBQ0FELE9BQUssQ0FBQ2dRLE9BQU4sQ0FBYy9QLFNBQWQsSUFBMkJELEtBQUssQ0FBQ2tCLE1BQU4sQ0FBYWpCLFNBQWIsSUFBMEIsQ0FBMUIsSUFBK0IwUCxnQkFBZ0IsR0FBRyxDQUFILEdBQU8sQ0FBdEQsQ0FBM0I7QUFFQVIsZUFBYSxHQUFHeFgsR0FBRyxJQUFJa0osR0FBdkI7QUFFQTRPLFdBQVMsR0FBRyxDQUFFelAsS0FBSyxDQUFDa0IsTUFBTixDQUFhakIsU0FBYixDQUFGLENBQVo7QUFDQUQsT0FBSyxDQUFDa0IsTUFBTixDQUFhakIsU0FBYixJQUEwQm1QLE1BQU0sR0FBR0YsT0FBbkM7QUFFQVEsV0FBUyxHQUFHLENBQUUxUCxLQUFLLENBQUNZLE1BQU4sQ0FBYVgsU0FBYixDQUFGLENBQVo7QUFDQUQsT0FBSyxDQUFDWSxNQUFOLENBQWFYLFNBQWIsSUFBMEJ0SSxHQUFHLEdBQUdxSSxLQUFLLENBQUNXLE1BQU4sQ0FBYVYsU0FBYixDQUFoQztBQUVBNFAsaUJBQWUsR0FBRzdQLEtBQUssQ0FBQ25CLEVBQU4sQ0FBUzBDLEtBQVQsQ0FBZUksS0FBZixDQUFxQnlKLFFBQXJCLENBQThCLFlBQTlCLENBQWxCO0FBRUFvRSxlQUFhLEdBQUd4UCxLQUFLLENBQUNxQixVQUF0QjtBQUNBckIsT0FBSyxDQUFDcUIsVUFBTixHQUFtQixZQUFuQjtBQUNBeU8sY0FBWSxHQUFHLEtBQWYsQ0FyR3NFLENBdUd0RTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBQ0EsT0FBSzFQLFFBQVEsR0FBR0gsU0FBUyxHQUFHLENBQTVCLEVBQStCRyxRQUFRLEdBQUdGLE9BQTFDLEVBQW1ERSxRQUFRLEVBQTNELEVBQStEO0FBQzdEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxRQUFJSixLQUFLLENBQUNrQixNQUFOLENBQWFkLFFBQWIsSUFBeUJKLEtBQUssQ0FBQ21CLFNBQW5DLEVBQThDMk8sWUFBWSxHQUFHLElBQWY7QUFFOUNuWSxPQUFHLEdBQUdxSSxLQUFLLENBQUNXLE1BQU4sQ0FBYVAsUUFBYixJQUF5QkosS0FBSyxDQUFDWSxNQUFOLENBQWFSLFFBQWIsQ0FBL0I7QUFDQVMsT0FBRyxHQUFHYixLQUFLLENBQUNjLE1BQU4sQ0FBYVYsUUFBYixDQUFOOztBQUVBLFFBQUl6SSxHQUFHLElBQUlrSixHQUFYLEVBQWdCO0FBQ2Q7QUFDQTtBQUNEOztBQUVELFFBQUliLEtBQUssQ0FBQ2UsR0FBTixDQUFVbkIsVUFBVixDQUFxQmpJLEdBQUcsRUFBeEIsTUFBZ0M7QUFBSTtBQUFwQyxPQUErQyxDQUFDbVksWUFBcEQsRUFBa0U7QUFDaEU7QUFFQTtBQUNBWixhQUFPLEdBQUdFLE1BQU0sR0FBR3BQLEtBQUssQ0FBQ2tCLE1BQU4sQ0FBYWQsUUFBYixJQUF5QnpJLEdBQXpCLElBQWdDcUksS0FBSyxDQUFDVyxNQUFOLENBQWFQLFFBQWIsSUFBeUJKLEtBQUssQ0FBQ1ksTUFBTixDQUFhUixRQUFiLENBQXpELENBQW5CLENBSmdFLENBTWhFOztBQUNBLFVBQUlKLEtBQUssQ0FBQ2UsR0FBTixDQUFVbkIsVUFBVixDQUFxQmpJLEdBQXJCLE1BQThCO0FBQUs7QUFBdkMsUUFBb0Q7QUFDbEQ7QUFDQTtBQUNBQSxhQUFHO0FBQ0h1WCxpQkFBTztBQUNQRSxnQkFBTTtBQUNOSCxtQkFBUyxHQUFHLEtBQVo7QUFDQVUsMEJBQWdCLEdBQUcsSUFBbkI7QUFDRCxTQVJELE1BUU8sSUFBSTNQLEtBQUssQ0FBQ2UsR0FBTixDQUFVbkIsVUFBVixDQUFxQmpJLEdBQXJCLE1BQThCO0FBQUs7QUFBdkMsUUFBa0Q7QUFDdkRnWSwwQkFBZ0IsR0FBRyxJQUFuQjs7QUFFQSxjQUFJLENBQUMzUCxLQUFLLENBQUNnUSxPQUFOLENBQWM1UCxRQUFkLElBQTBCZ1AsTUFBM0IsSUFBcUMsQ0FBckMsS0FBMkMsQ0FBL0MsRUFBa0Q7QUFDaEQ7QUFDQTtBQUNBelgsZUFBRztBQUNIdVgsbUJBQU87QUFDUEUsa0JBQU07QUFDTkgscUJBQVMsR0FBRyxLQUFaO0FBQ0QsV0FQRCxNQU9PO0FBQ0w7QUFDQTtBQUNBO0FBQ0FBLHFCQUFTLEdBQUcsSUFBWjtBQUNEO0FBQ0YsU0FoQk0sTUFnQkE7QUFDTFUsd0JBQWdCLEdBQUcsS0FBbkI7QUFDRDs7QUFFRE4sZUFBUyxDQUFDL2EsSUFBVixDQUFlMEwsS0FBSyxDQUFDVyxNQUFOLENBQWFQLFFBQWIsQ0FBZjtBQUNBSixXQUFLLENBQUNXLE1BQU4sQ0FBYVAsUUFBYixJQUF5QnpJLEdBQXpCOztBQUVBLGFBQU9BLEdBQUcsR0FBR2tKLEdBQWIsRUFBa0I7QUFDaEIwRixVQUFFLEdBQUd2RyxLQUFLLENBQUNlLEdBQU4sQ0FBVW5CLFVBQVYsQ0FBcUJqSSxHQUFyQixDQUFMOztBQUVBLFlBQUkrTyxPQUFPLENBQUNILEVBQUQsQ0FBWCxFQUFpQjtBQUNmLGNBQUlBLEVBQUUsS0FBSyxJQUFYLEVBQWlCO0FBQ2Y2SSxrQkFBTSxJQUFJLElBQUksQ0FBQ0EsTUFBTSxHQUFHcFAsS0FBSyxDQUFDZ1EsT0FBTixDQUFjNVAsUUFBZCxDQUFULElBQW9DNk8sU0FBUyxHQUFHLENBQUgsR0FBTyxDQUFwRCxDQUFELElBQTJELENBQXpFO0FBQ0QsV0FGRCxNQUVPO0FBQ0xHLGtCQUFNO0FBQ1A7QUFDRixTQU5ELE1BTU87QUFDTDtBQUNEOztBQUVEelgsV0FBRztBQUNKOztBQUVEd1gsbUJBQWEsR0FBR3hYLEdBQUcsSUFBSWtKLEdBQXZCO0FBRUF5TyxnQkFBVSxDQUFDaGIsSUFBWCxDQUFnQjBMLEtBQUssQ0FBQ2dRLE9BQU4sQ0FBYzVQLFFBQWQsQ0FBaEI7QUFDQUosV0FBSyxDQUFDZ1EsT0FBTixDQUFjNVAsUUFBZCxJQUEwQkosS0FBSyxDQUFDa0IsTUFBTixDQUFhZCxRQUFiLElBQXlCLENBQXpCLElBQThCdVAsZ0JBQWdCLEdBQUcsQ0FBSCxHQUFPLENBQXJELENBQTFCO0FBRUFGLGVBQVMsQ0FBQ25iLElBQVYsQ0FBZTBMLEtBQUssQ0FBQ2tCLE1BQU4sQ0FBYWQsUUFBYixDQUFmO0FBQ0FKLFdBQUssQ0FBQ2tCLE1BQU4sQ0FBYWQsUUFBYixJQUF5QmdQLE1BQU0sR0FBR0YsT0FBbEM7QUFFQVEsZUFBUyxDQUFDcGIsSUFBVixDQUFlMEwsS0FBSyxDQUFDWSxNQUFOLENBQWFSLFFBQWIsQ0FBZjtBQUNBSixXQUFLLENBQUNZLE1BQU4sQ0FBYVIsUUFBYixJQUF5QnpJLEdBQUcsR0FBR3FJLEtBQUssQ0FBQ1csTUFBTixDQUFhUCxRQUFiLENBQS9CO0FBQ0E7QUFDRCxLQXBGNEQsQ0FzRjdEOzs7QUFDQSxRQUFJK08sYUFBSixFQUFtQjtBQUFFO0FBQVEsS0F2RmdDLENBeUY3RDs7O0FBQ0FTLGFBQVMsR0FBRyxLQUFaOztBQUNBLFNBQUsxYyxDQUFDLEdBQUcsQ0FBSixFQUFPMFksQ0FBQyxHQUFHaUUsZUFBZSxDQUFDNWMsTUFBaEMsRUFBd0NDLENBQUMsR0FBRzBZLENBQTVDLEVBQStDMVksQ0FBQyxFQUFoRCxFQUFvRDtBQUNsRCxVQUFJMmMsZUFBZSxDQUFDM2MsQ0FBRCxDQUFmLENBQW1COE0sS0FBbkIsRUFBMEJJLFFBQTFCLEVBQW9DRixPQUFwQyxFQUE2QyxJQUE3QyxDQUFKLEVBQXdEO0FBQ3REMFAsaUJBQVMsR0FBRyxJQUFaO0FBQ0E7QUFDRDtBQUNGOztBQUVELFFBQUlBLFNBQUosRUFBZTtBQUNiO0FBQ0E7QUFDQTtBQUNBO0FBQ0E1UCxXQUFLLENBQUNzQixPQUFOLEdBQWdCbEIsUUFBaEI7O0FBRUEsVUFBSUosS0FBSyxDQUFDbUIsU0FBTixLQUFvQixDQUF4QixFQUEyQjtBQUN6QjtBQUNBO0FBQ0E7QUFDQWtPLGlCQUFTLENBQUMvYSxJQUFWLENBQWUwTCxLQUFLLENBQUNXLE1BQU4sQ0FBYVAsUUFBYixDQUFmO0FBQ0FrUCxrQkFBVSxDQUFDaGIsSUFBWCxDQUFnQjBMLEtBQUssQ0FBQ2dRLE9BQU4sQ0FBYzVQLFFBQWQsQ0FBaEI7QUFDQXNQLGlCQUFTLENBQUNwYixJQUFWLENBQWUwTCxLQUFLLENBQUNZLE1BQU4sQ0FBYVIsUUFBYixDQUFmO0FBQ0FxUCxpQkFBUyxDQUFDbmIsSUFBVixDQUFlMEwsS0FBSyxDQUFDa0IsTUFBTixDQUFhZCxRQUFiLENBQWY7QUFDQUosYUFBSyxDQUFDa0IsTUFBTixDQUFhZCxRQUFiLEtBQTBCSixLQUFLLENBQUNtQixTQUFoQztBQUNEOztBQUVEO0FBQ0Q7O0FBRURrTyxhQUFTLENBQUMvYSxJQUFWLENBQWUwTCxLQUFLLENBQUNXLE1BQU4sQ0FBYVAsUUFBYixDQUFmO0FBQ0FrUCxjQUFVLENBQUNoYixJQUFYLENBQWdCMEwsS0FBSyxDQUFDZ1EsT0FBTixDQUFjNVAsUUFBZCxDQUFoQjtBQUNBc1AsYUFBUyxDQUFDcGIsSUFBVixDQUFlMEwsS0FBSyxDQUFDWSxNQUFOLENBQWFSLFFBQWIsQ0FBZjtBQUNBcVAsYUFBUyxDQUFDbmIsSUFBVixDQUFlMEwsS0FBSyxDQUFDa0IsTUFBTixDQUFhZCxRQUFiLENBQWYsRUExSDZELENBNEg3RDtBQUNBOztBQUNBSixTQUFLLENBQUNrQixNQUFOLENBQWFkLFFBQWIsSUFBeUIsQ0FBQyxDQUExQjtBQUNEOztBQUVEbVAsV0FBUyxHQUFHdlAsS0FBSyxDQUFDbUIsU0FBbEI7QUFDQW5CLE9BQUssQ0FBQ21CLFNBQU4sR0FBa0IsQ0FBbEI7QUFFQVosT0FBSyxHQUFVUCxLQUFLLENBQUMxTCxJQUFOLENBQVcsaUJBQVgsRUFBOEIsWUFBOUIsRUFBNEMsQ0FBNUMsQ0FBZjtBQUNBaU0sT0FBSyxDQUFDRCxNQUFOLEdBQWUsR0FBZjtBQUNBQyxPQUFLLENBQUNuRixHQUFOLEdBQWVvTSxLQUFLLEdBQUcsQ0FBRXZILFNBQUYsRUFBYSxDQUFiLENBQXZCO0FBRUFELE9BQUssQ0FBQ25CLEVBQU4sQ0FBUzBDLEtBQVQsQ0FBZUUsUUFBZixDQUF3QnpCLEtBQXhCLEVBQStCQyxTQUEvQixFQUEwQ0csUUFBMUM7QUFFQUcsT0FBSyxHQUFVUCxLQUFLLENBQUMxTCxJQUFOLENBQVcsa0JBQVgsRUFBK0IsWUFBL0IsRUFBNkMsQ0FBQyxDQUE5QyxDQUFmO0FBQ0FpTSxPQUFLLENBQUNELE1BQU4sR0FBZSxHQUFmO0FBRUFOLE9BQUssQ0FBQ3NCLE9BQU4sR0FBZ0J5TyxVQUFoQjtBQUNBL1AsT0FBSyxDQUFDcUIsVUFBTixHQUFtQm1PLGFBQW5CO0FBQ0FoSSxPQUFLLENBQUMsQ0FBRCxDQUFMLEdBQVd4SCxLQUFLLENBQUMwQixJQUFqQixDQXhRc0UsQ0EwUXRFO0FBQ0E7O0FBQ0EsT0FBS3hPLENBQUMsR0FBRyxDQUFULEVBQVlBLENBQUMsR0FBR3djLFNBQVMsQ0FBQ3pjLE1BQTFCLEVBQWtDQyxDQUFDLEVBQW5DLEVBQXVDO0FBQ3JDOE0sU0FBSyxDQUFDVyxNQUFOLENBQWF6TixDQUFDLEdBQUcrTSxTQUFqQixJQUE4Qm9QLFNBQVMsQ0FBQ25jLENBQUQsQ0FBdkM7QUFDQThNLFNBQUssQ0FBQ1ksTUFBTixDQUFhMU4sQ0FBQyxHQUFHK00sU0FBakIsSUFBOEJ5UCxTQUFTLENBQUN4YyxDQUFELENBQXZDO0FBQ0E4TSxTQUFLLENBQUNrQixNQUFOLENBQWFoTyxDQUFDLEdBQUcrTSxTQUFqQixJQUE4QndQLFNBQVMsQ0FBQ3ZjLENBQUQsQ0FBdkM7QUFDQThNLFNBQUssQ0FBQ2dRLE9BQU4sQ0FBYzljLENBQUMsR0FBRytNLFNBQWxCLElBQStCcVAsVUFBVSxDQUFDcGMsQ0FBRCxDQUF6QztBQUNEOztBQUNEOE0sT0FBSyxDQUFDbUIsU0FBTixHQUFrQm9PLFNBQWxCO0FBRUEsU0FBTyxJQUFQO0FBQ0QsQ0FyUkQsQzs7Ozs7Ozs7Ozs7O0FDUEE7QUFFYTs7QUFHYmxjLE1BQU0sQ0FBQ0MsT0FBUCxHQUFpQixTQUFTdVMsSUFBVCxDQUFjN0YsS0FBZCxFQUFxQkMsU0FBckIsRUFBZ0NDO0FBQU87QUFBdkMsRUFBcUQ7QUFDcEUsTUFBSUUsUUFBSixFQUFjNlAsSUFBZCxFQUFvQjFQLEtBQXBCOztBQUVBLE1BQUlQLEtBQUssQ0FBQ2tCLE1BQU4sQ0FBYWpCLFNBQWIsSUFBMEJELEtBQUssQ0FBQ21CLFNBQWhDLEdBQTRDLENBQWhELEVBQW1EO0FBQUUsV0FBTyxLQUFQO0FBQWU7O0FBRXBFOE8sTUFBSSxHQUFHN1AsUUFBUSxHQUFHSCxTQUFTLEdBQUcsQ0FBOUI7O0FBRUEsU0FBT0csUUFBUSxHQUFHRixPQUFsQixFQUEyQjtBQUN6QixRQUFJRixLQUFLLENBQUN5TCxPQUFOLENBQWNyTCxRQUFkLENBQUosRUFBNkI7QUFDM0JBLGNBQVE7QUFDUjtBQUNEOztBQUVELFFBQUlKLEtBQUssQ0FBQ2tCLE1BQU4sQ0FBYWQsUUFBYixJQUF5QkosS0FBSyxDQUFDbUIsU0FBL0IsSUFBNEMsQ0FBaEQsRUFBbUQ7QUFDakRmLGNBQVE7QUFDUjZQLFVBQUksR0FBRzdQLFFBQVA7QUFDQTtBQUNEOztBQUNEO0FBQ0Q7O0FBRURKLE9BQUssQ0FBQzBCLElBQU4sR0FBYXVPLElBQWI7QUFFQTFQLE9BQUssR0FBV1AsS0FBSyxDQUFDMUwsSUFBTixDQUFXLFlBQVgsRUFBeUIsTUFBekIsRUFBaUMsQ0FBakMsQ0FBaEI7QUFDQWlNLE9BQUssQ0FBQ3NNLE9BQU4sR0FBZ0I3TSxLQUFLLENBQUNrUSxRQUFOLENBQWVqUSxTQUFmLEVBQTBCZ1EsSUFBMUIsRUFBZ0MsSUFBSWpRLEtBQUssQ0FBQ21CLFNBQTFDLEVBQXFELElBQXJELENBQWhCO0FBQ0FaLE9BQUssQ0FBQ25GLEdBQU4sR0FBZ0IsQ0FBRTZFLFNBQUYsRUFBYUQsS0FBSyxDQUFDMEIsSUFBbkIsQ0FBaEI7QUFFQSxTQUFPLElBQVA7QUFDRCxDQTVCRCxDOzs7Ozs7Ozs7Ozs7QUNMQTtBQUVhOztBQUdick8sTUFBTSxDQUFDQyxPQUFQLEdBQWlCLFNBQVN5WixLQUFULENBQWUvTSxLQUFmLEVBQXNCQyxTQUF0QixFQUFpQ0MsT0FBakMsRUFBMENDLE1BQTFDLEVBQWtEO0FBQ2pFLE1BQUlULE1BQUo7QUFBQSxNQUFZMU0sR0FBWjtBQUFBLE1BQWlCK0wsTUFBakI7QUFBQSxNQUF5QnFCLFFBQXpCO0FBQUEsTUFBbUMrUCxHQUFuQztBQUFBLE1BQXdDNVAsS0FBeEM7QUFBQSxNQUErQ0QsTUFBL0M7QUFBQSxNQUNJOFAsYUFBYSxHQUFHLEtBRHBCO0FBQUEsTUFFSXpZLEdBQUcsR0FBR3FJLEtBQUssQ0FBQ1csTUFBTixDQUFhVixTQUFiLElBQTBCRCxLQUFLLENBQUNZLE1BQU4sQ0FBYVgsU0FBYixDQUZwQztBQUFBLE1BR0lZLEdBQUcsR0FBR2IsS0FBSyxDQUFDYyxNQUFOLENBQWFiLFNBQWIsQ0FIVixDQURpRSxDQU1qRTs7QUFDQSxNQUFJRCxLQUFLLENBQUNrQixNQUFOLENBQWFqQixTQUFiLElBQTBCRCxLQUFLLENBQUNtQixTQUFoQyxJQUE2QyxDQUFqRCxFQUFvRDtBQUFFLFdBQU8sS0FBUDtBQUFlOztBQUVyRSxNQUFJeEosR0FBRyxHQUFHLENBQU4sR0FBVWtKLEdBQWQsRUFBbUI7QUFBRSxXQUFPLEtBQVA7QUFBZTs7QUFFcENuQixRQUFNLEdBQUdNLEtBQUssQ0FBQ2UsR0FBTixDQUFVbkIsVUFBVixDQUFxQmpJLEdBQXJCLENBQVQ7O0FBRUEsTUFBSStILE1BQU0sS0FBSztBQUFJO0FBQWYsS0FBMEJBLE1BQU0sS0FBSztBQUFLO0FBQTlDLElBQXVEO0FBQ3JELGFBQU8sS0FBUDtBQUNELEtBZmdFLENBaUJqRTs7O0FBQ0F5USxLQUFHLEdBQUd4WSxHQUFOO0FBQ0FBLEtBQUcsR0FBR3FJLEtBQUssQ0FBQ3FRLFNBQU4sQ0FBZ0IxWSxHQUFoQixFQUFxQitILE1BQXJCLENBQU47QUFFQTFNLEtBQUcsR0FBRzJFLEdBQUcsR0FBR3dZLEdBQVo7O0FBRUEsTUFBSW5kLEdBQUcsR0FBRyxDQUFWLEVBQWE7QUFBRSxXQUFPLEtBQVA7QUFBZTs7QUFFOUJzTixRQUFNLEdBQUdOLEtBQUssQ0FBQ2UsR0FBTixDQUFVbkwsS0FBVixDQUFnQnVhLEdBQWhCLEVBQXFCeFksR0FBckIsQ0FBVDtBQUNBb0gsUUFBTSxHQUFHaUIsS0FBSyxDQUFDZSxHQUFOLENBQVVuTCxLQUFWLENBQWdCK0IsR0FBaEIsRUFBcUJrSixHQUFyQixDQUFUOztBQUVBLE1BQUluQixNQUFNLEtBQUs7QUFBSztBQUFwQixJQUE2QjtBQUMzQixVQUFJWCxNQUFNLENBQUMzQixPQUFQLENBQWVpSSxNQUFNLENBQUNDLFlBQVAsQ0FBb0I1RixNQUFwQixDQUFmLEtBQStDLENBQW5ELEVBQXNEO0FBQ3BELGVBQU8sS0FBUDtBQUNEO0FBQ0YsS0FoQ2dFLENBa0NqRTs7O0FBQ0EsTUFBSVMsTUFBSixFQUFZO0FBQUUsV0FBTyxJQUFQO0FBQWMsR0FuQ3FDLENBcUNqRTs7O0FBQ0FDLFVBQVEsR0FBR0gsU0FBWDs7QUFFQSxXQUFTO0FBQ1BHLFlBQVE7O0FBQ1IsUUFBSUEsUUFBUSxJQUFJRixPQUFoQixFQUF5QjtBQUN2QjtBQUNBO0FBQ0E7QUFDRDs7QUFFRHZJLE9BQUcsR0FBR3dZLEdBQUcsR0FBR25RLEtBQUssQ0FBQ1csTUFBTixDQUFhUCxRQUFiLElBQXlCSixLQUFLLENBQUNZLE1BQU4sQ0FBYVIsUUFBYixDQUFyQztBQUNBUyxPQUFHLEdBQUdiLEtBQUssQ0FBQ2MsTUFBTixDQUFhVixRQUFiLENBQU47O0FBRUEsUUFBSXpJLEdBQUcsR0FBR2tKLEdBQU4sSUFBYWIsS0FBSyxDQUFDa0IsTUFBTixDQUFhZCxRQUFiLElBQXlCSixLQUFLLENBQUNtQixTQUFoRCxFQUEyRDtBQUN6RDtBQUNBO0FBQ0E7QUFDQTtBQUNEOztBQUVELFFBQUluQixLQUFLLENBQUNlLEdBQU4sQ0FBVW5CLFVBQVYsQ0FBcUJqSSxHQUFyQixNQUE4QitILE1BQWxDLEVBQTBDO0FBQUU7QUFBVzs7QUFFdkQsUUFBSU0sS0FBSyxDQUFDa0IsTUFBTixDQUFhZCxRQUFiLElBQXlCSixLQUFLLENBQUNtQixTQUEvQixJQUE0QyxDQUFoRCxFQUFtRDtBQUNqRDtBQUNBO0FBQ0Q7O0FBRUR4SixPQUFHLEdBQUdxSSxLQUFLLENBQUNxUSxTQUFOLENBQWdCMVksR0FBaEIsRUFBcUIrSCxNQUFyQixDQUFOLENBekJPLENBMkJQOztBQUNBLFFBQUkvSCxHQUFHLEdBQUd3WSxHQUFOLEdBQVluZCxHQUFoQixFQUFxQjtBQUFFO0FBQVcsS0E1QjNCLENBOEJQOzs7QUFDQTJFLE9BQUcsR0FBR3FJLEtBQUssQ0FBQ29CLFVBQU4sQ0FBaUJ6SixHQUFqQixDQUFOOztBQUVBLFFBQUlBLEdBQUcsR0FBR2tKLEdBQVYsRUFBZTtBQUFFO0FBQVc7O0FBRTVCdVAsaUJBQWEsR0FBRyxJQUFoQixDQW5DTyxDQW9DUDs7QUFDQTtBQUNELEdBOUVnRSxDQWdGakU7OztBQUNBcGQsS0FBRyxHQUFHZ04sS0FBSyxDQUFDa0IsTUFBTixDQUFhakIsU0FBYixDQUFOO0FBRUFELE9BQUssQ0FBQzBCLElBQU4sR0FBYXRCLFFBQVEsSUFBSWdRLGFBQWEsR0FBRyxDQUFILEdBQU8sQ0FBeEIsQ0FBckI7QUFFQTdQLE9BQUssR0FBV1AsS0FBSyxDQUFDMUwsSUFBTixDQUFXLE9BQVgsRUFBb0IsTUFBcEIsRUFBNEIsQ0FBNUIsQ0FBaEI7QUFDQWlNLE9BQUssQ0FBQ2lCLElBQU4sR0FBZ0J6QyxNQUFoQjtBQUNBd0IsT0FBSyxDQUFDc00sT0FBTixHQUFnQjdNLEtBQUssQ0FBQ2tRLFFBQU4sQ0FBZWpRLFNBQVMsR0FBRyxDQUEzQixFQUE4QkcsUUFBOUIsRUFBd0NwTixHQUF4QyxFQUE2QyxJQUE3QyxDQUFoQjtBQUNBdU4sT0FBSyxDQUFDRCxNQUFOLEdBQWdCQSxNQUFoQjtBQUNBQyxPQUFLLENBQUNuRixHQUFOLEdBQWdCLENBQUU2RSxTQUFGLEVBQWFELEtBQUssQ0FBQzBCLElBQW5CLENBQWhCO0FBRUEsU0FBTyxJQUFQO0FBQ0QsQ0E1RkQsQzs7Ozs7Ozs7Ozs7O0FDTEE7QUFFYTs7QUFFYixJQUFJZ0YsT0FBTyxHQUFHOVIsbUJBQU8sQ0FBQywyRUFBRCxDQUFQLENBQTJCOFIsT0FBekM7O0FBR0FyVCxNQUFNLENBQUNDLE9BQVAsR0FBaUIsU0FBU2dkLE9BQVQsQ0FBaUJ0USxLQUFqQixFQUF3QkMsU0FBeEIsRUFBbUNDLE9BQW5DLEVBQTRDQyxNQUE1QyxFQUFvRDtBQUNuRSxNQUFJb0csRUFBSjtBQUFBLE1BQVFnQixLQUFSO0FBQUEsTUFBZWdKLEdBQWY7QUFBQSxNQUFvQmhRLEtBQXBCO0FBQUEsTUFDSTVJLEdBQUcsR0FBR3FJLEtBQUssQ0FBQ1csTUFBTixDQUFhVixTQUFiLElBQTBCRCxLQUFLLENBQUNZLE1BQU4sQ0FBYVgsU0FBYixDQURwQztBQUFBLE1BRUlZLEdBQUcsR0FBR2IsS0FBSyxDQUFDYyxNQUFOLENBQWFiLFNBQWIsQ0FGVixDQURtRSxDQUtuRTs7QUFDQSxNQUFJRCxLQUFLLENBQUNrQixNQUFOLENBQWFqQixTQUFiLElBQTBCRCxLQUFLLENBQUNtQixTQUFoQyxJQUE2QyxDQUFqRCxFQUFvRDtBQUFFLFdBQU8sS0FBUDtBQUFlOztBQUVyRW9GLElBQUUsR0FBSXZHLEtBQUssQ0FBQ2UsR0FBTixDQUFVbkIsVUFBVixDQUFxQmpJLEdBQXJCLENBQU47O0FBRUEsTUFBSTRPLEVBQUUsS0FBSztBQUFJO0FBQVgsS0FBc0I1TyxHQUFHLElBQUlrSixHQUFqQyxFQUFzQztBQUFFLFdBQU8sS0FBUDtBQUFlLEdBVlksQ0FZbkU7OztBQUNBMEcsT0FBSyxHQUFHLENBQVI7QUFDQWhCLElBQUUsR0FBR3ZHLEtBQUssQ0FBQ2UsR0FBTixDQUFVbkIsVUFBVixDQUFxQixFQUFFakksR0FBdkIsQ0FBTDs7QUFDQSxTQUFPNE8sRUFBRSxLQUFLO0FBQUk7QUFBWCxLQUFzQjVPLEdBQUcsR0FBR2tKLEdBQTVCLElBQW1DMEcsS0FBSyxJQUFJLENBQW5ELEVBQXNEO0FBQ3BEQSxTQUFLO0FBQ0xoQixNQUFFLEdBQUd2RyxLQUFLLENBQUNlLEdBQU4sQ0FBVW5CLFVBQVYsQ0FBcUIsRUFBRWpJLEdBQXZCLENBQUw7QUFDRDs7QUFFRCxNQUFJNFAsS0FBSyxHQUFHLENBQVIsSUFBYzVQLEdBQUcsR0FBR2tKLEdBQU4sSUFBYSxDQUFDNkYsT0FBTyxDQUFDSCxFQUFELENBQXZDLEVBQThDO0FBQUUsV0FBTyxLQUFQO0FBQWU7O0FBRS9ELE1BQUlwRyxNQUFKLEVBQVk7QUFBRSxXQUFPLElBQVA7QUFBYyxHQXRCdUMsQ0F3Qm5FOzs7QUFFQVUsS0FBRyxHQUFHYixLQUFLLENBQUN3USxjQUFOLENBQXFCM1AsR0FBckIsRUFBMEJsSixHQUExQixDQUFOO0FBQ0E0WSxLQUFHLEdBQUd2USxLQUFLLENBQUN5USxhQUFOLENBQW9CNVAsR0FBcEIsRUFBeUIsSUFBekIsRUFBK0JsSixHQUEvQixDQUFOLENBM0JtRSxDQTJCeEI7O0FBQzNDLE1BQUk0WSxHQUFHLEdBQUc1WSxHQUFOLElBQWErTyxPQUFPLENBQUMxRyxLQUFLLENBQUNlLEdBQU4sQ0FBVW5CLFVBQVYsQ0FBcUIyUSxHQUFHLEdBQUcsQ0FBM0IsQ0FBRCxDQUF4QixFQUF5RDtBQUN2RDFQLE9BQUcsR0FBRzBQLEdBQU47QUFDRDs7QUFFRHZRLE9BQUssQ0FBQzBCLElBQU4sR0FBYXpCLFNBQVMsR0FBRyxDQUF6QjtBQUVBTSxPQUFLLEdBQVVQLEtBQUssQ0FBQzFMLElBQU4sQ0FBVyxjQUFYLEVBQTJCLE1BQU0rUSxNQUFNLENBQUNrQyxLQUFELENBQXZDLEVBQWdELENBQWhELENBQWY7QUFDQWhILE9BQUssQ0FBQ0QsTUFBTixHQUFlLFdBQVcxSyxLQUFYLENBQWlCLENBQWpCLEVBQW9CMlIsS0FBcEIsQ0FBZjtBQUNBaEgsT0FBSyxDQUFDbkYsR0FBTixHQUFlLENBQUU2RSxTQUFGLEVBQWFELEtBQUssQ0FBQzBCLElBQW5CLENBQWY7QUFFQW5CLE9BQUssR0FBWVAsS0FBSyxDQUFDMUwsSUFBTixDQUFXLFFBQVgsRUFBcUIsRUFBckIsRUFBeUIsQ0FBekIsQ0FBakI7QUFDQWlNLE9BQUssQ0FBQ3NNLE9BQU4sR0FBaUI3TSxLQUFLLENBQUNlLEdBQU4sQ0FBVW5MLEtBQVYsQ0FBZ0IrQixHQUFoQixFQUFxQmtKLEdBQXJCLEVBQTBCN0IsSUFBMUIsRUFBakI7QUFDQXVCLE9BQUssQ0FBQ25GLEdBQU4sR0FBaUIsQ0FBRTZFLFNBQUYsRUFBYUQsS0FBSyxDQUFDMEIsSUFBbkIsQ0FBakI7QUFDQW5CLE9BQUssQ0FBQytNLFFBQU4sR0FBaUIsRUFBakI7QUFFQS9NLE9BQUssR0FBVVAsS0FBSyxDQUFDMUwsSUFBTixDQUFXLGVBQVgsRUFBNEIsTUFBTStRLE1BQU0sQ0FBQ2tDLEtBQUQsQ0FBeEMsRUFBaUQsQ0FBQyxDQUFsRCxDQUFmO0FBQ0FoSCxPQUFLLENBQUNELE1BQU4sR0FBZSxXQUFXMUssS0FBWCxDQUFpQixDQUFqQixFQUFvQjJSLEtBQXBCLENBQWY7QUFFQSxTQUFPLElBQVA7QUFDRCxDQS9DRCxDOzs7Ozs7Ozs7Ozs7QUNQQTtBQUVhOztBQUViLElBQUliLE9BQU8sR0FBRzlSLG1CQUFPLENBQUMsMkVBQUQsQ0FBUCxDQUEyQjhSLE9BQXpDOztBQUdBclQsTUFBTSxDQUFDQyxPQUFQLEdBQWlCLFNBQVNvZCxFQUFULENBQVkxUSxLQUFaLEVBQW1CQyxTQUFuQixFQUE4QkMsT0FBOUIsRUFBdUNDLE1BQXZDLEVBQStDO0FBQzlELE1BQUlULE1BQUo7QUFBQSxNQUFZaVIsR0FBWjtBQUFBLE1BQWlCcEssRUFBakI7QUFBQSxNQUFxQmhHLEtBQXJCO0FBQUEsTUFDSTVJLEdBQUcsR0FBR3FJLEtBQUssQ0FBQ1csTUFBTixDQUFhVixTQUFiLElBQTBCRCxLQUFLLENBQUNZLE1BQU4sQ0FBYVgsU0FBYixDQURwQztBQUFBLE1BRUlZLEdBQUcsR0FBR2IsS0FBSyxDQUFDYyxNQUFOLENBQWFiLFNBQWIsQ0FGVixDQUQ4RCxDQUs5RDs7QUFDQSxNQUFJRCxLQUFLLENBQUNrQixNQUFOLENBQWFqQixTQUFiLElBQTBCRCxLQUFLLENBQUNtQixTQUFoQyxJQUE2QyxDQUFqRCxFQUFvRDtBQUFFLFdBQU8sS0FBUDtBQUFlOztBQUVyRXpCLFFBQU0sR0FBR00sS0FBSyxDQUFDZSxHQUFOLENBQVVuQixVQUFWLENBQXFCakksR0FBRyxFQUF4QixDQUFULENBUjhELENBVTlEOztBQUNBLE1BQUkrSCxNQUFNLEtBQUs7QUFBSTtBQUFmLEtBQ0FBLE1BQU0sS0FBSztBQUFJO0FBRGYsS0FFQUEsTUFBTSxLQUFLO0FBQUk7QUFGbkIsSUFFNEI7QUFDMUIsYUFBTyxLQUFQO0FBQ0QsS0FmNkQsQ0FpQjlEOzs7QUFFQWlSLEtBQUcsR0FBRyxDQUFOOztBQUNBLFNBQU9oWixHQUFHLEdBQUdrSixHQUFiLEVBQWtCO0FBQ2hCMEYsTUFBRSxHQUFHdkcsS0FBSyxDQUFDZSxHQUFOLENBQVVuQixVQUFWLENBQXFCakksR0FBRyxFQUF4QixDQUFMOztBQUNBLFFBQUk0TyxFQUFFLEtBQUs3RyxNQUFQLElBQWlCLENBQUNnSCxPQUFPLENBQUNILEVBQUQsQ0FBN0IsRUFBbUM7QUFBRSxhQUFPLEtBQVA7QUFBZTs7QUFDcEQsUUFBSUEsRUFBRSxLQUFLN0csTUFBWCxFQUFtQjtBQUFFaVIsU0FBRztBQUFLO0FBQzlCOztBQUVELE1BQUlBLEdBQUcsR0FBRyxDQUFWLEVBQWE7QUFBRSxXQUFPLEtBQVA7QUFBZTs7QUFFOUIsTUFBSXhRLE1BQUosRUFBWTtBQUFFLFdBQU8sSUFBUDtBQUFjOztBQUU1QkgsT0FBSyxDQUFDMEIsSUFBTixHQUFhekIsU0FBUyxHQUFHLENBQXpCO0FBRUFNLE9BQUssR0FBVVAsS0FBSyxDQUFDMUwsSUFBTixDQUFXLElBQVgsRUFBaUIsSUFBakIsRUFBdUIsQ0FBdkIsQ0FBZjtBQUNBaU0sT0FBSyxDQUFDbkYsR0FBTixHQUFlLENBQUU2RSxTQUFGLEVBQWFELEtBQUssQ0FBQzBCLElBQW5CLENBQWY7QUFDQW5CLE9BQUssQ0FBQ0QsTUFBTixHQUFlbE4sS0FBSyxDQUFDdWQsR0FBRyxHQUFHLENBQVAsQ0FBTCxDQUFlN1csSUFBZixDQUFvQnVMLE1BQU0sQ0FBQ0MsWUFBUCxDQUFvQjVGLE1BQXBCLENBQXBCLENBQWY7QUFFQSxTQUFPLElBQVA7QUFDRCxDQXJDRCxDOzs7Ozs7Ozs7Ozs7QUNQQTtBQUVhOztBQUdiLElBQUlrUixXQUFXLEdBQUdoYyxtQkFBTyxDQUFDLHVGQUFELENBQXpCOztBQUNBLElBQUk4UCxzQkFBc0IsR0FBRzlQLG1CQUFPLENBQUMsK0VBQUQsQ0FBUCxDQUE2QjhQLHNCQUExRCxDLENBRUE7QUFDQTtBQUNBOzs7QUFDQSxJQUFJbU0sY0FBYyxHQUFHLENBQ25CLENBQUUsbUNBQUYsRUFBdUMseUJBQXZDLEVBQWtFLElBQWxFLENBRG1CLEVBRW5CLENBQUUsT0FBRixFQUFrQixLQUFsQixFQUEyQixJQUEzQixDQUZtQixFQUduQixDQUFFLE1BQUYsRUFBa0IsS0FBbEIsRUFBMkIsSUFBM0IsQ0FIbUIsRUFJbkIsQ0FBRSxVQUFGLEVBQWtCLEdBQWxCLEVBQTJCLElBQTNCLENBSm1CLEVBS25CLENBQUUsY0FBRixFQUFrQixPQUFsQixFQUEyQixJQUEzQixDQUxtQixFQU1uQixDQUFFLElBQUk3WSxNQUFKLENBQVcsVUFBVTRZLFdBQVcsQ0FBQzlXLElBQVosQ0FBaUIsR0FBakIsQ0FBVixHQUFrQyxrQkFBN0MsRUFBaUUsR0FBakUsQ0FBRixFQUF5RSxJQUF6RSxFQUErRSxJQUEvRSxDQU5tQixFQU9uQixDQUFFLElBQUk5QixNQUFKLENBQVcwTSxzQkFBc0IsQ0FBQ3RPLE1BQXZCLEdBQWdDLE9BQTNDLENBQUYsRUFBd0QsSUFBeEQsRUFBOEQsS0FBOUQsQ0FQbUIsQ0FBckI7O0FBV0EvQyxNQUFNLENBQUNDLE9BQVAsR0FBaUIsU0FBU21hLFVBQVQsQ0FBb0J6TixLQUFwQixFQUEyQkMsU0FBM0IsRUFBc0NDLE9BQXRDLEVBQStDQyxNQUEvQyxFQUF1RDtBQUN0RSxNQUFJak4sQ0FBSjtBQUFBLE1BQU9rTixRQUFQO0FBQUEsTUFBaUJHLEtBQWpCO0FBQUEsTUFBd0J1USxRQUF4QjtBQUFBLE1BQ0luWixHQUFHLEdBQUdxSSxLQUFLLENBQUNXLE1BQU4sQ0FBYVYsU0FBYixJQUEwQkQsS0FBSyxDQUFDWSxNQUFOLENBQWFYLFNBQWIsQ0FEcEM7QUFBQSxNQUVJWSxHQUFHLEdBQUdiLEtBQUssQ0FBQ2MsTUFBTixDQUFhYixTQUFiLENBRlYsQ0FEc0UsQ0FLdEU7O0FBQ0EsTUFBSUQsS0FBSyxDQUFDa0IsTUFBTixDQUFhakIsU0FBYixJQUEwQkQsS0FBSyxDQUFDbUIsU0FBaEMsSUFBNkMsQ0FBakQsRUFBb0Q7QUFBRSxXQUFPLEtBQVA7QUFBZTs7QUFFckUsTUFBSSxDQUFDbkIsS0FBSyxDQUFDbkIsRUFBTixDQUFTckMsT0FBVCxDQUFpQjBQLElBQXRCLEVBQTRCO0FBQUUsV0FBTyxLQUFQO0FBQWU7O0FBRTdDLE1BQUlsTSxLQUFLLENBQUNlLEdBQU4sQ0FBVW5CLFVBQVYsQ0FBcUJqSSxHQUFyQixNQUE4QjtBQUFJO0FBQXRDLElBQStDO0FBQUUsYUFBTyxLQUFQO0FBQWU7O0FBRWhFbVosVUFBUSxHQUFHOVEsS0FBSyxDQUFDZSxHQUFOLENBQVVuTCxLQUFWLENBQWdCK0IsR0FBaEIsRUFBcUJrSixHQUFyQixDQUFYOztBQUVBLE9BQUszTixDQUFDLEdBQUcsQ0FBVCxFQUFZQSxDQUFDLEdBQUcyZCxjQUFjLENBQUM1ZCxNQUEvQixFQUF1Q0MsQ0FBQyxFQUF4QyxFQUE0QztBQUMxQyxRQUFJMmQsY0FBYyxDQUFDM2QsQ0FBRCxDQUFkLENBQWtCLENBQWxCLEVBQXFCNkMsSUFBckIsQ0FBMEIrYSxRQUExQixDQUFKLEVBQXlDO0FBQUU7QUFBUTtBQUNwRDs7QUFFRCxNQUFJNWQsQ0FBQyxLQUFLMmQsY0FBYyxDQUFDNWQsTUFBekIsRUFBaUM7QUFBRSxXQUFPLEtBQVA7QUFBZTs7QUFFbEQsTUFBSWtOLE1BQUosRUFBWTtBQUNWO0FBQ0EsV0FBTzBRLGNBQWMsQ0FBQzNkLENBQUQsQ0FBZCxDQUFrQixDQUFsQixDQUFQO0FBQ0Q7O0FBRURrTixVQUFRLEdBQUdILFNBQVMsR0FBRyxDQUF2QixDQXpCc0UsQ0EyQnRFO0FBQ0E7O0FBQ0EsTUFBSSxDQUFDNFEsY0FBYyxDQUFDM2QsQ0FBRCxDQUFkLENBQWtCLENBQWxCLEVBQXFCNkMsSUFBckIsQ0FBMEIrYSxRQUExQixDQUFMLEVBQTBDO0FBQ3hDLFdBQU8xUSxRQUFRLEdBQUdGLE9BQWxCLEVBQTJCRSxRQUFRLEVBQW5DLEVBQXVDO0FBQ3JDLFVBQUlKLEtBQUssQ0FBQ2tCLE1BQU4sQ0FBYWQsUUFBYixJQUF5QkosS0FBSyxDQUFDbUIsU0FBbkMsRUFBOEM7QUFBRTtBQUFROztBQUV4RHhKLFNBQUcsR0FBR3FJLEtBQUssQ0FBQ1csTUFBTixDQUFhUCxRQUFiLElBQXlCSixLQUFLLENBQUNZLE1BQU4sQ0FBYVIsUUFBYixDQUEvQjtBQUNBUyxTQUFHLEdBQUdiLEtBQUssQ0FBQ2MsTUFBTixDQUFhVixRQUFiLENBQU47QUFDQTBRLGNBQVEsR0FBRzlRLEtBQUssQ0FBQ2UsR0FBTixDQUFVbkwsS0FBVixDQUFnQitCLEdBQWhCLEVBQXFCa0osR0FBckIsQ0FBWDs7QUFFQSxVQUFJZ1EsY0FBYyxDQUFDM2QsQ0FBRCxDQUFkLENBQWtCLENBQWxCLEVBQXFCNkMsSUFBckIsQ0FBMEIrYSxRQUExQixDQUFKLEVBQXlDO0FBQ3ZDLFlBQUlBLFFBQVEsQ0FBQzdkLE1BQVQsS0FBb0IsQ0FBeEIsRUFBMkI7QUFBRW1OLGtCQUFRO0FBQUs7O0FBQzFDO0FBQ0Q7QUFDRjtBQUNGOztBQUVESixPQUFLLENBQUMwQixJQUFOLEdBQWF0QixRQUFiO0FBRUFHLE9BQUssR0FBV1AsS0FBSyxDQUFDMUwsSUFBTixDQUFXLFlBQVgsRUFBeUIsRUFBekIsRUFBNkIsQ0FBN0IsQ0FBaEI7QUFDQWlNLE9BQUssQ0FBQ25GLEdBQU4sR0FBZ0IsQ0FBRTZFLFNBQUYsRUFBYUcsUUFBYixDQUFoQjtBQUNBRyxPQUFLLENBQUNzTSxPQUFOLEdBQWdCN00sS0FBSyxDQUFDa1EsUUFBTixDQUFlalEsU0FBZixFQUEwQkcsUUFBMUIsRUFBb0NKLEtBQUssQ0FBQ21CLFNBQTFDLEVBQXFELElBQXJELENBQWhCO0FBRUEsU0FBTyxJQUFQO0FBQ0QsQ0FuREQsQzs7Ozs7Ozs7Ozs7O0FDdEJBO0FBRWE7O0FBR2I5TixNQUFNLENBQUNDLE9BQVAsR0FBaUIsU0FBU3lkLFFBQVQsQ0FBa0IvUSxLQUFsQixFQUF5QkMsU0FBekIsRUFBb0NDO0FBQU87QUFBM0MsRUFBeUQ7QUFDeEUsTUFBSTJNLE9BQUo7QUFBQSxNQUFhK0MsU0FBYjtBQUFBLE1BQXdCMWMsQ0FBeEI7QUFBQSxNQUEyQjBZLENBQTNCO0FBQUEsTUFBOEJyTCxLQUE5QjtBQUFBLE1BQXFDNUksR0FBckM7QUFBQSxNQUEwQ2tKLEdBQTFDO0FBQUEsTUFBK0MwRyxLQUEvQztBQUFBLE1BQXNEN0gsTUFBdEQ7QUFBQSxNQUNJVSxRQUFRLEdBQUdILFNBQVMsR0FBRyxDQUQzQjtBQUFBLE1BQzhCdVAsYUFEOUI7QUFBQSxNQUVJSyxlQUFlLEdBQUc3UCxLQUFLLENBQUNuQixFQUFOLENBQVMwQyxLQUFULENBQWVJLEtBQWYsQ0FBcUJ5SixRQUFyQixDQUE4QixXQUE5QixDQUZ0QixDQUR3RSxDQUt4RTs7QUFDQSxNQUFJcEwsS0FBSyxDQUFDa0IsTUFBTixDQUFhakIsU0FBYixJQUEwQkQsS0FBSyxDQUFDbUIsU0FBaEMsSUFBNkMsQ0FBakQsRUFBb0Q7QUFBRSxXQUFPLEtBQVA7QUFBZTs7QUFFckVxTyxlQUFhLEdBQUd4UCxLQUFLLENBQUNxQixVQUF0QjtBQUNBckIsT0FBSyxDQUFDcUIsVUFBTixHQUFtQixXQUFuQixDQVR3RSxDQVN4QztBQUVoQzs7QUFDQSxTQUFPakIsUUFBUSxHQUFHRixPQUFYLElBQXNCLENBQUNGLEtBQUssQ0FBQ3lMLE9BQU4sQ0FBY3JMLFFBQWQsQ0FBOUIsRUFBdURBLFFBQVEsRUFBL0QsRUFBbUU7QUFDakU7QUFDQTtBQUNBLFFBQUlKLEtBQUssQ0FBQ2tCLE1BQU4sQ0FBYWQsUUFBYixJQUF5QkosS0FBSyxDQUFDbUIsU0FBL0IsR0FBMkMsQ0FBL0MsRUFBa0Q7QUFBRTtBQUFXLEtBSEUsQ0FLakU7QUFDQTtBQUNBOzs7QUFDQSxRQUFJbkIsS0FBSyxDQUFDa0IsTUFBTixDQUFhZCxRQUFiLEtBQTBCSixLQUFLLENBQUNtQixTQUFwQyxFQUErQztBQUM3Q3hKLFNBQUcsR0FBR3FJLEtBQUssQ0FBQ1csTUFBTixDQUFhUCxRQUFiLElBQXlCSixLQUFLLENBQUNZLE1BQU4sQ0FBYVIsUUFBYixDQUEvQjtBQUNBUyxTQUFHLEdBQUdiLEtBQUssQ0FBQ2MsTUFBTixDQUFhVixRQUFiLENBQU47O0FBRUEsVUFBSXpJLEdBQUcsR0FBR2tKLEdBQVYsRUFBZTtBQUNibkIsY0FBTSxHQUFHTSxLQUFLLENBQUNlLEdBQU4sQ0FBVW5CLFVBQVYsQ0FBcUJqSSxHQUFyQixDQUFUOztBQUVBLFlBQUkrSCxNQUFNLEtBQUs7QUFBSTtBQUFmLFdBQTBCQSxNQUFNLEtBQUs7QUFBSTtBQUE3QyxVQUFzRDtBQUNwRC9ILGVBQUcsR0FBR3FJLEtBQUssQ0FBQ3FRLFNBQU4sQ0FBZ0IxWSxHQUFoQixFQUFxQitILE1BQXJCLENBQU47QUFDQS9ILGVBQUcsR0FBR3FJLEtBQUssQ0FBQ29CLFVBQU4sQ0FBaUJ6SixHQUFqQixDQUFOOztBQUVBLGdCQUFJQSxHQUFHLElBQUlrSixHQUFYLEVBQWdCO0FBQ2QwRyxtQkFBSyxHQUFJN0gsTUFBTSxLQUFLO0FBQUk7QUFBZixnQkFBeUIsQ0FBekIsR0FBNkIsQ0FBdEM7QUFDQTtBQUNEO0FBQ0Y7QUFDRjtBQUNGLEtBekJnRSxDQTJCakU7OztBQUNBLFFBQUlNLEtBQUssQ0FBQ2tCLE1BQU4sQ0FBYWQsUUFBYixJQUF5QixDQUE3QixFQUFnQztBQUFFO0FBQVcsS0E1Qm9CLENBOEJqRTs7O0FBQ0F3UCxhQUFTLEdBQUcsS0FBWjs7QUFDQSxTQUFLMWMsQ0FBQyxHQUFHLENBQUosRUFBTzBZLENBQUMsR0FBR2lFLGVBQWUsQ0FBQzVjLE1BQWhDLEVBQXdDQyxDQUFDLEdBQUcwWSxDQUE1QyxFQUErQzFZLENBQUMsRUFBaEQsRUFBb0Q7QUFDbEQsVUFBSTJjLGVBQWUsQ0FBQzNjLENBQUQsQ0FBZixDQUFtQjhNLEtBQW5CLEVBQTBCSSxRQUExQixFQUFvQ0YsT0FBcEMsRUFBNkMsSUFBN0MsQ0FBSixFQUF3RDtBQUN0RDBQLGlCQUFTLEdBQUcsSUFBWjtBQUNBO0FBQ0Q7QUFDRjs7QUFDRCxRQUFJQSxTQUFKLEVBQWU7QUFBRTtBQUFRO0FBQzFCOztBQUVELE1BQUksQ0FBQ3JJLEtBQUwsRUFBWTtBQUNWO0FBQ0EsV0FBTyxLQUFQO0FBQ0Q7O0FBRURzRixTQUFPLEdBQUc3TSxLQUFLLENBQUNrUSxRQUFOLENBQWVqUSxTQUFmLEVBQTBCRyxRQUExQixFQUFvQ0osS0FBSyxDQUFDbUIsU0FBMUMsRUFBcUQsS0FBckQsRUFBNERuQyxJQUE1RCxFQUFWO0FBRUFnQixPQUFLLENBQUMwQixJQUFOLEdBQWF0QixRQUFRLEdBQUcsQ0FBeEI7QUFFQUcsT0FBSyxHQUFZUCxLQUFLLENBQUMxTCxJQUFOLENBQVcsY0FBWCxFQUEyQixNQUFNK1EsTUFBTSxDQUFDa0MsS0FBRCxDQUF2QyxFQUFnRCxDQUFoRCxDQUFqQjtBQUNBaEgsT0FBSyxDQUFDRCxNQUFOLEdBQWlCK0UsTUFBTSxDQUFDQyxZQUFQLENBQW9CNUYsTUFBcEIsQ0FBakI7QUFDQWEsT0FBSyxDQUFDbkYsR0FBTixHQUFpQixDQUFFNkUsU0FBRixFQUFhRCxLQUFLLENBQUMwQixJQUFuQixDQUFqQjtBQUVBbkIsT0FBSyxHQUFZUCxLQUFLLENBQUMxTCxJQUFOLENBQVcsUUFBWCxFQUFxQixFQUFyQixFQUF5QixDQUF6QixDQUFqQjtBQUNBaU0sT0FBSyxDQUFDc00sT0FBTixHQUFpQkEsT0FBakI7QUFDQXRNLE9BQUssQ0FBQ25GLEdBQU4sR0FBaUIsQ0FBRTZFLFNBQUYsRUFBYUQsS0FBSyxDQUFDMEIsSUFBTixHQUFhLENBQTFCLENBQWpCO0FBQ0FuQixPQUFLLENBQUMrTSxRQUFOLEdBQWlCLEVBQWpCO0FBRUEvTSxPQUFLLEdBQVlQLEtBQUssQ0FBQzFMLElBQU4sQ0FBVyxlQUFYLEVBQTRCLE1BQU0rUSxNQUFNLENBQUNrQyxLQUFELENBQXhDLEVBQWlELENBQUMsQ0FBbEQsQ0FBakI7QUFDQWhILE9BQUssQ0FBQ0QsTUFBTixHQUFpQitFLE1BQU0sQ0FBQ0MsWUFBUCxDQUFvQjVGLE1BQXBCLENBQWpCO0FBRUFNLE9BQUssQ0FBQ3FCLFVBQU4sR0FBbUJtTyxhQUFuQjtBQUVBLFNBQU8sSUFBUDtBQUNELENBN0VELEM7Ozs7Ozs7Ozs7OztBQ0xBO0FBRWE7O0FBRWIsSUFBSTlJLE9BQU8sR0FBRzlSLG1CQUFPLENBQUMsMkVBQUQsQ0FBUCxDQUEyQjhSLE9BQXpDLEMsQ0FHQTtBQUNBOzs7QUFDQSxTQUFTc0ssb0JBQVQsQ0FBOEJoUixLQUE5QixFQUFxQ0MsU0FBckMsRUFBZ0Q7QUFDOUMsTUFBSVAsTUFBSixFQUFZL0gsR0FBWixFQUFpQmtKLEdBQWpCLEVBQXNCMEYsRUFBdEI7QUFFQTVPLEtBQUcsR0FBR3FJLEtBQUssQ0FBQ1csTUFBTixDQUFhVixTQUFiLElBQTBCRCxLQUFLLENBQUNZLE1BQU4sQ0FBYVgsU0FBYixDQUFoQztBQUNBWSxLQUFHLEdBQUdiLEtBQUssQ0FBQ2MsTUFBTixDQUFhYixTQUFiLENBQU47QUFFQVAsUUFBTSxHQUFHTSxLQUFLLENBQUNlLEdBQU4sQ0FBVW5CLFVBQVYsQ0FBcUJqSSxHQUFHLEVBQXhCLENBQVQsQ0FOOEMsQ0FPOUM7O0FBQ0EsTUFBSStILE1BQU0sS0FBSztBQUFJO0FBQWYsS0FDQUEsTUFBTSxLQUFLO0FBQUk7QUFEZixLQUVBQSxNQUFNLEtBQUs7QUFBSTtBQUZuQixJQUU0QjtBQUMxQixhQUFPLENBQUMsQ0FBUjtBQUNEOztBQUVELE1BQUkvSCxHQUFHLEdBQUdrSixHQUFWLEVBQWU7QUFDYjBGLE1BQUUsR0FBR3ZHLEtBQUssQ0FBQ2UsR0FBTixDQUFVbkIsVUFBVixDQUFxQmpJLEdBQXJCLENBQUw7O0FBRUEsUUFBSSxDQUFDK08sT0FBTyxDQUFDSCxFQUFELENBQVosRUFBa0I7QUFDaEI7QUFDQSxhQUFPLENBQUMsQ0FBUjtBQUNEO0FBQ0Y7O0FBRUQsU0FBTzVPLEdBQVA7QUFDRCxDLENBRUQ7QUFDQTs7O0FBQ0EsU0FBU3NaLHFCQUFULENBQStCalIsS0FBL0IsRUFBc0NDLFNBQXRDLEVBQWlEO0FBQy9DLE1BQUlzRyxFQUFKO0FBQUEsTUFDSTVLLEtBQUssR0FBR3FFLEtBQUssQ0FBQ1csTUFBTixDQUFhVixTQUFiLElBQTBCRCxLQUFLLENBQUNZLE1BQU4sQ0FBYVgsU0FBYixDQUR0QztBQUFBLE1BRUl0SSxHQUFHLEdBQUdnRSxLQUZWO0FBQUEsTUFHSWtGLEdBQUcsR0FBR2IsS0FBSyxDQUFDYyxNQUFOLENBQWFiLFNBQWIsQ0FIVixDQUQrQyxDQU0vQzs7QUFDQSxNQUFJdEksR0FBRyxHQUFHLENBQU4sSUFBV2tKLEdBQWYsRUFBb0I7QUFBRSxXQUFPLENBQUMsQ0FBUjtBQUFZOztBQUVsQzBGLElBQUUsR0FBR3ZHLEtBQUssQ0FBQ2UsR0FBTixDQUFVbkIsVUFBVixDQUFxQmpJLEdBQUcsRUFBeEIsQ0FBTDs7QUFFQSxNQUFJNE8sRUFBRSxHQUFHO0FBQUk7QUFBVCxLQUFvQkEsRUFBRSxHQUFHO0FBQUk7QUFBakMsSUFBMEM7QUFBRSxhQUFPLENBQUMsQ0FBUjtBQUFZOztBQUV4RCxXQUFTO0FBQ1A7QUFDQSxRQUFJNU8sR0FBRyxJQUFJa0osR0FBWCxFQUFnQjtBQUFFLGFBQU8sQ0FBQyxDQUFSO0FBQVk7O0FBRTlCMEYsTUFBRSxHQUFHdkcsS0FBSyxDQUFDZSxHQUFOLENBQVVuQixVQUFWLENBQXFCakksR0FBRyxFQUF4QixDQUFMOztBQUVBLFFBQUk0TyxFQUFFLElBQUk7QUFBSTtBQUFWLE9BQXFCQSxFQUFFLElBQUk7QUFBSTtBQUFuQyxNQUE0QztBQUUxQztBQUNBO0FBQ0EsWUFBSTVPLEdBQUcsR0FBR2dFLEtBQU4sSUFBZSxFQUFuQixFQUF1QjtBQUFFLGlCQUFPLENBQUMsQ0FBUjtBQUFZOztBQUVyQztBQUNELE9BYk0sQ0FlUDs7O0FBQ0EsUUFBSTRLLEVBQUUsS0FBSztBQUFJO0FBQVgsT0FBc0JBLEVBQUUsS0FBSztBQUFJO0FBQXJDLE1BQThDO0FBQzVDO0FBQ0Q7O0FBRUQsV0FBTyxDQUFDLENBQVI7QUFDRDs7QUFHRCxNQUFJNU8sR0FBRyxHQUFHa0osR0FBVixFQUFlO0FBQ2IwRixNQUFFLEdBQUd2RyxLQUFLLENBQUNlLEdBQU4sQ0FBVW5CLFVBQVYsQ0FBcUJqSSxHQUFyQixDQUFMOztBQUVBLFFBQUksQ0FBQytPLE9BQU8sQ0FBQ0gsRUFBRCxDQUFaLEVBQWtCO0FBQ2hCO0FBQ0EsYUFBTyxDQUFDLENBQVI7QUFDRDtBQUNGOztBQUNELFNBQU81TyxHQUFQO0FBQ0Q7O0FBRUQsU0FBU3VaLG1CQUFULENBQTZCbFIsS0FBN0IsRUFBb0NyQyxHQUFwQyxFQUF5QztBQUN2QyxNQUFJekssQ0FBSjtBQUFBLE1BQU8wWSxDQUFQO0FBQUEsTUFDSXJFLEtBQUssR0FBR3ZILEtBQUssQ0FBQ3VILEtBQU4sR0FBYyxDQUQxQjs7QUFHQSxPQUFLclUsQ0FBQyxHQUFHeUssR0FBRyxHQUFHLENBQVYsRUFBYWlPLENBQUMsR0FBRzVMLEtBQUssQ0FBQ2QsTUFBTixDQUFhak0sTUFBYixHQUFzQixDQUE1QyxFQUErQ0MsQ0FBQyxHQUFHMFksQ0FBbkQsRUFBc0QxWSxDQUFDLEVBQXZELEVBQTJEO0FBQ3pELFFBQUk4TSxLQUFLLENBQUNkLE1BQU4sQ0FBYWhNLENBQWIsRUFBZ0JxVSxLQUFoQixLQUEwQkEsS0FBMUIsSUFBbUN2SCxLQUFLLENBQUNkLE1BQU4sQ0FBYWhNLENBQWIsRUFBZ0I0YSxJQUFoQixLQUF5QixnQkFBaEUsRUFBa0Y7QUFDaEY5TixXQUFLLENBQUNkLE1BQU4sQ0FBYWhNLENBQUMsR0FBRyxDQUFqQixFQUFvQjJhLE1BQXBCLEdBQTZCLElBQTdCO0FBQ0E3TixXQUFLLENBQUNkLE1BQU4sQ0FBYWhNLENBQWIsRUFBZ0IyYSxNQUFoQixHQUF5QixJQUF6QjtBQUNBM2EsT0FBQyxJQUFJLENBQUw7QUFDRDtBQUNGO0FBQ0Y7O0FBR0RHLE1BQU0sQ0FBQ0MsT0FBUCxHQUFpQixTQUFTZ0ssSUFBVCxDQUFjMEMsS0FBZCxFQUFxQkMsU0FBckIsRUFBZ0NDLE9BQWhDLEVBQXlDQyxNQUF6QyxFQUFpRDtBQUNoRSxNQUFJb0csRUFBSjtBQUFBLE1BQ0k0SyxZQURKO0FBQUEsTUFFSWplLENBRko7QUFBQSxNQUdJa2UsTUFISjtBQUFBLE1BSUlDLGlCQUpKO0FBQUEsTUFLSW5DLE9BTEo7QUFBQSxNQU1Jb0MsU0FOSjtBQUFBLE1BT0lDLFNBUEo7QUFBQSxNQVFJM0YsQ0FSSjtBQUFBLE1BU0k0RixTQVRKO0FBQUEsTUFVSUMsVUFWSjtBQUFBLE1BV0lDLGNBWEo7QUFBQSxNQVlJQyxXQVpKO0FBQUEsTUFhSTlRLEdBYko7QUFBQSxNQWNJVCxRQWRKO0FBQUEsTUFlSWdQLE1BZko7QUFBQSxNQWdCSXdDLGFBaEJKO0FBQUEsTUFpQklwQyxhQWpCSjtBQUFBLE1Ba0JJQyxTQWxCSjtBQUFBLE1BbUJJQyxTQW5CSjtBQUFBLE1Bb0JJbUMsUUFwQko7QUFBQSxNQXFCSWxhLEdBckJKO0FBQUEsTUFzQkltYSxjQXRCSjtBQUFBLE1BdUJJQyxZQXZCSjtBQUFBLE1Bd0JJcFcsS0F4Qko7QUFBQSxNQXlCSWlVLFNBekJKO0FBQUEsTUEwQklDLGVBMUJKO0FBQUEsTUEyQkl0UCxLQTNCSjtBQUFBLE1BNEJJeVIsc0JBQXNCLEdBQUcsS0E1QjdCO0FBQUEsTUE2Qkl4RyxLQUFLLEdBQUcsSUE3QlosQ0FEZ0UsQ0FnQ2hFOztBQUNBLE1BQUl4TCxLQUFLLENBQUNrQixNQUFOLENBQWFqQixTQUFiLElBQTBCRCxLQUFLLENBQUNtQixTQUFoQyxJQUE2QyxDQUFqRCxFQUFvRDtBQUFFLFdBQU8sS0FBUDtBQUFlLEdBakNMLENBbUNoRTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNBLE1BQUluQixLQUFLLENBQUNpUyxVQUFOLElBQW9CLENBQXBCLElBQ0FqUyxLQUFLLENBQUNrQixNQUFOLENBQWFqQixTQUFiLElBQTBCRCxLQUFLLENBQUNpUyxVQUFoQyxJQUE4QyxDQUQ5QyxJQUVBalMsS0FBSyxDQUFDa0IsTUFBTixDQUFhakIsU0FBYixJQUEwQkQsS0FBSyxDQUFDbUIsU0FGcEMsRUFFK0M7QUFDN0MsV0FBTyxLQUFQO0FBQ0QsR0E3QytELENBK0NoRTtBQUNBOzs7QUFDQSxNQUFJaEIsTUFBTSxJQUFJSCxLQUFLLENBQUNxQixVQUFOLEtBQXFCLFdBQW5DLEVBQWdEO0FBQzlDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxRQUFJckIsS0FBSyxDQUFDWSxNQUFOLENBQWFYLFNBQWIsS0FBMkJELEtBQUssQ0FBQ21CLFNBQXJDLEVBQWdEO0FBQzlDNlEsNEJBQXNCLEdBQUcsSUFBekI7QUFDRDtBQUNGLEdBMUQrRCxDQTREaEU7OztBQUNBLE1BQUksQ0FBQ0YsY0FBYyxHQUFHYixxQkFBcUIsQ0FBQ2pSLEtBQUQsRUFBUUMsU0FBUixDQUF2QyxLQUE4RCxDQUFsRSxFQUFxRTtBQUNuRXFSLGFBQVMsR0FBRyxJQUFaO0FBQ0EzVixTQUFLLEdBQUdxRSxLQUFLLENBQUNXLE1BQU4sQ0FBYVYsU0FBYixJQUEwQkQsS0FBSyxDQUFDWSxNQUFOLENBQWFYLFNBQWIsQ0FBbEM7QUFDQTBSLGVBQVcsR0FBR08sTUFBTSxDQUFDbFMsS0FBSyxDQUFDZSxHQUFOLENBQVVvUixNQUFWLENBQWlCeFcsS0FBakIsRUFBd0JtVyxjQUFjLEdBQUduVyxLQUFqQixHQUF5QixDQUFqRCxDQUFELENBQXBCLENBSG1FLENBS25FO0FBQ0E7O0FBQ0EsUUFBSXFXLHNCQUFzQixJQUFJTCxXQUFXLEtBQUssQ0FBOUMsRUFBaUQsT0FBTyxLQUFQO0FBRWxELEdBVEQsTUFTTyxJQUFJLENBQUNHLGNBQWMsR0FBR2Qsb0JBQW9CLENBQUNoUixLQUFELEVBQVFDLFNBQVIsQ0FBdEMsS0FBNkQsQ0FBakUsRUFBb0U7QUFDekVxUixhQUFTLEdBQUcsS0FBWjtBQUVELEdBSE0sTUFHQTtBQUNMLFdBQU8sS0FBUDtBQUNELEdBM0UrRCxDQTZFaEU7QUFDQTs7O0FBQ0EsTUFBSVUsc0JBQUosRUFBNEI7QUFDMUIsUUFBSWhTLEtBQUssQ0FBQ29CLFVBQU4sQ0FBaUIwUSxjQUFqQixLQUFvQzlSLEtBQUssQ0FBQ2MsTUFBTixDQUFhYixTQUFiLENBQXhDLEVBQWlFLE9BQU8sS0FBUDtBQUNsRSxHQWpGK0QsQ0FtRmhFOzs7QUFDQXlSLGdCQUFjLEdBQUcxUixLQUFLLENBQUNlLEdBQU4sQ0FBVW5CLFVBQVYsQ0FBcUJrUyxjQUFjLEdBQUcsQ0FBdEMsQ0FBakIsQ0FwRmdFLENBc0ZoRTs7QUFDQSxNQUFJM1IsTUFBSixFQUFZO0FBQUUsV0FBTyxJQUFQO0FBQWMsR0F2Rm9DLENBeUZoRTs7O0FBQ0FzUixZQUFVLEdBQUd6UixLQUFLLENBQUNkLE1BQU4sQ0FBYWpNLE1BQTFCOztBQUVBLE1BQUlxZSxTQUFKLEVBQWU7QUFDYi9RLFNBQUssR0FBU1AsS0FBSyxDQUFDMUwsSUFBTixDQUFXLG1CQUFYLEVBQWdDLElBQWhDLEVBQXNDLENBQXRDLENBQWQ7O0FBQ0EsUUFBSXFkLFdBQVcsS0FBSyxDQUFwQixFQUF1QjtBQUNyQnBSLFdBQUssQ0FBQzRDLEtBQU4sR0FBYyxDQUFFLENBQUUsT0FBRixFQUFXd08sV0FBWCxDQUFGLENBQWQ7QUFDRDtBQUVGLEdBTkQsTUFNTztBQUNMcFIsU0FBSyxHQUFTUCxLQUFLLENBQUMxTCxJQUFOLENBQVcsa0JBQVgsRUFBK0IsSUFBL0IsRUFBcUMsQ0FBckMsQ0FBZDtBQUNEOztBQUVEaU0sT0FBSyxDQUFDbkYsR0FBTixHQUFlb1csU0FBUyxHQUFHLENBQUV2UixTQUFGLEVBQWEsQ0FBYixDQUEzQjtBQUNBTSxPQUFLLENBQUNELE1BQU4sR0FBZStFLE1BQU0sQ0FBQ0MsWUFBUCxDQUFvQm9NLGNBQXBCLENBQWYsQ0F2R2dFLENBeUdoRTtBQUNBO0FBQ0E7O0FBRUF0UixVQUFRLEdBQUdILFNBQVg7QUFDQThSLGNBQVksR0FBRyxLQUFmO0FBQ0FsQyxpQkFBZSxHQUFHN1AsS0FBSyxDQUFDbkIsRUFBTixDQUFTMEMsS0FBVCxDQUFlSSxLQUFmLENBQXFCeUosUUFBckIsQ0FBOEIsTUFBOUIsQ0FBbEI7QUFFQW9FLGVBQWEsR0FBR3hQLEtBQUssQ0FBQ3FCLFVBQXRCO0FBQ0FyQixPQUFLLENBQUNxQixVQUFOLEdBQW1CLE1BQW5COztBQUVBLFNBQU9qQixRQUFRLEdBQUdGLE9BQWxCLEVBQTJCO0FBQ3pCdkksT0FBRyxHQUFHbWEsY0FBTjtBQUNBalIsT0FBRyxHQUFHYixLQUFLLENBQUNjLE1BQU4sQ0FBYVYsUUFBYixDQUFOO0FBRUE4TyxXQUFPLEdBQUdFLE1BQU0sR0FBR3BQLEtBQUssQ0FBQ2tCLE1BQU4sQ0FBYWQsUUFBYixJQUF5QjBSLGNBQXpCLElBQTJDOVIsS0FBSyxDQUFDVyxNQUFOLENBQWFWLFNBQWIsSUFBMEJELEtBQUssQ0FBQ1ksTUFBTixDQUFhWCxTQUFiLENBQXJFLENBQW5COztBQUVBLFdBQU90SSxHQUFHLEdBQUdrSixHQUFiLEVBQWtCO0FBQ2hCMEYsUUFBRSxHQUFHdkcsS0FBSyxDQUFDZSxHQUFOLENBQVVuQixVQUFWLENBQXFCakksR0FBckIsQ0FBTDs7QUFFQSxVQUFJNE8sRUFBRSxLQUFLLElBQVgsRUFBaUI7QUFDZjZJLGNBQU0sSUFBSSxJQUFJLENBQUNBLE1BQU0sR0FBR3BQLEtBQUssQ0FBQ2dRLE9BQU4sQ0FBYzVQLFFBQWQsQ0FBVixJQUFxQyxDQUFuRDtBQUNELE9BRkQsTUFFTyxJQUFJbUcsRUFBRSxLQUFLLElBQVgsRUFBaUI7QUFDdEI2SSxjQUFNO0FBQ1AsT0FGTSxNQUVBO0FBQ0w7QUFDRDs7QUFFRHpYLFNBQUc7QUFDSjs7QUFFRHdaLGdCQUFZLEdBQUd4WixHQUFmOztBQUVBLFFBQUl3WixZQUFZLElBQUl0USxHQUFwQixFQUF5QjtBQUN2QjtBQUNBd1EsdUJBQWlCLEdBQUcsQ0FBcEI7QUFDRCxLQUhELE1BR087QUFDTEEsdUJBQWlCLEdBQUdqQyxNQUFNLEdBQUdGLE9BQTdCO0FBQ0QsS0EzQndCLENBNkJ6QjtBQUNBOzs7QUFDQSxRQUFJbUMsaUJBQWlCLEdBQUcsQ0FBeEIsRUFBMkI7QUFBRUEsdUJBQWlCLEdBQUcsQ0FBcEI7QUFBd0IsS0EvQjVCLENBaUN6QjtBQUNBOzs7QUFDQUQsVUFBTSxHQUFHbEMsT0FBTyxHQUFHbUMsaUJBQW5CLENBbkN5QixDQXFDekI7O0FBQ0E5USxTQUFLLEdBQVVQLEtBQUssQ0FBQzFMLElBQU4sQ0FBVyxnQkFBWCxFQUE2QixJQUE3QixFQUFtQyxDQUFuQyxDQUFmO0FBQ0FpTSxTQUFLLENBQUNELE1BQU4sR0FBZStFLE1BQU0sQ0FBQ0MsWUFBUCxDQUFvQm9NLGNBQXBCLENBQWY7QUFDQW5SLFNBQUssQ0FBQ25GLEdBQU4sR0FBZW1XLFNBQVMsR0FBRyxDQUFFdFIsU0FBRixFQUFhLENBQWIsQ0FBM0IsQ0F4Q3lCLENBMEN6Qjs7QUFDQTRSLFlBQVEsR0FBRzdSLEtBQUssQ0FBQ3dMLEtBQWpCO0FBQ0FrRSxhQUFTLEdBQUcxUCxLQUFLLENBQUNZLE1BQU4sQ0FBYVgsU0FBYixDQUFaO0FBQ0F3UCxhQUFTLEdBQUd6UCxLQUFLLENBQUNrQixNQUFOLENBQWFqQixTQUFiLENBQVosQ0E3Q3lCLENBK0N6QjtBQUNBO0FBQ0E7QUFDQTs7QUFDQTJSLGlCQUFhLEdBQUc1UixLQUFLLENBQUNpUyxVQUF0QjtBQUNBalMsU0FBSyxDQUFDaVMsVUFBTixHQUFtQmpTLEtBQUssQ0FBQ21CLFNBQXpCO0FBQ0FuQixTQUFLLENBQUNtQixTQUFOLEdBQWtCaVEsTUFBbEI7QUFFQXBSLFNBQUssQ0FBQ3dMLEtBQU4sR0FBYyxJQUFkO0FBQ0F4TCxTQUFLLENBQUNZLE1BQU4sQ0FBYVgsU0FBYixJQUEwQmtSLFlBQVksR0FBR25SLEtBQUssQ0FBQ1csTUFBTixDQUFhVixTQUFiLENBQXpDO0FBQ0FELFNBQUssQ0FBQ2tCLE1BQU4sQ0FBYWpCLFNBQWIsSUFBMEJtUCxNQUExQjs7QUFFQSxRQUFJK0IsWUFBWSxJQUFJdFEsR0FBaEIsSUFBdUJiLEtBQUssQ0FBQ3lMLE9BQU4sQ0FBY3hMLFNBQVMsR0FBRyxDQUExQixDQUEzQixFQUF5RDtBQUN2RDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBRCxXQUFLLENBQUMwQixJQUFOLEdBQWFWLElBQUksQ0FBQ29SLEdBQUwsQ0FBU3BTLEtBQUssQ0FBQzBCLElBQU4sR0FBYSxDQUF0QixFQUF5QnhCLE9BQXpCLENBQWI7QUFDRCxLQVRELE1BU087QUFDTEYsV0FBSyxDQUFDbkIsRUFBTixDQUFTMEMsS0FBVCxDQUFlRSxRQUFmLENBQXdCekIsS0FBeEIsRUFBK0JDLFNBQS9CLEVBQTBDQyxPQUExQyxFQUFtRCxJQUFuRDtBQUNELEtBdEV3QixDQXdFekI7OztBQUNBLFFBQUksQ0FBQ0YsS0FBSyxDQUFDd0wsS0FBUCxJQUFnQnVHLFlBQXBCLEVBQWtDO0FBQ2hDdkcsV0FBSyxHQUFHLEtBQVI7QUFDRCxLQTNFd0IsQ0E0RXpCO0FBQ0E7OztBQUNBdUcsZ0JBQVksR0FBSS9SLEtBQUssQ0FBQzBCLElBQU4sR0FBYXpCLFNBQWQsR0FBMkIsQ0FBM0IsSUFBZ0NELEtBQUssQ0FBQ3lMLE9BQU4sQ0FBY3pMLEtBQUssQ0FBQzBCLElBQU4sR0FBYSxDQUEzQixDQUEvQztBQUVBMUIsU0FBSyxDQUFDbUIsU0FBTixHQUFrQm5CLEtBQUssQ0FBQ2lTLFVBQXhCO0FBQ0FqUyxTQUFLLENBQUNpUyxVQUFOLEdBQW1CTCxhQUFuQjtBQUNBNVIsU0FBSyxDQUFDWSxNQUFOLENBQWFYLFNBQWIsSUFBMEJ5UCxTQUExQjtBQUNBMVAsU0FBSyxDQUFDa0IsTUFBTixDQUFhakIsU0FBYixJQUEwQndQLFNBQTFCO0FBQ0F6UCxTQUFLLENBQUN3TCxLQUFOLEdBQWNxRyxRQUFkO0FBRUF0UixTQUFLLEdBQVVQLEtBQUssQ0FBQzFMLElBQU4sQ0FBVyxpQkFBWCxFQUE4QixJQUE5QixFQUFvQyxDQUFDLENBQXJDLENBQWY7QUFDQWlNLFNBQUssQ0FBQ0QsTUFBTixHQUFlK0UsTUFBTSxDQUFDQyxZQUFQLENBQW9Cb00sY0FBcEIsQ0FBZjtBQUVBdFIsWUFBUSxHQUFHSCxTQUFTLEdBQUdELEtBQUssQ0FBQzBCLElBQTdCO0FBQ0E2UCxhQUFTLENBQUMsQ0FBRCxDQUFULEdBQWVuUixRQUFmO0FBQ0ErUSxnQkFBWSxHQUFHblIsS0FBSyxDQUFDVyxNQUFOLENBQWFWLFNBQWIsQ0FBZjs7QUFFQSxRQUFJRyxRQUFRLElBQUlGLE9BQWhCLEVBQXlCO0FBQUU7QUFBUSxLQTdGVixDQStGekI7QUFDQTtBQUNBOzs7QUFDQSxRQUFJRixLQUFLLENBQUNrQixNQUFOLENBQWFkLFFBQWIsSUFBeUJKLEtBQUssQ0FBQ21CLFNBQW5DLEVBQThDO0FBQUU7QUFBUSxLQWxHL0IsQ0FvR3pCOzs7QUFDQSxRQUFJbkIsS0FBSyxDQUFDa0IsTUFBTixDQUFhakIsU0FBYixJQUEwQkQsS0FBSyxDQUFDbUIsU0FBaEMsSUFBNkMsQ0FBakQsRUFBb0Q7QUFBRTtBQUFRLEtBckdyQyxDQXVHekI7OztBQUNBeU8sYUFBUyxHQUFHLEtBQVo7O0FBQ0EsU0FBSzFjLENBQUMsR0FBRyxDQUFKLEVBQU8wWSxDQUFDLEdBQUdpRSxlQUFlLENBQUM1YyxNQUFoQyxFQUF3Q0MsQ0FBQyxHQUFHMFksQ0FBNUMsRUFBK0MxWSxDQUFDLEVBQWhELEVBQW9EO0FBQ2xELFVBQUkyYyxlQUFlLENBQUMzYyxDQUFELENBQWYsQ0FBbUI4TSxLQUFuQixFQUEwQkksUUFBMUIsRUFBb0NGLE9BQXBDLEVBQTZDLElBQTdDLENBQUosRUFBd0Q7QUFDdEQwUCxpQkFBUyxHQUFHLElBQVo7QUFDQTtBQUNEO0FBQ0Y7O0FBQ0QsUUFBSUEsU0FBSixFQUFlO0FBQUU7QUFBUSxLQS9HQSxDQWlIekI7OztBQUNBLFFBQUkwQixTQUFKLEVBQWU7QUFDYlEsb0JBQWMsR0FBR2IscUJBQXFCLENBQUNqUixLQUFELEVBQVFJLFFBQVIsQ0FBdEM7O0FBQ0EsVUFBSTBSLGNBQWMsR0FBRyxDQUFyQixFQUF3QjtBQUFFO0FBQVE7QUFDbkMsS0FIRCxNQUdPO0FBQ0xBLG9CQUFjLEdBQUdkLG9CQUFvQixDQUFDaFIsS0FBRCxFQUFRSSxRQUFSLENBQXJDOztBQUNBLFVBQUkwUixjQUFjLEdBQUcsQ0FBckIsRUFBd0I7QUFBRTtBQUFRO0FBQ25DOztBQUVELFFBQUlKLGNBQWMsS0FBSzFSLEtBQUssQ0FBQ2UsR0FBTixDQUFVbkIsVUFBVixDQUFxQmtTLGNBQWMsR0FBRyxDQUF0QyxDQUF2QixFQUFpRTtBQUFFO0FBQVE7QUFDNUUsR0EvTytELENBaVBoRTs7O0FBQ0EsTUFBSVIsU0FBSixFQUFlO0FBQ2IvUSxTQUFLLEdBQUdQLEtBQUssQ0FBQzFMLElBQU4sQ0FBVyxvQkFBWCxFQUFpQyxJQUFqQyxFQUF1QyxDQUFDLENBQXhDLENBQVI7QUFDRCxHQUZELE1BRU87QUFDTGlNLFNBQUssR0FBR1AsS0FBSyxDQUFDMUwsSUFBTixDQUFXLG1CQUFYLEVBQWdDLElBQWhDLEVBQXNDLENBQUMsQ0FBdkMsQ0FBUjtBQUNEOztBQUNEaU0sT0FBSyxDQUFDRCxNQUFOLEdBQWUrRSxNQUFNLENBQUNDLFlBQVAsQ0FBb0JvTSxjQUFwQixDQUFmO0FBRUFGLFdBQVMsQ0FBQyxDQUFELENBQVQsR0FBZXBSLFFBQWY7QUFDQUosT0FBSyxDQUFDMEIsSUFBTixHQUFhdEIsUUFBYjtBQUVBSixPQUFLLENBQUNxQixVQUFOLEdBQW1CbU8sYUFBbkIsQ0E1UGdFLENBOFBoRTs7QUFDQSxNQUFJaEUsS0FBSixFQUFXO0FBQ1QwRix1QkFBbUIsQ0FBQ2xSLEtBQUQsRUFBUXlSLFVBQVIsQ0FBbkI7QUFDRDs7QUFFRCxTQUFPLElBQVA7QUFDRCxDQXBRRCxDOzs7Ozs7Ozs7Ozs7QUNuR0E7QUFFYTs7QUFHYnBlLE1BQU0sQ0FBQ0MsT0FBUCxHQUFpQixTQUFTK2UsU0FBVCxDQUFtQnJTLEtBQW5CLEVBQTBCQztBQUFTO0FBQW5DLEVBQWtEO0FBQ2pFLE1BQUk0TSxPQUFKO0FBQUEsTUFBYStDLFNBQWI7QUFBQSxNQUF3QjFjLENBQXhCO0FBQUEsTUFBMkIwWSxDQUEzQjtBQUFBLE1BQThCckwsS0FBOUI7QUFBQSxNQUFxQ2lQLGFBQXJDO0FBQUEsTUFDSXBQLFFBQVEsR0FBR0gsU0FBUyxHQUFHLENBRDNCO0FBQUEsTUFFSTRQLGVBQWUsR0FBRzdQLEtBQUssQ0FBQ25CLEVBQU4sQ0FBUzBDLEtBQVQsQ0FBZUksS0FBZixDQUFxQnlKLFFBQXJCLENBQThCLFdBQTlCLENBRnRCO0FBQUEsTUFHSWxMLE9BQU8sR0FBR0YsS0FBSyxDQUFDc0IsT0FIcEI7QUFLQWtPLGVBQWEsR0FBR3hQLEtBQUssQ0FBQ3FCLFVBQXRCO0FBQ0FyQixPQUFLLENBQUNxQixVQUFOLEdBQW1CLFdBQW5CLENBUGlFLENBU2pFOztBQUNBLFNBQU9qQixRQUFRLEdBQUdGLE9BQVgsSUFBc0IsQ0FBQ0YsS0FBSyxDQUFDeUwsT0FBTixDQUFjckwsUUFBZCxDQUE5QixFQUF1REEsUUFBUSxFQUEvRCxFQUFtRTtBQUNqRTtBQUNBO0FBQ0EsUUFBSUosS0FBSyxDQUFDa0IsTUFBTixDQUFhZCxRQUFiLElBQXlCSixLQUFLLENBQUNtQixTQUEvQixHQUEyQyxDQUEvQyxFQUFrRDtBQUFFO0FBQVcsS0FIRSxDQUtqRTs7O0FBQ0EsUUFBSW5CLEtBQUssQ0FBQ2tCLE1BQU4sQ0FBYWQsUUFBYixJQUF5QixDQUE3QixFQUFnQztBQUFFO0FBQVcsS0FOb0IsQ0FRakU7OztBQUNBd1AsYUFBUyxHQUFHLEtBQVo7O0FBQ0EsU0FBSzFjLENBQUMsR0FBRyxDQUFKLEVBQU8wWSxDQUFDLEdBQUdpRSxlQUFlLENBQUM1YyxNQUFoQyxFQUF3Q0MsQ0FBQyxHQUFHMFksQ0FBNUMsRUFBK0MxWSxDQUFDLEVBQWhELEVBQW9EO0FBQ2xELFVBQUkyYyxlQUFlLENBQUMzYyxDQUFELENBQWYsQ0FBbUI4TSxLQUFuQixFQUEwQkksUUFBMUIsRUFBb0NGLE9BQXBDLEVBQTZDLElBQTdDLENBQUosRUFBd0Q7QUFDdEQwUCxpQkFBUyxHQUFHLElBQVo7QUFDQTtBQUNEO0FBQ0Y7O0FBQ0QsUUFBSUEsU0FBSixFQUFlO0FBQUU7QUFBUTtBQUMxQjs7QUFFRC9DLFNBQU8sR0FBRzdNLEtBQUssQ0FBQ2tRLFFBQU4sQ0FBZWpRLFNBQWYsRUFBMEJHLFFBQTFCLEVBQW9DSixLQUFLLENBQUNtQixTQUExQyxFQUFxRCxLQUFyRCxFQUE0RG5DLElBQTVELEVBQVY7QUFFQWdCLE9BQUssQ0FBQzBCLElBQU4sR0FBYXRCLFFBQWI7QUFFQUcsT0FBSyxHQUFZUCxLQUFLLENBQUMxTCxJQUFOLENBQVcsZ0JBQVgsRUFBNkIsR0FBN0IsRUFBa0MsQ0FBbEMsQ0FBakI7QUFDQWlNLE9BQUssQ0FBQ25GLEdBQU4sR0FBaUIsQ0FBRTZFLFNBQUYsRUFBYUQsS0FBSyxDQUFDMEIsSUFBbkIsQ0FBakI7QUFFQW5CLE9BQUssR0FBWVAsS0FBSyxDQUFDMUwsSUFBTixDQUFXLFFBQVgsRUFBcUIsRUFBckIsRUFBeUIsQ0FBekIsQ0FBakI7QUFDQWlNLE9BQUssQ0FBQ3NNLE9BQU4sR0FBaUJBLE9BQWpCO0FBQ0F0TSxPQUFLLENBQUNuRixHQUFOLEdBQWlCLENBQUU2RSxTQUFGLEVBQWFELEtBQUssQ0FBQzBCLElBQW5CLENBQWpCO0FBQ0FuQixPQUFLLENBQUMrTSxRQUFOLEdBQWlCLEVBQWpCO0FBRUEvTSxPQUFLLEdBQVlQLEtBQUssQ0FBQzFMLElBQU4sQ0FBVyxpQkFBWCxFQUE4QixHQUE5QixFQUFtQyxDQUFDLENBQXBDLENBQWpCO0FBRUEwTCxPQUFLLENBQUNxQixVQUFOLEdBQW1CbU8sYUFBbkI7QUFFQSxTQUFPLElBQVA7QUFDRCxDQTlDRCxDOzs7Ozs7Ozs7Ozs7QUNMYTs7QUFHYixJQUFJekksa0JBQWtCLEdBQUtuUyxtQkFBTyxDQUFDLDJFQUFELENBQVAsQ0FBMkJtUyxrQkFBdEQ7O0FBQ0EsSUFBSUwsT0FBTyxHQUFnQjlSLG1CQUFPLENBQUMsMkVBQUQsQ0FBUCxDQUEyQjhSLE9BQXREOztBQUdBclQsTUFBTSxDQUFDQyxPQUFQLEdBQWlCLFNBQVNnZixTQUFULENBQW1CdFMsS0FBbkIsRUFBMEJDLFNBQTFCLEVBQXFDc1MsUUFBckMsRUFBK0NwUyxNQUEvQyxFQUF1RDtBQUN0RSxNQUFJb0csRUFBSjtBQUFBLE1BQ0lpTSxVQURKO0FBQUEsTUFFSUMsYUFGSjtBQUFBLE1BR0l2UyxPQUhKO0FBQUEsTUFJSWdELElBSko7QUFBQSxNQUtJaFEsQ0FMSjtBQUFBLE1BTUkwWSxDQU5KO0FBQUEsTUFPSThHLEtBUEo7QUFBQSxNQVFJN0ssUUFSSjtBQUFBLE1BU0kySCxhQVRKO0FBQUEsTUFVSW1ELEdBVko7QUFBQSxNQVdJaFgsS0FYSjtBQUFBLE1BWUk5RSxHQVpKO0FBQUEsTUFhSStZLFNBYko7QUFBQSxNQWNJQyxlQWRKO0FBQUEsTUFlSStDLEtBZko7QUFBQSxNQWdCSXBMLEtBQUssR0FBRyxDQWhCWjtBQUFBLE1BaUJJN1AsR0FBRyxHQUFHcUksS0FBSyxDQUFDVyxNQUFOLENBQWFWLFNBQWIsSUFBMEJELEtBQUssQ0FBQ1ksTUFBTixDQUFhWCxTQUFiLENBakJwQztBQUFBLE1Ba0JJWSxHQUFHLEdBQUdiLEtBQUssQ0FBQ2MsTUFBTixDQUFhYixTQUFiLENBbEJWO0FBQUEsTUFtQklHLFFBQVEsR0FBR0gsU0FBUyxHQUFHLENBbkIzQixDQURzRSxDQXNCdEU7O0FBQ0EsTUFBSUQsS0FBSyxDQUFDa0IsTUFBTixDQUFhakIsU0FBYixJQUEwQkQsS0FBSyxDQUFDbUIsU0FBaEMsSUFBNkMsQ0FBakQsRUFBb0Q7QUFBRSxXQUFPLEtBQVA7QUFBZTs7QUFFckUsTUFBSW5CLEtBQUssQ0FBQ2UsR0FBTixDQUFVbkIsVUFBVixDQUFxQmpJLEdBQXJCLE1BQThCO0FBQUk7QUFBdEMsSUFBK0M7QUFBRSxhQUFPLEtBQVA7QUFBZSxLQXpCTSxDQTJCdEU7QUFDQTs7O0FBQ0EsU0FBTyxFQUFFQSxHQUFGLEdBQVFrSixHQUFmLEVBQW9CO0FBQ2xCLFFBQUliLEtBQUssQ0FBQ2UsR0FBTixDQUFVbkIsVUFBVixDQUFxQmpJLEdBQXJCLE1BQThCO0FBQUs7QUFBbkMsT0FDQXFJLEtBQUssQ0FBQ2UsR0FBTixDQUFVbkIsVUFBVixDQUFxQmpJLEdBQUcsR0FBRyxDQUEzQixNQUFrQztBQUFJO0FBRDFDLE1BQ21EO0FBQ2pELFlBQUlBLEdBQUcsR0FBRyxDQUFOLEtBQVlrSixHQUFoQixFQUFxQjtBQUFFLGlCQUFPLEtBQVA7QUFBZTs7QUFDdEMsWUFBSWIsS0FBSyxDQUFDZSxHQUFOLENBQVVuQixVQUFWLENBQXFCakksR0FBRyxHQUFHLENBQTNCLE1BQWtDO0FBQUk7QUFBMUMsVUFBbUQ7QUFBRSxtQkFBTyxLQUFQO0FBQWU7O0FBQ3BFO0FBQ0Q7QUFDRjs7QUFFRHVJLFNBQU8sR0FBR0YsS0FBSyxDQUFDc0IsT0FBaEIsQ0F0Q3NFLENBd0N0RTs7QUFDQXVPLGlCQUFlLEdBQUc3UCxLQUFLLENBQUNuQixFQUFOLENBQVMwQyxLQUFULENBQWVJLEtBQWYsQ0FBcUJ5SixRQUFyQixDQUE4QixXQUE5QixDQUFsQjtBQUVBb0UsZUFBYSxHQUFHeFAsS0FBSyxDQUFDcUIsVUFBdEI7QUFDQXJCLE9BQUssQ0FBQ3FCLFVBQU4sR0FBbUIsV0FBbkI7O0FBRUEsU0FBT2pCLFFBQVEsR0FBR0YsT0FBWCxJQUFzQixDQUFDRixLQUFLLENBQUN5TCxPQUFOLENBQWNyTCxRQUFkLENBQTlCLEVBQXVEQSxRQUFRLEVBQS9ELEVBQW1FO0FBQ2pFO0FBQ0E7QUFDQSxRQUFJSixLQUFLLENBQUNrQixNQUFOLENBQWFkLFFBQWIsSUFBeUJKLEtBQUssQ0FBQ21CLFNBQS9CLEdBQTJDLENBQS9DLEVBQWtEO0FBQUU7QUFBVyxLQUhFLENBS2pFOzs7QUFDQSxRQUFJbkIsS0FBSyxDQUFDa0IsTUFBTixDQUFhZCxRQUFiLElBQXlCLENBQTdCLEVBQWdDO0FBQUU7QUFBVyxLQU5vQixDQVFqRTs7O0FBQ0F3UCxhQUFTLEdBQUcsS0FBWjs7QUFDQSxTQUFLMWMsQ0FBQyxHQUFHLENBQUosRUFBTzBZLENBQUMsR0FBR2lFLGVBQWUsQ0FBQzVjLE1BQWhDLEVBQXdDQyxDQUFDLEdBQUcwWSxDQUE1QyxFQUErQzFZLENBQUMsRUFBaEQsRUFBb0Q7QUFDbEQsVUFBSTJjLGVBQWUsQ0FBQzNjLENBQUQsQ0FBZixDQUFtQjhNLEtBQW5CLEVBQTBCSSxRQUExQixFQUFvQ0YsT0FBcEMsRUFBNkMsSUFBN0MsQ0FBSixFQUF3RDtBQUN0RDBQLGlCQUFTLEdBQUcsSUFBWjtBQUNBO0FBQ0Q7QUFDRjs7QUFDRCxRQUFJQSxTQUFKLEVBQWU7QUFBRTtBQUFRO0FBQzFCOztBQUVEL1ksS0FBRyxHQUFHbUosS0FBSyxDQUFDa1EsUUFBTixDQUFlalEsU0FBZixFQUEwQkcsUUFBMUIsRUFBb0NKLEtBQUssQ0FBQ21CLFNBQTFDLEVBQXFELEtBQXJELEVBQTREbkMsSUFBNUQsRUFBTjtBQUNBNkIsS0FBRyxHQUFHaEssR0FBRyxDQUFDNUQsTUFBVjs7QUFFQSxPQUFLMEUsR0FBRyxHQUFHLENBQVgsRUFBY0EsR0FBRyxHQUFHa0osR0FBcEIsRUFBeUJsSixHQUFHLEVBQTVCLEVBQWdDO0FBQzlCNE8sTUFBRSxHQUFHMVAsR0FBRyxDQUFDK0ksVUFBSixDQUFlakksR0FBZixDQUFMOztBQUNBLFFBQUk0TyxFQUFFLEtBQUs7QUFBSztBQUFoQixNQUF5QjtBQUN2QixlQUFPLEtBQVA7QUFDRCxPQUZELE1BRU8sSUFBSUEsRUFBRSxLQUFLO0FBQUs7QUFBaEIsTUFBeUI7QUFDOUJzQixnQkFBUSxHQUFHbFEsR0FBWDtBQUNBO0FBQ0QsT0FITSxNQUdBLElBQUk0TyxFQUFFLEtBQUs7QUFBSztBQUFoQixNQUEwQjtBQUMvQmlCLGFBQUs7QUFDTixPQUZNLE1BRUEsSUFBSWpCLEVBQUUsS0FBSztBQUFLO0FBQWhCLE1BQXlCO0FBQzlCNU8sV0FBRzs7QUFDSCxZQUFJQSxHQUFHLEdBQUdrSixHQUFOLElBQWFoSyxHQUFHLENBQUMrSSxVQUFKLENBQWVqSSxHQUFmLE1BQXdCLElBQXpDLEVBQStDO0FBQzdDNlAsZUFBSztBQUNOO0FBQ0Y7QUFDRjs7QUFFRCxNQUFJSyxRQUFRLEdBQUcsQ0FBWCxJQUFnQmhSLEdBQUcsQ0FBQytJLFVBQUosQ0FBZWlJLFFBQVEsR0FBRyxDQUExQixNQUFpQztBQUFJO0FBQXpELElBQWtFO0FBQUUsYUFBTyxLQUFQO0FBQWUsS0FyRmIsQ0F1RnRFO0FBQ0E7OztBQUNBLE9BQUtsUSxHQUFHLEdBQUdrUSxRQUFRLEdBQUcsQ0FBdEIsRUFBeUJsUSxHQUFHLEdBQUdrSixHQUEvQixFQUFvQ2xKLEdBQUcsRUFBdkMsRUFBMkM7QUFDekM0TyxNQUFFLEdBQUcxUCxHQUFHLENBQUMrSSxVQUFKLENBQWVqSSxHQUFmLENBQUw7O0FBQ0EsUUFBSTRPLEVBQUUsS0FBSyxJQUFYLEVBQWlCO0FBQ2ZpQixXQUFLO0FBQ04sS0FGRCxNQUVPLElBQUlkLE9BQU8sQ0FBQ0gsRUFBRCxDQUFYLEVBQWlCO0FBQ3RCO0FBQ0QsS0FGTSxNQUVBO0FBQ0w7QUFDRDtBQUNGLEdBbEdxRSxDQW9HdEU7QUFDQTs7O0FBQ0FvTSxLQUFHLEdBQUczUyxLQUFLLENBQUNuQixFQUFOLENBQVNzSixPQUFULENBQWlCZCxvQkFBakIsQ0FBc0N4USxHQUF0QyxFQUEyQ2MsR0FBM0MsRUFBZ0RrSixHQUFoRCxDQUFOOztBQUNBLE1BQUksQ0FBQzhSLEdBQUcsQ0FBQ2xMLEVBQVQsRUFBYTtBQUFFLFdBQU8sS0FBUDtBQUFlOztBQUU5QnZFLE1BQUksR0FBR2xELEtBQUssQ0FBQ25CLEVBQU4sQ0FBU2tLLGFBQVQsQ0FBdUI0SixHQUFHLENBQUM5YixHQUEzQixDQUFQOztBQUNBLE1BQUksQ0FBQ21KLEtBQUssQ0FBQ25CLEVBQU4sQ0FBU2dLLFlBQVQsQ0FBc0IzRixJQUF0QixDQUFMLEVBQWtDO0FBQUUsV0FBTyxLQUFQO0FBQWU7O0FBRW5EdkwsS0FBRyxHQUFHZ2IsR0FBRyxDQUFDaGIsR0FBVjtBQUNBNlAsT0FBSyxJQUFJbUwsR0FBRyxDQUFDbkwsS0FBYixDQTdHc0UsQ0ErR3RFOztBQUNBZ0wsWUFBVSxHQUFHN2EsR0FBYjtBQUNBOGEsZUFBYSxHQUFHakwsS0FBaEIsQ0FqSHNFLENBbUh0RTtBQUNBOztBQUNBN0wsT0FBSyxHQUFHaEUsR0FBUjs7QUFDQSxTQUFPQSxHQUFHLEdBQUdrSixHQUFiLEVBQWtCbEosR0FBRyxFQUFyQixFQUF5QjtBQUN2QjRPLE1BQUUsR0FBRzFQLEdBQUcsQ0FBQytJLFVBQUosQ0FBZWpJLEdBQWYsQ0FBTDs7QUFDQSxRQUFJNE8sRUFBRSxLQUFLLElBQVgsRUFBaUI7QUFDZmlCLFdBQUs7QUFDTixLQUZELE1BRU8sSUFBSWQsT0FBTyxDQUFDSCxFQUFELENBQVgsRUFBaUI7QUFDdEI7QUFDRCxLQUZNLE1BRUE7QUFDTDtBQUNEO0FBQ0YsR0EvSHFFLENBaUl0RTtBQUNBOzs7QUFDQW9NLEtBQUcsR0FBRzNTLEtBQUssQ0FBQ25CLEVBQU4sQ0FBU3NKLE9BQVQsQ0FBaUJiLGNBQWpCLENBQWdDelEsR0FBaEMsRUFBcUNjLEdBQXJDLEVBQTBDa0osR0FBMUMsQ0FBTjs7QUFDQSxNQUFJbEosR0FBRyxHQUFHa0osR0FBTixJQUFhbEYsS0FBSyxLQUFLaEUsR0FBdkIsSUFBOEJnYixHQUFHLENBQUNsTCxFQUF0QyxFQUEwQztBQUN4Q21MLFNBQUssR0FBR0QsR0FBRyxDQUFDOWIsR0FBWjtBQUNBYyxPQUFHLEdBQUdnYixHQUFHLENBQUNoYixHQUFWO0FBQ0E2UCxTQUFLLElBQUltTCxHQUFHLENBQUNuTCxLQUFiO0FBQ0QsR0FKRCxNQUlPO0FBQ0xvTCxTQUFLLEdBQUcsRUFBUjtBQUNBamIsT0FBRyxHQUFHNmEsVUFBTjtBQUNBaEwsU0FBSyxHQUFHaUwsYUFBUjtBQUNELEdBNUlxRSxDQThJdEU7OztBQUNBLFNBQU85YSxHQUFHLEdBQUdrSixHQUFiLEVBQWtCO0FBQ2hCMEYsTUFBRSxHQUFHMVAsR0FBRyxDQUFDK0ksVUFBSixDQUFlakksR0FBZixDQUFMOztBQUNBLFFBQUksQ0FBQytPLE9BQU8sQ0FBQ0gsRUFBRCxDQUFaLEVBQWtCO0FBQUU7QUFBUTs7QUFDNUI1TyxPQUFHO0FBQ0o7O0FBRUQsTUFBSUEsR0FBRyxHQUFHa0osR0FBTixJQUFhaEssR0FBRyxDQUFDK0ksVUFBSixDQUFlakksR0FBZixNQUF3QixJQUF6QyxFQUErQztBQUM3QyxRQUFJaWIsS0FBSixFQUFXO0FBQ1Q7QUFDQTtBQUNBQSxXQUFLLEdBQUcsRUFBUjtBQUNBamIsU0FBRyxHQUFHNmEsVUFBTjtBQUNBaEwsV0FBSyxHQUFHaUwsYUFBUjs7QUFDQSxhQUFPOWEsR0FBRyxHQUFHa0osR0FBYixFQUFrQjtBQUNoQjBGLFVBQUUsR0FBRzFQLEdBQUcsQ0FBQytJLFVBQUosQ0FBZWpJLEdBQWYsQ0FBTDs7QUFDQSxZQUFJLENBQUMrTyxPQUFPLENBQUNILEVBQUQsQ0FBWixFQUFrQjtBQUFFO0FBQVE7O0FBQzVCNU8sV0FBRztBQUNKO0FBQ0Y7QUFDRjs7QUFFRCxNQUFJQSxHQUFHLEdBQUdrSixHQUFOLElBQWFoSyxHQUFHLENBQUMrSSxVQUFKLENBQWVqSSxHQUFmLE1BQXdCLElBQXpDLEVBQStDO0FBQzdDO0FBQ0EsV0FBTyxLQUFQO0FBQ0Q7O0FBRUQrYSxPQUFLLEdBQUczTCxrQkFBa0IsQ0FBQ2xRLEdBQUcsQ0FBQ2pCLEtBQUosQ0FBVSxDQUFWLEVBQWFpUyxRQUFiLENBQUQsQ0FBMUI7O0FBQ0EsTUFBSSxDQUFDNkssS0FBTCxFQUFZO0FBQ1Y7QUFDQSxXQUFPLEtBQVA7QUFDRCxHQTdLcUUsQ0ErS3RFOztBQUNBOzs7QUFDQSxNQUFJdlMsTUFBSixFQUFZO0FBQUUsV0FBTyxJQUFQO0FBQWM7O0FBRTVCLE1BQUksT0FBT0gsS0FBSyxDQUFDWixHQUFOLENBQVV5VCxVQUFqQixLQUFnQyxXQUFwQyxFQUFpRDtBQUMvQzdTLFNBQUssQ0FBQ1osR0FBTixDQUFVeVQsVUFBVixHQUF1QixFQUF2QjtBQUNEOztBQUNELE1BQUksT0FBTzdTLEtBQUssQ0FBQ1osR0FBTixDQUFVeVQsVUFBVixDQUFxQkgsS0FBckIsQ0FBUCxLQUF1QyxXQUEzQyxFQUF3RDtBQUN0RDFTLFNBQUssQ0FBQ1osR0FBTixDQUFVeVQsVUFBVixDQUFxQkgsS0FBckIsSUFBOEI7QUFBRUUsV0FBSyxFQUFFQSxLQUFUO0FBQWdCMVAsVUFBSSxFQUFFQTtBQUF0QixLQUE5QjtBQUNEOztBQUVEbEQsT0FBSyxDQUFDcUIsVUFBTixHQUFtQm1PLGFBQW5CO0FBRUF4UCxPQUFLLENBQUMwQixJQUFOLEdBQWF6QixTQUFTLEdBQUd1SCxLQUFaLEdBQW9CLENBQWpDO0FBQ0EsU0FBTyxJQUFQO0FBQ0QsQ0E5TEQsQzs7Ozs7Ozs7Ozs7O0FDUEE7QUFFYTs7QUFFYixJQUFJc0wsS0FBSyxHQUFHbGUsbUJBQU8sQ0FBQyw2REFBRCxDQUFuQjs7QUFDQSxJQUFJOFIsT0FBTyxHQUFHOVIsbUJBQU8sQ0FBQywyRUFBRCxDQUFQLENBQTJCOFIsT0FBekM7O0FBR0EsU0FBU3FNLFVBQVQsQ0FBb0JoUyxHQUFwQixFQUF5QmxDLEVBQXpCLEVBQTZCTyxHQUE3QixFQUFrQ0YsTUFBbEMsRUFBMEM7QUFDeEMsTUFBSXFILEVBQUosRUFBUXlNLENBQVIsRUFBV3JYLEtBQVgsRUFBa0JoRSxHQUFsQixFQUF1QjNFLEdBQXZCLEVBQTRCb2UsTUFBNUIsRUFBb0NoQyxNQUFwQyxFQUE0QzZELFlBQTVDO0FBRUEsT0FBS2xTLEdBQUwsR0FBV0EsR0FBWCxDQUh3QyxDQUt4Qzs7QUFDQSxPQUFLbEMsRUFBTCxHQUFjQSxFQUFkO0FBRUEsT0FBS08sR0FBTCxHQUFXQSxHQUFYLENBUndDLENBVXhDO0FBQ0E7QUFDQTs7QUFFQSxPQUFLRixNQUFMLEdBQWNBLE1BQWQ7QUFFQSxPQUFLeUIsTUFBTCxHQUFjLEVBQWQsQ0FoQndDLENBZ0JyQjs7QUFDbkIsT0FBS0csTUFBTCxHQUFjLEVBQWQsQ0FqQndDLENBaUJyQjs7QUFDbkIsT0FBS0YsTUFBTCxHQUFjLEVBQWQsQ0FsQndDLENBa0JyQjs7QUFDbkIsT0FBS00sTUFBTCxHQUFjLEVBQWQsQ0FuQndDLENBbUJyQjtBQUVuQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFDQSxPQUFLOE8sT0FBTCxHQUFlLEVBQWYsQ0EvQndDLENBaUN4Qzs7QUFDQSxPQUFLN08sU0FBTCxHQUFrQixDQUFsQixDQWxDd0MsQ0FrQ25CO0FBQ0E7O0FBQ3JCLE9BQUtPLElBQUwsR0FBa0IsQ0FBbEIsQ0FwQ3dDLENBb0NuQjs7QUFDckIsT0FBS0osT0FBTCxHQUFrQixDQUFsQixDQXJDd0MsQ0FxQ25COztBQUNyQixPQUFLa0ssS0FBTCxHQUFrQixLQUFsQixDQXRDd0MsQ0FzQ2Q7O0FBQzFCLE9BQUswSCxRQUFMLEdBQWtCLENBQUMsQ0FBbkIsQ0F2Q3dDLENBdUNsQjs7QUFDdEIsT0FBS2pCLFVBQUwsR0FBa0IsQ0FBQyxDQUFuQixDQXhDd0MsQ0F3Q2xCO0FBRXRCO0FBQ0E7O0FBQ0EsT0FBSzVRLFVBQUwsR0FBa0IsTUFBbEI7QUFFQSxPQUFLa0csS0FBTCxHQUFhLENBQWIsQ0E5Q3dDLENBZ0R4Qzs7QUFDQSxPQUFLbEssTUFBTCxHQUFjLEVBQWQsQ0FqRHdDLENBbUR4QztBQUNBOztBQUNBMlYsR0FBQyxHQUFHLEtBQUtqUyxHQUFUO0FBQ0FrUyxjQUFZLEdBQUcsS0FBZjs7QUFFQSxPQUFLdFgsS0FBSyxHQUFHaEUsR0FBRyxHQUFHeVosTUFBTSxHQUFHaEMsTUFBTSxHQUFHLENBQWhDLEVBQW1DcGMsR0FBRyxHQUFHZ2dCLENBQUMsQ0FBQy9mLE1BQWhELEVBQXdEMEUsR0FBRyxHQUFHM0UsR0FBOUQsRUFBbUUyRSxHQUFHLEVBQXRFLEVBQTBFO0FBQ3hFNE8sTUFBRSxHQUFHeU0sQ0FBQyxDQUFDcFQsVUFBRixDQUFhakksR0FBYixDQUFMOztBQUVBLFFBQUksQ0FBQ3NiLFlBQUwsRUFBbUI7QUFDakIsVUFBSXZNLE9BQU8sQ0FBQ0gsRUFBRCxDQUFYLEVBQWlCO0FBQ2Y2SyxjQUFNOztBQUVOLFlBQUk3SyxFQUFFLEtBQUssSUFBWCxFQUFpQjtBQUNmNkksZ0JBQU0sSUFBSSxJQUFJQSxNQUFNLEdBQUcsQ0FBdkI7QUFDRCxTQUZELE1BRU87QUFDTEEsZ0JBQU07QUFDUDs7QUFDRDtBQUNELE9BVEQsTUFTTztBQUNMNkQsb0JBQVksR0FBRyxJQUFmO0FBQ0Q7QUFDRjs7QUFFRCxRQUFJMU0sRUFBRSxLQUFLLElBQVAsSUFBZTVPLEdBQUcsS0FBSzNFLEdBQUcsR0FBRyxDQUFqQyxFQUFvQztBQUNsQyxVQUFJdVQsRUFBRSxLQUFLLElBQVgsRUFBaUI7QUFBRTVPLFdBQUc7QUFBSzs7QUFDM0IsV0FBS2dKLE1BQUwsQ0FBWXJNLElBQVosQ0FBaUJxSCxLQUFqQjtBQUNBLFdBQUttRixNQUFMLENBQVl4TSxJQUFaLENBQWlCcUQsR0FBakI7QUFDQSxXQUFLaUosTUFBTCxDQUFZdE0sSUFBWixDQUFpQjhjLE1BQWpCO0FBQ0EsV0FBS2xRLE1BQUwsQ0FBWTVNLElBQVosQ0FBaUI4YSxNQUFqQjtBQUNBLFdBQUtZLE9BQUwsQ0FBYTFiLElBQWIsQ0FBa0IsQ0FBbEI7QUFFQTJlLGtCQUFZLEdBQUcsS0FBZjtBQUNBN0IsWUFBTSxHQUFHLENBQVQ7QUFDQWhDLFlBQU0sR0FBRyxDQUFUO0FBQ0F6VCxXQUFLLEdBQUdoRSxHQUFHLEdBQUcsQ0FBZDtBQUNEO0FBQ0YsR0F2RnVDLENBeUZ4Qzs7O0FBQ0EsT0FBS2dKLE1BQUwsQ0FBWXJNLElBQVosQ0FBaUIwZSxDQUFDLENBQUMvZixNQUFuQjtBQUNBLE9BQUs2TixNQUFMLENBQVl4TSxJQUFaLENBQWlCMGUsQ0FBQyxDQUFDL2YsTUFBbkI7QUFDQSxPQUFLMk4sTUFBTCxDQUFZdE0sSUFBWixDQUFpQixDQUFqQjtBQUNBLE9BQUs0TSxNQUFMLENBQVk1TSxJQUFaLENBQWlCLENBQWpCO0FBQ0EsT0FBSzBiLE9BQUwsQ0FBYTFiLElBQWIsQ0FBa0IsQ0FBbEI7QUFFQSxPQUFLZ04sT0FBTCxHQUFlLEtBQUtYLE1BQUwsQ0FBWTFOLE1BQVosR0FBcUIsQ0FBcEMsQ0FoR3dDLENBZ0dEO0FBQ3hDLEMsQ0FFRDtBQUNBOzs7QUFDQThmLFVBQVUsQ0FBQzNkLFNBQVgsQ0FBcUJkLElBQXJCLEdBQTRCLFVBQVV3WixJQUFWLEVBQWdCMUwsR0FBaEIsRUFBcUIvQyxPQUFyQixFQUE4QjtBQUN4RCxNQUFJa0IsS0FBSyxHQUFHLElBQUl1UyxLQUFKLENBQVVoRixJQUFWLEVBQWdCMUwsR0FBaEIsRUFBcUIvQyxPQUFyQixDQUFaO0FBQ0FrQixPQUFLLENBQUNnQixLQUFOLEdBQWMsSUFBZDtBQUVBLE1BQUlsQyxPQUFPLEdBQUcsQ0FBZCxFQUFpQixLQUFLa0ksS0FBTCxHQUp1QyxDQUl6Qjs7QUFDL0JoSCxPQUFLLENBQUNnSCxLQUFOLEdBQWMsS0FBS0EsS0FBbkI7QUFDQSxNQUFJbEksT0FBTyxHQUFHLENBQWQsRUFBaUIsS0FBS2tJLEtBQUwsR0FOdUMsQ0FNekI7O0FBRS9CLE9BQUtySSxNQUFMLENBQVk1SyxJQUFaLENBQWlCaU0sS0FBakI7QUFDQSxTQUFPQSxLQUFQO0FBQ0QsQ0FWRDs7QUFZQXdTLFVBQVUsQ0FBQzNkLFNBQVgsQ0FBcUJxVyxPQUFyQixHQUErQixTQUFTQSxPQUFULENBQWlCL0osSUFBakIsRUFBdUI7QUFDcEQsU0FBTyxLQUFLZixNQUFMLENBQVllLElBQVosSUFBb0IsS0FBS2QsTUFBTCxDQUFZYyxJQUFaLENBQXBCLElBQXlDLEtBQUtaLE1BQUwsQ0FBWVksSUFBWixDQUFoRDtBQUNELENBRkQ7O0FBSUFxUixVQUFVLENBQUMzZCxTQUFYLENBQXFCbVcsY0FBckIsR0FBc0MsU0FBU0EsY0FBVCxDQUF3QnpWLElBQXhCLEVBQThCO0FBQ2xFLE9BQUssSUFBSStLLEdBQUcsR0FBRyxLQUFLUyxPQUFwQixFQUE2QnhMLElBQUksR0FBRytLLEdBQXBDLEVBQXlDL0ssSUFBSSxFQUE3QyxFQUFpRDtBQUMvQyxRQUFJLEtBQUs2SyxNQUFMLENBQVk3SyxJQUFaLElBQW9CLEtBQUs4SyxNQUFMLENBQVk5SyxJQUFaLENBQXBCLEdBQXdDLEtBQUtnTCxNQUFMLENBQVloTCxJQUFaLENBQTVDLEVBQStEO0FBQzdEO0FBQ0Q7QUFDRjs7QUFDRCxTQUFPQSxJQUFQO0FBQ0QsQ0FQRCxDLENBU0E7OztBQUNBaWQsVUFBVSxDQUFDM2QsU0FBWCxDQUFxQmdNLFVBQXJCLEdBQWtDLFNBQVNBLFVBQVQsQ0FBb0J6SixHQUFwQixFQUF5QjtBQUN6RCxNQUFJNE8sRUFBSjs7QUFFQSxPQUFLLElBQUkxRixHQUFHLEdBQUcsS0FBS0UsR0FBTCxDQUFTOU4sTUFBeEIsRUFBZ0MwRSxHQUFHLEdBQUdrSixHQUF0QyxFQUEyQ2xKLEdBQUcsRUFBOUMsRUFBa0Q7QUFDaEQ0TyxNQUFFLEdBQUcsS0FBS3hGLEdBQUwsQ0FBU25CLFVBQVQsQ0FBb0JqSSxHQUFwQixDQUFMOztBQUNBLFFBQUksQ0FBQytPLE9BQU8sQ0FBQ0gsRUFBRCxDQUFaLEVBQWtCO0FBQUU7QUFBUTtBQUM3Qjs7QUFDRCxTQUFPNU8sR0FBUDtBQUNELENBUkQsQyxDQVVBOzs7QUFDQW9iLFVBQVUsQ0FBQzNkLFNBQVgsQ0FBcUJvYixjQUFyQixHQUFzQyxTQUFTQSxjQUFULENBQXdCN1ksR0FBeEIsRUFBNkJ5YSxHQUE3QixFQUFrQztBQUN0RSxNQUFJemEsR0FBRyxJQUFJeWEsR0FBWCxFQUFnQjtBQUFFLFdBQU96YSxHQUFQO0FBQWE7O0FBRS9CLFNBQU9BLEdBQUcsR0FBR3lhLEdBQWIsRUFBa0I7QUFDaEIsUUFBSSxDQUFDMUwsT0FBTyxDQUFDLEtBQUszRixHQUFMLENBQVNuQixVQUFULENBQW9CLEVBQUVqSSxHQUF0QixDQUFELENBQVosRUFBMEM7QUFBRSxhQUFPQSxHQUFHLEdBQUcsQ0FBYjtBQUFpQjtBQUM5RDs7QUFDRCxTQUFPQSxHQUFQO0FBQ0QsQ0FQRCxDLENBU0E7OztBQUNBb2IsVUFBVSxDQUFDM2QsU0FBWCxDQUFxQmliLFNBQXJCLEdBQWlDLFNBQVNBLFNBQVQsQ0FBbUIxWSxHQUFuQixFQUF3QmtPLElBQXhCLEVBQThCO0FBQzdELE9BQUssSUFBSWhGLEdBQUcsR0FBRyxLQUFLRSxHQUFMLENBQVM5TixNQUF4QixFQUFnQzBFLEdBQUcsR0FBR2tKLEdBQXRDLEVBQTJDbEosR0FBRyxFQUE5QyxFQUFrRDtBQUNoRCxRQUFJLEtBQUtvSixHQUFMLENBQVNuQixVQUFULENBQW9CakksR0FBcEIsTUFBNkJrTyxJQUFqQyxFQUF1QztBQUFFO0FBQVE7QUFDbEQ7O0FBQ0QsU0FBT2xPLEdBQVA7QUFDRCxDQUxELEMsQ0FPQTs7O0FBQ0FvYixVQUFVLENBQUMzZCxTQUFYLENBQXFCcWIsYUFBckIsR0FBcUMsU0FBU0EsYUFBVCxDQUF1QjlZLEdBQXZCLEVBQTRCa08sSUFBNUIsRUFBa0N1TSxHQUFsQyxFQUF1QztBQUMxRSxNQUFJemEsR0FBRyxJQUFJeWEsR0FBWCxFQUFnQjtBQUFFLFdBQU96YSxHQUFQO0FBQWE7O0FBRS9CLFNBQU9BLEdBQUcsR0FBR3lhLEdBQWIsRUFBa0I7QUFDaEIsUUFBSXZNLElBQUksS0FBSyxLQUFLOUUsR0FBTCxDQUFTbkIsVUFBVCxDQUFvQixFQUFFakksR0FBdEIsQ0FBYixFQUF5QztBQUFFLGFBQU9BLEdBQUcsR0FBRyxDQUFiO0FBQWlCO0FBQzdEOztBQUNELFNBQU9BLEdBQVA7QUFDRCxDQVBELEMsQ0FTQTs7O0FBQ0FvYixVQUFVLENBQUMzZCxTQUFYLENBQXFCOGEsUUFBckIsR0FBZ0MsU0FBU0EsUUFBVCxDQUFrQmlELEtBQWxCLEVBQXlCdlgsR0FBekIsRUFBOEJ3VixNQUE5QixFQUFzQ2dDLFVBQXRDLEVBQWtEO0FBQ2hGLE1BQUlsZ0IsQ0FBSjtBQUFBLE1BQU9tZ0IsVUFBUDtBQUFBLE1BQW1COU0sRUFBbkI7QUFBQSxNQUF1QitNLEtBQXZCO0FBQUEsTUFBOEJyRCxJQUE5QjtBQUFBLE1BQW9Dc0QsS0FBcEM7QUFBQSxNQUEyQ0MsU0FBM0M7QUFBQSxNQUNJOVIsSUFBSSxHQUFHeVIsS0FEWDs7QUFHQSxNQUFJQSxLQUFLLElBQUl2WCxHQUFiLEVBQWtCO0FBQ2hCLFdBQU8sRUFBUDtBQUNEOztBQUVEMlgsT0FBSyxHQUFHLElBQUluZ0IsS0FBSixDQUFVd0ksR0FBRyxHQUFHdVgsS0FBaEIsQ0FBUjs7QUFFQSxPQUFLamdCLENBQUMsR0FBRyxDQUFULEVBQVl3TyxJQUFJLEdBQUc5RixHQUFuQixFQUF3QjhGLElBQUksSUFBSXhPLENBQUMsRUFBakMsRUFBcUM7QUFDbkNtZ0IsY0FBVSxHQUFHLENBQWI7QUFDQUcsYUFBUyxHQUFHRixLQUFLLEdBQUcsS0FBSzNTLE1BQUwsQ0FBWWUsSUFBWixDQUFwQjs7QUFFQSxRQUFJQSxJQUFJLEdBQUcsQ0FBUCxHQUFXOUYsR0FBWCxJQUFrQndYLFVBQXRCLEVBQWtDO0FBQ2hDO0FBQ0FuRCxVQUFJLEdBQUcsS0FBS25QLE1BQUwsQ0FBWVksSUFBWixJQUFvQixDQUEzQjtBQUNELEtBSEQsTUFHTztBQUNMdU8sVUFBSSxHQUFHLEtBQUtuUCxNQUFMLENBQVlZLElBQVosQ0FBUDtBQUNEOztBQUVELFdBQU80UixLQUFLLEdBQUdyRCxJQUFSLElBQWdCb0QsVUFBVSxHQUFHakMsTUFBcEMsRUFBNEM7QUFDMUM3SyxRQUFFLEdBQUcsS0FBS3hGLEdBQUwsQ0FBU25CLFVBQVQsQ0FBb0IwVCxLQUFwQixDQUFMOztBQUVBLFVBQUk1TSxPQUFPLENBQUNILEVBQUQsQ0FBWCxFQUFpQjtBQUNmLFlBQUlBLEVBQUUsS0FBSyxJQUFYLEVBQWlCO0FBQ2Y4TSxvQkFBVSxJQUFJLElBQUksQ0FBQ0EsVUFBVSxHQUFHLEtBQUtyRCxPQUFMLENBQWF0TyxJQUFiLENBQWQsSUFBb0MsQ0FBdEQ7QUFDRCxTQUZELE1BRU87QUFDTDJSLG9CQUFVO0FBQ1g7QUFDRixPQU5ELE1BTU8sSUFBSUMsS0FBSyxHQUFHRSxTQUFSLEdBQW9CLEtBQUs1UyxNQUFMLENBQVljLElBQVosQ0FBeEIsRUFBMkM7QUFDaEQ7QUFDQTJSLGtCQUFVO0FBQ1gsT0FITSxNQUdBO0FBQ0w7QUFDRDs7QUFFREMsV0FBSztBQUNOOztBQUVELFFBQUlELFVBQVUsR0FBR2pDLE1BQWpCLEVBQXlCO0FBQ3ZCO0FBQ0E7QUFDQW1DLFdBQUssQ0FBQ3JnQixDQUFELENBQUwsR0FBVyxJQUFJRSxLQUFKLENBQVVpZ0IsVUFBVSxHQUFHakMsTUFBYixHQUFzQixDQUFoQyxFQUFtQ3RYLElBQW5DLENBQXdDLEdBQXhDLElBQStDLEtBQUtpSCxHQUFMLENBQVNuTCxLQUFULENBQWUwZCxLQUFmLEVBQXNCckQsSUFBdEIsQ0FBMUQ7QUFDRCxLQUpELE1BSU87QUFDTHNELFdBQUssQ0FBQ3JnQixDQUFELENBQUwsR0FBVyxLQUFLNk4sR0FBTCxDQUFTbkwsS0FBVCxDQUFlMGQsS0FBZixFQUFzQnJELElBQXRCLENBQVg7QUFDRDtBQUNGOztBQUVELFNBQU9zRCxLQUFLLENBQUN6WixJQUFOLENBQVcsRUFBWCxDQUFQO0FBQ0QsQ0FsREQsQyxDQW9EQTs7O0FBQ0FpWixVQUFVLENBQUMzZCxTQUFYLENBQXFCMGQsS0FBckIsR0FBNkJBLEtBQTdCO0FBR0F6ZixNQUFNLENBQUNDLE9BQVAsR0FBaUJ5ZixVQUFqQixDOzs7Ozs7Ozs7Ozs7QUN0T0E7QUFFYTs7QUFFYixJQUFJck0sT0FBTyxHQUFHOVIsbUJBQU8sQ0FBQywyRUFBRCxDQUFQLENBQTJCOFIsT0FBekM7O0FBR0EsU0FBUytNLE9BQVQsQ0FBaUJ6VCxLQUFqQixFQUF3QjBCLElBQXhCLEVBQThCO0FBQzVCLE1BQUkvSixHQUFHLEdBQUdxSSxLQUFLLENBQUNXLE1BQU4sQ0FBYWUsSUFBYixJQUFxQjFCLEtBQUssQ0FBQ21CLFNBQXJDO0FBQUEsTUFDSU4sR0FBRyxHQUFHYixLQUFLLENBQUNjLE1BQU4sQ0FBYVksSUFBYixDQURWO0FBR0EsU0FBTzFCLEtBQUssQ0FBQ2UsR0FBTixDQUFVb1IsTUFBVixDQUFpQnhhLEdBQWpCLEVBQXNCa0osR0FBRyxHQUFHbEosR0FBNUIsQ0FBUDtBQUNEOztBQUVELFNBQVMrYixZQUFULENBQXNCN2MsR0FBdEIsRUFBMkI7QUFDekIsTUFBSXdHLE1BQU0sR0FBRyxFQUFiO0FBQUEsTUFDSTFGLEdBQUcsR0FBRyxDQURWO0FBQUEsTUFFSWtKLEdBQUcsR0FBR2hLLEdBQUcsQ0FBQzVELE1BRmQ7QUFBQSxNQUdJc1QsRUFISjtBQUFBLE1BSUlvTixPQUFPLEdBQUcsQ0FKZDtBQUFBLE1BS0lDLE9BQU8sR0FBRyxDQUxkO0FBQUEsTUFNSUMsVUFBVSxHQUFHLEtBTmpCO0FBQUEsTUFPSUMsWUFBWSxHQUFHLENBUG5CO0FBU0F2TixJQUFFLEdBQUkxUCxHQUFHLENBQUMrSSxVQUFKLENBQWVqSSxHQUFmLENBQU47O0FBRUEsU0FBT0EsR0FBRyxHQUFHa0osR0FBYixFQUFrQjtBQUNoQixRQUFJMEYsRUFBRSxLQUFLO0FBQUk7QUFBZixNQUF3QjtBQUN0QixZQUFJc04sVUFBSixFQUFnQjtBQUNkO0FBQ0E7QUFDQUEsb0JBQVUsR0FBRyxLQUFiO0FBQ0FDLHNCQUFZLEdBQUduYyxHQUFmO0FBQ0QsU0FMRCxNQUtPLElBQUlnYyxPQUFPLEdBQUcsQ0FBVixLQUFnQixDQUFwQixFQUF1QjtBQUM1QkUsb0JBQVUsR0FBRyxJQUFiO0FBQ0FDLHNCQUFZLEdBQUduYyxHQUFmO0FBQ0Q7QUFDRixPQVZELE1BVU8sSUFBSTRPLEVBQUUsS0FBSztBQUFJO0FBQVgsT0FBdUJvTixPQUFPLEdBQUcsQ0FBVixLQUFnQixDQUF2QyxJQUE2QyxDQUFDRSxVQUFsRCxFQUE4RDtBQUNuRXhXLFlBQU0sQ0FBQy9JLElBQVAsQ0FBWXVDLEdBQUcsQ0FBQ2tkLFNBQUosQ0FBY0gsT0FBZCxFQUF1QmpjLEdBQXZCLENBQVo7QUFDQWljLGFBQU8sR0FBR2pjLEdBQUcsR0FBRyxDQUFoQjtBQUNEOztBQUVELFFBQUk0TyxFQUFFLEtBQUs7QUFBSTtBQUFmLE1BQXdCO0FBQ3RCb04sZUFBTztBQUNSLE9BRkQsTUFFTztBQUNMQSxhQUFPLEdBQUcsQ0FBVjtBQUNEOztBQUVEaGMsT0FBRyxHQXRCYSxDQXdCaEI7QUFDQTs7QUFDQSxRQUFJQSxHQUFHLEtBQUtrSixHQUFSLElBQWVnVCxVQUFuQixFQUErQjtBQUM3QkEsZ0JBQVUsR0FBRyxLQUFiO0FBQ0FsYyxTQUFHLEdBQUdtYyxZQUFZLEdBQUcsQ0FBckI7QUFDRDs7QUFFRHZOLE1BQUUsR0FBRzFQLEdBQUcsQ0FBQytJLFVBQUosQ0FBZWpJLEdBQWYsQ0FBTDtBQUNEOztBQUVEMEYsUUFBTSxDQUFDL0ksSUFBUCxDQUFZdUMsR0FBRyxDQUFDa2QsU0FBSixDQUFjSCxPQUFkLENBQVo7QUFFQSxTQUFPdlcsTUFBUDtBQUNEOztBQUdEaEssTUFBTSxDQUFDQyxPQUFQLEdBQWlCLFNBQVMwZ0IsS0FBVCxDQUFlaFUsS0FBZixFQUFzQkMsU0FBdEIsRUFBaUNDLE9BQWpDLEVBQTBDQyxNQUExQyxFQUFrRDtBQUNqRSxNQUFJb0csRUFBSixFQUFRdUssUUFBUixFQUFrQm5aLEdBQWxCLEVBQXVCekUsQ0FBdkIsRUFBMEJrTixRQUExQixFQUFvQzZULE9BQXBDLEVBQTZDQyxXQUE3QyxFQUEwRDNULEtBQTFELEVBQ0k0VCxNQURKLEVBQ1lDLENBRFosRUFDZUMsVUFEZixFQUMyQkMsVUFEM0IsQ0FEaUUsQ0FJakU7O0FBQ0EsTUFBSXJVLFNBQVMsR0FBRyxDQUFaLEdBQWdCQyxPQUFwQixFQUE2QjtBQUFFLFdBQU8sS0FBUDtBQUFlOztBQUU5Q0UsVUFBUSxHQUFHSCxTQUFTLEdBQUcsQ0FBdkI7O0FBRUEsTUFBSUQsS0FBSyxDQUFDa0IsTUFBTixDQUFhZCxRQUFiLElBQXlCSixLQUFLLENBQUNtQixTQUFuQyxFQUE4QztBQUFFLFdBQU8sS0FBUDtBQUFlLEdBVEUsQ0FXakU7OztBQUNBLE1BQUluQixLQUFLLENBQUNrQixNQUFOLENBQWFkLFFBQWIsSUFBeUJKLEtBQUssQ0FBQ21CLFNBQS9CLElBQTRDLENBQWhELEVBQW1EO0FBQUUsV0FBTyxLQUFQO0FBQWUsR0FaSCxDQWNqRTtBQUNBO0FBQ0E7OztBQUVBeEosS0FBRyxHQUFHcUksS0FBSyxDQUFDVyxNQUFOLENBQWFQLFFBQWIsSUFBeUJKLEtBQUssQ0FBQ1ksTUFBTixDQUFhUixRQUFiLENBQS9COztBQUNBLE1BQUl6SSxHQUFHLElBQUlxSSxLQUFLLENBQUNjLE1BQU4sQ0FBYVYsUUFBYixDQUFYLEVBQW1DO0FBQUUsV0FBTyxLQUFQO0FBQWU7O0FBRXBEbUcsSUFBRSxHQUFHdkcsS0FBSyxDQUFDZSxHQUFOLENBQVVuQixVQUFWLENBQXFCakksR0FBRyxFQUF4QixDQUFMOztBQUNBLE1BQUk0TyxFQUFFLEtBQUs7QUFBSTtBQUFYLEtBQXNCQSxFQUFFLEtBQUs7QUFBSTtBQUFqQyxLQUE0Q0EsRUFBRSxLQUFLO0FBQUk7QUFBM0QsSUFBb0U7QUFBRSxhQUFPLEtBQVA7QUFBZTs7QUFFckYsU0FBTzVPLEdBQUcsR0FBR3FJLEtBQUssQ0FBQ2MsTUFBTixDQUFhVixRQUFiLENBQWIsRUFBcUM7QUFDbkNtRyxNQUFFLEdBQUd2RyxLQUFLLENBQUNlLEdBQU4sQ0FBVW5CLFVBQVYsQ0FBcUJqSSxHQUFyQixDQUFMOztBQUVBLFFBQUk0TyxFQUFFLEtBQUs7QUFBSTtBQUFYLE9BQXNCQSxFQUFFLEtBQUs7QUFBSTtBQUFqQyxPQUE0Q0EsRUFBRSxLQUFLO0FBQUk7QUFBdkQsT0FBa0UsQ0FBQ0csT0FBTyxDQUFDSCxFQUFELENBQTlFLEVBQW9GO0FBQUUsYUFBTyxLQUFQO0FBQWU7O0FBRXJHNU8sT0FBRztBQUNKOztBQUVEbVosVUFBUSxHQUFHMkMsT0FBTyxDQUFDelQsS0FBRCxFQUFRQyxTQUFTLEdBQUcsQ0FBcEIsQ0FBbEI7QUFFQWdVLFNBQU8sR0FBR25ELFFBQVEsQ0FBQy9YLEtBQVQsQ0FBZSxHQUFmLENBQVY7QUFDQW9iLFFBQU0sR0FBRyxFQUFUOztBQUNBLE9BQUtqaEIsQ0FBQyxHQUFHLENBQVQsRUFBWUEsQ0FBQyxHQUFHK2dCLE9BQU8sQ0FBQ2hoQixNQUF4QixFQUFnQ0MsQ0FBQyxFQUFqQyxFQUFxQztBQUNuQ2toQixLQUFDLEdBQUdILE9BQU8sQ0FBQy9nQixDQUFELENBQVAsQ0FBVzhMLElBQVgsRUFBSjs7QUFDQSxRQUFJLENBQUNvVixDQUFMLEVBQVE7QUFDTjtBQUNBO0FBQ0EsVUFBSWxoQixDQUFDLEtBQUssQ0FBTixJQUFXQSxDQUFDLEtBQUsrZ0IsT0FBTyxDQUFDaGhCLE1BQVIsR0FBaUIsQ0FBdEMsRUFBeUM7QUFDdkM7QUFDRCxPQUZELE1BRU87QUFDTCxlQUFPLEtBQVA7QUFDRDtBQUNGOztBQUVELFFBQUksQ0FBQyxXQUFXOEMsSUFBWCxDQUFnQnFlLENBQWhCLENBQUwsRUFBeUI7QUFBRSxhQUFPLEtBQVA7QUFBZTs7QUFDMUMsUUFBSUEsQ0FBQyxDQUFDeFUsVUFBRixDQUFhd1UsQ0FBQyxDQUFDbmhCLE1BQUYsR0FBVyxDQUF4QixNQUErQjtBQUFJO0FBQXZDLE1BQWdEO0FBQzlDa2hCLGNBQU0sQ0FBQzdmLElBQVAsQ0FBWThmLENBQUMsQ0FBQ3hVLFVBQUYsQ0FBYSxDQUFiLE1BQW9CO0FBQUk7QUFBeEIsVUFBa0MsUUFBbEMsR0FBNkMsT0FBekQ7QUFDRCxPQUZELE1BRU8sSUFBSXdVLENBQUMsQ0FBQ3hVLFVBQUYsQ0FBYSxDQUFiLE1BQW9CO0FBQUk7QUFBNUIsTUFBcUM7QUFDMUN1VSxjQUFNLENBQUM3ZixJQUFQLENBQVksTUFBWjtBQUNELE9BRk0sTUFFQTtBQUNMNmYsWUFBTSxDQUFDN2YsSUFBUCxDQUFZLEVBQVo7QUFDRDtBQUNGOztBQUVEd2MsVUFBUSxHQUFHMkMsT0FBTyxDQUFDelQsS0FBRCxFQUFRQyxTQUFSLENBQVAsQ0FBMEJqQixJQUExQixFQUFYOztBQUNBLE1BQUk4UixRQUFRLENBQUMxVCxPQUFULENBQWlCLEdBQWpCLE1BQTBCLENBQUMsQ0FBL0IsRUFBa0M7QUFBRSxXQUFPLEtBQVA7QUFBZTs7QUFDbkQsTUFBSTRDLEtBQUssQ0FBQ2tCLE1BQU4sQ0FBYWpCLFNBQWIsSUFBMEJELEtBQUssQ0FBQ21CLFNBQWhDLElBQTZDLENBQWpELEVBQW9EO0FBQUUsV0FBTyxLQUFQO0FBQWU7O0FBQ3JFOFMsU0FBTyxHQUFHUCxZQUFZLENBQUM1QyxRQUFRLENBQUNoYSxPQUFULENBQWlCLFVBQWpCLEVBQTZCLEVBQTdCLENBQUQsQ0FBdEIsQ0E3RGlFLENBK0RqRTtBQUNBOztBQUNBb2QsYUFBVyxHQUFHRCxPQUFPLENBQUNoaEIsTUFBdEI7O0FBQ0EsTUFBSWloQixXQUFXLEdBQUdDLE1BQU0sQ0FBQ2xoQixNQUF6QixFQUFpQztBQUFFLFdBQU8sS0FBUDtBQUFlOztBQUVsRCxNQUFJa04sTUFBSixFQUFZO0FBQUUsV0FBTyxJQUFQO0FBQWM7O0FBRTVCSSxPQUFLLEdBQU9QLEtBQUssQ0FBQzFMLElBQU4sQ0FBVyxZQUFYLEVBQXlCLE9BQXpCLEVBQWtDLENBQWxDLENBQVo7QUFDQWlNLE9BQUssQ0FBQ25GLEdBQU4sR0FBWWlaLFVBQVUsR0FBRyxDQUFFcFUsU0FBRixFQUFhLENBQWIsQ0FBekI7QUFFQU0sT0FBSyxHQUFPUCxLQUFLLENBQUMxTCxJQUFOLENBQVcsWUFBWCxFQUF5QixPQUF6QixFQUFrQyxDQUFsQyxDQUFaO0FBQ0FpTSxPQUFLLENBQUNuRixHQUFOLEdBQVksQ0FBRTZFLFNBQUYsRUFBYUEsU0FBUyxHQUFHLENBQXpCLENBQVo7QUFFQU0sT0FBSyxHQUFPUCxLQUFLLENBQUMxTCxJQUFOLENBQVcsU0FBWCxFQUFzQixJQUF0QixFQUE0QixDQUE1QixDQUFaO0FBQ0FpTSxPQUFLLENBQUNuRixHQUFOLEdBQVksQ0FBRTZFLFNBQUYsRUFBYUEsU0FBUyxHQUFHLENBQXpCLENBQVo7O0FBRUEsT0FBSy9NLENBQUMsR0FBRyxDQUFULEVBQVlBLENBQUMsR0FBRytnQixPQUFPLENBQUNoaEIsTUFBeEIsRUFBZ0NDLENBQUMsRUFBakMsRUFBcUM7QUFDbkNxTixTQUFLLEdBQVlQLEtBQUssQ0FBQzFMLElBQU4sQ0FBVyxTQUFYLEVBQXNCLElBQXRCLEVBQTRCLENBQTVCLENBQWpCO0FBQ0FpTSxTQUFLLENBQUNuRixHQUFOLEdBQWlCLENBQUU2RSxTQUFGLEVBQWFBLFNBQVMsR0FBRyxDQUF6QixDQUFqQjs7QUFDQSxRQUFJa1UsTUFBTSxDQUFDamhCLENBQUQsQ0FBVixFQUFlO0FBQ2JxTixXQUFLLENBQUM0QyxLQUFOLEdBQWUsQ0FBRSxDQUFFLE9BQUYsRUFBVyxnQkFBZ0JnUixNQUFNLENBQUNqaEIsQ0FBRCxDQUFqQyxDQUFGLENBQWY7QUFDRDs7QUFFRHFOLFNBQUssR0FBWVAsS0FBSyxDQUFDMUwsSUFBTixDQUFXLFFBQVgsRUFBcUIsRUFBckIsRUFBeUIsQ0FBekIsQ0FBakI7QUFDQWlNLFNBQUssQ0FBQ3NNLE9BQU4sR0FBaUJvSCxPQUFPLENBQUMvZ0IsQ0FBRCxDQUFQLENBQVc4TCxJQUFYLEVBQWpCO0FBQ0F1QixTQUFLLENBQUNuRixHQUFOLEdBQWlCLENBQUU2RSxTQUFGLEVBQWFBLFNBQVMsR0FBRyxDQUF6QixDQUFqQjtBQUNBTSxTQUFLLENBQUMrTSxRQUFOLEdBQWlCLEVBQWpCO0FBRUEvTSxTQUFLLEdBQVlQLEtBQUssQ0FBQzFMLElBQU4sQ0FBVyxVQUFYLEVBQXVCLElBQXZCLEVBQTZCLENBQUMsQ0FBOUIsQ0FBakI7QUFDRDs7QUFFRGlNLE9BQUssR0FBT1AsS0FBSyxDQUFDMUwsSUFBTixDQUFXLFVBQVgsRUFBdUIsSUFBdkIsRUFBNkIsQ0FBQyxDQUE5QixDQUFaO0FBQ0FpTSxPQUFLLEdBQU9QLEtBQUssQ0FBQzFMLElBQU4sQ0FBVyxhQUFYLEVBQTBCLE9BQTFCLEVBQW1DLENBQUMsQ0FBcEMsQ0FBWjtBQUVBaU0sT0FBSyxHQUFPUCxLQUFLLENBQUMxTCxJQUFOLENBQVcsWUFBWCxFQUF5QixPQUF6QixFQUFrQyxDQUFsQyxDQUFaO0FBQ0FpTSxPQUFLLENBQUNuRixHQUFOLEdBQVlrWixVQUFVLEdBQUcsQ0FBRXJVLFNBQVMsR0FBRyxDQUFkLEVBQWlCLENBQWpCLENBQXpCOztBQUVBLE9BQUtHLFFBQVEsR0FBR0gsU0FBUyxHQUFHLENBQTVCLEVBQStCRyxRQUFRLEdBQUdGLE9BQTFDLEVBQW1ERSxRQUFRLEVBQTNELEVBQStEO0FBQzdELFFBQUlKLEtBQUssQ0FBQ2tCLE1BQU4sQ0FBYWQsUUFBYixJQUF5QkosS0FBSyxDQUFDbUIsU0FBbkMsRUFBOEM7QUFBRTtBQUFROztBQUV4RDJQLFlBQVEsR0FBRzJDLE9BQU8sQ0FBQ3pULEtBQUQsRUFBUUksUUFBUixDQUFQLENBQXlCcEIsSUFBekIsRUFBWDs7QUFDQSxRQUFJOFIsUUFBUSxDQUFDMVQsT0FBVCxDQUFpQixHQUFqQixNQUEwQixDQUFDLENBQS9CLEVBQWtDO0FBQUU7QUFBUTs7QUFDNUMsUUFBSTRDLEtBQUssQ0FBQ2tCLE1BQU4sQ0FBYWQsUUFBYixJQUF5QkosS0FBSyxDQUFDbUIsU0FBL0IsSUFBNEMsQ0FBaEQsRUFBbUQ7QUFBRTtBQUFROztBQUM3RDhTLFdBQU8sR0FBR1AsWUFBWSxDQUFDNUMsUUFBUSxDQUFDaGEsT0FBVCxDQUFpQixVQUFqQixFQUE2QixFQUE3QixDQUFELENBQXRCO0FBRUF5SixTQUFLLEdBQUdQLEtBQUssQ0FBQzFMLElBQU4sQ0FBVyxTQUFYLEVBQXNCLElBQXRCLEVBQTRCLENBQTVCLENBQVI7O0FBQ0EsU0FBS3BCLENBQUMsR0FBRyxDQUFULEVBQVlBLENBQUMsR0FBR2doQixXQUFoQixFQUE2QmhoQixDQUFDLEVBQTlCLEVBQWtDO0FBQ2hDcU4sV0FBSyxHQUFZUCxLQUFLLENBQUMxTCxJQUFOLENBQVcsU0FBWCxFQUFzQixJQUF0QixFQUE0QixDQUE1QixDQUFqQjs7QUFDQSxVQUFJNmYsTUFBTSxDQUFDamhCLENBQUQsQ0FBVixFQUFlO0FBQ2JxTixhQUFLLENBQUM0QyxLQUFOLEdBQWUsQ0FBRSxDQUFFLE9BQUYsRUFBVyxnQkFBZ0JnUixNQUFNLENBQUNqaEIsQ0FBRCxDQUFqQyxDQUFGLENBQWY7QUFDRDs7QUFFRHFOLFdBQUssR0FBWVAsS0FBSyxDQUFDMUwsSUFBTixDQUFXLFFBQVgsRUFBcUIsRUFBckIsRUFBeUIsQ0FBekIsQ0FBakI7QUFDQWlNLFdBQUssQ0FBQ3NNLE9BQU4sR0FBaUJvSCxPQUFPLENBQUMvZ0IsQ0FBRCxDQUFQLEdBQWErZ0IsT0FBTyxDQUFDL2dCLENBQUQsQ0FBUCxDQUFXOEwsSUFBWCxFQUFiLEdBQWlDLEVBQWxEO0FBQ0F1QixXQUFLLENBQUMrTSxRQUFOLEdBQWlCLEVBQWpCO0FBRUEvTSxXQUFLLEdBQVlQLEtBQUssQ0FBQzFMLElBQU4sQ0FBVyxVQUFYLEVBQXVCLElBQXZCLEVBQTZCLENBQUMsQ0FBOUIsQ0FBakI7QUFDRDs7QUFDRGlNLFNBQUssR0FBR1AsS0FBSyxDQUFDMUwsSUFBTixDQUFXLFVBQVgsRUFBdUIsSUFBdkIsRUFBNkIsQ0FBQyxDQUE5QixDQUFSO0FBQ0Q7O0FBQ0RpTSxPQUFLLEdBQUdQLEtBQUssQ0FBQzFMLElBQU4sQ0FBVyxhQUFYLEVBQTBCLE9BQTFCLEVBQW1DLENBQUMsQ0FBcEMsQ0FBUjtBQUNBaU0sT0FBSyxHQUFHUCxLQUFLLENBQUMxTCxJQUFOLENBQVcsYUFBWCxFQUEwQixPQUExQixFQUFtQyxDQUFDLENBQXBDLENBQVI7QUFFQStmLFlBQVUsQ0FBQyxDQUFELENBQVYsR0FBZ0JDLFVBQVUsQ0FBQyxDQUFELENBQVYsR0FBZ0JsVSxRQUFoQztBQUNBSixPQUFLLENBQUMwQixJQUFOLEdBQWF0QixRQUFiO0FBQ0EsU0FBTyxJQUFQO0FBQ0QsQ0FqSUQsQzs7Ozs7Ozs7Ozs7O0FDbEVhOztBQUdiL00sTUFBTSxDQUFDQyxPQUFQLEdBQWlCLFNBQVNpTyxLQUFULENBQWV2QixLQUFmLEVBQXNCO0FBQ3JDLE1BQUlPLEtBQUo7O0FBRUEsTUFBSVAsS0FBSyxDQUFDZ0wsVUFBVixFQUFzQjtBQUNwQnpLLFNBQUssR0FBWSxJQUFJUCxLQUFLLENBQUM4UyxLQUFWLENBQWdCLFFBQWhCLEVBQTBCLEVBQTFCLEVBQThCLENBQTlCLENBQWpCO0FBQ0F2UyxTQUFLLENBQUNzTSxPQUFOLEdBQWlCN00sS0FBSyxDQUFDZSxHQUF2QjtBQUNBUixTQUFLLENBQUNuRixHQUFOLEdBQWlCLENBQUUsQ0FBRixFQUFLLENBQUwsQ0FBakI7QUFDQW1GLFNBQUssQ0FBQytNLFFBQU4sR0FBaUIsRUFBakI7QUFDQXROLFNBQUssQ0FBQ2QsTUFBTixDQUFhNUssSUFBYixDQUFrQmlNLEtBQWxCO0FBQ0QsR0FORCxNQU1PO0FBQ0xQLFNBQUssQ0FBQ25CLEVBQU4sQ0FBUzBDLEtBQVQsQ0FBZTBILEtBQWYsQ0FBcUJqSixLQUFLLENBQUNlLEdBQTNCLEVBQWdDZixLQUFLLENBQUNuQixFQUF0QyxFQUEwQ21CLEtBQUssQ0FBQ1osR0FBaEQsRUFBcURZLEtBQUssQ0FBQ2QsTUFBM0Q7QUFDRDtBQUNGLENBWkQsQzs7Ozs7Ozs7Ozs7O0FDSGE7O0FBRWI3TCxNQUFNLENBQUNDLE9BQVAsR0FBaUIsU0FBUzBVLE1BQVQsQ0FBZ0JoSSxLQUFoQixFQUF1QjtBQUN0QyxNQUFJZCxNQUFNLEdBQUdjLEtBQUssQ0FBQ2QsTUFBbkI7QUFBQSxNQUEyQnFWLEdBQTNCO0FBQUEsTUFBZ0NyaEIsQ0FBaEM7QUFBQSxNQUFtQzBZLENBQW5DLENBRHNDLENBR3RDOztBQUNBLE9BQUsxWSxDQUFDLEdBQUcsQ0FBSixFQUFPMFksQ0FBQyxHQUFHMU0sTUFBTSxDQUFDak0sTUFBdkIsRUFBK0JDLENBQUMsR0FBRzBZLENBQW5DLEVBQXNDMVksQ0FBQyxFQUF2QyxFQUEyQztBQUN6Q3FoQixPQUFHLEdBQUdyVixNQUFNLENBQUNoTSxDQUFELENBQVo7O0FBQ0EsUUFBSXFoQixHQUFHLENBQUN6RyxJQUFKLEtBQWEsUUFBakIsRUFBMkI7QUFDekI5TixXQUFLLENBQUNuQixFQUFOLENBQVNtSixNQUFULENBQWdCaUIsS0FBaEIsQ0FBc0JzTCxHQUFHLENBQUMxSCxPQUExQixFQUFtQzdNLEtBQUssQ0FBQ25CLEVBQXpDLEVBQTZDbUIsS0FBSyxDQUFDWixHQUFuRCxFQUF3RG1WLEdBQUcsQ0FBQ2pILFFBQTVEO0FBQ0Q7QUFDRjtBQUNGLENBVkQsQzs7Ozs7Ozs7Ozs7O0FDRkE7QUFDQTtBQUNBO0FBQ0E7QUFDYTs7QUFHYixJQUFJeEksY0FBYyxHQUFHbFEsbUJBQU8sQ0FBQywyRUFBRCxDQUFQLENBQTJCa1EsY0FBaEQ7O0FBR0EsU0FBUzBQLFVBQVQsQ0FBb0IzZCxHQUFwQixFQUF5QjtBQUN2QixTQUFPLFlBQVlkLElBQVosQ0FBaUJjLEdBQWpCLENBQVA7QUFDRDs7QUFDRCxTQUFTNGQsV0FBVCxDQUFxQjVkLEdBQXJCLEVBQTBCO0FBQ3hCLFNBQU8sYUFBYWQsSUFBYixDQUFrQmMsR0FBbEIsQ0FBUDtBQUNEOztBQUdEeEQsTUFBTSxDQUFDQyxPQUFQLEdBQWlCLFNBQVN3VyxPQUFULENBQWlCOUosS0FBakIsRUFBd0I7QUFDdkMsTUFBSTlNLENBQUo7QUFBQSxNQUFPd2hCLENBQVA7QUFBQSxNQUFVOUksQ0FBVjtBQUFBLE1BQWExTSxNQUFiO0FBQUEsTUFBcUJxQixLQUFyQjtBQUFBLE1BQTRCb1UsWUFBNUI7QUFBQSxNQUEwQ0MsS0FBMUM7QUFBQSxNQUFpREMsRUFBakQ7QUFBQSxNQUFxRG5kLElBQXJEO0FBQUEsTUFBMkRDLEdBQTNEO0FBQUEsTUFBZ0VpYyxPQUFoRTtBQUFBLE1BQ0lyTSxLQURKO0FBQUEsTUFDV3VOLGFBRFg7QUFBQSxNQUMwQjFZLEdBRDFCO0FBQUEsTUFDK0IyWSxPQUQvQjtBQUFBLE1BQ3dDQyxPQUR4QztBQUFBLE1BRUlDLFdBQVcsR0FBR2pWLEtBQUssQ0FBQ2QsTUFGeEI7QUFBQSxNQUdJZ1csS0FISjs7QUFLQSxNQUFJLENBQUNsVixLQUFLLENBQUNuQixFQUFOLENBQVNyQyxPQUFULENBQWlCc04sT0FBdEIsRUFBK0I7QUFBRTtBQUFTOztBQUUxQyxPQUFLNEssQ0FBQyxHQUFHLENBQUosRUFBTzlJLENBQUMsR0FBR3FKLFdBQVcsQ0FBQ2hpQixNQUE1QixFQUFvQ3loQixDQUFDLEdBQUc5SSxDQUF4QyxFQUEyQzhJLENBQUMsRUFBNUMsRUFBZ0Q7QUFDOUMsUUFBSU8sV0FBVyxDQUFDUCxDQUFELENBQVgsQ0FBZTVHLElBQWYsS0FBd0IsUUFBeEIsSUFDQSxDQUFDOU4sS0FBSyxDQUFDbkIsRUFBTixDQUFTaUwsT0FBVCxDQUFpQnRPLE9BQWpCLENBQXlCeVosV0FBVyxDQUFDUCxDQUFELENBQVgsQ0FBZTdILE9BQXhDLENBREwsRUFDdUQ7QUFDckQ7QUFDRDs7QUFFRDNOLFVBQU0sR0FBRytWLFdBQVcsQ0FBQ1AsQ0FBRCxDQUFYLENBQWVwSCxRQUF4QjtBQUVBd0gsaUJBQWEsR0FBRyxDQUFoQixDQVI4QyxDQVU5QztBQUNBOztBQUNBLFNBQUs1aEIsQ0FBQyxHQUFHZ00sTUFBTSxDQUFDak0sTUFBUCxHQUFnQixDQUF6QixFQUE0QkMsQ0FBQyxJQUFJLENBQWpDLEVBQW9DQSxDQUFDLEVBQXJDLEVBQXlDO0FBQ3ZDeWhCLGtCQUFZLEdBQUd6VixNQUFNLENBQUNoTSxDQUFELENBQXJCLENBRHVDLENBR3ZDOztBQUNBLFVBQUl5aEIsWUFBWSxDQUFDN0csSUFBYixLQUFzQixZQUExQixFQUF3QztBQUN0QzVhLFNBQUM7O0FBQ0QsZUFBT2dNLE1BQU0sQ0FBQ2hNLENBQUQsQ0FBTixDQUFVcVUsS0FBVixLQUFvQm9OLFlBQVksQ0FBQ3BOLEtBQWpDLElBQTBDckksTUFBTSxDQUFDaE0sQ0FBRCxDQUFOLENBQVU0YSxJQUFWLEtBQW1CLFdBQXBFLEVBQWlGO0FBQy9FNWEsV0FBQztBQUNGOztBQUNEO0FBQ0QsT0FWc0MsQ0FZdkM7OztBQUNBLFVBQUl5aEIsWUFBWSxDQUFDN0csSUFBYixLQUFzQixhQUExQixFQUF5QztBQUN2QyxZQUFJMEcsVUFBVSxDQUFDRyxZQUFZLENBQUM5SCxPQUFkLENBQVYsSUFBb0NpSSxhQUFhLEdBQUcsQ0FBeEQsRUFBMkQ7QUFDekRBLHVCQUFhO0FBQ2Q7O0FBQ0QsWUFBSUwsV0FBVyxDQUFDRSxZQUFZLENBQUM5SCxPQUFkLENBQWYsRUFBdUM7QUFDckNpSSx1QkFBYTtBQUNkO0FBQ0Y7O0FBQ0QsVUFBSUEsYUFBYSxHQUFHLENBQXBCLEVBQXVCO0FBQUU7QUFBVzs7QUFFcEMsVUFBSUgsWUFBWSxDQUFDN0csSUFBYixLQUFzQixNQUF0QixJQUFnQzlOLEtBQUssQ0FBQ25CLEVBQU4sQ0FBU2lMLE9BQVQsQ0FBaUIvVCxJQUFqQixDQUFzQjRlLFlBQVksQ0FBQzlILE9BQW5DLENBQXBDLEVBQWlGO0FBRS9FblYsWUFBSSxHQUFHaWQsWUFBWSxDQUFDOUgsT0FBcEI7QUFDQXFJLGFBQUssR0FBR2xWLEtBQUssQ0FBQ25CLEVBQU4sQ0FBU2lMLE9BQVQsQ0FBaUIxUixLQUFqQixDQUF1QlYsSUFBdkIsQ0FBUixDQUgrRSxDQUsvRTs7QUFDQWtkLGFBQUssR0FBRyxFQUFSO0FBQ0FyTixhQUFLLEdBQUdvTixZQUFZLENBQUNwTixLQUFyQjtBQUNBcU0sZUFBTyxHQUFHLENBQVY7O0FBRUEsYUFBS2lCLEVBQUUsR0FBRyxDQUFWLEVBQWFBLEVBQUUsR0FBR0ssS0FBSyxDQUFDamlCLE1BQXhCLEVBQWdDNGhCLEVBQUUsRUFBbEMsRUFBc0M7QUFFcEN6WSxhQUFHLEdBQUc4WSxLQUFLLENBQUNMLEVBQUQsQ0FBTCxDQUFVelksR0FBaEI7QUFDQTJZLGlCQUFPLEdBQUcvVSxLQUFLLENBQUNuQixFQUFOLENBQVNrSyxhQUFULENBQXVCM00sR0FBdkIsQ0FBVjs7QUFDQSxjQUFJLENBQUM0RCxLQUFLLENBQUNuQixFQUFOLENBQVNnSyxZQUFULENBQXNCa00sT0FBdEIsQ0FBTCxFQUFxQztBQUFFO0FBQVc7O0FBRWxEQyxpQkFBTyxHQUFHRSxLQUFLLENBQUNMLEVBQUQsQ0FBTCxDQUFVbmQsSUFBcEIsQ0FOb0MsQ0FRcEM7QUFDQTtBQUNBO0FBQ0E7O0FBQ0EsY0FBSSxDQUFDd2QsS0FBSyxDQUFDTCxFQUFELENBQUwsQ0FBVS9ZLE1BQWYsRUFBdUI7QUFDckJrWixtQkFBTyxHQUFHaFYsS0FBSyxDQUFDbkIsRUFBTixDQUFTMkssaUJBQVQsQ0FBMkIsWUFBWXdMLE9BQXZDLEVBQWdEbGUsT0FBaEQsQ0FBd0QsWUFBeEQsRUFBc0UsRUFBdEUsQ0FBVjtBQUNELFdBRkQsTUFFTyxJQUFJb2UsS0FBSyxDQUFDTCxFQUFELENBQUwsQ0FBVS9ZLE1BQVYsS0FBcUIsU0FBckIsSUFBa0MsQ0FBQyxZQUFZL0YsSUFBWixDQUFpQmlmLE9BQWpCLENBQXZDLEVBQWtFO0FBQ3ZFQSxtQkFBTyxHQUFHaFYsS0FBSyxDQUFDbkIsRUFBTixDQUFTMkssaUJBQVQsQ0FBMkIsWUFBWXdMLE9BQXZDLEVBQWdEbGUsT0FBaEQsQ0FBd0QsVUFBeEQsRUFBb0UsRUFBcEUsQ0FBVjtBQUNELFdBRk0sTUFFQTtBQUNMa2UsbUJBQU8sR0FBR2hWLEtBQUssQ0FBQ25CLEVBQU4sQ0FBUzJLLGlCQUFULENBQTJCd0wsT0FBM0IsQ0FBVjtBQUNEOztBQUVEcmQsYUFBRyxHQUFHdWQsS0FBSyxDQUFDTCxFQUFELENBQUwsQ0FBVTVZLEtBQWhCOztBQUVBLGNBQUl0RSxHQUFHLEdBQUdpYyxPQUFWLEVBQW1CO0FBQ2pCclQsaUJBQUssR0FBVyxJQUFJUCxLQUFLLENBQUM4UyxLQUFWLENBQWdCLE1BQWhCLEVBQXdCLEVBQXhCLEVBQTRCLENBQTVCLENBQWhCO0FBQ0F2UyxpQkFBSyxDQUFDc00sT0FBTixHQUFnQm5WLElBQUksQ0FBQzlCLEtBQUwsQ0FBV2dlLE9BQVgsRUFBb0JqYyxHQUFwQixDQUFoQjtBQUNBNEksaUJBQUssQ0FBQ2dILEtBQU4sR0FBZ0JBLEtBQWhCO0FBQ0FxTixpQkFBSyxDQUFDdGdCLElBQU4sQ0FBV2lNLEtBQVg7QUFDRDs7QUFFREEsZUFBSyxHQUFXLElBQUlQLEtBQUssQ0FBQzhTLEtBQVYsQ0FBZ0IsV0FBaEIsRUFBNkIsR0FBN0IsRUFBa0MsQ0FBbEMsQ0FBaEI7QUFDQXZTLGVBQUssQ0FBQzRDLEtBQU4sR0FBZ0IsQ0FBRSxDQUFFLE1BQUYsRUFBVTRSLE9BQVYsQ0FBRixDQUFoQjtBQUNBeFUsZUFBSyxDQUFDZ0gsS0FBTixHQUFnQkEsS0FBSyxFQUFyQjtBQUNBaEgsZUFBSyxDQUFDRCxNQUFOLEdBQWdCLFNBQWhCO0FBQ0FDLGVBQUssQ0FBQ2lCLElBQU4sR0FBZ0IsTUFBaEI7QUFDQW9ULGVBQUssQ0FBQ3RnQixJQUFOLENBQVdpTSxLQUFYO0FBRUFBLGVBQUssR0FBVyxJQUFJUCxLQUFLLENBQUM4UyxLQUFWLENBQWdCLE1BQWhCLEVBQXdCLEVBQXhCLEVBQTRCLENBQTVCLENBQWhCO0FBQ0F2UyxlQUFLLENBQUNzTSxPQUFOLEdBQWdCbUksT0FBaEI7QUFDQXpVLGVBQUssQ0FBQ2dILEtBQU4sR0FBZ0JBLEtBQWhCO0FBQ0FxTixlQUFLLENBQUN0Z0IsSUFBTixDQUFXaU0sS0FBWDtBQUVBQSxlQUFLLEdBQVcsSUFBSVAsS0FBSyxDQUFDOFMsS0FBVixDQUFnQixZQUFoQixFQUE4QixHQUE5QixFQUFtQyxDQUFDLENBQXBDLENBQWhCO0FBQ0F2UyxlQUFLLENBQUNnSCxLQUFOLEdBQWdCLEVBQUVBLEtBQWxCO0FBQ0FoSCxlQUFLLENBQUNELE1BQU4sR0FBZ0IsU0FBaEI7QUFDQUMsZUFBSyxDQUFDaUIsSUFBTixHQUFnQixNQUFoQjtBQUNBb1QsZUFBSyxDQUFDdGdCLElBQU4sQ0FBV2lNLEtBQVg7QUFFQXFULGlCQUFPLEdBQUdzQixLQUFLLENBQUNMLEVBQUQsQ0FBTCxDQUFVM1ksU0FBcEI7QUFDRDs7QUFDRCxZQUFJMFgsT0FBTyxHQUFHbGMsSUFBSSxDQUFDekUsTUFBbkIsRUFBMkI7QUFDekJzTixlQUFLLEdBQVcsSUFBSVAsS0FBSyxDQUFDOFMsS0FBVixDQUFnQixNQUFoQixFQUF3QixFQUF4QixFQUE0QixDQUE1QixDQUFoQjtBQUNBdlMsZUFBSyxDQUFDc00sT0FBTixHQUFnQm5WLElBQUksQ0FBQzlCLEtBQUwsQ0FBV2dlLE9BQVgsQ0FBaEI7QUFDQXJULGVBQUssQ0FBQ2dILEtBQU4sR0FBZ0JBLEtBQWhCO0FBQ0FxTixlQUFLLENBQUN0Z0IsSUFBTixDQUFXaU0sS0FBWDtBQUNELFNBaEU4RSxDQWtFL0U7OztBQUNBMFUsbUJBQVcsQ0FBQ1AsQ0FBRCxDQUFYLENBQWVwSCxRQUFmLEdBQTBCcE8sTUFBTSxHQUFHNEYsY0FBYyxDQUFDNUYsTUFBRCxFQUFTaE0sQ0FBVCxFQUFZMGhCLEtBQVosQ0FBakQ7QUFDRDtBQUNGO0FBQ0Y7QUFDRixDQWxIRCxDOzs7Ozs7Ozs7Ozs7QUNsQkE7Q0FLQTs7QUFDQSxJQUFJTyxXQUFXLEdBQUksV0FBbkI7QUFDQSxJQUFJQyxPQUFPLEdBQVEsS0FBbkI7O0FBR0EvaEIsTUFBTSxDQUFDQyxPQUFQLEdBQWlCLFNBQVMrRixTQUFULENBQW1CMkcsS0FBbkIsRUFBMEI7QUFDekMsTUFBSW5KLEdBQUosQ0FEeUMsQ0FHekM7O0FBQ0FBLEtBQUcsR0FBR21KLEtBQUssQ0FBQ2UsR0FBTixDQUFVakssT0FBVixDQUFrQnFlLFdBQWxCLEVBQStCLElBQS9CLENBQU4sQ0FKeUMsQ0FNekM7O0FBQ0F0ZSxLQUFHLEdBQUdBLEdBQUcsQ0FBQ0MsT0FBSixDQUFZc2UsT0FBWixFQUFxQixRQUFyQixDQUFOO0FBRUFwVixPQUFLLENBQUNlLEdBQU4sR0FBWWxLLEdBQVo7QUFDRCxDQVZELEM7Ozs7Ozs7Ozs7OztBQ1ZBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Q0FHQTtBQUNBO0FBQ0E7O0FBRUEsSUFBSXdlLE9BQU8sR0FBRyw4QkFBZCxDLENBRUE7QUFDQTs7QUFDQSxJQUFJQyxtQkFBbUIsR0FBRyxpQkFBMUI7QUFFQSxJQUFJQyxjQUFjLEdBQUcsa0JBQXJCO0FBQ0EsSUFBSUMsV0FBVyxHQUFHO0FBQ2hCdlEsR0FBQyxFQUFFLEdBRGE7QUFFaEJ3USxHQUFDLEVBQUUsR0FGYTtBQUdoQkMsR0FBQyxFQUFFLEdBSGE7QUFJaEJDLElBQUUsRUFBRTtBQUpZLENBQWxCOztBQU9BLFNBQVNDLFNBQVQsQ0FBbUJ4ZCxLQUFuQixFQUEwQnZDLElBQTFCLEVBQWdDO0FBQzlCLFNBQU8yZixXQUFXLENBQUMzZixJQUFJLENBQUNtRyxXQUFMLEVBQUQsQ0FBbEI7QUFDRDs7QUFFRCxTQUFTNlosY0FBVCxDQUF3QkMsWUFBeEIsRUFBc0M7QUFDcEMsTUFBSTVpQixDQUFKO0FBQUEsTUFBT3FOLEtBQVA7QUFBQSxNQUFjd1YsZUFBZSxHQUFHLENBQWhDOztBQUVBLE9BQUs3aUIsQ0FBQyxHQUFHNGlCLFlBQVksQ0FBQzdpQixNQUFiLEdBQXNCLENBQS9CLEVBQWtDQyxDQUFDLElBQUksQ0FBdkMsRUFBMENBLENBQUMsRUFBM0MsRUFBK0M7QUFDN0NxTixTQUFLLEdBQUd1VixZQUFZLENBQUM1aUIsQ0FBRCxDQUFwQjs7QUFFQSxRQUFJcU4sS0FBSyxDQUFDdU4sSUFBTixLQUFlLE1BQWYsSUFBeUIsQ0FBQ2lJLGVBQTlCLEVBQStDO0FBQzdDeFYsV0FBSyxDQUFDc00sT0FBTixHQUFnQnRNLEtBQUssQ0FBQ3NNLE9BQU4sQ0FBYy9WLE9BQWQsQ0FBc0J5ZSxjQUF0QixFQUFzQ0ssU0FBdEMsQ0FBaEI7QUFDRDs7QUFFRCxRQUFJclYsS0FBSyxDQUFDdU4sSUFBTixLQUFlLFdBQWYsSUFBOEJ2TixLQUFLLENBQUNpQixJQUFOLEtBQWUsTUFBakQsRUFBeUQ7QUFDdkR1VSxxQkFBZTtBQUNoQjs7QUFFRCxRQUFJeFYsS0FBSyxDQUFDdU4sSUFBTixLQUFlLFlBQWYsSUFBK0J2TixLQUFLLENBQUNpQixJQUFOLEtBQWUsTUFBbEQsRUFBMEQ7QUFDeER1VSxxQkFBZTtBQUNoQjtBQUNGO0FBQ0Y7O0FBRUQsU0FBU0MsWUFBVCxDQUFzQkYsWUFBdEIsRUFBb0M7QUFDbEMsTUFBSTVpQixDQUFKO0FBQUEsTUFBT3FOLEtBQVA7QUFBQSxNQUFjd1YsZUFBZSxHQUFHLENBQWhDOztBQUVBLE9BQUs3aUIsQ0FBQyxHQUFHNGlCLFlBQVksQ0FBQzdpQixNQUFiLEdBQXNCLENBQS9CLEVBQWtDQyxDQUFDLElBQUksQ0FBdkMsRUFBMENBLENBQUMsRUFBM0MsRUFBK0M7QUFDN0NxTixTQUFLLEdBQUd1VixZQUFZLENBQUM1aUIsQ0FBRCxDQUFwQjs7QUFFQSxRQUFJcU4sS0FBSyxDQUFDdU4sSUFBTixLQUFlLE1BQWYsSUFBeUIsQ0FBQ2lJLGVBQTlCLEVBQStDO0FBQzdDLFVBQUlWLE9BQU8sQ0FBQ3RmLElBQVIsQ0FBYXdLLEtBQUssQ0FBQ3NNLE9BQW5CLENBQUosRUFBaUM7QUFDL0J0TSxhQUFLLENBQUNzTSxPQUFOLEdBQWdCdE0sS0FBSyxDQUFDc00sT0FBTixDQUNiL1YsT0FEYSxDQUNMLE1BREssRUFDRyxHQURILEVBRWQ7QUFDQTtBQUhjLFNBSWJBLE9BSmEsQ0FJTCxTQUpLLEVBSU0sR0FKTixFQUlXQSxPQUpYLENBSW1CLFVBSm5CLEVBSStCLE1BSi9CLEVBS2JBLE9BTGEsQ0FLTCxhQUxLLEVBS1UsUUFMVixFQUtvQkEsT0FMcEIsQ0FLNEIsUUFMNUIsRUFLc0MsR0FMdEMsRUFNZDtBQU5jLFNBT2JBLE9BUGEsQ0FPTCx1QkFQSyxFQU9vQixZQVBwQixFQVFkO0FBUmMsU0FTYkEsT0FUYSxDQVNMLGtCQVRLLEVBU2UsWUFUZixFQVViQSxPQVZhLENBVUwsMEJBVkssRUFVdUIsWUFWdkIsQ0FBaEI7QUFXRDtBQUNGOztBQUVELFFBQUl5SixLQUFLLENBQUN1TixJQUFOLEtBQWUsV0FBZixJQUE4QnZOLEtBQUssQ0FBQ2lCLElBQU4sS0FBZSxNQUFqRCxFQUF5RDtBQUN2RHVVLHFCQUFlO0FBQ2hCOztBQUVELFFBQUl4VixLQUFLLENBQUN1TixJQUFOLEtBQWUsWUFBZixJQUErQnZOLEtBQUssQ0FBQ2lCLElBQU4sS0FBZSxNQUFsRCxFQUEwRDtBQUN4RHVVLHFCQUFlO0FBQ2hCO0FBQ0Y7QUFDRjs7QUFHRDFpQixNQUFNLENBQUNDLE9BQVAsR0FBaUIsU0FBU3dELE9BQVQsQ0FBaUJrSixLQUFqQixFQUF3QjtBQUN2QyxNQUFJaVcsTUFBSjs7QUFFQSxNQUFJLENBQUNqVyxLQUFLLENBQUNuQixFQUFOLENBQVNyQyxPQUFULENBQWlCOFAsV0FBdEIsRUFBbUM7QUFBRTtBQUFTOztBQUU5QyxPQUFLMkosTUFBTSxHQUFHalcsS0FBSyxDQUFDZCxNQUFOLENBQWFqTSxNQUFiLEdBQXNCLENBQXBDLEVBQXVDZ2pCLE1BQU0sSUFBSSxDQUFqRCxFQUFvREEsTUFBTSxFQUExRCxFQUE4RDtBQUU1RCxRQUFJalcsS0FBSyxDQUFDZCxNQUFOLENBQWErVyxNQUFiLEVBQXFCbkksSUFBckIsS0FBOEIsUUFBbEMsRUFBNEM7QUFBRTtBQUFXOztBQUV6RCxRQUFJd0gsbUJBQW1CLENBQUN2ZixJQUFwQixDQUF5QmlLLEtBQUssQ0FBQ2QsTUFBTixDQUFhK1csTUFBYixFQUFxQnBKLE9BQTlDLENBQUosRUFBNEQ7QUFDMURnSixvQkFBYyxDQUFDN1YsS0FBSyxDQUFDZCxNQUFOLENBQWErVyxNQUFiLEVBQXFCM0ksUUFBdEIsQ0FBZDtBQUNEOztBQUVELFFBQUkrSCxPQUFPLENBQUN0ZixJQUFSLENBQWFpSyxLQUFLLENBQUNkLE1BQU4sQ0FBYStXLE1BQWIsRUFBcUJwSixPQUFsQyxDQUFKLEVBQWdEO0FBQzlDbUosa0JBQVksQ0FBQ2hXLEtBQUssQ0FBQ2QsTUFBTixDQUFhK1csTUFBYixFQUFxQjNJLFFBQXRCLENBQVo7QUFDRDtBQUVGO0FBQ0YsQ0FsQkQsQzs7Ozs7Ozs7Ozs7O0FDeEZBO0FBQ0E7QUFDYTs7QUFHYixJQUFJM0csWUFBWSxHQUFLL1IsbUJBQU8sQ0FBQywyRUFBRCxDQUFQLENBQTJCK1IsWUFBaEQ7O0FBQ0EsSUFBSUUsV0FBVyxHQUFNalMsbUJBQU8sQ0FBQywyRUFBRCxDQUFQLENBQTJCaVMsV0FBaEQ7O0FBQ0EsSUFBSUMsY0FBYyxHQUFHbFMsbUJBQU8sQ0FBQywyRUFBRCxDQUFQLENBQTJCa1MsY0FBaEQ7O0FBRUEsSUFBSW9QLGFBQWEsR0FBRyxNQUFwQjtBQUNBLElBQUlDLFFBQVEsR0FBRyxPQUFmO0FBQ0EsSUFBSUMsVUFBVSxHQUFHLFFBQWpCO0FBQTJCOztBQUczQixTQUFTQyxTQUFULENBQW1CeGYsR0FBbkIsRUFBd0JvRixLQUF4QixFQUErQnNLLEVBQS9CLEVBQW1DO0FBQ2pDLFNBQU8xUCxHQUFHLENBQUNzYixNQUFKLENBQVcsQ0FBWCxFQUFjbFcsS0FBZCxJQUF1QnNLLEVBQXZCLEdBQTRCMVAsR0FBRyxDQUFDc2IsTUFBSixDQUFXbFcsS0FBSyxHQUFHLENBQW5CLENBQW5DO0FBQ0Q7O0FBRUQsU0FBU3FhLGVBQVQsQ0FBeUJwWCxNQUF6QixFQUFpQ2MsS0FBakMsRUFBd0M7QUFDdEMsTUFBSTlNLENBQUosRUFBT3FOLEtBQVAsRUFBYzdJLElBQWQsRUFBb0IwYyxDQUFwQixFQUF1QnpjLEdBQXZCLEVBQTRCa0osR0FBNUIsRUFBaUMwVixTQUFqQyxFQUE0Q0MsSUFBNUMsRUFBa0RDLFFBQWxELEVBQTREQyxRQUE1RCxFQUNJQyxlQURKLEVBQ3FCQyxlQURyQixFQUNzQ0MsZ0JBRHRDLEVBQ3dEQyxnQkFEeEQsRUFFSUMsT0FGSixFQUVhQyxRQUZiLEVBRXVCdEMsQ0FGdkIsRUFFMEJ1QyxRQUYxQixFQUVvQ0MsS0FGcEMsRUFFMkNDLFNBRjNDLEVBRXNEQyxVQUZ0RDtBQUlBRixPQUFLLEdBQUcsRUFBUjs7QUFFQSxPQUFLaGtCLENBQUMsR0FBRyxDQUFULEVBQVlBLENBQUMsR0FBR2dNLE1BQU0sQ0FBQ2pNLE1BQXZCLEVBQStCQyxDQUFDLEVBQWhDLEVBQW9DO0FBQ2xDcU4sU0FBSyxHQUFHckIsTUFBTSxDQUFDaE0sQ0FBRCxDQUFkO0FBRUFxakIsYUFBUyxHQUFHclgsTUFBTSxDQUFDaE0sQ0FBRCxDQUFOLENBQVVxVSxLQUF0Qjs7QUFFQSxTQUFLbU4sQ0FBQyxHQUFHd0MsS0FBSyxDQUFDamtCLE1BQU4sR0FBZSxDQUF4QixFQUEyQnloQixDQUFDLElBQUksQ0FBaEMsRUFBbUNBLENBQUMsRUFBcEMsRUFBd0M7QUFDdEMsVUFBSXdDLEtBQUssQ0FBQ3hDLENBQUQsQ0FBTCxDQUFTbk4sS0FBVCxJQUFrQmdQLFNBQXRCLEVBQWlDO0FBQUU7QUFBUTtBQUM1Qzs7QUFDRFcsU0FBSyxDQUFDamtCLE1BQU4sR0FBZXloQixDQUFDLEdBQUcsQ0FBbkI7O0FBRUEsUUFBSW5VLEtBQUssQ0FBQ3VOLElBQU4sS0FBZSxNQUFuQixFQUEyQjtBQUFFO0FBQVc7O0FBRXhDcFcsUUFBSSxHQUFHNkksS0FBSyxDQUFDc00sT0FBYjtBQUNBbFYsT0FBRyxHQUFHLENBQU47QUFDQWtKLE9BQUcsR0FBR25KLElBQUksQ0FBQ3pFLE1BQVg7QUFFQTs7QUFDQW9rQixTQUFLLEVBQ0wsT0FBTzFmLEdBQUcsR0FBR2tKLEdBQWIsRUFBa0I7QUFDaEJzVixjQUFRLENBQUNqYSxTQUFULEdBQXFCdkUsR0FBckI7QUFDQXljLE9BQUMsR0FBRytCLFFBQVEsQ0FBQ2xaLElBQVQsQ0FBY3ZGLElBQWQsQ0FBSjs7QUFDQSxVQUFJLENBQUMwYyxDQUFMLEVBQVE7QUFBRTtBQUFROztBQUVsQjJDLGFBQU8sR0FBR0MsUUFBUSxHQUFHLElBQXJCO0FBQ0FyZixTQUFHLEdBQUd5YyxDQUFDLENBQUNuWSxLQUFGLEdBQVUsQ0FBaEI7QUFDQWdiLGNBQVEsR0FBSTdDLENBQUMsQ0FBQyxDQUFELENBQUQsS0FBUyxHQUFyQixDQVBnQixDQVNoQjtBQUNBO0FBQ0E7O0FBQ0FxQyxjQUFRLEdBQUcsSUFBWDs7QUFFQSxVQUFJckMsQ0FBQyxDQUFDblksS0FBRixHQUFVLENBQVYsSUFBZSxDQUFuQixFQUFzQjtBQUNwQndhLGdCQUFRLEdBQUcvZSxJQUFJLENBQUNrSSxVQUFMLENBQWdCd1UsQ0FBQyxDQUFDblksS0FBRixHQUFVLENBQTFCLENBQVg7QUFDRCxPQUZELE1BRU87QUFDTCxhQUFLeVksQ0FBQyxHQUFHeGhCLENBQUMsR0FBRyxDQUFiLEVBQWdCd2hCLENBQUMsSUFBSSxDQUFyQixFQUF3QkEsQ0FBQyxFQUF6QixFQUE2QjtBQUMzQixjQUFJeFYsTUFBTSxDQUFDd1YsQ0FBRCxDQUFOLENBQVU1RyxJQUFWLEtBQW1CLFdBQW5CLElBQWtDNU8sTUFBTSxDQUFDd1YsQ0FBRCxDQUFOLENBQVU1RyxJQUFWLEtBQW1CLFdBQXpELEVBQXNFLE1BRDNDLENBQ2tEOztBQUM3RSxjQUFJNU8sTUFBTSxDQUFDd1YsQ0FBRCxDQUFOLENBQVU1RyxJQUFWLEtBQW1CLE1BQXZCLEVBQStCO0FBRS9CMkksa0JBQVEsR0FBR3ZYLE1BQU0sQ0FBQ3dWLENBQUQsQ0FBTixDQUFVN0gsT0FBVixDQUFrQmpOLFVBQWxCLENBQTZCVixNQUFNLENBQUN3VixDQUFELENBQU4sQ0FBVTdILE9BQVYsQ0FBa0I1WixNQUFsQixHQUEyQixDQUF4RCxDQUFYO0FBQ0E7QUFDRDtBQUNGLE9BeEJlLENBMEJoQjtBQUNBO0FBQ0E7OztBQUNBeWpCLGNBQVEsR0FBRyxJQUFYOztBQUVBLFVBQUkvZSxHQUFHLEdBQUdrSixHQUFWLEVBQWU7QUFDYjZWLGdCQUFRLEdBQUdoZixJQUFJLENBQUNrSSxVQUFMLENBQWdCakksR0FBaEIsQ0FBWDtBQUNELE9BRkQsTUFFTztBQUNMLGFBQUsrYyxDQUFDLEdBQUd4aEIsQ0FBQyxHQUFHLENBQWIsRUFBZ0J3aEIsQ0FBQyxHQUFHeFYsTUFBTSxDQUFDak0sTUFBM0IsRUFBbUN5aEIsQ0FBQyxFQUFwQyxFQUF3QztBQUN0QyxjQUFJeFYsTUFBTSxDQUFDd1YsQ0FBRCxDQUFOLENBQVU1RyxJQUFWLEtBQW1CLFdBQW5CLElBQWtDNU8sTUFBTSxDQUFDd1YsQ0FBRCxDQUFOLENBQVU1RyxJQUFWLEtBQW1CLFdBQXpELEVBQXNFLE1BRGhDLENBQ3VDOztBQUM3RSxjQUFJNU8sTUFBTSxDQUFDd1YsQ0FBRCxDQUFOLENBQVU1RyxJQUFWLEtBQW1CLE1BQXZCLEVBQStCO0FBRS9CNEksa0JBQVEsR0FBR3hYLE1BQU0sQ0FBQ3dWLENBQUQsQ0FBTixDQUFVN0gsT0FBVixDQUFrQmpOLFVBQWxCLENBQTZCLENBQTdCLENBQVg7QUFDQTtBQUNEO0FBQ0Y7O0FBRUQrVyxxQkFBZSxHQUFHN1AsY0FBYyxDQUFDMlAsUUFBRCxDQUFkLElBQTRCNVAsV0FBVyxDQUFDeEIsTUFBTSxDQUFDQyxZQUFQLENBQW9CbVIsUUFBcEIsQ0FBRCxDQUF6RDtBQUNBRyxxQkFBZSxHQUFHOVAsY0FBYyxDQUFDNFAsUUFBRCxDQUFkLElBQTRCN1AsV0FBVyxDQUFDeEIsTUFBTSxDQUFDQyxZQUFQLENBQW9Cb1IsUUFBcEIsQ0FBRCxDQUF6RDtBQUVBRyxzQkFBZ0IsR0FBR2xRLFlBQVksQ0FBQzhQLFFBQUQsQ0FBL0I7QUFDQUssc0JBQWdCLEdBQUduUSxZQUFZLENBQUMrUCxRQUFELENBQS9COztBQUVBLFVBQUlJLGdCQUFKLEVBQXNCO0FBQ3BCQyxlQUFPLEdBQUcsS0FBVjtBQUNELE9BRkQsTUFFTyxJQUFJSCxlQUFKLEVBQXFCO0FBQzFCLFlBQUksRUFBRUMsZ0JBQWdCLElBQUlGLGVBQXRCLENBQUosRUFBNEM7QUFDMUNJLGlCQUFPLEdBQUcsS0FBVjtBQUNEO0FBQ0Y7O0FBRUQsVUFBSUYsZ0JBQUosRUFBc0I7QUFDcEJHLGdCQUFRLEdBQUcsS0FBWDtBQUNELE9BRkQsTUFFTyxJQUFJTCxlQUFKLEVBQXFCO0FBQzFCLFlBQUksRUFBRUcsZ0JBQWdCLElBQUlGLGVBQXRCLENBQUosRUFBNEM7QUFDMUNJLGtCQUFRLEdBQUcsS0FBWDtBQUNEO0FBQ0Y7O0FBRUQsVUFBSU4sUUFBUSxLQUFLO0FBQUs7QUFBbEIsU0FBNkJ0QyxDQUFDLENBQUMsQ0FBRCxDQUFELEtBQVMsR0FBMUMsRUFBK0M7QUFDN0MsWUFBSXFDLFFBQVEsSUFBSTtBQUFLO0FBQWpCLFdBQTRCQSxRQUFRLElBQUk7QUFBSztBQUFqRCxVQUEwRDtBQUN4RDtBQUNBTyxvQkFBUSxHQUFHRCxPQUFPLEdBQUcsS0FBckI7QUFDRDtBQUNGOztBQUVELFVBQUlBLE9BQU8sSUFBSUMsUUFBZixFQUF5QjtBQUN2QjtBQUNBRCxlQUFPLEdBQUcsS0FBVjtBQUNBQyxnQkFBUSxHQUFHSixlQUFYO0FBQ0Q7O0FBRUQsVUFBSSxDQUFDRyxPQUFELElBQVksQ0FBQ0MsUUFBakIsRUFBMkI7QUFDekI7QUFDQSxZQUFJQyxRQUFKLEVBQWM7QUFDWjFXLGVBQUssQ0FBQ3NNLE9BQU4sR0FBZ0J3SixTQUFTLENBQUM5VixLQUFLLENBQUNzTSxPQUFQLEVBQWdCdUgsQ0FBQyxDQUFDblksS0FBbEIsRUFBeUJtYSxVQUF6QixDQUF6QjtBQUNEOztBQUNEO0FBQ0Q7O0FBRUQsVUFBSVksUUFBSixFQUFjO0FBQ1o7QUFDQSxhQUFLdEMsQ0FBQyxHQUFHd0MsS0FBSyxDQUFDamtCLE1BQU4sR0FBZSxDQUF4QixFQUEyQnloQixDQUFDLElBQUksQ0FBaEMsRUFBbUNBLENBQUMsRUFBcEMsRUFBd0M7QUFDdEM4QixjQUFJLEdBQUdVLEtBQUssQ0FBQ3hDLENBQUQsQ0FBWjs7QUFDQSxjQUFJd0MsS0FBSyxDQUFDeEMsQ0FBRCxDQUFMLENBQVNuTixLQUFULEdBQWlCZ1AsU0FBckIsRUFBZ0M7QUFBRTtBQUFROztBQUMxQyxjQUFJQyxJQUFJLENBQUNjLE1BQUwsS0FBZ0JMLFFBQWhCLElBQTRCQyxLQUFLLENBQUN4QyxDQUFELENBQUwsQ0FBU25OLEtBQVQsS0FBbUJnUCxTQUFuRCxFQUE4RDtBQUM1REMsZ0JBQUksR0FBR1UsS0FBSyxDQUFDeEMsQ0FBRCxDQUFaOztBQUVBLGdCQUFJdUMsUUFBSixFQUFjO0FBQ1pFLHVCQUFTLEdBQUduWCxLQUFLLENBQUNuQixFQUFOLENBQVNyQyxPQUFULENBQWlCK1AsTUFBakIsQ0FBd0IsQ0FBeEIsQ0FBWjtBQUNBNkssd0JBQVUsR0FBR3BYLEtBQUssQ0FBQ25CLEVBQU4sQ0FBU3JDLE9BQVQsQ0FBaUIrUCxNQUFqQixDQUF3QixDQUF4QixDQUFiO0FBQ0QsYUFIRCxNQUdPO0FBQ0w0Syx1QkFBUyxHQUFHblgsS0FBSyxDQUFDbkIsRUFBTixDQUFTckMsT0FBVCxDQUFpQitQLE1BQWpCLENBQXdCLENBQXhCLENBQVo7QUFDQTZLLHdCQUFVLEdBQUdwWCxLQUFLLENBQUNuQixFQUFOLENBQVNyQyxPQUFULENBQWlCK1AsTUFBakIsQ0FBd0IsQ0FBeEIsQ0FBYjtBQUNELGFBVDJELENBVzVEO0FBQ0E7QUFDQTs7O0FBQ0FoTSxpQkFBSyxDQUFDc00sT0FBTixHQUFnQndKLFNBQVMsQ0FBQzlWLEtBQUssQ0FBQ3NNLE9BQVAsRUFBZ0J1SCxDQUFDLENBQUNuWSxLQUFsQixFQUF5Qm1iLFVBQXpCLENBQXpCO0FBQ0FsWSxrQkFBTSxDQUFDc1gsSUFBSSxDQUFDalcsS0FBTixDQUFOLENBQW1Cc00sT0FBbkIsR0FBNkJ3SixTQUFTLENBQ3BDblgsTUFBTSxDQUFDc1gsSUFBSSxDQUFDalcsS0FBTixDQUFOLENBQW1Cc00sT0FEaUIsRUFDUjJKLElBQUksQ0FBQzdlLEdBREcsRUFDRXdmLFNBREYsQ0FBdEM7QUFHQXhmLGVBQUcsSUFBSXlmLFVBQVUsQ0FBQ25rQixNQUFYLEdBQW9CLENBQTNCOztBQUNBLGdCQUFJdWpCLElBQUksQ0FBQ2pXLEtBQUwsS0FBZXJOLENBQW5CLEVBQXNCO0FBQUV5RSxpQkFBRyxJQUFJd2YsU0FBUyxDQUFDbGtCLE1BQVYsR0FBbUIsQ0FBMUI7QUFBOEI7O0FBRXREeUUsZ0JBQUksR0FBRzZJLEtBQUssQ0FBQ3NNLE9BQWI7QUFDQWhNLGVBQUcsR0FBR25KLElBQUksQ0FBQ3pFLE1BQVg7QUFFQWlrQixpQkFBSyxDQUFDamtCLE1BQU4sR0FBZXloQixDQUFmO0FBQ0EscUJBQVMyQyxLQUFUO0FBQ0Q7QUFDRjtBQUNGOztBQUVELFVBQUlOLE9BQUosRUFBYTtBQUNYRyxhQUFLLENBQUM1aUIsSUFBTixDQUFXO0FBQ1RpTSxlQUFLLEVBQUVyTixDQURFO0FBRVR5RSxhQUFHLEVBQUV5YyxDQUFDLENBQUNuWSxLQUZFO0FBR1RxYixnQkFBTSxFQUFFTCxRQUhDO0FBSVQxUCxlQUFLLEVBQUVnUDtBQUpFLFNBQVg7QUFNRCxPQVBELE1BT08sSUFBSVMsUUFBUSxJQUFJQyxRQUFoQixFQUEwQjtBQUMvQjFXLGFBQUssQ0FBQ3NNLE9BQU4sR0FBZ0J3SixTQUFTLENBQUM5VixLQUFLLENBQUNzTSxPQUFQLEVBQWdCdUgsQ0FBQyxDQUFDblksS0FBbEIsRUFBeUJtYSxVQUF6QixDQUF6QjtBQUNEO0FBQ0Y7QUFDRjtBQUNGOztBQUdEL2lCLE1BQU0sQ0FBQ0MsT0FBUCxHQUFpQixTQUFTaWtCLFdBQVQsQ0FBcUJ2WCxLQUFyQixFQUE0QjtBQUMzQztBQUNBLE1BQUlpVyxNQUFKOztBQUVBLE1BQUksQ0FBQ2pXLEtBQUssQ0FBQ25CLEVBQU4sQ0FBU3JDLE9BQVQsQ0FBaUI4UCxXQUF0QixFQUFtQztBQUFFO0FBQVM7O0FBRTlDLE9BQUsySixNQUFNLEdBQUdqVyxLQUFLLENBQUNkLE1BQU4sQ0FBYWpNLE1BQWIsR0FBc0IsQ0FBcEMsRUFBdUNnakIsTUFBTSxJQUFJLENBQWpELEVBQW9EQSxNQUFNLEVBQTFELEVBQThEO0FBRTVELFFBQUlqVyxLQUFLLENBQUNkLE1BQU4sQ0FBYStXLE1BQWIsRUFBcUJuSSxJQUFyQixLQUE4QixRQUE5QixJQUNBLENBQUNvSSxhQUFhLENBQUNuZ0IsSUFBZCxDQUFtQmlLLEtBQUssQ0FBQ2QsTUFBTixDQUFhK1csTUFBYixFQUFxQnBKLE9BQXhDLENBREwsRUFDdUQ7QUFDckQ7QUFDRDs7QUFFRHlKLG1CQUFlLENBQUN0VyxLQUFLLENBQUNkLE1BQU4sQ0FBYStXLE1BQWIsRUFBcUIzSSxRQUF0QixFQUFnQ3ROLEtBQWhDLENBQWY7QUFDRDtBQUNGLENBZkQsQzs7Ozs7Ozs7Ozs7O0FDbkxBO0FBQ0E7QUFDYTs7QUFFYixJQUFJOFMsS0FBSyxHQUFHbGUsbUJBQU8sQ0FBQyw2REFBRCxDQUFuQjs7QUFHQSxTQUFTNGlCLFNBQVQsQ0FBbUJ6VyxHQUFuQixFQUF3QmxDLEVBQXhCLEVBQTRCTyxHQUE1QixFQUFpQztBQUMvQixPQUFLMkIsR0FBTCxHQUFXQSxHQUFYO0FBQ0EsT0FBSzNCLEdBQUwsR0FBV0EsR0FBWDtBQUNBLE9BQUtGLE1BQUwsR0FBYyxFQUFkO0FBQ0EsT0FBSzhMLFVBQUwsR0FBa0IsS0FBbEI7QUFDQSxPQUFLbk0sRUFBTCxHQUFVQSxFQUFWLENBTCtCLENBS2pCO0FBQ2YsQyxDQUVEOzs7QUFDQTJZLFNBQVMsQ0FBQ3BpQixTQUFWLENBQW9CMGQsS0FBcEIsR0FBNEJBLEtBQTVCO0FBR0F6ZixNQUFNLENBQUNDLE9BQVAsR0FBaUJra0IsU0FBakIsQzs7Ozs7Ozs7Ozs7O0FDbkJBO0FBRWE7QUFHYjs7QUFDQSxJQUFJQyxRQUFRLEdBQU0sMElBQWxCO0FBQ0EsSUFBSUMsV0FBVyxHQUFHLHNEQUFsQjs7QUFHQXJrQixNQUFNLENBQUNDLE9BQVAsR0FBaUIsU0FBU3FrQixRQUFULENBQWtCM1gsS0FBbEIsRUFBeUJHLE1BQXpCLEVBQWlDO0FBQ2hELE1BQUl0SSxJQUFKO0FBQUEsTUFBVStmLFNBQVY7QUFBQSxNQUFxQkMsVUFBckI7QUFBQSxNQUFpQ3piLEdBQWpDO0FBQUEsTUFBc0MyWSxPQUF0QztBQUFBLE1BQStDeFUsS0FBL0M7QUFBQSxNQUNJNUksR0FBRyxHQUFHcUksS0FBSyxDQUFDckksR0FEaEI7O0FBR0EsTUFBSXFJLEtBQUssQ0FBQ2UsR0FBTixDQUFVbkIsVUFBVixDQUFxQmpJLEdBQXJCLE1BQThCO0FBQUk7QUFBdEMsSUFBK0M7QUFBRSxhQUFPLEtBQVA7QUFBZTs7QUFFaEVFLE1BQUksR0FBR21JLEtBQUssQ0FBQ2UsR0FBTixDQUFVbkwsS0FBVixDQUFnQitCLEdBQWhCLENBQVA7O0FBRUEsTUFBSUUsSUFBSSxDQUFDdUYsT0FBTCxDQUFhLEdBQWIsSUFBb0IsQ0FBeEIsRUFBMkI7QUFBRSxXQUFPLEtBQVA7QUFBZTs7QUFFNUMsTUFBSXNhLFdBQVcsQ0FBQzNoQixJQUFaLENBQWlCOEIsSUFBakIsQ0FBSixFQUE0QjtBQUMxQitmLGFBQVMsR0FBRy9mLElBQUksQ0FBQ08sS0FBTCxDQUFXc2YsV0FBWCxDQUFaO0FBRUF0YixPQUFHLEdBQUd3YixTQUFTLENBQUMsQ0FBRCxDQUFULENBQWFoaUIsS0FBYixDQUFtQixDQUFuQixFQUFzQixDQUFDLENBQXZCLENBQU47QUFDQW1mLFdBQU8sR0FBRy9VLEtBQUssQ0FBQ25CLEVBQU4sQ0FBU2tLLGFBQVQsQ0FBdUIzTSxHQUF2QixDQUFWOztBQUNBLFFBQUksQ0FBQzRELEtBQUssQ0FBQ25CLEVBQU4sQ0FBU2dLLFlBQVQsQ0FBc0JrTSxPQUF0QixDQUFMLEVBQXFDO0FBQUUsYUFBTyxLQUFQO0FBQWU7O0FBRXRELFFBQUksQ0FBQzVVLE1BQUwsRUFBYTtBQUNYSSxXQUFLLEdBQVdQLEtBQUssQ0FBQzFMLElBQU4sQ0FBVyxXQUFYLEVBQXdCLEdBQXhCLEVBQTZCLENBQTdCLENBQWhCO0FBQ0FpTSxXQUFLLENBQUM0QyxLQUFOLEdBQWdCLENBQUUsQ0FBRSxNQUFGLEVBQVU0UixPQUFWLENBQUYsQ0FBaEI7QUFDQXhVLFdBQUssQ0FBQ0QsTUFBTixHQUFnQixVQUFoQjtBQUNBQyxXQUFLLENBQUNpQixJQUFOLEdBQWdCLE1BQWhCO0FBRUFqQixXQUFLLEdBQVdQLEtBQUssQ0FBQzFMLElBQU4sQ0FBVyxNQUFYLEVBQW1CLEVBQW5CLEVBQXVCLENBQXZCLENBQWhCO0FBQ0FpTSxXQUFLLENBQUNzTSxPQUFOLEdBQWdCN00sS0FBSyxDQUFDbkIsRUFBTixDQUFTMkssaUJBQVQsQ0FBMkJwTixHQUEzQixDQUFoQjtBQUVBbUUsV0FBSyxHQUFXUCxLQUFLLENBQUMxTCxJQUFOLENBQVcsWUFBWCxFQUF5QixHQUF6QixFQUE4QixDQUFDLENBQS9CLENBQWhCO0FBQ0FpTSxXQUFLLENBQUNELE1BQU4sR0FBZ0IsVUFBaEI7QUFDQUMsV0FBSyxDQUFDaUIsSUFBTixHQUFnQixNQUFoQjtBQUNEOztBQUVEeEIsU0FBSyxDQUFDckksR0FBTixJQUFhaWdCLFNBQVMsQ0FBQyxDQUFELENBQVQsQ0FBYTNrQixNQUExQjtBQUNBLFdBQU8sSUFBUDtBQUNEOztBQUVELE1BQUl3a0IsUUFBUSxDQUFDMWhCLElBQVQsQ0FBYzhCLElBQWQsQ0FBSixFQUF5QjtBQUN2QmdnQixjQUFVLEdBQUdoZ0IsSUFBSSxDQUFDTyxLQUFMLENBQVdxZixRQUFYLENBQWI7QUFFQXJiLE9BQUcsR0FBR3liLFVBQVUsQ0FBQyxDQUFELENBQVYsQ0FBY2ppQixLQUFkLENBQW9CLENBQXBCLEVBQXVCLENBQUMsQ0FBeEIsQ0FBTjtBQUNBbWYsV0FBTyxHQUFHL1UsS0FBSyxDQUFDbkIsRUFBTixDQUFTa0ssYUFBVCxDQUF1QixZQUFZM00sR0FBbkMsQ0FBVjs7QUFDQSxRQUFJLENBQUM0RCxLQUFLLENBQUNuQixFQUFOLENBQVNnSyxZQUFULENBQXNCa00sT0FBdEIsQ0FBTCxFQUFxQztBQUFFLGFBQU8sS0FBUDtBQUFlOztBQUV0RCxRQUFJLENBQUM1VSxNQUFMLEVBQWE7QUFDWEksV0FBSyxHQUFXUCxLQUFLLENBQUMxTCxJQUFOLENBQVcsV0FBWCxFQUF3QixHQUF4QixFQUE2QixDQUE3QixDQUFoQjtBQUNBaU0sV0FBSyxDQUFDNEMsS0FBTixHQUFnQixDQUFFLENBQUUsTUFBRixFQUFVNFIsT0FBVixDQUFGLENBQWhCO0FBQ0F4VSxXQUFLLENBQUNELE1BQU4sR0FBZ0IsVUFBaEI7QUFDQUMsV0FBSyxDQUFDaUIsSUFBTixHQUFnQixNQUFoQjtBQUVBakIsV0FBSyxHQUFXUCxLQUFLLENBQUMxTCxJQUFOLENBQVcsTUFBWCxFQUFtQixFQUFuQixFQUF1QixDQUF2QixDQUFoQjtBQUNBaU0sV0FBSyxDQUFDc00sT0FBTixHQUFnQjdNLEtBQUssQ0FBQ25CLEVBQU4sQ0FBUzJLLGlCQUFULENBQTJCcE4sR0FBM0IsQ0FBaEI7QUFFQW1FLFdBQUssR0FBV1AsS0FBSyxDQUFDMUwsSUFBTixDQUFXLFlBQVgsRUFBeUIsR0FBekIsRUFBOEIsQ0FBQyxDQUEvQixDQUFoQjtBQUNBaU0sV0FBSyxDQUFDRCxNQUFOLEdBQWdCLFVBQWhCO0FBQ0FDLFdBQUssQ0FBQ2lCLElBQU4sR0FBZ0IsTUFBaEI7QUFDRDs7QUFFRHhCLFNBQUssQ0FBQ3JJLEdBQU4sSUFBYWtnQixVQUFVLENBQUMsQ0FBRCxDQUFWLENBQWM1a0IsTUFBM0I7QUFDQSxXQUFPLElBQVA7QUFDRDs7QUFFRCxTQUFPLEtBQVA7QUFDRCxDQTdERCxDOzs7Ozs7Ozs7Ozs7QUNWQTtBQUVhOztBQUViSSxNQUFNLENBQUNDLE9BQVAsR0FBaUIsU0FBU3drQixRQUFULENBQWtCOVgsS0FBbEIsRUFBeUJHLE1BQXpCLEVBQWlDO0FBQ2hELE1BQUl4RSxLQUFKO0FBQUEsTUFBV2tGLEdBQVg7QUFBQSxNQUFnQm5CLE1BQWhCO0FBQUEsTUFBd0JxWSxVQUF4QjtBQUFBLE1BQW9DQyxRQUFwQztBQUFBLE1BQThDelgsS0FBOUM7QUFBQSxNQUNJNUksR0FBRyxHQUFHcUksS0FBSyxDQUFDckksR0FEaEI7QUFBQSxNQUVJNE8sRUFBRSxHQUFHdkcsS0FBSyxDQUFDZSxHQUFOLENBQVVuQixVQUFWLENBQXFCakksR0FBckIsQ0FGVDs7QUFJQSxNQUFJNE8sRUFBRSxLQUFLO0FBQUk7QUFBZixJQUF3QjtBQUFFLGFBQU8sS0FBUDtBQUFlOztBQUV6QzVLLE9BQUssR0FBR2hFLEdBQVI7QUFDQUEsS0FBRztBQUNIa0osS0FBRyxHQUFHYixLQUFLLENBQUM4SCxNQUFaOztBQUVBLFNBQU9uUSxHQUFHLEdBQUdrSixHQUFOLElBQWFiLEtBQUssQ0FBQ2UsR0FBTixDQUFVbkIsVUFBVixDQUFxQmpJLEdBQXJCLE1BQThCO0FBQUk7QUFBdEQsSUFBK0Q7QUFBRUEsT0FBRztBQUFLOztBQUV6RStILFFBQU0sR0FBR00sS0FBSyxDQUFDZSxHQUFOLENBQVVuTCxLQUFWLENBQWdCK0YsS0FBaEIsRUFBdUJoRSxHQUF2QixDQUFUO0FBRUFvZ0IsWUFBVSxHQUFHQyxRQUFRLEdBQUdyZ0IsR0FBeEI7O0FBRUEsU0FBTyxDQUFDb2dCLFVBQVUsR0FBRy9YLEtBQUssQ0FBQ2UsR0FBTixDQUFVM0QsT0FBVixDQUFrQixHQUFsQixFQUF1QjRhLFFBQXZCLENBQWQsTUFBb0QsQ0FBQyxDQUE1RCxFQUErRDtBQUM3REEsWUFBUSxHQUFHRCxVQUFVLEdBQUcsQ0FBeEI7O0FBRUEsV0FBT0MsUUFBUSxHQUFHblgsR0FBWCxJQUFrQmIsS0FBSyxDQUFDZSxHQUFOLENBQVVuQixVQUFWLENBQXFCb1ksUUFBckIsTUFBbUM7QUFBSTtBQUFoRSxNQUF5RTtBQUFFQSxjQUFRO0FBQUs7O0FBRXhGLFFBQUlBLFFBQVEsR0FBR0QsVUFBWCxLQUEwQnJZLE1BQU0sQ0FBQ3pNLE1BQXJDLEVBQTZDO0FBQzNDLFVBQUksQ0FBQ2tOLE1BQUwsRUFBYTtBQUNYSSxhQUFLLEdBQVdQLEtBQUssQ0FBQzFMLElBQU4sQ0FBVyxhQUFYLEVBQTBCLE1BQTFCLEVBQWtDLENBQWxDLENBQWhCO0FBQ0FpTSxhQUFLLENBQUNELE1BQU4sR0FBZ0JaLE1BQWhCO0FBQ0FhLGFBQUssQ0FBQ3NNLE9BQU4sR0FBZ0I3TSxLQUFLLENBQUNlLEdBQU4sQ0FBVW5MLEtBQVYsQ0FBZ0IrQixHQUFoQixFQUFxQm9nQixVQUFyQixFQUNiamhCLE9BRGEsQ0FDTCxLQURLLEVBQ0UsR0FERixFQUViQSxPQUZhLENBRUwsVUFGSyxFQUVPLElBRlAsQ0FBaEI7QUFHRDs7QUFDRGtKLFdBQUssQ0FBQ3JJLEdBQU4sR0FBWXFnQixRQUFaO0FBQ0EsYUFBTyxJQUFQO0FBQ0Q7QUFDRjs7QUFFRCxNQUFJLENBQUM3WCxNQUFMLEVBQWE7QUFBRUgsU0FBSyxDQUFDZ00sT0FBTixJQUFpQnRNLE1BQWpCO0FBQTBCOztBQUN6Q00sT0FBSyxDQUFDckksR0FBTixJQUFhK0gsTUFBTSxDQUFDek0sTUFBcEI7QUFDQSxTQUFPLElBQVA7QUFDRCxDQXRDRCxDOzs7Ozs7Ozs7Ozs7QUNKQTtBQUNBO0FBQ2E7O0FBR2IsU0FBU2dsQixpQkFBVCxDQUEyQmpZLEtBQTNCLEVBQWtDa1ksVUFBbEMsRUFBOEM7QUFDNUMsTUFBSUMsU0FBSjtBQUFBLE1BQWVDLFNBQWY7QUFBQSxNQUEwQkMsTUFBMUI7QUFBQSxNQUFrQ0MsTUFBbEM7QUFBQSxNQUEwQ0MsWUFBMUM7QUFBQSxNQUF3REMsZUFBeEQ7QUFBQSxNQUNJQyxVQURKO0FBQUEsTUFDZ0JDLFFBRGhCO0FBQUEsTUFFSUMsYUFBYSxHQUFHLEVBRnBCO0FBQUEsTUFHSTlYLEdBQUcsR0FBR3FYLFVBQVUsQ0FBQ2psQixNQUhyQjs7QUFLQSxPQUFLa2xCLFNBQVMsR0FBRyxDQUFqQixFQUFvQkEsU0FBUyxHQUFHdFgsR0FBaEMsRUFBcUNzWCxTQUFTLEVBQTlDLEVBQWtEO0FBQ2hERSxVQUFNLEdBQUdILFVBQVUsQ0FBQ0MsU0FBRCxDQUFuQixDQURnRCxDQUdoRDtBQUNBO0FBQ0E7QUFDQTs7QUFDQUUsVUFBTSxDQUFDcGxCLE1BQVAsR0FBZ0JvbEIsTUFBTSxDQUFDcGxCLE1BQVAsSUFBaUIsQ0FBakM7QUFFQSxRQUFJLENBQUNvbEIsTUFBTSxDQUFDTyxLQUFaLEVBQW1CLFNBVDZCLENBV2hEO0FBQ0E7O0FBQ0EsUUFBSSxDQUFDRCxhQUFhLENBQUNwaEIsY0FBZCxDQUE2QjhnQixNQUFNLENBQUMzWSxNQUFwQyxDQUFMLEVBQWtEO0FBQ2hEaVosbUJBQWEsQ0FBQ04sTUFBTSxDQUFDM1ksTUFBUixDQUFiLEdBQStCLENBQUUsQ0FBQyxDQUFILEVBQU0sQ0FBQyxDQUFQLEVBQVUsQ0FBQyxDQUFYLENBQS9CO0FBQ0Q7O0FBRUQ2WSxnQkFBWSxHQUFHSSxhQUFhLENBQUNOLE1BQU0sQ0FBQzNZLE1BQVIsQ0FBYixDQUE2QjJZLE1BQU0sQ0FBQ3BsQixNQUFQLEdBQWdCLENBQTdDLENBQWY7QUFDQXVsQixtQkFBZSxHQUFHLENBQUMsQ0FBbkI7QUFFQUosYUFBUyxHQUFHRCxTQUFTLEdBQUdFLE1BQU0sQ0FBQ1EsSUFBbkIsR0FBMEIsQ0FBdEM7O0FBRUEsV0FBT1QsU0FBUyxHQUFHRyxZQUFuQixFQUFpQ0gsU0FBUyxJQUFJRSxNQUFNLENBQUNPLElBQVAsR0FBYyxDQUE1RCxFQUErRDtBQUM3RFAsWUFBTSxHQUFHSixVQUFVLENBQUNFLFNBQUQsQ0FBbkI7QUFFQSxVQUFJRSxNQUFNLENBQUM1WSxNQUFQLEtBQWtCMlksTUFBTSxDQUFDM1ksTUFBN0IsRUFBcUM7QUFFckMsVUFBSThZLGVBQWUsS0FBSyxDQUFDLENBQXpCLEVBQTRCQSxlQUFlLEdBQUdKLFNBQWxCOztBQUU1QixVQUFJRSxNQUFNLENBQUNRLElBQVAsSUFDQVIsTUFBTSxDQUFDMWMsR0FBUCxHQUFhLENBRGIsSUFFQTBjLE1BQU0sQ0FBQy9RLEtBQVAsS0FBaUI4USxNQUFNLENBQUM5USxLQUY1QixFQUVtQztBQUVqQ2tSLGtCQUFVLEdBQUcsS0FBYixDQUZpQyxDQUlqQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFDQSxZQUFJSCxNQUFNLENBQUNNLEtBQVAsSUFBZ0JQLE1BQU0sQ0FBQ1MsSUFBM0IsRUFBaUM7QUFDL0IsY0FBSSxDQUFDUixNQUFNLENBQUNybEIsTUFBUCxHQUFnQm9sQixNQUFNLENBQUNwbEIsTUFBeEIsSUFBa0MsQ0FBbEMsS0FBd0MsQ0FBNUMsRUFBK0M7QUFDN0MsZ0JBQUlxbEIsTUFBTSxDQUFDcmxCLE1BQVAsR0FBZ0IsQ0FBaEIsS0FBc0IsQ0FBdEIsSUFBMkJvbEIsTUFBTSxDQUFDcGxCLE1BQVAsR0FBZ0IsQ0FBaEIsS0FBc0IsQ0FBckQsRUFBd0Q7QUFDdER3bEIsd0JBQVUsR0FBRyxJQUFiO0FBQ0Q7QUFDRjtBQUNGOztBQUVELFlBQUksQ0FBQ0EsVUFBTCxFQUFpQjtBQUNmO0FBQ0E7QUFDQTtBQUNBO0FBQ0FDLGtCQUFRLEdBQUdOLFNBQVMsR0FBRyxDQUFaLElBQWlCLENBQUNGLFVBQVUsQ0FBQ0UsU0FBUyxHQUFHLENBQWIsQ0FBVixDQUEwQlUsSUFBNUMsR0FDVFosVUFBVSxDQUFDRSxTQUFTLEdBQUcsQ0FBYixDQUFWLENBQTBCUyxJQUExQixHQUFpQyxDQUR4QixHQUVULENBRkY7QUFJQVIsZ0JBQU0sQ0FBQ1EsSUFBUCxHQUFlVixTQUFTLEdBQUdDLFNBQVosR0FBd0JNLFFBQXZDO0FBQ0FMLGdCQUFNLENBQUNTLElBQVAsR0FBZSxLQUFmO0FBQ0FSLGdCQUFNLENBQUMxYyxHQUFQLEdBQWV1YyxTQUFmO0FBQ0FHLGdCQUFNLENBQUNPLElBQVAsR0FBZUgsUUFBZjtBQUNBSixnQkFBTSxDQUFDTSxLQUFQLEdBQWUsS0FBZjtBQUNBSix5QkFBZSxHQUFHLENBQUMsQ0FBbkI7QUFDQTtBQUNEO0FBQ0Y7QUFDRjs7QUFFRCxRQUFJQSxlQUFlLEtBQUssQ0FBQyxDQUF6QixFQUE0QjtBQUMxQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBRyxtQkFBYSxDQUFDTixNQUFNLENBQUMzWSxNQUFSLENBQWIsQ0FBNkIsQ0FBQzJZLE1BQU0sQ0FBQ3BsQixNQUFQLElBQWlCLENBQWxCLElBQXVCLENBQXBELElBQXlEdWxCLGVBQXpEO0FBQ0Q7QUFDRjtBQUNGOztBQUdEbmxCLE1BQU0sQ0FBQ0MsT0FBUCxHQUFpQixTQUFTeWxCLFVBQVQsQ0FBb0IvWSxLQUFwQixFQUEyQjtBQUMxQyxNQUFJZ1osSUFBSjtBQUFBLE1BQ0lDLFdBQVcsR0FBR2paLEtBQUssQ0FBQ2laLFdBRHhCO0FBQUEsTUFFSXBZLEdBQUcsR0FBR2IsS0FBSyxDQUFDaVosV0FBTixDQUFrQmhtQixNQUY1QjtBQUlBZ2xCLG1CQUFpQixDQUFDalksS0FBRCxFQUFRQSxLQUFLLENBQUNrWSxVQUFkLENBQWpCOztBQUVBLE9BQUtjLElBQUksR0FBRyxDQUFaLEVBQWVBLElBQUksR0FBR25ZLEdBQXRCLEVBQTJCbVksSUFBSSxFQUEvQixFQUFtQztBQUNqQyxRQUFJQyxXQUFXLENBQUNELElBQUQsQ0FBWCxJQUFxQkMsV0FBVyxDQUFDRCxJQUFELENBQVgsQ0FBa0JkLFVBQTNDLEVBQXVEO0FBQ3JERCx1QkFBaUIsQ0FBQ2pZLEtBQUQsRUFBUWlaLFdBQVcsQ0FBQ0QsSUFBRCxDQUFYLENBQWtCZCxVQUExQixDQUFqQjtBQUNEO0FBQ0Y7QUFDRixDQVpELEM7Ozs7Ozs7Ozs7OztBQy9GQTtBQUNBO0NBSUE7QUFDQTs7QUFDQTdrQixNQUFNLENBQUNDLE9BQVAsQ0FBZW1PLFFBQWYsR0FBMEIsU0FBU3lYLFFBQVQsQ0FBa0JsWixLQUFsQixFQUF5QkcsTUFBekIsRUFBaUM7QUFDekQsTUFBSWpOLENBQUo7QUFBQSxNQUFPaW1CLE9BQVA7QUFBQSxNQUFnQjVZLEtBQWhCO0FBQUEsTUFDSTVFLEtBQUssR0FBR3FFLEtBQUssQ0FBQ3JJLEdBRGxCO0FBQUEsTUFFSStILE1BQU0sR0FBR00sS0FBSyxDQUFDZSxHQUFOLENBQVVuQixVQUFWLENBQXFCakUsS0FBckIsQ0FGYjs7QUFJQSxNQUFJd0UsTUFBSixFQUFZO0FBQUUsV0FBTyxLQUFQO0FBQWU7O0FBRTdCLE1BQUlULE1BQU0sS0FBSztBQUFLO0FBQWhCLEtBQTJCQSxNQUFNLEtBQUs7QUFBSztBQUEvQyxJQUF3RDtBQUFFLGFBQU8sS0FBUDtBQUFlOztBQUV6RXlaLFNBQU8sR0FBR25aLEtBQUssQ0FBQ29aLFVBQU4sQ0FBaUJwWixLQUFLLENBQUNySSxHQUF2QixFQUE0QitILE1BQU0sS0FBSyxJQUF2QyxDQUFWOztBQUVBLE9BQUt4TSxDQUFDLEdBQUcsQ0FBVCxFQUFZQSxDQUFDLEdBQUdpbUIsT0FBTyxDQUFDbG1CLE1BQXhCLEVBQWdDQyxDQUFDLEVBQWpDLEVBQXFDO0FBQ25DcU4sU0FBSyxHQUFXUCxLQUFLLENBQUMxTCxJQUFOLENBQVcsTUFBWCxFQUFtQixFQUFuQixFQUF1QixDQUF2QixDQUFoQjtBQUNBaU0sU0FBSyxDQUFDc00sT0FBTixHQUFnQnhILE1BQU0sQ0FBQ0MsWUFBUCxDQUFvQjVGLE1BQXBCLENBQWhCO0FBRUFNLFNBQUssQ0FBQ2tZLFVBQU4sQ0FBaUI1akIsSUFBakIsQ0FBc0I7QUFDcEI7QUFDQTtBQUNBb0wsWUFBTSxFQUFFQSxNQUhZO0FBS3BCO0FBQ0E7QUFDQXpNLFlBQU0sRUFBRWttQixPQUFPLENBQUNsbUIsTUFQSTtBQVNwQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBNGxCLFVBQUksRUFBSTNsQixDQWhCWTtBQWtCcEI7QUFDQTtBQUNBcU4sV0FBSyxFQUFHUCxLQUFLLENBQUNkLE1BQU4sQ0FBYWpNLE1BQWIsR0FBc0IsQ0FwQlY7QUFzQnBCO0FBQ0E7QUFDQTtBQUNBMkksU0FBRyxFQUFLLENBQUMsQ0F6Qlc7QUEyQnBCO0FBQ0E7QUFDQTtBQUNBa2QsVUFBSSxFQUFJSyxPQUFPLENBQUNFLFFBOUJJO0FBK0JwQlQsV0FBSyxFQUFHTyxPQUFPLENBQUNHO0FBL0JJLEtBQXRCO0FBaUNEOztBQUVEdFosT0FBSyxDQUFDckksR0FBTixJQUFhd2hCLE9BQU8sQ0FBQ2xtQixNQUFyQjtBQUVBLFNBQU8sSUFBUDtBQUNELENBckREOztBQXdEQSxTQUFTNlksV0FBVCxDQUFxQjlMLEtBQXJCLEVBQTRCa1ksVUFBNUIsRUFBd0M7QUFDdEMsTUFBSWhsQixDQUFKO0FBQUEsTUFDSXFtQixVQURKO0FBQUEsTUFFSUMsUUFGSjtBQUFBLE1BR0lqWixLQUhKO0FBQUEsTUFJSWdHLEVBSko7QUFBQSxNQUtJa1QsUUFMSjtBQUFBLE1BTUk1WSxHQUFHLEdBQUdxWCxVQUFVLENBQUNqbEIsTUFOckI7O0FBUUEsT0FBS0MsQ0FBQyxHQUFHMk4sR0FBRyxHQUFHLENBQWYsRUFBa0IzTixDQUFDLElBQUksQ0FBdkIsRUFBMEJBLENBQUMsRUFBM0IsRUFBK0I7QUFDN0JxbUIsY0FBVSxHQUFHckIsVUFBVSxDQUFDaGxCLENBQUQsQ0FBdkI7O0FBRUEsUUFBSXFtQixVQUFVLENBQUM3WixNQUFYLEtBQXNCO0FBQUk7QUFBMUIsT0FBcUM2WixVQUFVLENBQUM3WixNQUFYLEtBQXNCO0FBQUk7QUFBbkUsTUFBNEU7QUFDMUU7QUFDRCxPQUw0QixDQU83Qjs7O0FBQ0EsUUFBSTZaLFVBQVUsQ0FBQzNkLEdBQVgsS0FBbUIsQ0FBQyxDQUF4QixFQUEyQjtBQUN6QjtBQUNEOztBQUVENGQsWUFBUSxHQUFHdEIsVUFBVSxDQUFDcUIsVUFBVSxDQUFDM2QsR0FBWixDQUFyQixDQVo2QixDQWM3QjtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUNBNmQsWUFBUSxHQUFHdm1CLENBQUMsR0FBRyxDQUFKLElBQ0FnbEIsVUFBVSxDQUFDaGxCLENBQUMsR0FBRyxDQUFMLENBQVYsQ0FBa0IwSSxHQUFsQixLQUEwQjJkLFVBQVUsQ0FBQzNkLEdBQVgsR0FBaUIsQ0FEM0MsSUFFQXNjLFVBQVUsQ0FBQ2hsQixDQUFDLEdBQUcsQ0FBTCxDQUFWLENBQWtCcU4sS0FBbEIsS0FBNEJnWixVQUFVLENBQUNoWixLQUFYLEdBQW1CLENBRi9DLElBR0EyWCxVQUFVLENBQUNxQixVQUFVLENBQUMzZCxHQUFYLEdBQWlCLENBQWxCLENBQVYsQ0FBK0IyRSxLQUEvQixLQUF5Q2laLFFBQVEsQ0FBQ2paLEtBQVQsR0FBaUIsQ0FIMUQsSUFJQTJYLFVBQVUsQ0FBQ2hsQixDQUFDLEdBQUcsQ0FBTCxDQUFWLENBQWtCd00sTUFBbEIsS0FBNkI2WixVQUFVLENBQUM3WixNQUpuRDtBQU1BNkcsTUFBRSxHQUFHbEIsTUFBTSxDQUFDQyxZQUFQLENBQW9CaVUsVUFBVSxDQUFDN1osTUFBL0IsQ0FBTDtBQUVBYSxTQUFLLEdBQVdQLEtBQUssQ0FBQ2QsTUFBTixDQUFhcWEsVUFBVSxDQUFDaFosS0FBeEIsQ0FBaEI7QUFDQUEsU0FBSyxDQUFDdU4sSUFBTixHQUFnQjJMLFFBQVEsR0FBRyxhQUFILEdBQW1CLFNBQTNDO0FBQ0FsWixTQUFLLENBQUM2QixHQUFOLEdBQWdCcVgsUUFBUSxHQUFHLFFBQUgsR0FBYyxJQUF0QztBQUNBbFosU0FBSyxDQUFDbEIsT0FBTixHQUFnQixDQUFoQjtBQUNBa0IsU0FBSyxDQUFDRCxNQUFOLEdBQWdCbVosUUFBUSxHQUFHbFQsRUFBRSxHQUFHQSxFQUFSLEdBQWFBLEVBQXJDO0FBQ0FoRyxTQUFLLENBQUNzTSxPQUFOLEdBQWdCLEVBQWhCO0FBRUF0TSxTQUFLLEdBQVdQLEtBQUssQ0FBQ2QsTUFBTixDQUFhc2EsUUFBUSxDQUFDalosS0FBdEIsQ0FBaEI7QUFDQUEsU0FBSyxDQUFDdU4sSUFBTixHQUFnQjJMLFFBQVEsR0FBRyxjQUFILEdBQW9CLFVBQTVDO0FBQ0FsWixTQUFLLENBQUM2QixHQUFOLEdBQWdCcVgsUUFBUSxHQUFHLFFBQUgsR0FBYyxJQUF0QztBQUNBbFosU0FBSyxDQUFDbEIsT0FBTixHQUFnQixDQUFDLENBQWpCO0FBQ0FrQixTQUFLLENBQUNELE1BQU4sR0FBZ0JtWixRQUFRLEdBQUdsVCxFQUFFLEdBQUdBLEVBQVIsR0FBYUEsRUFBckM7QUFDQWhHLFNBQUssQ0FBQ3NNLE9BQU4sR0FBZ0IsRUFBaEI7O0FBRUEsUUFBSTRNLFFBQUosRUFBYztBQUNaelosV0FBSyxDQUFDZCxNQUFOLENBQWFnWixVQUFVLENBQUNobEIsQ0FBQyxHQUFHLENBQUwsQ0FBVixDQUFrQnFOLEtBQS9CLEVBQXNDc00sT0FBdEMsR0FBZ0QsRUFBaEQ7QUFDQTdNLFdBQUssQ0FBQ2QsTUFBTixDQUFhZ1osVUFBVSxDQUFDcUIsVUFBVSxDQUFDM2QsR0FBWCxHQUFpQixDQUFsQixDQUFWLENBQStCMkUsS0FBNUMsRUFBbURzTSxPQUFuRCxHQUE2RCxFQUE3RDtBQUNBM1osT0FBQztBQUNGO0FBQ0Y7QUFDRixDLENBR0Q7QUFDQTs7O0FBQ0FHLE1BQU0sQ0FBQ0MsT0FBUCxDQUFld1ksV0FBZixHQUE2QixTQUFTb04sUUFBVCxDQUFrQmxaLEtBQWxCLEVBQXlCO0FBQ3BELE1BQUlnWixJQUFKO0FBQUEsTUFDSUMsV0FBVyxHQUFHalosS0FBSyxDQUFDaVosV0FEeEI7QUFBQSxNQUVJcFksR0FBRyxHQUFHYixLQUFLLENBQUNpWixXQUFOLENBQWtCaG1CLE1BRjVCO0FBSUE2WSxhQUFXLENBQUM5TCxLQUFELEVBQVFBLEtBQUssQ0FBQ2tZLFVBQWQsQ0FBWDs7QUFFQSxPQUFLYyxJQUFJLEdBQUcsQ0FBWixFQUFlQSxJQUFJLEdBQUduWSxHQUF0QixFQUEyQm1ZLElBQUksRUFBL0IsRUFBbUM7QUFDakMsUUFBSUMsV0FBVyxDQUFDRCxJQUFELENBQVgsSUFBcUJDLFdBQVcsQ0FBQ0QsSUFBRCxDQUFYLENBQWtCZCxVQUEzQyxFQUF1RDtBQUNyRHBNLGlCQUFXLENBQUM5TCxLQUFELEVBQVFpWixXQUFXLENBQUNELElBQUQsQ0FBWCxDQUFrQmQsVUFBMUIsQ0FBWDtBQUNEO0FBQ0Y7QUFDRixDQVpELEM7Ozs7Ozs7Ozs7OztBQzVIQTtBQUVhOztBQUViLElBQUl2UyxRQUFRLEdBQVkvUSxtQkFBTyxDQUFDLGlGQUFELENBQS9COztBQUNBLElBQUlnUSxHQUFHLEdBQWlCaFEsbUJBQU8sQ0FBQywyRUFBRCxDQUFQLENBQTJCZ1EsR0FBbkQ7O0FBQ0EsSUFBSUksaUJBQWlCLEdBQUdwUSxtQkFBTyxDQUFDLDJFQUFELENBQVAsQ0FBMkJvUSxpQkFBbkQ7O0FBQ0EsSUFBSUUsYUFBYSxHQUFPdFEsbUJBQU8sQ0FBQywyRUFBRCxDQUFQLENBQTJCc1EsYUFBbkQ7O0FBR0EsSUFBSXdVLFVBQVUsR0FBRyxzQ0FBakI7QUFDQSxJQUFJQyxRQUFRLEdBQUssMkJBQWpCOztBQUdBdG1CLE1BQU0sQ0FBQ0MsT0FBUCxHQUFpQixTQUFTNFMsTUFBVCxDQUFnQmxHLEtBQWhCLEVBQXVCRyxNQUF2QixFQUErQjtBQUM5QyxNQUFJb0csRUFBSjtBQUFBLE1BQVFWLElBQVI7QUFBQSxNQUFjek4sS0FBZDtBQUFBLE1BQXFCVCxHQUFHLEdBQUdxSSxLQUFLLENBQUNySSxHQUFqQztBQUFBLE1BQXNDa0osR0FBRyxHQUFHYixLQUFLLENBQUM4SCxNQUFsRDs7QUFFQSxNQUFJOUgsS0FBSyxDQUFDZSxHQUFOLENBQVVuQixVQUFWLENBQXFCakksR0FBckIsTUFBOEI7QUFBSTtBQUF0QyxJQUErQztBQUFFLGFBQU8sS0FBUDtBQUFlOztBQUVoRSxNQUFJQSxHQUFHLEdBQUcsQ0FBTixHQUFVa0osR0FBZCxFQUFtQjtBQUNqQjBGLE1BQUUsR0FBR3ZHLEtBQUssQ0FBQ2UsR0FBTixDQUFVbkIsVUFBVixDQUFxQmpJLEdBQUcsR0FBRyxDQUEzQixDQUFMOztBQUVBLFFBQUk0TyxFQUFFLEtBQUs7QUFBSztBQUFoQixNQUF5QjtBQUN2Qm5PLGFBQUssR0FBRzRILEtBQUssQ0FBQ2UsR0FBTixDQUFVbkwsS0FBVixDQUFnQitCLEdBQWhCLEVBQXFCUyxLQUFyQixDQUEyQnNoQixVQUEzQixDQUFSOztBQUNBLFlBQUl0aEIsS0FBSixFQUFXO0FBQ1QsY0FBSSxDQUFDK0gsTUFBTCxFQUFhO0FBQ1gwRixnQkFBSSxHQUFHek4sS0FBSyxDQUFDLENBQUQsQ0FBTCxDQUFTLENBQVQsRUFBWTRELFdBQVosT0FBOEIsR0FBOUIsR0FBb0M4SixRQUFRLENBQUMxTixLQUFLLENBQUMsQ0FBRCxDQUFMLENBQVN4QyxLQUFULENBQWUsQ0FBZixDQUFELEVBQW9CLEVBQXBCLENBQTVDLEdBQXNFa1EsUUFBUSxDQUFDMU4sS0FBSyxDQUFDLENBQUQsQ0FBTixFQUFXLEVBQVgsQ0FBckY7QUFDQTRILGlCQUFLLENBQUNnTSxPQUFOLElBQWlCaEgsaUJBQWlCLENBQUNhLElBQUQsQ0FBakIsR0FBMEJYLGFBQWEsQ0FBQ1csSUFBRCxDQUF2QyxHQUFnRFgsYUFBYSxDQUFDLE1BQUQsQ0FBOUU7QUFDRDs7QUFDRGxGLGVBQUssQ0FBQ3JJLEdBQU4sSUFBYVMsS0FBSyxDQUFDLENBQUQsQ0FBTCxDQUFTbkYsTUFBdEI7QUFDQSxpQkFBTyxJQUFQO0FBQ0Q7QUFDRixPQVZELE1BVU87QUFDTG1GLFdBQUssR0FBRzRILEtBQUssQ0FBQ2UsR0FBTixDQUFVbkwsS0FBVixDQUFnQitCLEdBQWhCLEVBQXFCUyxLQUFyQixDQUEyQnVoQixRQUEzQixDQUFSOztBQUNBLFVBQUl2aEIsS0FBSixFQUFXO0FBQ1QsWUFBSXdNLEdBQUcsQ0FBQ2UsUUFBRCxFQUFXdk4sS0FBSyxDQUFDLENBQUQsQ0FBaEIsQ0FBUCxFQUE2QjtBQUMzQixjQUFJLENBQUMrSCxNQUFMLEVBQWE7QUFBRUgsaUJBQUssQ0FBQ2dNLE9BQU4sSUFBaUJyRyxRQUFRLENBQUN2TixLQUFLLENBQUMsQ0FBRCxDQUFOLENBQXpCO0FBQXNDOztBQUNyRDRILGVBQUssQ0FBQ3JJLEdBQU4sSUFBYVMsS0FBSyxDQUFDLENBQUQsQ0FBTCxDQUFTbkYsTUFBdEI7QUFDQSxpQkFBTyxJQUFQO0FBQ0Q7QUFDRjtBQUNGO0FBQ0Y7O0FBRUQsTUFBSSxDQUFDa04sTUFBTCxFQUFhO0FBQUVILFNBQUssQ0FBQ2dNLE9BQU4sSUFBaUIsR0FBakI7QUFBdUI7O0FBQ3RDaE0sT0FBSyxDQUFDckksR0FBTjtBQUNBLFNBQU8sSUFBUDtBQUNELENBakNELEM7Ozs7Ozs7Ozs7OztBQ2RBO0FBRWE7O0FBRWIsSUFBSStPLE9BQU8sR0FBRzlSLG1CQUFPLENBQUMsMkVBQUQsQ0FBUCxDQUEyQjhSLE9BQXpDOztBQUVBLElBQUlrVCxPQUFPLEdBQUcsRUFBZDs7QUFFQSxLQUFLLElBQUkxbUIsQ0FBQyxHQUFHLENBQWIsRUFBZ0JBLENBQUMsR0FBRyxHQUFwQixFQUF5QkEsQ0FBQyxFQUExQixFQUE4QjtBQUFFMG1CLFNBQU8sQ0FBQ3RsQixJQUFSLENBQWEsQ0FBYjtBQUFrQjs7QUFFbEQscUNBQ0d5RSxLQURILENBQ1MsRUFEVCxFQUNhNUMsT0FEYixDQUNxQixVQUFVb1EsRUFBVixFQUFjO0FBQUVxVCxTQUFPLENBQUNyVCxFQUFFLENBQUMzRyxVQUFILENBQWMsQ0FBZCxDQUFELENBQVAsR0FBNEIsQ0FBNUI7QUFBZ0MsQ0FEckU7O0FBSUF2TSxNQUFNLENBQUNDLE9BQVAsR0FBaUIsU0FBU3VtQixNQUFULENBQWdCN1osS0FBaEIsRUFBdUJHLE1BQXZCLEVBQStCO0FBQzlDLE1BQUlvRyxFQUFKO0FBQUEsTUFBUTVPLEdBQUcsR0FBR3FJLEtBQUssQ0FBQ3JJLEdBQXBCO0FBQUEsTUFBeUJrSixHQUFHLEdBQUdiLEtBQUssQ0FBQzhILE1BQXJDOztBQUVBLE1BQUk5SCxLQUFLLENBQUNlLEdBQU4sQ0FBVW5CLFVBQVYsQ0FBcUJqSSxHQUFyQixNQUE4QjtBQUFJO0FBQXRDLElBQStDO0FBQUUsYUFBTyxLQUFQO0FBQWU7O0FBRWhFQSxLQUFHOztBQUVILE1BQUlBLEdBQUcsR0FBR2tKLEdBQVYsRUFBZTtBQUNiMEYsTUFBRSxHQUFHdkcsS0FBSyxDQUFDZSxHQUFOLENBQVVuQixVQUFWLENBQXFCakksR0FBckIsQ0FBTDs7QUFFQSxRQUFJNE8sRUFBRSxHQUFHLEdBQUwsSUFBWXFULE9BQU8sQ0FBQ3JULEVBQUQsQ0FBUCxLQUFnQixDQUFoQyxFQUFtQztBQUNqQyxVQUFJLENBQUNwRyxNQUFMLEVBQWE7QUFBRUgsYUFBSyxDQUFDZ00sT0FBTixJQUFpQmhNLEtBQUssQ0FBQ2UsR0FBTixDQUFVcEosR0FBVixDQUFqQjtBQUFrQzs7QUFDakRxSSxXQUFLLENBQUNySSxHQUFOLElBQWEsQ0FBYjtBQUNBLGFBQU8sSUFBUDtBQUNEOztBQUVELFFBQUk0TyxFQUFFLEtBQUssSUFBWCxFQUFpQjtBQUNmLFVBQUksQ0FBQ3BHLE1BQUwsRUFBYTtBQUNYSCxhQUFLLENBQUMxTCxJQUFOLENBQVcsV0FBWCxFQUF3QixJQUF4QixFQUE4QixDQUE5QjtBQUNEOztBQUVEcUQsU0FBRyxHQUxZLENBTWY7O0FBQ0EsYUFBT0EsR0FBRyxHQUFHa0osR0FBYixFQUFrQjtBQUNoQjBGLFVBQUUsR0FBR3ZHLEtBQUssQ0FBQ2UsR0FBTixDQUFVbkIsVUFBVixDQUFxQmpJLEdBQXJCLENBQUw7O0FBQ0EsWUFBSSxDQUFDK08sT0FBTyxDQUFDSCxFQUFELENBQVosRUFBa0I7QUFBRTtBQUFROztBQUM1QjVPLFdBQUc7QUFDSjs7QUFFRHFJLFdBQUssQ0FBQ3JJLEdBQU4sR0FBWUEsR0FBWjtBQUNBLGFBQU8sSUFBUDtBQUNEO0FBQ0Y7O0FBRUQsTUFBSSxDQUFDd0ksTUFBTCxFQUFhO0FBQUVILFNBQUssQ0FBQ2dNLE9BQU4sSUFBaUIsSUFBakI7QUFBd0I7O0FBQ3ZDaE0sT0FBSyxDQUFDckksR0FBTjtBQUNBLFNBQU8sSUFBUDtBQUNELENBckNELEM7Ozs7Ozs7Ozs7OztBQ2RBO0FBRWE7O0FBR2IsSUFBSThNLFdBQVcsR0FBRzdQLG1CQUFPLENBQUMsK0VBQUQsQ0FBUCxDQUE2QjZQLFdBQS9DOztBQUdBLFNBQVNxVixRQUFULENBQWtCdlQsRUFBbEIsRUFBc0I7QUFDcEI7QUFDQSxNQUFJd1QsRUFBRSxHQUFHeFQsRUFBRSxHQUFHLElBQWQsQ0FGb0IsQ0FFQTs7QUFDcEIsU0FBUXdULEVBQUUsSUFBSTtBQUFJO0FBQVgsS0FBd0JBLEVBQUUsSUFBSTtBQUFJO0FBQXpDO0FBQ0Q7O0FBR0QxbUIsTUFBTSxDQUFDQyxPQUFQLEdBQWlCLFNBQVNvYSxXQUFULENBQXFCMU4sS0FBckIsRUFBNEJHLE1BQTVCLEVBQW9DO0FBQ25ELE1BQUlvRyxFQUFKO0FBQUEsTUFBUW5PLEtBQVI7QUFBQSxNQUFleUksR0FBZjtBQUFBLE1BQW9CTixLQUFwQjtBQUFBLE1BQ0k1SSxHQUFHLEdBQUdxSSxLQUFLLENBQUNySSxHQURoQjs7QUFHQSxNQUFJLENBQUNxSSxLQUFLLENBQUNuQixFQUFOLENBQVNyQyxPQUFULENBQWlCMFAsSUFBdEIsRUFBNEI7QUFBRSxXQUFPLEtBQVA7QUFBZSxHQUpNLENBTW5EOzs7QUFDQXJMLEtBQUcsR0FBR2IsS0FBSyxDQUFDOEgsTUFBWjs7QUFDQSxNQUFJOUgsS0FBSyxDQUFDZSxHQUFOLENBQVVuQixVQUFWLENBQXFCakksR0FBckIsTUFBOEI7QUFBSTtBQUFsQyxLQUNBQSxHQUFHLEdBQUcsQ0FBTixJQUFXa0osR0FEZixFQUNvQjtBQUNsQixXQUFPLEtBQVA7QUFDRCxHQVhrRCxDQWFuRDs7O0FBQ0EwRixJQUFFLEdBQUd2RyxLQUFLLENBQUNlLEdBQU4sQ0FBVW5CLFVBQVYsQ0FBcUJqSSxHQUFHLEdBQUcsQ0FBM0IsQ0FBTDs7QUFDQSxNQUFJNE8sRUFBRSxLQUFLO0FBQUk7QUFBWCxLQUNBQSxFQUFFLEtBQUs7QUFBSTtBQURYLEtBRUFBLEVBQUUsS0FBSztBQUFJO0FBRlgsS0FHQSxDQUFDdVQsUUFBUSxDQUFDdlQsRUFBRCxDQUhiLEVBR21CO0FBQ2pCLFdBQU8sS0FBUDtBQUNEOztBQUVEbk8sT0FBSyxHQUFHNEgsS0FBSyxDQUFDZSxHQUFOLENBQVVuTCxLQUFWLENBQWdCK0IsR0FBaEIsRUFBcUJTLEtBQXJCLENBQTJCcU0sV0FBM0IsQ0FBUjs7QUFDQSxNQUFJLENBQUNyTSxLQUFMLEVBQVk7QUFBRSxXQUFPLEtBQVA7QUFBZTs7QUFFN0IsTUFBSSxDQUFDK0gsTUFBTCxFQUFhO0FBQ1hJLFNBQUssR0FBV1AsS0FBSyxDQUFDMUwsSUFBTixDQUFXLGFBQVgsRUFBMEIsRUFBMUIsRUFBOEIsQ0FBOUIsQ0FBaEI7QUFDQWlNLFNBQUssQ0FBQ3NNLE9BQU4sR0FBZ0I3TSxLQUFLLENBQUNlLEdBQU4sQ0FBVW5MLEtBQVYsQ0FBZ0IrQixHQUFoQixFQUFxQkEsR0FBRyxHQUFHUyxLQUFLLENBQUMsQ0FBRCxDQUFMLENBQVNuRixNQUFwQyxDQUFoQjtBQUNEOztBQUNEK00sT0FBSyxDQUFDckksR0FBTixJQUFhUyxLQUFLLENBQUMsQ0FBRCxDQUFMLENBQVNuRixNQUF0QjtBQUNBLFNBQU8sSUFBUDtBQUNELENBL0JELEM7Ozs7Ozs7Ozs7OztBQ2ZBO0FBRWE7O0FBRWIsSUFBSThULGtCQUFrQixHQUFLblMsbUJBQU8sQ0FBQywyRUFBRCxDQUFQLENBQTJCbVMsa0JBQXREOztBQUNBLElBQUlMLE9BQU8sR0FBZ0I5UixtQkFBTyxDQUFDLDJFQUFELENBQVAsQ0FBMkI4UixPQUF0RDs7QUFHQXJULE1BQU0sQ0FBQ0MsT0FBUCxHQUFpQixTQUFTOFosS0FBVCxDQUFlcE4sS0FBZixFQUFzQkcsTUFBdEIsRUFBOEI7QUFDN0MsTUFBSWdELEtBQUo7QUFBQSxNQUNJMEMsSUFESjtBQUFBLE1BRUlnSCxPQUZKO0FBQUEsTUFHSTZGLEtBSEo7QUFBQSxNQUlJN0ssUUFKSjtBQUFBLE1BS0ltUyxVQUxKO0FBQUEsTUFNSXJpQixHQU5KO0FBQUEsTUFPSXNpQixHQVBKO0FBQUEsTUFRSXRILEdBUko7QUFBQSxNQVNJQyxLQVRKO0FBQUEsTUFVSXJTLEtBVko7QUFBQSxNQVdJckIsTUFYSjtBQUFBLE1BWUl2RCxLQVpKO0FBQUEsTUFhSXVILElBQUksR0FBRyxFQWJYO0FBQUEsTUFjSTZFLE1BQU0sR0FBRy9ILEtBQUssQ0FBQ3JJLEdBZG5CO0FBQUEsTUFlSWtKLEdBQUcsR0FBR2IsS0FBSyxDQUFDOEgsTUFmaEI7O0FBaUJBLE1BQUk5SCxLQUFLLENBQUNlLEdBQU4sQ0FBVW5CLFVBQVYsQ0FBcUJJLEtBQUssQ0FBQ3JJLEdBQTNCLE1BQW9DO0FBQUk7QUFBNUMsSUFBcUQ7QUFBRSxhQUFPLEtBQVA7QUFBZTs7QUFDdEUsTUFBSXFJLEtBQUssQ0FBQ2UsR0FBTixDQUFVbkIsVUFBVixDQUFxQkksS0FBSyxDQUFDckksR0FBTixHQUFZLENBQWpDLE1BQXdDO0FBQUk7QUFBaEQsSUFBeUQ7QUFBRSxhQUFPLEtBQVA7QUFBZTs7QUFFMUVxaUIsWUFBVSxHQUFHaGEsS0FBSyxDQUFDckksR0FBTixHQUFZLENBQXpCO0FBQ0FrUSxVQUFRLEdBQUc3SCxLQUFLLENBQUNuQixFQUFOLENBQVNzSixPQUFULENBQWlCZixjQUFqQixDQUFnQ3BILEtBQWhDLEVBQXVDQSxLQUFLLENBQUNySSxHQUFOLEdBQVksQ0FBbkQsRUFBc0QsS0FBdEQsQ0FBWCxDQXRCNkMsQ0F3QjdDOztBQUNBLE1BQUlrUSxRQUFRLEdBQUcsQ0FBZixFQUFrQjtBQUFFLFdBQU8sS0FBUDtBQUFlOztBQUVuQ2xRLEtBQUcsR0FBR2tRLFFBQVEsR0FBRyxDQUFqQjs7QUFDQSxNQUFJbFEsR0FBRyxHQUFHa0osR0FBTixJQUFhYixLQUFLLENBQUNlLEdBQU4sQ0FBVW5CLFVBQVYsQ0FBcUJqSSxHQUFyQixNQUE4QjtBQUFJO0FBQW5ELElBQTREO0FBQzFEO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQUEsU0FBRzs7QUFDSCxhQUFPQSxHQUFHLEdBQUdrSixHQUFiLEVBQWtCbEosR0FBRyxFQUFyQixFQUF5QjtBQUN2QmtPLFlBQUksR0FBRzdGLEtBQUssQ0FBQ2UsR0FBTixDQUFVbkIsVUFBVixDQUFxQmpJLEdBQXJCLENBQVA7O0FBQ0EsWUFBSSxDQUFDK08sT0FBTyxDQUFDYixJQUFELENBQVIsSUFBa0JBLElBQUksS0FBSyxJQUEvQixFQUFxQztBQUFFO0FBQVE7QUFDaEQ7O0FBQ0QsVUFBSWxPLEdBQUcsSUFBSWtKLEdBQVgsRUFBZ0I7QUFBRSxlQUFPLEtBQVA7QUFBZSxPQVp5QixDQWMxRDtBQUNBOzs7QUFDQWxGLFdBQUssR0FBR2hFLEdBQVI7QUFDQWdiLFNBQUcsR0FBRzNTLEtBQUssQ0FBQ25CLEVBQU4sQ0FBU3NKLE9BQVQsQ0FBaUJkLG9CQUFqQixDQUFzQ3JILEtBQUssQ0FBQ2UsR0FBNUMsRUFBaURwSixHQUFqRCxFQUFzRHFJLEtBQUssQ0FBQzhILE1BQTVELENBQU47O0FBQ0EsVUFBSTZLLEdBQUcsQ0FBQ2xMLEVBQVIsRUFBWTtBQUNWdkUsWUFBSSxHQUFHbEQsS0FBSyxDQUFDbkIsRUFBTixDQUFTa0ssYUFBVCxDQUF1QjRKLEdBQUcsQ0FBQzliLEdBQTNCLENBQVA7O0FBQ0EsWUFBSW1KLEtBQUssQ0FBQ25CLEVBQU4sQ0FBU2dLLFlBQVQsQ0FBc0IzRixJQUF0QixDQUFKLEVBQWlDO0FBQy9CdkwsYUFBRyxHQUFHZ2IsR0FBRyxDQUFDaGIsR0FBVjtBQUNELFNBRkQsTUFFTztBQUNMdUwsY0FBSSxHQUFHLEVBQVA7QUFDRDtBQUNGLE9BekJ5RCxDQTJCMUQ7QUFDQTs7O0FBQ0F2SCxXQUFLLEdBQUdoRSxHQUFSOztBQUNBLGFBQU9BLEdBQUcsR0FBR2tKLEdBQWIsRUFBa0JsSixHQUFHLEVBQXJCLEVBQXlCO0FBQ3ZCa08sWUFBSSxHQUFHN0YsS0FBSyxDQUFDZSxHQUFOLENBQVVuQixVQUFWLENBQXFCakksR0FBckIsQ0FBUDs7QUFDQSxZQUFJLENBQUMrTyxPQUFPLENBQUNiLElBQUQsQ0FBUixJQUFrQkEsSUFBSSxLQUFLLElBQS9CLEVBQXFDO0FBQUU7QUFBUTtBQUNoRCxPQWpDeUQsQ0FtQzFEO0FBQ0E7OztBQUNBOE0sU0FBRyxHQUFHM1MsS0FBSyxDQUFDbkIsRUFBTixDQUFTc0osT0FBVCxDQUFpQmIsY0FBakIsQ0FBZ0N0SCxLQUFLLENBQUNlLEdBQXRDLEVBQTJDcEosR0FBM0MsRUFBZ0RxSSxLQUFLLENBQUM4SCxNQUF0RCxDQUFOOztBQUNBLFVBQUluUSxHQUFHLEdBQUdrSixHQUFOLElBQWFsRixLQUFLLEtBQUtoRSxHQUF2QixJQUE4QmdiLEdBQUcsQ0FBQ2xMLEVBQXRDLEVBQTBDO0FBQ3hDbUwsYUFBSyxHQUFHRCxHQUFHLENBQUM5YixHQUFaO0FBQ0FjLFdBQUcsR0FBR2diLEdBQUcsQ0FBQ2hiLEdBQVYsQ0FGd0MsQ0FJeEM7QUFDQTs7QUFDQSxlQUFPQSxHQUFHLEdBQUdrSixHQUFiLEVBQWtCbEosR0FBRyxFQUFyQixFQUF5QjtBQUN2QmtPLGNBQUksR0FBRzdGLEtBQUssQ0FBQ2UsR0FBTixDQUFVbkIsVUFBVixDQUFxQmpJLEdBQXJCLENBQVA7O0FBQ0EsY0FBSSxDQUFDK08sT0FBTyxDQUFDYixJQUFELENBQVIsSUFBa0JBLElBQUksS0FBSyxJQUEvQixFQUFxQztBQUFFO0FBQVE7QUFDaEQ7QUFDRixPQVZELE1BVU87QUFDTCtNLGFBQUssR0FBRyxFQUFSO0FBQ0Q7O0FBRUQsVUFBSWpiLEdBQUcsSUFBSWtKLEdBQVAsSUFBY2IsS0FBSyxDQUFDZSxHQUFOLENBQVVuQixVQUFWLENBQXFCakksR0FBckIsTUFBOEI7QUFBSTtBQUFwRCxRQUE2RDtBQUMzRHFJLGVBQUssQ0FBQ3JJLEdBQU4sR0FBWW9RLE1BQVo7QUFDQSxpQkFBTyxLQUFQO0FBQ0Q7O0FBQ0RwUSxTQUFHO0FBQ0osS0F6REQsTUF5RE87QUFDTDtBQUNBO0FBQ0E7QUFDQSxRQUFJLE9BQU9xSSxLQUFLLENBQUNaLEdBQU4sQ0FBVXlULFVBQWpCLEtBQWdDLFdBQXBDLEVBQWlEO0FBQUUsYUFBTyxLQUFQO0FBQWU7O0FBRWxFLFFBQUlsYixHQUFHLEdBQUdrSixHQUFOLElBQWFiLEtBQUssQ0FBQ2UsR0FBTixDQUFVbkIsVUFBVixDQUFxQmpJLEdBQXJCLE1BQThCO0FBQUk7QUFBbkQsTUFBNEQ7QUFDMURnRSxhQUFLLEdBQUdoRSxHQUFHLEdBQUcsQ0FBZDtBQUNBQSxXQUFHLEdBQUdxSSxLQUFLLENBQUNuQixFQUFOLENBQVNzSixPQUFULENBQWlCZixjQUFqQixDQUFnQ3BILEtBQWhDLEVBQXVDckksR0FBdkMsQ0FBTjs7QUFDQSxZQUFJQSxHQUFHLElBQUksQ0FBWCxFQUFjO0FBQ1orYSxlQUFLLEdBQUcxUyxLQUFLLENBQUNlLEdBQU4sQ0FBVW5MLEtBQVYsQ0FBZ0IrRixLQUFoQixFQUF1QmhFLEdBQUcsRUFBMUIsQ0FBUjtBQUNELFNBRkQsTUFFTztBQUNMQSxhQUFHLEdBQUdrUSxRQUFRLEdBQUcsQ0FBakI7QUFDRDtBQUNGLE9BUkQsTUFRTztBQUNMbFEsU0FBRyxHQUFHa1EsUUFBUSxHQUFHLENBQWpCO0FBQ0QsS0FoQkksQ0FrQkw7QUFDQTs7O0FBQ0EsUUFBSSxDQUFDNkssS0FBTCxFQUFZO0FBQUVBLFdBQUssR0FBRzFTLEtBQUssQ0FBQ2UsR0FBTixDQUFVbkwsS0FBVixDQUFnQm9rQixVQUFoQixFQUE0Qm5TLFFBQTVCLENBQVI7QUFBZ0Q7O0FBRTlEb1MsT0FBRyxHQUFHamEsS0FBSyxDQUFDWixHQUFOLENBQVV5VCxVQUFWLENBQXFCOUwsa0JBQWtCLENBQUMyTCxLQUFELENBQXZDLENBQU47O0FBQ0EsUUFBSSxDQUFDdUgsR0FBTCxFQUFVO0FBQ1JqYSxXQUFLLENBQUNySSxHQUFOLEdBQVlvUSxNQUFaO0FBQ0EsYUFBTyxLQUFQO0FBQ0Q7O0FBQ0Q3RSxRQUFJLEdBQUcrVyxHQUFHLENBQUMvVyxJQUFYO0FBQ0EwUCxTQUFLLEdBQUdxSCxHQUFHLENBQUNySCxLQUFaO0FBQ0QsR0FsSDRDLENBb0g3QztBQUNBO0FBQ0E7QUFDQTs7O0FBQ0EsTUFBSSxDQUFDelMsTUFBTCxFQUFhO0FBQ1gwTSxXQUFPLEdBQUc3TSxLQUFLLENBQUNlLEdBQU4sQ0FBVW5MLEtBQVYsQ0FBZ0Jva0IsVUFBaEIsRUFBNEJuUyxRQUE1QixDQUFWO0FBRUE3SCxTQUFLLENBQUNuQixFQUFOLENBQVNtSixNQUFULENBQWdCaUIsS0FBaEIsQ0FDRTRELE9BREYsRUFFRTdNLEtBQUssQ0FBQ25CLEVBRlIsRUFHRW1CLEtBQUssQ0FBQ1osR0FIUixFQUlFRixNQUFNLEdBQUcsRUFKWDtBQU9BcUIsU0FBSyxHQUFZUCxLQUFLLENBQUMxTCxJQUFOLENBQVcsT0FBWCxFQUFvQixLQUFwQixFQUEyQixDQUEzQixDQUFqQjtBQUNBaU0sU0FBSyxDQUFDNEMsS0FBTixHQUFpQkEsS0FBSyxHQUFHLENBQUUsQ0FBRSxLQUFGLEVBQVNELElBQVQsQ0FBRixFQUFtQixDQUFFLEtBQUYsRUFBUyxFQUFULENBQW5CLENBQXpCO0FBQ0EzQyxTQUFLLENBQUMrTSxRQUFOLEdBQWlCcE8sTUFBakI7QUFDQXFCLFNBQUssQ0FBQ3NNLE9BQU4sR0FBaUJBLE9BQWpCOztBQUVBLFFBQUkrRixLQUFKLEVBQVc7QUFDVHpQLFdBQUssQ0FBQzdPLElBQU4sQ0FBVyxDQUFFLE9BQUYsRUFBV3NlLEtBQVgsQ0FBWDtBQUNEO0FBQ0Y7O0FBRUQ1UyxPQUFLLENBQUNySSxHQUFOLEdBQVlBLEdBQVo7QUFDQXFJLE9BQUssQ0FBQzhILE1BQU4sR0FBZWpILEdBQWY7QUFDQSxTQUFPLElBQVA7QUFDRCxDQS9JRCxDOzs7Ozs7Ozs7Ozs7QUNSQTtBQUVhOztBQUViLElBQUlrRyxrQkFBa0IsR0FBS25TLG1CQUFPLENBQUMsMkVBQUQsQ0FBUCxDQUEyQm1TLGtCQUF0RDs7QUFDQSxJQUFJTCxPQUFPLEdBQWdCOVIsbUJBQU8sQ0FBQywyRUFBRCxDQUFQLENBQTJCOFIsT0FBdEQ7O0FBR0FyVCxNQUFNLENBQUNDLE9BQVAsR0FBaUIsU0FBUzBILElBQVQsQ0FBY2dGLEtBQWQsRUFBcUJHLE1BQXJCLEVBQTZCO0FBQzVDLE1BQUlnRCxLQUFKO0FBQUEsTUFDSTBDLElBREo7QUFBQSxNQUVJNk0sS0FGSjtBQUFBLE1BR0k3SyxRQUhKO0FBQUEsTUFJSW1TLFVBSko7QUFBQSxNQUtJcmlCLEdBTEo7QUFBQSxNQU1JZ2IsR0FOSjtBQUFBLE1BT0lzSCxHQVBKO0FBQUEsTUFRSXJILEtBUko7QUFBQSxNQVNJclMsS0FUSjtBQUFBLE1BVUkyQyxJQUFJLEdBQUcsRUFWWDtBQUFBLE1BV0k2RSxNQUFNLEdBQUcvSCxLQUFLLENBQUNySSxHQVhuQjtBQUFBLE1BWUlrSixHQUFHLEdBQUdiLEtBQUssQ0FBQzhILE1BWmhCO0FBQUEsTUFhSW5NLEtBQUssR0FBR3FFLEtBQUssQ0FBQ3JJLEdBYmxCO0FBQUEsTUFjSXVpQixjQUFjLEdBQUcsSUFkckI7O0FBZ0JBLE1BQUlsYSxLQUFLLENBQUNlLEdBQU4sQ0FBVW5CLFVBQVYsQ0FBcUJJLEtBQUssQ0FBQ3JJLEdBQTNCLE1BQW9DO0FBQUk7QUFBNUMsSUFBcUQ7QUFBRSxhQUFPLEtBQVA7QUFBZTs7QUFFdEVxaUIsWUFBVSxHQUFHaGEsS0FBSyxDQUFDckksR0FBTixHQUFZLENBQXpCO0FBQ0FrUSxVQUFRLEdBQUc3SCxLQUFLLENBQUNuQixFQUFOLENBQVNzSixPQUFULENBQWlCZixjQUFqQixDQUFnQ3BILEtBQWhDLEVBQXVDQSxLQUFLLENBQUNySSxHQUE3QyxFQUFrRCxJQUFsRCxDQUFYLENBcEI0QyxDQXNCNUM7O0FBQ0EsTUFBSWtRLFFBQVEsR0FBRyxDQUFmLEVBQWtCO0FBQUUsV0FBTyxLQUFQO0FBQWU7O0FBRW5DbFEsS0FBRyxHQUFHa1EsUUFBUSxHQUFHLENBQWpCOztBQUNBLE1BQUlsUSxHQUFHLEdBQUdrSixHQUFOLElBQWFiLEtBQUssQ0FBQ2UsR0FBTixDQUFVbkIsVUFBVixDQUFxQmpJLEdBQXJCLE1BQThCO0FBQUk7QUFBbkQsSUFBNEQ7QUFDMUQ7QUFDQTtBQUNBO0FBRUE7QUFDQXVpQixvQkFBYyxHQUFHLEtBQWpCLENBTjBELENBUTFEO0FBQ0E7O0FBQ0F2aUIsU0FBRzs7QUFDSCxhQUFPQSxHQUFHLEdBQUdrSixHQUFiLEVBQWtCbEosR0FBRyxFQUFyQixFQUF5QjtBQUN2QmtPLFlBQUksR0FBRzdGLEtBQUssQ0FBQ2UsR0FBTixDQUFVbkIsVUFBVixDQUFxQmpJLEdBQXJCLENBQVA7O0FBQ0EsWUFBSSxDQUFDK08sT0FBTyxDQUFDYixJQUFELENBQVIsSUFBa0JBLElBQUksS0FBSyxJQUEvQixFQUFxQztBQUFFO0FBQVE7QUFDaEQ7O0FBQ0QsVUFBSWxPLEdBQUcsSUFBSWtKLEdBQVgsRUFBZ0I7QUFBRSxlQUFPLEtBQVA7QUFBZSxPQWZ5QixDQWlCMUQ7QUFDQTs7O0FBQ0FsRixXQUFLLEdBQUdoRSxHQUFSO0FBQ0FnYixTQUFHLEdBQUczUyxLQUFLLENBQUNuQixFQUFOLENBQVNzSixPQUFULENBQWlCZCxvQkFBakIsQ0FBc0NySCxLQUFLLENBQUNlLEdBQTVDLEVBQWlEcEosR0FBakQsRUFBc0RxSSxLQUFLLENBQUM4SCxNQUE1RCxDQUFOOztBQUNBLFVBQUk2SyxHQUFHLENBQUNsTCxFQUFSLEVBQVk7QUFDVnZFLFlBQUksR0FBR2xELEtBQUssQ0FBQ25CLEVBQU4sQ0FBU2tLLGFBQVQsQ0FBdUI0SixHQUFHLENBQUM5YixHQUEzQixDQUFQOztBQUNBLFlBQUltSixLQUFLLENBQUNuQixFQUFOLENBQVNnSyxZQUFULENBQXNCM0YsSUFBdEIsQ0FBSixFQUFpQztBQUMvQnZMLGFBQUcsR0FBR2diLEdBQUcsQ0FBQ2hiLEdBQVY7QUFDRCxTQUZELE1BRU87QUFDTHVMLGNBQUksR0FBRyxFQUFQO0FBQ0Q7QUFDRixPQTVCeUQsQ0E4QjFEO0FBQ0E7OztBQUNBdkgsV0FBSyxHQUFHaEUsR0FBUjs7QUFDQSxhQUFPQSxHQUFHLEdBQUdrSixHQUFiLEVBQWtCbEosR0FBRyxFQUFyQixFQUF5QjtBQUN2QmtPLFlBQUksR0FBRzdGLEtBQUssQ0FBQ2UsR0FBTixDQUFVbkIsVUFBVixDQUFxQmpJLEdBQXJCLENBQVA7O0FBQ0EsWUFBSSxDQUFDK08sT0FBTyxDQUFDYixJQUFELENBQVIsSUFBa0JBLElBQUksS0FBSyxJQUEvQixFQUFxQztBQUFFO0FBQVE7QUFDaEQsT0FwQ3lELENBc0MxRDtBQUNBOzs7QUFDQThNLFNBQUcsR0FBRzNTLEtBQUssQ0FBQ25CLEVBQU4sQ0FBU3NKLE9BQVQsQ0FBaUJiLGNBQWpCLENBQWdDdEgsS0FBSyxDQUFDZSxHQUF0QyxFQUEyQ3BKLEdBQTNDLEVBQWdEcUksS0FBSyxDQUFDOEgsTUFBdEQsQ0FBTjs7QUFDQSxVQUFJblEsR0FBRyxHQUFHa0osR0FBTixJQUFhbEYsS0FBSyxLQUFLaEUsR0FBdkIsSUFBOEJnYixHQUFHLENBQUNsTCxFQUF0QyxFQUEwQztBQUN4Q21MLGFBQUssR0FBR0QsR0FBRyxDQUFDOWIsR0FBWjtBQUNBYyxXQUFHLEdBQUdnYixHQUFHLENBQUNoYixHQUFWLENBRndDLENBSXhDO0FBQ0E7O0FBQ0EsZUFBT0EsR0FBRyxHQUFHa0osR0FBYixFQUFrQmxKLEdBQUcsRUFBckIsRUFBeUI7QUFDdkJrTyxjQUFJLEdBQUc3RixLQUFLLENBQUNlLEdBQU4sQ0FBVW5CLFVBQVYsQ0FBcUJqSSxHQUFyQixDQUFQOztBQUNBLGNBQUksQ0FBQytPLE9BQU8sQ0FBQ2IsSUFBRCxDQUFSLElBQWtCQSxJQUFJLEtBQUssSUFBL0IsRUFBcUM7QUFBRTtBQUFRO0FBQ2hEO0FBQ0YsT0FWRCxNQVVPO0FBQ0wrTSxhQUFLLEdBQUcsRUFBUjtBQUNEOztBQUVELFVBQUlqYixHQUFHLElBQUlrSixHQUFQLElBQWNiLEtBQUssQ0FBQ2UsR0FBTixDQUFVbkIsVUFBVixDQUFxQmpJLEdBQXJCLE1BQThCO0FBQUk7QUFBcEQsUUFBNkQ7QUFDM0Q7QUFDQXVpQix3QkFBYyxHQUFHLElBQWpCO0FBQ0Q7O0FBQ0R2aUIsU0FBRztBQUNKOztBQUVELE1BQUl1aUIsY0FBSixFQUFvQjtBQUNsQjtBQUNBO0FBQ0E7QUFDQSxRQUFJLE9BQU9sYSxLQUFLLENBQUNaLEdBQU4sQ0FBVXlULFVBQWpCLEtBQWdDLFdBQXBDLEVBQWlEO0FBQUUsYUFBTyxLQUFQO0FBQWU7O0FBRWxFLFFBQUlsYixHQUFHLEdBQUdrSixHQUFOLElBQWFiLEtBQUssQ0FBQ2UsR0FBTixDQUFVbkIsVUFBVixDQUFxQmpJLEdBQXJCLE1BQThCO0FBQUk7QUFBbkQsTUFBNEQ7QUFDMURnRSxhQUFLLEdBQUdoRSxHQUFHLEdBQUcsQ0FBZDtBQUNBQSxXQUFHLEdBQUdxSSxLQUFLLENBQUNuQixFQUFOLENBQVNzSixPQUFULENBQWlCZixjQUFqQixDQUFnQ3BILEtBQWhDLEVBQXVDckksR0FBdkMsQ0FBTjs7QUFDQSxZQUFJQSxHQUFHLElBQUksQ0FBWCxFQUFjO0FBQ1orYSxlQUFLLEdBQUcxUyxLQUFLLENBQUNlLEdBQU4sQ0FBVW5MLEtBQVYsQ0FBZ0IrRixLQUFoQixFQUF1QmhFLEdBQUcsRUFBMUIsQ0FBUjtBQUNELFNBRkQsTUFFTztBQUNMQSxhQUFHLEdBQUdrUSxRQUFRLEdBQUcsQ0FBakI7QUFDRDtBQUNGLE9BUkQsTUFRTztBQUNMbFEsU0FBRyxHQUFHa1EsUUFBUSxHQUFHLENBQWpCO0FBQ0QsS0FoQmlCLENBa0JsQjtBQUNBOzs7QUFDQSxRQUFJLENBQUM2SyxLQUFMLEVBQVk7QUFBRUEsV0FBSyxHQUFHMVMsS0FBSyxDQUFDZSxHQUFOLENBQVVuTCxLQUFWLENBQWdCb2tCLFVBQWhCLEVBQTRCblMsUUFBNUIsQ0FBUjtBQUFnRDs7QUFFOURvUyxPQUFHLEdBQUdqYSxLQUFLLENBQUNaLEdBQU4sQ0FBVXlULFVBQVYsQ0FBcUI5TCxrQkFBa0IsQ0FBQzJMLEtBQUQsQ0FBdkMsQ0FBTjs7QUFDQSxRQUFJLENBQUN1SCxHQUFMLEVBQVU7QUFDUmphLFdBQUssQ0FBQ3JJLEdBQU4sR0FBWW9RLE1BQVo7QUFDQSxhQUFPLEtBQVA7QUFDRDs7QUFDRDdFLFFBQUksR0FBRytXLEdBQUcsQ0FBQy9XLElBQVg7QUFDQTBQLFNBQUssR0FBR3FILEdBQUcsQ0FBQ3JILEtBQVo7QUFDRCxHQXJIMkMsQ0F1SDVDO0FBQ0E7QUFDQTtBQUNBOzs7QUFDQSxNQUFJLENBQUN6UyxNQUFMLEVBQWE7QUFDWEgsU0FBSyxDQUFDckksR0FBTixHQUFZcWlCLFVBQVo7QUFDQWhhLFNBQUssQ0FBQzhILE1BQU4sR0FBZUQsUUFBZjtBQUVBdEgsU0FBSyxHQUFVUCxLQUFLLENBQUMxTCxJQUFOLENBQVcsV0FBWCxFQUF3QixHQUF4QixFQUE2QixDQUE3QixDQUFmO0FBQ0FpTSxTQUFLLENBQUM0QyxLQUFOLEdBQWVBLEtBQUssR0FBRyxDQUFFLENBQUUsTUFBRixFQUFVRCxJQUFWLENBQUYsQ0FBdkI7O0FBQ0EsUUFBSTBQLEtBQUosRUFBVztBQUNUelAsV0FBSyxDQUFDN08sSUFBTixDQUFXLENBQUUsT0FBRixFQUFXc2UsS0FBWCxDQUFYO0FBQ0Q7O0FBRUQ1UyxTQUFLLENBQUNuQixFQUFOLENBQVNtSixNQUFULENBQWdCdkcsUUFBaEIsQ0FBeUJ6QixLQUF6QjtBQUVBTyxTQUFLLEdBQVVQLEtBQUssQ0FBQzFMLElBQU4sQ0FBVyxZQUFYLEVBQXlCLEdBQXpCLEVBQThCLENBQUMsQ0FBL0IsQ0FBZjtBQUNEOztBQUVEMEwsT0FBSyxDQUFDckksR0FBTixHQUFZQSxHQUFaO0FBQ0FxSSxPQUFLLENBQUM4SCxNQUFOLEdBQWVqSCxHQUFmO0FBQ0EsU0FBTyxJQUFQO0FBQ0QsQ0E3SUQsQzs7Ozs7Ozs7Ozs7O0FDUkE7QUFFYTs7QUFFYixJQUFJNkYsT0FBTyxHQUFHOVIsbUJBQU8sQ0FBQywyRUFBRCxDQUFQLENBQTJCOFIsT0FBekM7O0FBR0FyVCxNQUFNLENBQUNDLE9BQVAsR0FBaUIsU0FBUzZtQixPQUFULENBQWlCbmEsS0FBakIsRUFBd0JHLE1BQXhCLEVBQWdDO0FBQy9DLE1BQUlpYSxJQUFKO0FBQUEsTUFBVXZaLEdBQVY7QUFBQSxNQUFlbEosR0FBRyxHQUFHcUksS0FBSyxDQUFDckksR0FBM0I7O0FBRUEsTUFBSXFJLEtBQUssQ0FBQ2UsR0FBTixDQUFVbkIsVUFBVixDQUFxQmpJLEdBQXJCLE1BQThCO0FBQUk7QUFBdEMsSUFBZ0Q7QUFBRSxhQUFPLEtBQVA7QUFBZTs7QUFFakV5aUIsTUFBSSxHQUFHcGEsS0FBSyxDQUFDZ00sT0FBTixDQUFjL1ksTUFBZCxHQUF1QixDQUE5QjtBQUNBNE4sS0FBRyxHQUFHYixLQUFLLENBQUM4SCxNQUFaLENBTitDLENBUS9DO0FBQ0E7QUFDQTtBQUNBOztBQUNBLE1BQUksQ0FBQzNILE1BQUwsRUFBYTtBQUNYLFFBQUlpYSxJQUFJLElBQUksQ0FBUixJQUFhcGEsS0FBSyxDQUFDZ00sT0FBTixDQUFjcE0sVUFBZCxDQUF5QndhLElBQXpCLE1BQW1DLElBQXBELEVBQTBEO0FBQ3hELFVBQUlBLElBQUksSUFBSSxDQUFSLElBQWFwYSxLQUFLLENBQUNnTSxPQUFOLENBQWNwTSxVQUFkLENBQXlCd2EsSUFBSSxHQUFHLENBQWhDLE1BQXVDLElBQXhELEVBQThEO0FBQzVEcGEsYUFBSyxDQUFDZ00sT0FBTixHQUFnQmhNLEtBQUssQ0FBQ2dNLE9BQU4sQ0FBY2xWLE9BQWQsQ0FBc0IsS0FBdEIsRUFBNkIsRUFBN0IsQ0FBaEI7QUFDQWtKLGFBQUssQ0FBQzFMLElBQU4sQ0FBVyxXQUFYLEVBQXdCLElBQXhCLEVBQThCLENBQTlCO0FBQ0QsT0FIRCxNQUdPO0FBQ0wwTCxhQUFLLENBQUNnTSxPQUFOLEdBQWdCaE0sS0FBSyxDQUFDZ00sT0FBTixDQUFjcFcsS0FBZCxDQUFvQixDQUFwQixFQUF1QixDQUFDLENBQXhCLENBQWhCO0FBQ0FvSyxhQUFLLENBQUMxTCxJQUFOLENBQVcsV0FBWCxFQUF3QixJQUF4QixFQUE4QixDQUE5QjtBQUNEO0FBRUYsS0FURCxNQVNPO0FBQ0wwTCxXQUFLLENBQUMxTCxJQUFOLENBQVcsV0FBWCxFQUF3QixJQUF4QixFQUE4QixDQUE5QjtBQUNEO0FBQ0Y7O0FBRURxRCxLQUFHLEdBM0I0QyxDQTZCL0M7O0FBQ0EsU0FBT0EsR0FBRyxHQUFHa0osR0FBTixJQUFhNkYsT0FBTyxDQUFDMUcsS0FBSyxDQUFDZSxHQUFOLENBQVVuQixVQUFWLENBQXFCakksR0FBckIsQ0FBRCxDQUEzQixFQUF3RDtBQUFFQSxPQUFHO0FBQUs7O0FBRWxFcUksT0FBSyxDQUFDckksR0FBTixHQUFZQSxHQUFaO0FBQ0EsU0FBTyxJQUFQO0FBQ0QsQ0FsQ0QsQzs7Ozs7Ozs7Ozs7O0FDUEE7QUFFYTs7QUFHYixJQUFJbWIsS0FBSyxHQUFZbGUsbUJBQU8sQ0FBQyw2REFBRCxDQUE1Qjs7QUFDQSxJQUFJK1IsWUFBWSxHQUFLL1IsbUJBQU8sQ0FBQywyRUFBRCxDQUFQLENBQTJCK1IsWUFBaEQ7O0FBQ0EsSUFBSUUsV0FBVyxHQUFNalMsbUJBQU8sQ0FBQywyRUFBRCxDQUFQLENBQTJCaVMsV0FBaEQ7O0FBQ0EsSUFBSUMsY0FBYyxHQUFHbFMsbUJBQU8sQ0FBQywyRUFBRCxDQUFQLENBQTJCa1MsY0FBaEQ7O0FBR0EsU0FBU3VULFdBQVQsQ0FBcUJ0WixHQUFyQixFQUEwQmxDLEVBQTFCLEVBQThCTyxHQUE5QixFQUFtQ3NNLFNBQW5DLEVBQThDO0FBQzVDLE9BQUszSyxHQUFMLEdBQVdBLEdBQVg7QUFDQSxPQUFLM0IsR0FBTCxHQUFXQSxHQUFYO0FBQ0EsT0FBS1AsRUFBTCxHQUFVQSxFQUFWO0FBQ0EsT0FBS0ssTUFBTCxHQUFjd00sU0FBZDtBQUNBLE9BQUt1TixXQUFMLEdBQW1CN2xCLEtBQUssQ0FBQ3NZLFNBQVMsQ0FBQ3pZLE1BQVgsQ0FBeEI7QUFFQSxPQUFLMEUsR0FBTCxHQUFXLENBQVg7QUFDQSxPQUFLbVEsTUFBTCxHQUFjLEtBQUsvRyxHQUFMLENBQVM5TixNQUF2QjtBQUNBLE9BQUtzVSxLQUFMLEdBQWEsQ0FBYjtBQUNBLE9BQUt5RSxPQUFMLEdBQWUsRUFBZjtBQUNBLE9BQUtzTyxZQUFMLEdBQW9CLENBQXBCLENBWDRDLENBYTVDO0FBQ0E7O0FBQ0EsT0FBS3ZPLEtBQUwsR0FBYSxFQUFiLENBZjRDLENBaUI1Qzs7QUFDQSxPQUFLbU0sVUFBTCxHQUFrQixFQUFsQixDQWxCNEMsQ0FvQjVDOztBQUNBLE9BQUtxQyxnQkFBTCxHQUF3QixFQUF4QjtBQUNELEMsQ0FHRDtBQUNBOzs7QUFDQUYsV0FBVyxDQUFDamxCLFNBQVosQ0FBc0I2VyxXQUF0QixHQUFvQyxZQUFZO0FBQzlDLE1BQUkxTCxLQUFLLEdBQUcsSUFBSXVTLEtBQUosQ0FBVSxNQUFWLEVBQWtCLEVBQWxCLEVBQXNCLENBQXRCLENBQVo7QUFDQXZTLE9BQUssQ0FBQ3NNLE9BQU4sR0FBZ0IsS0FBS2IsT0FBckI7QUFDQXpMLE9BQUssQ0FBQ2dILEtBQU4sR0FBYyxLQUFLK1MsWUFBbkI7QUFDQSxPQUFLcGIsTUFBTCxDQUFZNUssSUFBWixDQUFpQmlNLEtBQWpCO0FBQ0EsT0FBS3lMLE9BQUwsR0FBZSxFQUFmO0FBQ0EsU0FBT3pMLEtBQVA7QUFDRCxDQVBELEMsQ0FVQTtBQUNBO0FBQ0E7OztBQUNBOFosV0FBVyxDQUFDamxCLFNBQVosQ0FBc0JkLElBQXRCLEdBQTZCLFVBQVV3WixJQUFWLEVBQWdCMUwsR0FBaEIsRUFBcUIvQyxPQUFyQixFQUE4QjtBQUN6RCxNQUFJLEtBQUsyTSxPQUFULEVBQWtCO0FBQ2hCLFNBQUtDLFdBQUw7QUFDRDs7QUFFRCxNQUFJMUwsS0FBSyxHQUFHLElBQUl1UyxLQUFKLENBQVVoRixJQUFWLEVBQWdCMUwsR0FBaEIsRUFBcUIvQyxPQUFyQixDQUFaO0FBQ0EsTUFBSW1iLFVBQVUsR0FBRyxJQUFqQjs7QUFFQSxNQUFJbmIsT0FBTyxHQUFHLENBQWQsRUFBaUI7QUFDZjtBQUNBLFNBQUtrSSxLQUFMO0FBQ0EsU0FBSzJRLFVBQUwsR0FBa0IsS0FBS3FDLGdCQUFMLENBQXNCRSxHQUF0QixFQUFsQjtBQUNEOztBQUVEbGEsT0FBSyxDQUFDZ0gsS0FBTixHQUFjLEtBQUtBLEtBQW5COztBQUVBLE1BQUlsSSxPQUFPLEdBQUcsQ0FBZCxFQUFpQjtBQUNmO0FBQ0EsU0FBS2tJLEtBQUw7O0FBQ0EsU0FBS2dULGdCQUFMLENBQXNCam1CLElBQXRCLENBQTJCLEtBQUs0akIsVUFBaEM7O0FBQ0EsU0FBS0EsVUFBTCxHQUFrQixFQUFsQjtBQUNBc0MsY0FBVSxHQUFHO0FBQUV0QyxnQkFBVSxFQUFFLEtBQUtBO0FBQW5CLEtBQWI7QUFDRDs7QUFFRCxPQUFLb0MsWUFBTCxHQUFvQixLQUFLL1MsS0FBekI7QUFDQSxPQUFLckksTUFBTCxDQUFZNUssSUFBWixDQUFpQmlNLEtBQWpCO0FBQ0EsT0FBSzBZLFdBQUwsQ0FBaUIza0IsSUFBakIsQ0FBc0JrbUIsVUFBdEI7QUFDQSxTQUFPamEsS0FBUDtBQUNELENBNUJELEMsQ0ErQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDQThaLFdBQVcsQ0FBQ2psQixTQUFaLENBQXNCZ2tCLFVBQXRCLEdBQW1DLFVBQVV6ZCxLQUFWLEVBQWlCK2UsWUFBakIsRUFBK0I7QUFDaEUsTUFBSS9pQixHQUFHLEdBQUdnRSxLQUFWO0FBQUEsTUFBaUI4YSxRQUFqQjtBQUFBLE1BQTJCQyxRQUEzQjtBQUFBLE1BQXFDaUUsS0FBckM7QUFBQSxNQUE0Q3RCLFFBQTVDO0FBQUEsTUFBc0RDLFNBQXREO0FBQUEsTUFDSXpDLGdCQURKO0FBQUEsTUFDc0JGLGVBRHRCO0FBQUEsTUFFSUcsZ0JBRko7QUFBQSxNQUVzQkYsZUFGdEI7QUFBQSxNQUdJZ0UsYUFBYSxHQUFHLElBSHBCO0FBQUEsTUFJSUMsY0FBYyxHQUFHLElBSnJCO0FBQUEsTUFLSWhhLEdBQUcsR0FBRyxLQUFLaUgsTUFMZjtBQUFBLE1BTUlwSSxNQUFNLEdBQUcsS0FBS3FCLEdBQUwsQ0FBU25CLFVBQVQsQ0FBb0JqRSxLQUFwQixDQU5iLENBRGdFLENBU2hFOztBQUNBOGEsVUFBUSxHQUFHOWEsS0FBSyxHQUFHLENBQVIsR0FBWSxLQUFLb0YsR0FBTCxDQUFTbkIsVUFBVCxDQUFvQmpFLEtBQUssR0FBRyxDQUE1QixDQUFaLEdBQTZDLElBQXhEOztBQUVBLFNBQU9oRSxHQUFHLEdBQUdrSixHQUFOLElBQWEsS0FBS0UsR0FBTCxDQUFTbkIsVUFBVCxDQUFvQmpJLEdBQXBCLE1BQTZCK0gsTUFBakQsRUFBeUQ7QUFBRS9ILE9BQUc7QUFBSzs7QUFFbkVnakIsT0FBSyxHQUFHaGpCLEdBQUcsR0FBR2dFLEtBQWQsQ0FkZ0UsQ0FnQmhFOztBQUNBK2EsVUFBUSxHQUFHL2UsR0FBRyxHQUFHa0osR0FBTixHQUFZLEtBQUtFLEdBQUwsQ0FBU25CLFVBQVQsQ0FBb0JqSSxHQUFwQixDQUFaLEdBQXVDLElBQWxEO0FBRUFnZixpQkFBZSxHQUFHN1AsY0FBYyxDQUFDMlAsUUFBRCxDQUFkLElBQTRCNVAsV0FBVyxDQUFDeEIsTUFBTSxDQUFDQyxZQUFQLENBQW9CbVIsUUFBcEIsQ0FBRCxDQUF6RDtBQUNBRyxpQkFBZSxHQUFHOVAsY0FBYyxDQUFDNFAsUUFBRCxDQUFkLElBQTRCN1AsV0FBVyxDQUFDeEIsTUFBTSxDQUFDQyxZQUFQLENBQW9Cb1IsUUFBcEIsQ0FBRCxDQUF6RDtBQUVBRyxrQkFBZ0IsR0FBR2xRLFlBQVksQ0FBQzhQLFFBQUQsQ0FBL0I7QUFDQUssa0JBQWdCLEdBQUduUSxZQUFZLENBQUMrUCxRQUFELENBQS9COztBQUVBLE1BQUlJLGdCQUFKLEVBQXNCO0FBQ3BCOEQsaUJBQWEsR0FBRyxLQUFoQjtBQUNELEdBRkQsTUFFTyxJQUFJaEUsZUFBSixFQUFxQjtBQUMxQixRQUFJLEVBQUVDLGdCQUFnQixJQUFJRixlQUF0QixDQUFKLEVBQTRDO0FBQzFDaUUsbUJBQWEsR0FBRyxLQUFoQjtBQUNEO0FBQ0Y7O0FBRUQsTUFBSS9ELGdCQUFKLEVBQXNCO0FBQ3BCZ0Usa0JBQWMsR0FBRyxLQUFqQjtBQUNELEdBRkQsTUFFTyxJQUFJbEUsZUFBSixFQUFxQjtBQUMxQixRQUFJLEVBQUVHLGdCQUFnQixJQUFJRixlQUF0QixDQUFKLEVBQTRDO0FBQzFDaUUsb0JBQWMsR0FBRyxLQUFqQjtBQUNEO0FBQ0Y7O0FBRUQsTUFBSSxDQUFDSCxZQUFMLEVBQW1CO0FBQ2pCckIsWUFBUSxHQUFJdUIsYUFBYSxLQUFNLENBQUNDLGNBQUQsSUFBbUJsRSxlQUF6QixDQUF6QjtBQUNBMkMsYUFBUyxHQUFHdUIsY0FBYyxLQUFLLENBQUNELGFBQUQsSUFBbUJoRSxlQUF4QixDQUExQjtBQUNELEdBSEQsTUFHTztBQUNMeUMsWUFBUSxHQUFJdUIsYUFBWjtBQUNBdEIsYUFBUyxHQUFHdUIsY0FBWjtBQUNEOztBQUVELFNBQU87QUFDTHhCLFlBQVEsRUFBR0EsUUFETjtBQUVMQyxhQUFTLEVBQUVBLFNBRk47QUFHTHJtQixVQUFNLEVBQUswbkI7QUFITixHQUFQO0FBS0QsQ0F0REQsQyxDQXlEQTs7O0FBQ0FOLFdBQVcsQ0FBQ2psQixTQUFaLENBQXNCMGQsS0FBdEIsR0FBOEJBLEtBQTlCO0FBR0F6ZixNQUFNLENBQUNDLE9BQVAsR0FBaUIrbUIsV0FBakIsQzs7Ozs7Ozs7Ozs7O0FDckpBO0FBQ0E7Q0FJQTtBQUNBOztBQUNBaG5CLE1BQU0sQ0FBQ0MsT0FBUCxDQUFlbU8sUUFBZixHQUEwQixTQUFTcVosYUFBVCxDQUF1QjlhLEtBQXZCLEVBQThCRyxNQUE5QixFQUFzQztBQUM5RCxNQUFJak4sQ0FBSjtBQUFBLE1BQU9pbUIsT0FBUDtBQUFBLE1BQWdCNVksS0FBaEI7QUFBQSxNQUF1QnZOLEdBQXZCO0FBQUEsTUFBNEJ1VCxFQUE1QjtBQUFBLE1BQ0k1SyxLQUFLLEdBQUdxRSxLQUFLLENBQUNySSxHQURsQjtBQUFBLE1BRUkrSCxNQUFNLEdBQUdNLEtBQUssQ0FBQ2UsR0FBTixDQUFVbkIsVUFBVixDQUFxQmpFLEtBQXJCLENBRmI7O0FBSUEsTUFBSXdFLE1BQUosRUFBWTtBQUFFLFdBQU8sS0FBUDtBQUFlOztBQUU3QixNQUFJVCxNQUFNLEtBQUs7QUFBSTtBQUFuQixJQUE0QjtBQUFFLGFBQU8sS0FBUDtBQUFlOztBQUU3Q3laLFNBQU8sR0FBR25aLEtBQUssQ0FBQ29aLFVBQU4sQ0FBaUJwWixLQUFLLENBQUNySSxHQUF2QixFQUE0QixJQUE1QixDQUFWO0FBQ0EzRSxLQUFHLEdBQUdtbUIsT0FBTyxDQUFDbG1CLE1BQWQ7QUFDQXNULElBQUUsR0FBR2xCLE1BQU0sQ0FBQ0MsWUFBUCxDQUFvQjVGLE1BQXBCLENBQUw7O0FBRUEsTUFBSTFNLEdBQUcsR0FBRyxDQUFWLEVBQWE7QUFBRSxXQUFPLEtBQVA7QUFBZTs7QUFFOUIsTUFBSUEsR0FBRyxHQUFHLENBQVYsRUFBYTtBQUNYdU4sU0FBSyxHQUFXUCxLQUFLLENBQUMxTCxJQUFOLENBQVcsTUFBWCxFQUFtQixFQUFuQixFQUF1QixDQUF2QixDQUFoQjtBQUNBaU0sU0FBSyxDQUFDc00sT0FBTixHQUFnQnRHLEVBQWhCO0FBQ0F2VCxPQUFHO0FBQ0o7O0FBRUQsT0FBS0UsQ0FBQyxHQUFHLENBQVQsRUFBWUEsQ0FBQyxHQUFHRixHQUFoQixFQUFxQkUsQ0FBQyxJQUFJLENBQTFCLEVBQTZCO0FBQzNCcU4sU0FBSyxHQUFXUCxLQUFLLENBQUMxTCxJQUFOLENBQVcsTUFBWCxFQUFtQixFQUFuQixFQUF1QixDQUF2QixDQUFoQjtBQUNBaU0sU0FBSyxDQUFDc00sT0FBTixHQUFnQnRHLEVBQUUsR0FBR0EsRUFBckI7QUFFQXZHLFNBQUssQ0FBQ2tZLFVBQU4sQ0FBaUI1akIsSUFBakIsQ0FBc0I7QUFDcEJvTCxZQUFNLEVBQUVBLE1BRFk7QUFFcEJ6TSxZQUFNLEVBQUUsQ0FGWTtBQUVUO0FBQ1g0bEIsVUFBSSxFQUFJM2xCLENBSFk7QUFJcEJxTixXQUFLLEVBQUdQLEtBQUssQ0FBQ2QsTUFBTixDQUFhak0sTUFBYixHQUFzQixDQUpWO0FBS3BCMkksU0FBRyxFQUFLLENBQUMsQ0FMVztBQU1wQmtkLFVBQUksRUFBSUssT0FBTyxDQUFDRSxRQU5JO0FBT3BCVCxXQUFLLEVBQUdPLE9BQU8sQ0FBQ0c7QUFQSSxLQUF0QjtBQVNEOztBQUVEdFosT0FBSyxDQUFDckksR0FBTixJQUFhd2hCLE9BQU8sQ0FBQ2xtQixNQUFyQjtBQUVBLFNBQU8sSUFBUDtBQUNELENBdkNEOztBQTBDQSxTQUFTNlksV0FBVCxDQUFxQjlMLEtBQXJCLEVBQTRCa1ksVUFBNUIsRUFBd0M7QUFDdEMsTUFBSWhsQixDQUFKO0FBQUEsTUFBT3doQixDQUFQO0FBQUEsTUFDSTZFLFVBREo7QUFBQSxNQUVJQyxRQUZKO0FBQUEsTUFHSWpaLEtBSEo7QUFBQSxNQUlJd2EsV0FBVyxHQUFHLEVBSmxCO0FBQUEsTUFLSWxhLEdBQUcsR0FBR3FYLFVBQVUsQ0FBQ2psQixNQUxyQjs7QUFPQSxPQUFLQyxDQUFDLEdBQUcsQ0FBVCxFQUFZQSxDQUFDLEdBQUcyTixHQUFoQixFQUFxQjNOLENBQUMsRUFBdEIsRUFBMEI7QUFDeEJxbUIsY0FBVSxHQUFHckIsVUFBVSxDQUFDaGxCLENBQUQsQ0FBdkI7O0FBRUEsUUFBSXFtQixVQUFVLENBQUM3WixNQUFYLEtBQXNCO0FBQUk7QUFBOUIsTUFBdUM7QUFDckM7QUFDRDs7QUFFRCxRQUFJNlosVUFBVSxDQUFDM2QsR0FBWCxLQUFtQixDQUFDLENBQXhCLEVBQTJCO0FBQ3pCO0FBQ0Q7O0FBRUQ0ZCxZQUFRLEdBQUd0QixVQUFVLENBQUNxQixVQUFVLENBQUMzZCxHQUFaLENBQXJCO0FBRUEyRSxTQUFLLEdBQVdQLEtBQUssQ0FBQ2QsTUFBTixDQUFhcWEsVUFBVSxDQUFDaFosS0FBeEIsQ0FBaEI7QUFDQUEsU0FBSyxDQUFDdU4sSUFBTixHQUFnQixRQUFoQjtBQUNBdk4sU0FBSyxDQUFDNkIsR0FBTixHQUFnQixHQUFoQjtBQUNBN0IsU0FBSyxDQUFDbEIsT0FBTixHQUFnQixDQUFoQjtBQUNBa0IsU0FBSyxDQUFDRCxNQUFOLEdBQWdCLElBQWhCO0FBQ0FDLFNBQUssQ0FBQ3NNLE9BQU4sR0FBZ0IsRUFBaEI7QUFFQXRNLFNBQUssR0FBV1AsS0FBSyxDQUFDZCxNQUFOLENBQWFzYSxRQUFRLENBQUNqWixLQUF0QixDQUFoQjtBQUNBQSxTQUFLLENBQUN1TixJQUFOLEdBQWdCLFNBQWhCO0FBQ0F2TixTQUFLLENBQUM2QixHQUFOLEdBQWdCLEdBQWhCO0FBQ0E3QixTQUFLLENBQUNsQixPQUFOLEdBQWdCLENBQUMsQ0FBakI7QUFDQWtCLFNBQUssQ0FBQ0QsTUFBTixHQUFnQixJQUFoQjtBQUNBQyxTQUFLLENBQUNzTSxPQUFOLEdBQWdCLEVBQWhCOztBQUVBLFFBQUk3TSxLQUFLLENBQUNkLE1BQU4sQ0FBYXNhLFFBQVEsQ0FBQ2paLEtBQVQsR0FBaUIsQ0FBOUIsRUFBaUN1TixJQUFqQyxLQUEwQyxNQUExQyxJQUNBOU4sS0FBSyxDQUFDZCxNQUFOLENBQWFzYSxRQUFRLENBQUNqWixLQUFULEdBQWlCLENBQTlCLEVBQWlDc00sT0FBakMsS0FBNkMsR0FEakQsRUFDc0Q7QUFFcERrTyxpQkFBVyxDQUFDem1CLElBQVosQ0FBaUJrbEIsUUFBUSxDQUFDalosS0FBVCxHQUFpQixDQUFsQztBQUNEO0FBQ0YsR0F4Q3FDLENBMEN0QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNBLFNBQU93YSxXQUFXLENBQUM5bkIsTUFBbkIsRUFBMkI7QUFDekJDLEtBQUMsR0FBRzZuQixXQUFXLENBQUNOLEdBQVosRUFBSjtBQUNBL0YsS0FBQyxHQUFHeGhCLENBQUMsR0FBRyxDQUFSOztBQUVBLFdBQU93aEIsQ0FBQyxHQUFHMVUsS0FBSyxDQUFDZCxNQUFOLENBQWFqTSxNQUFqQixJQUEyQitNLEtBQUssQ0FBQ2QsTUFBTixDQUFhd1YsQ0FBYixFQUFnQjVHLElBQWhCLEtBQXlCLFNBQTNELEVBQXNFO0FBQ3BFNEcsT0FBQztBQUNGOztBQUVEQSxLQUFDOztBQUVELFFBQUl4aEIsQ0FBQyxLQUFLd2hCLENBQVYsRUFBYTtBQUNYblUsV0FBSyxHQUFHUCxLQUFLLENBQUNkLE1BQU4sQ0FBYXdWLENBQWIsQ0FBUjtBQUNBMVUsV0FBSyxDQUFDZCxNQUFOLENBQWF3VixDQUFiLElBQWtCMVUsS0FBSyxDQUFDZCxNQUFOLENBQWFoTSxDQUFiLENBQWxCO0FBQ0E4TSxXQUFLLENBQUNkLE1BQU4sQ0FBYWhNLENBQWIsSUFBa0JxTixLQUFsQjtBQUNEO0FBQ0Y7QUFDRixDLENBR0Q7QUFDQTs7O0FBQ0FsTixNQUFNLENBQUNDLE9BQVAsQ0FBZXdZLFdBQWYsR0FBNkIsU0FBU2dQLGFBQVQsQ0FBdUI5YSxLQUF2QixFQUE4QjtBQUN6RCxNQUFJZ1osSUFBSjtBQUFBLE1BQ0lDLFdBQVcsR0FBR2paLEtBQUssQ0FBQ2laLFdBRHhCO0FBQUEsTUFFSXBZLEdBQUcsR0FBR2IsS0FBSyxDQUFDaVosV0FBTixDQUFrQmhtQixNQUY1QjtBQUlBNlksYUFBVyxDQUFDOUwsS0FBRCxFQUFRQSxLQUFLLENBQUNrWSxVQUFkLENBQVg7O0FBRUEsT0FBS2MsSUFBSSxHQUFHLENBQVosRUFBZUEsSUFBSSxHQUFHblksR0FBdEIsRUFBMkJtWSxJQUFJLEVBQS9CLEVBQW1DO0FBQ2pDLFFBQUlDLFdBQVcsQ0FBQ0QsSUFBRCxDQUFYLElBQXFCQyxXQUFXLENBQUNELElBQUQsQ0FBWCxDQUFrQmQsVUFBM0MsRUFBdUQ7QUFDckRwTSxpQkFBVyxDQUFDOUwsS0FBRCxFQUFRaVosV0FBVyxDQUFDRCxJQUFELENBQVgsQ0FBa0JkLFVBQTFCLENBQVg7QUFDRDtBQUNGO0FBQ0YsQ0FaRCxDOzs7Ozs7Ozs7Ozs7QUN0SEE7QUFDQTtDQUtBO0FBQ0E7QUFFQTtBQUVBO0FBQ0E7O0FBQ0EsU0FBUzhDLGdCQUFULENBQTBCelUsRUFBMUIsRUFBOEI7QUFDNUIsVUFBUUEsRUFBUjtBQUNFLFNBQUs7QUFBSTtBQUFUO0FBQ0EsU0FBSztBQUFJO0FBQVQ7QUFDQSxTQUFLO0FBQUk7QUFBVDtBQUNBLFNBQUs7QUFBSTtBQUFUO0FBQ0EsU0FBSztBQUFJO0FBQVQ7QUFDQSxTQUFLO0FBQUk7QUFBVDtBQUNBLFNBQUs7QUFBSTtBQUFUO0FBQ0EsU0FBSztBQUFJO0FBQVQ7QUFDQSxTQUFLO0FBQUk7QUFBVDtBQUNBLFNBQUs7QUFBSTtBQUFUO0FBQ0EsU0FBSztBQUFJO0FBQVQ7QUFDQSxTQUFLO0FBQUk7QUFBVDtBQUNBLFNBQUs7QUFBSTtBQUFUO0FBQ0EsU0FBSztBQUFJO0FBQVQ7QUFDQSxTQUFLO0FBQUk7QUFBVDtBQUNBLFNBQUs7QUFBSTtBQUFUO0FBQ0EsU0FBSztBQUFJO0FBQVQ7QUFDQSxTQUFLO0FBQUk7QUFBVDtBQUNBLFNBQUs7QUFBSTtBQUFUO0FBQ0EsU0FBSztBQUFJO0FBQVQ7QUFDQSxTQUFLO0FBQUk7QUFBVDtBQUNBLFNBQUs7QUFBSTtBQUFUO0FBQ0EsU0FBSztBQUFJO0FBQVQ7QUFDRSxhQUFPLElBQVA7O0FBQ0Y7QUFDRSxhQUFPLEtBQVA7QUExQko7QUE0QkQ7O0FBRURsVCxNQUFNLENBQUNDLE9BQVAsR0FBaUIsU0FBU29FLElBQVQsQ0FBY3NJLEtBQWQsRUFBcUJHLE1BQXJCLEVBQTZCO0FBQzVDLE1BQUl4SSxHQUFHLEdBQUdxSSxLQUFLLENBQUNySSxHQUFoQjs7QUFFQSxTQUFPQSxHQUFHLEdBQUdxSSxLQUFLLENBQUM4SCxNQUFaLElBQXNCLENBQUNrVCxnQkFBZ0IsQ0FBQ2hiLEtBQUssQ0FBQ2UsR0FBTixDQUFVbkIsVUFBVixDQUFxQmpJLEdBQXJCLENBQUQsQ0FBOUMsRUFBMkU7QUFDekVBLE9BQUc7QUFDSjs7QUFFRCxNQUFJQSxHQUFHLEtBQUtxSSxLQUFLLENBQUNySSxHQUFsQixFQUF1QjtBQUFFLFdBQU8sS0FBUDtBQUFlOztBQUV4QyxNQUFJLENBQUN3SSxNQUFMLEVBQWE7QUFBRUgsU0FBSyxDQUFDZ00sT0FBTixJQUFpQmhNLEtBQUssQ0FBQ2UsR0FBTixDQUFVbkwsS0FBVixDQUFnQm9LLEtBQUssQ0FBQ3JJLEdBQXRCLEVBQTJCQSxHQUEzQixDQUFqQjtBQUFtRDs7QUFFbEVxSSxPQUFLLENBQUNySSxHQUFOLEdBQVlBLEdBQVo7QUFFQSxTQUFPLElBQVA7QUFDRCxDQWRELEMsQ0FnQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ2xFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ2E7O0FBR2J0RSxNQUFNLENBQUNDLE9BQVAsR0FBaUIsU0FBUzJuQixhQUFULENBQXVCamIsS0FBdkIsRUFBOEI7QUFDN0MsTUFBSWdaLElBQUo7QUFBQSxNQUFVL0ksSUFBVjtBQUFBLE1BQ0kxSSxLQUFLLEdBQUcsQ0FEWjtBQUFBLE1BRUlySSxNQUFNLEdBQUdjLEtBQUssQ0FBQ2QsTUFGbkI7QUFBQSxNQUdJMkIsR0FBRyxHQUFHYixLQUFLLENBQUNkLE1BQU4sQ0FBYWpNLE1BSHZCOztBQUtBLE9BQUsrbEIsSUFBSSxHQUFHL0ksSUFBSSxHQUFHLENBQW5CLEVBQXNCK0ksSUFBSSxHQUFHblksR0FBN0IsRUFBa0NtWSxJQUFJLEVBQXRDLEVBQTBDO0FBQ3hDO0FBQ0E7QUFDQSxRQUFJOVosTUFBTSxDQUFDOFosSUFBRCxDQUFOLENBQWEzWixPQUFiLEdBQXVCLENBQTNCLEVBQThCa0ksS0FBSyxHQUhLLENBR0Q7O0FBQ3ZDckksVUFBTSxDQUFDOFosSUFBRCxDQUFOLENBQWF6UixLQUFiLEdBQXFCQSxLQUFyQjtBQUNBLFFBQUlySSxNQUFNLENBQUM4WixJQUFELENBQU4sQ0FBYTNaLE9BQWIsR0FBdUIsQ0FBM0IsRUFBOEJrSSxLQUFLLEdBTEssQ0FLRDs7QUFFdkMsUUFBSXJJLE1BQU0sQ0FBQzhaLElBQUQsQ0FBTixDQUFhbEwsSUFBYixLQUFzQixNQUF0QixJQUNBa0wsSUFBSSxHQUFHLENBQVAsR0FBV25ZLEdBRFgsSUFFQTNCLE1BQU0sQ0FBQzhaLElBQUksR0FBRyxDQUFSLENBQU4sQ0FBaUJsTCxJQUFqQixLQUEwQixNQUY5QixFQUVzQztBQUVwQztBQUNBNU8sWUFBTSxDQUFDOFosSUFBSSxHQUFHLENBQVIsQ0FBTixDQUFpQm5NLE9BQWpCLEdBQTJCM04sTUFBTSxDQUFDOFosSUFBRCxDQUFOLENBQWFuTSxPQUFiLEdBQXVCM04sTUFBTSxDQUFDOFosSUFBSSxHQUFHLENBQVIsQ0FBTixDQUFpQm5NLE9BQW5FO0FBQ0QsS0FORCxNQU1PO0FBQ0wsVUFBSW1NLElBQUksS0FBSy9JLElBQWIsRUFBbUI7QUFBRS9RLGNBQU0sQ0FBQytRLElBQUQsQ0FBTixHQUFlL1EsTUFBTSxDQUFDOFosSUFBRCxDQUFyQjtBQUE4Qjs7QUFFbkQvSSxVQUFJO0FBQ0w7QUFDRjs7QUFFRCxNQUFJK0ksSUFBSSxLQUFLL0ksSUFBYixFQUFtQjtBQUNqQi9RLFVBQU0sQ0FBQ2pNLE1BQVAsR0FBZ0JnZCxJQUFoQjtBQUNEO0FBQ0YsQ0E3QkQsQzs7Ozs7Ozs7Ozs7O0FDWEE7QUFFYTtBQUdiOzs7O0FBSUE7Ozs7OztBQUtBLFNBQVM2QyxLQUFULENBQWVoRixJQUFmLEVBQXFCMUwsR0FBckIsRUFBMEIvQyxPQUExQixFQUFtQztBQUNqQzs7Ozs7QUFLQSxPQUFLeU8sSUFBTCxHQUFnQkEsSUFBaEI7QUFFQTs7Ozs7O0FBS0EsT0FBSzFMLEdBQUwsR0FBZ0JBLEdBQWhCO0FBRUE7Ozs7OztBQUtBLE9BQUtlLEtBQUwsR0FBZ0IsSUFBaEI7QUFFQTs7Ozs7O0FBS0EsT0FBSy9ILEdBQUwsR0FBZ0IsSUFBaEI7QUFFQTs7Ozs7Ozs7OztBQVNBLE9BQUtpRSxPQUFMLEdBQWdCQSxPQUFoQjtBQUVBOzs7Ozs7QUFLQSxPQUFLa0ksS0FBTCxHQUFnQixDQUFoQjtBQUVBOzs7Ozs7QUFLQSxPQUFLK0YsUUFBTCxHQUFnQixJQUFoQjtBQUVBOzs7Ozs7O0FBTUEsT0FBS1QsT0FBTCxHQUFnQixFQUFoQjtBQUVBOzs7Ozs7QUFLQSxPQUFLdk0sTUFBTCxHQUFnQixFQUFoQjtBQUVBOzs7Ozs7QUFLQSxPQUFLa0IsSUFBTCxHQUFnQixFQUFoQjtBQUVBOzs7Ozs7QUFLQSxPQUFLMFosSUFBTCxHQUFnQixJQUFoQjtBQUVBOzs7Ozs7O0FBTUEsT0FBSzNaLEtBQUwsR0FBZ0IsS0FBaEI7QUFFQTs7Ozs7OztBQU1BLE9BQUtzTSxNQUFMLEdBQWdCLEtBQWhCO0FBQ0Q7QUFHRDs7Ozs7OztBQUtBaUYsS0FBSyxDQUFDMWQsU0FBTixDQUFnQmdPLFNBQWhCLEdBQTRCLFNBQVNBLFNBQVQsQ0FBbUJ2TixJQUFuQixFQUF5QjtBQUNuRCxNQUFJc04sS0FBSixFQUFXalEsQ0FBWCxFQUFjRixHQUFkOztBQUVBLE1BQUksQ0FBQyxLQUFLbVEsS0FBVixFQUFpQjtBQUFFLFdBQU8sQ0FBQyxDQUFSO0FBQVk7O0FBRS9CQSxPQUFLLEdBQUcsS0FBS0EsS0FBYjs7QUFFQSxPQUFLalEsQ0FBQyxHQUFHLENBQUosRUFBT0YsR0FBRyxHQUFHbVEsS0FBSyxDQUFDbFEsTUFBeEIsRUFBZ0NDLENBQUMsR0FBR0YsR0FBcEMsRUFBeUNFLENBQUMsRUFBMUMsRUFBOEM7QUFDNUMsUUFBSWlRLEtBQUssQ0FBQ2pRLENBQUQsQ0FBTCxDQUFTLENBQVQsTUFBZ0IyQyxJQUFwQixFQUEwQjtBQUFFLGFBQU8zQyxDQUFQO0FBQVc7QUFDeEM7O0FBQ0QsU0FBTyxDQUFDLENBQVI7QUFDRCxDQVhEO0FBY0E7Ozs7Ozs7QUFLQTRmLEtBQUssQ0FBQzFkLFNBQU4sQ0FBZ0JrSyxRQUFoQixHQUEyQixTQUFTQSxRQUFULENBQWtCNmIsUUFBbEIsRUFBNEI7QUFDckQsTUFBSSxLQUFLaFksS0FBVCxFQUFnQjtBQUNkLFNBQUtBLEtBQUwsQ0FBVzdPLElBQVgsQ0FBZ0I2bUIsUUFBaEI7QUFDRCxHQUZELE1BRU87QUFDTCxTQUFLaFksS0FBTCxHQUFhLENBQUVnWSxRQUFGLENBQWI7QUFDRDtBQUNGLENBTkQ7QUFTQTs7Ozs7OztBQUtBckksS0FBSyxDQUFDMWQsU0FBTixDQUFnQmdtQixPQUFoQixHQUEwQixTQUFTQSxPQUFULENBQWlCdmxCLElBQWpCLEVBQXVCdEIsS0FBdkIsRUFBOEI7QUFDdEQsTUFBSW9KLEdBQUcsR0FBRyxLQUFLeUYsU0FBTCxDQUFldk4sSUFBZixDQUFWO0FBQUEsTUFDSXNsQixRQUFRLEdBQUcsQ0FBRXRsQixJQUFGLEVBQVF0QixLQUFSLENBRGY7O0FBR0EsTUFBSW9KLEdBQUcsR0FBRyxDQUFWLEVBQWE7QUFDWCxTQUFLMkIsUUFBTCxDQUFjNmIsUUFBZDtBQUNELEdBRkQsTUFFTztBQUNMLFNBQUtoWSxLQUFMLENBQVd4RixHQUFYLElBQWtCd2QsUUFBbEI7QUFDRDtBQUNGLENBVEQ7QUFZQTs7Ozs7OztBQUtBckksS0FBSyxDQUFDMWQsU0FBTixDQUFnQmltQixPQUFoQixHQUEwQixTQUFTQSxPQUFULENBQWlCeGxCLElBQWpCLEVBQXVCO0FBQy9DLE1BQUk4SCxHQUFHLEdBQUcsS0FBS3lGLFNBQUwsQ0FBZXZOLElBQWYsQ0FBVjtBQUFBLE1BQWdDdEIsS0FBSyxHQUFHLElBQXhDOztBQUNBLE1BQUlvSixHQUFHLElBQUksQ0FBWCxFQUFjO0FBQ1pwSixTQUFLLEdBQUcsS0FBSzRPLEtBQUwsQ0FBV3hGLEdBQVgsRUFBZ0IsQ0FBaEIsQ0FBUjtBQUNEOztBQUNELFNBQU9wSixLQUFQO0FBQ0QsQ0FORDtBQVNBOzs7Ozs7OztBQU1BdWUsS0FBSyxDQUFDMWQsU0FBTixDQUFnQmttQixRQUFoQixHQUEyQixTQUFTQSxRQUFULENBQWtCemxCLElBQWxCLEVBQXdCdEIsS0FBeEIsRUFBK0I7QUFDeEQsTUFBSW9KLEdBQUcsR0FBRyxLQUFLeUYsU0FBTCxDQUFldk4sSUFBZixDQUFWOztBQUVBLE1BQUk4SCxHQUFHLEdBQUcsQ0FBVixFQUFhO0FBQ1gsU0FBSzJCLFFBQUwsQ0FBYyxDQUFFekosSUFBRixFQUFRdEIsS0FBUixDQUFkO0FBQ0QsR0FGRCxNQUVPO0FBQ0wsU0FBSzRPLEtBQUwsQ0FBV3hGLEdBQVgsRUFBZ0IsQ0FBaEIsSUFBcUIsS0FBS3dGLEtBQUwsQ0FBV3hGLEdBQVgsRUFBZ0IsQ0FBaEIsSUFBcUIsR0FBckIsR0FBMkJwSixLQUFoRDtBQUNEO0FBQ0YsQ0FSRDs7QUFXQWxCLE1BQU0sQ0FBQ0MsT0FBUCxHQUFpQndmLEtBQWpCLEM7Ozs7Ozs7Ozs7OztBQ25NYTtBQUdiOztBQUVBLElBQUl5SSxXQUFXLEdBQUcsRUFBbEI7O0FBRUEsU0FBU0MsY0FBVCxDQUF3QkMsT0FBeEIsRUFBaUM7QUFDL0IsTUFBSXZvQixDQUFKO0FBQUEsTUFBT3FULEVBQVA7QUFBQSxNQUFXd0YsS0FBSyxHQUFHd1AsV0FBVyxDQUFDRSxPQUFELENBQTlCOztBQUNBLE1BQUkxUCxLQUFKLEVBQVc7QUFBRSxXQUFPQSxLQUFQO0FBQWU7O0FBRTVCQSxPQUFLLEdBQUd3UCxXQUFXLENBQUNFLE9BQUQsQ0FBWCxHQUF1QixFQUEvQjs7QUFFQSxPQUFLdm9CLENBQUMsR0FBRyxDQUFULEVBQVlBLENBQUMsR0FBRyxHQUFoQixFQUFxQkEsQ0FBQyxFQUF0QixFQUEwQjtBQUN4QnFULE1BQUUsR0FBR2xCLE1BQU0sQ0FBQ0MsWUFBUCxDQUFvQnBTLENBQXBCLENBQUw7QUFDQTZZLFNBQUssQ0FBQ3pYLElBQU4sQ0FBV2lTLEVBQVg7QUFDRDs7QUFFRCxPQUFLclQsQ0FBQyxHQUFHLENBQVQsRUFBWUEsQ0FBQyxHQUFHdW9CLE9BQU8sQ0FBQ3hvQixNQUF4QixFQUFnQ0MsQ0FBQyxFQUFqQyxFQUFxQztBQUNuQ3FULE1BQUUsR0FBR2tWLE9BQU8sQ0FBQzdiLFVBQVIsQ0FBbUIxTSxDQUFuQixDQUFMO0FBQ0E2WSxTQUFLLENBQUN4RixFQUFELENBQUwsR0FBWSxNQUFNLENBQUMsTUFBTUEsRUFBRSxDQUFDN1EsUUFBSCxDQUFZLEVBQVosRUFBZ0JzUixXQUFoQixFQUFQLEVBQXNDcFIsS0FBdEMsQ0FBNEMsQ0FBQyxDQUE3QyxDQUFsQjtBQUNEOztBQUVELFNBQU9tVyxLQUFQO0FBQ0QsQyxDQUdEO0FBQ0E7OztBQUNBLFNBQVNyQyxNQUFULENBQWdCZ1MsTUFBaEIsRUFBd0JELE9BQXhCLEVBQWlDO0FBQy9CLE1BQUkxUCxLQUFKOztBQUVBLE1BQUksT0FBTzBQLE9BQVAsS0FBbUIsUUFBdkIsRUFBaUM7QUFDL0JBLFdBQU8sR0FBRy9SLE1BQU0sQ0FBQ2lTLFlBQWpCO0FBQ0Q7O0FBRUQ1UCxPQUFLLEdBQUd5UCxjQUFjLENBQUNDLE9BQUQsQ0FBdEI7QUFFQSxTQUFPQyxNQUFNLENBQUM1a0IsT0FBUCxDQUFlLG1CQUFmLEVBQW9DLFVBQVM4a0IsR0FBVCxFQUFjO0FBQ3ZELFFBQUkxb0IsQ0FBSjtBQUFBLFFBQU8wWSxDQUFQO0FBQUEsUUFBVWlRLEVBQVY7QUFBQSxRQUFjQyxFQUFkO0FBQUEsUUFBa0JDLEVBQWxCO0FBQUEsUUFBc0JDLEVBQXRCO0FBQUEsUUFBMEJDLEdBQTFCO0FBQUEsUUFDSTVlLE1BQU0sR0FBRyxFQURiOztBQUdBLFNBQUtuSyxDQUFDLEdBQUcsQ0FBSixFQUFPMFksQ0FBQyxHQUFHZ1EsR0FBRyxDQUFDM29CLE1BQXBCLEVBQTRCQyxDQUFDLEdBQUcwWSxDQUFoQyxFQUFtQzFZLENBQUMsSUFBSSxDQUF4QyxFQUEyQztBQUN6QzJvQixRQUFFLEdBQUcvVixRQUFRLENBQUM4VixHQUFHLENBQUNobUIsS0FBSixDQUFVMUMsQ0FBQyxHQUFHLENBQWQsRUFBaUJBLENBQUMsR0FBRyxDQUFyQixDQUFELEVBQTBCLEVBQTFCLENBQWI7O0FBRUEsVUFBSTJvQixFQUFFLEdBQUcsSUFBVCxFQUFlO0FBQ2J4ZSxjQUFNLElBQUkwTyxLQUFLLENBQUM4UCxFQUFELENBQWY7QUFDQTtBQUNEOztBQUVELFVBQUksQ0FBQ0EsRUFBRSxHQUFHLElBQU4sTUFBZ0IsSUFBaEIsSUFBeUIzb0IsQ0FBQyxHQUFHLENBQUosR0FBUTBZLENBQXJDLEVBQXlDO0FBQ3ZDO0FBQ0FrUSxVQUFFLEdBQUdoVyxRQUFRLENBQUM4VixHQUFHLENBQUNobUIsS0FBSixDQUFVMUMsQ0FBQyxHQUFHLENBQWQsRUFBaUJBLENBQUMsR0FBRyxDQUFyQixDQUFELEVBQTBCLEVBQTFCLENBQWI7O0FBRUEsWUFBSSxDQUFDNG9CLEVBQUUsR0FBRyxJQUFOLE1BQWdCLElBQXBCLEVBQTBCO0FBQ3hCRyxhQUFHLEdBQUtKLEVBQUUsSUFBSSxDQUFQLEdBQVksS0FBYixHQUF1QkMsRUFBRSxHQUFHLElBQWxDOztBQUVBLGNBQUlHLEdBQUcsR0FBRyxJQUFWLEVBQWdCO0FBQ2Q1ZSxrQkFBTSxJQUFJLGNBQVY7QUFDRCxXQUZELE1BRU87QUFDTEEsa0JBQU0sSUFBSWdJLE1BQU0sQ0FBQ0MsWUFBUCxDQUFvQjJXLEdBQXBCLENBQVY7QUFDRDs7QUFFRC9vQixXQUFDLElBQUksQ0FBTDtBQUNBO0FBQ0Q7QUFDRjs7QUFFRCxVQUFJLENBQUMyb0IsRUFBRSxHQUFHLElBQU4sTUFBZ0IsSUFBaEIsSUFBeUIzb0IsQ0FBQyxHQUFHLENBQUosR0FBUTBZLENBQXJDLEVBQXlDO0FBQ3ZDO0FBQ0FrUSxVQUFFLEdBQUdoVyxRQUFRLENBQUM4VixHQUFHLENBQUNobUIsS0FBSixDQUFVMUMsQ0FBQyxHQUFHLENBQWQsRUFBaUJBLENBQUMsR0FBRyxDQUFyQixDQUFELEVBQTBCLEVBQTFCLENBQWI7QUFDQTZvQixVQUFFLEdBQUdqVyxRQUFRLENBQUM4VixHQUFHLENBQUNobUIsS0FBSixDQUFVMUMsQ0FBQyxHQUFHLENBQWQsRUFBaUJBLENBQUMsR0FBRyxDQUFyQixDQUFELEVBQTBCLEVBQTFCLENBQWI7O0FBRUEsWUFBSSxDQUFDNG9CLEVBQUUsR0FBRyxJQUFOLE1BQWdCLElBQWhCLElBQXdCLENBQUNDLEVBQUUsR0FBRyxJQUFOLE1BQWdCLElBQTVDLEVBQWtEO0FBQ2hERSxhQUFHLEdBQUtKLEVBQUUsSUFBSSxFQUFQLEdBQWEsTUFBZCxHQUEwQkMsRUFBRSxJQUFJLENBQVAsR0FBWSxLQUFyQyxHQUErQ0MsRUFBRSxHQUFHLElBQTFEOztBQUVBLGNBQUlFLEdBQUcsR0FBRyxLQUFOLElBQWdCQSxHQUFHLElBQUksTUFBUCxJQUFpQkEsR0FBRyxJQUFJLE1BQTVDLEVBQXFEO0FBQ25ENWUsa0JBQU0sSUFBSSxvQkFBVjtBQUNELFdBRkQsTUFFTztBQUNMQSxrQkFBTSxJQUFJZ0ksTUFBTSxDQUFDQyxZQUFQLENBQW9CMlcsR0FBcEIsQ0FBVjtBQUNEOztBQUVEL29CLFdBQUMsSUFBSSxDQUFMO0FBQ0E7QUFDRDtBQUNGOztBQUVELFVBQUksQ0FBQzJvQixFQUFFLEdBQUcsSUFBTixNQUFnQixJQUFoQixJQUF5QjNvQixDQUFDLEdBQUcsQ0FBSixHQUFRMFksQ0FBckMsRUFBeUM7QUFDdkM7QUFDQWtRLFVBQUUsR0FBR2hXLFFBQVEsQ0FBQzhWLEdBQUcsQ0FBQ2htQixLQUFKLENBQVUxQyxDQUFDLEdBQUcsQ0FBZCxFQUFpQkEsQ0FBQyxHQUFHLENBQXJCLENBQUQsRUFBMEIsRUFBMUIsQ0FBYjtBQUNBNm9CLFVBQUUsR0FBR2pXLFFBQVEsQ0FBQzhWLEdBQUcsQ0FBQ2htQixLQUFKLENBQVUxQyxDQUFDLEdBQUcsQ0FBZCxFQUFpQkEsQ0FBQyxHQUFHLENBQXJCLENBQUQsRUFBMEIsRUFBMUIsQ0FBYjtBQUNBOG9CLFVBQUUsR0FBR2xXLFFBQVEsQ0FBQzhWLEdBQUcsQ0FBQ2htQixLQUFKLENBQVUxQyxDQUFDLEdBQUcsRUFBZCxFQUFrQkEsQ0FBQyxHQUFHLEVBQXRCLENBQUQsRUFBNEIsRUFBNUIsQ0FBYjs7QUFFQSxZQUFJLENBQUM0b0IsRUFBRSxHQUFHLElBQU4sTUFBZ0IsSUFBaEIsSUFBd0IsQ0FBQ0MsRUFBRSxHQUFHLElBQU4sTUFBZ0IsSUFBeEMsSUFBZ0QsQ0FBQ0MsRUFBRSxHQUFHLElBQU4sTUFBZ0IsSUFBcEUsRUFBMEU7QUFDeEVDLGFBQUcsR0FBS0osRUFBRSxJQUFJLEVBQVAsR0FBYSxRQUFkLEdBQTRCQyxFQUFFLElBQUksRUFBUCxHQUFhLE9BQXhDLEdBQXFEQyxFQUFFLElBQUksQ0FBUCxHQUFZLEtBQWhFLEdBQTBFQyxFQUFFLEdBQUcsSUFBckY7O0FBRUEsY0FBSUMsR0FBRyxHQUFHLE9BQU4sSUFBaUJBLEdBQUcsR0FBRyxRQUEzQixFQUFxQztBQUNuQzVlLGtCQUFNLElBQUksMEJBQVY7QUFDRCxXQUZELE1BRU87QUFDTDRlLGVBQUcsSUFBSSxPQUFQO0FBQ0E1ZSxrQkFBTSxJQUFJZ0ksTUFBTSxDQUFDQyxZQUFQLENBQW9CLFVBQVUyVyxHQUFHLElBQUksRUFBakIsQ0FBcEIsRUFBMEMsVUFBVUEsR0FBRyxHQUFHLEtBQWhCLENBQTFDLENBQVY7QUFDRDs7QUFFRC9vQixXQUFDLElBQUksQ0FBTDtBQUNBO0FBQ0Q7QUFDRjs7QUFFRG1LLFlBQU0sSUFBSSxRQUFWO0FBQ0Q7O0FBRUQsV0FBT0EsTUFBUDtBQUNELEdBMUVNLENBQVA7QUEyRUQ7O0FBR0RxTSxNQUFNLENBQUNpUyxZQUFQLEdBQXdCLGFBQXhCO0FBQ0FqUyxNQUFNLENBQUN3UyxjQUFQLEdBQXdCLEVBQXhCO0FBR0E3b0IsTUFBTSxDQUFDQyxPQUFQLEdBQWlCb1csTUFBakIsQzs7Ozs7Ozs7Ozs7O0FDeEhhOztBQUdiLElBQUl5UyxXQUFXLEdBQUcsRUFBbEIsQyxDQUdBO0FBQ0E7QUFDQTs7QUFDQSxTQUFTQyxjQUFULENBQXdCWCxPQUF4QixFQUFpQztBQUMvQixNQUFJdm9CLENBQUo7QUFBQSxNQUFPcVQsRUFBUDtBQUFBLE1BQVd3RixLQUFLLEdBQUdvUSxXQUFXLENBQUNWLE9BQUQsQ0FBOUI7O0FBQ0EsTUFBSTFQLEtBQUosRUFBVztBQUFFLFdBQU9BLEtBQVA7QUFBZTs7QUFFNUJBLE9BQUssR0FBR29RLFdBQVcsQ0FBQ1YsT0FBRCxDQUFYLEdBQXVCLEVBQS9COztBQUVBLE9BQUt2b0IsQ0FBQyxHQUFHLENBQVQsRUFBWUEsQ0FBQyxHQUFHLEdBQWhCLEVBQXFCQSxDQUFDLEVBQXRCLEVBQTBCO0FBQ3hCcVQsTUFBRSxHQUFHbEIsTUFBTSxDQUFDQyxZQUFQLENBQW9CcFMsQ0FBcEIsQ0FBTDs7QUFFQSxRQUFJLGNBQWM2QyxJQUFkLENBQW1Cd1EsRUFBbkIsQ0FBSixFQUE0QjtBQUMxQjtBQUNBd0YsV0FBSyxDQUFDelgsSUFBTixDQUFXaVMsRUFBWDtBQUNELEtBSEQsTUFHTztBQUNMd0YsV0FBSyxDQUFDelgsSUFBTixDQUFXLE1BQU0sQ0FBQyxNQUFNcEIsQ0FBQyxDQUFDd0MsUUFBRixDQUFXLEVBQVgsRUFBZXNSLFdBQWYsRUFBUCxFQUFxQ3BSLEtBQXJDLENBQTJDLENBQUMsQ0FBNUMsQ0FBakI7QUFDRDtBQUNGOztBQUVELE9BQUsxQyxDQUFDLEdBQUcsQ0FBVCxFQUFZQSxDQUFDLEdBQUd1b0IsT0FBTyxDQUFDeG9CLE1BQXhCLEVBQWdDQyxDQUFDLEVBQWpDLEVBQXFDO0FBQ25DNlksU0FBSyxDQUFDMFAsT0FBTyxDQUFDN2IsVUFBUixDQUFtQjFNLENBQW5CLENBQUQsQ0FBTCxHQUErQnVvQixPQUFPLENBQUN2b0IsQ0FBRCxDQUF0QztBQUNEOztBQUVELFNBQU82WSxLQUFQO0FBQ0QsQyxDQUdEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDQSxTQUFTekMsTUFBVCxDQUFnQm9TLE1BQWhCLEVBQXdCRCxPQUF4QixFQUFpQ1ksV0FBakMsRUFBOEM7QUFDNUMsTUFBSW5wQixDQUFKO0FBQUEsTUFBTzBZLENBQVA7QUFBQSxNQUFVL0YsSUFBVjtBQUFBLE1BQWdCeVcsUUFBaEI7QUFBQSxNQUEwQnZRLEtBQTFCO0FBQUEsTUFDSTFPLE1BQU0sR0FBRyxFQURiOztBQUdBLE1BQUksT0FBT29lLE9BQVAsS0FBbUIsUUFBdkIsRUFBaUM7QUFDL0I7QUFDQVksZUFBVyxHQUFJWixPQUFmO0FBQ0FBLFdBQU8sR0FBR25TLE1BQU0sQ0FBQ3FTLFlBQWpCO0FBQ0Q7O0FBRUQsTUFBSSxPQUFPVSxXQUFQLEtBQXVCLFdBQTNCLEVBQXdDO0FBQ3RDQSxlQUFXLEdBQUcsSUFBZDtBQUNEOztBQUVEdFEsT0FBSyxHQUFHcVEsY0FBYyxDQUFDWCxPQUFELENBQXRCOztBQUVBLE9BQUt2b0IsQ0FBQyxHQUFHLENBQUosRUFBTzBZLENBQUMsR0FBRzhQLE1BQU0sQ0FBQ3pvQixNQUF2QixFQUErQkMsQ0FBQyxHQUFHMFksQ0FBbkMsRUFBc0MxWSxDQUFDLEVBQXZDLEVBQTJDO0FBQ3pDMlMsUUFBSSxHQUFHNlYsTUFBTSxDQUFDOWIsVUFBUCxDQUFrQjFNLENBQWxCLENBQVA7O0FBRUEsUUFBSW1wQixXQUFXLElBQUl4VyxJQUFJLEtBQUs7QUFBSztBQUE3QixPQUF3QzNTLENBQUMsR0FBRyxDQUFKLEdBQVEwWSxDQUFwRCxFQUF1RDtBQUNyRCxVQUFJLGlCQUFpQjdWLElBQWpCLENBQXNCMmxCLE1BQU0sQ0FBQzlsQixLQUFQLENBQWExQyxDQUFDLEdBQUcsQ0FBakIsRUFBb0JBLENBQUMsR0FBRyxDQUF4QixDQUF0QixDQUFKLEVBQXVEO0FBQ3JEbUssY0FBTSxJQUFJcWUsTUFBTSxDQUFDOWxCLEtBQVAsQ0FBYTFDLENBQWIsRUFBZ0JBLENBQUMsR0FBRyxDQUFwQixDQUFWO0FBQ0FBLFNBQUMsSUFBSSxDQUFMO0FBQ0E7QUFDRDtBQUNGOztBQUVELFFBQUkyUyxJQUFJLEdBQUcsR0FBWCxFQUFnQjtBQUNkeEksWUFBTSxJQUFJME8sS0FBSyxDQUFDbEcsSUFBRCxDQUFmO0FBQ0E7QUFDRDs7QUFFRCxRQUFJQSxJQUFJLElBQUksTUFBUixJQUFrQkEsSUFBSSxJQUFJLE1BQTlCLEVBQXNDO0FBQ3BDLFVBQUlBLElBQUksSUFBSSxNQUFSLElBQWtCQSxJQUFJLElBQUksTUFBMUIsSUFBb0MzUyxDQUFDLEdBQUcsQ0FBSixHQUFRMFksQ0FBaEQsRUFBbUQ7QUFDakQwUSxnQkFBUSxHQUFHWixNQUFNLENBQUM5YixVQUFQLENBQWtCMU0sQ0FBQyxHQUFHLENBQXRCLENBQVg7O0FBQ0EsWUFBSW9wQixRQUFRLElBQUksTUFBWixJQUFzQkEsUUFBUSxJQUFJLE1BQXRDLEVBQThDO0FBQzVDamYsZ0JBQU0sSUFBSWtmLGtCQUFrQixDQUFDYixNQUFNLENBQUN4b0IsQ0FBRCxDQUFOLEdBQVl3b0IsTUFBTSxDQUFDeG9CLENBQUMsR0FBRyxDQUFMLENBQW5CLENBQTVCO0FBQ0FBLFdBQUM7QUFDRDtBQUNEO0FBQ0Y7O0FBQ0RtSyxZQUFNLElBQUksV0FBVjtBQUNBO0FBQ0Q7O0FBRURBLFVBQU0sSUFBSWtmLGtCQUFrQixDQUFDYixNQUFNLENBQUN4b0IsQ0FBRCxDQUFQLENBQTVCO0FBQ0Q7O0FBRUQsU0FBT21LLE1BQVA7QUFDRDs7QUFFRGlNLE1BQU0sQ0FBQ3FTLFlBQVAsR0FBd0Isc0JBQXhCO0FBQ0FyUyxNQUFNLENBQUM0UyxjQUFQLEdBQXdCLFdBQXhCO0FBR0E3b0IsTUFBTSxDQUFDQyxPQUFQLEdBQWlCZ1csTUFBakIsQzs7Ozs7Ozs7Ozs7O0FDaEdhOztBQUdialcsTUFBTSxDQUFDQyxPQUFQLEdBQWlCLFNBQVNpVyxNQUFULENBQWdCbk4sR0FBaEIsRUFBcUI7QUFDcEMsTUFBSWlCLE1BQU0sR0FBRyxFQUFiO0FBRUFBLFFBQU0sSUFBSWpCLEdBQUcsQ0FBQytNLFFBQUosSUFBZ0IsRUFBMUI7QUFDQTlMLFFBQU0sSUFBSWpCLEdBQUcsQ0FBQ29nQixPQUFKLEdBQWMsSUFBZCxHQUFxQixFQUEvQjtBQUNBbmYsUUFBTSxJQUFJakIsR0FBRyxDQUFDcWdCLElBQUosR0FBV3JnQixHQUFHLENBQUNxZ0IsSUFBSixHQUFXLEdBQXRCLEdBQTRCLEVBQXRDOztBQUVBLE1BQUlyZ0IsR0FBRyxDQUFDOE0sUUFBSixJQUFnQjlNLEdBQUcsQ0FBQzhNLFFBQUosQ0FBYTlMLE9BQWIsQ0FBcUIsR0FBckIsTUFBOEIsQ0FBQyxDQUFuRCxFQUFzRDtBQUNwRDtBQUNBQyxVQUFNLElBQUksTUFBTWpCLEdBQUcsQ0FBQzhNLFFBQVYsR0FBcUIsR0FBL0I7QUFDRCxHQUhELE1BR087QUFDTDdMLFVBQU0sSUFBSWpCLEdBQUcsQ0FBQzhNLFFBQUosSUFBZ0IsRUFBMUI7QUFDRDs7QUFFRDdMLFFBQU0sSUFBSWpCLEdBQUcsQ0FBQ3NnQixJQUFKLEdBQVcsTUFBTXRnQixHQUFHLENBQUNzZ0IsSUFBckIsR0FBNEIsRUFBdEM7QUFDQXJmLFFBQU0sSUFBSWpCLEdBQUcsQ0FBQ3VnQixRQUFKLElBQWdCLEVBQTFCO0FBQ0F0ZixRQUFNLElBQUlqQixHQUFHLENBQUNlLE1BQUosSUFBYyxFQUF4QjtBQUNBRSxRQUFNLElBQUlqQixHQUFHLENBQUN3Z0IsSUFBSixJQUFZLEVBQXRCO0FBRUEsU0FBT3ZmLE1BQVA7QUFDRCxDQXBCRCxDOzs7Ozs7Ozs7Ozs7QUNKYTs7QUFHYmhLLE1BQU0sQ0FBQ0MsT0FBUCxDQUFlZ1csTUFBZixHQUF3QjFVLG1CQUFPLENBQUMsb0RBQUQsQ0FBL0I7QUFDQXZCLE1BQU0sQ0FBQ0MsT0FBUCxDQUFlb1csTUFBZixHQUF3QjlVLG1CQUFPLENBQUMsb0RBQUQsQ0FBL0I7QUFDQXZCLE1BQU0sQ0FBQ0MsT0FBUCxDQUFlaVcsTUFBZixHQUF3QjNVLG1CQUFPLENBQUMsb0RBQUQsQ0FBL0I7QUFDQXZCLE1BQU0sQ0FBQ0MsT0FBUCxDQUFlMlYsS0FBZixHQUF3QnJVLG1CQUFPLENBQUMsa0RBQUQsQ0FBL0IsQzs7Ozs7Ozs7Ozs7O0FDTkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtDQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBR0EsU0FBU2lvQixHQUFULEdBQWU7QUFDYixPQUFLMVQsUUFBTCxHQUFnQixJQUFoQjtBQUNBLE9BQUtxVCxPQUFMLEdBQWUsSUFBZjtBQUNBLE9BQUtDLElBQUwsR0FBWSxJQUFaO0FBQ0EsT0FBS0MsSUFBTCxHQUFZLElBQVo7QUFDQSxPQUFLeFQsUUFBTCxHQUFnQixJQUFoQjtBQUNBLE9BQUswVCxJQUFMLEdBQVksSUFBWjtBQUNBLE9BQUt6ZixNQUFMLEdBQWMsSUFBZDtBQUNBLE9BQUt3ZixRQUFMLEdBQWdCLElBQWhCO0FBQ0QsQyxDQUVEO0FBRUE7QUFDQTs7O0FBQ0EsSUFBSUcsZUFBZSxHQUFHLG1CQUF0QjtBQUFBLElBQ0lDLFdBQVcsR0FBRyxVQURsQjtBQUFBLElBR0k7QUFDQUMsaUJBQWlCLEdBQUcsb0NBSnhCO0FBQUEsSUFNSTtBQUNBO0FBQ0FDLE1BQU0sR0FBRyxDQUFFLEdBQUYsRUFBTyxHQUFQLEVBQVksR0FBWixFQUFpQixHQUFqQixFQUFzQixHQUF0QixFQUEyQixJQUEzQixFQUFpQyxJQUFqQyxFQUF1QyxJQUF2QyxDQVJiO0FBQUEsSUFVSTtBQUNBQyxNQUFNLEdBQUcsQ0FBRSxHQUFGLEVBQU8sR0FBUCxFQUFZLEdBQVosRUFBaUIsSUFBakIsRUFBdUIsR0FBdkIsRUFBNEIsR0FBNUIsRUFBa0MxZixNQUFsQyxDQUF5Q3lmLE1BQXpDLENBWGI7QUFBQSxJQWFJO0FBQ0FFLFVBQVUsR0FBRyxDQUFFLElBQUYsRUFBUzNmLE1BQVQsQ0FBZ0IwZixNQUFoQixDQWRqQjtBQUFBLElBZUk7QUFDQTtBQUNBO0FBQ0E7QUFDQUUsWUFBWSxHQUFHLENBQUUsR0FBRixFQUFPLEdBQVAsRUFBWSxHQUFaLEVBQWlCLEdBQWpCLEVBQXNCLEdBQXRCLEVBQTRCNWYsTUFBNUIsQ0FBbUMyZixVQUFuQyxDQW5CbkI7QUFBQSxJQW9CSUUsZUFBZSxHQUFHLENBQUUsR0FBRixFQUFPLEdBQVAsRUFBWSxHQUFaLENBcEJ0QjtBQUFBLElBcUJJQyxjQUFjLEdBQUcsR0FyQnJCO0FBQUEsSUFzQklDLG1CQUFtQixHQUFHLHdCQXRCMUI7QUFBQSxJQXVCSUMsaUJBQWlCLEdBQUcsOEJBdkJ4QjtBQUFBLElBd0JJOztBQUNBO0FBQ0E7QUFDQUMsZ0JBQWdCLEdBQUc7QUFDakIsZ0JBQWMsSUFERztBQUVqQixpQkFBZTtBQUZFLENBM0J2QjtBQUFBLElBK0JJO0FBQ0FDLGVBQWUsR0FBRztBQUNoQixVQUFRLElBRFE7QUFFaEIsV0FBUyxJQUZPO0FBR2hCLFNBQU8sSUFIUztBQUloQixZQUFVLElBSk07QUFLaEIsVUFBUSxJQUxRO0FBTWhCLFdBQVMsSUFOTztBQU9oQixZQUFVLElBUE07QUFRaEIsVUFBUSxJQVJRO0FBU2hCLGFBQVcsSUFUSztBQVVoQixXQUFTO0FBVk8sQ0FoQ3RCO0FBNENJOztBQUVKLFNBQVNDLFFBQVQsQ0FBa0J2aEIsR0FBbEIsRUFBdUJ3aEIsaUJBQXZCLEVBQTBDO0FBQ3hDLE1BQUl4aEIsR0FBRyxJQUFJQSxHQUFHLFlBQVl5Z0IsR0FBMUIsRUFBK0I7QUFBRSxXQUFPemdCLEdBQVA7QUFBYTs7QUFFOUMsTUFBSXloQixDQUFDLEdBQUcsSUFBSWhCLEdBQUosRUFBUjtBQUNBZ0IsR0FBQyxDQUFDNVUsS0FBRixDQUFRN00sR0FBUixFQUFhd2hCLGlCQUFiO0FBQ0EsU0FBT0MsQ0FBUDtBQUNEOztBQUVEaEIsR0FBRyxDQUFDem5CLFNBQUosQ0FBYzZULEtBQWQsR0FBc0IsVUFBUzdNLEdBQVQsRUFBY3doQixpQkFBZCxFQUFpQztBQUNyRCxNQUFJMXFCLENBQUo7QUFBQSxNQUFPMFksQ0FBUDtBQUFBLE1BQVVrUyxVQUFWO0FBQUEsTUFBc0JDLEdBQXRCO0FBQUEsTUFBMkJ2QixPQUEzQjtBQUFBLE1BQ0l3QixJQUFJLEdBQUc1aEIsR0FEWCxDQURxRCxDQUlyRDtBQUNBOztBQUNBNGhCLE1BQUksR0FBR0EsSUFBSSxDQUFDaGYsSUFBTCxFQUFQOztBQUVBLE1BQUksQ0FBQzRlLGlCQUFELElBQXNCeGhCLEdBQUcsQ0FBQ3JELEtBQUosQ0FBVSxHQUFWLEVBQWU5RixNQUFmLEtBQTBCLENBQXBELEVBQXVEO0FBQ3JEO0FBQ0EsUUFBSWdyQixVQUFVLEdBQUdqQixpQkFBaUIsQ0FBQy9mLElBQWxCLENBQXVCK2dCLElBQXZCLENBQWpCOztBQUNBLFFBQUlDLFVBQUosRUFBZ0I7QUFDZCxXQUFLdEIsUUFBTCxHQUFnQnNCLFVBQVUsQ0FBQyxDQUFELENBQTFCOztBQUNBLFVBQUlBLFVBQVUsQ0FBQyxDQUFELENBQWQsRUFBbUI7QUFDakIsYUFBSzlnQixNQUFMLEdBQWM4Z0IsVUFBVSxDQUFDLENBQUQsQ0FBeEI7QUFDRDs7QUFDRCxhQUFPLElBQVA7QUFDRDtBQUNGOztBQUVELE1BQUlDLEtBQUssR0FBR3BCLGVBQWUsQ0FBQzdmLElBQWhCLENBQXFCK2dCLElBQXJCLENBQVo7O0FBQ0EsTUFBSUUsS0FBSixFQUFXO0FBQ1RBLFNBQUssR0FBR0EsS0FBSyxDQUFDLENBQUQsQ0FBYjtBQUNBSixjQUFVLEdBQUdJLEtBQUssQ0FBQ2xpQixXQUFOLEVBQWI7QUFDQSxTQUFLbU4sUUFBTCxHQUFnQitVLEtBQWhCO0FBQ0FGLFFBQUksR0FBR0EsSUFBSSxDQUFDN0wsTUFBTCxDQUFZK0wsS0FBSyxDQUFDanJCLE1BQWxCLENBQVA7QUFDRCxHQTFCb0QsQ0E0QnJEO0FBQ0E7QUFDQTtBQUNBOzs7QUFDQSxNQUFJMnFCLGlCQUFpQixJQUFJTSxLQUFyQixJQUE4QkYsSUFBSSxDQUFDNWxCLEtBQUwsQ0FBVyxzQkFBWCxDQUFsQyxFQUFzRTtBQUNwRW9rQixXQUFPLEdBQUd3QixJQUFJLENBQUM3TCxNQUFMLENBQVksQ0FBWixFQUFlLENBQWYsTUFBc0IsSUFBaEM7O0FBQ0EsUUFBSXFLLE9BQU8sSUFBSSxFQUFFMEIsS0FBSyxJQUFJVCxnQkFBZ0IsQ0FBQ1MsS0FBRCxDQUEzQixDQUFmLEVBQW9EO0FBQ2xERixVQUFJLEdBQUdBLElBQUksQ0FBQzdMLE1BQUwsQ0FBWSxDQUFaLENBQVA7QUFDQSxXQUFLcUssT0FBTCxHQUFlLElBQWY7QUFDRDtBQUNGOztBQUVELE1BQUksQ0FBQ2lCLGdCQUFnQixDQUFDUyxLQUFELENBQWpCLEtBQ0MxQixPQUFPLElBQUswQixLQUFLLElBQUksQ0FBQ1IsZUFBZSxDQUFDUSxLQUFELENBRHRDLENBQUosRUFDcUQ7QUFFbkQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFFQTtBQUNBLFFBQUlDLE9BQU8sR0FBRyxDQUFDLENBQWY7O0FBQ0EsU0FBS2pyQixDQUFDLEdBQUcsQ0FBVCxFQUFZQSxDQUFDLEdBQUdtcUIsZUFBZSxDQUFDcHFCLE1BQWhDLEVBQXdDQyxDQUFDLEVBQXpDLEVBQTZDO0FBQzNDNnFCLFNBQUcsR0FBR0MsSUFBSSxDQUFDNWdCLE9BQUwsQ0FBYWlnQixlQUFlLENBQUNucUIsQ0FBRCxDQUE1QixDQUFOOztBQUNBLFVBQUk2cUIsR0FBRyxLQUFLLENBQUMsQ0FBVCxLQUFlSSxPQUFPLEtBQUssQ0FBQyxDQUFiLElBQWtCSixHQUFHLEdBQUdJLE9BQXZDLENBQUosRUFBcUQ7QUFDbkRBLGVBQU8sR0FBR0osR0FBVjtBQUNEO0FBQ0YsS0F4QmtELENBMEJuRDtBQUNBOzs7QUFDQSxRQUFJdEIsSUFBSixFQUFVMkIsTUFBVjs7QUFDQSxRQUFJRCxPQUFPLEtBQUssQ0FBQyxDQUFqQixFQUFvQjtBQUNsQjtBQUNBQyxZQUFNLEdBQUdKLElBQUksQ0FBQ0ssV0FBTCxDQUFpQixHQUFqQixDQUFUO0FBQ0QsS0FIRCxNQUdPO0FBQ0w7QUFDQTtBQUNBRCxZQUFNLEdBQUdKLElBQUksQ0FBQ0ssV0FBTCxDQUFpQixHQUFqQixFQUFzQkYsT0FBdEIsQ0FBVDtBQUNELEtBcENrRCxDQXNDbkQ7QUFDQTs7O0FBQ0EsUUFBSUMsTUFBTSxLQUFLLENBQUMsQ0FBaEIsRUFBbUI7QUFDakIzQixVQUFJLEdBQUd1QixJQUFJLENBQUNwb0IsS0FBTCxDQUFXLENBQVgsRUFBY3dvQixNQUFkLENBQVA7QUFDQUosVUFBSSxHQUFHQSxJQUFJLENBQUNwb0IsS0FBTCxDQUFXd29CLE1BQU0sR0FBRyxDQUFwQixDQUFQO0FBQ0EsV0FBSzNCLElBQUwsR0FBWUEsSUFBWjtBQUNELEtBNUNrRCxDQThDbkQ7OztBQUNBMEIsV0FBTyxHQUFHLENBQUMsQ0FBWDs7QUFDQSxTQUFLanJCLENBQUMsR0FBRyxDQUFULEVBQVlBLENBQUMsR0FBR2txQixZQUFZLENBQUNucUIsTUFBN0IsRUFBcUNDLENBQUMsRUFBdEMsRUFBMEM7QUFDeEM2cUIsU0FBRyxHQUFHQyxJQUFJLENBQUM1Z0IsT0FBTCxDQUFhZ2dCLFlBQVksQ0FBQ2xxQixDQUFELENBQXpCLENBQU47O0FBQ0EsVUFBSTZxQixHQUFHLEtBQUssQ0FBQyxDQUFULEtBQWVJLE9BQU8sS0FBSyxDQUFDLENBQWIsSUFBa0JKLEdBQUcsR0FBR0ksT0FBdkMsQ0FBSixFQUFxRDtBQUNuREEsZUFBTyxHQUFHSixHQUFWO0FBQ0Q7QUFDRixLQXJEa0QsQ0FzRG5EOzs7QUFDQSxRQUFJSSxPQUFPLEtBQUssQ0FBQyxDQUFqQixFQUFvQjtBQUNsQkEsYUFBTyxHQUFHSCxJQUFJLENBQUMvcUIsTUFBZjtBQUNEOztBQUVELFFBQUkrcUIsSUFBSSxDQUFDRyxPQUFPLEdBQUcsQ0FBWCxDQUFKLEtBQXNCLEdBQTFCLEVBQStCO0FBQUVBLGFBQU87QUFBSzs7QUFDN0MsUUFBSUcsSUFBSSxHQUFHTixJQUFJLENBQUNwb0IsS0FBTCxDQUFXLENBQVgsRUFBY3VvQixPQUFkLENBQVg7QUFDQUgsUUFBSSxHQUFHQSxJQUFJLENBQUNwb0IsS0FBTCxDQUFXdW9CLE9BQVgsQ0FBUCxDQTdEbUQsQ0ErRG5EOztBQUNBLFNBQUtJLFNBQUwsQ0FBZUQsSUFBZixFQWhFbUQsQ0FrRW5EO0FBQ0E7O0FBQ0EsU0FBS3BWLFFBQUwsR0FBZ0IsS0FBS0EsUUFBTCxJQUFpQixFQUFqQyxDQXBFbUQsQ0FzRW5EO0FBQ0E7O0FBQ0EsUUFBSXNWLFlBQVksR0FBRyxLQUFLdFYsUUFBTCxDQUFjLENBQWQsTUFBcUIsR0FBckIsSUFDZixLQUFLQSxRQUFMLENBQWMsS0FBS0EsUUFBTCxDQUFjalcsTUFBZCxHQUF1QixDQUFyQyxNQUE0QyxHQURoRCxDQXhFbUQsQ0EyRW5EOztBQUNBLFFBQUksQ0FBQ3VyQixZQUFMLEVBQW1CO0FBQ2pCLFVBQUlDLFNBQVMsR0FBRyxLQUFLdlYsUUFBTCxDQUFjblEsS0FBZCxDQUFvQixJQUFwQixDQUFoQjs7QUFDQSxXQUFLN0YsQ0FBQyxHQUFHLENBQUosRUFBTzBZLENBQUMsR0FBRzZTLFNBQVMsQ0FBQ3hyQixNQUExQixFQUFrQ0MsQ0FBQyxHQUFHMFksQ0FBdEMsRUFBeUMxWSxDQUFDLEVBQTFDLEVBQThDO0FBQzVDLFlBQUl3ckIsSUFBSSxHQUFHRCxTQUFTLENBQUN2ckIsQ0FBRCxDQUFwQjs7QUFDQSxZQUFJLENBQUN3ckIsSUFBTCxFQUFXO0FBQUU7QUFBVzs7QUFDeEIsWUFBSSxDQUFDQSxJQUFJLENBQUN0bUIsS0FBTCxDQUFXbWxCLG1CQUFYLENBQUwsRUFBc0M7QUFDcEMsY0FBSW9CLE9BQU8sR0FBRyxFQUFkOztBQUNBLGVBQUssSUFBSWpLLENBQUMsR0FBRyxDQUFSLEVBQVdwZCxDQUFDLEdBQUdvbkIsSUFBSSxDQUFDenJCLE1BQXpCLEVBQWlDeWhCLENBQUMsR0FBR3BkLENBQXJDLEVBQXdDb2QsQ0FBQyxFQUF6QyxFQUE2QztBQUMzQyxnQkFBSWdLLElBQUksQ0FBQzllLFVBQUwsQ0FBZ0I4VSxDQUFoQixJQUFxQixHQUF6QixFQUE4QjtBQUM1QjtBQUNBO0FBQ0E7QUFDQWlLLHFCQUFPLElBQUksR0FBWDtBQUNELGFBTEQsTUFLTztBQUNMQSxxQkFBTyxJQUFJRCxJQUFJLENBQUNoSyxDQUFELENBQWY7QUFDRDtBQUNGLFdBWG1DLENBWXBDOzs7QUFDQSxjQUFJLENBQUNpSyxPQUFPLENBQUN2bUIsS0FBUixDQUFjbWxCLG1CQUFkLENBQUwsRUFBeUM7QUFDdkMsZ0JBQUlxQixVQUFVLEdBQUdILFNBQVMsQ0FBQzdvQixLQUFWLENBQWdCLENBQWhCLEVBQW1CMUMsQ0FBbkIsQ0FBakI7QUFDQSxnQkFBSTJyQixPQUFPLEdBQUdKLFNBQVMsQ0FBQzdvQixLQUFWLENBQWdCMUMsQ0FBQyxHQUFHLENBQXBCLENBQWQ7QUFDQSxnQkFBSTRyQixHQUFHLEdBQUdKLElBQUksQ0FBQ3RtQixLQUFMLENBQVdvbEIsaUJBQVgsQ0FBVjs7QUFDQSxnQkFBSXNCLEdBQUosRUFBUztBQUNQRix3QkFBVSxDQUFDdHFCLElBQVgsQ0FBZ0J3cUIsR0FBRyxDQUFDLENBQUQsQ0FBbkI7QUFDQUQscUJBQU8sQ0FBQ0UsT0FBUixDQUFnQkQsR0FBRyxDQUFDLENBQUQsQ0FBbkI7QUFDRDs7QUFDRCxnQkFBSUQsT0FBTyxDQUFDNXJCLE1BQVosRUFBb0I7QUFDbEIrcUIsa0JBQUksR0FBR2EsT0FBTyxDQUFDL2tCLElBQVIsQ0FBYSxHQUFiLElBQW9Ca2tCLElBQTNCO0FBQ0Q7O0FBQ0QsaUJBQUs5VSxRQUFMLEdBQWdCMFYsVUFBVSxDQUFDOWtCLElBQVgsQ0FBZ0IsR0FBaEIsQ0FBaEI7QUFDQTtBQUNEO0FBQ0Y7QUFDRjtBQUNGOztBQUVELFFBQUksS0FBS29QLFFBQUwsQ0FBY2pXLE1BQWQsR0FBdUJxcUIsY0FBM0IsRUFBMkM7QUFDekMsV0FBS3BVLFFBQUwsR0FBZ0IsRUFBaEI7QUFDRCxLQWxIa0QsQ0FvSG5EO0FBQ0E7OztBQUNBLFFBQUlzVixZQUFKLEVBQWtCO0FBQ2hCLFdBQUt0VixRQUFMLEdBQWdCLEtBQUtBLFFBQUwsQ0FBY2lKLE1BQWQsQ0FBcUIsQ0FBckIsRUFBd0IsS0FBS2pKLFFBQUwsQ0FBY2pXLE1BQWQsR0FBdUIsQ0FBL0MsQ0FBaEI7QUFDRDtBQUNGLEdBbEtvRCxDQW9LckQ7OztBQUNBLE1BQUkycEIsSUFBSSxHQUFHb0IsSUFBSSxDQUFDNWdCLE9BQUwsQ0FBYSxHQUFiLENBQVg7O0FBQ0EsTUFBSXdmLElBQUksS0FBSyxDQUFDLENBQWQsRUFBaUI7QUFDZjtBQUNBLFNBQUtBLElBQUwsR0FBWW9CLElBQUksQ0FBQzdMLE1BQUwsQ0FBWXlLLElBQVosQ0FBWjtBQUNBb0IsUUFBSSxHQUFHQSxJQUFJLENBQUNwb0IsS0FBTCxDQUFXLENBQVgsRUFBY2duQixJQUFkLENBQVA7QUFDRDs7QUFDRCxNQUFJb0MsRUFBRSxHQUFHaEIsSUFBSSxDQUFDNWdCLE9BQUwsQ0FBYSxHQUFiLENBQVQ7O0FBQ0EsTUFBSTRoQixFQUFFLEtBQUssQ0FBQyxDQUFaLEVBQWU7QUFDYixTQUFLN2hCLE1BQUwsR0FBYzZnQixJQUFJLENBQUM3TCxNQUFMLENBQVk2TSxFQUFaLENBQWQ7QUFDQWhCLFFBQUksR0FBR0EsSUFBSSxDQUFDcG9CLEtBQUwsQ0FBVyxDQUFYLEVBQWNvcEIsRUFBZCxDQUFQO0FBQ0Q7O0FBQ0QsTUFBSWhCLElBQUosRUFBVTtBQUFFLFNBQUtyQixRQUFMLEdBQWdCcUIsSUFBaEI7QUFBdUI7O0FBQ25DLE1BQUlOLGVBQWUsQ0FBQ0ksVUFBRCxDQUFmLElBQ0EsS0FBSzVVLFFBREwsSUFDaUIsQ0FBQyxLQUFLeVQsUUFEM0IsRUFDcUM7QUFDbkMsU0FBS0EsUUFBTCxHQUFnQixFQUFoQjtBQUNEOztBQUVELFNBQU8sSUFBUDtBQUNELENBdkxEOztBQXlMQUUsR0FBRyxDQUFDem5CLFNBQUosQ0FBY21wQixTQUFkLEdBQTBCLFVBQVNELElBQVQsRUFBZTtBQUN2QyxNQUFJNUIsSUFBSSxHQUFHSyxXQUFXLENBQUM5ZixJQUFaLENBQWlCcWhCLElBQWpCLENBQVg7O0FBQ0EsTUFBSTVCLElBQUosRUFBVTtBQUNSQSxRQUFJLEdBQUdBLElBQUksQ0FBQyxDQUFELENBQVg7O0FBQ0EsUUFBSUEsSUFBSSxLQUFLLEdBQWIsRUFBa0I7QUFDaEIsV0FBS0EsSUFBTCxHQUFZQSxJQUFJLENBQUN2SyxNQUFMLENBQVksQ0FBWixDQUFaO0FBQ0Q7O0FBQ0RtTSxRQUFJLEdBQUdBLElBQUksQ0FBQ25NLE1BQUwsQ0FBWSxDQUFaLEVBQWVtTSxJQUFJLENBQUNyckIsTUFBTCxHQUFjeXBCLElBQUksQ0FBQ3pwQixNQUFsQyxDQUFQO0FBQ0Q7O0FBQ0QsTUFBSXFyQixJQUFKLEVBQVU7QUFBRSxTQUFLcFYsUUFBTCxHQUFnQm9WLElBQWhCO0FBQXVCO0FBQ3BDLENBVkQ7O0FBWUFqckIsTUFBTSxDQUFDQyxPQUFQLEdBQWlCcXFCLFFBQWpCLEM7Ozs7Ozs7Ozs7Ozs7QUN2VEE7QUFDQTs7QUFBRSxXQUFTc0IsSUFBVCxFQUFlO0FBRWhCO0FBQ0EsTUFBSUMsV0FBVyxHQUFHLDhCQUFPNXJCLE9BQVAsTUFBa0IsUUFBbEIsSUFBOEJBLE9BQTlCLElBQ2pCLENBQUNBLE9BQU8sQ0FBQzZyQixRQURRLElBQ0k3ckIsT0FEdEI7QUFFQSxNQUFJOHJCLFVBQVUsR0FBRyw4QkFBTy9yQixNQUFQLE1BQWlCLFFBQWpCLElBQTZCQSxNQUE3QixJQUNoQixDQUFDQSxNQUFNLENBQUM4ckIsUUFEUSxJQUNJOXJCLE1BRHJCO0FBRUEsTUFBSWdzQixVQUFVLEdBQUcsUUFBT0MsTUFBUCx5Q0FBT0EsTUFBUCxNQUFpQixRQUFqQixJQUE2QkEsTUFBOUM7O0FBQ0EsTUFDQ0QsVUFBVSxDQUFDQyxNQUFYLEtBQXNCRCxVQUF0QixJQUNBQSxVQUFVLENBQUNFLE1BQVgsS0FBc0JGLFVBRHRCLElBRUFBLFVBQVUsQ0FBQ3puQixJQUFYLEtBQW9CeW5CLFVBSHJCLEVBSUU7QUFDREosUUFBSSxHQUFHSSxVQUFQO0FBQ0E7QUFFRDs7Ozs7OztBQUtBLE1BQUk3VyxRQUFKOztBQUVBO0FBQ0FnWCxRQUFNLEdBQUcsVUFIVDtBQUFBLE1BR3FCOztBQUVyQjtBQUNBQyxNQUFJLEdBQUcsRUFOUDtBQUFBLE1BT0FDLElBQUksR0FBRyxDQVBQO0FBQUEsTUFRQUMsSUFBSSxHQUFHLEVBUlA7QUFBQSxNQVNBQyxJQUFJLEdBQUcsRUFUUDtBQUFBLE1BVUFDLElBQUksR0FBRyxHQVZQO0FBQUEsTUFXQUMsV0FBVyxHQUFHLEVBWGQ7QUFBQSxNQVlBQyxRQUFRLEdBQUcsR0FaWDtBQUFBLE1BWWdCO0FBQ2hCQyxXQUFTLEdBQUcsR0FiWjtBQUFBLE1BYWlCOztBQUVqQjtBQUNBQyxlQUFhLEdBQUcsT0FoQmhCO0FBQUEsTUFpQkFDLGFBQWEsR0FBRyxjQWpCaEI7QUFBQSxNQWlCZ0M7QUFDaENDLGlCQUFlLEdBQUcsMkJBbEJsQjtBQUFBLE1Ba0IrQzs7QUFFL0M7QUFDQUMsUUFBTSxHQUFHO0FBQ1IsZ0JBQVksaURBREo7QUFFUixpQkFBYSxnREFGTDtBQUdSLHFCQUFpQjtBQUhULEdBckJUOztBQTJCQTtBQUNBQyxlQUFhLEdBQUdaLElBQUksR0FBR0MsSUE1QnZCO0FBQUEsTUE2QkF6ZSxLQUFLLEdBQUdELElBQUksQ0FBQ0MsS0E3QmI7QUFBQSxNQThCQXFmLGtCQUFrQixHQUFHamIsTUFBTSxDQUFDQyxZQTlCNUI7O0FBZ0NBO0FBQ0FoUCxLQWpDQTtBQW1DQTs7QUFFQTs7Ozs7OztBQU1BLFdBQVNpcUIsS0FBVCxDQUFlelMsSUFBZixFQUFxQjtBQUNwQixVQUFNLElBQUkwUyxVQUFKLENBQWVKLE1BQU0sQ0FBQ3RTLElBQUQsQ0FBckIsQ0FBTjtBQUNBO0FBRUQ7Ozs7Ozs7Ozs7QUFRQSxXQUFTMVMsR0FBVCxDQUFhcWxCLEtBQWIsRUFBb0JsUyxFQUFwQixFQUF3QjtBQUN2QixRQUFJdGIsTUFBTSxHQUFHd3RCLEtBQUssQ0FBQ3h0QixNQUFuQjtBQUNBLFFBQUlvSyxNQUFNLEdBQUcsRUFBYjs7QUFDQSxXQUFPcEssTUFBTSxFQUFiLEVBQWlCO0FBQ2hCb0ssWUFBTSxDQUFDcEssTUFBRCxDQUFOLEdBQWlCc2IsRUFBRSxDQUFDa1MsS0FBSyxDQUFDeHRCLE1BQUQsQ0FBTixDQUFuQjtBQUNBOztBQUNELFdBQU9vSyxNQUFQO0FBQ0E7QUFFRDs7Ozs7Ozs7Ozs7O0FBVUEsV0FBU3FqQixTQUFULENBQW1CaEYsTUFBbkIsRUFBMkJuTixFQUEzQixFQUErQjtBQUM5QixRQUFJb1MsS0FBSyxHQUFHakYsTUFBTSxDQUFDM2lCLEtBQVAsQ0FBYSxHQUFiLENBQVo7QUFDQSxRQUFJc0UsTUFBTSxHQUFHLEVBQWI7O0FBQ0EsUUFBSXNqQixLQUFLLENBQUMxdEIsTUFBTixHQUFlLENBQW5CLEVBQXNCO0FBQ3JCO0FBQ0E7QUFDQW9LLFlBQU0sR0FBR3NqQixLQUFLLENBQUMsQ0FBRCxDQUFMLEdBQVcsR0FBcEI7QUFDQWpGLFlBQU0sR0FBR2lGLEtBQUssQ0FBQyxDQUFELENBQWQ7QUFDQSxLQVI2QixDQVM5Qjs7O0FBQ0FqRixVQUFNLEdBQUdBLE1BQU0sQ0FBQzVrQixPQUFQLENBQWVxcEIsZUFBZixFQUFnQyxNQUFoQyxDQUFUO0FBQ0EsUUFBSVMsTUFBTSxHQUFHbEYsTUFBTSxDQUFDM2lCLEtBQVAsQ0FBYSxHQUFiLENBQWI7QUFDQSxRQUFJOG5CLE9BQU8sR0FBR3psQixHQUFHLENBQUN3bEIsTUFBRCxFQUFTclMsRUFBVCxDQUFILENBQWdCelUsSUFBaEIsQ0FBcUIsR0FBckIsQ0FBZDtBQUNBLFdBQU91RCxNQUFNLEdBQUd3akIsT0FBaEI7QUFDQTtBQUVEOzs7Ozs7Ozs7Ozs7Ozs7QUFhQSxXQUFTQyxVQUFULENBQW9CcEYsTUFBcEIsRUFBNEI7QUFDM0IsUUFBSXFGLE1BQU0sR0FBRyxFQUFiO0FBQUEsUUFDSUMsT0FBTyxHQUFHLENBRGQ7QUFBQSxRQUVJL3RCLE1BQU0sR0FBR3lvQixNQUFNLENBQUN6b0IsTUFGcEI7QUFBQSxRQUdJc0IsS0FISjtBQUFBLFFBSUkwc0IsS0FKSjs7QUFLQSxXQUFPRCxPQUFPLEdBQUcvdEIsTUFBakIsRUFBeUI7QUFDeEJzQixXQUFLLEdBQUdtbkIsTUFBTSxDQUFDOWIsVUFBUCxDQUFrQm9oQixPQUFPLEVBQXpCLENBQVI7O0FBQ0EsVUFBSXpzQixLQUFLLElBQUksTUFBVCxJQUFtQkEsS0FBSyxJQUFJLE1BQTVCLElBQXNDeXNCLE9BQU8sR0FBRy90QixNQUFwRCxFQUE0RDtBQUMzRDtBQUNBZ3VCLGFBQUssR0FBR3ZGLE1BQU0sQ0FBQzliLFVBQVAsQ0FBa0JvaEIsT0FBTyxFQUF6QixDQUFSOztBQUNBLFlBQUksQ0FBQ0MsS0FBSyxHQUFHLE1BQVQsS0FBb0IsTUFBeEIsRUFBZ0M7QUFBRTtBQUNqQ0YsZ0JBQU0sQ0FBQ3pzQixJQUFQLENBQVksQ0FBQyxDQUFDQyxLQUFLLEdBQUcsS0FBVCxLQUFtQixFQUFwQixLQUEyQjBzQixLQUFLLEdBQUcsS0FBbkMsSUFBNEMsT0FBeEQ7QUFDQSxTQUZELE1BRU87QUFDTjtBQUNBO0FBQ0FGLGdCQUFNLENBQUN6c0IsSUFBUCxDQUFZQyxLQUFaO0FBQ0F5c0IsaUJBQU87QUFDUDtBQUNELE9BWEQsTUFXTztBQUNORCxjQUFNLENBQUN6c0IsSUFBUCxDQUFZQyxLQUFaO0FBQ0E7QUFDRDs7QUFDRCxXQUFPd3NCLE1BQVA7QUFDQTtBQUVEOzs7Ozs7Ozs7O0FBUUEsV0FBU0csVUFBVCxDQUFvQlQsS0FBcEIsRUFBMkI7QUFDMUIsV0FBT3JsQixHQUFHLENBQUNxbEIsS0FBRCxFQUFRLFVBQVNsc0IsS0FBVCxFQUFnQjtBQUNqQyxVQUFJd3NCLE1BQU0sR0FBRyxFQUFiOztBQUNBLFVBQUl4c0IsS0FBSyxHQUFHLE1BQVosRUFBb0I7QUFDbkJBLGFBQUssSUFBSSxPQUFUO0FBQ0F3c0IsY0FBTSxJQUFJVCxrQkFBa0IsQ0FBQy9yQixLQUFLLEtBQUssRUFBVixHQUFlLEtBQWYsR0FBdUIsTUFBeEIsQ0FBNUI7QUFDQUEsYUFBSyxHQUFHLFNBQVNBLEtBQUssR0FBRyxLQUF6QjtBQUNBOztBQUNEd3NCLFlBQU0sSUFBSVQsa0JBQWtCLENBQUMvckIsS0FBRCxDQUE1QjtBQUNBLGFBQU93c0IsTUFBUDtBQUNBLEtBVFMsQ0FBSCxDQVNKam5CLElBVEksQ0FTQyxFQVRELENBQVA7QUFVQTtBQUVEOzs7Ozs7Ozs7OztBQVNBLFdBQVNxbkIsWUFBVCxDQUFzQkMsU0FBdEIsRUFBaUM7QUFDaEMsUUFBSUEsU0FBUyxHQUFHLEVBQVosR0FBaUIsRUFBckIsRUFBeUI7QUFDeEIsYUFBT0EsU0FBUyxHQUFHLEVBQW5CO0FBQ0E7O0FBQ0QsUUFBSUEsU0FBUyxHQUFHLEVBQVosR0FBaUIsRUFBckIsRUFBeUI7QUFDeEIsYUFBT0EsU0FBUyxHQUFHLEVBQW5CO0FBQ0E7O0FBQ0QsUUFBSUEsU0FBUyxHQUFHLEVBQVosR0FBaUIsRUFBckIsRUFBeUI7QUFDeEIsYUFBT0EsU0FBUyxHQUFHLEVBQW5CO0FBQ0E7O0FBQ0QsV0FBTzNCLElBQVA7QUFDQTtBQUVEOzs7Ozs7Ozs7Ozs7O0FBV0EsV0FBUzRCLFlBQVQsQ0FBc0JDLEtBQXRCLEVBQTZCQyxJQUE3QixFQUFtQztBQUNsQztBQUNBO0FBQ0EsV0FBT0QsS0FBSyxHQUFHLEVBQVIsR0FBYSxNQUFNQSxLQUFLLEdBQUcsRUFBZCxDQUFiLElBQWtDLENBQUNDLElBQUksSUFBSSxDQUFULEtBQWUsQ0FBakQsQ0FBUDtBQUNBO0FBRUQ7Ozs7Ozs7QUFLQSxXQUFTQyxLQUFULENBQWVDLEtBQWYsRUFBc0JDLFNBQXRCLEVBQWlDQyxTQUFqQyxFQUE0QztBQUMzQyxRQUFJcnFCLENBQUMsR0FBRyxDQUFSO0FBQ0FtcUIsU0FBSyxHQUFHRSxTQUFTLEdBQUcxZ0IsS0FBSyxDQUFDd2dCLEtBQUssR0FBRzVCLElBQVQsQ0FBUixHQUF5QjRCLEtBQUssSUFBSSxDQUFuRDtBQUNBQSxTQUFLLElBQUl4Z0IsS0FBSyxDQUFDd2dCLEtBQUssR0FBR0MsU0FBVCxDQUFkOztBQUNBO0FBQUs7QUFBeUJELFNBQUssR0FBR3BCLGFBQWEsR0FBR1YsSUFBaEIsSUFBd0IsQ0FBOUQsRUFBaUVyb0IsQ0FBQyxJQUFJbW9CLElBQXRFLEVBQTRFO0FBQzNFZ0MsV0FBSyxHQUFHeGdCLEtBQUssQ0FBQ3dnQixLQUFLLEdBQUdwQixhQUFULENBQWI7QUFDQTs7QUFDRCxXQUFPcGYsS0FBSyxDQUFDM0osQ0FBQyxHQUFHLENBQUMrb0IsYUFBYSxHQUFHLENBQWpCLElBQXNCb0IsS0FBdEIsSUFBK0JBLEtBQUssR0FBRzdCLElBQXZDLENBQUwsQ0FBWjtBQUNBO0FBRUQ7Ozs7Ozs7OztBQU9BLFdBQVNsVyxNQUFULENBQWdCa1ksS0FBaEIsRUFBdUI7QUFDdEI7QUFDQSxRQUFJYixNQUFNLEdBQUcsRUFBYjtBQUFBLFFBQ0ljLFdBQVcsR0FBR0QsS0FBSyxDQUFDM3VCLE1BRHhCO0FBQUEsUUFFSTZ1QixHQUZKO0FBQUEsUUFHSTV1QixDQUFDLEdBQUcsQ0FIUjtBQUFBLFFBSUl1QyxDQUFDLEdBQUdzcUIsUUFKUjtBQUFBLFFBS0lnQyxJQUFJLEdBQUdqQyxXQUxYO0FBQUEsUUFNSWtDLEtBTko7QUFBQSxRQU9JdE4sQ0FQSjtBQUFBLFFBUUl6WSxLQVJKO0FBQUEsUUFTSWdtQixJQVRKO0FBQUEsUUFVSUMsQ0FWSjtBQUFBLFFBV0k1cUIsQ0FYSjtBQUFBLFFBWUlncUIsS0FaSjtBQUFBLFFBYUlsTixDQWJKOztBQWNJO0FBQ0ErTixjQWZKLENBRnNCLENBbUJ0QjtBQUNBO0FBQ0E7O0FBRUFILFNBQUssR0FBR0osS0FBSyxDQUFDdkQsV0FBTixDQUFrQjJCLFNBQWxCLENBQVI7O0FBQ0EsUUFBSWdDLEtBQUssR0FBRyxDQUFaLEVBQWU7QUFDZEEsV0FBSyxHQUFHLENBQVI7QUFDQTs7QUFFRCxTQUFLdE4sQ0FBQyxHQUFHLENBQVQsRUFBWUEsQ0FBQyxHQUFHc04sS0FBaEIsRUFBdUIsRUFBRXROLENBQXpCLEVBQTRCO0FBQzNCO0FBQ0EsVUFBSWtOLEtBQUssQ0FBQ2hpQixVQUFOLENBQWlCOFUsQ0FBakIsS0FBdUIsSUFBM0IsRUFBaUM7QUFDaEM2TCxhQUFLLENBQUMsV0FBRCxDQUFMO0FBQ0E7O0FBQ0RRLFlBQU0sQ0FBQ3pzQixJQUFQLENBQVlzdEIsS0FBSyxDQUFDaGlCLFVBQU4sQ0FBaUI4VSxDQUFqQixDQUFaO0FBQ0EsS0FsQ3FCLENBb0N0QjtBQUNBOzs7QUFFQSxTQUFLelksS0FBSyxHQUFHK2xCLEtBQUssR0FBRyxDQUFSLEdBQVlBLEtBQUssR0FBRyxDQUFwQixHQUF3QixDQUFyQyxFQUF3Qy9sQixLQUFLLEdBQUc0bEIsV0FBaEQ7QUFBNkQ7QUFBMkI7QUFFdkY7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQUtJLElBQUksR0FBRy91QixDQUFQLEVBQVVndkIsQ0FBQyxHQUFHLENBQWQsRUFBaUI1cUIsQ0FBQyxHQUFHbW9CLElBQTFCO0FBQWdDO0FBQW9Cbm9CLE9BQUMsSUFBSW1vQixJQUF6RCxFQUErRDtBQUU5RCxZQUFJeGpCLEtBQUssSUFBSTRsQixXQUFiLEVBQTBCO0FBQ3pCdEIsZUFBSyxDQUFDLGVBQUQsQ0FBTDtBQUNBOztBQUVEZSxhQUFLLEdBQUdILFlBQVksQ0FBQ1MsS0FBSyxDQUFDaGlCLFVBQU4sQ0FBaUIzRCxLQUFLLEVBQXRCLENBQUQsQ0FBcEI7O0FBRUEsWUFBSXFsQixLQUFLLElBQUk3QixJQUFULElBQWlCNkIsS0FBSyxHQUFHcmdCLEtBQUssQ0FBQyxDQUFDdWUsTUFBTSxHQUFHdHNCLENBQVYsSUFBZWd2QixDQUFoQixDQUFsQyxFQUFzRDtBQUNyRDNCLGVBQUssQ0FBQyxVQUFELENBQUw7QUFDQTs7QUFFRHJ0QixTQUFDLElBQUlvdUIsS0FBSyxHQUFHWSxDQUFiO0FBQ0E5TixTQUFDLEdBQUc5YyxDQUFDLElBQUl5cUIsSUFBTCxHQUFZckMsSUFBWixHQUFvQnBvQixDQUFDLElBQUl5cUIsSUFBSSxHQUFHcEMsSUFBWixHQUFtQkEsSUFBbkIsR0FBMEJyb0IsQ0FBQyxHQUFHeXFCLElBQXREOztBQUVBLFlBQUlULEtBQUssR0FBR2xOLENBQVosRUFBZTtBQUNkO0FBQ0E7O0FBRUQrTixrQkFBVSxHQUFHMUMsSUFBSSxHQUFHckwsQ0FBcEI7O0FBQ0EsWUFBSThOLENBQUMsR0FBR2poQixLQUFLLENBQUN1ZSxNQUFNLEdBQUcyQyxVQUFWLENBQWIsRUFBb0M7QUFDbkM1QixlQUFLLENBQUMsVUFBRCxDQUFMO0FBQ0E7O0FBRUQyQixTQUFDLElBQUlDLFVBQUw7QUFFQTs7QUFFREwsU0FBRyxHQUFHZixNQUFNLENBQUM5dEIsTUFBUCxHQUFnQixDQUF0QjtBQUNBOHVCLFVBQUksR0FBR1AsS0FBSyxDQUFDdHVCLENBQUMsR0FBRyt1QixJQUFMLEVBQVdILEdBQVgsRUFBZ0JHLElBQUksSUFBSSxDQUF4QixDQUFaLENBcEN1RixDQXNDdkY7QUFDQTs7QUFDQSxVQUFJaGhCLEtBQUssQ0FBQy9OLENBQUMsR0FBRzR1QixHQUFMLENBQUwsR0FBaUJ0QyxNQUFNLEdBQUcvcEIsQ0FBOUIsRUFBaUM7QUFDaEM4cUIsYUFBSyxDQUFDLFVBQUQsQ0FBTDtBQUNBOztBQUVEOXFCLE9BQUMsSUFBSXdMLEtBQUssQ0FBQy9OLENBQUMsR0FBRzR1QixHQUFMLENBQVY7QUFDQTV1QixPQUFDLElBQUk0dUIsR0FBTCxDQTdDdUYsQ0ErQ3ZGOztBQUNBZixZQUFNLENBQUNuUyxNQUFQLENBQWMxYixDQUFDLEVBQWYsRUFBbUIsQ0FBbkIsRUFBc0J1QyxDQUF0QjtBQUVBOztBQUVELFdBQU95ckIsVUFBVSxDQUFDSCxNQUFELENBQWpCO0FBQ0E7QUFFRDs7Ozs7Ozs7O0FBT0EsV0FBU3pYLE1BQVQsQ0FBZ0JzWSxLQUFoQixFQUF1QjtBQUN0QixRQUFJbnNCLENBQUo7QUFBQSxRQUNJZ3NCLEtBREo7QUFBQSxRQUVJVyxjQUZKO0FBQUEsUUFHSUMsV0FISjtBQUFBLFFBSUlOLElBSko7QUFBQSxRQUtJck4sQ0FMSjtBQUFBLFFBTUk5WCxDQU5KO0FBQUEsUUFPSTBsQixDQVBKO0FBQUEsUUFRSWhyQixDQVJKO0FBQUEsUUFTSThjLENBVEo7QUFBQSxRQVVJbU8sWUFWSjtBQUFBLFFBV0l4QixNQUFNLEdBQUcsRUFYYjs7QUFZSTtBQUNBYyxlQWJKOztBQWNJO0FBQ0FXLHlCQWZKO0FBQUEsUUFnQklMLFVBaEJKO0FBQUEsUUFpQklNLE9BakJKLENBRHNCLENBb0J0Qjs7QUFDQWIsU0FBSyxHQUFHZCxVQUFVLENBQUNjLEtBQUQsQ0FBbEIsQ0FyQnNCLENBdUJ0Qjs7QUFDQUMsZUFBVyxHQUFHRCxLQUFLLENBQUMzdUIsTUFBcEIsQ0F4QnNCLENBMEJ0Qjs7QUFDQXdDLEtBQUMsR0FBR3NxQixRQUFKO0FBQ0EwQixTQUFLLEdBQUcsQ0FBUjtBQUNBTSxRQUFJLEdBQUdqQyxXQUFQLENBN0JzQixDQStCdEI7O0FBQ0EsU0FBS3BMLENBQUMsR0FBRyxDQUFULEVBQVlBLENBQUMsR0FBR21OLFdBQWhCLEVBQTZCLEVBQUVuTixDQUEvQixFQUFrQztBQUNqQzZOLGtCQUFZLEdBQUdYLEtBQUssQ0FBQ2xOLENBQUQsQ0FBcEI7O0FBQ0EsVUFBSTZOLFlBQVksR0FBRyxJQUFuQixFQUF5QjtBQUN4QnhCLGNBQU0sQ0FBQ3pzQixJQUFQLENBQVlnc0Isa0JBQWtCLENBQUNpQyxZQUFELENBQTlCO0FBQ0E7QUFDRDs7QUFFREgsa0JBQWMsR0FBR0MsV0FBVyxHQUFHdEIsTUFBTSxDQUFDOXRCLE1BQXRDLENBdkNzQixDQXlDdEI7QUFDQTtBQUVBOztBQUNBLFFBQUlvdkIsV0FBSixFQUFpQjtBQUNoQnRCLFlBQU0sQ0FBQ3pzQixJQUFQLENBQVkwckIsU0FBWjtBQUNBLEtBL0NxQixDQWlEdEI7OztBQUNBLFdBQU9vQyxjQUFjLEdBQUdQLFdBQXhCLEVBQXFDO0FBRXBDO0FBQ0E7QUFDQSxXQUFLamxCLENBQUMsR0FBRzRpQixNQUFKLEVBQVk5SyxDQUFDLEdBQUcsQ0FBckIsRUFBd0JBLENBQUMsR0FBR21OLFdBQTVCLEVBQXlDLEVBQUVuTixDQUEzQyxFQUE4QztBQUM3QzZOLG9CQUFZLEdBQUdYLEtBQUssQ0FBQ2xOLENBQUQsQ0FBcEI7O0FBQ0EsWUFBSTZOLFlBQVksSUFBSTlzQixDQUFoQixJQUFxQjhzQixZQUFZLEdBQUczbEIsQ0FBeEMsRUFBMkM7QUFDMUNBLFdBQUMsR0FBRzJsQixZQUFKO0FBQ0E7QUFDRCxPQVRtQyxDQVdwQztBQUNBOzs7QUFDQUMsMkJBQXFCLEdBQUdKLGNBQWMsR0FBRyxDQUF6Qzs7QUFDQSxVQUFJeGxCLENBQUMsR0FBR25ILENBQUosR0FBUXdMLEtBQUssQ0FBQyxDQUFDdWUsTUFBTSxHQUFHaUMsS0FBVixJQUFtQmUscUJBQXBCLENBQWpCLEVBQTZEO0FBQzVEakMsYUFBSyxDQUFDLFVBQUQsQ0FBTDtBQUNBOztBQUVEa0IsV0FBSyxJQUFJLENBQUM3a0IsQ0FBQyxHQUFHbkgsQ0FBTCxJQUFVK3NCLHFCQUFuQjtBQUNBL3NCLE9BQUMsR0FBR21ILENBQUo7O0FBRUEsV0FBSzhYLENBQUMsR0FBRyxDQUFULEVBQVlBLENBQUMsR0FBR21OLFdBQWhCLEVBQTZCLEVBQUVuTixDQUEvQixFQUFrQztBQUNqQzZOLG9CQUFZLEdBQUdYLEtBQUssQ0FBQ2xOLENBQUQsQ0FBcEI7O0FBRUEsWUFBSTZOLFlBQVksR0FBRzlzQixDQUFmLElBQW9CLEVBQUVnc0IsS0FBRixHQUFVakMsTUFBbEMsRUFBMEM7QUFDekNlLGVBQUssQ0FBQyxVQUFELENBQUw7QUFDQTs7QUFFRCxZQUFJZ0MsWUFBWSxJQUFJOXNCLENBQXBCLEVBQXVCO0FBQ3RCO0FBQ0EsZUFBSzZzQixDQUFDLEdBQUdiLEtBQUosRUFBV25xQixDQUFDLEdBQUdtb0IsSUFBcEI7QUFBMEI7QUFBb0Jub0IsV0FBQyxJQUFJbW9CLElBQW5ELEVBQXlEO0FBQ3hEckwsYUFBQyxHQUFHOWMsQ0FBQyxJQUFJeXFCLElBQUwsR0FBWXJDLElBQVosR0FBb0Jwb0IsQ0FBQyxJQUFJeXFCLElBQUksR0FBR3BDLElBQVosR0FBbUJBLElBQW5CLEdBQTBCcm9CLENBQUMsR0FBR3lxQixJQUF0RDs7QUFDQSxnQkFBSU8sQ0FBQyxHQUFHbE8sQ0FBUixFQUFXO0FBQ1Y7QUFDQTs7QUFDRHFPLG1CQUFPLEdBQUdILENBQUMsR0FBR2xPLENBQWQ7QUFDQStOLHNCQUFVLEdBQUcxQyxJQUFJLEdBQUdyTCxDQUFwQjtBQUNBMk0sa0JBQU0sQ0FBQ3pzQixJQUFQLENBQ0Nnc0Isa0JBQWtCLENBQUNlLFlBQVksQ0FBQ2pOLENBQUMsR0FBR3FPLE9BQU8sR0FBR04sVUFBZixFQUEyQixDQUEzQixDQUFiLENBRG5CO0FBR0FHLGFBQUMsR0FBR3JoQixLQUFLLENBQUN3aEIsT0FBTyxHQUFHTixVQUFYLENBQVQ7QUFDQTs7QUFFRHBCLGdCQUFNLENBQUN6c0IsSUFBUCxDQUFZZ3NCLGtCQUFrQixDQUFDZSxZQUFZLENBQUNpQixDQUFELEVBQUksQ0FBSixDQUFiLENBQTlCO0FBQ0FQLGNBQUksR0FBR1AsS0FBSyxDQUFDQyxLQUFELEVBQVFlLHFCQUFSLEVBQStCSixjQUFjLElBQUlDLFdBQWpELENBQVo7QUFDQVosZUFBSyxHQUFHLENBQVI7QUFDQSxZQUFFVyxjQUFGO0FBQ0E7QUFDRDs7QUFFRCxRQUFFWCxLQUFGO0FBQ0EsUUFBRWhzQixDQUFGO0FBRUE7O0FBQ0QsV0FBT3NyQixNQUFNLENBQUNqbkIsSUFBUCxDQUFZLEVBQVosQ0FBUDtBQUNBO0FBRUQ7Ozs7Ozs7Ozs7Ozs7QUFXQSxXQUFTMlAsU0FBVCxDQUFtQm1ZLEtBQW5CLEVBQTBCO0FBQ3pCLFdBQU9sQixTQUFTLENBQUNrQixLQUFELEVBQVEsVUFBU2xHLE1BQVQsRUFBaUI7QUFDeEMsYUFBT3VFLGFBQWEsQ0FBQ2xxQixJQUFkLENBQW1CMmxCLE1BQW5CLElBQ0poUyxNQUFNLENBQUNnUyxNQUFNLENBQUM5bEIsS0FBUCxDQUFhLENBQWIsRUFBZ0JvRyxXQUFoQixFQUFELENBREYsR0FFSjBmLE1BRkg7QUFHQSxLQUplLENBQWhCO0FBS0E7QUFFRDs7Ozs7Ozs7Ozs7OztBQVdBLFdBQVN0UyxPQUFULENBQWlCd1ksS0FBakIsRUFBd0I7QUFDdkIsV0FBT2xCLFNBQVMsQ0FBQ2tCLEtBQUQsRUFBUSxVQUFTbEcsTUFBVCxFQUFpQjtBQUN4QyxhQUFPd0UsYUFBYSxDQUFDbnFCLElBQWQsQ0FBbUIybEIsTUFBbkIsSUFDSixTQUFTcFMsTUFBTSxDQUFDb1MsTUFBRCxDQURYLEdBRUpBLE1BRkg7QUFHQSxLQUplLENBQWhCO0FBS0E7QUFFRDs7QUFFQTs7O0FBQ0FsVCxVQUFRLEdBQUc7QUFDVjs7Ozs7QUFLQSxlQUFXLE9BTkQ7O0FBT1Y7Ozs7Ozs7QUFPQSxZQUFRO0FBQ1AsZ0JBQVVzWSxVQURIO0FBRVAsZ0JBQVVJO0FBRkgsS0FkRTtBQWtCVixjQUFVeFgsTUFsQkE7QUFtQlYsY0FBVUosTUFuQkE7QUFvQlYsZUFBV0YsT0FwQkQ7QUFxQlYsaUJBQWFLO0FBckJILEdBQVg7QUF3QkE7QUFDQTtBQUNBOztBQUNBLE1BQ0MsU0FDQSxRQUFPaVosb0dBQVAsS0FBcUIsUUFEckIsSUFFQUEsb0dBSEQsRUFJRTtBQUNEQSx1Q0FBbUIsWUFBVztBQUM3QixhQUFPbGEsUUFBUDtBQUNBLEtBRks7QUFBQSxvR0FBTjtBQUdBLEdBUkQsTUFRTyxJQUFJMFcsV0FBVyxJQUFJRSxVQUFuQixFQUErQjtBQUNyQyxRQUFJL3JCLE1BQU0sQ0FBQ0MsT0FBUCxJQUFrQjRyQixXQUF0QixFQUFtQztBQUNsQztBQUNBRSxnQkFBVSxDQUFDOXJCLE9BQVgsR0FBcUJrVixRQUFyQjtBQUNBLEtBSEQsTUFHTztBQUNOO0FBQ0EsV0FBS2xTLEdBQUwsSUFBWWtTLFFBQVosRUFBc0I7QUFDckJBLGdCQUFRLENBQUNqUixjQUFULENBQXdCakIsR0FBeEIsTUFBaUM0b0IsV0FBVyxDQUFDNW9CLEdBQUQsQ0FBWCxHQUFtQmtTLFFBQVEsQ0FBQ2xTLEdBQUQsQ0FBNUQ7QUFDQTtBQUNEO0FBQ0QsR0FWTSxNQVVBO0FBQ047QUFDQTJvQixRQUFJLENBQUN6VyxRQUFMLEdBQWdCQSxRQUFoQjtBQUNBO0FBRUQsQ0FuaEJDLEVBbWhCQSxJQW5oQkEsQ0FBRCxDOzs7Ozs7Ozs7Ozs7QUNERG5WLE1BQU0sQ0FBQ0MsT0FBUCxHQUFlLG9CQUFmLEM7Ozs7Ozs7Ozs7O0FDQUFELE1BQU0sQ0FBQ0MsT0FBUCxHQUFlLHVOQUFmLEM7Ozs7Ozs7Ozs7O0FDQUFELE1BQU0sQ0FBQ0MsT0FBUCxHQUFlLG8yREFBZixDOzs7Ozs7Ozs7OztBQ0FBRCxNQUFNLENBQUNDLE9BQVAsR0FBZSwwREFBZixDOzs7Ozs7Ozs7Ozs7QUNBYTs7QUFFYkEsT0FBTyxDQUFDcXZCLEdBQVIsR0FBYy90QixtQkFBTyxDQUFDLG1GQUFELENBQXJCO0FBQ0F0QixPQUFPLENBQUNzdkIsRUFBUixHQUFjaHVCLG1CQUFPLENBQUMsaUZBQUQsQ0FBckI7QUFDQXRCLE9BQU8sQ0FBQ3V2QixFQUFSLEdBQWNqdUIsbUJBQU8sQ0FBQyxpRkFBRCxDQUFyQjtBQUNBdEIsT0FBTyxDQUFDd3ZCLENBQVIsR0FBY2x1QixtQkFBTyxDQUFDLCtFQUFELENBQXJCO0FBQ0F0QixPQUFPLENBQUN5dkIsQ0FBUixHQUFjbnVCLG1CQUFPLENBQUMsK0VBQUQsQ0FBckIsQzs7Ozs7Ozs7Ozs7QUNOQXZCLE1BQU0sQ0FBQ0MsT0FBUCxHQUFlLGtJQUFmLEM7Ozs7Ozs7Ozs7OztBQ0FBO0FBQUE7QUFBQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRWU7QUFDZjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSx5QkFBeUI7QUFDekI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7O0FDakdBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7OztBQ0RBLElBQUkwdkIsQ0FBSixDLENBRUE7O0FBQ0FBLENBQUMsR0FBSSxZQUFXO0FBQ2YsU0FBTyxJQUFQO0FBQ0EsQ0FGRyxFQUFKOztBQUlBLElBQUk7QUFDSDtBQUNBQSxHQUFDLEdBQUdBLENBQUMsSUFBSSxJQUFJQyxRQUFKLENBQWEsYUFBYixHQUFUO0FBQ0EsQ0FIRCxDQUdFLE9BQU9DLENBQVAsRUFBVTtBQUNYO0FBQ0EsTUFBSSxRQUFPM0QsTUFBUCx5Q0FBT0EsTUFBUCxPQUFrQixRQUF0QixFQUFnQ3lELENBQUMsR0FBR3pELE1BQUo7QUFDaEMsQyxDQUVEO0FBQ0E7QUFDQTs7O0FBRUFsc0IsTUFBTSxDQUFDQyxPQUFQLEdBQWlCMHZCLENBQWpCLEM7Ozs7Ozs7Ozs7O0FDbkJBM3ZCLE1BQU0sQ0FBQ0MsT0FBUCxHQUFpQixVQUFTRCxNQUFULEVBQWlCO0FBQ2pDLE1BQUksQ0FBQ0EsTUFBTSxDQUFDOHZCLGVBQVosRUFBNkI7QUFDNUI5dkIsVUFBTSxDQUFDK3ZCLFNBQVAsR0FBbUIsWUFBVyxDQUFFLENBQWhDOztBQUNBL3ZCLFVBQU0sQ0FBQ2d3QixLQUFQLEdBQWUsRUFBZixDQUY0QixDQUc1Qjs7QUFDQSxRQUFJLENBQUNod0IsTUFBTSxDQUFDaWEsUUFBWixFQUFzQmphLE1BQU0sQ0FBQ2lhLFFBQVAsR0FBa0IsRUFBbEI7QUFDdEIxWixVQUFNLENBQUMwdkIsY0FBUCxDQUFzQmp3QixNQUF0QixFQUE4QixRQUE5QixFQUF3QztBQUN2Q2t3QixnQkFBVSxFQUFFLElBRDJCO0FBRXZDQyxTQUFHLEVBQUUsZUFBVztBQUNmLGVBQU9ud0IsTUFBTSxDQUFDdVksQ0FBZDtBQUNBO0FBSnNDLEtBQXhDO0FBTUFoWSxVQUFNLENBQUMwdkIsY0FBUCxDQUFzQmp3QixNQUF0QixFQUE4QixJQUE5QixFQUFvQztBQUNuQ2t3QixnQkFBVSxFQUFFLElBRHVCO0FBRW5DQyxTQUFHLEVBQUUsZUFBVztBQUNmLGVBQU9ud0IsTUFBTSxDQUFDSCxDQUFkO0FBQ0E7QUFKa0MsS0FBcEM7QUFNQUcsVUFBTSxDQUFDOHZCLGVBQVAsR0FBeUIsQ0FBekI7QUFDQTs7QUFDRCxTQUFPOXZCLE1BQVA7QUFDQSxDQXJCRCxDOzs7Ozs7Ozs7Ozs7QUNBQTtBQUFBO0FBQUE7QUFBQTtBQUNpRTtBQUNMOzs7QUFHNUQ7QUFDZ0c7QUFDaEcsZ0JBQWdCLDJHQUFVO0FBQzFCLEVBQUUsbUZBQU07QUFDUjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQSxJQUFJLEtBQVUsRUFBRSxZQVlmO0FBQ0Q7QUFDZSxnRjs7Ozs7Ozs7Ozs7O0FDakNmO0FBQUE7QUFBQSx3Q0FBeUwsQ0FBZ0IsaVBBQUcsRUFBQyxDOzs7Ozs7Ozs7OztBQ0E3TSxpRCIsImZpbGUiOiJNYXJrZG93bkJsb2NrLmpzIiwic291cmNlc0NvbnRlbnQiOlsiKGZ1bmN0aW9uIHdlYnBhY2tVbml2ZXJzYWxNb2R1bGVEZWZpbml0aW9uKHJvb3QsIGZhY3RvcnkpIHtcblx0aWYodHlwZW9mIGV4cG9ydHMgPT09ICdvYmplY3QnICYmIHR5cGVvZiBtb2R1bGUgPT09ICdvYmplY3QnKVxuXHRcdG1vZHVsZS5leHBvcnRzID0gZmFjdG9yeShyZXF1aXJlKFwidnVlXCIpKTtcblx0ZWxzZSBpZih0eXBlb2YgZGVmaW5lID09PSAnZnVuY3Rpb24nICYmIGRlZmluZS5hbWQpXG5cdFx0ZGVmaW5lKFwidnVlLWxpYlwiLCBbXCJ2dWVcIl0sIGZhY3RvcnkpO1xuXHRlbHNlIGlmKHR5cGVvZiBleHBvcnRzID09PSAnb2JqZWN0Jylcblx0XHRleHBvcnRzW1widnVlLWxpYlwiXSA9IGZhY3RvcnkocmVxdWlyZShcInZ1ZVwiKSk7XG5cdGVsc2Vcblx0XHRyb290W1widnVlLWxpYlwiXSA9IGZhY3Rvcnkocm9vdFtcInZ1ZVwiXSk7XG59KSh3aW5kb3csIGZ1bmN0aW9uKF9fV0VCUEFDS19FWFRFUk5BTF9NT0RVTEVfdnVlX18pIHtcbnJldHVybiAiLCIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9uIGZvciBoYXJtb255IGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuIFx0XHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIG5hbWUpKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIG5hbWUsIHsgZW51bWVyYWJsZTogdHJ1ZSwgZ2V0OiBnZXR0ZXIgfSk7XG4gXHRcdH1cbiBcdH07XG5cbiBcdC8vIGRlZmluZSBfX2VzTW9kdWxlIG9uIGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uciA9IGZ1bmN0aW9uKGV4cG9ydHMpIHtcbiBcdFx0aWYodHlwZW9mIFN5bWJvbCAhPT0gJ3VuZGVmaW5lZCcgJiYgU3ltYm9sLnRvU3RyaW5nVGFnKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFN5bWJvbC50b1N0cmluZ1RhZywgeyB2YWx1ZTogJ01vZHVsZScgfSk7XG4gXHRcdH1cbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdfX2VzTW9kdWxlJywgeyB2YWx1ZTogdHJ1ZSB9KTtcbiBcdH07XG5cbiBcdC8vIGNyZWF0ZSBhIGZha2UgbmFtZXNwYWNlIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDE6IHZhbHVlIGlzIGEgbW9kdWxlIGlkLCByZXF1aXJlIGl0XG4gXHQvLyBtb2RlICYgMjogbWVyZ2UgYWxsIHByb3BlcnRpZXMgb2YgdmFsdWUgaW50byB0aGUgbnNcbiBcdC8vIG1vZGUgJiA0OiByZXR1cm4gdmFsdWUgd2hlbiBhbHJlYWR5IG5zIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDh8MTogYmVoYXZlIGxpa2UgcmVxdWlyZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy50ID0gZnVuY3Rpb24odmFsdWUsIG1vZGUpIHtcbiBcdFx0aWYobW9kZSAmIDEpIHZhbHVlID0gX193ZWJwYWNrX3JlcXVpcmVfXyh2YWx1ZSk7XG4gXHRcdGlmKG1vZGUgJiA4KSByZXR1cm4gdmFsdWU7XG4gXHRcdGlmKChtb2RlICYgNCkgJiYgdHlwZW9mIHZhbHVlID09PSAnb2JqZWN0JyAmJiB2YWx1ZSAmJiB2YWx1ZS5fX2VzTW9kdWxlKSByZXR1cm4gdmFsdWU7XG4gXHRcdHZhciBucyA9IE9iamVjdC5jcmVhdGUobnVsbCk7XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18ucihucyk7XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShucywgJ2RlZmF1bHQnLCB7IGVudW1lcmFibGU6IHRydWUsIHZhbHVlOiB2YWx1ZSB9KTtcbiBcdFx0aWYobW9kZSAmIDIgJiYgdHlwZW9mIHZhbHVlICE9ICdzdHJpbmcnKSBmb3IodmFyIGtleSBpbiB2YWx1ZSkgX193ZWJwYWNrX3JlcXVpcmVfXy5kKG5zLCBrZXksIGZ1bmN0aW9uKGtleSkgeyByZXR1cm4gdmFsdWVba2V5XTsgfS5iaW5kKG51bGwsIGtleSkpO1xuIFx0XHRyZXR1cm4gbnM7XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gXCIuL3NyYy9jb21wb25lbnRzL01hcmtkb3duQmxvY2sudnVlXCIpO1xuIiwiZnVuY3Rpb24gX2FycmF5TGlrZVRvQXJyYXkoYXJyLCBsZW4pIHtcbiAgaWYgKGxlbiA9PSBudWxsIHx8IGxlbiA+IGFyci5sZW5ndGgpIGxlbiA9IGFyci5sZW5ndGg7XG5cbiAgZm9yICh2YXIgaSA9IDAsIGFycjIgPSBuZXcgQXJyYXkobGVuKTsgaSA8IGxlbjsgaSsrKSB7XG4gICAgYXJyMltpXSA9IGFycltpXTtcbiAgfVxuXG4gIHJldHVybiBhcnIyO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IF9hcnJheUxpa2VUb0FycmF5OyIsImZ1bmN0aW9uIF9hcnJheVdpdGhIb2xlcyhhcnIpIHtcbiAgaWYgKEFycmF5LmlzQXJyYXkoYXJyKSkgcmV0dXJuIGFycjtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBfYXJyYXlXaXRoSG9sZXM7IiwiZnVuY3Rpb24gX2l0ZXJhYmxlVG9BcnJheUxpbWl0KGFyciwgaSkge1xuICBpZiAodHlwZW9mIFN5bWJvbCA9PT0gXCJ1bmRlZmluZWRcIiB8fCAhKFN5bWJvbC5pdGVyYXRvciBpbiBPYmplY3QoYXJyKSkpIHJldHVybjtcbiAgdmFyIF9hcnIgPSBbXTtcbiAgdmFyIF9uID0gdHJ1ZTtcbiAgdmFyIF9kID0gZmFsc2U7XG4gIHZhciBfZSA9IHVuZGVmaW5lZDtcblxuICB0cnkge1xuICAgIGZvciAodmFyIF9pID0gYXJyW1N5bWJvbC5pdGVyYXRvcl0oKSwgX3M7ICEoX24gPSAoX3MgPSBfaS5uZXh0KCkpLmRvbmUpOyBfbiA9IHRydWUpIHtcbiAgICAgIF9hcnIucHVzaChfcy52YWx1ZSk7XG5cbiAgICAgIGlmIChpICYmIF9hcnIubGVuZ3RoID09PSBpKSBicmVhaztcbiAgICB9XG4gIH0gY2F0Y2ggKGVycikge1xuICAgIF9kID0gdHJ1ZTtcbiAgICBfZSA9IGVycjtcbiAgfSBmaW5hbGx5IHtcbiAgICB0cnkge1xuICAgICAgaWYgKCFfbiAmJiBfaVtcInJldHVyblwiXSAhPSBudWxsKSBfaVtcInJldHVyblwiXSgpO1xuICAgIH0gZmluYWxseSB7XG4gICAgICBpZiAoX2QpIHRocm93IF9lO1xuICAgIH1cbiAgfVxuXG4gIHJldHVybiBfYXJyO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IF9pdGVyYWJsZVRvQXJyYXlMaW1pdDsiLCJmdW5jdGlvbiBfbm9uSXRlcmFibGVSZXN0KCkge1xuICB0aHJvdyBuZXcgVHlwZUVycm9yKFwiSW52YWxpZCBhdHRlbXB0IHRvIGRlc3RydWN0dXJlIG5vbi1pdGVyYWJsZSBpbnN0YW5jZS5cXG5JbiBvcmRlciB0byBiZSBpdGVyYWJsZSwgbm9uLWFycmF5IG9iamVjdHMgbXVzdCBoYXZlIGEgW1N5bWJvbC5pdGVyYXRvcl0oKSBtZXRob2QuXCIpO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IF9ub25JdGVyYWJsZVJlc3Q7IiwidmFyIGFycmF5V2l0aEhvbGVzID0gcmVxdWlyZShcIi4vYXJyYXlXaXRoSG9sZXNcIik7XG5cbnZhciBpdGVyYWJsZVRvQXJyYXlMaW1pdCA9IHJlcXVpcmUoXCIuL2l0ZXJhYmxlVG9BcnJheUxpbWl0XCIpO1xuXG52YXIgdW5zdXBwb3J0ZWRJdGVyYWJsZVRvQXJyYXkgPSByZXF1aXJlKFwiLi91bnN1cHBvcnRlZEl0ZXJhYmxlVG9BcnJheVwiKTtcblxudmFyIG5vbkl0ZXJhYmxlUmVzdCA9IHJlcXVpcmUoXCIuL25vbkl0ZXJhYmxlUmVzdFwiKTtcblxuZnVuY3Rpb24gX3NsaWNlZFRvQXJyYXkoYXJyLCBpKSB7XG4gIHJldHVybiBhcnJheVdpdGhIb2xlcyhhcnIpIHx8IGl0ZXJhYmxlVG9BcnJheUxpbWl0KGFyciwgaSkgfHwgdW5zdXBwb3J0ZWRJdGVyYWJsZVRvQXJyYXkoYXJyLCBpKSB8fCBub25JdGVyYWJsZVJlc3QoKTtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBfc2xpY2VkVG9BcnJheTsiLCJmdW5jdGlvbiBfdHlwZW9mKG9iaikge1xuICBcIkBiYWJlbC9oZWxwZXJzIC0gdHlwZW9mXCI7XG5cbiAgaWYgKHR5cGVvZiBTeW1ib2wgPT09IFwiZnVuY3Rpb25cIiAmJiB0eXBlb2YgU3ltYm9sLml0ZXJhdG9yID09PSBcInN5bWJvbFwiKSB7XG4gICAgbW9kdWxlLmV4cG9ydHMgPSBfdHlwZW9mID0gZnVuY3Rpb24gX3R5cGVvZihvYmopIHtcbiAgICAgIHJldHVybiB0eXBlb2Ygb2JqO1xuICAgIH07XG4gIH0gZWxzZSB7XG4gICAgbW9kdWxlLmV4cG9ydHMgPSBfdHlwZW9mID0gZnVuY3Rpb24gX3R5cGVvZihvYmopIHtcbiAgICAgIHJldHVybiBvYmogJiYgdHlwZW9mIFN5bWJvbCA9PT0gXCJmdW5jdGlvblwiICYmIG9iai5jb25zdHJ1Y3RvciA9PT0gU3ltYm9sICYmIG9iaiAhPT0gU3ltYm9sLnByb3RvdHlwZSA/IFwic3ltYm9sXCIgOiB0eXBlb2Ygb2JqO1xuICAgIH07XG4gIH1cblxuICByZXR1cm4gX3R5cGVvZihvYmopO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IF90eXBlb2Y7IiwidmFyIGFycmF5TGlrZVRvQXJyYXkgPSByZXF1aXJlKFwiLi9hcnJheUxpa2VUb0FycmF5XCIpO1xuXG5mdW5jdGlvbiBfdW5zdXBwb3J0ZWRJdGVyYWJsZVRvQXJyYXkobywgbWluTGVuKSB7XG4gIGlmICghbykgcmV0dXJuO1xuICBpZiAodHlwZW9mIG8gPT09IFwic3RyaW5nXCIpIHJldHVybiBhcnJheUxpa2VUb0FycmF5KG8sIG1pbkxlbik7XG4gIHZhciBuID0gT2JqZWN0LnByb3RvdHlwZS50b1N0cmluZy5jYWxsKG8pLnNsaWNlKDgsIC0xKTtcbiAgaWYgKG4gPT09IFwiT2JqZWN0XCIgJiYgby5jb25zdHJ1Y3RvcikgbiA9IG8uY29uc3RydWN0b3IubmFtZTtcbiAgaWYgKG4gPT09IFwiTWFwXCIgfHwgbiA9PT0gXCJTZXRcIikgcmV0dXJuIEFycmF5LmZyb20obyk7XG4gIGlmIChuID09PSBcIkFyZ3VtZW50c1wiIHx8IC9eKD86VWl8SSludCg/Ojh8MTZ8MzIpKD86Q2xhbXBlZCk/QXJyYXkkLy50ZXN0KG4pKSByZXR1cm4gYXJyYXlMaWtlVG9BcnJheShvLCBtaW5MZW4pO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IF91bnN1cHBvcnRlZEl0ZXJhYmxlVG9BcnJheTsiLCI8c2NyaXB0PlxuaW1wb3J0IFZ1ZSBmcm9tICd2dWUnXG5pbXBvcnQgTWFya2Rvd25JdCBmcm9tICdtYXJrZG93bi1pdCdcbmltcG9ydCBtaWxhIGZyb20gJ21hcmtkb3duLWl0LWxpbmstYXR0cmlidXRlcydcbmltcG9ydCBjdXN0b21CbG9jayBmcm9tICdtYXJrZG93bi1pdC1jdXN0b20tYmxvY2snXG5pbXBvcnQgY29udGFpbmVyIGZyb20gJ21hcmtkb3duLWl0LWNvbnRhaW5lcidcbi8qKlxuICogQSBjb21wb25lbnQgdGhhdCByZW5kZXJzIG1hcmtkb3duIHRleHQgaW4gVnVlIHVzaW5nICoqbWFya2Rvd24taXQqKiBhbmQgc29tZSBvZiBpdHMgcGx1Z2lucy5cbiAqL1xuZXhwb3J0IGRlZmF1bHQge1xuICBwcm9wczoge1xuICAgIC8qKlxuICAgICAqIFRoZSB0ZXh0IHRvIGJlIGRpc3BsYXllZCwgc2hvdWxkIGJlIGZvcm1hdHRlZCBpbiBtYXJrZG93biBzeW50YXhcbiAgICAgKi9cbiAgICB0ZXh0OiB7XG4gICAgICB0eXBlOiBTdHJpbmcsXG4gICAgICByZXF1aXJlZDogdHJ1ZSxcbiAgICAgIGRlZmF1bHQ6ICcnXG4gICAgfSxcbiAgICAvKipcbiAgICAgKiBEZWNpZGVzIHdoZXRoZXIgYmxvY2sgdGFncyBhcmUgcmVuZGVyZWQgb3Igbm90LCBleGFtcGxlOlxuICAgICAqXG4gICAgICogaW5wdXRcbiAgICAgKlxuICAgICAqIGBgYG1kXG4gICAgICogc29tZSBwYXJhZ3JhcGhcbiAgICAgKlxuICAgICAqIHNvbWUgb3RoZXIgcGFyYWdyYXBoXG4gICAgICogYGBgXG4gICAgICpcbiAgICAgKiBvdXRwdXQgd2l0aCB0cnVlXG4gICAgICpcbiAgICAgKiBgYGBodG1sXG4gICAgICogJmx0O2RpdiZndDtcbiAgICAgKiAgICZsdDtwJmd0O3NvbWUgcGFyYWdyYXBoJmx0Oy9wJmd0O1xuICAgICAqICAgJmx0O3AmZ3Q7c29tZSBvdGhlciBwYXJhZ3JhcGgmbHQ7L3AmZ3Q7XG4gICAgICogJmx0Oy9kaXYmZ3Q7XG4gICAgICogYGBgXG4gICAgICpcbiAgICAgKiBvdXRwdXQgd2l0aCBmYWxzZVxuICAgICAqXG4gICAgICogYGBgaHRtbFxuICAgICAqICZsdDtkaXYmZ3Q7c29tZSBwYXJhZ3JhcGggc29tZSBvdGhlciBwYXJhZ3JhcGgmbHQ7L2RpdiZndDtcbiAgICAgKiBgYGBcbiAgICAgKiBAc2VlIFttYXJrZG93bi1pdCBpbmxpbmUgcmVuZGVyaW5nXShodHRwczovL2dpdGh1Yi5jb20vbWFya2Rvd24taXQvbWFya2Rvd24taXQjc2ltcGxlKVxuICAgICAqL1xuICAgIGlubGluZToge1xuICAgICAgdHlwZTogQm9vbGVhbixcbiAgICAgIGRlZmF1bHQ6IGZhbHNlXG4gICAgfSxcbiAgICAvKipcbiAgICAgKiBUaGUgYmFzZSByb290IHRhZyBvZiB0aGlzIGVsZW1lbnQsIGNhbiBiZSBhbnkgdGFnIChkaXYsIHNwYW4sIHAsIGV0YylcbiAgICAgKlxuICAgICAqIFNob3VsZCBiZSBhIHZhbGlkIGh0bWwgdGFnLlxuICAgICAqL1xuICAgIHRhZzoge1xuICAgICAgdHlwZTogU3RyaW5nLFxuICAgICAgZGVmYXVsdDogJ2RpdidcbiAgICB9LFxuICAgIC8qKlxuICAgICAqIEF1dG9jb252ZXJ0IFVSTC1saWtlIHRleHQgdG8gbGlua3NcbiAgICAgKlxuICAgICAqIEBzZWUgW21hcmtkb3duLWl0IGRvY3NdKGh0dHBzOi8vZ2l0aHViLmNvbS9tYXJrZG93bi1pdC9tYXJrZG93bi1pdCNpbml0LXdpdGgtcHJlc2V0cy1hbmQtb3B0aW9ucylcbiAgICAgKi9cbiAgICBsaW5raWZ5OiB7XG4gICAgICB0eXBlOiBCb29sZWFuLFxuICAgICAgZGVmYXVsdDogZmFsc2VcbiAgICB9LFxuICAgIC8qKlxuICAgICAqIEVuYWJsZSBzb21lIGxhbmd1YWdlLW5ldXRyYWwgcmVwbGFjZW1lbnQgKyBxdW90ZXMgYmVhdXRpZmljYXRpb25cbiAgICAgKlxuICAgICAqIEBzZWUgW21hcmtkb3duLWl0IGRvY3NdKGh0dHBzOi8vZ2l0aHViLmNvbS9tYXJrZG93bi1pdC9tYXJrZG93bi1pdCNpbml0LXdpdGgtcHJlc2V0cy1hbmQtb3B0aW9ucylcbiAgICAgKi9cbiAgICB0eXBvZ3JhcGhlcjoge1xuICAgICAgdHlwZTogQm9vbGVhbixcbiAgICAgIGRlZmF1bHQ6IGZhbHNlXG4gICAgfSxcbiAgICAvKipcbiAgICAgKiBIeXBlcmxpbmtzIEF0dHJpYnV0ZXMsIGFwcGxpZWQgdG8gYWxsIGh5cGVybGlua3MuXG4gICAgICpcbiAgICAgKiBUaGlzIHVzZXMgKiptYXJrZG93bi1pdC1saW5rLWF0dHJpYnV0ZXMqKiwgdGhlIG9iamVjdCBpcyB0aGUgY29uZmlndXJhdGlvbiBmb3IgdGhlIHBsdWdpblxuICAgICAqXG4gICAgICogQHNlZSBbbWFya2Rvd24taXQtbGluay1hdHRyaWJ1dGVzIGRvY3NdKGh0dHBzOi8vZ2l0aHViLmNvbS9jcm9va2VkbmVpZ2hib3IvbWFya2Rvd24taXQtbGluay1hdHRyaWJ1dGVzKVxuICAgICAqL1xuICAgIGxpbmtzQXR0cmlidXRlczoge1xuICAgICAgdHlwZTogT2JqZWN0LFxuICAgICAgZGVmYXVsdCAoKSB7XG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgYXR0cnM6IHtcbiAgICAgICAgICAgIHRhcmdldDogJ19ibGFuaycsXG4gICAgICAgICAgICByZWw6ICdub29wZW5lcidcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9LFxuICAgIC8qKlxuICAgICAqIE9iamVjdCBjb250YWluaW5nIGFueSBudW1iZXIgb2YgKiptYXJrZG93bi1pdC1jdXN0b20tYmxvY2sqKiBmdW5jdGlvbnNcbiAgICAgKlxuICAgICAqIEVhY2ggZnVuY3Rpb24gc2hvdWxkIGhhdmU6XG4gICAgICpcbiAgICAgKiAqIHRoZSBuYW1lIG9mIHRoZSBjdXN0b20gYmxvY2sgYXMgZnVuY3Rpb24gbmFtZVxuICAgICAqICogcGFyc2VyIGZvciB0aGUgY3VzdG9tIGJsb2NrIGFzIHRoZSBib2R5IG9mIHRoZSBmdW5jdGlvblxuICAgICAqXG4gICAgICogQHNlZSBbbWFya2Rvd24taXQtY3VzdG9tLWJsb2NrIGRvY3NdKGh0dHBzOi8vZ2l0aHViLmNvbS9wb3N2YS9tYXJrZG93bi1pdC1jdXN0b20tYmxvY2spXG4gICAgICovXG4gICAgY3VzdG9tQmxvY2tzOiB7XG4gICAgICB0eXBlOiBPYmplY3QsXG4gICAgICBkZWZhdWx0ICgpIHtcbiAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAnbWFyaycgKGFyZykge1xuICAgICAgICAgICAgcmV0dXJuIGA8bWFyaz4ke2FyZ308L21hcms+YFxuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfVxuICAgIH0sXG4gICAgLyoqXG4gICAgICogT2JqZWN0IGNvbnRhaW5pbmcgYW55IG51bWJlciBvZiAqKm1hcmtkb3duLWl0LWNvbnRhaW5lcioqXG4gICAgICogY29uZmlndXJhdGlvbiBvYmplY3RzIGluZGV4ZWQgYnkgdGhlaXIgY29udGFpbmVyIG5hbWUsIGxpa2UgdGhpczpcbiAgICAgKlxuICAgICAqIHwgICAgICAga2V5ICAgICAgfCAgICAgICAgICAgIHZhbHVlICAgICAgICAgICAgICAgfFxuICAgICAqIHwtLS0tLS0tLS0tLS0tLS0tfC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tfFxuICAgICAqIHwgY29udGFpbmVyLW5hbWUgfCB7IHZhbGlkYXRlOiAuLi4sIHJlbmRlcjogLi4uIH0gfFxuICAgICAqXG4gICAgICogbWFya2Rvd24taXQtY29udGFpbmVyIGlzIGEgcGx1Z2luIGZvciBjcmVhdGluZyBibG9jay1sZXZlbCBjdXN0b20gY29udGFpbmVycyBmb3IgbWFya2Rvd24taXQgbWFya2Rvd24gcGFyc2VyLlxuICAgICAqXG4gICAgICogQHNlZSBbbWFya2Rvd24taXQtY29udGFpbmVyIGRvY3NdKGh0dHBzOi8vZ2l0aHViLmNvbS9tYXJrZG93bi1pdC9tYXJrZG93bi1pdC1jb250YWluZXIpXG4gICAgICovXG4gICAgY3VzdG9tQ29udGFpbmVyczoge1xuICAgICAgdHlwZTogT2JqZWN0LFxuICAgICAgZGVmYXVsdCAoKSB7XG4gICAgICAgIHJldHVybiB7fVxuICAgICAgfVxuICAgIH1cbiAgfSxcblxuICBjcmVhdGVkICgpIHtcbiAgICB0aGlzLmJ1aWxkTWQoKVxuICB9LFxuXG4gIHJlbmRlciAoY3JlYXRlRWxlbWVudCkge1xuICAgIGNvbnN0IHQgPSB0aGlzLmdldFRlbXBsYXRlKHRoaXMuJHByb3BzLnRleHQsIHRoaXMuJHByb3BzLmlubGluZSwgdGhpcy4kcHJvcHMudGFnKVxuICAgIGNvbnN0IGNvbXBpbGVkID0gVnVlLmNvbXBpbGUodClcblxuICAgIC8vIGNyZWF0ZSBhIG5ldyBzY29wZSBmb3IgdGhlIGNvbXBpbGVkIG1hcmtkb3duIHRlbXBsYXRlXG4gICAgY29uc3Qgc2NvcGUgPSB7XG4gICAgICBfYzogdGhpcy4kcGFyZW50Ll9jLFxuICAgICAgX3Y6IHRoaXMuJHBhcmVudC5fdixcbiAgICAgIF9tOiB0aGlzLiRwYXJlbnQuX20sXG4gICAgICBfc3RhdGljVHJlZXM6IFtdLFxuICAgICAgX3JlbmRlclByb3h5OiB0aGlzLiRwYXJlbnQuX3JlbmRlclByb3h5LFxuICAgICAgJG9wdGlvbnM6IHtcbiAgICAgICAgc3RhdGljUmVuZGVyRm5zOiBjb21waWxlZC5zdGF0aWNSZW5kZXJGbnNcbiAgICAgIH1cbiAgICB9XG4gICAgLy8gY29uc29sZS5sb2coJ3JlbmRlcmluZyBtYXJrZG93bicpXG4gICAgcmV0dXJuIGNyZWF0ZUVsZW1lbnQoeyBuYW1lOiAnTWFya2Rvd25CbG9jaycsIHJlbmRlcjogY29tcGlsZWQucmVuZGVyLmJpbmQoc2NvcGUpIH0pXG4gIH0sXG5cbiAgd2F0Y2g6IHtcbiAgICBsaW5raWZ5ICh2YWx1ZSkge1xuICAgICAgdGhpcy5idWlsZE1kKClcbiAgICAgIHRoaXMuJGZvcmNlVXBkYXRlKClcbiAgICB9LFxuICAgIHR5cG9ncmFwaGVyICh2YWx1ZSkge1xuICAgICAgdGhpcy5idWlsZE1kKClcbiAgICAgIHRoaXMuJGZvcmNlVXBkYXRlKClcbiAgICB9LFxuICAgIGxpbmtzQXR0cmlidXRlcyAodmFsdWUpIHtcbiAgICAgIHRoaXMuYnVpbGRNZCgpXG4gICAgICB0aGlzLiRmb3JjZVVwZGF0ZSgpXG4gICAgfSxcbiAgICBjdXN0b21CbG9ja3MgKHZhbHVlKSB7XG4gICAgICB0aGlzLmJ1aWxkTWQoKVxuICAgICAgdGhpcy4kZm9yY2VVcGRhdGUoKVxuICAgIH0sXG4gICAgY3VzdG9tQ29udGFpbmVycyAodmFsdWUpIHtcbiAgICAgIHRoaXMuYnVpbGRNZCgpXG4gICAgICB0aGlzLiRmb3JjZVVwZGF0ZSgpXG4gICAgfVxuICB9LFxuXG4gIG1ldGhvZHM6IHtcbiAgICBidWlsZE1kICgpIHtcbiAgICAgIHRoaXMubWQgPSBuZXcgTWFya2Rvd25JdCh7XG4gICAgICAgIGxpbmtpZnk6IHRoaXMubGlua2lmeSxcbiAgICAgICAgdHlwb2dyYXBoZXI6IHRoaXMudHlwb2dyYXBoZXJcbiAgICAgIH0pXG4gICAgICB0aGlzLm1kLnVzZShtaWxhLCB0aGlzLmxpbmtzQXR0cmlidXRlcylcbiAgICAgIHRoaXMubWQudXNlKGN1c3RvbUJsb2NrLCB0aGlzLmN1c3RvbUJsb2NrcylcblxuICAgICAgaWYgKHRoaXMuY3VzdG9tQ29udGFpbmVycykge1xuICAgICAgICBPYmplY3Qua2V5cyh0aGlzLmN1c3RvbUNvbnRhaW5lcnMpLmZvckVhY2goa2V5ID0+IHtcbiAgICAgICAgICB0aGlzLm1kLnVzZShjb250YWluZXIsIGtleSwgdGhpcy5jdXN0b21Db250YWluZXJzW2tleV0pXG4gICAgICAgIH0pXG4gICAgICB9XG4gICAgfSxcbiAgICBnZXRUZW1wbGF0ZSAodGV4dCwgaW5saW5lID0gZmFsc2UsIHJvb3RUYWcgPSAnc3BhbicpIHtcbiAgICAgIC8vIGNoZWNrIGlmIHJvb3QgdGFnIGV4aXN0cyB3aGVuIGlubGluZSA9IHRydWVcbiAgICAgIGlmIChpbmxpbmUgPT09IHRydWUgJiYgIXJvb3RUYWcpIHtcbiAgICAgICAgdGhyb3cgbmV3IEVycm9yKCdbTWFya2Rvd25QYXJzZXJdIFdoZW4gdXNpbmcgaW5saW5lIG9wdGlvbiB5b3UgbXVzdCBzcGVjaWZ5IGEgcm9vdCB0YWcnKVxuICAgICAgfVxuICAgICAgbGV0IHRlbXBsYXRlTWFya2Rvd25cbiAgICAgIGlmIChpbmxpbmUpIHtcbiAgICAgICAgLy8gd2UgcGFyc2UgdGhpcyB3aXRoIG1hcmtkb3duXG4gICAgICAgIC8vIGFuZCBhZGQgYSByb290IHRhZyBvZiBvdXIgY2hvaWNlXG4gICAgICAgIHRlbXBsYXRlTWFya2Rvd24gPSAocm9vdFRhZyA/ICc8JyArIHJvb3RUYWcgKyAnPicgOiAnJykgKyB0aGlzLm1kLnJlbmRlcklubGluZSh0ZXh0KSArIChyb290VGFnID8gJzwvJyArIHJvb3RUYWcgKyAnPicgOiAnJylcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIC8vIG9yIHVzZSB0aGlzIGlmIHlvdSBoYXZlIG11bHRpbGluZSBlbGVtZW50cyBvciB5b3Ugd2FudCBhIDxwPiB0YWcgYXMgcm9vdCBvZiBhIHNpbmdsZSBsaW5lXG4gICAgICAgIHRlbXBsYXRlTWFya2Rvd24gPSAocm9vdFRhZyA/ICc8JyArIHJvb3RUYWcgKyAnPicgOiAnJykgKyB0aGlzLm1kLnJlbmRlcih0ZXh0KSArIChyb290VGFnID8gJzwvJyArIHJvb3RUYWcgKyAnPicgOiAnJylcbiAgICAgIH1cblxuICAgICAgLy8gUVVJQ0sgSEFDSzpcbiAgICAgIC8vIFtFbHNlXShTb21ldGhpbmcgXCJPcHRpb25hbFwiKSB1c3VhbGx5IGdldCBjb21waWxlZCB0byBodG1sIGxpbmtzXG4gICAgICAvLyBzbyB3ZSByZXBsYWNlIGFueSA8YSBocmVmPVwiU29tZXRoaW5nXCIgdGl0bGU9XCJPcHRpb25hbFwiPkVsc2U8L2E+XG5cbiAgICAgIC8vIHdpdGggPHJvdXRlci1saW5rIDp0bz1cIntuYW1lOiAnU29tZXRoaW5nJyBwYXJhbXM6IHtPcHRpb25hbH19XCI+RWxzZTwvcm91dGVyLWxpbms+XG4gICAgICAvLyBOT1RFOiB0aGlzIHNob3VsZCBsZWF2ZSBhbG9uZSBsaW5rcyB3aWNoIGNvbnRhaW5zIG5vbi13b3JkIGNoYXJhY3RlcnMgbGlrZSAuIG9yIC8gc28gZXh0ZXJuYWwgbGlua3Mgc3RpbGwgd29ya1xuICAgICAgLy8gY29uc3QgdGVtcGxhdGUgPSB0ZW1wbGF0ZU1hcmtkb3duLnJlcGxhY2UoLzxhIGhyZWY9XCIoW1xcd10rKVwiWyBdKih0aXRsZT1cIihbXlwiXSopXCIpP1tePl0qPihbXjxdKyk8XFwvYT4vaWdtLCAobWF0Y2gsIHAxLCBwMiwgcDMsIHA0KSA9PiB7XG4gICAgICAvLyAgIHJldHVybiAnPHJvdXRlci1saW5rIDp0bz1cIntuYW1lOiBcXCcnICsgcDEgKyAnXFwnJyArIChwMiA/ICcsIHBhcmFtczogeycgKyBwMyArICd9JyA6ICcnKSArICd9XCI+JyArIHA0ICsgJzwvcm91dGVyLWxpbms+J1xuICAgICAgLy8gfSlcbiAgICAgIC8vIHJldHVybiBhIG5ldyBjb21wb25lbnQgc28gdGhhdCByb3V0ZXItbGlua3MgZ2V0IHBhcnNlZCBhbmQgcmVuZGVyZWRcbiAgICAgIHJldHVybiB0ZW1wbGF0ZU1hcmtkb3duXG4gICAgfVxuICB9XG59XG48L3NjcmlwdD5cbiIsIid1c2Ugc3RyaWN0JztcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gSGVscGVyc1xuXG4vLyBNZXJnZSBvYmplY3RzXG4vL1xuZnVuY3Rpb24gYXNzaWduKG9iaiAvKmZyb20xLCBmcm9tMiwgZnJvbTMsIC4uLiovKSB7XG4gIHZhciBzb3VyY2VzID0gQXJyYXkucHJvdG90eXBlLnNsaWNlLmNhbGwoYXJndW1lbnRzLCAxKTtcblxuICBzb3VyY2VzLmZvckVhY2goZnVuY3Rpb24gKHNvdXJjZSkge1xuICAgIGlmICghc291cmNlKSB7IHJldHVybjsgfVxuXG4gICAgT2JqZWN0LmtleXMoc291cmNlKS5mb3JFYWNoKGZ1bmN0aW9uIChrZXkpIHtcbiAgICAgIG9ialtrZXldID0gc291cmNlW2tleV07XG4gICAgfSk7XG4gIH0pO1xuXG4gIHJldHVybiBvYmo7XG59XG5cbmZ1bmN0aW9uIF9jbGFzcyhvYmopIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUudG9TdHJpbmcuY2FsbChvYmopOyB9XG5mdW5jdGlvbiBpc1N0cmluZyhvYmopIHsgcmV0dXJuIF9jbGFzcyhvYmopID09PSAnW29iamVjdCBTdHJpbmddJzsgfVxuZnVuY3Rpb24gaXNPYmplY3Qob2JqKSB7IHJldHVybiBfY2xhc3Mob2JqKSA9PT0gJ1tvYmplY3QgT2JqZWN0XSc7IH1cbmZ1bmN0aW9uIGlzUmVnRXhwKG9iaikgeyByZXR1cm4gX2NsYXNzKG9iaikgPT09ICdbb2JqZWN0IFJlZ0V4cF0nOyB9XG5mdW5jdGlvbiBpc0Z1bmN0aW9uKG9iaikgeyByZXR1cm4gX2NsYXNzKG9iaikgPT09ICdbb2JqZWN0IEZ1bmN0aW9uXSc7IH1cblxuXG5mdW5jdGlvbiBlc2NhcGVSRShzdHIpIHsgcmV0dXJuIHN0ci5yZXBsYWNlKC9bLj8qK14kW1xcXVxcXFwoKXt9fC1dL2csICdcXFxcJCYnKTsgfVxuXG4vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vL1xuXG5cbnZhciBkZWZhdWx0T3B0aW9ucyA9IHtcbiAgZnV6enlMaW5rOiB0cnVlLFxuICBmdXp6eUVtYWlsOiB0cnVlLFxuICBmdXp6eUlQOiBmYWxzZVxufTtcblxuXG5mdW5jdGlvbiBpc09wdGlvbnNPYmoob2JqKSB7XG4gIHJldHVybiBPYmplY3Qua2V5cyhvYmogfHwge30pLnJlZHVjZShmdW5jdGlvbiAoYWNjLCBrKSB7XG4gICAgcmV0dXJuIGFjYyB8fCBkZWZhdWx0T3B0aW9ucy5oYXNPd25Qcm9wZXJ0eShrKTtcbiAgfSwgZmFsc2UpO1xufVxuXG5cbnZhciBkZWZhdWx0U2NoZW1hcyA9IHtcbiAgJ2h0dHA6Jzoge1xuICAgIHZhbGlkYXRlOiBmdW5jdGlvbiAodGV4dCwgcG9zLCBzZWxmKSB7XG4gICAgICB2YXIgdGFpbCA9IHRleHQuc2xpY2UocG9zKTtcblxuICAgICAgaWYgKCFzZWxmLnJlLmh0dHApIHtcbiAgICAgICAgLy8gY29tcGlsZSBsYXppbHksIGJlY2F1c2UgXCJob3N0XCItY29udGFpbmluZyB2YXJpYWJsZXMgY2FuIGNoYW5nZSBvbiB0bGRzIHVwZGF0ZS5cbiAgICAgICAgc2VsZi5yZS5odHRwID0gIG5ldyBSZWdFeHAoXG4gICAgICAgICAgJ15cXFxcL1xcXFwvJyArIHNlbGYucmUuc3JjX2F1dGggKyBzZWxmLnJlLnNyY19ob3N0X3BvcnRfc3RyaWN0ICsgc2VsZi5yZS5zcmNfcGF0aCwgJ2knXG4gICAgICAgICk7XG4gICAgICB9XG4gICAgICBpZiAoc2VsZi5yZS5odHRwLnRlc3QodGFpbCkpIHtcbiAgICAgICAgcmV0dXJuIHRhaWwubWF0Y2goc2VsZi5yZS5odHRwKVswXS5sZW5ndGg7XG4gICAgICB9XG4gICAgICByZXR1cm4gMDtcbiAgICB9XG4gIH0sXG4gICdodHRwczonOiAgJ2h0dHA6JyxcbiAgJ2Z0cDonOiAgICAnaHR0cDonLFxuICAnLy8nOiAgICAgIHtcbiAgICB2YWxpZGF0ZTogZnVuY3Rpb24gKHRleHQsIHBvcywgc2VsZikge1xuICAgICAgdmFyIHRhaWwgPSB0ZXh0LnNsaWNlKHBvcyk7XG5cbiAgICAgIGlmICghc2VsZi5yZS5ub19odHRwKSB7XG4gICAgICAvLyBjb21waWxlIGxhemlseSwgYmVjYXVzZSBcImhvc3RcIi1jb250YWluaW5nIHZhcmlhYmxlcyBjYW4gY2hhbmdlIG9uIHRsZHMgdXBkYXRlLlxuICAgICAgICBzZWxmLnJlLm5vX2h0dHAgPSAgbmV3IFJlZ0V4cChcbiAgICAgICAgICAnXicgK1xuICAgICAgICAgIHNlbGYucmUuc3JjX2F1dGggK1xuICAgICAgICAgIC8vIERvbid0IGFsbG93IHNpbmdsZS1sZXZlbCBkb21haW5zLCBiZWNhdXNlIG9mIGZhbHNlIHBvc2l0aXZlcyBsaWtlICcvL3Rlc3QnXG4gICAgICAgICAgLy8gd2l0aCBjb2RlIGNvbW1lbnRzXG4gICAgICAgICAgJyg/OmxvY2FsaG9zdHwoPzooPzonICsgc2VsZi5yZS5zcmNfZG9tYWluICsgJylcXFxcLikrJyArIHNlbGYucmUuc3JjX2RvbWFpbl9yb290ICsgJyknICtcbiAgICAgICAgICBzZWxmLnJlLnNyY19wb3J0ICtcbiAgICAgICAgICBzZWxmLnJlLnNyY19ob3N0X3Rlcm1pbmF0b3IgK1xuICAgICAgICAgIHNlbGYucmUuc3JjX3BhdGgsXG5cbiAgICAgICAgICAnaSdcbiAgICAgICAgKTtcbiAgICAgIH1cblxuICAgICAgaWYgKHNlbGYucmUubm9faHR0cC50ZXN0KHRhaWwpKSB7XG4gICAgICAgIC8vIHNob3VsZCBub3QgYmUgYDovL2AgJiBgLy8vYCwgdGhhdCBwcm90ZWN0cyBmcm9tIGVycm9ycyBpbiBwcm90b2NvbCBuYW1lXG4gICAgICAgIGlmIChwb3MgPj0gMyAmJiB0ZXh0W3BvcyAtIDNdID09PSAnOicpIHsgcmV0dXJuIDA7IH1cbiAgICAgICAgaWYgKHBvcyA+PSAzICYmIHRleHRbcG9zIC0gM10gPT09ICcvJykgeyByZXR1cm4gMDsgfVxuICAgICAgICByZXR1cm4gdGFpbC5tYXRjaChzZWxmLnJlLm5vX2h0dHApWzBdLmxlbmd0aDtcbiAgICAgIH1cbiAgICAgIHJldHVybiAwO1xuICAgIH1cbiAgfSxcbiAgJ21haWx0bzonOiB7XG4gICAgdmFsaWRhdGU6IGZ1bmN0aW9uICh0ZXh0LCBwb3MsIHNlbGYpIHtcbiAgICAgIHZhciB0YWlsID0gdGV4dC5zbGljZShwb3MpO1xuXG4gICAgICBpZiAoIXNlbGYucmUubWFpbHRvKSB7XG4gICAgICAgIHNlbGYucmUubWFpbHRvID0gIG5ldyBSZWdFeHAoXG4gICAgICAgICAgJ14nICsgc2VsZi5yZS5zcmNfZW1haWxfbmFtZSArICdAJyArIHNlbGYucmUuc3JjX2hvc3Rfc3RyaWN0LCAnaSdcbiAgICAgICAgKTtcbiAgICAgIH1cbiAgICAgIGlmIChzZWxmLnJlLm1haWx0by50ZXN0KHRhaWwpKSB7XG4gICAgICAgIHJldHVybiB0YWlsLm1hdGNoKHNlbGYucmUubWFpbHRvKVswXS5sZW5ndGg7XG4gICAgICB9XG4gICAgICByZXR1cm4gMDtcbiAgICB9XG4gIH1cbn07XG5cbi8qZXNsaW50LWRpc2FibGUgbWF4LWxlbiovXG5cbi8vIFJFIHBhdHRlcm4gZm9yIDItY2hhcmFjdGVyIHRsZHMgKGF1dG9nZW5lcmF0ZWQgYnkgLi9zdXBwb3J0L3RsZHNfMmNoYXJfZ2VuLmpzKVxudmFyIHRsZHNfMmNoX3NyY19yZSA9ICdhW2NkZWZnaWxtbm9xcnN0dXd4el18YlthYmRlZmdoaWptbm9yc3R2d3l6XXxjW2FjZGZnaGlrbG1ub3J1dnd4eXpdfGRbZWprbW96XXxlW2NlZ3JzdHVdfGZbaWprbW9yXXxnW2FiZGVmZ2hpbG1ucHFyc3R1d3ldfGhba21ucnR1XXxpW2RlbG1ub3Fyc3RdfGpbZW1vcF18a1tlZ2hpbW5wcnd5el18bFthYmNpa3JzdHV2eV18bVthY2RlZ2hrbG1ub3BxcnN0dXZ3eHl6XXxuW2FjZWZnaWxvcHJ1el18b218cFthZWZnaGtsbW5yc3R3eV18cWF8cltlb3N1d118c1thYmNkZWdoaWprbG1ub3J0dXZ4eXpdfHRbY2RmZ2hqa2xtbm9ydHZ3el18dVthZ2tzeXpdfHZbYWNlZ2ludV18d1tmc118eVtldF18elthbXddJztcblxuLy8gRE9OJ1QgdHJ5IHRvIG1ha2UgUFJzIHdpdGggY2hhbmdlcy4gRXh0ZW5kIFRMRHMgd2l0aCBMaW5raWZ5SXQudGxkcygpIGluc3RlYWRcbnZhciB0bGRzX2RlZmF1bHQgPSAnYml6fGNvbXxlZHV8Z292fG5ldHxvcmd8cHJvfHdlYnx4eHh8YWVyb3xhc2lhfGNvb3B8aW5mb3xtdXNldW18bmFtZXxzaG9wfNGA0YQnLnNwbGl0KCd8Jyk7XG5cbi8qZXNsaW50LWVuYWJsZSBtYXgtbGVuKi9cblxuLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy9cblxuZnVuY3Rpb24gcmVzZXRTY2FuQ2FjaGUoc2VsZikge1xuICBzZWxmLl9faW5kZXhfXyA9IC0xO1xuICBzZWxmLl9fdGV4dF9jYWNoZV9fICAgPSAnJztcbn1cblxuZnVuY3Rpb24gY3JlYXRlVmFsaWRhdG9yKHJlKSB7XG4gIHJldHVybiBmdW5jdGlvbiAodGV4dCwgcG9zKSB7XG4gICAgdmFyIHRhaWwgPSB0ZXh0LnNsaWNlKHBvcyk7XG5cbiAgICBpZiAocmUudGVzdCh0YWlsKSkge1xuICAgICAgcmV0dXJuIHRhaWwubWF0Y2gocmUpWzBdLmxlbmd0aDtcbiAgICB9XG4gICAgcmV0dXJuIDA7XG4gIH07XG59XG5cbmZ1bmN0aW9uIGNyZWF0ZU5vcm1hbGl6ZXIoKSB7XG4gIHJldHVybiBmdW5jdGlvbiAobWF0Y2gsIHNlbGYpIHtcbiAgICBzZWxmLm5vcm1hbGl6ZShtYXRjaCk7XG4gIH07XG59XG5cbi8vIFNjaGVtYXMgY29tcGlsZXIuIEJ1aWxkIHJlZ2V4cHMuXG4vL1xuZnVuY3Rpb24gY29tcGlsZShzZWxmKSB7XG5cbiAgLy8gTG9hZCAmIGNsb25lIFJFIHBhdHRlcm5zLlxuICB2YXIgcmUgPSBzZWxmLnJlID0gcmVxdWlyZSgnLi9saWIvcmUnKShzZWxmLl9fb3B0c19fKTtcblxuICAvLyBEZWZpbmUgZHluYW1pYyBwYXR0ZXJuc1xuICB2YXIgdGxkcyA9IHNlbGYuX190bGRzX18uc2xpY2UoKTtcblxuICBzZWxmLm9uQ29tcGlsZSgpO1xuXG4gIGlmICghc2VsZi5fX3RsZHNfcmVwbGFjZWRfXykge1xuICAgIHRsZHMucHVzaCh0bGRzXzJjaF9zcmNfcmUpO1xuICB9XG4gIHRsZHMucHVzaChyZS5zcmNfeG4pO1xuXG4gIHJlLnNyY190bGRzID0gdGxkcy5qb2luKCd8Jyk7XG5cbiAgZnVuY3Rpb24gdW50cGwodHBsKSB7IHJldHVybiB0cGwucmVwbGFjZSgnJVRMRFMlJywgcmUuc3JjX3RsZHMpOyB9XG5cbiAgcmUuZW1haWxfZnV6enkgICAgICA9IFJlZ0V4cCh1bnRwbChyZS50cGxfZW1haWxfZnV6enkpLCAnaScpO1xuICByZS5saW5rX2Z1enp5ICAgICAgID0gUmVnRXhwKHVudHBsKHJlLnRwbF9saW5rX2Z1enp5KSwgJ2knKTtcbiAgcmUubGlua19ub19pcF9mdXp6eSA9IFJlZ0V4cCh1bnRwbChyZS50cGxfbGlua19ub19pcF9mdXp6eSksICdpJyk7XG4gIHJlLmhvc3RfZnV6enlfdGVzdCAgPSBSZWdFeHAodW50cGwocmUudHBsX2hvc3RfZnV6enlfdGVzdCksICdpJyk7XG5cbiAgLy9cbiAgLy8gQ29tcGlsZSBlYWNoIHNjaGVtYVxuICAvL1xuXG4gIHZhciBhbGlhc2VzID0gW107XG5cbiAgc2VsZi5fX2NvbXBpbGVkX18gPSB7fTsgLy8gUmVzZXQgY29tcGlsZWQgZGF0YVxuXG4gIGZ1bmN0aW9uIHNjaGVtYUVycm9yKG5hbWUsIHZhbCkge1xuICAgIHRocm93IG5ldyBFcnJvcignKExpbmtpZnlJdCkgSW52YWxpZCBzY2hlbWEgXCInICsgbmFtZSArICdcIjogJyArIHZhbCk7XG4gIH1cblxuICBPYmplY3Qua2V5cyhzZWxmLl9fc2NoZW1hc19fKS5mb3JFYWNoKGZ1bmN0aW9uIChuYW1lKSB7XG4gICAgdmFyIHZhbCA9IHNlbGYuX19zY2hlbWFzX19bbmFtZV07XG5cbiAgICAvLyBza2lwIGRpc2FibGVkIG1ldGhvZHNcbiAgICBpZiAodmFsID09PSBudWxsKSB7IHJldHVybjsgfVxuXG4gICAgdmFyIGNvbXBpbGVkID0geyB2YWxpZGF0ZTogbnVsbCwgbGluazogbnVsbCB9O1xuXG4gICAgc2VsZi5fX2NvbXBpbGVkX19bbmFtZV0gPSBjb21waWxlZDtcblxuICAgIGlmIChpc09iamVjdCh2YWwpKSB7XG4gICAgICBpZiAoaXNSZWdFeHAodmFsLnZhbGlkYXRlKSkge1xuICAgICAgICBjb21waWxlZC52YWxpZGF0ZSA9IGNyZWF0ZVZhbGlkYXRvcih2YWwudmFsaWRhdGUpO1xuICAgICAgfSBlbHNlIGlmIChpc0Z1bmN0aW9uKHZhbC52YWxpZGF0ZSkpIHtcbiAgICAgICAgY29tcGlsZWQudmFsaWRhdGUgPSB2YWwudmFsaWRhdGU7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBzY2hlbWFFcnJvcihuYW1lLCB2YWwpO1xuICAgICAgfVxuXG4gICAgICBpZiAoaXNGdW5jdGlvbih2YWwubm9ybWFsaXplKSkge1xuICAgICAgICBjb21waWxlZC5ub3JtYWxpemUgPSB2YWwubm9ybWFsaXplO1xuICAgICAgfSBlbHNlIGlmICghdmFsLm5vcm1hbGl6ZSkge1xuICAgICAgICBjb21waWxlZC5ub3JtYWxpemUgPSBjcmVhdGVOb3JtYWxpemVyKCk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBzY2hlbWFFcnJvcihuYW1lLCB2YWwpO1xuICAgICAgfVxuXG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgaWYgKGlzU3RyaW5nKHZhbCkpIHtcbiAgICAgIGFsaWFzZXMucHVzaChuYW1lKTtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICBzY2hlbWFFcnJvcihuYW1lLCB2YWwpO1xuICB9KTtcblxuICAvL1xuICAvLyBDb21waWxlIHBvc3Rwb25lZCBhbGlhc2VzXG4gIC8vXG5cbiAgYWxpYXNlcy5mb3JFYWNoKGZ1bmN0aW9uIChhbGlhcykge1xuICAgIGlmICghc2VsZi5fX2NvbXBpbGVkX19bc2VsZi5fX3NjaGVtYXNfX1thbGlhc11dKSB7XG4gICAgICAvLyBTaWxlbnRseSBmYWlsIG9uIG1pc3NlZCBzY2hlbWFzIHRvIGF2b2lkIGVycm9ucyBvbiBkaXNhYmxlLlxuICAgICAgLy8gc2NoZW1hRXJyb3IoYWxpYXMsIHNlbGYuX19zY2hlbWFzX19bYWxpYXNdKTtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICBzZWxmLl9fY29tcGlsZWRfX1thbGlhc10udmFsaWRhdGUgPVxuICAgICAgc2VsZi5fX2NvbXBpbGVkX19bc2VsZi5fX3NjaGVtYXNfX1thbGlhc11dLnZhbGlkYXRlO1xuICAgIHNlbGYuX19jb21waWxlZF9fW2FsaWFzXS5ub3JtYWxpemUgPVxuICAgICAgc2VsZi5fX2NvbXBpbGVkX19bc2VsZi5fX3NjaGVtYXNfX1thbGlhc11dLm5vcm1hbGl6ZTtcbiAgfSk7XG5cbiAgLy9cbiAgLy8gRmFrZSByZWNvcmQgZm9yIGd1ZXNzZWQgbGlua3NcbiAgLy9cbiAgc2VsZi5fX2NvbXBpbGVkX19bJyddID0geyB2YWxpZGF0ZTogbnVsbCwgbm9ybWFsaXplOiBjcmVhdGVOb3JtYWxpemVyKCkgfTtcblxuICAvL1xuICAvLyBCdWlsZCBzY2hlbWEgY29uZGl0aW9uXG4gIC8vXG4gIHZhciBzbGlzdCA9IE9iamVjdC5rZXlzKHNlbGYuX19jb21waWxlZF9fKVxuICAgICAgICAgICAgICAgICAgICAgIC5maWx0ZXIoZnVuY3Rpb24gKG5hbWUpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIEZpbHRlciBkaXNhYmxlZCAmIGZha2Ugc2NoZW1hc1xuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIG5hbWUubGVuZ3RoID4gMCAmJiBzZWxmLl9fY29tcGlsZWRfX1tuYW1lXTtcbiAgICAgICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgICAgICAgIC5tYXAoZXNjYXBlUkUpXG4gICAgICAgICAgICAgICAgICAgICAgLmpvaW4oJ3wnKTtcbiAgLy8gKD8hXykgY2F1c2UgMS41eCBzbG93ZG93blxuICBzZWxmLnJlLnNjaGVtYV90ZXN0ICAgPSBSZWdFeHAoJyhefCg/IV8pKD86Wz48XFx1ZmY1Y118JyArIHJlLnNyY19aUENjICsgJykpKCcgKyBzbGlzdCArICcpJywgJ2knKTtcbiAgc2VsZi5yZS5zY2hlbWFfc2VhcmNoID0gUmVnRXhwKCcoXnwoPyFfKSg/Ols+PFxcdWZmNWNdfCcgKyByZS5zcmNfWlBDYyArICcpKSgnICsgc2xpc3QgKyAnKScsICdpZycpO1xuXG4gIHNlbGYucmUucHJldGVzdCA9IFJlZ0V4cChcbiAgICAnKCcgKyBzZWxmLnJlLnNjaGVtYV90ZXN0LnNvdXJjZSArICcpfCgnICsgc2VsZi5yZS5ob3N0X2Z1enp5X3Rlc3Quc291cmNlICsgJyl8QCcsXG4gICAgJ2knXG4gICk7XG5cbiAgLy9cbiAgLy8gQ2xlYW51cFxuICAvL1xuXG4gIHJlc2V0U2NhbkNhY2hlKHNlbGYpO1xufVxuXG4vKipcbiAqIGNsYXNzIE1hdGNoXG4gKlxuICogTWF0Y2ggcmVzdWx0LiBTaW5nbGUgZWxlbWVudCBvZiBhcnJheSwgcmV0dXJuZWQgYnkgW1tMaW5raWZ5SXQjbWF0Y2hdXVxuICoqL1xuZnVuY3Rpb24gTWF0Y2goc2VsZiwgc2hpZnQpIHtcbiAgdmFyIHN0YXJ0ID0gc2VsZi5fX2luZGV4X18sXG4gICAgICBlbmQgICA9IHNlbGYuX19sYXN0X2luZGV4X18sXG4gICAgICB0ZXh0ICA9IHNlbGYuX190ZXh0X2NhY2hlX18uc2xpY2Uoc3RhcnQsIGVuZCk7XG5cbiAgLyoqXG4gICAqIE1hdGNoI3NjaGVtYSAtPiBTdHJpbmdcbiAgICpcbiAgICogUHJlZml4IChwcm90b2NvbCkgZm9yIG1hdGNoZWQgc3RyaW5nLlxuICAgKiovXG4gIHRoaXMuc2NoZW1hICAgID0gc2VsZi5fX3NjaGVtYV9fLnRvTG93ZXJDYXNlKCk7XG4gIC8qKlxuICAgKiBNYXRjaCNpbmRleCAtPiBOdW1iZXJcbiAgICpcbiAgICogRmlyc3QgcG9zaXRpb24gb2YgbWF0Y2hlZCBzdHJpbmcuXG4gICAqKi9cbiAgdGhpcy5pbmRleCAgICAgPSBzdGFydCArIHNoaWZ0O1xuICAvKipcbiAgICogTWF0Y2gjbGFzdEluZGV4IC0+IE51bWJlclxuICAgKlxuICAgKiBOZXh0IHBvc2l0aW9uIGFmdGVyIG1hdGNoZWQgc3RyaW5nLlxuICAgKiovXG4gIHRoaXMubGFzdEluZGV4ID0gZW5kICsgc2hpZnQ7XG4gIC8qKlxuICAgKiBNYXRjaCNyYXcgLT4gU3RyaW5nXG4gICAqXG4gICAqIE1hdGNoZWQgc3RyaW5nLlxuICAgKiovXG4gIHRoaXMucmF3ICAgICAgID0gdGV4dDtcbiAgLyoqXG4gICAqIE1hdGNoI3RleHQgLT4gU3RyaW5nXG4gICAqXG4gICAqIE5vdG1hbGl6ZWQgdGV4dCBvZiBtYXRjaGVkIHN0cmluZy5cbiAgICoqL1xuICB0aGlzLnRleHQgICAgICA9IHRleHQ7XG4gIC8qKlxuICAgKiBNYXRjaCN1cmwgLT4gU3RyaW5nXG4gICAqXG4gICAqIE5vcm1hbGl6ZWQgdXJsIG9mIG1hdGNoZWQgc3RyaW5nLlxuICAgKiovXG4gIHRoaXMudXJsICAgICAgID0gdGV4dDtcbn1cblxuZnVuY3Rpb24gY3JlYXRlTWF0Y2goc2VsZiwgc2hpZnQpIHtcbiAgdmFyIG1hdGNoID0gbmV3IE1hdGNoKHNlbGYsIHNoaWZ0KTtcblxuICBzZWxmLl9fY29tcGlsZWRfX1ttYXRjaC5zY2hlbWFdLm5vcm1hbGl6ZShtYXRjaCwgc2VsZik7XG5cbiAgcmV0dXJuIG1hdGNoO1xufVxuXG5cbi8qKlxuICogY2xhc3MgTGlua2lmeUl0XG4gKiovXG5cbi8qKlxuICogbmV3IExpbmtpZnlJdChzY2hlbWFzLCBvcHRpb25zKVxuICogLSBzY2hlbWFzIChPYmplY3QpOiBPcHRpb25hbC4gQWRkaXRpb25hbCBzY2hlbWFzIHRvIHZhbGlkYXRlIChwcmVmaXgvdmFsaWRhdG9yKVxuICogLSBvcHRpb25zIChPYmplY3QpOiB7IGZ1enp5TGlua3xmdXp6eUVtYWlsfGZ1enp5SVA6IHRydWV8ZmFsc2UgfVxuICpcbiAqIENyZWF0ZXMgbmV3IGxpbmtpZmllciBpbnN0YW5jZSB3aXRoIG9wdGlvbmFsIGFkZGl0aW9uYWwgc2NoZW1hcy5cbiAqIENhbiBiZSBjYWxsZWQgd2l0aG91dCBgbmV3YCBrZXl3b3JkIGZvciBjb252ZW5pZW5jZS5cbiAqXG4gKiBCeSBkZWZhdWx0IHVuZGVyc3RhbmRzOlxuICpcbiAqIC0gYGh0dHAocyk6Ly8uLi5gICwgYGZ0cDovLy4uLmAsIGBtYWlsdG86Li4uYCAmIGAvLy4uLmAgbGlua3NcbiAqIC0gXCJmdXp6eVwiIGxpbmtzIGFuZCBlbWFpbHMgKGV4YW1wbGUuY29tLCBmb29AYmFyLmNvbSkuXG4gKlxuICogYHNjaGVtYXNgIGlzIGFuIG9iamVjdCwgd2hlcmUgZWFjaCBrZXkvdmFsdWUgZGVzY3JpYmVzIHByb3RvY29sL3J1bGU6XG4gKlxuICogLSBfX2tleV9fIC0gbGluayBwcmVmaXggKHVzdWFsbHksIHByb3RvY29sIG5hbWUgd2l0aCBgOmAgYXQgdGhlIGVuZCwgYHNreXBlOmBcbiAqICAgZm9yIGV4YW1wbGUpLiBgbGlua2lmeS1pdGAgbWFrZXMgc2h1cmUgdGhhdCBwcmVmaXggaXMgbm90IHByZWNlZWRlZCB3aXRoXG4gKiAgIGFscGhhbnVtZXJpYyBjaGFyIGFuZCBzeW1ib2xzLiBPbmx5IHdoaXRlc3BhY2VzIGFuZCBwdW5jdHVhdGlvbiBhbGxvd2VkLlxuICogLSBfX3ZhbHVlX18gLSBydWxlIHRvIGNoZWNrIHRhaWwgYWZ0ZXIgbGluayBwcmVmaXhcbiAqICAgLSBfU3RyaW5nXyAtIGp1c3QgYWxpYXMgdG8gZXhpc3RpbmcgcnVsZVxuICogICAtIF9PYmplY3RfXG4gKiAgICAgLSBfdmFsaWRhdGVfIC0gdmFsaWRhdG9yIGZ1bmN0aW9uIChzaG91bGQgcmV0dXJuIG1hdGNoZWQgbGVuZ3RoIG9uIHN1Y2Nlc3MpLFxuICogICAgICAgb3IgYFJlZ0V4cGAuXG4gKiAgICAgLSBfbm9ybWFsaXplXyAtIG9wdGlvbmFsIGZ1bmN0aW9uIHRvIG5vcm1hbGl6ZSB0ZXh0ICYgdXJsIG9mIG1hdGNoZWQgcmVzdWx0XG4gKiAgICAgICAoZm9yIGV4YW1wbGUsIGZvciBAdHdpdHRlciBtZW50aW9ucykuXG4gKlxuICogYG9wdGlvbnNgOlxuICpcbiAqIC0gX19mdXp6eUxpbmtfXyAtIHJlY29nbmlnZSBVUkwtcyB3aXRob3V0IGBodHRwKHMpOmAgcHJlZml4LiBEZWZhdWx0IGB0cnVlYC5cbiAqIC0gX19mdXp6eUlQX18gLSBhbGxvdyBJUHMgaW4gZnV6enkgbGlua3MgYWJvdmUuIENhbiBjb25mbGljdCB3aXRoIHNvbWUgdGV4dHNcbiAqICAgbGlrZSB2ZXJzaW9uIG51bWJlcnMuIERlZmF1bHQgYGZhbHNlYC5cbiAqIC0gX19mdXp6eUVtYWlsX18gLSByZWNvZ25pemUgZW1haWxzIHdpdGhvdXQgYG1haWx0bzpgIHByZWZpeC5cbiAqXG4gKiovXG5mdW5jdGlvbiBMaW5raWZ5SXQoc2NoZW1hcywgb3B0aW9ucykge1xuICBpZiAoISh0aGlzIGluc3RhbmNlb2YgTGlua2lmeUl0KSkge1xuICAgIHJldHVybiBuZXcgTGlua2lmeUl0KHNjaGVtYXMsIG9wdGlvbnMpO1xuICB9XG5cbiAgaWYgKCFvcHRpb25zKSB7XG4gICAgaWYgKGlzT3B0aW9uc09iaihzY2hlbWFzKSkge1xuICAgICAgb3B0aW9ucyA9IHNjaGVtYXM7XG4gICAgICBzY2hlbWFzID0ge307XG4gICAgfVxuICB9XG5cbiAgdGhpcy5fX29wdHNfXyAgICAgICAgICAgPSBhc3NpZ24oe30sIGRlZmF1bHRPcHRpb25zLCBvcHRpb25zKTtcblxuICAvLyBDYWNoZSBsYXN0IHRlc3RlZCByZXN1bHQuIFVzZWQgdG8gc2tpcCByZXBlYXRpbmcgc3RlcHMgb24gbmV4dCBgbWF0Y2hgIGNhbGwuXG4gIHRoaXMuX19pbmRleF9fICAgICAgICAgID0gLTE7XG4gIHRoaXMuX19sYXN0X2luZGV4X18gICAgID0gLTE7IC8vIE5leHQgc2NhbiBwb3NpdGlvblxuICB0aGlzLl9fc2NoZW1hX18gICAgICAgICA9ICcnO1xuICB0aGlzLl9fdGV4dF9jYWNoZV9fICAgICA9ICcnO1xuXG4gIHRoaXMuX19zY2hlbWFzX18gICAgICAgID0gYXNzaWduKHt9LCBkZWZhdWx0U2NoZW1hcywgc2NoZW1hcyk7XG4gIHRoaXMuX19jb21waWxlZF9fICAgICAgID0ge307XG5cbiAgdGhpcy5fX3RsZHNfXyAgICAgICAgICAgPSB0bGRzX2RlZmF1bHQ7XG4gIHRoaXMuX190bGRzX3JlcGxhY2VkX18gID0gZmFsc2U7XG5cbiAgdGhpcy5yZSA9IHt9O1xuXG4gIGNvbXBpbGUodGhpcyk7XG59XG5cblxuLyoqIGNoYWluYWJsZVxuICogTGlua2lmeUl0I2FkZChzY2hlbWEsIGRlZmluaXRpb24pXG4gKiAtIHNjaGVtYSAoU3RyaW5nKTogcnVsZSBuYW1lIChmaXhlZCBwYXR0ZXJuIHByZWZpeClcbiAqIC0gZGVmaW5pdGlvbiAoU3RyaW5nfFJlZ0V4cHxPYmplY3QpOiBzY2hlbWEgZGVmaW5pdGlvblxuICpcbiAqIEFkZCBuZXcgcnVsZSBkZWZpbml0aW9uLiBTZWUgY29uc3RydWN0b3IgZGVzY3JpcHRpb24gZm9yIGRldGFpbHMuXG4gKiovXG5MaW5raWZ5SXQucHJvdG90eXBlLmFkZCA9IGZ1bmN0aW9uIGFkZChzY2hlbWEsIGRlZmluaXRpb24pIHtcbiAgdGhpcy5fX3NjaGVtYXNfX1tzY2hlbWFdID0gZGVmaW5pdGlvbjtcbiAgY29tcGlsZSh0aGlzKTtcbiAgcmV0dXJuIHRoaXM7XG59O1xuXG5cbi8qKiBjaGFpbmFibGVcbiAqIExpbmtpZnlJdCNzZXQob3B0aW9ucylcbiAqIC0gb3B0aW9ucyAoT2JqZWN0KTogeyBmdXp6eUxpbmt8ZnV6enlFbWFpbHxmdXp6eUlQOiB0cnVlfGZhbHNlIH1cbiAqXG4gKiBTZXQgcmVjb2duaXRpb24gb3B0aW9ucyBmb3IgbGlua3Mgd2l0aG91dCBzY2hlbWEuXG4gKiovXG5MaW5raWZ5SXQucHJvdG90eXBlLnNldCA9IGZ1bmN0aW9uIHNldChvcHRpb25zKSB7XG4gIHRoaXMuX19vcHRzX18gPSBhc3NpZ24odGhpcy5fX29wdHNfXywgb3B0aW9ucyk7XG4gIHJldHVybiB0aGlzO1xufTtcblxuXG4vKipcbiAqIExpbmtpZnlJdCN0ZXN0KHRleHQpIC0+IEJvb2xlYW5cbiAqXG4gKiBTZWFyY2hlcyBsaW5raWZpYWJsZSBwYXR0ZXJuIGFuZCByZXR1cm5zIGB0cnVlYCBvbiBzdWNjZXNzIG9yIGBmYWxzZWAgb24gZmFpbC5cbiAqKi9cbkxpbmtpZnlJdC5wcm90b3R5cGUudGVzdCA9IGZ1bmN0aW9uIHRlc3QodGV4dCkge1xuICAvLyBSZXNldCBzY2FuIGNhY2hlXG4gIHRoaXMuX190ZXh0X2NhY2hlX18gPSB0ZXh0O1xuICB0aGlzLl9faW5kZXhfXyAgICAgID0gLTE7XG5cbiAgaWYgKCF0ZXh0Lmxlbmd0aCkgeyByZXR1cm4gZmFsc2U7IH1cblxuICB2YXIgbSwgbWwsIG1lLCBsZW4sIHNoaWZ0LCBuZXh0LCByZSwgdGxkX3BvcywgYXRfcG9zO1xuXG4gIC8vIHRyeSB0byBzY2FuIGZvciBsaW5rIHdpdGggc2NoZW1hIC0gdGhhdCdzIHRoZSBtb3N0IHNpbXBsZSBydWxlXG4gIGlmICh0aGlzLnJlLnNjaGVtYV90ZXN0LnRlc3QodGV4dCkpIHtcbiAgICByZSA9IHRoaXMucmUuc2NoZW1hX3NlYXJjaDtcbiAgICByZS5sYXN0SW5kZXggPSAwO1xuICAgIHdoaWxlICgobSA9IHJlLmV4ZWModGV4dCkpICE9PSBudWxsKSB7XG4gICAgICBsZW4gPSB0aGlzLnRlc3RTY2hlbWFBdCh0ZXh0LCBtWzJdLCByZS5sYXN0SW5kZXgpO1xuICAgICAgaWYgKGxlbikge1xuICAgICAgICB0aGlzLl9fc2NoZW1hX18gICAgID0gbVsyXTtcbiAgICAgICAgdGhpcy5fX2luZGV4X18gICAgICA9IG0uaW5kZXggKyBtWzFdLmxlbmd0aDtcbiAgICAgICAgdGhpcy5fX2xhc3RfaW5kZXhfXyA9IG0uaW5kZXggKyBtWzBdLmxlbmd0aCArIGxlbjtcbiAgICAgICAgYnJlYWs7XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgaWYgKHRoaXMuX19vcHRzX18uZnV6enlMaW5rICYmIHRoaXMuX19jb21waWxlZF9fWydodHRwOiddKSB7XG4gICAgLy8gZ3Vlc3Mgc2NoZW1hbGVzcyBsaW5rc1xuICAgIHRsZF9wb3MgPSB0ZXh0LnNlYXJjaCh0aGlzLnJlLmhvc3RfZnV6enlfdGVzdCk7XG4gICAgaWYgKHRsZF9wb3MgPj0gMCkge1xuICAgICAgLy8gaWYgdGxkIGlzIGxvY2F0ZWQgYWZ0ZXIgZm91bmQgbGluayAtIG5vIG5lZWQgdG8gY2hlY2sgZnV6enkgcGF0dGVyblxuICAgICAgaWYgKHRoaXMuX19pbmRleF9fIDwgMCB8fCB0bGRfcG9zIDwgdGhpcy5fX2luZGV4X18pIHtcbiAgICAgICAgaWYgKChtbCA9IHRleHQubWF0Y2godGhpcy5fX29wdHNfXy5mdXp6eUlQID8gdGhpcy5yZS5saW5rX2Z1enp5IDogdGhpcy5yZS5saW5rX25vX2lwX2Z1enp5KSkgIT09IG51bGwpIHtcblxuICAgICAgICAgIHNoaWZ0ID0gbWwuaW5kZXggKyBtbFsxXS5sZW5ndGg7XG5cbiAgICAgICAgICBpZiAodGhpcy5fX2luZGV4X18gPCAwIHx8IHNoaWZ0IDwgdGhpcy5fX2luZGV4X18pIHtcbiAgICAgICAgICAgIHRoaXMuX19zY2hlbWFfXyAgICAgPSAnJztcbiAgICAgICAgICAgIHRoaXMuX19pbmRleF9fICAgICAgPSBzaGlmdDtcbiAgICAgICAgICAgIHRoaXMuX19sYXN0X2luZGV4X18gPSBtbC5pbmRleCArIG1sWzBdLmxlbmd0aDtcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICBpZiAodGhpcy5fX29wdHNfXy5mdXp6eUVtYWlsICYmIHRoaXMuX19jb21waWxlZF9fWydtYWlsdG86J10pIHtcbiAgICAvLyBndWVzcyBzY2hlbWFsZXNzIGVtYWlsc1xuICAgIGF0X3BvcyA9IHRleHQuaW5kZXhPZignQCcpO1xuICAgIGlmIChhdF9wb3MgPj0gMCkge1xuICAgICAgLy8gV2UgY2FuJ3Qgc2tpcCB0aGlzIGNoZWNrLCBiZWNhdXNlIHRoaXMgY2FzZXMgYXJlIHBvc3NpYmxlOlxuICAgICAgLy8gMTkyLjE2OC4xLjFAZ21haWwuY29tLCBteS5pbkBleGFtcGxlLmNvbVxuICAgICAgaWYgKChtZSA9IHRleHQubWF0Y2godGhpcy5yZS5lbWFpbF9mdXp6eSkpICE9PSBudWxsKSB7XG5cbiAgICAgICAgc2hpZnQgPSBtZS5pbmRleCArIG1lWzFdLmxlbmd0aDtcbiAgICAgICAgbmV4dCAgPSBtZS5pbmRleCArIG1lWzBdLmxlbmd0aDtcblxuICAgICAgICBpZiAodGhpcy5fX2luZGV4X18gPCAwIHx8IHNoaWZ0IDwgdGhpcy5fX2luZGV4X18gfHxcbiAgICAgICAgICAgIChzaGlmdCA9PT0gdGhpcy5fX2luZGV4X18gJiYgbmV4dCA+IHRoaXMuX19sYXN0X2luZGV4X18pKSB7XG4gICAgICAgICAgdGhpcy5fX3NjaGVtYV9fICAgICA9ICdtYWlsdG86JztcbiAgICAgICAgICB0aGlzLl9faW5kZXhfXyAgICAgID0gc2hpZnQ7XG4gICAgICAgICAgdGhpcy5fX2xhc3RfaW5kZXhfXyA9IG5leHQ7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICByZXR1cm4gdGhpcy5fX2luZGV4X18gPj0gMDtcbn07XG5cblxuLyoqXG4gKiBMaW5raWZ5SXQjcHJldGVzdCh0ZXh0KSAtPiBCb29sZWFuXG4gKlxuICogVmVyeSBxdWljayBjaGVjaywgdGhhdCBjYW4gZ2l2ZSBmYWxzZSBwb3NpdGl2ZXMuIFJldHVybnMgdHJ1ZSBpZiBsaW5rIE1BWSBCRVxuICogY2FuIGV4aXN0cy4gQ2FuIGJlIHVzZWQgZm9yIHNwZWVkIG9wdGltaXphdGlvbiwgd2hlbiB5b3UgbmVlZCB0byBjaGVjayB0aGF0XG4gKiBsaW5rIE5PVCBleGlzdHMuXG4gKiovXG5MaW5raWZ5SXQucHJvdG90eXBlLnByZXRlc3QgPSBmdW5jdGlvbiBwcmV0ZXN0KHRleHQpIHtcbiAgcmV0dXJuIHRoaXMucmUucHJldGVzdC50ZXN0KHRleHQpO1xufTtcblxuXG4vKipcbiAqIExpbmtpZnlJdCN0ZXN0U2NoZW1hQXQodGV4dCwgbmFtZSwgcG9zaXRpb24pIC0+IE51bWJlclxuICogLSB0ZXh0IChTdHJpbmcpOiB0ZXh0IHRvIHNjYW5cbiAqIC0gbmFtZSAoU3RyaW5nKTogcnVsZSAoc2NoZW1hKSBuYW1lXG4gKiAtIHBvc2l0aW9uIChOdW1iZXIpOiB0ZXh0IG9mZnNldCB0byBjaGVjayBmcm9tXG4gKlxuICogU2ltaWxhciB0byBbW0xpbmtpZnlJdCN0ZXN0XV0gYnV0IGNoZWNrcyBvbmx5IHNwZWNpZmljIHByb3RvY29sIHRhaWwgZXhhY3RseVxuICogYXQgZ2l2ZW4gcG9zaXRpb24uIFJldHVybnMgbGVuZ3RoIG9mIGZvdW5kIHBhdHRlcm4gKDAgb24gZmFpbCkuXG4gKiovXG5MaW5raWZ5SXQucHJvdG90eXBlLnRlc3RTY2hlbWFBdCA9IGZ1bmN0aW9uIHRlc3RTY2hlbWFBdCh0ZXh0LCBzY2hlbWEsIHBvcykge1xuICAvLyBJZiBub3Qgc3VwcG9ydGVkIHNjaGVtYSBjaGVjayByZXF1ZXN0ZWQgLSB0ZXJtaW5hdGVcbiAgaWYgKCF0aGlzLl9fY29tcGlsZWRfX1tzY2hlbWEudG9Mb3dlckNhc2UoKV0pIHtcbiAgICByZXR1cm4gMDtcbiAgfVxuICByZXR1cm4gdGhpcy5fX2NvbXBpbGVkX19bc2NoZW1hLnRvTG93ZXJDYXNlKCldLnZhbGlkYXRlKHRleHQsIHBvcywgdGhpcyk7XG59O1xuXG5cbi8qKlxuICogTGlua2lmeUl0I21hdGNoKHRleHQpIC0+IEFycmF5fG51bGxcbiAqXG4gKiBSZXR1cm5zIGFycmF5IG9mIGZvdW5kIGxpbmsgZGVzY3JpcHRpb25zIG9yIGBudWxsYCBvbiBmYWlsLiBXZSBzdHJvbmdseVxuICogcmVjb21tZW5kIHRvIHVzZSBbW0xpbmtpZnlJdCN0ZXN0XV0gZmlyc3QsIGZvciBiZXN0IHNwZWVkLlxuICpcbiAqICMjIyMjIFJlc3VsdCBtYXRjaCBkZXNjcmlwdGlvblxuICpcbiAqIC0gX19zY2hlbWFfXyAtIGxpbmsgc2NoZW1hLCBjYW4gYmUgZW1wdHkgZm9yIGZ1enp5IGxpbmtzLCBvciBgLy9gIGZvclxuICogICBwcm90b2NvbC1uZXV0cmFsICBsaW5rcy5cbiAqIC0gX19pbmRleF9fIC0gb2Zmc2V0IG9mIG1hdGNoZWQgdGV4dFxuICogLSBfX2xhc3RJbmRleF9fIC0gaW5kZXggb2YgbmV4dCBjaGFyIGFmdGVyIG1hdGhjaCBlbmRcbiAqIC0gX19yYXdfXyAtIG1hdGNoZWQgdGV4dFxuICogLSBfX3RleHRfXyAtIG5vcm1hbGl6ZWQgdGV4dFxuICogLSBfX3VybF9fIC0gbGluaywgZ2VuZXJhdGVkIGZyb20gbWF0Y2hlZCB0ZXh0XG4gKiovXG5MaW5raWZ5SXQucHJvdG90eXBlLm1hdGNoID0gZnVuY3Rpb24gbWF0Y2godGV4dCkge1xuICB2YXIgc2hpZnQgPSAwLCByZXN1bHQgPSBbXTtcblxuICAvLyBUcnkgdG8gdGFrZSBwcmV2aW91cyBlbGVtZW50IGZyb20gY2FjaGUsIGlmIC50ZXN0KCkgY2FsbGVkIGJlZm9yZVxuICBpZiAodGhpcy5fX2luZGV4X18gPj0gMCAmJiB0aGlzLl9fdGV4dF9jYWNoZV9fID09PSB0ZXh0KSB7XG4gICAgcmVzdWx0LnB1c2goY3JlYXRlTWF0Y2godGhpcywgc2hpZnQpKTtcbiAgICBzaGlmdCA9IHRoaXMuX19sYXN0X2luZGV4X187XG4gIH1cblxuICAvLyBDdXQgaGVhZCBpZiBjYWNoZSB3YXMgdXNlZFxuICB2YXIgdGFpbCA9IHNoaWZ0ID8gdGV4dC5zbGljZShzaGlmdCkgOiB0ZXh0O1xuXG4gIC8vIFNjYW4gc3RyaW5nIHVudGlsIGVuZCByZWFjaGVkXG4gIHdoaWxlICh0aGlzLnRlc3QodGFpbCkpIHtcbiAgICByZXN1bHQucHVzaChjcmVhdGVNYXRjaCh0aGlzLCBzaGlmdCkpO1xuXG4gICAgdGFpbCA9IHRhaWwuc2xpY2UodGhpcy5fX2xhc3RfaW5kZXhfXyk7XG4gICAgc2hpZnQgKz0gdGhpcy5fX2xhc3RfaW5kZXhfXztcbiAgfVxuXG4gIGlmIChyZXN1bHQubGVuZ3RoKSB7XG4gICAgcmV0dXJuIHJlc3VsdDtcbiAgfVxuXG4gIHJldHVybiBudWxsO1xufTtcblxuXG4vKiogY2hhaW5hYmxlXG4gKiBMaW5raWZ5SXQjdGxkcyhsaXN0IFssIGtlZXBPbGRdKSAtPiB0aGlzXG4gKiAtIGxpc3QgKEFycmF5KTogbGlzdCBvZiB0bGRzXG4gKiAtIGtlZXBPbGQgKEJvb2xlYW4pOiBtZXJnZSB3aXRoIGN1cnJlbnQgbGlzdCBpZiBgdHJ1ZWAgKGBmYWxzZWAgYnkgZGVmYXVsdClcbiAqXG4gKiBMb2FkIChvciBtZXJnZSkgbmV3IHRsZHMgbGlzdC4gVGhvc2UgYXJlIHVzZXIgZm9yIGZ1enp5IGxpbmtzICh3aXRob3V0IHByZWZpeClcbiAqIHRvIGF2b2lkIGZhbHNlIHBvc2l0aXZlcy4gQnkgZGVmYXVsdCB0aGlzIGFsZ29yeXRobSB1c2VkOlxuICpcbiAqIC0gaG9zdG5hbWUgd2l0aCBhbnkgMi1sZXR0ZXIgcm9vdCB6b25lcyBhcmUgb2suXG4gKiAtIGJpenxjb218ZWR1fGdvdnxuZXR8b3JnfHByb3x3ZWJ8eHh4fGFlcm98YXNpYXxjb29wfGluZm98bXVzZXVtfG5hbWV8c2hvcHzRgNGEXG4gKiAgIGFyZSBvay5cbiAqIC0gZW5jb2RlZCAoYHhuLS0uLi5gKSByb290IHpvbmVzIGFyZSBvay5cbiAqXG4gKiBJZiBsaXN0IGlzIHJlcGxhY2VkLCB0aGVuIGV4YWN0IG1hdGNoIGZvciAyLWNoYXJzIHJvb3Qgem9uZXMgd2lsbCBiZSBjaGVja2VkLlxuICoqL1xuTGlua2lmeUl0LnByb3RvdHlwZS50bGRzID0gZnVuY3Rpb24gdGxkcyhsaXN0LCBrZWVwT2xkKSB7XG4gIGxpc3QgPSBBcnJheS5pc0FycmF5KGxpc3QpID8gbGlzdCA6IFsgbGlzdCBdO1xuXG4gIGlmICgha2VlcE9sZCkge1xuICAgIHRoaXMuX190bGRzX18gPSBsaXN0LnNsaWNlKCk7XG4gICAgdGhpcy5fX3RsZHNfcmVwbGFjZWRfXyA9IHRydWU7XG4gICAgY29tcGlsZSh0aGlzKTtcbiAgICByZXR1cm4gdGhpcztcbiAgfVxuXG4gIHRoaXMuX190bGRzX18gPSB0aGlzLl9fdGxkc19fLmNvbmNhdChsaXN0KVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC5zb3J0KClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAuZmlsdGVyKGZ1bmN0aW9uIChlbCwgaWR4LCBhcnIpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBlbCAhPT0gYXJyW2lkeCAtIDFdO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLnJldmVyc2UoKTtcblxuICBjb21waWxlKHRoaXMpO1xuICByZXR1cm4gdGhpcztcbn07XG5cbi8qKlxuICogTGlua2lmeUl0I25vcm1hbGl6ZShtYXRjaClcbiAqXG4gKiBEZWZhdWx0IG5vcm1hbGl6ZXIgKGlmIHNjaGVtYSBkb2VzIG5vdCBkZWZpbmUgaXQncyBvd24pLlxuICoqL1xuTGlua2lmeUl0LnByb3RvdHlwZS5ub3JtYWxpemUgPSBmdW5jdGlvbiBub3JtYWxpemUobWF0Y2gpIHtcblxuICAvLyBEbyBtaW5pbWFsIHBvc3NpYmxlIGNoYW5nZXMgYnkgZGVmYXVsdC4gTmVlZCB0byBjb2xsZWN0IGZlZWRiYWNrIHByaW9yXG4gIC8vIHRvIG1vdmUgZm9yd2FyZCBodHRwczovL2dpdGh1Yi5jb20vbWFya2Rvd24taXQvbGlua2lmeS1pdC9pc3N1ZXMvMVxuXG4gIGlmICghbWF0Y2guc2NoZW1hKSB7IG1hdGNoLnVybCA9ICdodHRwOi8vJyArIG1hdGNoLnVybDsgfVxuXG4gIGlmIChtYXRjaC5zY2hlbWEgPT09ICdtYWlsdG86JyAmJiAhL15tYWlsdG86L2kudGVzdChtYXRjaC51cmwpKSB7XG4gICAgbWF0Y2gudXJsID0gJ21haWx0bzonICsgbWF0Y2gudXJsO1xuICB9XG59O1xuXG5cbi8qKlxuICogTGlua2lmeUl0I29uQ29tcGlsZSgpXG4gKlxuICogT3ZlcnJpZGUgdG8gbW9kaWZ5IGJhc2ljIFJlZ0V4cC1zLlxuICoqL1xuTGlua2lmeUl0LnByb3RvdHlwZS5vbkNvbXBpbGUgPSBmdW5jdGlvbiBvbkNvbXBpbGUoKSB7XG59O1xuXG5cbm1vZHVsZS5leHBvcnRzID0gTGlua2lmeUl0O1xuIiwiJ3VzZSBzdHJpY3QnO1xuXG5cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKG9wdHMpIHtcbiAgdmFyIHJlID0ge307XG5cbiAgLy8gVXNlIGRpcmVjdCBleHRyYWN0IGluc3RlYWQgb2YgYHJlZ2VuZXJhdGVgIHRvIHJlZHVzZSBicm93c2VyaWZpZWQgc2l6ZVxuICByZS5zcmNfQW55ID0gcmVxdWlyZSgndWMubWljcm8vcHJvcGVydGllcy9BbnkvcmVnZXgnKS5zb3VyY2U7XG4gIHJlLnNyY19DYyAgPSByZXF1aXJlKCd1Yy5taWNyby9jYXRlZ29yaWVzL0NjL3JlZ2V4Jykuc291cmNlO1xuICByZS5zcmNfWiAgID0gcmVxdWlyZSgndWMubWljcm8vY2F0ZWdvcmllcy9aL3JlZ2V4Jykuc291cmNlO1xuICByZS5zcmNfUCAgID0gcmVxdWlyZSgndWMubWljcm8vY2F0ZWdvcmllcy9QL3JlZ2V4Jykuc291cmNlO1xuXG4gIC8vIFxccHtcXFpcXFBcXENjXFxDRn0gKHdoaXRlIHNwYWNlcyArIGNvbnRyb2wgKyBmb3JtYXQgKyBwdW5jdHVhdGlvbilcbiAgcmUuc3JjX1pQQ2MgPSBbIHJlLnNyY19aLCByZS5zcmNfUCwgcmUuc3JjX0NjIF0uam9pbignfCcpO1xuXG4gIC8vIFxccHtcXFpcXENjfSAod2hpdGUgc3BhY2VzICsgY29udHJvbClcbiAgcmUuc3JjX1pDYyA9IFsgcmUuc3JjX1osIHJlLnNyY19DYyBdLmpvaW4oJ3wnKTtcblxuICAvLyBFeHBlcmltZW50YWwuIExpc3Qgb2YgY2hhcnMsIGNvbXBsZXRlbHkgcHJvaGliaXRlZCBpbiBsaW5rc1xuICAvLyBiZWNhdXNlIGNhbiBzZXBhcmF0ZSBpdCBmcm9tIG90aGVyIHBhcnQgb2YgdGV4dFxuICB2YXIgdGV4dF9zZXBhcmF0b3JzID0gJ1s+PFxcdWZmNWNdJztcblxuICAvLyBBbGwgcG9zc2libGUgd29yZCBjaGFyYWN0ZXJzIChldmVyeXRoaW5nIHdpdGhvdXQgcHVuY3R1YXRpb24sIHNwYWNlcyAmIGNvbnRyb2xzKVxuICAvLyBEZWZpbmVkIHZpYSBwdW5jdHVhdGlvbiAmIHNwYWNlcyB0byBzYXZlIHNwYWNlXG4gIC8vIFNob3VsZCBiZSBzb21ldGhpbmcgbGlrZSBcXHB7XFxMXFxOXFxTXFxNfSAoXFx3IGJ1dCB3aXRob3V0IGBfYClcbiAgcmUuc3JjX3BzZXVkb19sZXR0ZXIgICAgICAgPSAnKD86KD8hJyArIHRleHRfc2VwYXJhdG9ycyArICd8JyArIHJlLnNyY19aUENjICsgJyknICsgcmUuc3JjX0FueSArICcpJztcbiAgLy8gVGhlIHNhbWUgYXMgYWJvdGhlIGJ1dCB3aXRob3V0IFswLTldXG4gIC8vIHZhciBzcmNfcHNldWRvX2xldHRlcl9ub25fZCA9ICcoPzooPyFbMC05XXwnICsgc3JjX1pQQ2MgKyAnKScgKyBzcmNfQW55ICsgJyknO1xuXG4gIC8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vXG5cbiAgcmUuc3JjX2lwNCA9XG5cbiAgICAnKD86KDI1WzAtNV18MlswLTRdWzAtOV18WzAxXT9bMC05XVswLTldPylcXFxcLil7M30oMjVbMC01XXwyWzAtNF1bMC05XXxbMDFdP1swLTldWzAtOV0/KSc7XG5cbiAgLy8gUHJvaGliaXQgYW55IG9mIFwiQC9bXSgpXCIgaW4gdXNlci9wYXNzIHRvIGF2b2lkIHdyb25nIGRvbWFpbiBmZXRjaC5cbiAgcmUuc3JjX2F1dGggICAgPSAnKD86KD86KD8hJyArIHJlLnNyY19aQ2MgKyAnfFtAL1xcXFxbXFxcXF0oKV0pLikrQCk/JztcblxuICByZS5zcmNfcG9ydCA9XG5cbiAgICAnKD86Oig/OjYoPzpbMC00XVxcXFxkezN9fDUoPzpbMC00XVxcXFxkezJ9fDUoPzpbMC0yXVxcXFxkfDNbMC01XSkpKXxbMS01XT9cXFxcZHsxLDR9KSk/JztcblxuICByZS5zcmNfaG9zdF90ZXJtaW5hdG9yID1cblxuICAgICcoPz0kfCcgKyB0ZXh0X3NlcGFyYXRvcnMgKyAnfCcgKyByZS5zcmNfWlBDYyArICcpKD8hLXxffDpcXFxcZHxcXFxcLi18XFxcXC4oPyEkfCcgKyByZS5zcmNfWlBDYyArICcpKSc7XG5cbiAgcmUuc3JjX3BhdGggPVxuXG4gICAgJyg/OicgK1xuICAgICAgJ1svPyNdJyArXG4gICAgICAgICcoPzonICtcbiAgICAgICAgICAnKD8hJyArIHJlLnNyY19aQ2MgKyAnfCcgKyB0ZXh0X3NlcGFyYXRvcnMgKyAnfFsoKVtcXFxcXXt9LixcIlxcJz8hXFxcXC1dKS58JyArXG4gICAgICAgICAgJ1xcXFxbKD86KD8hJyArIHJlLnNyY19aQ2MgKyAnfFxcXFxdKS4pKlxcXFxdfCcgK1xuICAgICAgICAgICdcXFxcKCg/Oig/IScgKyByZS5zcmNfWkNjICsgJ3xbKV0pLikqXFxcXCl8JyArXG4gICAgICAgICAgJ1xcXFx7KD86KD8hJyArIHJlLnNyY19aQ2MgKyAnfFt9XSkuKSpcXFxcfXwnICtcbiAgICAgICAgICAnXFxcXFwiKD86KD8hJyArIHJlLnNyY19aQ2MgKyAnfFtcIl0pLikrXFxcXFwifCcgK1xuICAgICAgICAgIFwiXFxcXCcoPzooPyFcIiArIHJlLnNyY19aQ2MgKyBcInxbJ10pLikrXFxcXCd8XCIgK1xuICAgICAgICAgIFwiXFxcXCcoPz1cIiArIHJlLnNyY19wc2V1ZG9fbGV0dGVyICsgJ3xbLV0pLnwnICsgIC8vIGFsbG93IGBJJ21fa2luZ2AgaWYgbm8gcGFpciBmb3VuZFxuICAgICAgICAgICdcXFxcLnsyLDR9W2EtekEtWjAtOSUvXXwnICsgLy8gZ2l0aHViIGhhcyAuLi4gaW4gY29tbWl0IHJhbmdlIGxpbmtzLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vIGdvb2dsZSBoYXMgLi4uLiBpbiBsaW5rcyAoaXNzdWUgIzY2KVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vIFJlc3RyaWN0IHRvXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gLSBlbmdsaXNoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gLSBwZXJjZW50LWVuY29kZWRcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyAtIHBhcnRzIG9mIGZpbGUgcGF0aFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vIHVudGlsIG1vcmUgZXhhbXBsZXMgZm91bmQuXG4gICAgICAgICAgJ1xcXFwuKD8hJyArIHJlLnNyY19aQ2MgKyAnfFsuXSkufCcgK1xuICAgICAgICAgIChvcHRzICYmIG9wdHNbJy0tLSddID9cbiAgICAgICAgICAgICdcXFxcLSg/IS0tKD86W14tXXwkKSkoPzotKil8JyAvLyBgLS0tYCA9PiBsb25nIGRhc2gsIHRlcm1pbmF0ZVxuICAgICAgICAgICAgOlxuICAgICAgICAgICAgJ1xcXFwtK3wnXG4gICAgICAgICAgKSArXG4gICAgICAgICAgJ1xcXFwsKD8hJyArIHJlLnNyY19aQ2MgKyAnKS58JyArICAgICAgLy8gYWxsb3cgYCwsLGAgaW4gcGF0aHNcbiAgICAgICAgICAnXFxcXCEoPyEnICsgcmUuc3JjX1pDYyArICd8WyFdKS58JyArXG4gICAgICAgICAgJ1xcXFw/KD8hJyArIHJlLnNyY19aQ2MgKyAnfFs/XSkuJyArXG4gICAgICAgICcpKycgK1xuICAgICAgJ3xcXFxcLycgK1xuICAgICcpPyc7XG5cbiAgLy8gQWxsb3cgYW55dGhpbmcgaW4gbWFya2Rvd24gc3BlYywgZm9yYmlkIHF1b3RlIChcIikgYXQgdGhlIGZpcnN0IHBvc2l0aW9uXG4gIC8vIGJlY2F1c2UgZW1haWxzIGVuY2xvc2VkIGluIHF1b3RlcyBhcmUgZmFyIG1vcmUgY29tbW9uXG4gIHJlLnNyY19lbWFpbF9uYW1lID1cblxuICAgICdbXFxcXC07OiY9XFxcXCtcXFxcJCxcXFxcLmEtekEtWjAtOV9dW1xcXFwtOzomPVxcXFwrXFxcXCQsXFxcXFwiXFxcXC5hLXpBLVowLTlfXSonO1xuXG4gIHJlLnNyY194biA9XG5cbiAgICAneG4tLVthLXowLTlcXFxcLV17MSw1OX0nO1xuXG4gIC8vIE1vcmUgdG8gcmVhZCBhYm91dCBkb21haW4gbmFtZXNcbiAgLy8gaHR0cDovL3NlcnZlcmZhdWx0LmNvbS9xdWVzdGlvbnMvNjM4MjYwL1xuXG4gIHJlLnNyY19kb21haW5fcm9vdCA9XG5cbiAgICAvLyBBbGxvdyBsZXR0ZXJzICYgZGlnaXRzIChodHRwOi8vdGVzdDEpXG4gICAgJyg/OicgK1xuICAgICAgcmUuc3JjX3huICtcbiAgICAgICd8JyArXG4gICAgICByZS5zcmNfcHNldWRvX2xldHRlciArICd7MSw2M30nICtcbiAgICAnKSc7XG5cbiAgcmUuc3JjX2RvbWFpbiA9XG5cbiAgICAnKD86JyArXG4gICAgICByZS5zcmNfeG4gK1xuICAgICAgJ3wnICtcbiAgICAgICcoPzonICsgcmUuc3JjX3BzZXVkb19sZXR0ZXIgKyAnKScgK1xuICAgICAgJ3wnICtcbiAgICAgICcoPzonICsgcmUuc3JjX3BzZXVkb19sZXR0ZXIgKyAnKD86LXwnICsgcmUuc3JjX3BzZXVkb19sZXR0ZXIgKyAnKXswLDYxfScgKyByZS5zcmNfcHNldWRvX2xldHRlciArICcpJyArXG4gICAgJyknO1xuXG4gIHJlLnNyY19ob3N0ID1cblxuICAgICcoPzonICtcbiAgICAvLyBEb24ndCBuZWVkIElQIGNoZWNrLCBiZWNhdXNlIGRpZ2l0cyBhcmUgYWxyZWFkeSBhbGxvd2VkIGluIG5vcm1hbCBkb21haW4gbmFtZXNcbiAgICAvLyAgIHNyY19pcDQgK1xuICAgIC8vICd8JyArXG4gICAgICAnKD86KD86KD86JyArIHJlLnNyY19kb21haW4gKyAnKVxcXFwuKSonICsgcmUuc3JjX2RvbWFpbi8qX3Jvb3QqLyArICcpJyArXG4gICAgJyknO1xuXG4gIHJlLnRwbF9ob3N0X2Z1enp5ID1cblxuICAgICcoPzonICtcbiAgICAgIHJlLnNyY19pcDQgK1xuICAgICd8JyArXG4gICAgICAnKD86KD86KD86JyArIHJlLnNyY19kb21haW4gKyAnKVxcXFwuKSsoPzolVExEUyUpKScgK1xuICAgICcpJztcblxuICByZS50cGxfaG9zdF9ub19pcF9mdXp6eSA9XG5cbiAgICAnKD86KD86KD86JyArIHJlLnNyY19kb21haW4gKyAnKVxcXFwuKSsoPzolVExEUyUpKSc7XG5cbiAgcmUuc3JjX2hvc3Rfc3RyaWN0ID1cblxuICAgIHJlLnNyY19ob3N0ICsgcmUuc3JjX2hvc3RfdGVybWluYXRvcjtcblxuICByZS50cGxfaG9zdF9mdXp6eV9zdHJpY3QgPVxuXG4gICAgcmUudHBsX2hvc3RfZnV6enkgKyByZS5zcmNfaG9zdF90ZXJtaW5hdG9yO1xuXG4gIHJlLnNyY19ob3N0X3BvcnRfc3RyaWN0ID1cblxuICAgIHJlLnNyY19ob3N0ICsgcmUuc3JjX3BvcnQgKyByZS5zcmNfaG9zdF90ZXJtaW5hdG9yO1xuXG4gIHJlLnRwbF9ob3N0X3BvcnRfZnV6enlfc3RyaWN0ID1cblxuICAgIHJlLnRwbF9ob3N0X2Z1enp5ICsgcmUuc3JjX3BvcnQgKyByZS5zcmNfaG9zdF90ZXJtaW5hdG9yO1xuXG4gIHJlLnRwbF9ob3N0X3BvcnRfbm9faXBfZnV6enlfc3RyaWN0ID1cblxuICAgIHJlLnRwbF9ob3N0X25vX2lwX2Z1enp5ICsgcmUuc3JjX3BvcnQgKyByZS5zcmNfaG9zdF90ZXJtaW5hdG9yO1xuXG5cbiAgLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy9cbiAgLy8gTWFpbiBydWxlc1xuXG4gIC8vIFJ1ZGUgdGVzdCBmdXp6eSBsaW5rcyBieSBob3N0LCBmb3IgcXVpY2sgZGVueVxuICByZS50cGxfaG9zdF9mdXp6eV90ZXN0ID1cblxuICAgICdsb2NhbGhvc3R8d3d3XFxcXC58XFxcXC5cXFxcZHsxLDN9XFxcXC58KD86XFxcXC4oPzolVExEUyUpKD86JyArIHJlLnNyY19aUENjICsgJ3w+fCQpKSc7XG5cbiAgcmUudHBsX2VtYWlsX2Z1enp5ID1cblxuICAgICAgJyhefCcgKyB0ZXh0X3NlcGFyYXRvcnMgKyAnfFwifFxcXFwofCcgKyByZS5zcmNfWkNjICsgJyknICtcbiAgICAgICcoJyArIHJlLnNyY19lbWFpbF9uYW1lICsgJ0AnICsgcmUudHBsX2hvc3RfZnV6enlfc3RyaWN0ICsgJyknO1xuXG4gIHJlLnRwbF9saW5rX2Z1enp5ID1cbiAgICAgIC8vIEZ1enp5IGxpbmsgY2FuJ3QgYmUgcHJlcGVuZGVkIHdpdGggLjovXFwtIGFuZCBub24gcHVuY3R1YXRpb24uXG4gICAgICAvLyBidXQgY2FuIHN0YXJ0IHdpdGggPiAobWFya2Rvd24gYmxvY2txdW90ZSlcbiAgICAgICcoXnwoPyFbLjovXFxcXC1fQF0pKD86WyQrPD0+XmB8XFx1ZmY1Y118JyArIHJlLnNyY19aUENjICsgJykpJyArXG4gICAgICAnKCg/IVskKzw9Pl5gfFxcdWZmNWNdKScgKyByZS50cGxfaG9zdF9wb3J0X2Z1enp5X3N0cmljdCArIHJlLnNyY19wYXRoICsgJyknO1xuXG4gIHJlLnRwbF9saW5rX25vX2lwX2Z1enp5ID1cbiAgICAgIC8vIEZ1enp5IGxpbmsgY2FuJ3QgYmUgcHJlcGVuZGVkIHdpdGggLjovXFwtIGFuZCBub24gcHVuY3R1YXRpb24uXG4gICAgICAvLyBidXQgY2FuIHN0YXJ0IHdpdGggPiAobWFya2Rvd24gYmxvY2txdW90ZSlcbiAgICAgICcoXnwoPyFbLjovXFxcXC1fQF0pKD86WyQrPD0+XmB8XFx1ZmY1Y118JyArIHJlLnNyY19aUENjICsgJykpJyArXG4gICAgICAnKCg/IVskKzw9Pl5gfFxcdWZmNWNdKScgKyByZS50cGxfaG9zdF9wb3J0X25vX2lwX2Z1enp5X3N0cmljdCArIHJlLnNyY19wYXRoICsgJyknO1xuXG4gIHJldHVybiByZTtcbn07XG4iLCIvLyBQcm9jZXNzIGJsb2NrLWxldmVsIGN1c3RvbSBjb250YWluZXJzXG4vL1xuJ3VzZSBzdHJpY3QnO1xuXG5cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gY29udGFpbmVyX3BsdWdpbihtZCwgbmFtZSwgb3B0aW9ucykge1xuXG4gIGZ1bmN0aW9uIHZhbGlkYXRlRGVmYXVsdChwYXJhbXMpIHtcbiAgICByZXR1cm4gcGFyYW1zLnRyaW0oKS5zcGxpdCgnICcsIDIpWzBdID09PSBuYW1lO1xuICB9XG5cbiAgZnVuY3Rpb24gcmVuZGVyRGVmYXVsdCh0b2tlbnMsIGlkeCwgX29wdGlvbnMsIGVudiwgc2VsZikge1xuXG4gICAgLy8gYWRkIGEgY2xhc3MgdG8gdGhlIG9wZW5pbmcgdGFnXG4gICAgaWYgKHRva2Vuc1tpZHhdLm5lc3RpbmcgPT09IDEpIHtcbiAgICAgIHRva2Vuc1tpZHhdLmF0dHJQdXNoKFsgJ2NsYXNzJywgbmFtZSBdKTtcbiAgICB9XG5cbiAgICByZXR1cm4gc2VsZi5yZW5kZXJUb2tlbih0b2tlbnMsIGlkeCwgX29wdGlvbnMsIGVudiwgc2VsZik7XG4gIH1cblxuICBvcHRpb25zID0gb3B0aW9ucyB8fCB7fTtcblxuICB2YXIgbWluX21hcmtlcnMgPSAzLFxuICAgICAgbWFya2VyX3N0ciAgPSBvcHRpb25zLm1hcmtlciB8fCAnOicsXG4gICAgICBtYXJrZXJfY2hhciA9IG1hcmtlcl9zdHIuY2hhckNvZGVBdCgwKSxcbiAgICAgIG1hcmtlcl9sZW4gID0gbWFya2VyX3N0ci5sZW5ndGgsXG4gICAgICB2YWxpZGF0ZSAgICA9IG9wdGlvbnMudmFsaWRhdGUgfHwgdmFsaWRhdGVEZWZhdWx0LFxuICAgICAgcmVuZGVyICAgICAgPSBvcHRpb25zLnJlbmRlciB8fCByZW5kZXJEZWZhdWx0O1xuXG4gIGZ1bmN0aW9uIGNvbnRhaW5lcihzdGF0ZSwgc3RhcnRMaW5lLCBlbmRMaW5lLCBzaWxlbnQpIHtcbiAgICB2YXIgcG9zLCBuZXh0TGluZSwgbWFya2VyX2NvdW50LCBtYXJrdXAsIHBhcmFtcywgdG9rZW4sXG4gICAgICAgIG9sZF9wYXJlbnQsIG9sZF9saW5lX21heCxcbiAgICAgICAgYXV0b19jbG9zZWQgPSBmYWxzZSxcbiAgICAgICAgc3RhcnQgPSBzdGF0ZS5iTWFya3Nbc3RhcnRMaW5lXSArIHN0YXRlLnRTaGlmdFtzdGFydExpbmVdLFxuICAgICAgICBtYXggPSBzdGF0ZS5lTWFya3Nbc3RhcnRMaW5lXTtcblxuICAgIC8vIENoZWNrIG91dCB0aGUgZmlyc3QgY2hhcmFjdGVyIHF1aWNrbHksXG4gICAgLy8gdGhpcyBzaG91bGQgZmlsdGVyIG91dCBtb3N0IG9mIG5vbi1jb250YWluZXJzXG4gICAgLy9cbiAgICBpZiAobWFya2VyX2NoYXIgIT09IHN0YXRlLnNyYy5jaGFyQ29kZUF0KHN0YXJ0KSkgeyByZXR1cm4gZmFsc2U7IH1cblxuICAgIC8vIENoZWNrIG91dCB0aGUgcmVzdCBvZiB0aGUgbWFya2VyIHN0cmluZ1xuICAgIC8vXG4gICAgZm9yIChwb3MgPSBzdGFydCArIDE7IHBvcyA8PSBtYXg7IHBvcysrKSB7XG4gICAgICBpZiAobWFya2VyX3N0clsocG9zIC0gc3RhcnQpICUgbWFya2VyX2xlbl0gIT09IHN0YXRlLnNyY1twb3NdKSB7XG4gICAgICAgIGJyZWFrO1xuICAgICAgfVxuICAgIH1cblxuICAgIG1hcmtlcl9jb3VudCA9IE1hdGguZmxvb3IoKHBvcyAtIHN0YXJ0KSAvIG1hcmtlcl9sZW4pO1xuICAgIGlmIChtYXJrZXJfY291bnQgPCBtaW5fbWFya2VycykgeyByZXR1cm4gZmFsc2U7IH1cbiAgICBwb3MgLT0gKHBvcyAtIHN0YXJ0KSAlIG1hcmtlcl9sZW47XG5cbiAgICBtYXJrdXAgPSBzdGF0ZS5zcmMuc2xpY2Uoc3RhcnQsIHBvcyk7XG4gICAgcGFyYW1zID0gc3RhdGUuc3JjLnNsaWNlKHBvcywgbWF4KTtcbiAgICBpZiAoIXZhbGlkYXRlKHBhcmFtcykpIHsgcmV0dXJuIGZhbHNlOyB9XG5cbiAgICAvLyBTaW5jZSBzdGFydCBpcyBmb3VuZCwgd2UgY2FuIHJlcG9ydCBzdWNjZXNzIGhlcmUgaW4gdmFsaWRhdGlvbiBtb2RlXG4gICAgLy9cbiAgICBpZiAoc2lsZW50KSB7IHJldHVybiB0cnVlOyB9XG5cbiAgICAvLyBTZWFyY2ggZm9yIHRoZSBlbmQgb2YgdGhlIGJsb2NrXG4gICAgLy9cbiAgICBuZXh0TGluZSA9IHN0YXJ0TGluZTtcblxuICAgIGZvciAoOzspIHtcbiAgICAgIG5leHRMaW5lKys7XG4gICAgICBpZiAobmV4dExpbmUgPj0gZW5kTGluZSkge1xuICAgICAgICAvLyB1bmNsb3NlZCBibG9jayBzaG91bGQgYmUgYXV0b2Nsb3NlZCBieSBlbmQgb2YgZG9jdW1lbnQuXG4gICAgICAgIC8vIGFsc28gYmxvY2sgc2VlbXMgdG8gYmUgYXV0b2Nsb3NlZCBieSBlbmQgb2YgcGFyZW50XG4gICAgICAgIGJyZWFrO1xuICAgICAgfVxuXG4gICAgICBzdGFydCA9IHN0YXRlLmJNYXJrc1tuZXh0TGluZV0gKyBzdGF0ZS50U2hpZnRbbmV4dExpbmVdO1xuICAgICAgbWF4ID0gc3RhdGUuZU1hcmtzW25leHRMaW5lXTtcblxuICAgICAgaWYgKHN0YXJ0IDwgbWF4ICYmIHN0YXRlLnNDb3VudFtuZXh0TGluZV0gPCBzdGF0ZS5ibGtJbmRlbnQpIHtcbiAgICAgICAgLy8gbm9uLWVtcHR5IGxpbmUgd2l0aCBuZWdhdGl2ZSBpbmRlbnQgc2hvdWxkIHN0b3AgdGhlIGxpc3Q6XG4gICAgICAgIC8vIC0gYGBgXG4gICAgICAgIC8vICB0ZXN0XG4gICAgICAgIGJyZWFrO1xuICAgICAgfVxuXG4gICAgICBpZiAobWFya2VyX2NoYXIgIT09IHN0YXRlLnNyYy5jaGFyQ29kZUF0KHN0YXJ0KSkgeyBjb250aW51ZTsgfVxuXG4gICAgICBpZiAoc3RhdGUuc0NvdW50W25leHRMaW5lXSAtIHN0YXRlLmJsa0luZGVudCA+PSA0KSB7XG4gICAgICAgIC8vIGNsb3NpbmcgZmVuY2Ugc2hvdWxkIGJlIGluZGVudGVkIGxlc3MgdGhhbiA0IHNwYWNlc1xuICAgICAgICBjb250aW51ZTtcbiAgICAgIH1cblxuICAgICAgZm9yIChwb3MgPSBzdGFydCArIDE7IHBvcyA8PSBtYXg7IHBvcysrKSB7XG4gICAgICAgIGlmIChtYXJrZXJfc3RyWyhwb3MgLSBzdGFydCkgJSBtYXJrZXJfbGVuXSAhPT0gc3RhdGUuc3JjW3Bvc10pIHtcbiAgICAgICAgICBicmVhaztcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICAvLyBjbG9zaW5nIGNvZGUgZmVuY2UgbXVzdCBiZSBhdCBsZWFzdCBhcyBsb25nIGFzIHRoZSBvcGVuaW5nIG9uZVxuICAgICAgaWYgKE1hdGguZmxvb3IoKHBvcyAtIHN0YXJ0KSAvIG1hcmtlcl9sZW4pIDwgbWFya2VyX2NvdW50KSB7IGNvbnRpbnVlOyB9XG5cbiAgICAgIC8vIG1ha2Ugc3VyZSB0YWlsIGhhcyBzcGFjZXMgb25seVxuICAgICAgcG9zIC09IChwb3MgLSBzdGFydCkgJSBtYXJrZXJfbGVuO1xuICAgICAgcG9zID0gc3RhdGUuc2tpcFNwYWNlcyhwb3MpO1xuXG4gICAgICBpZiAocG9zIDwgbWF4KSB7IGNvbnRpbnVlOyB9XG5cbiAgICAgIC8vIGZvdW5kIVxuICAgICAgYXV0b19jbG9zZWQgPSB0cnVlO1xuICAgICAgYnJlYWs7XG4gICAgfVxuXG4gICAgb2xkX3BhcmVudCA9IHN0YXRlLnBhcmVudFR5cGU7XG4gICAgb2xkX2xpbmVfbWF4ID0gc3RhdGUubGluZU1heDtcbiAgICBzdGF0ZS5wYXJlbnRUeXBlID0gJ2NvbnRhaW5lcic7XG5cbiAgICAvLyB0aGlzIHdpbGwgcHJldmVudCBsYXp5IGNvbnRpbnVhdGlvbnMgZnJvbSBldmVyIGdvaW5nIHBhc3Qgb3VyIGVuZCBtYXJrZXJcbiAgICBzdGF0ZS5saW5lTWF4ID0gbmV4dExpbmU7XG5cbiAgICB0b2tlbiAgICAgICAgPSBzdGF0ZS5wdXNoKCdjb250YWluZXJfJyArIG5hbWUgKyAnX29wZW4nLCAnZGl2JywgMSk7XG4gICAgdG9rZW4ubWFya3VwID0gbWFya3VwO1xuICAgIHRva2VuLmJsb2NrICA9IHRydWU7XG4gICAgdG9rZW4uaW5mbyAgID0gcGFyYW1zO1xuICAgIHRva2VuLm1hcCAgICA9IFsgc3RhcnRMaW5lLCBuZXh0TGluZSBdO1xuXG4gICAgc3RhdGUubWQuYmxvY2sudG9rZW5pemUoc3RhdGUsIHN0YXJ0TGluZSArIDEsIG5leHRMaW5lKTtcblxuICAgIHRva2VuICAgICAgICA9IHN0YXRlLnB1c2goJ2NvbnRhaW5lcl8nICsgbmFtZSArICdfY2xvc2UnLCAnZGl2JywgLTEpO1xuICAgIHRva2VuLm1hcmt1cCA9IHN0YXRlLnNyYy5zbGljZShzdGFydCwgcG9zKTtcbiAgICB0b2tlbi5ibG9jayAgPSB0cnVlO1xuXG4gICAgc3RhdGUucGFyZW50VHlwZSA9IG9sZF9wYXJlbnQ7XG4gICAgc3RhdGUubGluZU1heCA9IG9sZF9saW5lX21heDtcbiAgICBzdGF0ZS5saW5lID0gbmV4dExpbmUgKyAoYXV0b19jbG9zZWQgPyAxIDogMCk7XG5cbiAgICByZXR1cm4gdHJ1ZTtcbiAgfVxuXG4gIG1kLmJsb2NrLnJ1bGVyLmJlZm9yZSgnZmVuY2UnLCAnY29udGFpbmVyXycgKyBuYW1lLCBjb250YWluZXIsIHtcbiAgICBhbHQ6IFsgJ3BhcmFncmFwaCcsICdyZWZlcmVuY2UnLCAnYmxvY2txdW90ZScsICdsaXN0JyBdXG4gIH0pO1xuICBtZC5yZW5kZXJlci5ydWxlc1snY29udGFpbmVyXycgKyBuYW1lICsgJ19vcGVuJ10gPSByZW5kZXI7XG4gIG1kLnJlbmRlcmVyLnJ1bGVzWydjb250YWluZXJfJyArIG5hbWUgKyAnX2Nsb3NlJ10gPSByZW5kZXI7XG59O1xuIiwiY29uc3QgZW1iZWRSRSA9IC9AXFxbKFtcXHctXSspXFxdXFwoKC4rKVxcKS9pbVxuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIHBsdWdpbiAobWQsIG9wdGlvbnMpIHtcbiAgbWQucmVuZGVyZXIucnVsZXMuY3VzdG9tID0gZnVuY3Rpb24gdG9rZW5pemVCbG9jayAodG9rZW5zLCBpZHgpIHtcbiAgICBjb25zdCB7IHRhZywgYXJnIH0gPSB0b2tlbnNbaWR4XS5pbmZvXG4gICAgaWYgKCF0YWcpIHJldHVybiAnJ1xuICAgIHJldHVybiBvcHRpb25zW3RhZ10oYXJnKSArICdcXG4nXG4gIH1cblxuICBtZC5ibG9jay5ydWxlci5iZWZvcmUoXG4gICAgJ2ZlbmNlJyxcbiAgICAnY3VzdG9tJyxcbiAgICBmdW5jdGlvbiBjdXN0b21FbWJlZCAoc3RhdGUsIHN0YXJ0TGluZSwgZW5kTGluZSwgc2lsZW50KSB7XG4gICAgICBsZXQgc3RhcnRQb3MgPSBzdGF0ZS5iTWFya3Nbc3RhcnRMaW5lXSArIHN0YXRlLnRTaGlmdFtzdGFydExpbmVdXG4gICAgICBsZXQgbWF4UG9zID0gc3RhdGUuZU1hcmtzW3N0YXJ0TGluZV1cbiAgICAgIGNvbnN0IGJsb2NrID0gc3RhdGUuc3JjLnNsaWNlKHN0YXJ0UG9zLCBtYXhQb3MpXG4gICAgICBsZXQgcG9pbnRlciA9IHsgbGluZTogc3RhcnRMaW5lLCBwb3M6IHN0YXJ0UG9zIH1cblxuICAgICAgLy8gWFhYIHd0ZlxuICAgICAgaWYgKHN0YXJ0TGluZSAhPT0gMCkge1xuICAgICAgICBsZXQgcHJldkxpbmVTdGFydFBvcyA9IHN0YXRlLmJNYXJrc1tzdGFydExpbmUgLSAxXSArXG4gICAgICAgICAgICAgIHN0YXRlLnRTaGlmdFtzdGFydExpbmUgLSAxXVxuICAgICAgICBsZXQgcHJldkxpbmVNYXhQb3MgPSBzdGF0ZS5lTWFya3Nbc3RhcnRMaW5lIC0gMV1cbiAgICAgICAgaWYgKHByZXZMaW5lTWF4UG9zID4gcHJldkxpbmVTdGFydFBvcykgcmV0dXJuIGZhbHNlXG4gICAgICB9XG5cbiAgICAgIC8vIENoZWNrIGlmIGl0J3MgQFt0YWddKGFyZylcbiAgICAgIGlmIChzdGF0ZS5zcmMuY2hhckNvZGVBdChwb2ludGVyLnBvcykgIT09IDB4NDAvKiBAICovIHx8XG4gICAgICAgICAgc3RhdGUuc3JjLmNoYXJDb2RlQXQocG9pbnRlci5wb3MgKyAxKSAhPT0gMHg1Qi8qIFsgKi8pIHtcbiAgICAgICAgcmV0dXJuIGZhbHNlXG4gICAgICB9XG5cbiAgICAgIGNvbnN0IG1hdGNoID0gZW1iZWRSRS5leGVjKGJsb2NrKVxuXG4gICAgICBpZiAoIW1hdGNoIHx8IG1hdGNoLmxlbmd0aCA8IDMpIHtcbiAgICAgICAgcmV0dXJuIGZhbHNlXG4gICAgICB9XG5cbiAgICAgIGNvbnN0IFthbGwsIHRhZywgYXJnXSA9IG1hdGNoXG5cbiAgICAgIHBvaW50ZXIucG9zICs9IGFsbC5sZW5ndGhcblxuICAgICAgLy8gQmxvY2sgZW1iZWQgbXVzdCBiZSBhdCBlbmQgb2YgaW5wdXQgb3IgdGhlIG5leHQgbGluZSBtdXN0IGJlIGJsYW5rLlxuICAgICAgLy8gVE9ETyBzb21ldGhpbmcgY2FuIGJlIGRvbmUgaGVyZSB0byBtYWtlIGl0IHdvcmsgd2l0aG91dCBibGFuayBsaW5lc1xuICAgICAgaWYgKGVuZExpbmUgIT09IHBvaW50ZXIubGluZSArIDEpIHtcbiAgICAgICAgbGV0IG5leHRMaW5lU3RhcnRQb3MgPSBzdGF0ZS5iTWFya3NbcG9pbnRlci5saW5lICsgMV0gKyBzdGF0ZS50U2hpZnRbcG9pbnRlci5saW5lICsgMV1cbiAgICAgICAgbGV0IG5leHRMaW5lTWF4UG9zID0gc3RhdGUuZU1hcmtzW3BvaW50ZXIubGluZSArIDFdXG4gICAgICAgIGlmIChuZXh0TGluZU1heFBvcyA+IG5leHRMaW5lU3RhcnRQb3MpIHJldHVybiBmYWxzZVxuICAgICAgfVxuXG4gICAgICBpZiAocG9pbnRlci5saW5lID49IGVuZExpbmUpIHJldHVybiBmYWxzZVxuXG4gICAgICBpZiAoIXNpbGVudCkge1xuICAgICAgICBsZXQgdG9rZW4gPSBzdGF0ZS5wdXNoKCdjdXN0b20nLCAnZGl2JywgMClcbiAgICAgICAgdG9rZW4ubWFya3VwID0gc3RhdGUuc3JjLnNsaWNlKHN0YXJ0UG9zLCBwb2ludGVyLnBvcylcbiAgICAgICAgdG9rZW4uaW5mbyA9IHsgYXJnLCB0YWcgfVxuICAgICAgICB0b2tlbi5ibG9jayA9IHRydWVcbiAgICAgICAgdG9rZW4ubWFwID0gWyBzdGFydExpbmUsIHBvaW50ZXIubGluZSArIDEgXVxuICAgICAgICBzdGF0ZS5saW5lID0gcG9pbnRlci5saW5lICsgMVxuICAgICAgfVxuXG4gICAgICByZXR1cm4gdHJ1ZVxuICAgIH0sXG4gICAgeyBhbHQ6IFsgJ3BhcmFncmFwaCcsICdyZWZlcmVuY2UnLCAnYmxvY2txdW90ZScsICdsaXN0JyBdIH1cbiAgKVxufVxuIiwiJ3VzZSBzdHJpY3QnXG5cbi8vIEFkYXB0ZWQgZnJvbSBodHRwczovL2dpdGh1Yi5jb20vbWFya2Rvd24taXQvbWFya2Rvd24taXQvYmxvYi9mYmM2YjBmZWQ1NjNiYTdjMDA1NTdhYjYzOGZkMTk3NTJmOGU3NTlkL2RvY3MvYXJjaGl0ZWN0dXJlLm1kXG5cbmZ1bmN0aW9uIGZpbmRGaXJzdE1hdGNoaW5nQ29uZmlnIChsaW5rLCBjb25maWdzKSB7XG4gIHZhciBpLCBjb25maWdcbiAgdmFyIGhyZWYgPSBsaW5rLmF0dHJzW2xpbmsuYXR0ckluZGV4KCdocmVmJyldWzFdXG5cbiAgZm9yIChpID0gMDsgaSA8IGNvbmZpZ3MubGVuZ3RoOyArK2kpIHtcbiAgICBjb25maWcgPSBjb25maWdzW2ldXG5cbiAgICAvLyBpZiB0aGVyZSBpcyBubyBwYXR0ZXJuLCBjb25maWcgbWF0Y2hlcyBmb3IgYWxsIGxpbmtzXG4gICAgLy8gb3RoZXJ3aXNlLCBvbmx5IHJldHVybiBjb25maWcgaWYgaHJlZiBtYXRjaGVzIHRoZSBwYXR0ZXJuIHNldFxuICAgIGlmICghY29uZmlnLnBhdHRlcm4gfHwgbmV3IFJlZ0V4cChjb25maWcucGF0dGVybikudGVzdChocmVmKSkge1xuICAgICAgcmV0dXJuIGNvbmZpZ1xuICAgIH1cbiAgfVxufVxuXG5mdW5jdGlvbiBhcHBseUF0dHJpYnV0ZXMgKGlkeCwgdG9rZW5zLCBhdHRyaWJ1dGVzKSB7XG4gIE9iamVjdC5rZXlzKGF0dHJpYnV0ZXMpLmZvckVhY2goZnVuY3Rpb24gKGF0dHIpIHtcbiAgICB2YXIgYXR0ckluZGV4XG4gICAgdmFyIHZhbHVlID0gYXR0cmlidXRlc1thdHRyXVxuXG4gICAgaWYgKGF0dHIgPT09ICdjbGFzc05hbWUnKSB7XG4gICAgICAvLyB3aGVuIGRlYWxpbmcgd2l0aCBhcHBseWluZyBjbGFzc2VzXG4gICAgICAvLyBwcm9ncmFtYXRpY2FsbHksIHNvbWUgcHJvZ3JhbW1lcnNcbiAgICAgIC8vIG1heSBwcmVmZXIgdG8gdXNlIHRoZSBjbGFzc05hbWUgc3ludGF4XG4gICAgICBhdHRyID0gJ2NsYXNzJ1xuICAgIH1cblxuICAgIGF0dHJJbmRleCA9IHRva2Vuc1tpZHhdLmF0dHJJbmRleChhdHRyKVxuXG4gICAgaWYgKGF0dHJJbmRleCA8IDApIHsgLy8gYXR0ciBkb2Vzbid0IGV4aXN0LCBhZGQgbmV3IGF0dHJpYnV0ZVxuICAgICAgdG9rZW5zW2lkeF0uYXR0clB1c2goW2F0dHIsIHZhbHVlXSlcbiAgICB9IGVsc2UgeyAvLyBhdHRyIGFscmVhZHkgZXhpc3RzLCBvdmVyd3JpdGUgaXRcbiAgICAgIHRva2Vuc1tpZHhdLmF0dHJzW2F0dHJJbmRleF1bMV0gPSB2YWx1ZSAvLyByZXBsYWNlIHZhbHVlIG9mIGV4aXN0aW5nIGF0dHJcbiAgICB9XG4gIH0pXG59XG5cbmZ1bmN0aW9uIG1hcmtkb3duaXRMaW5rQXR0cmlidXRlcyAobWQsIGNvbmZpZ3MpIHtcbiAgaWYgKCFjb25maWdzKSB7XG4gICAgY29uZmlncyA9IFtdXG4gIH0gZWxzZSB7XG4gICAgY29uZmlncyA9IEFycmF5LmlzQXJyYXkoY29uZmlncykgPyBjb25maWdzIDogW2NvbmZpZ3NdXG4gIH1cblxuICBPYmplY3QuZnJlZXplKGNvbmZpZ3MpXG5cbiAgdmFyIGRlZmF1bHRSZW5kZXIgPSBtZC5yZW5kZXJlci5ydWxlcy5saW5rX29wZW4gfHwgdGhpcy5kZWZhdWx0UmVuZGVyXG5cbiAgbWQucmVuZGVyZXIucnVsZXMubGlua19vcGVuID0gZnVuY3Rpb24gKHRva2VucywgaWR4LCBvcHRpb25zLCBlbnYsIHNlbGYpIHtcbiAgICB2YXIgY29uZmlnID0gZmluZEZpcnN0TWF0Y2hpbmdDb25maWcodG9rZW5zW2lkeF0sIGNvbmZpZ3MpXG4gICAgdmFyIGF0dHJpYnV0ZXMgPSBjb25maWcgJiYgY29uZmlnLmF0dHJzXG5cbiAgICBpZiAoYXR0cmlidXRlcykge1xuICAgICAgYXBwbHlBdHRyaWJ1dGVzKGlkeCwgdG9rZW5zLCBhdHRyaWJ1dGVzKVxuICAgIH1cblxuICAgIC8vIHBhc3MgdG9rZW4gdG8gZGVmYXVsdCByZW5kZXJlci5cbiAgICByZXR1cm4gZGVmYXVsdFJlbmRlcih0b2tlbnMsIGlkeCwgb3B0aW9ucywgZW52LCBzZWxmKVxuICB9XG59XG5cbm1hcmtkb3duaXRMaW5rQXR0cmlidXRlcy5kZWZhdWx0UmVuZGVyID0gZnVuY3Rpb24gKHRva2VucywgaWR4LCBvcHRpb25zLCBlbnYsIHNlbGYpIHtcbiAgcmV0dXJuIHNlbGYucmVuZGVyVG9rZW4odG9rZW5zLCBpZHgsIG9wdGlvbnMpXG59XG5cbm1vZHVsZS5leHBvcnRzID0gbWFya2Rvd25pdExpbmtBdHRyaWJ1dGVzXG4iLCIndXNlIHN0cmljdCc7XG5cblxubW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKCcuL2xpYi8nKTtcbiIsIi8vIEhUTUw1IGVudGl0aWVzIG1hcDogeyBuYW1lIC0+IHV0ZjE2c3RyaW5nIH1cbi8vXG4ndXNlIHN0cmljdCc7XG5cbi8qZXNsaW50IHF1b3RlczowKi9cbm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZSgnZW50aXRpZXMvbGliL21hcHMvZW50aXRpZXMuanNvbicpO1xuIiwiLy8gTGlzdCBvZiB2YWxpZCBodG1sIGJsb2NrcyBuYW1lcywgYWNjb3J0aW5nIHRvIGNvbW1vbm1hcmsgc3BlY1xuLy8gaHR0cDovL2pnbS5naXRodWIuaW8vQ29tbW9uTWFyay9zcGVjLmh0bWwjaHRtbC1ibG9ja3NcblxuJ3VzZSBzdHJpY3QnO1xuXG5cbm1vZHVsZS5leHBvcnRzID0gW1xuICAnYWRkcmVzcycsXG4gICdhcnRpY2xlJyxcbiAgJ2FzaWRlJyxcbiAgJ2Jhc2UnLFxuICAnYmFzZWZvbnQnLFxuICAnYmxvY2txdW90ZScsXG4gICdib2R5JyxcbiAgJ2NhcHRpb24nLFxuICAnY2VudGVyJyxcbiAgJ2NvbCcsXG4gICdjb2xncm91cCcsXG4gICdkZCcsXG4gICdkZXRhaWxzJyxcbiAgJ2RpYWxvZycsXG4gICdkaXInLFxuICAnZGl2JyxcbiAgJ2RsJyxcbiAgJ2R0JyxcbiAgJ2ZpZWxkc2V0JyxcbiAgJ2ZpZ2NhcHRpb24nLFxuICAnZmlndXJlJyxcbiAgJ2Zvb3RlcicsXG4gICdmb3JtJyxcbiAgJ2ZyYW1lJyxcbiAgJ2ZyYW1lc2V0JyxcbiAgJ2gxJyxcbiAgJ2gyJyxcbiAgJ2gzJyxcbiAgJ2g0JyxcbiAgJ2g1JyxcbiAgJ2g2JyxcbiAgJ2hlYWQnLFxuICAnaGVhZGVyJyxcbiAgJ2hyJyxcbiAgJ2h0bWwnLFxuICAnaWZyYW1lJyxcbiAgJ2xlZ2VuZCcsXG4gICdsaScsXG4gICdsaW5rJyxcbiAgJ21haW4nLFxuICAnbWVudScsXG4gICdtZW51aXRlbScsXG4gICdtZXRhJyxcbiAgJ25hdicsXG4gICdub2ZyYW1lcycsXG4gICdvbCcsXG4gICdvcHRncm91cCcsXG4gICdvcHRpb24nLFxuICAncCcsXG4gICdwYXJhbScsXG4gICdzZWN0aW9uJyxcbiAgJ3NvdXJjZScsXG4gICdzdW1tYXJ5JyxcbiAgJ3RhYmxlJyxcbiAgJ3Rib2R5JyxcbiAgJ3RkJyxcbiAgJ3Rmb290JyxcbiAgJ3RoJyxcbiAgJ3RoZWFkJyxcbiAgJ3RpdGxlJyxcbiAgJ3RyJyxcbiAgJ3RyYWNrJyxcbiAgJ3VsJ1xuXTtcbiIsIi8vIFJlZ2V4cHMgdG8gbWF0Y2ggaHRtbCBlbGVtZW50c1xuXG4ndXNlIHN0cmljdCc7XG5cbnZhciBhdHRyX25hbWUgICAgID0gJ1thLXpBLVpfOl1bYS16QS1aMC05Oi5fLV0qJztcblxudmFyIHVucXVvdGVkICAgICAgPSAnW15cIlxcJz08PmBcXFxceDAwLVxcXFx4MjBdKyc7XG52YXIgc2luZ2xlX3F1b3RlZCA9IFwiJ1teJ10qJ1wiO1xudmFyIGRvdWJsZV9xdW90ZWQgPSAnXCJbXlwiXSpcIic7XG5cbnZhciBhdHRyX3ZhbHVlICA9ICcoPzonICsgdW5xdW90ZWQgKyAnfCcgKyBzaW5nbGVfcXVvdGVkICsgJ3wnICsgZG91YmxlX3F1b3RlZCArICcpJztcblxudmFyIGF0dHJpYnV0ZSAgID0gJyg/OlxcXFxzKycgKyBhdHRyX25hbWUgKyAnKD86XFxcXHMqPVxcXFxzKicgKyBhdHRyX3ZhbHVlICsgJyk/KSc7XG5cbnZhciBvcGVuX3RhZyAgICA9ICc8W0EtWmEtel1bQS1aYS16MC05XFxcXC1dKicgKyBhdHRyaWJ1dGUgKyAnKlxcXFxzKlxcXFwvPz4nO1xuXG52YXIgY2xvc2VfdGFnICAgPSAnPFxcXFwvW0EtWmEtel1bQS1aYS16MC05XFxcXC1dKlxcXFxzKj4nO1xudmFyIGNvbW1lbnQgICAgID0gJzwhLS0tLT58PCEtLSg/Oi0/W14+LV0pKD86LT9bXi1dKSotLT4nO1xudmFyIHByb2Nlc3NpbmcgID0gJzxbP10uKj9bP10+JztcbnZhciBkZWNsYXJhdGlvbiA9ICc8IVtBLVpdK1xcXFxzK1tePl0qPic7XG52YXIgY2RhdGEgICAgICAgPSAnPCFcXFxcW0NEQVRBXFxcXFtbXFxcXHNcXFxcU10qP1xcXFxdXFxcXF0+JztcblxudmFyIEhUTUxfVEFHX1JFID0gbmV3IFJlZ0V4cCgnXig/OicgKyBvcGVuX3RhZyArICd8JyArIGNsb3NlX3RhZyArICd8JyArIGNvbW1lbnQgK1xuICAgICAgICAgICAgICAgICAgICAgICAgJ3wnICsgcHJvY2Vzc2luZyArICd8JyArIGRlY2xhcmF0aW9uICsgJ3wnICsgY2RhdGEgKyAnKScpO1xudmFyIEhUTUxfT1BFTl9DTE9TRV9UQUdfUkUgPSBuZXcgUmVnRXhwKCdeKD86JyArIG9wZW5fdGFnICsgJ3wnICsgY2xvc2VfdGFnICsgJyknKTtcblxubW9kdWxlLmV4cG9ydHMuSFRNTF9UQUdfUkUgPSBIVE1MX1RBR19SRTtcbm1vZHVsZS5leHBvcnRzLkhUTUxfT1BFTl9DTE9TRV9UQUdfUkUgPSBIVE1MX09QRU5fQ0xPU0VfVEFHX1JFO1xuIiwiLy8gVXRpbGl0aWVzXG4vL1xuJ3VzZSBzdHJpY3QnO1xuXG5cbmZ1bmN0aW9uIF9jbGFzcyhvYmopIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUudG9TdHJpbmcuY2FsbChvYmopOyB9XG5cbmZ1bmN0aW9uIGlzU3RyaW5nKG9iaikgeyByZXR1cm4gX2NsYXNzKG9iaikgPT09ICdbb2JqZWN0IFN0cmluZ10nOyB9XG5cbnZhciBfaGFzT3duUHJvcGVydHkgPSBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5O1xuXG5mdW5jdGlvbiBoYXMob2JqZWN0LCBrZXkpIHtcbiAgcmV0dXJuIF9oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iamVjdCwga2V5KTtcbn1cblxuLy8gTWVyZ2Ugb2JqZWN0c1xuLy9cbmZ1bmN0aW9uIGFzc2lnbihvYmogLypmcm9tMSwgZnJvbTIsIGZyb20zLCAuLi4qLykge1xuICB2YXIgc291cmNlcyA9IEFycmF5LnByb3RvdHlwZS5zbGljZS5jYWxsKGFyZ3VtZW50cywgMSk7XG5cbiAgc291cmNlcy5mb3JFYWNoKGZ1bmN0aW9uIChzb3VyY2UpIHtcbiAgICBpZiAoIXNvdXJjZSkgeyByZXR1cm47IH1cblxuICAgIGlmICh0eXBlb2Ygc291cmNlICE9PSAnb2JqZWN0Jykge1xuICAgICAgdGhyb3cgbmV3IFR5cGVFcnJvcihzb3VyY2UgKyAnbXVzdCBiZSBvYmplY3QnKTtcbiAgICB9XG5cbiAgICBPYmplY3Qua2V5cyhzb3VyY2UpLmZvckVhY2goZnVuY3Rpb24gKGtleSkge1xuICAgICAgb2JqW2tleV0gPSBzb3VyY2Vba2V5XTtcbiAgICB9KTtcbiAgfSk7XG5cbiAgcmV0dXJuIG9iajtcbn1cblxuLy8gUmVtb3ZlIGVsZW1lbnQgZnJvbSBhcnJheSBhbmQgcHV0IGFub3RoZXIgYXJyYXkgYXQgdGhvc2UgcG9zaXRpb24uXG4vLyBVc2VmdWwgZm9yIHNvbWUgb3BlcmF0aW9ucyB3aXRoIHRva2Vuc1xuZnVuY3Rpb24gYXJyYXlSZXBsYWNlQXQoc3JjLCBwb3MsIG5ld0VsZW1lbnRzKSB7XG4gIHJldHVybiBbXS5jb25jYXQoc3JjLnNsaWNlKDAsIHBvcyksIG5ld0VsZW1lbnRzLCBzcmMuc2xpY2UocG9zICsgMSkpO1xufVxuXG4vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vL1xuXG5mdW5jdGlvbiBpc1ZhbGlkRW50aXR5Q29kZShjKSB7XG4gIC8qZXNsaW50IG5vLWJpdHdpc2U6MCovXG4gIC8vIGJyb2tlbiBzZXF1ZW5jZVxuICBpZiAoYyA+PSAweEQ4MDAgJiYgYyA8PSAweERGRkYpIHsgcmV0dXJuIGZhbHNlOyB9XG4gIC8vIG5ldmVyIHVzZWRcbiAgaWYgKGMgPj0gMHhGREQwICYmIGMgPD0gMHhGREVGKSB7IHJldHVybiBmYWxzZTsgfVxuICBpZiAoKGMgJiAweEZGRkYpID09PSAweEZGRkYgfHwgKGMgJiAweEZGRkYpID09PSAweEZGRkUpIHsgcmV0dXJuIGZhbHNlOyB9XG4gIC8vIGNvbnRyb2wgY29kZXNcbiAgaWYgKGMgPj0gMHgwMCAmJiBjIDw9IDB4MDgpIHsgcmV0dXJuIGZhbHNlOyB9XG4gIGlmIChjID09PSAweDBCKSB7IHJldHVybiBmYWxzZTsgfVxuICBpZiAoYyA+PSAweDBFICYmIGMgPD0gMHgxRikgeyByZXR1cm4gZmFsc2U7IH1cbiAgaWYgKGMgPj0gMHg3RiAmJiBjIDw9IDB4OUYpIHsgcmV0dXJuIGZhbHNlOyB9XG4gIC8vIG91dCBvZiByYW5nZVxuICBpZiAoYyA+IDB4MTBGRkZGKSB7IHJldHVybiBmYWxzZTsgfVxuICByZXR1cm4gdHJ1ZTtcbn1cblxuZnVuY3Rpb24gZnJvbUNvZGVQb2ludChjKSB7XG4gIC8qZXNsaW50IG5vLWJpdHdpc2U6MCovXG4gIGlmIChjID4gMHhmZmZmKSB7XG4gICAgYyAtPSAweDEwMDAwO1xuICAgIHZhciBzdXJyb2dhdGUxID0gMHhkODAwICsgKGMgPj4gMTApLFxuICAgICAgICBzdXJyb2dhdGUyID0gMHhkYzAwICsgKGMgJiAweDNmZik7XG5cbiAgICByZXR1cm4gU3RyaW5nLmZyb21DaGFyQ29kZShzdXJyb2dhdGUxLCBzdXJyb2dhdGUyKTtcbiAgfVxuICByZXR1cm4gU3RyaW5nLmZyb21DaGFyQ29kZShjKTtcbn1cblxuXG52YXIgVU5FU0NBUEVfTURfUkUgID0gL1xcXFwoWyFcIiMkJSYnKCkqKyxcXC0uXFwvOjs8PT4/QFtcXFxcXFxdXl9ge3x9fl0pL2c7XG52YXIgRU5USVRZX1JFICAgICAgID0gLyYoW2EteiNdW2EtejAtOV17MSwzMX0pOy9naTtcbnZhciBVTkVTQ0FQRV9BTExfUkUgPSBuZXcgUmVnRXhwKFVORVNDQVBFX01EX1JFLnNvdXJjZSArICd8JyArIEVOVElUWV9SRS5zb3VyY2UsICdnaScpO1xuXG52YXIgRElHSVRBTF9FTlRJVFlfVEVTVF9SRSA9IC9eIygoPzp4W2EtZjAtOV17MSw4fXxbMC05XXsxLDh9KSkvaTtcblxudmFyIGVudGl0aWVzID0gcmVxdWlyZSgnLi9lbnRpdGllcycpO1xuXG5mdW5jdGlvbiByZXBsYWNlRW50aXR5UGF0dGVybihtYXRjaCwgbmFtZSkge1xuICB2YXIgY29kZSA9IDA7XG5cbiAgaWYgKGhhcyhlbnRpdGllcywgbmFtZSkpIHtcbiAgICByZXR1cm4gZW50aXRpZXNbbmFtZV07XG4gIH1cblxuICBpZiAobmFtZS5jaGFyQ29kZUF0KDApID09PSAweDIzLyogIyAqLyAmJiBESUdJVEFMX0VOVElUWV9URVNUX1JFLnRlc3QobmFtZSkpIHtcbiAgICBjb2RlID0gbmFtZVsxXS50b0xvd2VyQ2FzZSgpID09PSAneCcgP1xuICAgICAgcGFyc2VJbnQobmFtZS5zbGljZSgyKSwgMTYpIDogcGFyc2VJbnQobmFtZS5zbGljZSgxKSwgMTApO1xuXG4gICAgaWYgKGlzVmFsaWRFbnRpdHlDb2RlKGNvZGUpKSB7XG4gICAgICByZXR1cm4gZnJvbUNvZGVQb2ludChjb2RlKTtcbiAgICB9XG4gIH1cblxuICByZXR1cm4gbWF0Y2g7XG59XG5cbi8qZnVuY3Rpb24gcmVwbGFjZUVudGl0aWVzKHN0cikge1xuICBpZiAoc3RyLmluZGV4T2YoJyYnKSA8IDApIHsgcmV0dXJuIHN0cjsgfVxuXG4gIHJldHVybiBzdHIucmVwbGFjZShFTlRJVFlfUkUsIHJlcGxhY2VFbnRpdHlQYXR0ZXJuKTtcbn0qL1xuXG5mdW5jdGlvbiB1bmVzY2FwZU1kKHN0cikge1xuICBpZiAoc3RyLmluZGV4T2YoJ1xcXFwnKSA8IDApIHsgcmV0dXJuIHN0cjsgfVxuICByZXR1cm4gc3RyLnJlcGxhY2UoVU5FU0NBUEVfTURfUkUsICckMScpO1xufVxuXG5mdW5jdGlvbiB1bmVzY2FwZUFsbChzdHIpIHtcbiAgaWYgKHN0ci5pbmRleE9mKCdcXFxcJykgPCAwICYmIHN0ci5pbmRleE9mKCcmJykgPCAwKSB7IHJldHVybiBzdHI7IH1cblxuICByZXR1cm4gc3RyLnJlcGxhY2UoVU5FU0NBUEVfQUxMX1JFLCBmdW5jdGlvbiAobWF0Y2gsIGVzY2FwZWQsIGVudGl0eSkge1xuICAgIGlmIChlc2NhcGVkKSB7IHJldHVybiBlc2NhcGVkOyB9XG4gICAgcmV0dXJuIHJlcGxhY2VFbnRpdHlQYXR0ZXJuKG1hdGNoLCBlbnRpdHkpO1xuICB9KTtcbn1cblxuLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy9cblxudmFyIEhUTUxfRVNDQVBFX1RFU1RfUkUgPSAvWyY8PlwiXS87XG52YXIgSFRNTF9FU0NBUEVfUkVQTEFDRV9SRSA9IC9bJjw+XCJdL2c7XG52YXIgSFRNTF9SRVBMQUNFTUVOVFMgPSB7XG4gICcmJzogJyZhbXA7JyxcbiAgJzwnOiAnJmx0OycsXG4gICc+JzogJyZndDsnLFxuICAnXCInOiAnJnF1b3Q7J1xufTtcblxuZnVuY3Rpb24gcmVwbGFjZVVuc2FmZUNoYXIoY2gpIHtcbiAgcmV0dXJuIEhUTUxfUkVQTEFDRU1FTlRTW2NoXTtcbn1cblxuZnVuY3Rpb24gZXNjYXBlSHRtbChzdHIpIHtcbiAgaWYgKEhUTUxfRVNDQVBFX1RFU1RfUkUudGVzdChzdHIpKSB7XG4gICAgcmV0dXJuIHN0ci5yZXBsYWNlKEhUTUxfRVNDQVBFX1JFUExBQ0VfUkUsIHJlcGxhY2VVbnNhZmVDaGFyKTtcbiAgfVxuICByZXR1cm4gc3RyO1xufVxuXG4vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vL1xuXG52YXIgUkVHRVhQX0VTQ0FQRV9SRSA9IC9bLj8qK14kW1xcXVxcXFwoKXt9fC1dL2c7XG5cbmZ1bmN0aW9uIGVzY2FwZVJFKHN0cikge1xuICByZXR1cm4gc3RyLnJlcGxhY2UoUkVHRVhQX0VTQ0FQRV9SRSwgJ1xcXFwkJicpO1xufVxuXG4vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vL1xuXG5mdW5jdGlvbiBpc1NwYWNlKGNvZGUpIHtcbiAgc3dpdGNoIChjb2RlKSB7XG4gICAgY2FzZSAweDA5OlxuICAgIGNhc2UgMHgyMDpcbiAgICAgIHJldHVybiB0cnVlO1xuICB9XG4gIHJldHVybiBmYWxzZTtcbn1cblxuLy8gWnMgKHVuaWNvZGUgY2xhc3MpIHx8IFtcXHRcXGZcXHZcXHJcXG5dXG5mdW5jdGlvbiBpc1doaXRlU3BhY2UoY29kZSkge1xuICBpZiAoY29kZSA+PSAweDIwMDAgJiYgY29kZSA8PSAweDIwMEEpIHsgcmV0dXJuIHRydWU7IH1cbiAgc3dpdGNoIChjb2RlKSB7XG4gICAgY2FzZSAweDA5OiAvLyBcXHRcbiAgICBjYXNlIDB4MEE6IC8vIFxcblxuICAgIGNhc2UgMHgwQjogLy8gXFx2XG4gICAgY2FzZSAweDBDOiAvLyBcXGZcbiAgICBjYXNlIDB4MEQ6IC8vIFxcclxuICAgIGNhc2UgMHgyMDpcbiAgICBjYXNlIDB4QTA6XG4gICAgY2FzZSAweDE2ODA6XG4gICAgY2FzZSAweDIwMkY6XG4gICAgY2FzZSAweDIwNUY6XG4gICAgY2FzZSAweDMwMDA6XG4gICAgICByZXR1cm4gdHJ1ZTtcbiAgfVxuICByZXR1cm4gZmFsc2U7XG59XG5cbi8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vXG5cbi8qZXNsaW50LWRpc2FibGUgbWF4LWxlbiovXG52YXIgVU5JQ09ERV9QVU5DVF9SRSA9IHJlcXVpcmUoJ3VjLm1pY3JvL2NhdGVnb3JpZXMvUC9yZWdleCcpO1xuXG4vLyBDdXJyZW50bHkgd2l0aG91dCBhc3RyYWwgY2hhcmFjdGVycyBzdXBwb3J0LlxuZnVuY3Rpb24gaXNQdW5jdENoYXIoY2gpIHtcbiAgcmV0dXJuIFVOSUNPREVfUFVOQ1RfUkUudGVzdChjaCk7XG59XG5cblxuLy8gTWFya2Rvd24gQVNDSUkgcHVuY3R1YXRpb24gY2hhcmFjdGVycy5cbi8vXG4vLyAhLCBcIiwgIywgJCwgJSwgJiwgJywgKCwgKSwgKiwgKywgLCwgLSwgLiwgLywgOiwgOywgPCwgPSwgPiwgPywgQCwgWywgXFwsIF0sIF4sIF8sIGAsIHssIHwsIH0sIG9yIH5cbi8vIGh0dHA6Ly9zcGVjLmNvbW1vbm1hcmsub3JnLzAuMTUvI2FzY2lpLXB1bmN0dWF0aW9uLWNoYXJhY3RlclxuLy9cbi8vIERvbid0IGNvbmZ1c2Ugd2l0aCB1bmljb2RlIHB1bmN0dWF0aW9uICEhISBJdCBsYWNrcyBzb21lIGNoYXJzIGluIGFzY2lpIHJhbmdlLlxuLy9cbmZ1bmN0aW9uIGlzTWRBc2NpaVB1bmN0KGNoKSB7XG4gIHN3aXRjaCAoY2gpIHtcbiAgICBjYXNlIDB4MjEvKiAhICovOlxuICAgIGNhc2UgMHgyMi8qIFwiICovOlxuICAgIGNhc2UgMHgyMy8qICMgKi86XG4gICAgY2FzZSAweDI0LyogJCAqLzpcbiAgICBjYXNlIDB4MjUvKiAlICovOlxuICAgIGNhc2UgMHgyNi8qICYgKi86XG4gICAgY2FzZSAweDI3LyogJyAqLzpcbiAgICBjYXNlIDB4MjgvKiAoICovOlxuICAgIGNhc2UgMHgyOS8qICkgKi86XG4gICAgY2FzZSAweDJBLyogKiAqLzpcbiAgICBjYXNlIDB4MkIvKiArICovOlxuICAgIGNhc2UgMHgyQy8qICwgKi86XG4gICAgY2FzZSAweDJELyogLSAqLzpcbiAgICBjYXNlIDB4MkUvKiAuICovOlxuICAgIGNhc2UgMHgyRi8qIC8gKi86XG4gICAgY2FzZSAweDNBLyogOiAqLzpcbiAgICBjYXNlIDB4M0IvKiA7ICovOlxuICAgIGNhc2UgMHgzQy8qIDwgKi86XG4gICAgY2FzZSAweDNELyogPSAqLzpcbiAgICBjYXNlIDB4M0UvKiA+ICovOlxuICAgIGNhc2UgMHgzRi8qID8gKi86XG4gICAgY2FzZSAweDQwLyogQCAqLzpcbiAgICBjYXNlIDB4NUIvKiBbICovOlxuICAgIGNhc2UgMHg1Qy8qIFxcICovOlxuICAgIGNhc2UgMHg1RC8qIF0gKi86XG4gICAgY2FzZSAweDVFLyogXiAqLzpcbiAgICBjYXNlIDB4NUYvKiBfICovOlxuICAgIGNhc2UgMHg2MC8qIGAgKi86XG4gICAgY2FzZSAweDdCLyogeyAqLzpcbiAgICBjYXNlIDB4N0MvKiB8ICovOlxuICAgIGNhc2UgMHg3RC8qIH0gKi86XG4gICAgY2FzZSAweDdFLyogfiAqLzpcbiAgICAgIHJldHVybiB0cnVlO1xuICAgIGRlZmF1bHQ6XG4gICAgICByZXR1cm4gZmFsc2U7XG4gIH1cbn1cblxuLy8gSGVwbGVyIHRvIHVuaWZ5IFtyZWZlcmVuY2UgbGFiZWxzXS5cbi8vXG5mdW5jdGlvbiBub3JtYWxpemVSZWZlcmVuY2Uoc3RyKSB7XG4gIC8vIFRyaW0gYW5kIGNvbGxhcHNlIHdoaXRlc3BhY2VcbiAgLy9cbiAgc3RyID0gc3RyLnRyaW0oKS5yZXBsYWNlKC9cXHMrL2csICcgJyk7XG5cbiAgLy8gSW4gbm9kZSB2MTAgJ+G6nicudG9Mb3dlckNhc2UoKSA9PT0gJ+G5vicsIHdoaWNoIGlzIHByZXN1bWVkIHRvIGJlIGEgYnVnXG4gIC8vIGZpeGVkIGluIHYxMiAoY291bGRuJ3QgZmluZCBhbnkgZGV0YWlscykuXG4gIC8vXG4gIC8vIFNvIHRyZWF0IHRoaXMgb25lIGFzIGEgc3BlY2lhbCBjYXNlXG4gIC8vIChyZW1vdmUgdGhpcyB3aGVuIG5vZGUgdjEwIGlzIG5vIGxvbmdlciBzdXBwb3J0ZWQpLlxuICAvL1xuICBpZiAoJ+G6nicudG9Mb3dlckNhc2UoKSA9PT0gJ+G5vicpIHtcbiAgICBzdHIgPSBzdHIucmVwbGFjZSgv4bqeL2csICfDnycpO1xuICB9XG5cbiAgLy8gLnRvTG93ZXJDYXNlKCkudG9VcHBlckNhc2UoKSBzaG91bGQgZ2V0IHJpZCBvZiBhbGwgZGlmZmVyZW5jZXNcbiAgLy8gYmV0d2VlbiBsZXR0ZXIgdmFyaWFudHMuXG4gIC8vXG4gIC8vIFNpbXBsZSAudG9Mb3dlckNhc2UoKSBkb2Vzbid0IG5vcm1hbGl6ZSAxMjUgY29kZSBwb2ludHMgY29ycmVjdGx5LFxuICAvLyBhbmQgLnRvVXBwZXJDYXNlIGRvZXNuJ3Qgbm9ybWFsaXplIDYgb2YgdGhlbSAobGlzdCBvZiBleGNlcHRpb25zOlxuICAvLyDEsCwgz7QsIOG6niwg4oSmLCDihKosIOKEqyAtIHRob3NlIGFyZSBhbHJlYWR5IHVwcGVyY2FzZWQsIGJ1dCBoYXZlIGRpZmZlcmVudGx5XG4gIC8vIHVwcGVyY2FzZWQgdmVyc2lvbnMpLlxuICAvL1xuICAvLyBIZXJlJ3MgYW4gZXhhbXBsZSBzaG93aW5nIGhvdyBpdCBoYXBwZW5zLiBMZXRzIHRha2UgZ3JlZWsgbGV0dGVyIG9tZWdhOlxuICAvLyB1cHBlcmNhc2UgVSswMzk4ICjOmCksIFUrMDNmNCAoz7QpIGFuZCBsb3dlcmNhc2UgVSswM2I4ICjOuCksIFUrMDNkMSAoz5EpXG4gIC8vXG4gIC8vIFVuaWNvZGUgZW50cmllczpcbiAgLy8gMDM5ODtHUkVFSyBDQVBJVEFMIExFVFRFUiBUSEVUQTtMdTswO0w7Ozs7O047Ozs7MDNCODtcbiAgLy8gMDNCODtHUkVFSyBTTUFMTCBMRVRURVIgVEhFVEE7TGw7MDtMOzs7OztOOzs7MDM5ODs7MDM5OFxuICAvLyAwM0QxO0dSRUVLIFRIRVRBIFNZTUJPTDtMbDswO0w7PGNvbXBhdD4gMDNCODs7OztOO0dSRUVLIFNNQUxMIExFVFRFUiBTQ1JJUFQgVEhFVEE7OzAzOTg7OzAzOThcbiAgLy8gMDNGNDtHUkVFSyBDQVBJVEFMIFRIRVRBIFNZTUJPTDtMdTswO0w7PGNvbXBhdD4gMDM5ODs7OztOOzs7OzAzQjg7XG4gIC8vXG4gIC8vIENhc2UtaW5zZW5zaXRpdmUgY29tcGFyaXNvbiBzaG91bGQgdHJlYXQgYWxsIG9mIHRoZW0gYXMgZXF1aXZhbGVudC5cbiAgLy9cbiAgLy8gQnV0IC50b0xvd2VyQ2FzZSgpIGRvZXNuJ3QgY2hhbmdlIM+RIChpdCdzIGFscmVhZHkgbG93ZXJjYXNlKSxcbiAgLy8gYW5kIC50b1VwcGVyQ2FzZSgpIGRvZXNuJ3QgY2hhbmdlIM+0IChhbHJlYWR5IHVwcGVyY2FzZSkuXG4gIC8vXG4gIC8vIEFwcGx5aW5nIGZpcnN0IGxvd2VyIHRoZW4gdXBwZXIgY2FzZSBub3JtYWxpemVzIGFueSBjaGFyYWN0ZXI6XG4gIC8vICdcXHUwMzk4XFx1MDNmNFxcdTAzYjhcXHUwM2QxJy50b0xvd2VyQ2FzZSgpLnRvVXBwZXJDYXNlKCkgPT09ICdcXHUwMzk4XFx1MDM5OFxcdTAzOThcXHUwMzk4J1xuICAvL1xuICAvLyBOb3RlOiB0aGlzIGlzIGVxdWl2YWxlbnQgdG8gdW5pY29kZSBjYXNlIGZvbGRpbmc7IHVuaWNvZGUgbm9ybWFsaXphdGlvblxuICAvLyBpcyBhIGRpZmZlcmVudCBzdGVwIHRoYXQgaXMgbm90IHJlcXVpcmVkIGhlcmUuXG4gIC8vXG4gIC8vIEZpbmFsIHJlc3VsdCBzaG91bGQgYmUgdXBwZXJjYXNlZCwgYmVjYXVzZSBpdCdzIGxhdGVyIHN0b3JlZCBpbiBhbiBvYmplY3RcbiAgLy8gKHRoaXMgYXZvaWQgYSBjb25mbGljdCB3aXRoIE9iamVjdC5wcm90b3R5cGUgbWVtYmVycyxcbiAgLy8gbW9zdCBub3RhYmx5LCBgX19wcm90b19fYClcbiAgLy9cbiAgcmV0dXJuIHN0ci50b0xvd2VyQ2FzZSgpLnRvVXBwZXJDYXNlKCk7XG59XG5cbi8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vXG5cbi8vIFJlLWV4cG9ydCBsaWJyYXJpZXMgY29tbW9ubHkgdXNlZCBpbiBib3RoIG1hcmtkb3duLWl0IGFuZCBpdHMgcGx1Z2lucyxcbi8vIHNvIHBsdWdpbnMgd29uJ3QgaGF2ZSB0byBkZXBlbmQgb24gdGhlbSBleHBsaWNpdGx5LCB3aGljaCByZWR1Y2VzIHRoZWlyXG4vLyBidW5kbGVkIHNpemUgKGUuZy4gYSBicm93c2VyIGJ1aWxkKS5cbi8vXG5leHBvcnRzLmxpYiAgICAgICAgICAgICAgICAgPSB7fTtcbmV4cG9ydHMubGliLm1kdXJsICAgICAgICAgICA9IHJlcXVpcmUoJ21kdXJsJyk7XG5leHBvcnRzLmxpYi51Y21pY3JvICAgICAgICAgPSByZXF1aXJlKCd1Yy5taWNybycpO1xuXG5leHBvcnRzLmFzc2lnbiAgICAgICAgICAgICAgPSBhc3NpZ247XG5leHBvcnRzLmlzU3RyaW5nICAgICAgICAgICAgPSBpc1N0cmluZztcbmV4cG9ydHMuaGFzICAgICAgICAgICAgICAgICA9IGhhcztcbmV4cG9ydHMudW5lc2NhcGVNZCAgICAgICAgICA9IHVuZXNjYXBlTWQ7XG5leHBvcnRzLnVuZXNjYXBlQWxsICAgICAgICAgPSB1bmVzY2FwZUFsbDtcbmV4cG9ydHMuaXNWYWxpZEVudGl0eUNvZGUgICA9IGlzVmFsaWRFbnRpdHlDb2RlO1xuZXhwb3J0cy5mcm9tQ29kZVBvaW50ICAgICAgID0gZnJvbUNvZGVQb2ludDtcbi8vIGV4cG9ydHMucmVwbGFjZUVudGl0aWVzICAgICA9IHJlcGxhY2VFbnRpdGllcztcbmV4cG9ydHMuZXNjYXBlSHRtbCAgICAgICAgICA9IGVzY2FwZUh0bWw7XG5leHBvcnRzLmFycmF5UmVwbGFjZUF0ICAgICAgPSBhcnJheVJlcGxhY2VBdDtcbmV4cG9ydHMuaXNTcGFjZSAgICAgICAgICAgICA9IGlzU3BhY2U7XG5leHBvcnRzLmlzV2hpdGVTcGFjZSAgICAgICAgPSBpc1doaXRlU3BhY2U7XG5leHBvcnRzLmlzTWRBc2NpaVB1bmN0ICAgICAgPSBpc01kQXNjaWlQdW5jdDtcbmV4cG9ydHMuaXNQdW5jdENoYXIgICAgICAgICA9IGlzUHVuY3RDaGFyO1xuZXhwb3J0cy5lc2NhcGVSRSAgICAgICAgICAgID0gZXNjYXBlUkU7XG5leHBvcnRzLm5vcm1hbGl6ZVJlZmVyZW5jZSAgPSBub3JtYWxpemVSZWZlcmVuY2U7XG4iLCIvLyBKdXN0IGEgc2hvcnRjdXQgZm9yIGJ1bGsgZXhwb3J0XG4ndXNlIHN0cmljdCc7XG5cblxuZXhwb3J0cy5wYXJzZUxpbmtMYWJlbCAgICAgICA9IHJlcXVpcmUoJy4vcGFyc2VfbGlua19sYWJlbCcpO1xuZXhwb3J0cy5wYXJzZUxpbmtEZXN0aW5hdGlvbiA9IHJlcXVpcmUoJy4vcGFyc2VfbGlua19kZXN0aW5hdGlvbicpO1xuZXhwb3J0cy5wYXJzZUxpbmtUaXRsZSAgICAgICA9IHJlcXVpcmUoJy4vcGFyc2VfbGlua190aXRsZScpO1xuIiwiLy8gUGFyc2UgbGluayBkZXN0aW5hdGlvblxuLy9cbid1c2Ugc3RyaWN0JztcblxuXG52YXIgdW5lc2NhcGVBbGwgPSByZXF1aXJlKCcuLi9jb21tb24vdXRpbHMnKS51bmVzY2FwZUFsbDtcblxuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIHBhcnNlTGlua0Rlc3RpbmF0aW9uKHN0ciwgcG9zLCBtYXgpIHtcbiAgdmFyIGNvZGUsIGxldmVsLFxuICAgICAgbGluZXMgPSAwLFxuICAgICAgc3RhcnQgPSBwb3MsXG4gICAgICByZXN1bHQgPSB7XG4gICAgICAgIG9rOiBmYWxzZSxcbiAgICAgICAgcG9zOiAwLFxuICAgICAgICBsaW5lczogMCxcbiAgICAgICAgc3RyOiAnJ1xuICAgICAgfTtcblxuICBpZiAoc3RyLmNoYXJDb2RlQXQocG9zKSA9PT0gMHgzQyAvKiA8ICovKSB7XG4gICAgcG9zKys7XG4gICAgd2hpbGUgKHBvcyA8IG1heCkge1xuICAgICAgY29kZSA9IHN0ci5jaGFyQ29kZUF0KHBvcyk7XG4gICAgICBpZiAoY29kZSA9PT0gMHgwQSAvKiBcXG4gKi8pIHsgcmV0dXJuIHJlc3VsdDsgfVxuICAgICAgaWYgKGNvZGUgPT09IDB4M0UgLyogPiAqLykge1xuICAgICAgICByZXN1bHQucG9zID0gcG9zICsgMTtcbiAgICAgICAgcmVzdWx0LnN0ciA9IHVuZXNjYXBlQWxsKHN0ci5zbGljZShzdGFydCArIDEsIHBvcykpO1xuICAgICAgICByZXN1bHQub2sgPSB0cnVlO1xuICAgICAgICByZXR1cm4gcmVzdWx0O1xuICAgICAgfVxuICAgICAgaWYgKGNvZGUgPT09IDB4NUMgLyogXFwgKi8gJiYgcG9zICsgMSA8IG1heCkge1xuICAgICAgICBwb3MgKz0gMjtcbiAgICAgICAgY29udGludWU7XG4gICAgICB9XG5cbiAgICAgIHBvcysrO1xuICAgIH1cblxuICAgIC8vIG5vIGNsb3NpbmcgJz4nXG4gICAgcmV0dXJuIHJlc3VsdDtcbiAgfVxuXG4gIC8vIHRoaXMgc2hvdWxkIGJlIC4uLiB9IGVsc2UgeyAuLi4gYnJhbmNoXG5cbiAgbGV2ZWwgPSAwO1xuICB3aGlsZSAocG9zIDwgbWF4KSB7XG4gICAgY29kZSA9IHN0ci5jaGFyQ29kZUF0KHBvcyk7XG5cbiAgICBpZiAoY29kZSA9PT0gMHgyMCkgeyBicmVhazsgfVxuXG4gICAgLy8gYXNjaWkgY29udHJvbCBjaGFyYWN0ZXJzXG4gICAgaWYgKGNvZGUgPCAweDIwIHx8IGNvZGUgPT09IDB4N0YpIHsgYnJlYWs7IH1cblxuICAgIGlmIChjb2RlID09PSAweDVDIC8qIFxcICovICYmIHBvcyArIDEgPCBtYXgpIHtcbiAgICAgIHBvcyArPSAyO1xuICAgICAgY29udGludWU7XG4gICAgfVxuXG4gICAgaWYgKGNvZGUgPT09IDB4MjggLyogKCAqLykge1xuICAgICAgbGV2ZWwrKztcbiAgICB9XG5cbiAgICBpZiAoY29kZSA9PT0gMHgyOSAvKiApICovKSB7XG4gICAgICBpZiAobGV2ZWwgPT09IDApIHsgYnJlYWs7IH1cbiAgICAgIGxldmVsLS07XG4gICAgfVxuXG4gICAgcG9zKys7XG4gIH1cblxuICBpZiAoc3RhcnQgPT09IHBvcykgeyByZXR1cm4gcmVzdWx0OyB9XG4gIGlmIChsZXZlbCAhPT0gMCkgeyByZXR1cm4gcmVzdWx0OyB9XG5cbiAgcmVzdWx0LnN0ciA9IHVuZXNjYXBlQWxsKHN0ci5zbGljZShzdGFydCwgcG9zKSk7XG4gIHJlc3VsdC5saW5lcyA9IGxpbmVzO1xuICByZXN1bHQucG9zID0gcG9zO1xuICByZXN1bHQub2sgPSB0cnVlO1xuICByZXR1cm4gcmVzdWx0O1xufTtcbiIsIi8vIFBhcnNlIGxpbmsgbGFiZWxcbi8vXG4vLyB0aGlzIGZ1bmN0aW9uIGFzc3VtZXMgdGhhdCBmaXJzdCBjaGFyYWN0ZXIgKFwiW1wiKSBhbHJlYWR5IG1hdGNoZXM7XG4vLyByZXR1cm5zIHRoZSBlbmQgb2YgdGhlIGxhYmVsXG4vL1xuJ3VzZSBzdHJpY3QnO1xuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIHBhcnNlTGlua0xhYmVsKHN0YXRlLCBzdGFydCwgZGlzYWJsZU5lc3RlZCkge1xuICB2YXIgbGV2ZWwsIGZvdW5kLCBtYXJrZXIsIHByZXZQb3MsXG4gICAgICBsYWJlbEVuZCA9IC0xLFxuICAgICAgbWF4ID0gc3RhdGUucG9zTWF4LFxuICAgICAgb2xkUG9zID0gc3RhdGUucG9zO1xuXG4gIHN0YXRlLnBvcyA9IHN0YXJ0ICsgMTtcbiAgbGV2ZWwgPSAxO1xuXG4gIHdoaWxlIChzdGF0ZS5wb3MgPCBtYXgpIHtcbiAgICBtYXJrZXIgPSBzdGF0ZS5zcmMuY2hhckNvZGVBdChzdGF0ZS5wb3MpO1xuICAgIGlmIChtYXJrZXIgPT09IDB4NUQgLyogXSAqLykge1xuICAgICAgbGV2ZWwtLTtcbiAgICAgIGlmIChsZXZlbCA9PT0gMCkge1xuICAgICAgICBmb3VuZCA9IHRydWU7XG4gICAgICAgIGJyZWFrO1xuICAgICAgfVxuICAgIH1cblxuICAgIHByZXZQb3MgPSBzdGF0ZS5wb3M7XG4gICAgc3RhdGUubWQuaW5saW5lLnNraXBUb2tlbihzdGF0ZSk7XG4gICAgaWYgKG1hcmtlciA9PT0gMHg1QiAvKiBbICovKSB7XG4gICAgICBpZiAocHJldlBvcyA9PT0gc3RhdGUucG9zIC0gMSkge1xuICAgICAgICAvLyBpbmNyZWFzZSBsZXZlbCBpZiB3ZSBmaW5kIHRleHQgYFtgLCB3aGljaCBpcyBub3QgYSBwYXJ0IG9mIGFueSB0b2tlblxuICAgICAgICBsZXZlbCsrO1xuICAgICAgfSBlbHNlIGlmIChkaXNhYmxlTmVzdGVkKSB7XG4gICAgICAgIHN0YXRlLnBvcyA9IG9sZFBvcztcbiAgICAgICAgcmV0dXJuIC0xO1xuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIGlmIChmb3VuZCkge1xuICAgIGxhYmVsRW5kID0gc3RhdGUucG9zO1xuICB9XG5cbiAgLy8gcmVzdG9yZSBvbGQgc3RhdGVcbiAgc3RhdGUucG9zID0gb2xkUG9zO1xuXG4gIHJldHVybiBsYWJlbEVuZDtcbn07XG4iLCIvLyBQYXJzZSBsaW5rIHRpdGxlXG4vL1xuJ3VzZSBzdHJpY3QnO1xuXG5cbnZhciB1bmVzY2FwZUFsbCA9IHJlcXVpcmUoJy4uL2NvbW1vbi91dGlscycpLnVuZXNjYXBlQWxsO1xuXG5cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gcGFyc2VMaW5rVGl0bGUoc3RyLCBwb3MsIG1heCkge1xuICB2YXIgY29kZSxcbiAgICAgIG1hcmtlcixcbiAgICAgIGxpbmVzID0gMCxcbiAgICAgIHN0YXJ0ID0gcG9zLFxuICAgICAgcmVzdWx0ID0ge1xuICAgICAgICBvazogZmFsc2UsXG4gICAgICAgIHBvczogMCxcbiAgICAgICAgbGluZXM6IDAsXG4gICAgICAgIHN0cjogJydcbiAgICAgIH07XG5cbiAgaWYgKHBvcyA+PSBtYXgpIHsgcmV0dXJuIHJlc3VsdDsgfVxuXG4gIG1hcmtlciA9IHN0ci5jaGFyQ29kZUF0KHBvcyk7XG5cbiAgaWYgKG1hcmtlciAhPT0gMHgyMiAvKiBcIiAqLyAmJiBtYXJrZXIgIT09IDB4MjcgLyogJyAqLyAmJiBtYXJrZXIgIT09IDB4MjggLyogKCAqLykgeyByZXR1cm4gcmVzdWx0OyB9XG5cbiAgcG9zKys7XG5cbiAgLy8gaWYgb3BlbmluZyBtYXJrZXIgaXMgXCIoXCIsIHN3aXRjaCBpdCB0byBjbG9zaW5nIG1hcmtlciBcIilcIlxuICBpZiAobWFya2VyID09PSAweDI4KSB7IG1hcmtlciA9IDB4Mjk7IH1cblxuICB3aGlsZSAocG9zIDwgbWF4KSB7XG4gICAgY29kZSA9IHN0ci5jaGFyQ29kZUF0KHBvcyk7XG4gICAgaWYgKGNvZGUgPT09IG1hcmtlcikge1xuICAgICAgcmVzdWx0LnBvcyA9IHBvcyArIDE7XG4gICAgICByZXN1bHQubGluZXMgPSBsaW5lcztcbiAgICAgIHJlc3VsdC5zdHIgPSB1bmVzY2FwZUFsbChzdHIuc2xpY2Uoc3RhcnQgKyAxLCBwb3MpKTtcbiAgICAgIHJlc3VsdC5vayA9IHRydWU7XG4gICAgICByZXR1cm4gcmVzdWx0O1xuICAgIH0gZWxzZSBpZiAoY29kZSA9PT0gMHgwQSkge1xuICAgICAgbGluZXMrKztcbiAgICB9IGVsc2UgaWYgKGNvZGUgPT09IDB4NUMgLyogXFwgKi8gJiYgcG9zICsgMSA8IG1heCkge1xuICAgICAgcG9zKys7XG4gICAgICBpZiAoc3RyLmNoYXJDb2RlQXQocG9zKSA9PT0gMHgwQSkge1xuICAgICAgICBsaW5lcysrO1xuICAgICAgfVxuICAgIH1cblxuICAgIHBvcysrO1xuICB9XG5cbiAgcmV0dXJuIHJlc3VsdDtcbn07XG4iLCIvLyBNYWluIHBhcnNlciBjbGFzc1xuXG4ndXNlIHN0cmljdCc7XG5cblxudmFyIHV0aWxzICAgICAgICA9IHJlcXVpcmUoJy4vY29tbW9uL3V0aWxzJyk7XG52YXIgaGVscGVycyAgICAgID0gcmVxdWlyZSgnLi9oZWxwZXJzJyk7XG52YXIgUmVuZGVyZXIgICAgID0gcmVxdWlyZSgnLi9yZW5kZXJlcicpO1xudmFyIFBhcnNlckNvcmUgICA9IHJlcXVpcmUoJy4vcGFyc2VyX2NvcmUnKTtcbnZhciBQYXJzZXJCbG9jayAgPSByZXF1aXJlKCcuL3BhcnNlcl9ibG9jaycpO1xudmFyIFBhcnNlcklubGluZSA9IHJlcXVpcmUoJy4vcGFyc2VyX2lubGluZScpO1xudmFyIExpbmtpZnlJdCAgICA9IHJlcXVpcmUoJ2xpbmtpZnktaXQnKTtcbnZhciBtZHVybCAgICAgICAgPSByZXF1aXJlKCdtZHVybCcpO1xudmFyIHB1bnljb2RlICAgICA9IHJlcXVpcmUoJ3B1bnljb2RlJyk7XG5cblxudmFyIGNvbmZpZyA9IHtcbiAgJ2RlZmF1bHQnOiByZXF1aXJlKCcuL3ByZXNldHMvZGVmYXVsdCcpLFxuICB6ZXJvOiByZXF1aXJlKCcuL3ByZXNldHMvemVybycpLFxuICBjb21tb25tYXJrOiByZXF1aXJlKCcuL3ByZXNldHMvY29tbW9ubWFyaycpXG59O1xuXG4vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vL1xuLy9cbi8vIFRoaXMgdmFsaWRhdG9yIGNhbiBwcm9oaWJpdCBtb3JlIHRoYW4gcmVhbGx5IG5lZWRlZCB0byBwcmV2ZW50IFhTUy4gSXQncyBhXG4vLyB0cmFkZW9mZiB0byBrZWVwIGNvZGUgc2ltcGxlIGFuZCB0byBiZSBzZWN1cmUgYnkgZGVmYXVsdC5cbi8vXG4vLyBJZiB5b3UgbmVlZCBkaWZmZXJlbnQgc2V0dXAgLSBvdmVycmlkZSB2YWxpZGF0b3IgbWV0aG9kIGFzIHlvdSB3aXNoLiBPclxuLy8gcmVwbGFjZSBpdCB3aXRoIGR1bW15IGZ1bmN0aW9uIGFuZCB1c2UgZXh0ZXJuYWwgc2FuaXRpemVyLlxuLy9cblxudmFyIEJBRF9QUk9UT19SRSA9IC9eKHZic2NyaXB0fGphdmFzY3JpcHR8ZmlsZXxkYXRhKTovO1xudmFyIEdPT0RfREFUQV9SRSA9IC9eZGF0YTppbWFnZVxcLyhnaWZ8cG5nfGpwZWd8d2VicCk7LztcblxuZnVuY3Rpb24gdmFsaWRhdGVMaW5rKHVybCkge1xuICAvLyB1cmwgc2hvdWxkIGJlIG5vcm1hbGl6ZWQgYXQgdGhpcyBwb2ludCwgYW5kIGV4aXN0aW5nIGVudGl0aWVzIGFyZSBkZWNvZGVkXG4gIHZhciBzdHIgPSB1cmwudHJpbSgpLnRvTG93ZXJDYXNlKCk7XG5cbiAgcmV0dXJuIEJBRF9QUk9UT19SRS50ZXN0KHN0cikgPyAoR09PRF9EQVRBX1JFLnRlc3Qoc3RyKSA/IHRydWUgOiBmYWxzZSkgOiB0cnVlO1xufVxuXG4vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vL1xuXG5cbnZhciBSRUNPREVfSE9TVE5BTUVfRk9SID0gWyAnaHR0cDonLCAnaHR0cHM6JywgJ21haWx0bzonIF07XG5cbmZ1bmN0aW9uIG5vcm1hbGl6ZUxpbmsodXJsKSB7XG4gIHZhciBwYXJzZWQgPSBtZHVybC5wYXJzZSh1cmwsIHRydWUpO1xuXG4gIGlmIChwYXJzZWQuaG9zdG5hbWUpIHtcbiAgICAvLyBFbmNvZGUgaG9zdG5hbWVzIGluIHVybHMgbGlrZTpcbiAgICAvLyBgaHR0cDovL2hvc3QvYCwgYGh0dHBzOi8vaG9zdC9gLCBgbWFpbHRvOnVzZXJAaG9zdGAsIGAvL2hvc3QvYFxuICAgIC8vXG4gICAgLy8gV2UgZG9uJ3QgZW5jb2RlIHVua25vd24gc2NoZW1hcywgYmVjYXVzZSBpdCdzIGxpa2VseSB0aGF0IHdlIGVuY29kZVxuICAgIC8vIHNvbWV0aGluZyB3ZSBzaG91bGRuJ3QgKGUuZy4gYHNreXBlOm5hbWVgIHRyZWF0ZWQgYXMgYHNreXBlOmhvc3RgKVxuICAgIC8vXG4gICAgaWYgKCFwYXJzZWQucHJvdG9jb2wgfHwgUkVDT0RFX0hPU1ROQU1FX0ZPUi5pbmRleE9mKHBhcnNlZC5wcm90b2NvbCkgPj0gMCkge1xuICAgICAgdHJ5IHtcbiAgICAgICAgcGFyc2VkLmhvc3RuYW1lID0gcHVueWNvZGUudG9BU0NJSShwYXJzZWQuaG9zdG5hbWUpO1xuICAgICAgfSBjYXRjaCAoZXIpIHsgLyoqLyB9XG4gICAgfVxuICB9XG5cbiAgcmV0dXJuIG1kdXJsLmVuY29kZShtZHVybC5mb3JtYXQocGFyc2VkKSk7XG59XG5cbmZ1bmN0aW9uIG5vcm1hbGl6ZUxpbmtUZXh0KHVybCkge1xuICB2YXIgcGFyc2VkID0gbWR1cmwucGFyc2UodXJsLCB0cnVlKTtcblxuICBpZiAocGFyc2VkLmhvc3RuYW1lKSB7XG4gICAgLy8gRW5jb2RlIGhvc3RuYW1lcyBpbiB1cmxzIGxpa2U6XG4gICAgLy8gYGh0dHA6Ly9ob3N0L2AsIGBodHRwczovL2hvc3QvYCwgYG1haWx0bzp1c2VyQGhvc3RgLCBgLy9ob3N0L2BcbiAgICAvL1xuICAgIC8vIFdlIGRvbid0IGVuY29kZSB1bmtub3duIHNjaGVtYXMsIGJlY2F1c2UgaXQncyBsaWtlbHkgdGhhdCB3ZSBlbmNvZGVcbiAgICAvLyBzb21ldGhpbmcgd2Ugc2hvdWxkbid0IChlLmcuIGBza3lwZTpuYW1lYCB0cmVhdGVkIGFzIGBza3lwZTpob3N0YClcbiAgICAvL1xuICAgIGlmICghcGFyc2VkLnByb3RvY29sIHx8IFJFQ09ERV9IT1NUTkFNRV9GT1IuaW5kZXhPZihwYXJzZWQucHJvdG9jb2wpID49IDApIHtcbiAgICAgIHRyeSB7XG4gICAgICAgIHBhcnNlZC5ob3N0bmFtZSA9IHB1bnljb2RlLnRvVW5pY29kZShwYXJzZWQuaG9zdG5hbWUpO1xuICAgICAgfSBjYXRjaCAoZXIpIHsgLyoqLyB9XG4gICAgfVxuICB9XG5cbiAgcmV0dXJuIG1kdXJsLmRlY29kZShtZHVybC5mb3JtYXQocGFyc2VkKSk7XG59XG5cblxuLyoqXG4gKiBjbGFzcyBNYXJrZG93bkl0XG4gKlxuICogTWFpbiBwYXJzZXIvcmVuZGVyZXIgY2xhc3MuXG4gKlxuICogIyMjIyMgVXNhZ2VcbiAqXG4gKiBgYGBqYXZhc2NyaXB0XG4gKiAvLyBub2RlLmpzLCBcImNsYXNzaWNcIiB3YXk6XG4gKiB2YXIgTWFya2Rvd25JdCA9IHJlcXVpcmUoJ21hcmtkb3duLWl0JyksXG4gKiAgICAgbWQgPSBuZXcgTWFya2Rvd25JdCgpO1xuICogdmFyIHJlc3VsdCA9IG1kLnJlbmRlcignIyBtYXJrZG93bi1pdCBydWxlenohJyk7XG4gKlxuICogLy8gbm9kZS5qcywgdGhlIHNhbWUsIGJ1dCB3aXRoIHN1Z2FyOlxuICogdmFyIG1kID0gcmVxdWlyZSgnbWFya2Rvd24taXQnKSgpO1xuICogdmFyIHJlc3VsdCA9IG1kLnJlbmRlcignIyBtYXJrZG93bi1pdCBydWxlenohJyk7XG4gKlxuICogLy8gYnJvd3NlciB3aXRob3V0IEFNRCwgYWRkZWQgdG8gXCJ3aW5kb3dcIiBvbiBzY3JpcHQgbG9hZFxuICogLy8gTm90ZSwgdGhlcmUgYXJlIG5vIGRhc2guXG4gKiB2YXIgbWQgPSB3aW5kb3cubWFya2Rvd25pdCgpO1xuICogdmFyIHJlc3VsdCA9IG1kLnJlbmRlcignIyBtYXJrZG93bi1pdCBydWxlenohJyk7XG4gKiBgYGBcbiAqXG4gKiBTaW5nbGUgbGluZSByZW5kZXJpbmcsIHdpdGhvdXQgcGFyYWdyYXBoIHdyYXA6XG4gKlxuICogYGBgamF2YXNjcmlwdFxuICogdmFyIG1kID0gcmVxdWlyZSgnbWFya2Rvd24taXQnKSgpO1xuICogdmFyIHJlc3VsdCA9IG1kLnJlbmRlcklubGluZSgnX19tYXJrZG93bi1pdF9fIHJ1bGV6eiEnKTtcbiAqIGBgYFxuICoqL1xuXG4vKipcbiAqIG5ldyBNYXJrZG93bkl0KFtwcmVzZXROYW1lLCBvcHRpb25zXSlcbiAqIC0gcHJlc2V0TmFtZSAoU3RyaW5nKTogb3B0aW9uYWwsIGBjb21tb25tYXJrYCAvIGB6ZXJvYFxuICogLSBvcHRpb25zIChPYmplY3QpXG4gKlxuICogQ3JlYXRlcyBwYXJzZXIgaW5zdGFuc2Ugd2l0aCBnaXZlbiBjb25maWcuIENhbiBiZSBjYWxsZWQgd2l0aG91dCBgbmV3YC5cbiAqXG4gKiAjIyMjIyBwcmVzZXROYW1lXG4gKlxuICogTWFya2Rvd25JdCBwcm92aWRlcyBuYW1lZCBwcmVzZXRzIGFzIGEgY29udmVuaWVuY2UgdG8gcXVpY2tseVxuICogZW5hYmxlL2Rpc2FibGUgYWN0aXZlIHN5bnRheCBydWxlcyBhbmQgb3B0aW9ucyBmb3IgY29tbW9uIHVzZSBjYXNlcy5cbiAqXG4gKiAtIFtcImNvbW1vbm1hcmtcIl0oaHR0cHM6Ly9naXRodWIuY29tL21hcmtkb3duLWl0L21hcmtkb3duLWl0L2Jsb2IvbWFzdGVyL2xpYi9wcmVzZXRzL2NvbW1vbm1hcmsuanMpIC1cbiAqICAgY29uZmlndXJlcyBwYXJzZXIgdG8gc3RyaWN0IFtDb21tb25NYXJrXShodHRwOi8vY29tbW9ubWFyay5vcmcvKSBtb2RlLlxuICogLSBbZGVmYXVsdF0oaHR0cHM6Ly9naXRodWIuY29tL21hcmtkb3duLWl0L21hcmtkb3duLWl0L2Jsb2IvbWFzdGVyL2xpYi9wcmVzZXRzL2RlZmF1bHQuanMpIC1cbiAqICAgc2ltaWxhciB0byBHRk0sIHVzZWQgd2hlbiBubyBwcmVzZXQgbmFtZSBnaXZlbi4gRW5hYmxlcyBhbGwgYXZhaWxhYmxlIHJ1bGVzLFxuICogICBidXQgc3RpbGwgd2l0aG91dCBodG1sLCB0eXBvZ3JhcGhlciAmIGF1dG9saW5rZXIuXG4gKiAtIFtcInplcm9cIl0oaHR0cHM6Ly9naXRodWIuY29tL21hcmtkb3duLWl0L21hcmtkb3duLWl0L2Jsb2IvbWFzdGVyL2xpYi9wcmVzZXRzL3plcm8uanMpIC1cbiAqICAgYWxsIHJ1bGVzIGRpc2FibGVkLiBVc2VmdWwgdG8gcXVpY2tseSBzZXR1cCB5b3VyIGNvbmZpZyB2aWEgYC5lbmFibGUoKWAuXG4gKiAgIEZvciBleGFtcGxlLCB3aGVuIHlvdSBuZWVkIG9ubHkgYGJvbGRgIGFuZCBgaXRhbGljYCBtYXJrdXAgYW5kIG5vdGhpbmcgZWxzZS5cbiAqXG4gKiAjIyMjIyBvcHRpb25zOlxuICpcbiAqIC0gX19odG1sX18gLSBgZmFsc2VgLiBTZXQgYHRydWVgIHRvIGVuYWJsZSBIVE1MIHRhZ3MgaW4gc291cmNlLiBCZSBjYXJlZnVsIVxuICogICBUaGF0J3Mgbm90IHNhZmUhIFlvdSBtYXkgbmVlZCBleHRlcm5hbCBzYW5pdGl6ZXIgdG8gcHJvdGVjdCBvdXRwdXQgZnJvbSBYU1MuXG4gKiAgIEl0J3MgYmV0dGVyIHRvIGV4dGVuZCBmZWF0dXJlcyB2aWEgcGx1Z2lucywgaW5zdGVhZCBvZiBlbmFibGluZyBIVE1MLlxuICogLSBfX3hodG1sT3V0X18gLSBgZmFsc2VgLiBTZXQgYHRydWVgIHRvIGFkZCAnLycgd2hlbiBjbG9zaW5nIHNpbmdsZSB0YWdzXG4gKiAgIChgPGJyIC8+YCkuIFRoaXMgaXMgbmVlZGVkIG9ubHkgZm9yIGZ1bGwgQ29tbW9uTWFyayBjb21wYXRpYmlsaXR5LiBJbiByZWFsXG4gKiAgIHdvcmxkIHlvdSB3aWxsIG5lZWQgSFRNTCBvdXRwdXQuXG4gKiAtIF9fYnJlYWtzX18gLSBgZmFsc2VgLiBTZXQgYHRydWVgIHRvIGNvbnZlcnQgYFxcbmAgaW4gcGFyYWdyYXBocyBpbnRvIGA8YnI+YC5cbiAqIC0gX19sYW5nUHJlZml4X18gLSBgbGFuZ3VhZ2UtYC4gQ1NTIGxhbmd1YWdlIGNsYXNzIHByZWZpeCBmb3IgZmVuY2VkIGJsb2Nrcy5cbiAqICAgQ2FuIGJlIHVzZWZ1bCBmb3IgZXh0ZXJuYWwgaGlnaGxpZ2h0ZXJzLlxuICogLSBfX2xpbmtpZnlfXyAtIGBmYWxzZWAuIFNldCBgdHJ1ZWAgdG8gYXV0b2NvbnZlcnQgVVJMLWxpa2UgdGV4dCB0byBsaW5rcy5cbiAqIC0gX190eXBvZ3JhcGhlcl9fICAtIGBmYWxzZWAuIFNldCBgdHJ1ZWAgdG8gZW5hYmxlIFtzb21lIGxhbmd1YWdlLW5ldXRyYWxcbiAqICAgcmVwbGFjZW1lbnRdKGh0dHBzOi8vZ2l0aHViLmNvbS9tYXJrZG93bi1pdC9tYXJrZG93bi1pdC9ibG9iL21hc3Rlci9saWIvcnVsZXNfY29yZS9yZXBsYWNlbWVudHMuanMpICtcbiAqICAgcXVvdGVzIGJlYXV0aWZpY2F0aW9uIChzbWFydHF1b3RlcykuXG4gKiAtIF9fcXVvdGVzX18gLSBg4oCc4oCd4oCY4oCZYCwgU3RyaW5nIG9yIEFycmF5LiBEb3VibGUgKyBzaW5nbGUgcXVvdGVzIHJlcGxhY2VtZW50XG4gKiAgIHBhaXJzLCB3aGVuIHR5cG9ncmFwaGVyIGVuYWJsZWQgYW5kIHNtYXJ0cXVvdGVzIG9uLiBGb3IgZXhhbXBsZSwgeW91IGNhblxuICogICB1c2UgYCfCq8K74oCe4oCcJ2AgZm9yIFJ1c3NpYW4sIGAn4oCe4oCc4oCa4oCYJ2AgZm9yIEdlcm1hbiwgYW5kXG4gKiAgIGBbJ8KrXFx4QTAnLCAnXFx4QTDCuycsICfigLlcXHhBMCcsICdcXHhBMOKAuiddYCBmb3IgRnJlbmNoIChpbmNsdWRpbmcgbmJzcCkuXG4gKiAtIF9faGlnaGxpZ2h0X18gLSBgbnVsbGAuIEhpZ2hsaWdodGVyIGZ1bmN0aW9uIGZvciBmZW5jZWQgY29kZSBibG9ja3MuXG4gKiAgIEhpZ2hsaWdodGVyIGBmdW5jdGlvbiAoc3RyLCBsYW5nKWAgc2hvdWxkIHJldHVybiBlc2NhcGVkIEhUTUwuIEl0IGNhbiBhbHNvXG4gKiAgIHJldHVybiBlbXB0eSBzdHJpbmcgaWYgdGhlIHNvdXJjZSB3YXMgbm90IGNoYW5nZWQgYW5kIHNob3VsZCBiZSBlc2NhcGVkXG4gKiAgIGV4dGVybmFseS4gSWYgcmVzdWx0IHN0YXJ0cyB3aXRoIDxwcmUuLi4gaW50ZXJuYWwgd3JhcHBlciBpcyBza2lwcGVkLlxuICpcbiAqICMjIyMjIEV4YW1wbGVcbiAqXG4gKiBgYGBqYXZhc2NyaXB0XG4gKiAvLyBjb21tb25tYXJrIG1vZGVcbiAqIHZhciBtZCA9IHJlcXVpcmUoJ21hcmtkb3duLWl0JykoJ2NvbW1vbm1hcmsnKTtcbiAqXG4gKiAvLyBkZWZhdWx0IG1vZGVcbiAqIHZhciBtZCA9IHJlcXVpcmUoJ21hcmtkb3duLWl0JykoKTtcbiAqXG4gKiAvLyBlbmFibGUgZXZlcnl0aGluZ1xuICogdmFyIG1kID0gcmVxdWlyZSgnbWFya2Rvd24taXQnKSh7XG4gKiAgIGh0bWw6IHRydWUsXG4gKiAgIGxpbmtpZnk6IHRydWUsXG4gKiAgIHR5cG9ncmFwaGVyOiB0cnVlXG4gKiB9KTtcbiAqIGBgYFxuICpcbiAqICMjIyMjIFN5bnRheCBoaWdobGlnaHRpbmdcbiAqXG4gKiBgYGBqc1xuICogdmFyIGhsanMgPSByZXF1aXJlKCdoaWdobGlnaHQuanMnKSAvLyBodHRwczovL2hpZ2hsaWdodGpzLm9yZy9cbiAqXG4gKiB2YXIgbWQgPSByZXF1aXJlKCdtYXJrZG93bi1pdCcpKHtcbiAqICAgaGlnaGxpZ2h0OiBmdW5jdGlvbiAoc3RyLCBsYW5nKSB7XG4gKiAgICAgaWYgKGxhbmcgJiYgaGxqcy5nZXRMYW5ndWFnZShsYW5nKSkge1xuICogICAgICAgdHJ5IHtcbiAqICAgICAgICAgcmV0dXJuIGhsanMuaGlnaGxpZ2h0KGxhbmcsIHN0ciwgdHJ1ZSkudmFsdWU7XG4gKiAgICAgICB9IGNhdGNoIChfXykge31cbiAqICAgICB9XG4gKlxuICogICAgIHJldHVybiAnJzsgLy8gdXNlIGV4dGVybmFsIGRlZmF1bHQgZXNjYXBpbmdcbiAqICAgfVxuICogfSk7XG4gKiBgYGBcbiAqXG4gKiBPciB3aXRoIGZ1bGwgd3JhcHBlciBvdmVycmlkZSAoaWYgeW91IG5lZWQgYXNzaWduIGNsYXNzIHRvIGA8cHJlPmApOlxuICpcbiAqIGBgYGphdmFzY3JpcHRcbiAqIHZhciBobGpzID0gcmVxdWlyZSgnaGlnaGxpZ2h0LmpzJykgLy8gaHR0cHM6Ly9oaWdobGlnaHRqcy5vcmcvXG4gKlxuICogLy8gQWN0dWFsIGRlZmF1bHQgdmFsdWVzXG4gKiB2YXIgbWQgPSByZXF1aXJlKCdtYXJrZG93bi1pdCcpKHtcbiAqICAgaGlnaGxpZ2h0OiBmdW5jdGlvbiAoc3RyLCBsYW5nKSB7XG4gKiAgICAgaWYgKGxhbmcgJiYgaGxqcy5nZXRMYW5ndWFnZShsYW5nKSkge1xuICogICAgICAgdHJ5IHtcbiAqICAgICAgICAgcmV0dXJuICc8cHJlIGNsYXNzPVwiaGxqc1wiPjxjb2RlPicgK1xuICogICAgICAgICAgICAgICAgaGxqcy5oaWdobGlnaHQobGFuZywgc3RyLCB0cnVlKS52YWx1ZSArXG4gKiAgICAgICAgICAgICAgICAnPC9jb2RlPjwvcHJlPic7XG4gKiAgICAgICB9IGNhdGNoIChfXykge31cbiAqICAgICB9XG4gKlxuICogICAgIHJldHVybiAnPHByZSBjbGFzcz1cImhsanNcIj48Y29kZT4nICsgbWQudXRpbHMuZXNjYXBlSHRtbChzdHIpICsgJzwvY29kZT48L3ByZT4nO1xuICogICB9XG4gKiB9KTtcbiAqIGBgYFxuICpcbiAqKi9cbmZ1bmN0aW9uIE1hcmtkb3duSXQocHJlc2V0TmFtZSwgb3B0aW9ucykge1xuICBpZiAoISh0aGlzIGluc3RhbmNlb2YgTWFya2Rvd25JdCkpIHtcbiAgICByZXR1cm4gbmV3IE1hcmtkb3duSXQocHJlc2V0TmFtZSwgb3B0aW9ucyk7XG4gIH1cblxuICBpZiAoIW9wdGlvbnMpIHtcbiAgICBpZiAoIXV0aWxzLmlzU3RyaW5nKHByZXNldE5hbWUpKSB7XG4gICAgICBvcHRpb25zID0gcHJlc2V0TmFtZSB8fCB7fTtcbiAgICAgIHByZXNldE5hbWUgPSAnZGVmYXVsdCc7XG4gICAgfVxuICB9XG5cbiAgLyoqXG4gICAqIE1hcmtkb3duSXQjaW5saW5lIC0+IFBhcnNlcklubGluZVxuICAgKlxuICAgKiBJbnN0YW5jZSBvZiBbW1BhcnNlcklubGluZV1dLiBZb3UgbWF5IG5lZWQgaXQgdG8gYWRkIG5ldyBydWxlcyB3aGVuXG4gICAqIHdyaXRpbmcgcGx1Z2lucy4gRm9yIHNpbXBsZSBydWxlcyBjb250cm9sIHVzZSBbW01hcmtkb3duSXQuZGlzYWJsZV1dIGFuZFxuICAgKiBbW01hcmtkb3duSXQuZW5hYmxlXV0uXG4gICAqKi9cbiAgdGhpcy5pbmxpbmUgPSBuZXcgUGFyc2VySW5saW5lKCk7XG5cbiAgLyoqXG4gICAqIE1hcmtkb3duSXQjYmxvY2sgLT4gUGFyc2VyQmxvY2tcbiAgICpcbiAgICogSW5zdGFuY2Ugb2YgW1tQYXJzZXJCbG9ja11dLiBZb3UgbWF5IG5lZWQgaXQgdG8gYWRkIG5ldyBydWxlcyB3aGVuXG4gICAqIHdyaXRpbmcgcGx1Z2lucy4gRm9yIHNpbXBsZSBydWxlcyBjb250cm9sIHVzZSBbW01hcmtkb3duSXQuZGlzYWJsZV1dIGFuZFxuICAgKiBbW01hcmtkb3duSXQuZW5hYmxlXV0uXG4gICAqKi9cbiAgdGhpcy5ibG9jayA9IG5ldyBQYXJzZXJCbG9jaygpO1xuXG4gIC8qKlxuICAgKiBNYXJrZG93bkl0I2NvcmUgLT4gQ29yZVxuICAgKlxuICAgKiBJbnN0YW5jZSBvZiBbW0NvcmVdXSBjaGFpbiBleGVjdXRvci4gWW91IG1heSBuZWVkIGl0IHRvIGFkZCBuZXcgcnVsZXMgd2hlblxuICAgKiB3cml0aW5nIHBsdWdpbnMuIEZvciBzaW1wbGUgcnVsZXMgY29udHJvbCB1c2UgW1tNYXJrZG93bkl0LmRpc2FibGVdXSBhbmRcbiAgICogW1tNYXJrZG93bkl0LmVuYWJsZV1dLlxuICAgKiovXG4gIHRoaXMuY29yZSA9IG5ldyBQYXJzZXJDb3JlKCk7XG5cbiAgLyoqXG4gICAqIE1hcmtkb3duSXQjcmVuZGVyZXIgLT4gUmVuZGVyZXJcbiAgICpcbiAgICogSW5zdGFuY2Ugb2YgW1tSZW5kZXJlcl1dLiBVc2UgaXQgdG8gbW9kaWZ5IG91dHB1dCBsb29rLiBPciB0byBhZGQgcmVuZGVyaW5nXG4gICAqIHJ1bGVzIGZvciBuZXcgdG9rZW4gdHlwZXMsIGdlbmVyYXRlZCBieSBwbHVnaW5zLlxuICAgKlxuICAgKiAjIyMjIyBFeGFtcGxlXG4gICAqXG4gICAqIGBgYGphdmFzY3JpcHRcbiAgICogdmFyIG1kID0gcmVxdWlyZSgnbWFya2Rvd24taXQnKSgpO1xuICAgKlxuICAgKiBmdW5jdGlvbiBteVRva2VuKHRva2VucywgaWR4LCBvcHRpb25zLCBlbnYsIHNlbGYpIHtcbiAgICogICAvLy4uLlxuICAgKiAgIHJldHVybiByZXN1bHQ7XG4gICAqIH07XG4gICAqXG4gICAqIG1kLnJlbmRlcmVyLnJ1bGVzWydteV90b2tlbiddID0gbXlUb2tlblxuICAgKiBgYGBcbiAgICpcbiAgICogU2VlIFtbUmVuZGVyZXJdXSBkb2NzIGFuZCBbc291cmNlIGNvZGVdKGh0dHBzOi8vZ2l0aHViLmNvbS9tYXJrZG93bi1pdC9tYXJrZG93bi1pdC9ibG9iL21hc3Rlci9saWIvcmVuZGVyZXIuanMpLlxuICAgKiovXG4gIHRoaXMucmVuZGVyZXIgPSBuZXcgUmVuZGVyZXIoKTtcblxuICAvKipcbiAgICogTWFya2Rvd25JdCNsaW5raWZ5IC0+IExpbmtpZnlJdFxuICAgKlxuICAgKiBbbGlua2lmeS1pdF0oaHR0cHM6Ly9naXRodWIuY29tL21hcmtkb3duLWl0L2xpbmtpZnktaXQpIGluc3RhbmNlLlxuICAgKiBVc2VkIGJ5IFtsaW5raWZ5XShodHRwczovL2dpdGh1Yi5jb20vbWFya2Rvd24taXQvbWFya2Rvd24taXQvYmxvYi9tYXN0ZXIvbGliL3J1bGVzX2NvcmUvbGlua2lmeS5qcylcbiAgICogcnVsZS5cbiAgICoqL1xuICB0aGlzLmxpbmtpZnkgPSBuZXcgTGlua2lmeUl0KCk7XG5cbiAgLyoqXG4gICAqIE1hcmtkb3duSXQjdmFsaWRhdGVMaW5rKHVybCkgLT4gQm9vbGVhblxuICAgKlxuICAgKiBMaW5rIHZhbGlkYXRpb24gZnVuY3Rpb24uIENvbW1vbk1hcmsgYWxsb3dzIHRvbyBtdWNoIGluIGxpbmtzLiBCeSBkZWZhdWx0XG4gICAqIHdlIGRpc2FibGUgYGphdmFzY3JpcHQ6YCwgYHZic2NyaXB0OmAsIGBmaWxlOmAgc2NoZW1hcywgYW5kIGFsbW9zdCBhbGwgYGRhdGE6Li4uYCBzY2hlbWFzXG4gICAqIGV4Y2VwdCBzb21lIGVtYmVkZGVkIGltYWdlIHR5cGVzLlxuICAgKlxuICAgKiBZb3UgY2FuIGNoYW5nZSB0aGlzIGJlaGF2aW91cjpcbiAgICpcbiAgICogYGBgamF2YXNjcmlwdFxuICAgKiB2YXIgbWQgPSByZXF1aXJlKCdtYXJrZG93bi1pdCcpKCk7XG4gICAqIC8vIGVuYWJsZSBldmVyeXRoaW5nXG4gICAqIG1kLnZhbGlkYXRlTGluayA9IGZ1bmN0aW9uICgpIHsgcmV0dXJuIHRydWU7IH1cbiAgICogYGBgXG4gICAqKi9cbiAgdGhpcy52YWxpZGF0ZUxpbmsgPSB2YWxpZGF0ZUxpbms7XG5cbiAgLyoqXG4gICAqIE1hcmtkb3duSXQjbm9ybWFsaXplTGluayh1cmwpIC0+IFN0cmluZ1xuICAgKlxuICAgKiBGdW5jdGlvbiB1c2VkIHRvIGVuY29kZSBsaW5rIHVybCB0byBhIG1hY2hpbmUtcmVhZGFibGUgZm9ybWF0LFxuICAgKiB3aGljaCBpbmNsdWRlcyB1cmwtZW5jb2RpbmcsIHB1bnljb2RlLCBldGMuXG4gICAqKi9cbiAgdGhpcy5ub3JtYWxpemVMaW5rID0gbm9ybWFsaXplTGluaztcblxuICAvKipcbiAgICogTWFya2Rvd25JdCNub3JtYWxpemVMaW5rVGV4dCh1cmwpIC0+IFN0cmluZ1xuICAgKlxuICAgKiBGdW5jdGlvbiB1c2VkIHRvIGRlY29kZSBsaW5rIHVybCB0byBhIGh1bWFuLXJlYWRhYmxlIGZvcm1hdGBcbiAgICoqL1xuICB0aGlzLm5vcm1hbGl6ZUxpbmtUZXh0ID0gbm9ybWFsaXplTGlua1RleHQ7XG5cblxuICAvLyBFeHBvc2UgdXRpbHMgJiBoZWxwZXJzIGZvciBlYXN5IGFjY2VzIGZyb20gcGx1Z2luc1xuXG4gIC8qKlxuICAgKiBNYXJrZG93bkl0I3V0aWxzIC0+IHV0aWxzXG4gICAqXG4gICAqIEFzc29ydGVkIHV0aWxpdHkgZnVuY3Rpb25zLCB1c2VmdWwgdG8gd3JpdGUgcGx1Z2lucy4gU2VlIGRldGFpbHNcbiAgICogW2hlcmVdKGh0dHBzOi8vZ2l0aHViLmNvbS9tYXJrZG93bi1pdC9tYXJrZG93bi1pdC9ibG9iL21hc3Rlci9saWIvY29tbW9uL3V0aWxzLmpzKS5cbiAgICoqL1xuICB0aGlzLnV0aWxzID0gdXRpbHM7XG5cbiAgLyoqXG4gICAqIE1hcmtkb3duSXQjaGVscGVycyAtPiBoZWxwZXJzXG4gICAqXG4gICAqIExpbmsgY29tcG9uZW50cyBwYXJzZXIgZnVuY3Rpb25zLCB1c2VmdWwgdG8gd3JpdGUgcGx1Z2lucy4gU2VlIGRldGFpbHNcbiAgICogW2hlcmVdKGh0dHBzOi8vZ2l0aHViLmNvbS9tYXJrZG93bi1pdC9tYXJrZG93bi1pdC9ibG9iL21hc3Rlci9saWIvaGVscGVycykuXG4gICAqKi9cbiAgdGhpcy5oZWxwZXJzID0gdXRpbHMuYXNzaWduKHt9LCBoZWxwZXJzKTtcblxuXG4gIHRoaXMub3B0aW9ucyA9IHt9O1xuICB0aGlzLmNvbmZpZ3VyZShwcmVzZXROYW1lKTtcblxuICBpZiAob3B0aW9ucykgeyB0aGlzLnNldChvcHRpb25zKTsgfVxufVxuXG5cbi8qKiBjaGFpbmFibGVcbiAqIE1hcmtkb3duSXQuc2V0KG9wdGlvbnMpXG4gKlxuICogU2V0IHBhcnNlciBvcHRpb25zIChpbiB0aGUgc2FtZSBmb3JtYXQgYXMgaW4gY29uc3RydWN0b3IpLiBQcm9iYWJseSwgeW91XG4gKiB3aWxsIG5ldmVyIG5lZWQgaXQsIGJ1dCB5b3UgY2FuIGNoYW5nZSBvcHRpb25zIGFmdGVyIGNvbnN0cnVjdG9yIGNhbGwuXG4gKlxuICogIyMjIyMgRXhhbXBsZVxuICpcbiAqIGBgYGphdmFzY3JpcHRcbiAqIHZhciBtZCA9IHJlcXVpcmUoJ21hcmtkb3duLWl0JykoKVxuICogICAgICAgICAgICAgLnNldCh7IGh0bWw6IHRydWUsIGJyZWFrczogdHJ1ZSB9KVxuICogICAgICAgICAgICAgLnNldCh7IHR5cG9ncmFwaGVyLCB0cnVlIH0pO1xuICogYGBgXG4gKlxuICogX19Ob3RlOl9fIFRvIGFjaGlldmUgdGhlIGJlc3QgcG9zc2libGUgcGVyZm9ybWFuY2UsIGRvbid0IG1vZGlmeSBhXG4gKiBgbWFya2Rvd24taXRgIGluc3RhbmNlIG9wdGlvbnMgb24gdGhlIGZseS4gSWYgeW91IG5lZWQgbXVsdGlwbGUgY29uZmlndXJhdGlvbnNcbiAqIGl0J3MgYmVzdCB0byBjcmVhdGUgbXVsdGlwbGUgaW5zdGFuY2VzIGFuZCBpbml0aWFsaXplIGVhY2ggd2l0aCBzZXBhcmF0ZVxuICogY29uZmlnLlxuICoqL1xuTWFya2Rvd25JdC5wcm90b3R5cGUuc2V0ID0gZnVuY3Rpb24gKG9wdGlvbnMpIHtcbiAgdXRpbHMuYXNzaWduKHRoaXMub3B0aW9ucywgb3B0aW9ucyk7XG4gIHJldHVybiB0aGlzO1xufTtcblxuXG4vKiogY2hhaW5hYmxlLCBpbnRlcm5hbFxuICogTWFya2Rvd25JdC5jb25maWd1cmUocHJlc2V0cylcbiAqXG4gKiBCYXRjaCBsb2FkIG9mIGFsbCBvcHRpb25zIGFuZCBjb21wZW5lbnQgc2V0dGluZ3MuIFRoaXMgaXMgaW50ZXJuYWwgbWV0aG9kLFxuICogYW5kIHlvdSBwcm9iYWJseSB3aWxsIG5vdCBuZWVkIGl0LiBCdXQgaWYgeW91IHdpdGggLSBzZWUgYXZhaWxhYmxlIHByZXNldHNcbiAqIGFuZCBkYXRhIHN0cnVjdHVyZSBbaGVyZV0oaHR0cHM6Ly9naXRodWIuY29tL21hcmtkb3duLWl0L21hcmtkb3duLWl0L3RyZWUvbWFzdGVyL2xpYi9wcmVzZXRzKVxuICpcbiAqIFdlIHN0cm9uZ2x5IHJlY29tbWVuZCB0byB1c2UgcHJlc2V0cyBpbnN0ZWFkIG9mIGRpcmVjdCBjb25maWcgbG9hZHMuIFRoYXRcbiAqIHdpbGwgZ2l2ZSBiZXR0ZXIgY29tcGF0aWJpbGl0eSB3aXRoIG5leHQgdmVyc2lvbnMuXG4gKiovXG5NYXJrZG93bkl0LnByb3RvdHlwZS5jb25maWd1cmUgPSBmdW5jdGlvbiAocHJlc2V0cykge1xuICB2YXIgc2VsZiA9IHRoaXMsIHByZXNldE5hbWU7XG5cbiAgaWYgKHV0aWxzLmlzU3RyaW5nKHByZXNldHMpKSB7XG4gICAgcHJlc2V0TmFtZSA9IHByZXNldHM7XG4gICAgcHJlc2V0cyA9IGNvbmZpZ1twcmVzZXROYW1lXTtcbiAgICBpZiAoIXByZXNldHMpIHsgdGhyb3cgbmV3IEVycm9yKCdXcm9uZyBgbWFya2Rvd24taXRgIHByZXNldCBcIicgKyBwcmVzZXROYW1lICsgJ1wiLCBjaGVjayBuYW1lJyk7IH1cbiAgfVxuXG4gIGlmICghcHJlc2V0cykgeyB0aHJvdyBuZXcgRXJyb3IoJ1dyb25nIGBtYXJrZG93bi1pdGAgcHJlc2V0LCBjYW5cXCd0IGJlIGVtcHR5Jyk7IH1cblxuICBpZiAocHJlc2V0cy5vcHRpb25zKSB7IHNlbGYuc2V0KHByZXNldHMub3B0aW9ucyk7IH1cblxuICBpZiAocHJlc2V0cy5jb21wb25lbnRzKSB7XG4gICAgT2JqZWN0LmtleXMocHJlc2V0cy5jb21wb25lbnRzKS5mb3JFYWNoKGZ1bmN0aW9uIChuYW1lKSB7XG4gICAgICBpZiAocHJlc2V0cy5jb21wb25lbnRzW25hbWVdLnJ1bGVzKSB7XG4gICAgICAgIHNlbGZbbmFtZV0ucnVsZXIuZW5hYmxlT25seShwcmVzZXRzLmNvbXBvbmVudHNbbmFtZV0ucnVsZXMpO1xuICAgICAgfVxuICAgICAgaWYgKHByZXNldHMuY29tcG9uZW50c1tuYW1lXS5ydWxlczIpIHtcbiAgICAgICAgc2VsZltuYW1lXS5ydWxlcjIuZW5hYmxlT25seShwcmVzZXRzLmNvbXBvbmVudHNbbmFtZV0ucnVsZXMyKTtcbiAgICAgIH1cbiAgICB9KTtcbiAgfVxuICByZXR1cm4gdGhpcztcbn07XG5cblxuLyoqIGNoYWluYWJsZVxuICogTWFya2Rvd25JdC5lbmFibGUobGlzdCwgaWdub3JlSW52YWxpZClcbiAqIC0gbGlzdCAoU3RyaW5nfEFycmF5KTogcnVsZSBuYW1lIG9yIGxpc3Qgb2YgcnVsZSBuYW1lcyB0byBlbmFibGVcbiAqIC0gaWdub3JlSW52YWxpZCAoQm9vbGVhbik6IHNldCBgdHJ1ZWAgdG8gaWdub3JlIGVycm9ycyB3aGVuIHJ1bGUgbm90IGZvdW5kLlxuICpcbiAqIEVuYWJsZSBsaXN0IG9yIHJ1bGVzLiBJdCB3aWxsIGF1dG9tYXRpY2FsbHkgZmluZCBhcHByb3ByaWF0ZSBjb21wb25lbnRzLFxuICogY29udGFpbmluZyBydWxlcyB3aXRoIGdpdmVuIG5hbWVzLiBJZiBydWxlIG5vdCBmb3VuZCwgYW5kIGBpZ25vcmVJbnZhbGlkYFxuICogbm90IHNldCAtIHRocm93cyBleGNlcHRpb24uXG4gKlxuICogIyMjIyMgRXhhbXBsZVxuICpcbiAqIGBgYGphdmFzY3JpcHRcbiAqIHZhciBtZCA9IHJlcXVpcmUoJ21hcmtkb3duLWl0JykoKVxuICogICAgICAgICAgICAgLmVuYWJsZShbJ3N1YicsICdzdXAnXSlcbiAqICAgICAgICAgICAgIC5kaXNhYmxlKCdzbWFydHF1b3RlcycpO1xuICogYGBgXG4gKiovXG5NYXJrZG93bkl0LnByb3RvdHlwZS5lbmFibGUgPSBmdW5jdGlvbiAobGlzdCwgaWdub3JlSW52YWxpZCkge1xuICB2YXIgcmVzdWx0ID0gW107XG5cbiAgaWYgKCFBcnJheS5pc0FycmF5KGxpc3QpKSB7IGxpc3QgPSBbIGxpc3QgXTsgfVxuXG4gIFsgJ2NvcmUnLCAnYmxvY2snLCAnaW5saW5lJyBdLmZvckVhY2goZnVuY3Rpb24gKGNoYWluKSB7XG4gICAgcmVzdWx0ID0gcmVzdWx0LmNvbmNhdCh0aGlzW2NoYWluXS5ydWxlci5lbmFibGUobGlzdCwgdHJ1ZSkpO1xuICB9LCB0aGlzKTtcblxuICByZXN1bHQgPSByZXN1bHQuY29uY2F0KHRoaXMuaW5saW5lLnJ1bGVyMi5lbmFibGUobGlzdCwgdHJ1ZSkpO1xuXG4gIHZhciBtaXNzZWQgPSBsaXN0LmZpbHRlcihmdW5jdGlvbiAobmFtZSkgeyByZXR1cm4gcmVzdWx0LmluZGV4T2YobmFtZSkgPCAwOyB9KTtcblxuICBpZiAobWlzc2VkLmxlbmd0aCAmJiAhaWdub3JlSW52YWxpZCkge1xuICAgIHRocm93IG5ldyBFcnJvcignTWFya2Rvd25JdC4gRmFpbGVkIHRvIGVuYWJsZSB1bmtub3duIHJ1bGUocyk6ICcgKyBtaXNzZWQpO1xuICB9XG5cbiAgcmV0dXJuIHRoaXM7XG59O1xuXG5cbi8qKiBjaGFpbmFibGVcbiAqIE1hcmtkb3duSXQuZGlzYWJsZShsaXN0LCBpZ25vcmVJbnZhbGlkKVxuICogLSBsaXN0IChTdHJpbmd8QXJyYXkpOiBydWxlIG5hbWUgb3IgbGlzdCBvZiBydWxlIG5hbWVzIHRvIGRpc2FibGUuXG4gKiAtIGlnbm9yZUludmFsaWQgKEJvb2xlYW4pOiBzZXQgYHRydWVgIHRvIGlnbm9yZSBlcnJvcnMgd2hlbiBydWxlIG5vdCBmb3VuZC5cbiAqXG4gKiBUaGUgc2FtZSBhcyBbW01hcmtkb3duSXQuZW5hYmxlXV0sIGJ1dCB0dXJuIHNwZWNpZmllZCBydWxlcyBvZmYuXG4gKiovXG5NYXJrZG93bkl0LnByb3RvdHlwZS5kaXNhYmxlID0gZnVuY3Rpb24gKGxpc3QsIGlnbm9yZUludmFsaWQpIHtcbiAgdmFyIHJlc3VsdCA9IFtdO1xuXG4gIGlmICghQXJyYXkuaXNBcnJheShsaXN0KSkgeyBsaXN0ID0gWyBsaXN0IF07IH1cblxuICBbICdjb3JlJywgJ2Jsb2NrJywgJ2lubGluZScgXS5mb3JFYWNoKGZ1bmN0aW9uIChjaGFpbikge1xuICAgIHJlc3VsdCA9IHJlc3VsdC5jb25jYXQodGhpc1tjaGFpbl0ucnVsZXIuZGlzYWJsZShsaXN0LCB0cnVlKSk7XG4gIH0sIHRoaXMpO1xuXG4gIHJlc3VsdCA9IHJlc3VsdC5jb25jYXQodGhpcy5pbmxpbmUucnVsZXIyLmRpc2FibGUobGlzdCwgdHJ1ZSkpO1xuXG4gIHZhciBtaXNzZWQgPSBsaXN0LmZpbHRlcihmdW5jdGlvbiAobmFtZSkgeyByZXR1cm4gcmVzdWx0LmluZGV4T2YobmFtZSkgPCAwOyB9KTtcblxuICBpZiAobWlzc2VkLmxlbmd0aCAmJiAhaWdub3JlSW52YWxpZCkge1xuICAgIHRocm93IG5ldyBFcnJvcignTWFya2Rvd25JdC4gRmFpbGVkIHRvIGRpc2FibGUgdW5rbm93biBydWxlKHMpOiAnICsgbWlzc2VkKTtcbiAgfVxuICByZXR1cm4gdGhpcztcbn07XG5cblxuLyoqIGNoYWluYWJsZVxuICogTWFya2Rvd25JdC51c2UocGx1Z2luLCBwYXJhbXMpXG4gKlxuICogTG9hZCBzcGVjaWZpZWQgcGx1Z2luIHdpdGggZ2l2ZW4gcGFyYW1zIGludG8gY3VycmVudCBwYXJzZXIgaW5zdGFuY2UuXG4gKiBJdCdzIGp1c3QgYSBzdWdhciB0byBjYWxsIGBwbHVnaW4obWQsIHBhcmFtcylgIHdpdGggY3VycmluZy5cbiAqXG4gKiAjIyMjIyBFeGFtcGxlXG4gKlxuICogYGBgamF2YXNjcmlwdFxuICogdmFyIGl0ZXJhdG9yID0gcmVxdWlyZSgnbWFya2Rvd24taXQtZm9yLWlubGluZScpO1xuICogdmFyIG1kID0gcmVxdWlyZSgnbWFya2Rvd24taXQnKSgpXG4gKiAgICAgICAgICAgICAudXNlKGl0ZXJhdG9yLCAnZm9vX3JlcGxhY2UnLCAndGV4dCcsIGZ1bmN0aW9uICh0b2tlbnMsIGlkeCkge1xuICogICAgICAgICAgICAgICB0b2tlbnNbaWR4XS5jb250ZW50ID0gdG9rZW5zW2lkeF0uY29udGVudC5yZXBsYWNlKC9mb28vZywgJ2JhcicpO1xuICogICAgICAgICAgICAgfSk7XG4gKiBgYGBcbiAqKi9cbk1hcmtkb3duSXQucHJvdG90eXBlLnVzZSA9IGZ1bmN0aW9uIChwbHVnaW4gLyosIHBhcmFtcywgLi4uICovKSB7XG4gIHZhciBhcmdzID0gWyB0aGlzIF0uY29uY2F0KEFycmF5LnByb3RvdHlwZS5zbGljZS5jYWxsKGFyZ3VtZW50cywgMSkpO1xuICBwbHVnaW4uYXBwbHkocGx1Z2luLCBhcmdzKTtcbiAgcmV0dXJuIHRoaXM7XG59O1xuXG5cbi8qKiBpbnRlcm5hbFxuICogTWFya2Rvd25JdC5wYXJzZShzcmMsIGVudikgLT4gQXJyYXlcbiAqIC0gc3JjIChTdHJpbmcpOiBzb3VyY2Ugc3RyaW5nXG4gKiAtIGVudiAoT2JqZWN0KTogZW52aXJvbm1lbnQgc2FuZGJveFxuICpcbiAqIFBhcnNlIGlucHV0IHN0cmluZyBhbmQgcmV0dXJucyBsaXN0IG9mIGJsb2NrIHRva2VucyAoc3BlY2lhbCB0b2tlbiB0eXBlXG4gKiBcImlubGluZVwiIHdpbGwgY29udGFpbiBsaXN0IG9mIGlubGluZSB0b2tlbnMpLiBZb3Ugc2hvdWxkIG5vdCBjYWxsIHRoaXNcbiAqIG1ldGhvZCBkaXJlY3RseSwgdW50aWwgeW91IHdyaXRlIGN1c3RvbSByZW5kZXJlciAoZm9yIGV4YW1wbGUsIHRvIHByb2R1Y2VcbiAqIEFTVCkuXG4gKlxuICogYGVudmAgaXMgdXNlZCB0byBwYXNzIGRhdGEgYmV0d2VlbiBcImRpc3RyaWJ1dGVkXCIgcnVsZXMgYW5kIHJldHVybiBhZGRpdGlvbmFsXG4gKiBtZXRhZGF0YSBsaWtlIHJlZmVyZW5jZSBpbmZvLCBuZWVkZWQgZm9yIHRoZSByZW5kZXJlci4gSXQgYWxzbyBjYW4gYmUgdXNlZCB0b1xuICogaW5qZWN0IGRhdGEgaW4gc3BlY2lmaWMgY2FzZXMuIFVzdWFsbHksIHlvdSB3aWxsIGJlIG9rIHRvIHBhc3MgYHt9YCxcbiAqIGFuZCB0aGVuIHBhc3MgdXBkYXRlZCBvYmplY3QgdG8gcmVuZGVyZXIuXG4gKiovXG5NYXJrZG93bkl0LnByb3RvdHlwZS5wYXJzZSA9IGZ1bmN0aW9uIChzcmMsIGVudikge1xuICBpZiAodHlwZW9mIHNyYyAhPT0gJ3N0cmluZycpIHtcbiAgICB0aHJvdyBuZXcgRXJyb3IoJ0lucHV0IGRhdGEgc2hvdWxkIGJlIGEgU3RyaW5nJyk7XG4gIH1cblxuICB2YXIgc3RhdGUgPSBuZXcgdGhpcy5jb3JlLlN0YXRlKHNyYywgdGhpcywgZW52KTtcblxuICB0aGlzLmNvcmUucHJvY2VzcyhzdGF0ZSk7XG5cbiAgcmV0dXJuIHN0YXRlLnRva2Vucztcbn07XG5cblxuLyoqXG4gKiBNYXJrZG93bkl0LnJlbmRlcihzcmMgWywgZW52XSkgLT4gU3RyaW5nXG4gKiAtIHNyYyAoU3RyaW5nKTogc291cmNlIHN0cmluZ1xuICogLSBlbnYgKE9iamVjdCk6IGVudmlyb25tZW50IHNhbmRib3hcbiAqXG4gKiBSZW5kZXIgbWFya2Rvd24gc3RyaW5nIGludG8gaHRtbC4gSXQgZG9lcyBhbGwgbWFnaWMgZm9yIHlvdSA6KS5cbiAqXG4gKiBgZW52YCBjYW4gYmUgdXNlZCB0byBpbmplY3QgYWRkaXRpb25hbCBtZXRhZGF0YSAoYHt9YCBieSBkZWZhdWx0KS5cbiAqIEJ1dCB5b3Ugd2lsbCBub3QgbmVlZCBpdCB3aXRoIGhpZ2ggcHJvYmFiaWxpdHkuIFNlZSBhbHNvIGNvbW1lbnRcbiAqIGluIFtbTWFya2Rvd25JdC5wYXJzZV1dLlxuICoqL1xuTWFya2Rvd25JdC5wcm90b3R5cGUucmVuZGVyID0gZnVuY3Rpb24gKHNyYywgZW52KSB7XG4gIGVudiA9IGVudiB8fCB7fTtcblxuICByZXR1cm4gdGhpcy5yZW5kZXJlci5yZW5kZXIodGhpcy5wYXJzZShzcmMsIGVudiksIHRoaXMub3B0aW9ucywgZW52KTtcbn07XG5cblxuLyoqIGludGVybmFsXG4gKiBNYXJrZG93bkl0LnBhcnNlSW5saW5lKHNyYywgZW52KSAtPiBBcnJheVxuICogLSBzcmMgKFN0cmluZyk6IHNvdXJjZSBzdHJpbmdcbiAqIC0gZW52IChPYmplY3QpOiBlbnZpcm9ubWVudCBzYW5kYm94XG4gKlxuICogVGhlIHNhbWUgYXMgW1tNYXJrZG93bkl0LnBhcnNlXV0gYnV0IHNraXAgYWxsIGJsb2NrIHJ1bGVzLiBJdCByZXR1cm5zIHRoZVxuICogYmxvY2sgdG9rZW5zIGxpc3Qgd2l0aCB0aGUgc2luZ2xlIGBpbmxpbmVgIGVsZW1lbnQsIGNvbnRhaW5pbmcgcGFyc2VkIGlubGluZVxuICogdG9rZW5zIGluIGBjaGlsZHJlbmAgcHJvcGVydHkuIEFsc28gdXBkYXRlcyBgZW52YCBvYmplY3QuXG4gKiovXG5NYXJrZG93bkl0LnByb3RvdHlwZS5wYXJzZUlubGluZSA9IGZ1bmN0aW9uIChzcmMsIGVudikge1xuICB2YXIgc3RhdGUgPSBuZXcgdGhpcy5jb3JlLlN0YXRlKHNyYywgdGhpcywgZW52KTtcblxuICBzdGF0ZS5pbmxpbmVNb2RlID0gdHJ1ZTtcbiAgdGhpcy5jb3JlLnByb2Nlc3Moc3RhdGUpO1xuXG4gIHJldHVybiBzdGF0ZS50b2tlbnM7XG59O1xuXG5cbi8qKlxuICogTWFya2Rvd25JdC5yZW5kZXJJbmxpbmUoc3JjIFssIGVudl0pIC0+IFN0cmluZ1xuICogLSBzcmMgKFN0cmluZyk6IHNvdXJjZSBzdHJpbmdcbiAqIC0gZW52IChPYmplY3QpOiBlbnZpcm9ubWVudCBzYW5kYm94XG4gKlxuICogU2ltaWxhciB0byBbW01hcmtkb3duSXQucmVuZGVyXV0gYnV0IGZvciBzaW5nbGUgcGFyYWdyYXBoIGNvbnRlbnQuIFJlc3VsdFxuICogd2lsbCBOT1QgYmUgd3JhcHBlZCBpbnRvIGA8cD5gIHRhZ3MuXG4gKiovXG5NYXJrZG93bkl0LnByb3RvdHlwZS5yZW5kZXJJbmxpbmUgPSBmdW5jdGlvbiAoc3JjLCBlbnYpIHtcbiAgZW52ID0gZW52IHx8IHt9O1xuXG4gIHJldHVybiB0aGlzLnJlbmRlcmVyLnJlbmRlcih0aGlzLnBhcnNlSW5saW5lKHNyYywgZW52KSwgdGhpcy5vcHRpb25zLCBlbnYpO1xufTtcblxuXG5tb2R1bGUuZXhwb3J0cyA9IE1hcmtkb3duSXQ7XG4iLCIvKiogaW50ZXJuYWxcbiAqIGNsYXNzIFBhcnNlckJsb2NrXG4gKlxuICogQmxvY2stbGV2ZWwgdG9rZW5pemVyLlxuICoqL1xuJ3VzZSBzdHJpY3QnO1xuXG5cbnZhciBSdWxlciAgICAgICAgICAgPSByZXF1aXJlKCcuL3J1bGVyJyk7XG5cblxudmFyIF9ydWxlcyA9IFtcbiAgLy8gRmlyc3QgMiBwYXJhbXMgLSBydWxlIG5hbWUgJiBzb3VyY2UuIFNlY29uZGFyeSBhcnJheSAtIGxpc3Qgb2YgcnVsZXMsXG4gIC8vIHdoaWNoIGNhbiBiZSB0ZXJtaW5hdGVkIGJ5IHRoaXMgb25lLlxuICBbICd0YWJsZScsICAgICAgcmVxdWlyZSgnLi9ydWxlc19ibG9jay90YWJsZScpLCAgICAgIFsgJ3BhcmFncmFwaCcsICdyZWZlcmVuY2UnIF0gXSxcbiAgWyAnY29kZScsICAgICAgIHJlcXVpcmUoJy4vcnVsZXNfYmxvY2svY29kZScpIF0sXG4gIFsgJ2ZlbmNlJywgICAgICByZXF1aXJlKCcuL3J1bGVzX2Jsb2NrL2ZlbmNlJyksICAgICAgWyAncGFyYWdyYXBoJywgJ3JlZmVyZW5jZScsICdibG9ja3F1b3RlJywgJ2xpc3QnIF0gXSxcbiAgWyAnYmxvY2txdW90ZScsIHJlcXVpcmUoJy4vcnVsZXNfYmxvY2svYmxvY2txdW90ZScpLCBbICdwYXJhZ3JhcGgnLCAncmVmZXJlbmNlJywgJ2Jsb2NrcXVvdGUnLCAnbGlzdCcgXSBdLFxuICBbICdocicsICAgICAgICAgcmVxdWlyZSgnLi9ydWxlc19ibG9jay9ocicpLCAgICAgICAgIFsgJ3BhcmFncmFwaCcsICdyZWZlcmVuY2UnLCAnYmxvY2txdW90ZScsICdsaXN0JyBdIF0sXG4gIFsgJ2xpc3QnLCAgICAgICByZXF1aXJlKCcuL3J1bGVzX2Jsb2NrL2xpc3QnKSwgICAgICAgWyAncGFyYWdyYXBoJywgJ3JlZmVyZW5jZScsICdibG9ja3F1b3RlJyBdIF0sXG4gIFsgJ3JlZmVyZW5jZScsICByZXF1aXJlKCcuL3J1bGVzX2Jsb2NrL3JlZmVyZW5jZScpIF0sXG4gIFsgJ2hlYWRpbmcnLCAgICByZXF1aXJlKCcuL3J1bGVzX2Jsb2NrL2hlYWRpbmcnKSwgICAgWyAncGFyYWdyYXBoJywgJ3JlZmVyZW5jZScsICdibG9ja3F1b3RlJyBdIF0sXG4gIFsgJ2xoZWFkaW5nJywgICByZXF1aXJlKCcuL3J1bGVzX2Jsb2NrL2xoZWFkaW5nJykgXSxcbiAgWyAnaHRtbF9ibG9jaycsIHJlcXVpcmUoJy4vcnVsZXNfYmxvY2svaHRtbF9ibG9jaycpLCBbICdwYXJhZ3JhcGgnLCAncmVmZXJlbmNlJywgJ2Jsb2NrcXVvdGUnIF0gXSxcbiAgWyAncGFyYWdyYXBoJywgIHJlcXVpcmUoJy4vcnVsZXNfYmxvY2svcGFyYWdyYXBoJykgXVxuXTtcblxuXG4vKipcbiAqIG5ldyBQYXJzZXJCbG9jaygpXG4gKiovXG5mdW5jdGlvbiBQYXJzZXJCbG9jaygpIHtcbiAgLyoqXG4gICAqIFBhcnNlckJsb2NrI3J1bGVyIC0+IFJ1bGVyXG4gICAqXG4gICAqIFtbUnVsZXJdXSBpbnN0YW5jZS4gS2VlcCBjb25maWd1cmF0aW9uIG9mIGJsb2NrIHJ1bGVzLlxuICAgKiovXG4gIHRoaXMucnVsZXIgPSBuZXcgUnVsZXIoKTtcblxuICBmb3IgKHZhciBpID0gMDsgaSA8IF9ydWxlcy5sZW5ndGg7IGkrKykge1xuICAgIHRoaXMucnVsZXIucHVzaChfcnVsZXNbaV1bMF0sIF9ydWxlc1tpXVsxXSwgeyBhbHQ6IChfcnVsZXNbaV1bMl0gfHwgW10pLnNsaWNlKCkgfSk7XG4gIH1cbn1cblxuXG4vLyBHZW5lcmF0ZSB0b2tlbnMgZm9yIGlucHV0IHJhbmdlXG4vL1xuUGFyc2VyQmxvY2sucHJvdG90eXBlLnRva2VuaXplID0gZnVuY3Rpb24gKHN0YXRlLCBzdGFydExpbmUsIGVuZExpbmUpIHtcbiAgdmFyIG9rLCBpLFxuICAgICAgcnVsZXMgPSB0aGlzLnJ1bGVyLmdldFJ1bGVzKCcnKSxcbiAgICAgIGxlbiA9IHJ1bGVzLmxlbmd0aCxcbiAgICAgIGxpbmUgPSBzdGFydExpbmUsXG4gICAgICBoYXNFbXB0eUxpbmVzID0gZmFsc2UsXG4gICAgICBtYXhOZXN0aW5nID0gc3RhdGUubWQub3B0aW9ucy5tYXhOZXN0aW5nO1xuXG4gIHdoaWxlIChsaW5lIDwgZW5kTGluZSkge1xuICAgIHN0YXRlLmxpbmUgPSBsaW5lID0gc3RhdGUuc2tpcEVtcHR5TGluZXMobGluZSk7XG4gICAgaWYgKGxpbmUgPj0gZW5kTGluZSkgeyBicmVhazsgfVxuXG4gICAgLy8gVGVybWluYXRpb24gY29uZGl0aW9uIGZvciBuZXN0ZWQgY2FsbHMuXG4gICAgLy8gTmVzdGVkIGNhbGxzIGN1cnJlbnRseSB1c2VkIGZvciBibG9ja3F1b3RlcyAmIGxpc3RzXG4gICAgaWYgKHN0YXRlLnNDb3VudFtsaW5lXSA8IHN0YXRlLmJsa0luZGVudCkgeyBicmVhazsgfVxuXG4gICAgLy8gSWYgbmVzdGluZyBsZXZlbCBleGNlZWRlZCAtIHNraXAgdGFpbCB0byB0aGUgZW5kLiBUaGF0J3Mgbm90IG9yZGluYXJ5XG4gICAgLy8gc2l0dWF0aW9uIGFuZCB3ZSBzaG91bGQgbm90IGNhcmUgYWJvdXQgY29udGVudC5cbiAgICBpZiAoc3RhdGUubGV2ZWwgPj0gbWF4TmVzdGluZykge1xuICAgICAgc3RhdGUubGluZSA9IGVuZExpbmU7XG4gICAgICBicmVhaztcbiAgICB9XG5cbiAgICAvLyBUcnkgYWxsIHBvc3NpYmxlIHJ1bGVzLlxuICAgIC8vIE9uIHN1Y2Nlc3MsIHJ1bGUgc2hvdWxkOlxuICAgIC8vXG4gICAgLy8gLSB1cGRhdGUgYHN0YXRlLmxpbmVgXG4gICAgLy8gLSB1cGRhdGUgYHN0YXRlLnRva2Vuc2BcbiAgICAvLyAtIHJldHVybiB0cnVlXG5cbiAgICBmb3IgKGkgPSAwOyBpIDwgbGVuOyBpKyspIHtcbiAgICAgIG9rID0gcnVsZXNbaV0oc3RhdGUsIGxpbmUsIGVuZExpbmUsIGZhbHNlKTtcbiAgICAgIGlmIChvaykgeyBicmVhazsgfVxuICAgIH1cblxuICAgIC8vIHNldCBzdGF0ZS50aWdodCBpZiB3ZSBoYWQgYW4gZW1wdHkgbGluZSBiZWZvcmUgY3VycmVudCB0YWdcbiAgICAvLyBpLmUuIGxhdGVzdCBlbXB0eSBsaW5lIHNob3VsZCBub3QgY291bnRcbiAgICBzdGF0ZS50aWdodCA9ICFoYXNFbXB0eUxpbmVzO1xuXG4gICAgLy8gcGFyYWdyYXBoIG1pZ2h0IFwiZWF0XCIgb25lIG5ld2xpbmUgYWZ0ZXIgaXQgaW4gbmVzdGVkIGxpc3RzXG4gICAgaWYgKHN0YXRlLmlzRW1wdHkoc3RhdGUubGluZSAtIDEpKSB7XG4gICAgICBoYXNFbXB0eUxpbmVzID0gdHJ1ZTtcbiAgICB9XG5cbiAgICBsaW5lID0gc3RhdGUubGluZTtcblxuICAgIGlmIChsaW5lIDwgZW5kTGluZSAmJiBzdGF0ZS5pc0VtcHR5KGxpbmUpKSB7XG4gICAgICBoYXNFbXB0eUxpbmVzID0gdHJ1ZTtcbiAgICAgIGxpbmUrKztcbiAgICAgIHN0YXRlLmxpbmUgPSBsaW5lO1xuICAgIH1cbiAgfVxufTtcblxuXG4vKipcbiAqIFBhcnNlckJsb2NrLnBhcnNlKHN0ciwgbWQsIGVudiwgb3V0VG9rZW5zKVxuICpcbiAqIFByb2Nlc3MgaW5wdXQgc3RyaW5nIGFuZCBwdXNoIGJsb2NrIHRva2VucyBpbnRvIGBvdXRUb2tlbnNgXG4gKiovXG5QYXJzZXJCbG9jay5wcm90b3R5cGUucGFyc2UgPSBmdW5jdGlvbiAoc3JjLCBtZCwgZW52LCBvdXRUb2tlbnMpIHtcbiAgdmFyIHN0YXRlO1xuXG4gIGlmICghc3JjKSB7IHJldHVybjsgfVxuXG4gIHN0YXRlID0gbmV3IHRoaXMuU3RhdGUoc3JjLCBtZCwgZW52LCBvdXRUb2tlbnMpO1xuXG4gIHRoaXMudG9rZW5pemUoc3RhdGUsIHN0YXRlLmxpbmUsIHN0YXRlLmxpbmVNYXgpO1xufTtcblxuXG5QYXJzZXJCbG9jay5wcm90b3R5cGUuU3RhdGUgPSByZXF1aXJlKCcuL3J1bGVzX2Jsb2NrL3N0YXRlX2Jsb2NrJyk7XG5cblxubW9kdWxlLmV4cG9ydHMgPSBQYXJzZXJCbG9jaztcbiIsIi8qKiBpbnRlcm5hbFxuICogY2xhc3MgQ29yZVxuICpcbiAqIFRvcC1sZXZlbCBydWxlcyBleGVjdXRvci4gR2x1ZXMgYmxvY2svaW5saW5lIHBhcnNlcnMgYW5kIGRvZXMgaW50ZXJtZWRpYXRlXG4gKiB0cmFuc2Zvcm1hdGlvbnMuXG4gKiovXG4ndXNlIHN0cmljdCc7XG5cblxudmFyIFJ1bGVyICA9IHJlcXVpcmUoJy4vcnVsZXInKTtcblxuXG52YXIgX3J1bGVzID0gW1xuICBbICdub3JtYWxpemUnLCAgICAgIHJlcXVpcmUoJy4vcnVsZXNfY29yZS9ub3JtYWxpemUnKSAgICAgIF0sXG4gIFsgJ2Jsb2NrJywgICAgICAgICAgcmVxdWlyZSgnLi9ydWxlc19jb3JlL2Jsb2NrJykgICAgICAgICAgXSxcbiAgWyAnaW5saW5lJywgICAgICAgICByZXF1aXJlKCcuL3J1bGVzX2NvcmUvaW5saW5lJykgICAgICAgICBdLFxuICBbICdsaW5raWZ5JywgICAgICAgIHJlcXVpcmUoJy4vcnVsZXNfY29yZS9saW5raWZ5JykgICAgICAgIF0sXG4gIFsgJ3JlcGxhY2VtZW50cycsICAgcmVxdWlyZSgnLi9ydWxlc19jb3JlL3JlcGxhY2VtZW50cycpICAgXSxcbiAgWyAnc21hcnRxdW90ZXMnLCAgICByZXF1aXJlKCcuL3J1bGVzX2NvcmUvc21hcnRxdW90ZXMnKSAgICBdXG5dO1xuXG5cbi8qKlxuICogbmV3IENvcmUoKVxuICoqL1xuZnVuY3Rpb24gQ29yZSgpIHtcbiAgLyoqXG4gICAqIENvcmUjcnVsZXIgLT4gUnVsZXJcbiAgICpcbiAgICogW1tSdWxlcl1dIGluc3RhbmNlLiBLZWVwIGNvbmZpZ3VyYXRpb24gb2YgY29yZSBydWxlcy5cbiAgICoqL1xuICB0aGlzLnJ1bGVyID0gbmV3IFJ1bGVyKCk7XG5cbiAgZm9yICh2YXIgaSA9IDA7IGkgPCBfcnVsZXMubGVuZ3RoOyBpKyspIHtcbiAgICB0aGlzLnJ1bGVyLnB1c2goX3J1bGVzW2ldWzBdLCBfcnVsZXNbaV1bMV0pO1xuICB9XG59XG5cblxuLyoqXG4gKiBDb3JlLnByb2Nlc3Moc3RhdGUpXG4gKlxuICogRXhlY3V0ZXMgY29yZSBjaGFpbiBydWxlcy5cbiAqKi9cbkNvcmUucHJvdG90eXBlLnByb2Nlc3MgPSBmdW5jdGlvbiAoc3RhdGUpIHtcbiAgdmFyIGksIGwsIHJ1bGVzO1xuXG4gIHJ1bGVzID0gdGhpcy5ydWxlci5nZXRSdWxlcygnJyk7XG5cbiAgZm9yIChpID0gMCwgbCA9IHJ1bGVzLmxlbmd0aDsgaSA8IGw7IGkrKykge1xuICAgIHJ1bGVzW2ldKHN0YXRlKTtcbiAgfVxufTtcblxuQ29yZS5wcm90b3R5cGUuU3RhdGUgPSByZXF1aXJlKCcuL3J1bGVzX2NvcmUvc3RhdGVfY29yZScpO1xuXG5cbm1vZHVsZS5leHBvcnRzID0gQ29yZTtcbiIsIi8qKiBpbnRlcm5hbFxuICogY2xhc3MgUGFyc2VySW5saW5lXG4gKlxuICogVG9rZW5pemVzIHBhcmFncmFwaCBjb250ZW50LlxuICoqL1xuJ3VzZSBzdHJpY3QnO1xuXG5cbnZhciBSdWxlciAgICAgICAgICAgPSByZXF1aXJlKCcuL3J1bGVyJyk7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFBhcnNlciBydWxlc1xuXG52YXIgX3J1bGVzID0gW1xuICBbICd0ZXh0JywgICAgICAgICAgICByZXF1aXJlKCcuL3J1bGVzX2lubGluZS90ZXh0JykgXSxcbiAgWyAnbmV3bGluZScsICAgICAgICAgcmVxdWlyZSgnLi9ydWxlc19pbmxpbmUvbmV3bGluZScpIF0sXG4gIFsgJ2VzY2FwZScsICAgICAgICAgIHJlcXVpcmUoJy4vcnVsZXNfaW5saW5lL2VzY2FwZScpIF0sXG4gIFsgJ2JhY2t0aWNrcycsICAgICAgIHJlcXVpcmUoJy4vcnVsZXNfaW5saW5lL2JhY2t0aWNrcycpIF0sXG4gIFsgJ3N0cmlrZXRocm91Z2gnLCAgIHJlcXVpcmUoJy4vcnVsZXNfaW5saW5lL3N0cmlrZXRocm91Z2gnKS50b2tlbml6ZSBdLFxuICBbICdlbXBoYXNpcycsICAgICAgICByZXF1aXJlKCcuL3J1bGVzX2lubGluZS9lbXBoYXNpcycpLnRva2VuaXplIF0sXG4gIFsgJ2xpbmsnLCAgICAgICAgICAgIHJlcXVpcmUoJy4vcnVsZXNfaW5saW5lL2xpbmsnKSBdLFxuICBbICdpbWFnZScsICAgICAgICAgICByZXF1aXJlKCcuL3J1bGVzX2lubGluZS9pbWFnZScpIF0sXG4gIFsgJ2F1dG9saW5rJywgICAgICAgIHJlcXVpcmUoJy4vcnVsZXNfaW5saW5lL2F1dG9saW5rJykgXSxcbiAgWyAnaHRtbF9pbmxpbmUnLCAgICAgcmVxdWlyZSgnLi9ydWxlc19pbmxpbmUvaHRtbF9pbmxpbmUnKSBdLFxuICBbICdlbnRpdHknLCAgICAgICAgICByZXF1aXJlKCcuL3J1bGVzX2lubGluZS9lbnRpdHknKSBdXG5dO1xuXG52YXIgX3J1bGVzMiA9IFtcbiAgWyAnYmFsYW5jZV9wYWlycycsICAgcmVxdWlyZSgnLi9ydWxlc19pbmxpbmUvYmFsYW5jZV9wYWlycycpIF0sXG4gIFsgJ3N0cmlrZXRocm91Z2gnLCAgIHJlcXVpcmUoJy4vcnVsZXNfaW5saW5lL3N0cmlrZXRocm91Z2gnKS5wb3N0UHJvY2VzcyBdLFxuICBbICdlbXBoYXNpcycsICAgICAgICByZXF1aXJlKCcuL3J1bGVzX2lubGluZS9lbXBoYXNpcycpLnBvc3RQcm9jZXNzIF0sXG4gIFsgJ3RleHRfY29sbGFwc2UnLCAgIHJlcXVpcmUoJy4vcnVsZXNfaW5saW5lL3RleHRfY29sbGFwc2UnKSBdXG5dO1xuXG5cbi8qKlxuICogbmV3IFBhcnNlcklubGluZSgpXG4gKiovXG5mdW5jdGlvbiBQYXJzZXJJbmxpbmUoKSB7XG4gIHZhciBpO1xuXG4gIC8qKlxuICAgKiBQYXJzZXJJbmxpbmUjcnVsZXIgLT4gUnVsZXJcbiAgICpcbiAgICogW1tSdWxlcl1dIGluc3RhbmNlLiBLZWVwIGNvbmZpZ3VyYXRpb24gb2YgaW5saW5lIHJ1bGVzLlxuICAgKiovXG4gIHRoaXMucnVsZXIgPSBuZXcgUnVsZXIoKTtcblxuICBmb3IgKGkgPSAwOyBpIDwgX3J1bGVzLmxlbmd0aDsgaSsrKSB7XG4gICAgdGhpcy5ydWxlci5wdXNoKF9ydWxlc1tpXVswXSwgX3J1bGVzW2ldWzFdKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBQYXJzZXJJbmxpbmUjcnVsZXIyIC0+IFJ1bGVyXG4gICAqXG4gICAqIFtbUnVsZXJdXSBpbnN0YW5jZS4gU2Vjb25kIHJ1bGVyIHVzZWQgZm9yIHBvc3QtcHJvY2Vzc2luZ1xuICAgKiAoZS5nLiBpbiBlbXBoYXNpcy1saWtlIHJ1bGVzKS5cbiAgICoqL1xuICB0aGlzLnJ1bGVyMiA9IG5ldyBSdWxlcigpO1xuXG4gIGZvciAoaSA9IDA7IGkgPCBfcnVsZXMyLmxlbmd0aDsgaSsrKSB7XG4gICAgdGhpcy5ydWxlcjIucHVzaChfcnVsZXMyW2ldWzBdLCBfcnVsZXMyW2ldWzFdKTtcbiAgfVxufVxuXG5cbi8vIFNraXAgc2luZ2xlIHRva2VuIGJ5IHJ1bm5pbmcgYWxsIHJ1bGVzIGluIHZhbGlkYXRpb24gbW9kZTtcbi8vIHJldHVybnMgYHRydWVgIGlmIGFueSBydWxlIHJlcG9ydGVkIHN1Y2Nlc3Ncbi8vXG5QYXJzZXJJbmxpbmUucHJvdG90eXBlLnNraXBUb2tlbiA9IGZ1bmN0aW9uIChzdGF0ZSkge1xuICB2YXIgb2ssIGksIHBvcyA9IHN0YXRlLnBvcyxcbiAgICAgIHJ1bGVzID0gdGhpcy5ydWxlci5nZXRSdWxlcygnJyksXG4gICAgICBsZW4gPSBydWxlcy5sZW5ndGgsXG4gICAgICBtYXhOZXN0aW5nID0gc3RhdGUubWQub3B0aW9ucy5tYXhOZXN0aW5nLFxuICAgICAgY2FjaGUgPSBzdGF0ZS5jYWNoZTtcblxuXG4gIGlmICh0eXBlb2YgY2FjaGVbcG9zXSAhPT0gJ3VuZGVmaW5lZCcpIHtcbiAgICBzdGF0ZS5wb3MgPSBjYWNoZVtwb3NdO1xuICAgIHJldHVybjtcbiAgfVxuXG4gIGlmIChzdGF0ZS5sZXZlbCA8IG1heE5lc3RpbmcpIHtcbiAgICBmb3IgKGkgPSAwOyBpIDwgbGVuOyBpKyspIHtcbiAgICAgIC8vIEluY3JlbWVudCBzdGF0ZS5sZXZlbCBhbmQgZGVjcmVtZW50IGl0IGxhdGVyIHRvIGxpbWl0IHJlY3Vyc2lvbi5cbiAgICAgIC8vIEl0J3MgaGFybWxlc3MgdG8gZG8gaGVyZSwgYmVjYXVzZSBubyB0b2tlbnMgYXJlIGNyZWF0ZWQuIEJ1dCBpZGVhbGx5LFxuICAgICAgLy8gd2UnZCBuZWVkIGEgc2VwYXJhdGUgcHJpdmF0ZSBzdGF0ZSB2YXJpYWJsZSBmb3IgdGhpcyBwdXJwb3NlLlxuICAgICAgLy9cbiAgICAgIHN0YXRlLmxldmVsKys7XG4gICAgICBvayA9IHJ1bGVzW2ldKHN0YXRlLCB0cnVlKTtcbiAgICAgIHN0YXRlLmxldmVsLS07XG5cbiAgICAgIGlmIChvaykgeyBicmVhazsgfVxuICAgIH1cbiAgfSBlbHNlIHtcbiAgICAvLyBUb28gbXVjaCBuZXN0aW5nLCBqdXN0IHNraXAgdW50aWwgdGhlIGVuZCBvZiB0aGUgcGFyYWdyYXBoLlxuICAgIC8vXG4gICAgLy8gTk9URTogdGhpcyB3aWxsIGNhdXNlIGxpbmtzIHRvIGJlaGF2ZSBpbmNvcnJlY3RseSBpbiB0aGUgZm9sbG93aW5nIGNhc2UsXG4gICAgLy8gICAgICAgd2hlbiBhbiBhbW91bnQgb2YgYFtgIGlzIGV4YWN0bHkgZXF1YWwgdG8gYG1heE5lc3RpbmcgKyAxYDpcbiAgICAvL1xuICAgIC8vICAgICAgIFtbW1tbW1tbW1tbW1tbW1tbW1tbW2Zvb10oKVxuICAgIC8vXG4gICAgLy8gVE9ETzogcmVtb3ZlIHRoaXMgd29ya2Fyb3VuZCB3aGVuIENNIHN0YW5kYXJkIHdpbGwgYWxsb3cgbmVzdGVkIGxpbmtzXG4gICAgLy8gICAgICAgKHdlIGNhbiByZXBsYWNlIGl0IGJ5IHByZXZlbnRpbmcgbGlua3MgZnJvbSBiZWluZyBwYXJzZWQgaW5cbiAgICAvLyAgICAgICB2YWxpZGF0aW9uIG1vZGUpXG4gICAgLy9cbiAgICBzdGF0ZS5wb3MgPSBzdGF0ZS5wb3NNYXg7XG4gIH1cblxuICBpZiAoIW9rKSB7IHN0YXRlLnBvcysrOyB9XG4gIGNhY2hlW3Bvc10gPSBzdGF0ZS5wb3M7XG59O1xuXG5cbi8vIEdlbmVyYXRlIHRva2VucyBmb3IgaW5wdXQgcmFuZ2Vcbi8vXG5QYXJzZXJJbmxpbmUucHJvdG90eXBlLnRva2VuaXplID0gZnVuY3Rpb24gKHN0YXRlKSB7XG4gIHZhciBvaywgaSxcbiAgICAgIHJ1bGVzID0gdGhpcy5ydWxlci5nZXRSdWxlcygnJyksXG4gICAgICBsZW4gPSBydWxlcy5sZW5ndGgsXG4gICAgICBlbmQgPSBzdGF0ZS5wb3NNYXgsXG4gICAgICBtYXhOZXN0aW5nID0gc3RhdGUubWQub3B0aW9ucy5tYXhOZXN0aW5nO1xuXG4gIHdoaWxlIChzdGF0ZS5wb3MgPCBlbmQpIHtcbiAgICAvLyBUcnkgYWxsIHBvc3NpYmxlIHJ1bGVzLlxuICAgIC8vIE9uIHN1Y2Nlc3MsIHJ1bGUgc2hvdWxkOlxuICAgIC8vXG4gICAgLy8gLSB1cGRhdGUgYHN0YXRlLnBvc2BcbiAgICAvLyAtIHVwZGF0ZSBgc3RhdGUudG9rZW5zYFxuICAgIC8vIC0gcmV0dXJuIHRydWVcblxuICAgIGlmIChzdGF0ZS5sZXZlbCA8IG1heE5lc3RpbmcpIHtcbiAgICAgIGZvciAoaSA9IDA7IGkgPCBsZW47IGkrKykge1xuICAgICAgICBvayA9IHJ1bGVzW2ldKHN0YXRlLCBmYWxzZSk7XG4gICAgICAgIGlmIChvaykgeyBicmVhazsgfVxuICAgICAgfVxuICAgIH1cblxuICAgIGlmIChvaykge1xuICAgICAgaWYgKHN0YXRlLnBvcyA+PSBlbmQpIHsgYnJlYWs7IH1cbiAgICAgIGNvbnRpbnVlO1xuICAgIH1cblxuICAgIHN0YXRlLnBlbmRpbmcgKz0gc3RhdGUuc3JjW3N0YXRlLnBvcysrXTtcbiAgfVxuXG4gIGlmIChzdGF0ZS5wZW5kaW5nKSB7XG4gICAgc3RhdGUucHVzaFBlbmRpbmcoKTtcbiAgfVxufTtcblxuXG4vKipcbiAqIFBhcnNlcklubGluZS5wYXJzZShzdHIsIG1kLCBlbnYsIG91dFRva2VucylcbiAqXG4gKiBQcm9jZXNzIGlucHV0IHN0cmluZyBhbmQgcHVzaCBpbmxpbmUgdG9rZW5zIGludG8gYG91dFRva2Vuc2BcbiAqKi9cblBhcnNlcklubGluZS5wcm90b3R5cGUucGFyc2UgPSBmdW5jdGlvbiAoc3RyLCBtZCwgZW52LCBvdXRUb2tlbnMpIHtcbiAgdmFyIGksIHJ1bGVzLCBsZW47XG4gIHZhciBzdGF0ZSA9IG5ldyB0aGlzLlN0YXRlKHN0ciwgbWQsIGVudiwgb3V0VG9rZW5zKTtcblxuICB0aGlzLnRva2VuaXplKHN0YXRlKTtcblxuICBydWxlcyA9IHRoaXMucnVsZXIyLmdldFJ1bGVzKCcnKTtcbiAgbGVuID0gcnVsZXMubGVuZ3RoO1xuXG4gIGZvciAoaSA9IDA7IGkgPCBsZW47IGkrKykge1xuICAgIHJ1bGVzW2ldKHN0YXRlKTtcbiAgfVxufTtcblxuXG5QYXJzZXJJbmxpbmUucHJvdG90eXBlLlN0YXRlID0gcmVxdWlyZSgnLi9ydWxlc19pbmxpbmUvc3RhdGVfaW5saW5lJyk7XG5cblxubW9kdWxlLmV4cG9ydHMgPSBQYXJzZXJJbmxpbmU7XG4iLCIvLyBDb21tb25tYXJrIGRlZmF1bHQgb3B0aW9uc1xuXG4ndXNlIHN0cmljdCc7XG5cblxubW9kdWxlLmV4cG9ydHMgPSB7XG4gIG9wdGlvbnM6IHtcbiAgICBodG1sOiAgICAgICAgIHRydWUsICAgICAgICAgLy8gRW5hYmxlIEhUTUwgdGFncyBpbiBzb3VyY2VcbiAgICB4aHRtbE91dDogICAgIHRydWUsICAgICAgICAgLy8gVXNlICcvJyB0byBjbG9zZSBzaW5nbGUgdGFncyAoPGJyIC8+KVxuICAgIGJyZWFrczogICAgICAgZmFsc2UsICAgICAgICAvLyBDb252ZXJ0ICdcXG4nIGluIHBhcmFncmFwaHMgaW50byA8YnI+XG4gICAgbGFuZ1ByZWZpeDogICAnbGFuZ3VhZ2UtJywgIC8vIENTUyBsYW5ndWFnZSBwcmVmaXggZm9yIGZlbmNlZCBibG9ja3NcbiAgICBsaW5raWZ5OiAgICAgIGZhbHNlLCAgICAgICAgLy8gYXV0b2NvbnZlcnQgVVJMLWxpa2UgdGV4dHMgdG8gbGlua3NcblxuICAgIC8vIEVuYWJsZSBzb21lIGxhbmd1YWdlLW5ldXRyYWwgcmVwbGFjZW1lbnRzICsgcXVvdGVzIGJlYXV0aWZpY2F0aW9uXG4gICAgdHlwb2dyYXBoZXI6ICBmYWxzZSxcblxuICAgIC8vIERvdWJsZSArIHNpbmdsZSBxdW90ZXMgcmVwbGFjZW1lbnQgcGFpcnMsIHdoZW4gdHlwb2dyYXBoZXIgZW5hYmxlZCxcbiAgICAvLyBhbmQgc21hcnRxdW90ZXMgb24uIENvdWxkIGJlIGVpdGhlciBhIFN0cmluZyBvciBhbiBBcnJheS5cbiAgICAvL1xuICAgIC8vIEZvciBleGFtcGxlLCB5b3UgY2FuIHVzZSAnwqvCu+KAnuKAnCcgZm9yIFJ1c3NpYW4sICfigJ7igJzigJrigJgnIGZvciBHZXJtYW4sXG4gICAgLy8gYW5kIFsnwqtcXHhBMCcsICdcXHhBMMK7JywgJ+KAuVxceEEwJywgJ1xceEEw4oC6J10gZm9yIEZyZW5jaCAoaW5jbHVkaW5nIG5ic3ApLlxuICAgIHF1b3RlczogJ1xcdTIwMWNcXHUyMDFkXFx1MjAxOFxcdTIwMTknLCAvKiDigJzigJ3igJjigJkgKi9cblxuICAgIC8vIEhpZ2hsaWdodGVyIGZ1bmN0aW9uLiBTaG91bGQgcmV0dXJuIGVzY2FwZWQgSFRNTCxcbiAgICAvLyBvciAnJyBpZiB0aGUgc291cmNlIHN0cmluZyBpcyBub3QgY2hhbmdlZCBhbmQgc2hvdWxkIGJlIGVzY2FwZWQgZXh0ZXJuYWx5LlxuICAgIC8vIElmIHJlc3VsdCBzdGFydHMgd2l0aCA8cHJlLi4uIGludGVybmFsIHdyYXBwZXIgaXMgc2tpcHBlZC5cbiAgICAvL1xuICAgIC8vIGZ1bmN0aW9uICgvKnN0ciwgbGFuZyovKSB7IHJldHVybiAnJzsgfVxuICAgIC8vXG4gICAgaGlnaGxpZ2h0OiBudWxsLFxuXG4gICAgbWF4TmVzdGluZzogICAyMCAgICAgICAgICAgIC8vIEludGVybmFsIHByb3RlY3Rpb24sIHJlY3Vyc2lvbiBsaW1pdFxuICB9LFxuXG4gIGNvbXBvbmVudHM6IHtcblxuICAgIGNvcmU6IHtcbiAgICAgIHJ1bGVzOiBbXG4gICAgICAgICdub3JtYWxpemUnLFxuICAgICAgICAnYmxvY2snLFxuICAgICAgICAnaW5saW5lJ1xuICAgICAgXVxuICAgIH0sXG5cbiAgICBibG9jazoge1xuICAgICAgcnVsZXM6IFtcbiAgICAgICAgJ2Jsb2NrcXVvdGUnLFxuICAgICAgICAnY29kZScsXG4gICAgICAgICdmZW5jZScsXG4gICAgICAgICdoZWFkaW5nJyxcbiAgICAgICAgJ2hyJyxcbiAgICAgICAgJ2h0bWxfYmxvY2snLFxuICAgICAgICAnbGhlYWRpbmcnLFxuICAgICAgICAnbGlzdCcsXG4gICAgICAgICdyZWZlcmVuY2UnLFxuICAgICAgICAncGFyYWdyYXBoJ1xuICAgICAgXVxuICAgIH0sXG5cbiAgICBpbmxpbmU6IHtcbiAgICAgIHJ1bGVzOiBbXG4gICAgICAgICdhdXRvbGluaycsXG4gICAgICAgICdiYWNrdGlja3MnLFxuICAgICAgICAnZW1waGFzaXMnLFxuICAgICAgICAnZW50aXR5JyxcbiAgICAgICAgJ2VzY2FwZScsXG4gICAgICAgICdodG1sX2lubGluZScsXG4gICAgICAgICdpbWFnZScsXG4gICAgICAgICdsaW5rJyxcbiAgICAgICAgJ25ld2xpbmUnLFxuICAgICAgICAndGV4dCdcbiAgICAgIF0sXG4gICAgICBydWxlczI6IFtcbiAgICAgICAgJ2JhbGFuY2VfcGFpcnMnLFxuICAgICAgICAnZW1waGFzaXMnLFxuICAgICAgICAndGV4dF9jb2xsYXBzZSdcbiAgICAgIF1cbiAgICB9XG4gIH1cbn07XG4iLCIvLyBtYXJrZG93bi1pdCBkZWZhdWx0IG9wdGlvbnNcblxuJ3VzZSBzdHJpY3QnO1xuXG5cbm1vZHVsZS5leHBvcnRzID0ge1xuICBvcHRpb25zOiB7XG4gICAgaHRtbDogICAgICAgICBmYWxzZSwgICAgICAgIC8vIEVuYWJsZSBIVE1MIHRhZ3MgaW4gc291cmNlXG4gICAgeGh0bWxPdXQ6ICAgICBmYWxzZSwgICAgICAgIC8vIFVzZSAnLycgdG8gY2xvc2Ugc2luZ2xlIHRhZ3MgKDxiciAvPilcbiAgICBicmVha3M6ICAgICAgIGZhbHNlLCAgICAgICAgLy8gQ29udmVydCAnXFxuJyBpbiBwYXJhZ3JhcGhzIGludG8gPGJyPlxuICAgIGxhbmdQcmVmaXg6ICAgJ2xhbmd1YWdlLScsICAvLyBDU1MgbGFuZ3VhZ2UgcHJlZml4IGZvciBmZW5jZWQgYmxvY2tzXG4gICAgbGlua2lmeTogICAgICBmYWxzZSwgICAgICAgIC8vIGF1dG9jb252ZXJ0IFVSTC1saWtlIHRleHRzIHRvIGxpbmtzXG5cbiAgICAvLyBFbmFibGUgc29tZSBsYW5ndWFnZS1uZXV0cmFsIHJlcGxhY2VtZW50cyArIHF1b3RlcyBiZWF1dGlmaWNhdGlvblxuICAgIHR5cG9ncmFwaGVyOiAgZmFsc2UsXG5cbiAgICAvLyBEb3VibGUgKyBzaW5nbGUgcXVvdGVzIHJlcGxhY2VtZW50IHBhaXJzLCB3aGVuIHR5cG9ncmFwaGVyIGVuYWJsZWQsXG4gICAgLy8gYW5kIHNtYXJ0cXVvdGVzIG9uLiBDb3VsZCBiZSBlaXRoZXIgYSBTdHJpbmcgb3IgYW4gQXJyYXkuXG4gICAgLy9cbiAgICAvLyBGb3IgZXhhbXBsZSwgeW91IGNhbiB1c2UgJ8KrwrvigJ7igJwnIGZvciBSdXNzaWFuLCAn4oCe4oCc4oCa4oCYJyBmb3IgR2VybWFuLFxuICAgIC8vIGFuZCBbJ8KrXFx4QTAnLCAnXFx4QTDCuycsICfigLlcXHhBMCcsICdcXHhBMOKAuiddIGZvciBGcmVuY2ggKGluY2x1ZGluZyBuYnNwKS5cbiAgICBxdW90ZXM6ICdcXHUyMDFjXFx1MjAxZFxcdTIwMThcXHUyMDE5JywgLyog4oCc4oCd4oCY4oCZICovXG5cbiAgICAvLyBIaWdobGlnaHRlciBmdW5jdGlvbi4gU2hvdWxkIHJldHVybiBlc2NhcGVkIEhUTUwsXG4gICAgLy8gb3IgJycgaWYgdGhlIHNvdXJjZSBzdHJpbmcgaXMgbm90IGNoYW5nZWQgYW5kIHNob3VsZCBiZSBlc2NhcGVkIGV4dGVybmFseS5cbiAgICAvLyBJZiByZXN1bHQgc3RhcnRzIHdpdGggPHByZS4uLiBpbnRlcm5hbCB3cmFwcGVyIGlzIHNraXBwZWQuXG4gICAgLy9cbiAgICAvLyBmdW5jdGlvbiAoLypzdHIsIGxhbmcqLykgeyByZXR1cm4gJyc7IH1cbiAgICAvL1xuICAgIGhpZ2hsaWdodDogbnVsbCxcblxuICAgIG1heE5lc3Rpbmc6ICAgMTAwICAgICAgICAgICAgLy8gSW50ZXJuYWwgcHJvdGVjdGlvbiwgcmVjdXJzaW9uIGxpbWl0XG4gIH0sXG5cbiAgY29tcG9uZW50czoge1xuXG4gICAgY29yZToge30sXG4gICAgYmxvY2s6IHt9LFxuICAgIGlubGluZToge31cbiAgfVxufTtcbiIsIi8vIFwiWmVyb1wiIHByZXNldCwgd2l0aCBub3RoaW5nIGVuYWJsZWQuIFVzZWZ1bCBmb3IgbWFudWFsIGNvbmZpZ3VyaW5nIG9mIHNpbXBsZVxuLy8gbW9kZXMuIEZvciBleGFtcGxlLCB0byBwYXJzZSBib2xkL2l0YWxpYyBvbmx5LlxuXG4ndXNlIHN0cmljdCc7XG5cblxubW9kdWxlLmV4cG9ydHMgPSB7XG4gIG9wdGlvbnM6IHtcbiAgICBodG1sOiAgICAgICAgIGZhbHNlLCAgICAgICAgLy8gRW5hYmxlIEhUTUwgdGFncyBpbiBzb3VyY2VcbiAgICB4aHRtbE91dDogICAgIGZhbHNlLCAgICAgICAgLy8gVXNlICcvJyB0byBjbG9zZSBzaW5nbGUgdGFncyAoPGJyIC8+KVxuICAgIGJyZWFrczogICAgICAgZmFsc2UsICAgICAgICAvLyBDb252ZXJ0ICdcXG4nIGluIHBhcmFncmFwaHMgaW50byA8YnI+XG4gICAgbGFuZ1ByZWZpeDogICAnbGFuZ3VhZ2UtJywgIC8vIENTUyBsYW5ndWFnZSBwcmVmaXggZm9yIGZlbmNlZCBibG9ja3NcbiAgICBsaW5raWZ5OiAgICAgIGZhbHNlLCAgICAgICAgLy8gYXV0b2NvbnZlcnQgVVJMLWxpa2UgdGV4dHMgdG8gbGlua3NcblxuICAgIC8vIEVuYWJsZSBzb21lIGxhbmd1YWdlLW5ldXRyYWwgcmVwbGFjZW1lbnRzICsgcXVvdGVzIGJlYXV0aWZpY2F0aW9uXG4gICAgdHlwb2dyYXBoZXI6ICBmYWxzZSxcblxuICAgIC8vIERvdWJsZSArIHNpbmdsZSBxdW90ZXMgcmVwbGFjZW1lbnQgcGFpcnMsIHdoZW4gdHlwb2dyYXBoZXIgZW5hYmxlZCxcbiAgICAvLyBhbmQgc21hcnRxdW90ZXMgb24uIENvdWxkIGJlIGVpdGhlciBhIFN0cmluZyBvciBhbiBBcnJheS5cbiAgICAvL1xuICAgIC8vIEZvciBleGFtcGxlLCB5b3UgY2FuIHVzZSAnwqvCu+KAnuKAnCcgZm9yIFJ1c3NpYW4sICfigJ7igJzigJrigJgnIGZvciBHZXJtYW4sXG4gICAgLy8gYW5kIFsnwqtcXHhBMCcsICdcXHhBMMK7JywgJ+KAuVxceEEwJywgJ1xceEEw4oC6J10gZm9yIEZyZW5jaCAoaW5jbHVkaW5nIG5ic3ApLlxuICAgIHF1b3RlczogJ1xcdTIwMWNcXHUyMDFkXFx1MjAxOFxcdTIwMTknLCAvKiDigJzigJ3igJjigJkgKi9cblxuICAgIC8vIEhpZ2hsaWdodGVyIGZ1bmN0aW9uLiBTaG91bGQgcmV0dXJuIGVzY2FwZWQgSFRNTCxcbiAgICAvLyBvciAnJyBpZiB0aGUgc291cmNlIHN0cmluZyBpcyBub3QgY2hhbmdlZCBhbmQgc2hvdWxkIGJlIGVzY2FwZWQgZXh0ZXJuYWx5LlxuICAgIC8vIElmIHJlc3VsdCBzdGFydHMgd2l0aCA8cHJlLi4uIGludGVybmFsIHdyYXBwZXIgaXMgc2tpcHBlZC5cbiAgICAvL1xuICAgIC8vIGZ1bmN0aW9uICgvKnN0ciwgbGFuZyovKSB7IHJldHVybiAnJzsgfVxuICAgIC8vXG4gICAgaGlnaGxpZ2h0OiBudWxsLFxuXG4gICAgbWF4TmVzdGluZzogICAyMCAgICAgICAgICAgIC8vIEludGVybmFsIHByb3RlY3Rpb24sIHJlY3Vyc2lvbiBsaW1pdFxuICB9LFxuXG4gIGNvbXBvbmVudHM6IHtcblxuICAgIGNvcmU6IHtcbiAgICAgIHJ1bGVzOiBbXG4gICAgICAgICdub3JtYWxpemUnLFxuICAgICAgICAnYmxvY2snLFxuICAgICAgICAnaW5saW5lJ1xuICAgICAgXVxuICAgIH0sXG5cbiAgICBibG9jazoge1xuICAgICAgcnVsZXM6IFtcbiAgICAgICAgJ3BhcmFncmFwaCdcbiAgICAgIF1cbiAgICB9LFxuXG4gICAgaW5saW5lOiB7XG4gICAgICBydWxlczogW1xuICAgICAgICAndGV4dCdcbiAgICAgIF0sXG4gICAgICBydWxlczI6IFtcbiAgICAgICAgJ2JhbGFuY2VfcGFpcnMnLFxuICAgICAgICAndGV4dF9jb2xsYXBzZSdcbiAgICAgIF1cbiAgICB9XG4gIH1cbn07XG4iLCIvKipcbiAqIGNsYXNzIFJlbmRlcmVyXG4gKlxuICogR2VuZXJhdGVzIEhUTUwgZnJvbSBwYXJzZWQgdG9rZW4gc3RyZWFtLiBFYWNoIGluc3RhbmNlIGhhcyBpbmRlcGVuZGVudFxuICogY29weSBvZiBydWxlcy4gVGhvc2UgY2FuIGJlIHJld3JpdHRlbiB3aXRoIGVhc2UuIEFsc28sIHlvdSBjYW4gYWRkIG5ld1xuICogcnVsZXMgaWYgeW91IGNyZWF0ZSBwbHVnaW4gYW5kIGFkZHMgbmV3IHRva2VuIHR5cGVzLlxuICoqL1xuJ3VzZSBzdHJpY3QnO1xuXG5cbnZhciBhc3NpZ24gICAgICAgICAgPSByZXF1aXJlKCcuL2NvbW1vbi91dGlscycpLmFzc2lnbjtcbnZhciB1bmVzY2FwZUFsbCAgICAgPSByZXF1aXJlKCcuL2NvbW1vbi91dGlscycpLnVuZXNjYXBlQWxsO1xudmFyIGVzY2FwZUh0bWwgICAgICA9IHJlcXVpcmUoJy4vY29tbW9uL3V0aWxzJykuZXNjYXBlSHRtbDtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vL1xuXG52YXIgZGVmYXVsdF9ydWxlcyA9IHt9O1xuXG5cbmRlZmF1bHRfcnVsZXMuY29kZV9pbmxpbmUgPSBmdW5jdGlvbiAodG9rZW5zLCBpZHgsIG9wdGlvbnMsIGVudiwgc2xmKSB7XG4gIHZhciB0b2tlbiA9IHRva2Vuc1tpZHhdO1xuXG4gIHJldHVybiAgJzxjb2RlJyArIHNsZi5yZW5kZXJBdHRycyh0b2tlbikgKyAnPicgK1xuICAgICAgICAgIGVzY2FwZUh0bWwodG9rZW5zW2lkeF0uY29udGVudCkgK1xuICAgICAgICAgICc8L2NvZGU+Jztcbn07XG5cblxuZGVmYXVsdF9ydWxlcy5jb2RlX2Jsb2NrID0gZnVuY3Rpb24gKHRva2VucywgaWR4LCBvcHRpb25zLCBlbnYsIHNsZikge1xuICB2YXIgdG9rZW4gPSB0b2tlbnNbaWR4XTtcblxuICByZXR1cm4gICc8cHJlJyArIHNsZi5yZW5kZXJBdHRycyh0b2tlbikgKyAnPjxjb2RlPicgK1xuICAgICAgICAgIGVzY2FwZUh0bWwodG9rZW5zW2lkeF0uY29udGVudCkgK1xuICAgICAgICAgICc8L2NvZGU+PC9wcmU+XFxuJztcbn07XG5cblxuZGVmYXVsdF9ydWxlcy5mZW5jZSA9IGZ1bmN0aW9uICh0b2tlbnMsIGlkeCwgb3B0aW9ucywgZW52LCBzbGYpIHtcbiAgdmFyIHRva2VuID0gdG9rZW5zW2lkeF0sXG4gICAgICBpbmZvID0gdG9rZW4uaW5mbyA/IHVuZXNjYXBlQWxsKHRva2VuLmluZm8pLnRyaW0oKSA6ICcnLFxuICAgICAgbGFuZ05hbWUgPSAnJyxcbiAgICAgIGhpZ2hsaWdodGVkLCBpLCB0bXBBdHRycywgdG1wVG9rZW47XG5cbiAgaWYgKGluZm8pIHtcbiAgICBsYW5nTmFtZSA9IGluZm8uc3BsaXQoL1xccysvZylbMF07XG4gIH1cblxuICBpZiAob3B0aW9ucy5oaWdobGlnaHQpIHtcbiAgICBoaWdobGlnaHRlZCA9IG9wdGlvbnMuaGlnaGxpZ2h0KHRva2VuLmNvbnRlbnQsIGxhbmdOYW1lKSB8fCBlc2NhcGVIdG1sKHRva2VuLmNvbnRlbnQpO1xuICB9IGVsc2Uge1xuICAgIGhpZ2hsaWdodGVkID0gZXNjYXBlSHRtbCh0b2tlbi5jb250ZW50KTtcbiAgfVxuXG4gIGlmIChoaWdobGlnaHRlZC5pbmRleE9mKCc8cHJlJykgPT09IDApIHtcbiAgICByZXR1cm4gaGlnaGxpZ2h0ZWQgKyAnXFxuJztcbiAgfVxuXG4gIC8vIElmIGxhbmd1YWdlIGV4aXN0cywgaW5qZWN0IGNsYXNzIGdlbnRseSwgd2l0aG91dCBtb2RpZnlpbmcgb3JpZ2luYWwgdG9rZW4uXG4gIC8vIE1heSBiZSwgb25lIGRheSB3ZSB3aWxsIGFkZCAuY2xvbmUoKSBmb3IgdG9rZW4gYW5kIHNpbXBsaWZ5IHRoaXMgcGFydCwgYnV0XG4gIC8vIG5vdyB3ZSBwcmVmZXIgdG8ga2VlcCB0aGluZ3MgbG9jYWwuXG4gIGlmIChpbmZvKSB7XG4gICAgaSAgICAgICAgPSB0b2tlbi5hdHRySW5kZXgoJ2NsYXNzJyk7XG4gICAgdG1wQXR0cnMgPSB0b2tlbi5hdHRycyA/IHRva2VuLmF0dHJzLnNsaWNlKCkgOiBbXTtcblxuICAgIGlmIChpIDwgMCkge1xuICAgICAgdG1wQXR0cnMucHVzaChbICdjbGFzcycsIG9wdGlvbnMubGFuZ1ByZWZpeCArIGxhbmdOYW1lIF0pO1xuICAgIH0gZWxzZSB7XG4gICAgICB0bXBBdHRyc1tpXVsxXSArPSAnICcgKyBvcHRpb25zLmxhbmdQcmVmaXggKyBsYW5nTmFtZTtcbiAgICB9XG5cbiAgICAvLyBGYWtlIHRva2VuIGp1c3QgdG8gcmVuZGVyIGF0dHJpYnV0ZXNcbiAgICB0bXBUb2tlbiA9IHtcbiAgICAgIGF0dHJzOiB0bXBBdHRyc1xuICAgIH07XG5cbiAgICByZXR1cm4gICc8cHJlPjxjb2RlJyArIHNsZi5yZW5kZXJBdHRycyh0bXBUb2tlbikgKyAnPidcbiAgICAgICAgICArIGhpZ2hsaWdodGVkXG4gICAgICAgICAgKyAnPC9jb2RlPjwvcHJlPlxcbic7XG4gIH1cblxuXG4gIHJldHVybiAgJzxwcmU+PGNvZGUnICsgc2xmLnJlbmRlckF0dHJzKHRva2VuKSArICc+J1xuICAgICAgICArIGhpZ2hsaWdodGVkXG4gICAgICAgICsgJzwvY29kZT48L3ByZT5cXG4nO1xufTtcblxuXG5kZWZhdWx0X3J1bGVzLmltYWdlID0gZnVuY3Rpb24gKHRva2VucywgaWR4LCBvcHRpb25zLCBlbnYsIHNsZikge1xuICB2YXIgdG9rZW4gPSB0b2tlbnNbaWR4XTtcblxuICAvLyBcImFsdFwiIGF0dHIgTVVTVCBiZSBzZXQsIGV2ZW4gaWYgZW1wdHkuIEJlY2F1c2UgaXQncyBtYW5kYXRvcnkgYW5kXG4gIC8vIHNob3VsZCBiZSBwbGFjZWQgb24gcHJvcGVyIHBvc2l0aW9uIGZvciB0ZXN0cy5cbiAgLy9cbiAgLy8gUmVwbGFjZSBjb250ZW50IHdpdGggYWN0dWFsIHZhbHVlXG5cbiAgdG9rZW4uYXR0cnNbdG9rZW4uYXR0ckluZGV4KCdhbHQnKV1bMV0gPVxuICAgIHNsZi5yZW5kZXJJbmxpbmVBc1RleHQodG9rZW4uY2hpbGRyZW4sIG9wdGlvbnMsIGVudik7XG5cbiAgcmV0dXJuIHNsZi5yZW5kZXJUb2tlbih0b2tlbnMsIGlkeCwgb3B0aW9ucyk7XG59O1xuXG5cbmRlZmF1bHRfcnVsZXMuaGFyZGJyZWFrID0gZnVuY3Rpb24gKHRva2VucywgaWR4LCBvcHRpb25zIC8qLCBlbnYgKi8pIHtcbiAgcmV0dXJuIG9wdGlvbnMueGh0bWxPdXQgPyAnPGJyIC8+XFxuJyA6ICc8YnI+XFxuJztcbn07XG5kZWZhdWx0X3J1bGVzLnNvZnRicmVhayA9IGZ1bmN0aW9uICh0b2tlbnMsIGlkeCwgb3B0aW9ucyAvKiwgZW52ICovKSB7XG4gIHJldHVybiBvcHRpb25zLmJyZWFrcyA/IChvcHRpb25zLnhodG1sT3V0ID8gJzxiciAvPlxcbicgOiAnPGJyPlxcbicpIDogJ1xcbic7XG59O1xuXG5cbmRlZmF1bHRfcnVsZXMudGV4dCA9IGZ1bmN0aW9uICh0b2tlbnMsIGlkeCAvKiwgb3B0aW9ucywgZW52ICovKSB7XG4gIHJldHVybiBlc2NhcGVIdG1sKHRva2Vuc1tpZHhdLmNvbnRlbnQpO1xufTtcblxuXG5kZWZhdWx0X3J1bGVzLmh0bWxfYmxvY2sgPSBmdW5jdGlvbiAodG9rZW5zLCBpZHggLyosIG9wdGlvbnMsIGVudiAqLykge1xuICByZXR1cm4gdG9rZW5zW2lkeF0uY29udGVudDtcbn07XG5kZWZhdWx0X3J1bGVzLmh0bWxfaW5saW5lID0gZnVuY3Rpb24gKHRva2VucywgaWR4IC8qLCBvcHRpb25zLCBlbnYgKi8pIHtcbiAgcmV0dXJuIHRva2Vuc1tpZHhdLmNvbnRlbnQ7XG59O1xuXG5cbi8qKlxuICogbmV3IFJlbmRlcmVyKClcbiAqXG4gKiBDcmVhdGVzIG5ldyBbW1JlbmRlcmVyXV0gaW5zdGFuY2UgYW5kIGZpbGwgW1tSZW5kZXJlciNydWxlc11dIHdpdGggZGVmYXVsdHMuXG4gKiovXG5mdW5jdGlvbiBSZW5kZXJlcigpIHtcblxuICAvKipcbiAgICogUmVuZGVyZXIjcnVsZXMgLT4gT2JqZWN0XG4gICAqXG4gICAqIENvbnRhaW5zIHJlbmRlciBydWxlcyBmb3IgdG9rZW5zLiBDYW4gYmUgdXBkYXRlZCBhbmQgZXh0ZW5kZWQuXG4gICAqXG4gICAqICMjIyMjIEV4YW1wbGVcbiAgICpcbiAgICogYGBgamF2YXNjcmlwdFxuICAgKiB2YXIgbWQgPSByZXF1aXJlKCdtYXJrZG93bi1pdCcpKCk7XG4gICAqXG4gICAqIG1kLnJlbmRlcmVyLnJ1bGVzLnN0cm9uZ19vcGVuICA9IGZ1bmN0aW9uICgpIHsgcmV0dXJuICc8Yj4nOyB9O1xuICAgKiBtZC5yZW5kZXJlci5ydWxlcy5zdHJvbmdfY2xvc2UgPSBmdW5jdGlvbiAoKSB7IHJldHVybiAnPC9iPic7IH07XG4gICAqXG4gICAqIHZhciByZXN1bHQgPSBtZC5yZW5kZXJJbmxpbmUoLi4uKTtcbiAgICogYGBgXG4gICAqXG4gICAqIEVhY2ggcnVsZSBpcyBjYWxsZWQgYXMgaW5kZXBlbmRlbnQgc3RhdGljIGZ1bmN0aW9uIHdpdGggZml4ZWQgc2lnbmF0dXJlOlxuICAgKlxuICAgKiBgYGBqYXZhc2NyaXB0XG4gICAqIGZ1bmN0aW9uIG15X3Rva2VuX3JlbmRlcih0b2tlbnMsIGlkeCwgb3B0aW9ucywgZW52LCByZW5kZXJlcikge1xuICAgKiAgIC8vIC4uLlxuICAgKiAgIHJldHVybiByZW5kZXJlZEhUTUw7XG4gICAqIH1cbiAgICogYGBgXG4gICAqXG4gICAqIFNlZSBbc291cmNlIGNvZGVdKGh0dHBzOi8vZ2l0aHViLmNvbS9tYXJrZG93bi1pdC9tYXJrZG93bi1pdC9ibG9iL21hc3Rlci9saWIvcmVuZGVyZXIuanMpXG4gICAqIGZvciBtb3JlIGRldGFpbHMgYW5kIGV4YW1wbGVzLlxuICAgKiovXG4gIHRoaXMucnVsZXMgPSBhc3NpZ24oe30sIGRlZmF1bHRfcnVsZXMpO1xufVxuXG5cbi8qKlxuICogUmVuZGVyZXIucmVuZGVyQXR0cnModG9rZW4pIC0+IFN0cmluZ1xuICpcbiAqIFJlbmRlciB0b2tlbiBhdHRyaWJ1dGVzIHRvIHN0cmluZy5cbiAqKi9cblJlbmRlcmVyLnByb3RvdHlwZS5yZW5kZXJBdHRycyA9IGZ1bmN0aW9uIHJlbmRlckF0dHJzKHRva2VuKSB7XG4gIHZhciBpLCBsLCByZXN1bHQ7XG5cbiAgaWYgKCF0b2tlbi5hdHRycykgeyByZXR1cm4gJyc7IH1cblxuICByZXN1bHQgPSAnJztcblxuICBmb3IgKGkgPSAwLCBsID0gdG9rZW4uYXR0cnMubGVuZ3RoOyBpIDwgbDsgaSsrKSB7XG4gICAgcmVzdWx0ICs9ICcgJyArIGVzY2FwZUh0bWwodG9rZW4uYXR0cnNbaV1bMF0pICsgJz1cIicgKyBlc2NhcGVIdG1sKHRva2VuLmF0dHJzW2ldWzFdKSArICdcIic7XG4gIH1cblxuICByZXR1cm4gcmVzdWx0O1xufTtcblxuXG4vKipcbiAqIFJlbmRlcmVyLnJlbmRlclRva2VuKHRva2VucywgaWR4LCBvcHRpb25zKSAtPiBTdHJpbmdcbiAqIC0gdG9rZW5zIChBcnJheSk6IGxpc3Qgb2YgdG9rZW5zXG4gKiAtIGlkeCAoTnVtYmVkKTogdG9rZW4gaW5kZXggdG8gcmVuZGVyXG4gKiAtIG9wdGlvbnMgKE9iamVjdCk6IHBhcmFtcyBvZiBwYXJzZXIgaW5zdGFuY2VcbiAqXG4gKiBEZWZhdWx0IHRva2VuIHJlbmRlcmVyLiBDYW4gYmUgb3ZlcnJpZGVuIGJ5IGN1c3RvbSBmdW5jdGlvblxuICogaW4gW1tSZW5kZXJlciNydWxlc11dLlxuICoqL1xuUmVuZGVyZXIucHJvdG90eXBlLnJlbmRlclRva2VuID0gZnVuY3Rpb24gcmVuZGVyVG9rZW4odG9rZW5zLCBpZHgsIG9wdGlvbnMpIHtcbiAgdmFyIG5leHRUb2tlbixcbiAgICAgIHJlc3VsdCA9ICcnLFxuICAgICAgbmVlZExmID0gZmFsc2UsXG4gICAgICB0b2tlbiA9IHRva2Vuc1tpZHhdO1xuXG4gIC8vIFRpZ2h0IGxpc3QgcGFyYWdyYXBoc1xuICBpZiAodG9rZW4uaGlkZGVuKSB7XG4gICAgcmV0dXJuICcnO1xuICB9XG5cbiAgLy8gSW5zZXJ0IGEgbmV3bGluZSBiZXR3ZWVuIGhpZGRlbiBwYXJhZ3JhcGggYW5kIHN1YnNlcXVlbnQgb3BlbmluZ1xuICAvLyBibG9jay1sZXZlbCB0YWcuXG4gIC8vXG4gIC8vIEZvciBleGFtcGxlLCBoZXJlIHdlIHNob3VsZCBpbnNlcnQgYSBuZXdsaW5lIGJlZm9yZSBibG9ja3F1b3RlOlxuICAvLyAgLSBhXG4gIC8vICAgID5cbiAgLy9cbiAgaWYgKHRva2VuLmJsb2NrICYmIHRva2VuLm5lc3RpbmcgIT09IC0xICYmIGlkeCAmJiB0b2tlbnNbaWR4IC0gMV0uaGlkZGVuKSB7XG4gICAgcmVzdWx0ICs9ICdcXG4nO1xuICB9XG5cbiAgLy8gQWRkIHRva2VuIG5hbWUsIGUuZy4gYDxpbWdgXG4gIHJlc3VsdCArPSAodG9rZW4ubmVzdGluZyA9PT0gLTEgPyAnPC8nIDogJzwnKSArIHRva2VuLnRhZztcblxuICAvLyBFbmNvZGUgYXR0cmlidXRlcywgZS5nLiBgPGltZyBzcmM9XCJmb29cImBcbiAgcmVzdWx0ICs9IHRoaXMucmVuZGVyQXR0cnModG9rZW4pO1xuXG4gIC8vIEFkZCBhIHNsYXNoIGZvciBzZWxmLWNsb3NpbmcgdGFncywgZS5nLiBgPGltZyBzcmM9XCJmb29cIiAvYFxuICBpZiAodG9rZW4ubmVzdGluZyA9PT0gMCAmJiBvcHRpb25zLnhodG1sT3V0KSB7XG4gICAgcmVzdWx0ICs9ICcgLyc7XG4gIH1cblxuICAvLyBDaGVjayBpZiB3ZSBuZWVkIHRvIGFkZCBhIG5ld2xpbmUgYWZ0ZXIgdGhpcyB0YWdcbiAgaWYgKHRva2VuLmJsb2NrKSB7XG4gICAgbmVlZExmID0gdHJ1ZTtcblxuICAgIGlmICh0b2tlbi5uZXN0aW5nID09PSAxKSB7XG4gICAgICBpZiAoaWR4ICsgMSA8IHRva2Vucy5sZW5ndGgpIHtcbiAgICAgICAgbmV4dFRva2VuID0gdG9rZW5zW2lkeCArIDFdO1xuXG4gICAgICAgIGlmIChuZXh0VG9rZW4udHlwZSA9PT0gJ2lubGluZScgfHwgbmV4dFRva2VuLmhpZGRlbikge1xuICAgICAgICAgIC8vIEJsb2NrLWxldmVsIHRhZyBjb250YWluaW5nIGFuIGlubGluZSB0YWcuXG4gICAgICAgICAgLy9cbiAgICAgICAgICBuZWVkTGYgPSBmYWxzZTtcblxuICAgICAgICB9IGVsc2UgaWYgKG5leHRUb2tlbi5uZXN0aW5nID09PSAtMSAmJiBuZXh0VG9rZW4udGFnID09PSB0b2tlbi50YWcpIHtcbiAgICAgICAgICAvLyBPcGVuaW5nIHRhZyArIGNsb3NpbmcgdGFnIG9mIHRoZSBzYW1lIHR5cGUuIEUuZy4gYDxsaT48L2xpPmAuXG4gICAgICAgICAgLy9cbiAgICAgICAgICBuZWVkTGYgPSBmYWxzZTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIHJlc3VsdCArPSBuZWVkTGYgPyAnPlxcbicgOiAnPic7XG5cbiAgcmV0dXJuIHJlc3VsdDtcbn07XG5cblxuLyoqXG4gKiBSZW5kZXJlci5yZW5kZXJJbmxpbmUodG9rZW5zLCBvcHRpb25zLCBlbnYpIC0+IFN0cmluZ1xuICogLSB0b2tlbnMgKEFycmF5KTogbGlzdCBvbiBibG9jayB0b2tlbnMgdG8gcmVudGVyXG4gKiAtIG9wdGlvbnMgKE9iamVjdCk6IHBhcmFtcyBvZiBwYXJzZXIgaW5zdGFuY2VcbiAqIC0gZW52IChPYmplY3QpOiBhZGRpdGlvbmFsIGRhdGEgZnJvbSBwYXJzZWQgaW5wdXQgKHJlZmVyZW5jZXMsIGZvciBleGFtcGxlKVxuICpcbiAqIFRoZSBzYW1lIGFzIFtbUmVuZGVyZXIucmVuZGVyXV0sIGJ1dCBmb3Igc2luZ2xlIHRva2VuIG9mIGBpbmxpbmVgIHR5cGUuXG4gKiovXG5SZW5kZXJlci5wcm90b3R5cGUucmVuZGVySW5saW5lID0gZnVuY3Rpb24gKHRva2Vucywgb3B0aW9ucywgZW52KSB7XG4gIHZhciB0eXBlLFxuICAgICAgcmVzdWx0ID0gJycsXG4gICAgICBydWxlcyA9IHRoaXMucnVsZXM7XG5cbiAgZm9yICh2YXIgaSA9IDAsIGxlbiA9IHRva2Vucy5sZW5ndGg7IGkgPCBsZW47IGkrKykge1xuICAgIHR5cGUgPSB0b2tlbnNbaV0udHlwZTtcblxuICAgIGlmICh0eXBlb2YgcnVsZXNbdHlwZV0gIT09ICd1bmRlZmluZWQnKSB7XG4gICAgICByZXN1bHQgKz0gcnVsZXNbdHlwZV0odG9rZW5zLCBpLCBvcHRpb25zLCBlbnYsIHRoaXMpO1xuICAgIH0gZWxzZSB7XG4gICAgICByZXN1bHQgKz0gdGhpcy5yZW5kZXJUb2tlbih0b2tlbnMsIGksIG9wdGlvbnMpO1xuICAgIH1cbiAgfVxuXG4gIHJldHVybiByZXN1bHQ7XG59O1xuXG5cbi8qKiBpbnRlcm5hbFxuICogUmVuZGVyZXIucmVuZGVySW5saW5lQXNUZXh0KHRva2Vucywgb3B0aW9ucywgZW52KSAtPiBTdHJpbmdcbiAqIC0gdG9rZW5zIChBcnJheSk6IGxpc3Qgb24gYmxvY2sgdG9rZW5zIHRvIHJlbnRlclxuICogLSBvcHRpb25zIChPYmplY3QpOiBwYXJhbXMgb2YgcGFyc2VyIGluc3RhbmNlXG4gKiAtIGVudiAoT2JqZWN0KTogYWRkaXRpb25hbCBkYXRhIGZyb20gcGFyc2VkIGlucHV0IChyZWZlcmVuY2VzLCBmb3IgZXhhbXBsZSlcbiAqXG4gKiBTcGVjaWFsIGtsdWRnZSBmb3IgaW1hZ2UgYGFsdGAgYXR0cmlidXRlcyB0byBjb25mb3JtIENvbW1vbk1hcmsgc3BlYy5cbiAqIERvbid0IHRyeSB0byB1c2UgaXQhIFNwZWMgcmVxdWlyZXMgdG8gc2hvdyBgYWx0YCBjb250ZW50IHdpdGggc3RyaXBwZWQgbWFya3VwLFxuICogaW5zdGVhZCBvZiBzaW1wbGUgZXNjYXBpbmcuXG4gKiovXG5SZW5kZXJlci5wcm90b3R5cGUucmVuZGVySW5saW5lQXNUZXh0ID0gZnVuY3Rpb24gKHRva2Vucywgb3B0aW9ucywgZW52KSB7XG4gIHZhciByZXN1bHQgPSAnJztcblxuICBmb3IgKHZhciBpID0gMCwgbGVuID0gdG9rZW5zLmxlbmd0aDsgaSA8IGxlbjsgaSsrKSB7XG4gICAgaWYgKHRva2Vuc1tpXS50eXBlID09PSAndGV4dCcpIHtcbiAgICAgIHJlc3VsdCArPSB0b2tlbnNbaV0uY29udGVudDtcbiAgICB9IGVsc2UgaWYgKHRva2Vuc1tpXS50eXBlID09PSAnaW1hZ2UnKSB7XG4gICAgICByZXN1bHQgKz0gdGhpcy5yZW5kZXJJbmxpbmVBc1RleHQodG9rZW5zW2ldLmNoaWxkcmVuLCBvcHRpb25zLCBlbnYpO1xuICAgIH1cbiAgfVxuXG4gIHJldHVybiByZXN1bHQ7XG59O1xuXG5cbi8qKlxuICogUmVuZGVyZXIucmVuZGVyKHRva2Vucywgb3B0aW9ucywgZW52KSAtPiBTdHJpbmdcbiAqIC0gdG9rZW5zIChBcnJheSk6IGxpc3Qgb24gYmxvY2sgdG9rZW5zIHRvIHJlbnRlclxuICogLSBvcHRpb25zIChPYmplY3QpOiBwYXJhbXMgb2YgcGFyc2VyIGluc3RhbmNlXG4gKiAtIGVudiAoT2JqZWN0KTogYWRkaXRpb25hbCBkYXRhIGZyb20gcGFyc2VkIGlucHV0IChyZWZlcmVuY2VzLCBmb3IgZXhhbXBsZSlcbiAqXG4gKiBUYWtlcyB0b2tlbiBzdHJlYW0gYW5kIGdlbmVyYXRlcyBIVE1MLiBQcm9iYWJseSwgeW91IHdpbGwgbmV2ZXIgbmVlZCB0byBjYWxsXG4gKiB0aGlzIG1ldGhvZCBkaXJlY3RseS5cbiAqKi9cblJlbmRlcmVyLnByb3RvdHlwZS5yZW5kZXIgPSBmdW5jdGlvbiAodG9rZW5zLCBvcHRpb25zLCBlbnYpIHtcbiAgdmFyIGksIGxlbiwgdHlwZSxcbiAgICAgIHJlc3VsdCA9ICcnLFxuICAgICAgcnVsZXMgPSB0aGlzLnJ1bGVzO1xuXG4gIGZvciAoaSA9IDAsIGxlbiA9IHRva2Vucy5sZW5ndGg7IGkgPCBsZW47IGkrKykge1xuICAgIHR5cGUgPSB0b2tlbnNbaV0udHlwZTtcblxuICAgIGlmICh0eXBlID09PSAnaW5saW5lJykge1xuICAgICAgcmVzdWx0ICs9IHRoaXMucmVuZGVySW5saW5lKHRva2Vuc1tpXS5jaGlsZHJlbiwgb3B0aW9ucywgZW52KTtcbiAgICB9IGVsc2UgaWYgKHR5cGVvZiBydWxlc1t0eXBlXSAhPT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgIHJlc3VsdCArPSBydWxlc1t0b2tlbnNbaV0udHlwZV0odG9rZW5zLCBpLCBvcHRpb25zLCBlbnYsIHRoaXMpO1xuICAgIH0gZWxzZSB7XG4gICAgICByZXN1bHQgKz0gdGhpcy5yZW5kZXJUb2tlbih0b2tlbnMsIGksIG9wdGlvbnMsIGVudik7XG4gICAgfVxuICB9XG5cbiAgcmV0dXJuIHJlc3VsdDtcbn07XG5cbm1vZHVsZS5leHBvcnRzID0gUmVuZGVyZXI7XG4iLCIvKipcbiAqIGNsYXNzIFJ1bGVyXG4gKlxuICogSGVscGVyIGNsYXNzLCB1c2VkIGJ5IFtbTWFya2Rvd25JdCNjb3JlXV0sIFtbTWFya2Rvd25JdCNibG9ja11dIGFuZFxuICogW1tNYXJrZG93bkl0I2lubGluZV1dIHRvIG1hbmFnZSBzZXF1ZW5jZXMgb2YgZnVuY3Rpb25zIChydWxlcyk6XG4gKlxuICogLSBrZWVwIHJ1bGVzIGluIGRlZmluZWQgb3JkZXJcbiAqIC0gYXNzaWduIHRoZSBuYW1lIHRvIGVhY2ggcnVsZVxuICogLSBlbmFibGUvZGlzYWJsZSBydWxlc1xuICogLSBhZGQvcmVwbGFjZSBydWxlc1xuICogLSBhbGxvdyBhc3NpZ24gcnVsZXMgdG8gYWRkaXRpb25hbCBuYW1lZCBjaGFpbnMgKGluIHRoZSBzYW1lKVxuICogLSBjYWNoZWluZyBsaXN0cyBvZiBhY3RpdmUgcnVsZXNcbiAqXG4gKiBZb3Ugd2lsbCBub3QgbmVlZCB1c2UgdGhpcyBjbGFzcyBkaXJlY3RseSB1bnRpbCB3cml0ZSBwbHVnaW5zLiBGb3Igc2ltcGxlXG4gKiBydWxlcyBjb250cm9sIHVzZSBbW01hcmtkb3duSXQuZGlzYWJsZV1dLCBbW01hcmtkb3duSXQuZW5hYmxlXV0gYW5kXG4gKiBbW01hcmtkb3duSXQudXNlXV0uXG4gKiovXG4ndXNlIHN0cmljdCc7XG5cblxuLyoqXG4gKiBuZXcgUnVsZXIoKVxuICoqL1xuZnVuY3Rpb24gUnVsZXIoKSB7XG4gIC8vIExpc3Qgb2YgYWRkZWQgcnVsZXMuIEVhY2ggZWxlbWVudCBpczpcbiAgLy9cbiAgLy8ge1xuICAvLyAgIG5hbWU6IFhYWCxcbiAgLy8gICBlbmFibGVkOiBCb29sZWFuLFxuICAvLyAgIGZuOiBGdW5jdGlvbigpLFxuICAvLyAgIGFsdDogWyBuYW1lMiwgbmFtZTMgXVxuICAvLyB9XG4gIC8vXG4gIHRoaXMuX19ydWxlc19fID0gW107XG5cbiAgLy8gQ2FjaGVkIHJ1bGUgY2hhaW5zLlxuICAvL1xuICAvLyBGaXJzdCBsZXZlbCAtIGNoYWluIG5hbWUsICcnIGZvciBkZWZhdWx0LlxuICAvLyBTZWNvbmQgbGV2ZWwgLSBkaWdpbmFsIGFuY2hvciBmb3IgZmFzdCBmaWx0ZXJpbmcgYnkgY2hhcmNvZGVzLlxuICAvL1xuICB0aGlzLl9fY2FjaGVfXyA9IG51bGw7XG59XG5cbi8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBIZWxwZXIgbWV0aG9kcywgc2hvdWxkIG5vdCBiZSB1c2VkIGRpcmVjdGx5XG5cblxuLy8gRmluZCBydWxlIGluZGV4IGJ5IG5hbWVcbi8vXG5SdWxlci5wcm90b3R5cGUuX19maW5kX18gPSBmdW5jdGlvbiAobmFtZSkge1xuICBmb3IgKHZhciBpID0gMDsgaSA8IHRoaXMuX19ydWxlc19fLmxlbmd0aDsgaSsrKSB7XG4gICAgaWYgKHRoaXMuX19ydWxlc19fW2ldLm5hbWUgPT09IG5hbWUpIHtcbiAgICAgIHJldHVybiBpO1xuICAgIH1cbiAgfVxuICByZXR1cm4gLTE7XG59O1xuXG5cbi8vIEJ1aWxkIHJ1bGVzIGxvb2t1cCBjYWNoZVxuLy9cblJ1bGVyLnByb3RvdHlwZS5fX2NvbXBpbGVfXyA9IGZ1bmN0aW9uICgpIHtcbiAgdmFyIHNlbGYgPSB0aGlzO1xuICB2YXIgY2hhaW5zID0gWyAnJyBdO1xuXG4gIC8vIGNvbGxlY3QgdW5pcXVlIG5hbWVzXG4gIHNlbGYuX19ydWxlc19fLmZvckVhY2goZnVuY3Rpb24gKHJ1bGUpIHtcbiAgICBpZiAoIXJ1bGUuZW5hYmxlZCkgeyByZXR1cm47IH1cblxuICAgIHJ1bGUuYWx0LmZvckVhY2goZnVuY3Rpb24gKGFsdE5hbWUpIHtcbiAgICAgIGlmIChjaGFpbnMuaW5kZXhPZihhbHROYW1lKSA8IDApIHtcbiAgICAgICAgY2hhaW5zLnB1c2goYWx0TmFtZSk7XG4gICAgICB9XG4gICAgfSk7XG4gIH0pO1xuXG4gIHNlbGYuX19jYWNoZV9fID0ge307XG5cbiAgY2hhaW5zLmZvckVhY2goZnVuY3Rpb24gKGNoYWluKSB7XG4gICAgc2VsZi5fX2NhY2hlX19bY2hhaW5dID0gW107XG4gICAgc2VsZi5fX3J1bGVzX18uZm9yRWFjaChmdW5jdGlvbiAocnVsZSkge1xuICAgICAgaWYgKCFydWxlLmVuYWJsZWQpIHsgcmV0dXJuOyB9XG5cbiAgICAgIGlmIChjaGFpbiAmJiBydWxlLmFsdC5pbmRleE9mKGNoYWluKSA8IDApIHsgcmV0dXJuOyB9XG5cbiAgICAgIHNlbGYuX19jYWNoZV9fW2NoYWluXS5wdXNoKHJ1bGUuZm4pO1xuICAgIH0pO1xuICB9KTtcbn07XG5cblxuLyoqXG4gKiBSdWxlci5hdChuYW1lLCBmbiBbLCBvcHRpb25zXSlcbiAqIC0gbmFtZSAoU3RyaW5nKTogcnVsZSBuYW1lIHRvIHJlcGxhY2UuXG4gKiAtIGZuIChGdW5jdGlvbik6IG5ldyBydWxlIGZ1bmN0aW9uLlxuICogLSBvcHRpb25zIChPYmplY3QpOiBuZXcgcnVsZSBvcHRpb25zIChub3QgbWFuZGF0b3J5KS5cbiAqXG4gKiBSZXBsYWNlIHJ1bGUgYnkgbmFtZSB3aXRoIG5ldyBmdW5jdGlvbiAmIG9wdGlvbnMuIFRocm93cyBlcnJvciBpZiBuYW1lIG5vdFxuICogZm91bmQuXG4gKlxuICogIyMjIyMgT3B0aW9uczpcbiAqXG4gKiAtIF9fYWx0X18gLSBhcnJheSB3aXRoIG5hbWVzIG9mIFwiYWx0ZXJuYXRlXCIgY2hhaW5zLlxuICpcbiAqICMjIyMjIEV4YW1wbGVcbiAqXG4gKiBSZXBsYWNlIGV4aXN0aW5nIHR5cG9ncmFwaGVyIHJlcGxhY2VtZW50IHJ1bGUgd2l0aCBuZXcgb25lOlxuICpcbiAqIGBgYGphdmFzY3JpcHRcbiAqIHZhciBtZCA9IHJlcXVpcmUoJ21hcmtkb3duLWl0JykoKTtcbiAqXG4gKiBtZC5jb3JlLnJ1bGVyLmF0KCdyZXBsYWNlbWVudHMnLCBmdW5jdGlvbiByZXBsYWNlKHN0YXRlKSB7XG4gKiAgIC8vLi4uXG4gKiB9KTtcbiAqIGBgYFxuICoqL1xuUnVsZXIucHJvdG90eXBlLmF0ID0gZnVuY3Rpb24gKG5hbWUsIGZuLCBvcHRpb25zKSB7XG4gIHZhciBpbmRleCA9IHRoaXMuX19maW5kX18obmFtZSk7XG4gIHZhciBvcHQgPSBvcHRpb25zIHx8IHt9O1xuXG4gIGlmIChpbmRleCA9PT0gLTEpIHsgdGhyb3cgbmV3IEVycm9yKCdQYXJzZXIgcnVsZSBub3QgZm91bmQ6ICcgKyBuYW1lKTsgfVxuXG4gIHRoaXMuX19ydWxlc19fW2luZGV4XS5mbiA9IGZuO1xuICB0aGlzLl9fcnVsZXNfX1tpbmRleF0uYWx0ID0gb3B0LmFsdCB8fCBbXTtcbiAgdGhpcy5fX2NhY2hlX18gPSBudWxsO1xufTtcblxuXG4vKipcbiAqIFJ1bGVyLmJlZm9yZShiZWZvcmVOYW1lLCBydWxlTmFtZSwgZm4gWywgb3B0aW9uc10pXG4gKiAtIGJlZm9yZU5hbWUgKFN0cmluZyk6IG5ldyBydWxlIHdpbGwgYmUgYWRkZWQgYmVmb3JlIHRoaXMgb25lLlxuICogLSBydWxlTmFtZSAoU3RyaW5nKTogbmFtZSBvZiBhZGRlZCBydWxlLlxuICogLSBmbiAoRnVuY3Rpb24pOiBydWxlIGZ1bmN0aW9uLlxuICogLSBvcHRpb25zIChPYmplY3QpOiBydWxlIG9wdGlvbnMgKG5vdCBtYW5kYXRvcnkpLlxuICpcbiAqIEFkZCBuZXcgcnVsZSB0byBjaGFpbiBiZWZvcmUgb25lIHdpdGggZ2l2ZW4gbmFtZS4gU2VlIGFsc29cbiAqIFtbUnVsZXIuYWZ0ZXJdXSwgW1tSdWxlci5wdXNoXV0uXG4gKlxuICogIyMjIyMgT3B0aW9uczpcbiAqXG4gKiAtIF9fYWx0X18gLSBhcnJheSB3aXRoIG5hbWVzIG9mIFwiYWx0ZXJuYXRlXCIgY2hhaW5zLlxuICpcbiAqICMjIyMjIEV4YW1wbGVcbiAqXG4gKiBgYGBqYXZhc2NyaXB0XG4gKiB2YXIgbWQgPSByZXF1aXJlKCdtYXJrZG93bi1pdCcpKCk7XG4gKlxuICogbWQuYmxvY2sucnVsZXIuYmVmb3JlKCdwYXJhZ3JhcGgnLCAnbXlfcnVsZScsIGZ1bmN0aW9uIHJlcGxhY2Uoc3RhdGUpIHtcbiAqICAgLy8uLi5cbiAqIH0pO1xuICogYGBgXG4gKiovXG5SdWxlci5wcm90b3R5cGUuYmVmb3JlID0gZnVuY3Rpb24gKGJlZm9yZU5hbWUsIHJ1bGVOYW1lLCBmbiwgb3B0aW9ucykge1xuICB2YXIgaW5kZXggPSB0aGlzLl9fZmluZF9fKGJlZm9yZU5hbWUpO1xuICB2YXIgb3B0ID0gb3B0aW9ucyB8fCB7fTtcblxuICBpZiAoaW5kZXggPT09IC0xKSB7IHRocm93IG5ldyBFcnJvcignUGFyc2VyIHJ1bGUgbm90IGZvdW5kOiAnICsgYmVmb3JlTmFtZSk7IH1cblxuICB0aGlzLl9fcnVsZXNfXy5zcGxpY2UoaW5kZXgsIDAsIHtcbiAgICBuYW1lOiBydWxlTmFtZSxcbiAgICBlbmFibGVkOiB0cnVlLFxuICAgIGZuOiBmbixcbiAgICBhbHQ6IG9wdC5hbHQgfHwgW11cbiAgfSk7XG5cbiAgdGhpcy5fX2NhY2hlX18gPSBudWxsO1xufTtcblxuXG4vKipcbiAqIFJ1bGVyLmFmdGVyKGFmdGVyTmFtZSwgcnVsZU5hbWUsIGZuIFssIG9wdGlvbnNdKVxuICogLSBhZnRlck5hbWUgKFN0cmluZyk6IG5ldyBydWxlIHdpbGwgYmUgYWRkZWQgYWZ0ZXIgdGhpcyBvbmUuXG4gKiAtIHJ1bGVOYW1lIChTdHJpbmcpOiBuYW1lIG9mIGFkZGVkIHJ1bGUuXG4gKiAtIGZuIChGdW5jdGlvbik6IHJ1bGUgZnVuY3Rpb24uXG4gKiAtIG9wdGlvbnMgKE9iamVjdCk6IHJ1bGUgb3B0aW9ucyAobm90IG1hbmRhdG9yeSkuXG4gKlxuICogQWRkIG5ldyBydWxlIHRvIGNoYWluIGFmdGVyIG9uZSB3aXRoIGdpdmVuIG5hbWUuIFNlZSBhbHNvXG4gKiBbW1J1bGVyLmJlZm9yZV1dLCBbW1J1bGVyLnB1c2hdXS5cbiAqXG4gKiAjIyMjIyBPcHRpb25zOlxuICpcbiAqIC0gX19hbHRfXyAtIGFycmF5IHdpdGggbmFtZXMgb2YgXCJhbHRlcm5hdGVcIiBjaGFpbnMuXG4gKlxuICogIyMjIyMgRXhhbXBsZVxuICpcbiAqIGBgYGphdmFzY3JpcHRcbiAqIHZhciBtZCA9IHJlcXVpcmUoJ21hcmtkb3duLWl0JykoKTtcbiAqXG4gKiBtZC5pbmxpbmUucnVsZXIuYWZ0ZXIoJ3RleHQnLCAnbXlfcnVsZScsIGZ1bmN0aW9uIHJlcGxhY2Uoc3RhdGUpIHtcbiAqICAgLy8uLi5cbiAqIH0pO1xuICogYGBgXG4gKiovXG5SdWxlci5wcm90b3R5cGUuYWZ0ZXIgPSBmdW5jdGlvbiAoYWZ0ZXJOYW1lLCBydWxlTmFtZSwgZm4sIG9wdGlvbnMpIHtcbiAgdmFyIGluZGV4ID0gdGhpcy5fX2ZpbmRfXyhhZnRlck5hbWUpO1xuICB2YXIgb3B0ID0gb3B0aW9ucyB8fCB7fTtcblxuICBpZiAoaW5kZXggPT09IC0xKSB7IHRocm93IG5ldyBFcnJvcignUGFyc2VyIHJ1bGUgbm90IGZvdW5kOiAnICsgYWZ0ZXJOYW1lKTsgfVxuXG4gIHRoaXMuX19ydWxlc19fLnNwbGljZShpbmRleCArIDEsIDAsIHtcbiAgICBuYW1lOiBydWxlTmFtZSxcbiAgICBlbmFibGVkOiB0cnVlLFxuICAgIGZuOiBmbixcbiAgICBhbHQ6IG9wdC5hbHQgfHwgW11cbiAgfSk7XG5cbiAgdGhpcy5fX2NhY2hlX18gPSBudWxsO1xufTtcblxuLyoqXG4gKiBSdWxlci5wdXNoKHJ1bGVOYW1lLCBmbiBbLCBvcHRpb25zXSlcbiAqIC0gcnVsZU5hbWUgKFN0cmluZyk6IG5hbWUgb2YgYWRkZWQgcnVsZS5cbiAqIC0gZm4gKEZ1bmN0aW9uKTogcnVsZSBmdW5jdGlvbi5cbiAqIC0gb3B0aW9ucyAoT2JqZWN0KTogcnVsZSBvcHRpb25zIChub3QgbWFuZGF0b3J5KS5cbiAqXG4gKiBQdXNoIG5ldyBydWxlIHRvIHRoZSBlbmQgb2YgY2hhaW4uIFNlZSBhbHNvXG4gKiBbW1J1bGVyLmJlZm9yZV1dLCBbW1J1bGVyLmFmdGVyXV0uXG4gKlxuICogIyMjIyMgT3B0aW9uczpcbiAqXG4gKiAtIF9fYWx0X18gLSBhcnJheSB3aXRoIG5hbWVzIG9mIFwiYWx0ZXJuYXRlXCIgY2hhaW5zLlxuICpcbiAqICMjIyMjIEV4YW1wbGVcbiAqXG4gKiBgYGBqYXZhc2NyaXB0XG4gKiB2YXIgbWQgPSByZXF1aXJlKCdtYXJrZG93bi1pdCcpKCk7XG4gKlxuICogbWQuY29yZS5ydWxlci5wdXNoKCdteV9ydWxlJywgZnVuY3Rpb24gcmVwbGFjZShzdGF0ZSkge1xuICogICAvLy4uLlxuICogfSk7XG4gKiBgYGBcbiAqKi9cblJ1bGVyLnByb3RvdHlwZS5wdXNoID0gZnVuY3Rpb24gKHJ1bGVOYW1lLCBmbiwgb3B0aW9ucykge1xuICB2YXIgb3B0ID0gb3B0aW9ucyB8fCB7fTtcblxuICB0aGlzLl9fcnVsZXNfXy5wdXNoKHtcbiAgICBuYW1lOiBydWxlTmFtZSxcbiAgICBlbmFibGVkOiB0cnVlLFxuICAgIGZuOiBmbixcbiAgICBhbHQ6IG9wdC5hbHQgfHwgW11cbiAgfSk7XG5cbiAgdGhpcy5fX2NhY2hlX18gPSBudWxsO1xufTtcblxuXG4vKipcbiAqIFJ1bGVyLmVuYWJsZShsaXN0IFssIGlnbm9yZUludmFsaWRdKSAtPiBBcnJheVxuICogLSBsaXN0IChTdHJpbmd8QXJyYXkpOiBsaXN0IG9mIHJ1bGUgbmFtZXMgdG8gZW5hYmxlLlxuICogLSBpZ25vcmVJbnZhbGlkIChCb29sZWFuKTogc2V0IGB0cnVlYCB0byBpZ25vcmUgZXJyb3JzIHdoZW4gcnVsZSBub3QgZm91bmQuXG4gKlxuICogRW5hYmxlIHJ1bGVzIHdpdGggZ2l2ZW4gbmFtZXMuIElmIGFueSBydWxlIG5hbWUgbm90IGZvdW5kIC0gdGhyb3cgRXJyb3IuXG4gKiBFcnJvcnMgY2FuIGJlIGRpc2FibGVkIGJ5IHNlY29uZCBwYXJhbS5cbiAqXG4gKiBSZXR1cm5zIGxpc3Qgb2YgZm91bmQgcnVsZSBuYW1lcyAoaWYgbm8gZXhjZXB0aW9uIGhhcHBlbmVkKS5cbiAqXG4gKiBTZWUgYWxzbyBbW1J1bGVyLmRpc2FibGVdXSwgW1tSdWxlci5lbmFibGVPbmx5XV0uXG4gKiovXG5SdWxlci5wcm90b3R5cGUuZW5hYmxlID0gZnVuY3Rpb24gKGxpc3QsIGlnbm9yZUludmFsaWQpIHtcbiAgaWYgKCFBcnJheS5pc0FycmF5KGxpc3QpKSB7IGxpc3QgPSBbIGxpc3QgXTsgfVxuXG4gIHZhciByZXN1bHQgPSBbXTtcblxuICAvLyBTZWFyY2ggYnkgbmFtZSBhbmQgZW5hYmxlXG4gIGxpc3QuZm9yRWFjaChmdW5jdGlvbiAobmFtZSkge1xuICAgIHZhciBpZHggPSB0aGlzLl9fZmluZF9fKG5hbWUpO1xuXG4gICAgaWYgKGlkeCA8IDApIHtcbiAgICAgIGlmIChpZ25vcmVJbnZhbGlkKSB7IHJldHVybjsgfVxuICAgICAgdGhyb3cgbmV3IEVycm9yKCdSdWxlcyBtYW5hZ2VyOiBpbnZhbGlkIHJ1bGUgbmFtZSAnICsgbmFtZSk7XG4gICAgfVxuICAgIHRoaXMuX19ydWxlc19fW2lkeF0uZW5hYmxlZCA9IHRydWU7XG4gICAgcmVzdWx0LnB1c2gobmFtZSk7XG4gIH0sIHRoaXMpO1xuXG4gIHRoaXMuX19jYWNoZV9fID0gbnVsbDtcbiAgcmV0dXJuIHJlc3VsdDtcbn07XG5cblxuLyoqXG4gKiBSdWxlci5lbmFibGVPbmx5KGxpc3QgWywgaWdub3JlSW52YWxpZF0pXG4gKiAtIGxpc3QgKFN0cmluZ3xBcnJheSk6IGxpc3Qgb2YgcnVsZSBuYW1lcyB0byBlbmFibGUgKHdoaXRlbGlzdCkuXG4gKiAtIGlnbm9yZUludmFsaWQgKEJvb2xlYW4pOiBzZXQgYHRydWVgIHRvIGlnbm9yZSBlcnJvcnMgd2hlbiBydWxlIG5vdCBmb3VuZC5cbiAqXG4gKiBFbmFibGUgcnVsZXMgd2l0aCBnaXZlbiBuYW1lcywgYW5kIGRpc2FibGUgZXZlcnl0aGluZyBlbHNlLiBJZiBhbnkgcnVsZSBuYW1lXG4gKiBub3QgZm91bmQgLSB0aHJvdyBFcnJvci4gRXJyb3JzIGNhbiBiZSBkaXNhYmxlZCBieSBzZWNvbmQgcGFyYW0uXG4gKlxuICogU2VlIGFsc28gW1tSdWxlci5kaXNhYmxlXV0sIFtbUnVsZXIuZW5hYmxlXV0uXG4gKiovXG5SdWxlci5wcm90b3R5cGUuZW5hYmxlT25seSA9IGZ1bmN0aW9uIChsaXN0LCBpZ25vcmVJbnZhbGlkKSB7XG4gIGlmICghQXJyYXkuaXNBcnJheShsaXN0KSkgeyBsaXN0ID0gWyBsaXN0IF07IH1cblxuICB0aGlzLl9fcnVsZXNfXy5mb3JFYWNoKGZ1bmN0aW9uIChydWxlKSB7IHJ1bGUuZW5hYmxlZCA9IGZhbHNlOyB9KTtcblxuICB0aGlzLmVuYWJsZShsaXN0LCBpZ25vcmVJbnZhbGlkKTtcbn07XG5cblxuLyoqXG4gKiBSdWxlci5kaXNhYmxlKGxpc3QgWywgaWdub3JlSW52YWxpZF0pIC0+IEFycmF5XG4gKiAtIGxpc3QgKFN0cmluZ3xBcnJheSk6IGxpc3Qgb2YgcnVsZSBuYW1lcyB0byBkaXNhYmxlLlxuICogLSBpZ25vcmVJbnZhbGlkIChCb29sZWFuKTogc2V0IGB0cnVlYCB0byBpZ25vcmUgZXJyb3JzIHdoZW4gcnVsZSBub3QgZm91bmQuXG4gKlxuICogRGlzYWJsZSBydWxlcyB3aXRoIGdpdmVuIG5hbWVzLiBJZiBhbnkgcnVsZSBuYW1lIG5vdCBmb3VuZCAtIHRocm93IEVycm9yLlxuICogRXJyb3JzIGNhbiBiZSBkaXNhYmxlZCBieSBzZWNvbmQgcGFyYW0uXG4gKlxuICogUmV0dXJucyBsaXN0IG9mIGZvdW5kIHJ1bGUgbmFtZXMgKGlmIG5vIGV4Y2VwdGlvbiBoYXBwZW5lZCkuXG4gKlxuICogU2VlIGFsc28gW1tSdWxlci5lbmFibGVdXSwgW1tSdWxlci5lbmFibGVPbmx5XV0uXG4gKiovXG5SdWxlci5wcm90b3R5cGUuZGlzYWJsZSA9IGZ1bmN0aW9uIChsaXN0LCBpZ25vcmVJbnZhbGlkKSB7XG4gIGlmICghQXJyYXkuaXNBcnJheShsaXN0KSkgeyBsaXN0ID0gWyBsaXN0IF07IH1cblxuICB2YXIgcmVzdWx0ID0gW107XG5cbiAgLy8gU2VhcmNoIGJ5IG5hbWUgYW5kIGRpc2FibGVcbiAgbGlzdC5mb3JFYWNoKGZ1bmN0aW9uIChuYW1lKSB7XG4gICAgdmFyIGlkeCA9IHRoaXMuX19maW5kX18obmFtZSk7XG5cbiAgICBpZiAoaWR4IDwgMCkge1xuICAgICAgaWYgKGlnbm9yZUludmFsaWQpIHsgcmV0dXJuOyB9XG4gICAgICB0aHJvdyBuZXcgRXJyb3IoJ1J1bGVzIG1hbmFnZXI6IGludmFsaWQgcnVsZSBuYW1lICcgKyBuYW1lKTtcbiAgICB9XG4gICAgdGhpcy5fX3J1bGVzX19baWR4XS5lbmFibGVkID0gZmFsc2U7XG4gICAgcmVzdWx0LnB1c2gobmFtZSk7XG4gIH0sIHRoaXMpO1xuXG4gIHRoaXMuX19jYWNoZV9fID0gbnVsbDtcbiAgcmV0dXJuIHJlc3VsdDtcbn07XG5cblxuLyoqXG4gKiBSdWxlci5nZXRSdWxlcyhjaGFpbk5hbWUpIC0+IEFycmF5XG4gKlxuICogUmV0dXJuIGFycmF5IG9mIGFjdGl2ZSBmdW5jdGlvbnMgKHJ1bGVzKSBmb3IgZ2l2ZW4gY2hhaW4gbmFtZS4gSXQgYW5hbHl6ZXNcbiAqIHJ1bGVzIGNvbmZpZ3VyYXRpb24sIGNvbXBpbGVzIGNhY2hlcyBpZiBub3QgZXhpc3RzIGFuZCByZXR1cm5zIHJlc3VsdC5cbiAqXG4gKiBEZWZhdWx0IGNoYWluIG5hbWUgaXMgYCcnYCAoZW1wdHkgc3RyaW5nKS4gSXQgY2FuJ3QgYmUgc2tpcHBlZC4gVGhhdCdzXG4gKiBkb25lIGludGVudGlvbmFsbHksIHRvIGtlZXAgc2lnbmF0dXJlIG1vbm9tb3JwaGljIGZvciBoaWdoIHNwZWVkLlxuICoqL1xuUnVsZXIucHJvdG90eXBlLmdldFJ1bGVzID0gZnVuY3Rpb24gKGNoYWluTmFtZSkge1xuICBpZiAodGhpcy5fX2NhY2hlX18gPT09IG51bGwpIHtcbiAgICB0aGlzLl9fY29tcGlsZV9fKCk7XG4gIH1cblxuICAvLyBDaGFpbiBjYW4gYmUgZW1wdHksIGlmIHJ1bGVzIGRpc2FibGVkLiBCdXQgd2Ugc3RpbGwgaGF2ZSB0byByZXR1cm4gQXJyYXkuXG4gIHJldHVybiB0aGlzLl9fY2FjaGVfX1tjaGFpbk5hbWVdIHx8IFtdO1xufTtcblxubW9kdWxlLmV4cG9ydHMgPSBSdWxlcjtcbiIsIi8vIEJsb2NrIHF1b3Rlc1xuXG4ndXNlIHN0cmljdCc7XG5cbnZhciBpc1NwYWNlID0gcmVxdWlyZSgnLi4vY29tbW9uL3V0aWxzJykuaXNTcGFjZTtcblxuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIGJsb2NrcXVvdGUoc3RhdGUsIHN0YXJ0TGluZSwgZW5kTGluZSwgc2lsZW50KSB7XG4gIHZhciBhZGp1c3RUYWIsXG4gICAgICBjaCxcbiAgICAgIGksXG4gICAgICBpbml0aWFsLFxuICAgICAgbCxcbiAgICAgIGxhc3RMaW5lRW1wdHksXG4gICAgICBsaW5lcyxcbiAgICAgIG5leHRMaW5lLFxuICAgICAgb2Zmc2V0LFxuICAgICAgb2xkQk1hcmtzLFxuICAgICAgb2xkQlNDb3VudCxcbiAgICAgIG9sZEluZGVudCxcbiAgICAgIG9sZFBhcmVudFR5cGUsXG4gICAgICBvbGRTQ291bnQsXG4gICAgICBvbGRUU2hpZnQsXG4gICAgICBzcGFjZUFmdGVyTWFya2VyLFxuICAgICAgdGVybWluYXRlLFxuICAgICAgdGVybWluYXRvclJ1bGVzLFxuICAgICAgdG9rZW4sXG4gICAgICB3YXNPdXRkZW50ZWQsXG4gICAgICBvbGRMaW5lTWF4ID0gc3RhdGUubGluZU1heCxcbiAgICAgIHBvcyA9IHN0YXRlLmJNYXJrc1tzdGFydExpbmVdICsgc3RhdGUudFNoaWZ0W3N0YXJ0TGluZV0sXG4gICAgICBtYXggPSBzdGF0ZS5lTWFya3Nbc3RhcnRMaW5lXTtcblxuICAvLyBpZiBpdCdzIGluZGVudGVkIG1vcmUgdGhhbiAzIHNwYWNlcywgaXQgc2hvdWxkIGJlIGEgY29kZSBibG9ja1xuICBpZiAoc3RhdGUuc0NvdW50W3N0YXJ0TGluZV0gLSBzdGF0ZS5ibGtJbmRlbnQgPj0gNCkgeyByZXR1cm4gZmFsc2U7IH1cblxuICAvLyBjaGVjayB0aGUgYmxvY2sgcXVvdGUgbWFya2VyXG4gIGlmIChzdGF0ZS5zcmMuY2hhckNvZGVBdChwb3MrKykgIT09IDB4M0UvKiA+ICovKSB7IHJldHVybiBmYWxzZTsgfVxuXG4gIC8vIHdlIGtub3cgdGhhdCBpdCdzIGdvaW5nIHRvIGJlIGEgdmFsaWQgYmxvY2txdW90ZSxcbiAgLy8gc28gbm8gcG9pbnQgdHJ5aW5nIHRvIGZpbmQgdGhlIGVuZCBvZiBpdCBpbiBzaWxlbnQgbW9kZVxuICBpZiAoc2lsZW50KSB7IHJldHVybiB0cnVlOyB9XG5cbiAgLy8gc2tpcCBzcGFjZXMgYWZ0ZXIgXCI+XCIgYW5kIHJlLWNhbGN1bGF0ZSBvZmZzZXRcbiAgaW5pdGlhbCA9IG9mZnNldCA9IHN0YXRlLnNDb3VudFtzdGFydExpbmVdICsgcG9zIC0gKHN0YXRlLmJNYXJrc1tzdGFydExpbmVdICsgc3RhdGUudFNoaWZ0W3N0YXJ0TGluZV0pO1xuXG4gIC8vIHNraXAgb25lIG9wdGlvbmFsIHNwYWNlIGFmdGVyICc+J1xuICBpZiAoc3RhdGUuc3JjLmNoYXJDb2RlQXQocG9zKSA9PT0gMHgyMCAvKiBzcGFjZSAqLykge1xuICAgIC8vICcgPiAgIHRlc3QgJ1xuICAgIC8vICAgICBeIC0tIHBvc2l0aW9uIHN0YXJ0IG9mIGxpbmUgaGVyZTpcbiAgICBwb3MrKztcbiAgICBpbml0aWFsKys7XG4gICAgb2Zmc2V0Kys7XG4gICAgYWRqdXN0VGFiID0gZmFsc2U7XG4gICAgc3BhY2VBZnRlck1hcmtlciA9IHRydWU7XG4gIH0gZWxzZSBpZiAoc3RhdGUuc3JjLmNoYXJDb2RlQXQocG9zKSA9PT0gMHgwOSAvKiB0YWIgKi8pIHtcbiAgICBzcGFjZUFmdGVyTWFya2VyID0gdHJ1ZTtcblxuICAgIGlmICgoc3RhdGUuYnNDb3VudFtzdGFydExpbmVdICsgb2Zmc2V0KSAlIDQgPT09IDMpIHtcbiAgICAgIC8vICcgID5cXHQgIHRlc3QgJ1xuICAgICAgLy8gICAgICAgXiAtLSBwb3NpdGlvbiBzdGFydCBvZiBsaW5lIGhlcmUgKHRhYiBoYXMgd2lkdGg9PT0xKVxuICAgICAgcG9zKys7XG4gICAgICBpbml0aWFsKys7XG4gICAgICBvZmZzZXQrKztcbiAgICAgIGFkanVzdFRhYiA9IGZhbHNlO1xuICAgIH0gZWxzZSB7XG4gICAgICAvLyAnID5cXHQgIHRlc3QgJ1xuICAgICAgLy8gICAgXiAtLSBwb3NpdGlvbiBzdGFydCBvZiBsaW5lIGhlcmUgKyBzaGlmdCBic0NvdW50IHNsaWdodGx5XG4gICAgICAvLyAgICAgICAgIHRvIG1ha2UgZXh0cmEgc3BhY2UgYXBwZWFyXG4gICAgICBhZGp1c3RUYWIgPSB0cnVlO1xuICAgIH1cbiAgfSBlbHNlIHtcbiAgICBzcGFjZUFmdGVyTWFya2VyID0gZmFsc2U7XG4gIH1cblxuICBvbGRCTWFya3MgPSBbIHN0YXRlLmJNYXJrc1tzdGFydExpbmVdIF07XG4gIHN0YXRlLmJNYXJrc1tzdGFydExpbmVdID0gcG9zO1xuXG4gIHdoaWxlIChwb3MgPCBtYXgpIHtcbiAgICBjaCA9IHN0YXRlLnNyYy5jaGFyQ29kZUF0KHBvcyk7XG5cbiAgICBpZiAoaXNTcGFjZShjaCkpIHtcbiAgICAgIGlmIChjaCA9PT0gMHgwOSkge1xuICAgICAgICBvZmZzZXQgKz0gNCAtIChvZmZzZXQgKyBzdGF0ZS5ic0NvdW50W3N0YXJ0TGluZV0gKyAoYWRqdXN0VGFiID8gMSA6IDApKSAlIDQ7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBvZmZzZXQrKztcbiAgICAgIH1cbiAgICB9IGVsc2Uge1xuICAgICAgYnJlYWs7XG4gICAgfVxuXG4gICAgcG9zKys7XG4gIH1cblxuICBvbGRCU0NvdW50ID0gWyBzdGF0ZS5ic0NvdW50W3N0YXJ0TGluZV0gXTtcbiAgc3RhdGUuYnNDb3VudFtzdGFydExpbmVdID0gc3RhdGUuc0NvdW50W3N0YXJ0TGluZV0gKyAxICsgKHNwYWNlQWZ0ZXJNYXJrZXIgPyAxIDogMCk7XG5cbiAgbGFzdExpbmVFbXB0eSA9IHBvcyA+PSBtYXg7XG5cbiAgb2xkU0NvdW50ID0gWyBzdGF0ZS5zQ291bnRbc3RhcnRMaW5lXSBdO1xuICBzdGF0ZS5zQ291bnRbc3RhcnRMaW5lXSA9IG9mZnNldCAtIGluaXRpYWw7XG5cbiAgb2xkVFNoaWZ0ID0gWyBzdGF0ZS50U2hpZnRbc3RhcnRMaW5lXSBdO1xuICBzdGF0ZS50U2hpZnRbc3RhcnRMaW5lXSA9IHBvcyAtIHN0YXRlLmJNYXJrc1tzdGFydExpbmVdO1xuXG4gIHRlcm1pbmF0b3JSdWxlcyA9IHN0YXRlLm1kLmJsb2NrLnJ1bGVyLmdldFJ1bGVzKCdibG9ja3F1b3RlJyk7XG5cbiAgb2xkUGFyZW50VHlwZSA9IHN0YXRlLnBhcmVudFR5cGU7XG4gIHN0YXRlLnBhcmVudFR5cGUgPSAnYmxvY2txdW90ZSc7XG4gIHdhc091dGRlbnRlZCA9IGZhbHNlO1xuXG4gIC8vIFNlYXJjaCB0aGUgZW5kIG9mIHRoZSBibG9ja1xuICAvL1xuICAvLyBCbG9jayBlbmRzIHdpdGggZWl0aGVyOlxuICAvLyAgMS4gYW4gZW1wdHkgbGluZSBvdXRzaWRlOlxuICAvLyAgICAgYGBgXG4gIC8vICAgICA+IHRlc3RcbiAgLy9cbiAgLy8gICAgIGBgYFxuICAvLyAgMi4gYW4gZW1wdHkgbGluZSBpbnNpZGU6XG4gIC8vICAgICBgYGBcbiAgLy8gICAgID5cbiAgLy8gICAgIHRlc3RcbiAgLy8gICAgIGBgYFxuICAvLyAgMy4gYW5vdGhlciB0YWc6XG4gIC8vICAgICBgYGBcbiAgLy8gICAgID4gdGVzdFxuICAvLyAgICAgIC0gLSAtXG4gIC8vICAgICBgYGBcbiAgZm9yIChuZXh0TGluZSA9IHN0YXJ0TGluZSArIDE7IG5leHRMaW5lIDwgZW5kTGluZTsgbmV4dExpbmUrKykge1xuICAgIC8vIGNoZWNrIGlmIGl0J3Mgb3V0ZGVudGVkLCBpLmUuIGl0J3MgaW5zaWRlIGxpc3QgaXRlbSBhbmQgaW5kZW50ZWRcbiAgICAvLyBsZXNzIHRoYW4gc2FpZCBsaXN0IGl0ZW06XG4gICAgLy9cbiAgICAvLyBgYGBcbiAgICAvLyAxLiBhbnl0aGluZ1xuICAgIC8vICAgID4gY3VycmVudCBibG9ja3F1b3RlXG4gICAgLy8gMi4gY2hlY2tpbmcgdGhpcyBsaW5lXG4gICAgLy8gYGBgXG4gICAgaWYgKHN0YXRlLnNDb3VudFtuZXh0TGluZV0gPCBzdGF0ZS5ibGtJbmRlbnQpIHdhc091dGRlbnRlZCA9IHRydWU7XG5cbiAgICBwb3MgPSBzdGF0ZS5iTWFya3NbbmV4dExpbmVdICsgc3RhdGUudFNoaWZ0W25leHRMaW5lXTtcbiAgICBtYXggPSBzdGF0ZS5lTWFya3NbbmV4dExpbmVdO1xuXG4gICAgaWYgKHBvcyA+PSBtYXgpIHtcbiAgICAgIC8vIENhc2UgMTogbGluZSBpcyBub3QgaW5zaWRlIHRoZSBibG9ja3F1b3RlLCBhbmQgdGhpcyBsaW5lIGlzIGVtcHR5LlxuICAgICAgYnJlYWs7XG4gICAgfVxuXG4gICAgaWYgKHN0YXRlLnNyYy5jaGFyQ29kZUF0KHBvcysrKSA9PT0gMHgzRS8qID4gKi8gJiYgIXdhc091dGRlbnRlZCkge1xuICAgICAgLy8gVGhpcyBsaW5lIGlzIGluc2lkZSB0aGUgYmxvY2txdW90ZS5cblxuICAgICAgLy8gc2tpcCBzcGFjZXMgYWZ0ZXIgXCI+XCIgYW5kIHJlLWNhbGN1bGF0ZSBvZmZzZXRcbiAgICAgIGluaXRpYWwgPSBvZmZzZXQgPSBzdGF0ZS5zQ291bnRbbmV4dExpbmVdICsgcG9zIC0gKHN0YXRlLmJNYXJrc1tuZXh0TGluZV0gKyBzdGF0ZS50U2hpZnRbbmV4dExpbmVdKTtcblxuICAgICAgLy8gc2tpcCBvbmUgb3B0aW9uYWwgc3BhY2UgYWZ0ZXIgJz4nXG4gICAgICBpZiAoc3RhdGUuc3JjLmNoYXJDb2RlQXQocG9zKSA9PT0gMHgyMCAvKiBzcGFjZSAqLykge1xuICAgICAgICAvLyAnID4gICB0ZXN0ICdcbiAgICAgICAgLy8gICAgIF4gLS0gcG9zaXRpb24gc3RhcnQgb2YgbGluZSBoZXJlOlxuICAgICAgICBwb3MrKztcbiAgICAgICAgaW5pdGlhbCsrO1xuICAgICAgICBvZmZzZXQrKztcbiAgICAgICAgYWRqdXN0VGFiID0gZmFsc2U7XG4gICAgICAgIHNwYWNlQWZ0ZXJNYXJrZXIgPSB0cnVlO1xuICAgICAgfSBlbHNlIGlmIChzdGF0ZS5zcmMuY2hhckNvZGVBdChwb3MpID09PSAweDA5IC8qIHRhYiAqLykge1xuICAgICAgICBzcGFjZUFmdGVyTWFya2VyID0gdHJ1ZTtcblxuICAgICAgICBpZiAoKHN0YXRlLmJzQ291bnRbbmV4dExpbmVdICsgb2Zmc2V0KSAlIDQgPT09IDMpIHtcbiAgICAgICAgICAvLyAnICA+XFx0ICB0ZXN0ICdcbiAgICAgICAgICAvLyAgICAgICBeIC0tIHBvc2l0aW9uIHN0YXJ0IG9mIGxpbmUgaGVyZSAodGFiIGhhcyB3aWR0aD09PTEpXG4gICAgICAgICAgcG9zKys7XG4gICAgICAgICAgaW5pdGlhbCsrO1xuICAgICAgICAgIG9mZnNldCsrO1xuICAgICAgICAgIGFkanVzdFRhYiA9IGZhbHNlO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIC8vICcgPlxcdCAgdGVzdCAnXG4gICAgICAgICAgLy8gICAgXiAtLSBwb3NpdGlvbiBzdGFydCBvZiBsaW5lIGhlcmUgKyBzaGlmdCBic0NvdW50IHNsaWdodGx5XG4gICAgICAgICAgLy8gICAgICAgICB0byBtYWtlIGV4dHJhIHNwYWNlIGFwcGVhclxuICAgICAgICAgIGFkanVzdFRhYiA9IHRydWU7XG4gICAgICAgIH1cbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHNwYWNlQWZ0ZXJNYXJrZXIgPSBmYWxzZTtcbiAgICAgIH1cblxuICAgICAgb2xkQk1hcmtzLnB1c2goc3RhdGUuYk1hcmtzW25leHRMaW5lXSk7XG4gICAgICBzdGF0ZS5iTWFya3NbbmV4dExpbmVdID0gcG9zO1xuXG4gICAgICB3aGlsZSAocG9zIDwgbWF4KSB7XG4gICAgICAgIGNoID0gc3RhdGUuc3JjLmNoYXJDb2RlQXQocG9zKTtcblxuICAgICAgICBpZiAoaXNTcGFjZShjaCkpIHtcbiAgICAgICAgICBpZiAoY2ggPT09IDB4MDkpIHtcbiAgICAgICAgICAgIG9mZnNldCArPSA0IC0gKG9mZnNldCArIHN0YXRlLmJzQ291bnRbbmV4dExpbmVdICsgKGFkanVzdFRhYiA/IDEgOiAwKSkgJSA0O1xuICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBvZmZzZXQrKztcbiAgICAgICAgICB9XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgYnJlYWs7XG4gICAgICAgIH1cblxuICAgICAgICBwb3MrKztcbiAgICAgIH1cblxuICAgICAgbGFzdExpbmVFbXB0eSA9IHBvcyA+PSBtYXg7XG5cbiAgICAgIG9sZEJTQ291bnQucHVzaChzdGF0ZS5ic0NvdW50W25leHRMaW5lXSk7XG4gICAgICBzdGF0ZS5ic0NvdW50W25leHRMaW5lXSA9IHN0YXRlLnNDb3VudFtuZXh0TGluZV0gKyAxICsgKHNwYWNlQWZ0ZXJNYXJrZXIgPyAxIDogMCk7XG5cbiAgICAgIG9sZFNDb3VudC5wdXNoKHN0YXRlLnNDb3VudFtuZXh0TGluZV0pO1xuICAgICAgc3RhdGUuc0NvdW50W25leHRMaW5lXSA9IG9mZnNldCAtIGluaXRpYWw7XG5cbiAgICAgIG9sZFRTaGlmdC5wdXNoKHN0YXRlLnRTaGlmdFtuZXh0TGluZV0pO1xuICAgICAgc3RhdGUudFNoaWZ0W25leHRMaW5lXSA9IHBvcyAtIHN0YXRlLmJNYXJrc1tuZXh0TGluZV07XG4gICAgICBjb250aW51ZTtcbiAgICB9XG5cbiAgICAvLyBDYXNlIDI6IGxpbmUgaXMgbm90IGluc2lkZSB0aGUgYmxvY2txdW90ZSwgYW5kIHRoZSBsYXN0IGxpbmUgd2FzIGVtcHR5LlxuICAgIGlmIChsYXN0TGluZUVtcHR5KSB7IGJyZWFrOyB9XG5cbiAgICAvLyBDYXNlIDM6IGFub3RoZXIgdGFnIGZvdW5kLlxuICAgIHRlcm1pbmF0ZSA9IGZhbHNlO1xuICAgIGZvciAoaSA9IDAsIGwgPSB0ZXJtaW5hdG9yUnVsZXMubGVuZ3RoOyBpIDwgbDsgaSsrKSB7XG4gICAgICBpZiAodGVybWluYXRvclJ1bGVzW2ldKHN0YXRlLCBuZXh0TGluZSwgZW5kTGluZSwgdHJ1ZSkpIHtcbiAgICAgICAgdGVybWluYXRlID0gdHJ1ZTtcbiAgICAgICAgYnJlYWs7XG4gICAgICB9XG4gICAgfVxuXG4gICAgaWYgKHRlcm1pbmF0ZSkge1xuICAgICAgLy8gUXVpcmsgdG8gZW5mb3JjZSBcImhhcmQgdGVybWluYXRpb24gbW9kZVwiIGZvciBwYXJhZ3JhcGhzO1xuICAgICAgLy8gbm9ybWFsbHkgaWYgeW91IGNhbGwgYHRva2VuaXplKHN0YXRlLCBzdGFydExpbmUsIG5leHRMaW5lKWAsXG4gICAgICAvLyBwYXJhZ3JhcGhzIHdpbGwgbG9vayBiZWxvdyBuZXh0TGluZSBmb3IgcGFyYWdyYXBoIGNvbnRpbnVhdGlvbixcbiAgICAgIC8vIGJ1dCBpZiBibG9ja3F1b3RlIGlzIHRlcm1pbmF0ZWQgYnkgYW5vdGhlciB0YWcsIHRoZXkgc2hvdWxkbid0XG4gICAgICBzdGF0ZS5saW5lTWF4ID0gbmV4dExpbmU7XG5cbiAgICAgIGlmIChzdGF0ZS5ibGtJbmRlbnQgIT09IDApIHtcbiAgICAgICAgLy8gc3RhdGUuYmxrSW5kZW50IHdhcyBub24temVybywgd2Ugbm93IHNldCBpdCB0byB6ZXJvLFxuICAgICAgICAvLyBzbyB3ZSBuZWVkIHRvIHJlLWNhbGN1bGF0ZSBhbGwgb2Zmc2V0cyB0byBhcHBlYXIgYXNcbiAgICAgICAgLy8gaWYgaW5kZW50IHdhc24ndCBjaGFuZ2VkXG4gICAgICAgIG9sZEJNYXJrcy5wdXNoKHN0YXRlLmJNYXJrc1tuZXh0TGluZV0pO1xuICAgICAgICBvbGRCU0NvdW50LnB1c2goc3RhdGUuYnNDb3VudFtuZXh0TGluZV0pO1xuICAgICAgICBvbGRUU2hpZnQucHVzaChzdGF0ZS50U2hpZnRbbmV4dExpbmVdKTtcbiAgICAgICAgb2xkU0NvdW50LnB1c2goc3RhdGUuc0NvdW50W25leHRMaW5lXSk7XG4gICAgICAgIHN0YXRlLnNDb3VudFtuZXh0TGluZV0gLT0gc3RhdGUuYmxrSW5kZW50O1xuICAgICAgfVxuXG4gICAgICBicmVhaztcbiAgICB9XG5cbiAgICBvbGRCTWFya3MucHVzaChzdGF0ZS5iTWFya3NbbmV4dExpbmVdKTtcbiAgICBvbGRCU0NvdW50LnB1c2goc3RhdGUuYnNDb3VudFtuZXh0TGluZV0pO1xuICAgIG9sZFRTaGlmdC5wdXNoKHN0YXRlLnRTaGlmdFtuZXh0TGluZV0pO1xuICAgIG9sZFNDb3VudC5wdXNoKHN0YXRlLnNDb3VudFtuZXh0TGluZV0pO1xuXG4gICAgLy8gQSBuZWdhdGl2ZSBpbmRlbnRhdGlvbiBtZWFucyB0aGF0IHRoaXMgaXMgYSBwYXJhZ3JhcGggY29udGludWF0aW9uXG4gICAgLy9cbiAgICBzdGF0ZS5zQ291bnRbbmV4dExpbmVdID0gLTE7XG4gIH1cblxuICBvbGRJbmRlbnQgPSBzdGF0ZS5ibGtJbmRlbnQ7XG4gIHN0YXRlLmJsa0luZGVudCA9IDA7XG5cbiAgdG9rZW4gICAgICAgID0gc3RhdGUucHVzaCgnYmxvY2txdW90ZV9vcGVuJywgJ2Jsb2NrcXVvdGUnLCAxKTtcbiAgdG9rZW4ubWFya3VwID0gJz4nO1xuICB0b2tlbi5tYXAgICAgPSBsaW5lcyA9IFsgc3RhcnRMaW5lLCAwIF07XG5cbiAgc3RhdGUubWQuYmxvY2sudG9rZW5pemUoc3RhdGUsIHN0YXJ0TGluZSwgbmV4dExpbmUpO1xuXG4gIHRva2VuICAgICAgICA9IHN0YXRlLnB1c2goJ2Jsb2NrcXVvdGVfY2xvc2UnLCAnYmxvY2txdW90ZScsIC0xKTtcbiAgdG9rZW4ubWFya3VwID0gJz4nO1xuXG4gIHN0YXRlLmxpbmVNYXggPSBvbGRMaW5lTWF4O1xuICBzdGF0ZS5wYXJlbnRUeXBlID0gb2xkUGFyZW50VHlwZTtcbiAgbGluZXNbMV0gPSBzdGF0ZS5saW5lO1xuXG4gIC8vIFJlc3RvcmUgb3JpZ2luYWwgdFNoaWZ0OyB0aGlzIG1pZ2h0IG5vdCBiZSBuZWNlc3Nhcnkgc2luY2UgdGhlIHBhcnNlclxuICAvLyBoYXMgYWxyZWFkeSBiZWVuIGhlcmUsIGJ1dCBqdXN0IHRvIG1ha2Ugc3VyZSB3ZSBjYW4gZG8gdGhhdC5cbiAgZm9yIChpID0gMDsgaSA8IG9sZFRTaGlmdC5sZW5ndGg7IGkrKykge1xuICAgIHN0YXRlLmJNYXJrc1tpICsgc3RhcnRMaW5lXSA9IG9sZEJNYXJrc1tpXTtcbiAgICBzdGF0ZS50U2hpZnRbaSArIHN0YXJ0TGluZV0gPSBvbGRUU2hpZnRbaV07XG4gICAgc3RhdGUuc0NvdW50W2kgKyBzdGFydExpbmVdID0gb2xkU0NvdW50W2ldO1xuICAgIHN0YXRlLmJzQ291bnRbaSArIHN0YXJ0TGluZV0gPSBvbGRCU0NvdW50W2ldO1xuICB9XG4gIHN0YXRlLmJsa0luZGVudCA9IG9sZEluZGVudDtcblxuICByZXR1cm4gdHJ1ZTtcbn07XG4iLCIvLyBDb2RlIGJsb2NrICg0IHNwYWNlcyBwYWRkZWQpXG5cbid1c2Ugc3RyaWN0JztcblxuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIGNvZGUoc3RhdGUsIHN0YXJ0TGluZSwgZW5kTGluZS8qLCBzaWxlbnQqLykge1xuICB2YXIgbmV4dExpbmUsIGxhc3QsIHRva2VuO1xuXG4gIGlmIChzdGF0ZS5zQ291bnRbc3RhcnRMaW5lXSAtIHN0YXRlLmJsa0luZGVudCA8IDQpIHsgcmV0dXJuIGZhbHNlOyB9XG5cbiAgbGFzdCA9IG5leHRMaW5lID0gc3RhcnRMaW5lICsgMTtcblxuICB3aGlsZSAobmV4dExpbmUgPCBlbmRMaW5lKSB7XG4gICAgaWYgKHN0YXRlLmlzRW1wdHkobmV4dExpbmUpKSB7XG4gICAgICBuZXh0TGluZSsrO1xuICAgICAgY29udGludWU7XG4gICAgfVxuXG4gICAgaWYgKHN0YXRlLnNDb3VudFtuZXh0TGluZV0gLSBzdGF0ZS5ibGtJbmRlbnQgPj0gNCkge1xuICAgICAgbmV4dExpbmUrKztcbiAgICAgIGxhc3QgPSBuZXh0TGluZTtcbiAgICAgIGNvbnRpbnVlO1xuICAgIH1cbiAgICBicmVhaztcbiAgfVxuXG4gIHN0YXRlLmxpbmUgPSBsYXN0O1xuXG4gIHRva2VuICAgICAgICAgPSBzdGF0ZS5wdXNoKCdjb2RlX2Jsb2NrJywgJ2NvZGUnLCAwKTtcbiAgdG9rZW4uY29udGVudCA9IHN0YXRlLmdldExpbmVzKHN0YXJ0TGluZSwgbGFzdCwgNCArIHN0YXRlLmJsa0luZGVudCwgdHJ1ZSk7XG4gIHRva2VuLm1hcCAgICAgPSBbIHN0YXJ0TGluZSwgc3RhdGUubGluZSBdO1xuXG4gIHJldHVybiB0cnVlO1xufTtcbiIsIi8vIGZlbmNlcyAoYGBgIGxhbmcsIH5+fiBsYW5nKVxuXG4ndXNlIHN0cmljdCc7XG5cblxubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiBmZW5jZShzdGF0ZSwgc3RhcnRMaW5lLCBlbmRMaW5lLCBzaWxlbnQpIHtcbiAgdmFyIG1hcmtlciwgbGVuLCBwYXJhbXMsIG5leHRMaW5lLCBtZW0sIHRva2VuLCBtYXJrdXAsXG4gICAgICBoYXZlRW5kTWFya2VyID0gZmFsc2UsXG4gICAgICBwb3MgPSBzdGF0ZS5iTWFya3Nbc3RhcnRMaW5lXSArIHN0YXRlLnRTaGlmdFtzdGFydExpbmVdLFxuICAgICAgbWF4ID0gc3RhdGUuZU1hcmtzW3N0YXJ0TGluZV07XG5cbiAgLy8gaWYgaXQncyBpbmRlbnRlZCBtb3JlIHRoYW4gMyBzcGFjZXMsIGl0IHNob3VsZCBiZSBhIGNvZGUgYmxvY2tcbiAgaWYgKHN0YXRlLnNDb3VudFtzdGFydExpbmVdIC0gc3RhdGUuYmxrSW5kZW50ID49IDQpIHsgcmV0dXJuIGZhbHNlOyB9XG5cbiAgaWYgKHBvcyArIDMgPiBtYXgpIHsgcmV0dXJuIGZhbHNlOyB9XG5cbiAgbWFya2VyID0gc3RhdGUuc3JjLmNoYXJDb2RlQXQocG9zKTtcblxuICBpZiAobWFya2VyICE9PSAweDdFLyogfiAqLyAmJiBtYXJrZXIgIT09IDB4NjAgLyogYCAqLykge1xuICAgIHJldHVybiBmYWxzZTtcbiAgfVxuXG4gIC8vIHNjYW4gbWFya2VyIGxlbmd0aFxuICBtZW0gPSBwb3M7XG4gIHBvcyA9IHN0YXRlLnNraXBDaGFycyhwb3MsIG1hcmtlcik7XG5cbiAgbGVuID0gcG9zIC0gbWVtO1xuXG4gIGlmIChsZW4gPCAzKSB7IHJldHVybiBmYWxzZTsgfVxuXG4gIG1hcmt1cCA9IHN0YXRlLnNyYy5zbGljZShtZW0sIHBvcyk7XG4gIHBhcmFtcyA9IHN0YXRlLnNyYy5zbGljZShwb3MsIG1heCk7XG5cbiAgaWYgKG1hcmtlciA9PT0gMHg2MCAvKiBgICovKSB7XG4gICAgaWYgKHBhcmFtcy5pbmRleE9mKFN0cmluZy5mcm9tQ2hhckNvZGUobWFya2VyKSkgPj0gMCkge1xuICAgICAgcmV0dXJuIGZhbHNlO1xuICAgIH1cbiAgfVxuXG4gIC8vIFNpbmNlIHN0YXJ0IGlzIGZvdW5kLCB3ZSBjYW4gcmVwb3J0IHN1Y2Nlc3MgaGVyZSBpbiB2YWxpZGF0aW9uIG1vZGVcbiAgaWYgKHNpbGVudCkgeyByZXR1cm4gdHJ1ZTsgfVxuXG4gIC8vIHNlYXJjaCBlbmQgb2YgYmxvY2tcbiAgbmV4dExpbmUgPSBzdGFydExpbmU7XG5cbiAgZm9yICg7Oykge1xuICAgIG5leHRMaW5lKys7XG4gICAgaWYgKG5leHRMaW5lID49IGVuZExpbmUpIHtcbiAgICAgIC8vIHVuY2xvc2VkIGJsb2NrIHNob3VsZCBiZSBhdXRvY2xvc2VkIGJ5IGVuZCBvZiBkb2N1bWVudC5cbiAgICAgIC8vIGFsc28gYmxvY2sgc2VlbXMgdG8gYmUgYXV0b2Nsb3NlZCBieSBlbmQgb2YgcGFyZW50XG4gICAgICBicmVhaztcbiAgICB9XG5cbiAgICBwb3MgPSBtZW0gPSBzdGF0ZS5iTWFya3NbbmV4dExpbmVdICsgc3RhdGUudFNoaWZ0W25leHRMaW5lXTtcbiAgICBtYXggPSBzdGF0ZS5lTWFya3NbbmV4dExpbmVdO1xuXG4gICAgaWYgKHBvcyA8IG1heCAmJiBzdGF0ZS5zQ291bnRbbmV4dExpbmVdIDwgc3RhdGUuYmxrSW5kZW50KSB7XG4gICAgICAvLyBub24tZW1wdHkgbGluZSB3aXRoIG5lZ2F0aXZlIGluZGVudCBzaG91bGQgc3RvcCB0aGUgbGlzdDpcbiAgICAgIC8vIC0gYGBgXG4gICAgICAvLyAgdGVzdFxuICAgICAgYnJlYWs7XG4gICAgfVxuXG4gICAgaWYgKHN0YXRlLnNyYy5jaGFyQ29kZUF0KHBvcykgIT09IG1hcmtlcikgeyBjb250aW51ZTsgfVxuXG4gICAgaWYgKHN0YXRlLnNDb3VudFtuZXh0TGluZV0gLSBzdGF0ZS5ibGtJbmRlbnQgPj0gNCkge1xuICAgICAgLy8gY2xvc2luZyBmZW5jZSBzaG91bGQgYmUgaW5kZW50ZWQgbGVzcyB0aGFuIDQgc3BhY2VzXG4gICAgICBjb250aW51ZTtcbiAgICB9XG5cbiAgICBwb3MgPSBzdGF0ZS5za2lwQ2hhcnMocG9zLCBtYXJrZXIpO1xuXG4gICAgLy8gY2xvc2luZyBjb2RlIGZlbmNlIG11c3QgYmUgYXQgbGVhc3QgYXMgbG9uZyBhcyB0aGUgb3BlbmluZyBvbmVcbiAgICBpZiAocG9zIC0gbWVtIDwgbGVuKSB7IGNvbnRpbnVlOyB9XG5cbiAgICAvLyBtYWtlIHN1cmUgdGFpbCBoYXMgc3BhY2VzIG9ubHlcbiAgICBwb3MgPSBzdGF0ZS5za2lwU3BhY2VzKHBvcyk7XG5cbiAgICBpZiAocG9zIDwgbWF4KSB7IGNvbnRpbnVlOyB9XG5cbiAgICBoYXZlRW5kTWFya2VyID0gdHJ1ZTtcbiAgICAvLyBmb3VuZCFcbiAgICBicmVhaztcbiAgfVxuXG4gIC8vIElmIGEgZmVuY2UgaGFzIGhlYWRpbmcgc3BhY2VzLCB0aGV5IHNob3VsZCBiZSByZW1vdmVkIGZyb20gaXRzIGlubmVyIGJsb2NrXG4gIGxlbiA9IHN0YXRlLnNDb3VudFtzdGFydExpbmVdO1xuXG4gIHN0YXRlLmxpbmUgPSBuZXh0TGluZSArIChoYXZlRW5kTWFya2VyID8gMSA6IDApO1xuXG4gIHRva2VuICAgICAgICAgPSBzdGF0ZS5wdXNoKCdmZW5jZScsICdjb2RlJywgMCk7XG4gIHRva2VuLmluZm8gICAgPSBwYXJhbXM7XG4gIHRva2VuLmNvbnRlbnQgPSBzdGF0ZS5nZXRMaW5lcyhzdGFydExpbmUgKyAxLCBuZXh0TGluZSwgbGVuLCB0cnVlKTtcbiAgdG9rZW4ubWFya3VwICA9IG1hcmt1cDtcbiAgdG9rZW4ubWFwICAgICA9IFsgc3RhcnRMaW5lLCBzdGF0ZS5saW5lIF07XG5cbiAgcmV0dXJuIHRydWU7XG59O1xuIiwiLy8gaGVhZGluZyAoIywgIyMsIC4uLilcblxuJ3VzZSBzdHJpY3QnO1xuXG52YXIgaXNTcGFjZSA9IHJlcXVpcmUoJy4uL2NvbW1vbi91dGlscycpLmlzU3BhY2U7XG5cblxubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiBoZWFkaW5nKHN0YXRlLCBzdGFydExpbmUsIGVuZExpbmUsIHNpbGVudCkge1xuICB2YXIgY2gsIGxldmVsLCB0bXAsIHRva2VuLFxuICAgICAgcG9zID0gc3RhdGUuYk1hcmtzW3N0YXJ0TGluZV0gKyBzdGF0ZS50U2hpZnRbc3RhcnRMaW5lXSxcbiAgICAgIG1heCA9IHN0YXRlLmVNYXJrc1tzdGFydExpbmVdO1xuXG4gIC8vIGlmIGl0J3MgaW5kZW50ZWQgbW9yZSB0aGFuIDMgc3BhY2VzLCBpdCBzaG91bGQgYmUgYSBjb2RlIGJsb2NrXG4gIGlmIChzdGF0ZS5zQ291bnRbc3RhcnRMaW5lXSAtIHN0YXRlLmJsa0luZGVudCA+PSA0KSB7IHJldHVybiBmYWxzZTsgfVxuXG4gIGNoICA9IHN0YXRlLnNyYy5jaGFyQ29kZUF0KHBvcyk7XG5cbiAgaWYgKGNoICE9PSAweDIzLyogIyAqLyB8fCBwb3MgPj0gbWF4KSB7IHJldHVybiBmYWxzZTsgfVxuXG4gIC8vIGNvdW50IGhlYWRpbmcgbGV2ZWxcbiAgbGV2ZWwgPSAxO1xuICBjaCA9IHN0YXRlLnNyYy5jaGFyQ29kZUF0KCsrcG9zKTtcbiAgd2hpbGUgKGNoID09PSAweDIzLyogIyAqLyAmJiBwb3MgPCBtYXggJiYgbGV2ZWwgPD0gNikge1xuICAgIGxldmVsKys7XG4gICAgY2ggPSBzdGF0ZS5zcmMuY2hhckNvZGVBdCgrK3Bvcyk7XG4gIH1cblxuICBpZiAobGV2ZWwgPiA2IHx8IChwb3MgPCBtYXggJiYgIWlzU3BhY2UoY2gpKSkgeyByZXR1cm4gZmFsc2U7IH1cblxuICBpZiAoc2lsZW50KSB7IHJldHVybiB0cnVlOyB9XG5cbiAgLy8gTGV0J3MgY3V0IHRhaWxzIGxpa2UgJyAgICAjIyMgICcgZnJvbSB0aGUgZW5kIG9mIHN0cmluZ1xuXG4gIG1heCA9IHN0YXRlLnNraXBTcGFjZXNCYWNrKG1heCwgcG9zKTtcbiAgdG1wID0gc3RhdGUuc2tpcENoYXJzQmFjayhtYXgsIDB4MjMsIHBvcyk7IC8vICNcbiAgaWYgKHRtcCA+IHBvcyAmJiBpc1NwYWNlKHN0YXRlLnNyYy5jaGFyQ29kZUF0KHRtcCAtIDEpKSkge1xuICAgIG1heCA9IHRtcDtcbiAgfVxuXG4gIHN0YXRlLmxpbmUgPSBzdGFydExpbmUgKyAxO1xuXG4gIHRva2VuICAgICAgICA9IHN0YXRlLnB1c2goJ2hlYWRpbmdfb3BlbicsICdoJyArIFN0cmluZyhsZXZlbCksIDEpO1xuICB0b2tlbi5tYXJrdXAgPSAnIyMjIyMjIyMnLnNsaWNlKDAsIGxldmVsKTtcbiAgdG9rZW4ubWFwICAgID0gWyBzdGFydExpbmUsIHN0YXRlLmxpbmUgXTtcblxuICB0b2tlbiAgICAgICAgICA9IHN0YXRlLnB1c2goJ2lubGluZScsICcnLCAwKTtcbiAgdG9rZW4uY29udGVudCAgPSBzdGF0ZS5zcmMuc2xpY2UocG9zLCBtYXgpLnRyaW0oKTtcbiAgdG9rZW4ubWFwICAgICAgPSBbIHN0YXJ0TGluZSwgc3RhdGUubGluZSBdO1xuICB0b2tlbi5jaGlsZHJlbiA9IFtdO1xuXG4gIHRva2VuICAgICAgICA9IHN0YXRlLnB1c2goJ2hlYWRpbmdfY2xvc2UnLCAnaCcgKyBTdHJpbmcobGV2ZWwpLCAtMSk7XG4gIHRva2VuLm1hcmt1cCA9ICcjIyMjIyMjIycuc2xpY2UoMCwgbGV2ZWwpO1xuXG4gIHJldHVybiB0cnVlO1xufTtcbiIsIi8vIEhvcml6b250YWwgcnVsZVxuXG4ndXNlIHN0cmljdCc7XG5cbnZhciBpc1NwYWNlID0gcmVxdWlyZSgnLi4vY29tbW9uL3V0aWxzJykuaXNTcGFjZTtcblxuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIGhyKHN0YXRlLCBzdGFydExpbmUsIGVuZExpbmUsIHNpbGVudCkge1xuICB2YXIgbWFya2VyLCBjbnQsIGNoLCB0b2tlbixcbiAgICAgIHBvcyA9IHN0YXRlLmJNYXJrc1tzdGFydExpbmVdICsgc3RhdGUudFNoaWZ0W3N0YXJ0TGluZV0sXG4gICAgICBtYXggPSBzdGF0ZS5lTWFya3Nbc3RhcnRMaW5lXTtcblxuICAvLyBpZiBpdCdzIGluZGVudGVkIG1vcmUgdGhhbiAzIHNwYWNlcywgaXQgc2hvdWxkIGJlIGEgY29kZSBibG9ja1xuICBpZiAoc3RhdGUuc0NvdW50W3N0YXJ0TGluZV0gLSBzdGF0ZS5ibGtJbmRlbnQgPj0gNCkgeyByZXR1cm4gZmFsc2U7IH1cblxuICBtYXJrZXIgPSBzdGF0ZS5zcmMuY2hhckNvZGVBdChwb3MrKyk7XG5cbiAgLy8gQ2hlY2sgaHIgbWFya2VyXG4gIGlmIChtYXJrZXIgIT09IDB4MkEvKiAqICovICYmXG4gICAgICBtYXJrZXIgIT09IDB4MkQvKiAtICovICYmXG4gICAgICBtYXJrZXIgIT09IDB4NUYvKiBfICovKSB7XG4gICAgcmV0dXJuIGZhbHNlO1xuICB9XG5cbiAgLy8gbWFya2VycyBjYW4gYmUgbWl4ZWQgd2l0aCBzcGFjZXMsIGJ1dCB0aGVyZSBzaG91bGQgYmUgYXQgbGVhc3QgMyBvZiB0aGVtXG5cbiAgY250ID0gMTtcbiAgd2hpbGUgKHBvcyA8IG1heCkge1xuICAgIGNoID0gc3RhdGUuc3JjLmNoYXJDb2RlQXQocG9zKyspO1xuICAgIGlmIChjaCAhPT0gbWFya2VyICYmICFpc1NwYWNlKGNoKSkgeyByZXR1cm4gZmFsc2U7IH1cbiAgICBpZiAoY2ggPT09IG1hcmtlcikgeyBjbnQrKzsgfVxuICB9XG5cbiAgaWYgKGNudCA8IDMpIHsgcmV0dXJuIGZhbHNlOyB9XG5cbiAgaWYgKHNpbGVudCkgeyByZXR1cm4gdHJ1ZTsgfVxuXG4gIHN0YXRlLmxpbmUgPSBzdGFydExpbmUgKyAxO1xuXG4gIHRva2VuICAgICAgICA9IHN0YXRlLnB1c2goJ2hyJywgJ2hyJywgMCk7XG4gIHRva2VuLm1hcCAgICA9IFsgc3RhcnRMaW5lLCBzdGF0ZS5saW5lIF07XG4gIHRva2VuLm1hcmt1cCA9IEFycmF5KGNudCArIDEpLmpvaW4oU3RyaW5nLmZyb21DaGFyQ29kZShtYXJrZXIpKTtcblxuICByZXR1cm4gdHJ1ZTtcbn07XG4iLCIvLyBIVE1MIGJsb2NrXG5cbid1c2Ugc3RyaWN0JztcblxuXG52YXIgYmxvY2tfbmFtZXMgPSByZXF1aXJlKCcuLi9jb21tb24vaHRtbF9ibG9ja3MnKTtcbnZhciBIVE1MX09QRU5fQ0xPU0VfVEFHX1JFID0gcmVxdWlyZSgnLi4vY29tbW9uL2h0bWxfcmUnKS5IVE1MX09QRU5fQ0xPU0VfVEFHX1JFO1xuXG4vLyBBbiBhcnJheSBvZiBvcGVuaW5nIGFuZCBjb3JyZXNwb25kaW5nIGNsb3Npbmcgc2VxdWVuY2VzIGZvciBodG1sIHRhZ3MsXG4vLyBsYXN0IGFyZ3VtZW50IGRlZmluZXMgd2hldGhlciBpdCBjYW4gdGVybWluYXRlIGEgcGFyYWdyYXBoIG9yIG5vdFxuLy9cbnZhciBIVE1MX1NFUVVFTkNFUyA9IFtcbiAgWyAvXjwoc2NyaXB0fHByZXxzdHlsZSkoPz0oXFxzfD58JCkpL2ksIC88XFwvKHNjcmlwdHxwcmV8c3R5bGUpPi9pLCB0cnVlIF0sXG4gIFsgL148IS0tLywgICAgICAgIC8tLT4vLCAgIHRydWUgXSxcbiAgWyAvXjxcXD8vLCAgICAgICAgIC9cXD8+LywgICB0cnVlIF0sXG4gIFsgL148IVtBLVpdLywgICAgIC8+LywgICAgIHRydWUgXSxcbiAgWyAvXjwhXFxbQ0RBVEFcXFsvLCAvXFxdXFxdPi8sIHRydWUgXSxcbiAgWyBuZXcgUmVnRXhwKCdePC8/KCcgKyBibG9ja19uYW1lcy5qb2luKCd8JykgKyAnKSg/PShcXFxcc3wvPz58JCkpJywgJ2knKSwgL14kLywgdHJ1ZSBdLFxuICBbIG5ldyBSZWdFeHAoSFRNTF9PUEVOX0NMT1NFX1RBR19SRS5zb3VyY2UgKyAnXFxcXHMqJCcpLCAgL14kLywgZmFsc2UgXVxuXTtcblxuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIGh0bWxfYmxvY2soc3RhdGUsIHN0YXJ0TGluZSwgZW5kTGluZSwgc2lsZW50KSB7XG4gIHZhciBpLCBuZXh0TGluZSwgdG9rZW4sIGxpbmVUZXh0LFxuICAgICAgcG9zID0gc3RhdGUuYk1hcmtzW3N0YXJ0TGluZV0gKyBzdGF0ZS50U2hpZnRbc3RhcnRMaW5lXSxcbiAgICAgIG1heCA9IHN0YXRlLmVNYXJrc1tzdGFydExpbmVdO1xuXG4gIC8vIGlmIGl0J3MgaW5kZW50ZWQgbW9yZSB0aGFuIDMgc3BhY2VzLCBpdCBzaG91bGQgYmUgYSBjb2RlIGJsb2NrXG4gIGlmIChzdGF0ZS5zQ291bnRbc3RhcnRMaW5lXSAtIHN0YXRlLmJsa0luZGVudCA+PSA0KSB7IHJldHVybiBmYWxzZTsgfVxuXG4gIGlmICghc3RhdGUubWQub3B0aW9ucy5odG1sKSB7IHJldHVybiBmYWxzZTsgfVxuXG4gIGlmIChzdGF0ZS5zcmMuY2hhckNvZGVBdChwb3MpICE9PSAweDNDLyogPCAqLykgeyByZXR1cm4gZmFsc2U7IH1cblxuICBsaW5lVGV4dCA9IHN0YXRlLnNyYy5zbGljZShwb3MsIG1heCk7XG5cbiAgZm9yIChpID0gMDsgaSA8IEhUTUxfU0VRVUVOQ0VTLmxlbmd0aDsgaSsrKSB7XG4gICAgaWYgKEhUTUxfU0VRVUVOQ0VTW2ldWzBdLnRlc3QobGluZVRleHQpKSB7IGJyZWFrOyB9XG4gIH1cblxuICBpZiAoaSA9PT0gSFRNTF9TRVFVRU5DRVMubGVuZ3RoKSB7IHJldHVybiBmYWxzZTsgfVxuXG4gIGlmIChzaWxlbnQpIHtcbiAgICAvLyB0cnVlIGlmIHRoaXMgc2VxdWVuY2UgY2FuIGJlIGEgdGVybWluYXRvciwgZmFsc2Ugb3RoZXJ3aXNlXG4gICAgcmV0dXJuIEhUTUxfU0VRVUVOQ0VTW2ldWzJdO1xuICB9XG5cbiAgbmV4dExpbmUgPSBzdGFydExpbmUgKyAxO1xuXG4gIC8vIElmIHdlIGFyZSBoZXJlIC0gd2UgZGV0ZWN0ZWQgSFRNTCBibG9jay5cbiAgLy8gTGV0J3Mgcm9sbCBkb3duIHRpbGwgYmxvY2sgZW5kLlxuICBpZiAoIUhUTUxfU0VRVUVOQ0VTW2ldWzFdLnRlc3QobGluZVRleHQpKSB7XG4gICAgZm9yICg7IG5leHRMaW5lIDwgZW5kTGluZTsgbmV4dExpbmUrKykge1xuICAgICAgaWYgKHN0YXRlLnNDb3VudFtuZXh0TGluZV0gPCBzdGF0ZS5ibGtJbmRlbnQpIHsgYnJlYWs7IH1cblxuICAgICAgcG9zID0gc3RhdGUuYk1hcmtzW25leHRMaW5lXSArIHN0YXRlLnRTaGlmdFtuZXh0TGluZV07XG4gICAgICBtYXggPSBzdGF0ZS5lTWFya3NbbmV4dExpbmVdO1xuICAgICAgbGluZVRleHQgPSBzdGF0ZS5zcmMuc2xpY2UocG9zLCBtYXgpO1xuXG4gICAgICBpZiAoSFRNTF9TRVFVRU5DRVNbaV1bMV0udGVzdChsaW5lVGV4dCkpIHtcbiAgICAgICAgaWYgKGxpbmVUZXh0Lmxlbmd0aCAhPT0gMCkgeyBuZXh0TGluZSsrOyB9XG4gICAgICAgIGJyZWFrO1xuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIHN0YXRlLmxpbmUgPSBuZXh0TGluZTtcblxuICB0b2tlbiAgICAgICAgID0gc3RhdGUucHVzaCgnaHRtbF9ibG9jaycsICcnLCAwKTtcbiAgdG9rZW4ubWFwICAgICA9IFsgc3RhcnRMaW5lLCBuZXh0TGluZSBdO1xuICB0b2tlbi5jb250ZW50ID0gc3RhdGUuZ2V0TGluZXMoc3RhcnRMaW5lLCBuZXh0TGluZSwgc3RhdGUuYmxrSW5kZW50LCB0cnVlKTtcblxuICByZXR1cm4gdHJ1ZTtcbn07XG4iLCIvLyBsaGVhZGluZyAoLS0tLCA9PT0pXG5cbid1c2Ugc3RyaWN0JztcblxuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIGxoZWFkaW5nKHN0YXRlLCBzdGFydExpbmUsIGVuZExpbmUvKiwgc2lsZW50Ki8pIHtcbiAgdmFyIGNvbnRlbnQsIHRlcm1pbmF0ZSwgaSwgbCwgdG9rZW4sIHBvcywgbWF4LCBsZXZlbCwgbWFya2VyLFxuICAgICAgbmV4dExpbmUgPSBzdGFydExpbmUgKyAxLCBvbGRQYXJlbnRUeXBlLFxuICAgICAgdGVybWluYXRvclJ1bGVzID0gc3RhdGUubWQuYmxvY2sucnVsZXIuZ2V0UnVsZXMoJ3BhcmFncmFwaCcpO1xuXG4gIC8vIGlmIGl0J3MgaW5kZW50ZWQgbW9yZSB0aGFuIDMgc3BhY2VzLCBpdCBzaG91bGQgYmUgYSBjb2RlIGJsb2NrXG4gIGlmIChzdGF0ZS5zQ291bnRbc3RhcnRMaW5lXSAtIHN0YXRlLmJsa0luZGVudCA+PSA0KSB7IHJldHVybiBmYWxzZTsgfVxuXG4gIG9sZFBhcmVudFR5cGUgPSBzdGF0ZS5wYXJlbnRUeXBlO1xuICBzdGF0ZS5wYXJlbnRUeXBlID0gJ3BhcmFncmFwaCc7IC8vIHVzZSBwYXJhZ3JhcGggdG8gbWF0Y2ggdGVybWluYXRvclJ1bGVzXG5cbiAgLy8ganVtcCBsaW5lLWJ5LWxpbmUgdW50aWwgZW1wdHkgb25lIG9yIEVPRlxuICBmb3IgKDsgbmV4dExpbmUgPCBlbmRMaW5lICYmICFzdGF0ZS5pc0VtcHR5KG5leHRMaW5lKTsgbmV4dExpbmUrKykge1xuICAgIC8vIHRoaXMgd291bGQgYmUgYSBjb2RlIGJsb2NrIG5vcm1hbGx5LCBidXQgYWZ0ZXIgcGFyYWdyYXBoXG4gICAgLy8gaXQncyBjb25zaWRlcmVkIGEgbGF6eSBjb250aW51YXRpb24gcmVnYXJkbGVzcyBvZiB3aGF0J3MgdGhlcmVcbiAgICBpZiAoc3RhdGUuc0NvdW50W25leHRMaW5lXSAtIHN0YXRlLmJsa0luZGVudCA+IDMpIHsgY29udGludWU7IH1cblxuICAgIC8vXG4gICAgLy8gQ2hlY2sgZm9yIHVuZGVybGluZSBpbiBzZXRleHQgaGVhZGVyXG4gICAgLy9cbiAgICBpZiAoc3RhdGUuc0NvdW50W25leHRMaW5lXSA+PSBzdGF0ZS5ibGtJbmRlbnQpIHtcbiAgICAgIHBvcyA9IHN0YXRlLmJNYXJrc1tuZXh0TGluZV0gKyBzdGF0ZS50U2hpZnRbbmV4dExpbmVdO1xuICAgICAgbWF4ID0gc3RhdGUuZU1hcmtzW25leHRMaW5lXTtcblxuICAgICAgaWYgKHBvcyA8IG1heCkge1xuICAgICAgICBtYXJrZXIgPSBzdGF0ZS5zcmMuY2hhckNvZGVBdChwb3MpO1xuXG4gICAgICAgIGlmIChtYXJrZXIgPT09IDB4MkQvKiAtICovIHx8IG1hcmtlciA9PT0gMHgzRC8qID0gKi8pIHtcbiAgICAgICAgICBwb3MgPSBzdGF0ZS5za2lwQ2hhcnMocG9zLCBtYXJrZXIpO1xuICAgICAgICAgIHBvcyA9IHN0YXRlLnNraXBTcGFjZXMocG9zKTtcblxuICAgICAgICAgIGlmIChwb3MgPj0gbWF4KSB7XG4gICAgICAgICAgICBsZXZlbCA9IChtYXJrZXIgPT09IDB4M0QvKiA9ICovID8gMSA6IDIpO1xuICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuXG4gICAgLy8gcXVpcmsgZm9yIGJsb2NrcXVvdGVzLCB0aGlzIGxpbmUgc2hvdWxkIGFscmVhZHkgYmUgY2hlY2tlZCBieSB0aGF0IHJ1bGVcbiAgICBpZiAoc3RhdGUuc0NvdW50W25leHRMaW5lXSA8IDApIHsgY29udGludWU7IH1cblxuICAgIC8vIFNvbWUgdGFncyBjYW4gdGVybWluYXRlIHBhcmFncmFwaCB3aXRob3V0IGVtcHR5IGxpbmUuXG4gICAgdGVybWluYXRlID0gZmFsc2U7XG4gICAgZm9yIChpID0gMCwgbCA9IHRlcm1pbmF0b3JSdWxlcy5sZW5ndGg7IGkgPCBsOyBpKyspIHtcbiAgICAgIGlmICh0ZXJtaW5hdG9yUnVsZXNbaV0oc3RhdGUsIG5leHRMaW5lLCBlbmRMaW5lLCB0cnVlKSkge1xuICAgICAgICB0ZXJtaW5hdGUgPSB0cnVlO1xuICAgICAgICBicmVhaztcbiAgICAgIH1cbiAgICB9XG4gICAgaWYgKHRlcm1pbmF0ZSkgeyBicmVhazsgfVxuICB9XG5cbiAgaWYgKCFsZXZlbCkge1xuICAgIC8vIERpZG4ndCBmaW5kIHZhbGlkIHVuZGVybGluZVxuICAgIHJldHVybiBmYWxzZTtcbiAgfVxuXG4gIGNvbnRlbnQgPSBzdGF0ZS5nZXRMaW5lcyhzdGFydExpbmUsIG5leHRMaW5lLCBzdGF0ZS5ibGtJbmRlbnQsIGZhbHNlKS50cmltKCk7XG5cbiAgc3RhdGUubGluZSA9IG5leHRMaW5lICsgMTtcblxuICB0b2tlbiAgICAgICAgICA9IHN0YXRlLnB1c2goJ2hlYWRpbmdfb3BlbicsICdoJyArIFN0cmluZyhsZXZlbCksIDEpO1xuICB0b2tlbi5tYXJrdXAgICA9IFN0cmluZy5mcm9tQ2hhckNvZGUobWFya2VyKTtcbiAgdG9rZW4ubWFwICAgICAgPSBbIHN0YXJ0TGluZSwgc3RhdGUubGluZSBdO1xuXG4gIHRva2VuICAgICAgICAgID0gc3RhdGUucHVzaCgnaW5saW5lJywgJycsIDApO1xuICB0b2tlbi5jb250ZW50ICA9IGNvbnRlbnQ7XG4gIHRva2VuLm1hcCAgICAgID0gWyBzdGFydExpbmUsIHN0YXRlLmxpbmUgLSAxIF07XG4gIHRva2VuLmNoaWxkcmVuID0gW107XG5cbiAgdG9rZW4gICAgICAgICAgPSBzdGF0ZS5wdXNoKCdoZWFkaW5nX2Nsb3NlJywgJ2gnICsgU3RyaW5nKGxldmVsKSwgLTEpO1xuICB0b2tlbi5tYXJrdXAgICA9IFN0cmluZy5mcm9tQ2hhckNvZGUobWFya2VyKTtcblxuICBzdGF0ZS5wYXJlbnRUeXBlID0gb2xkUGFyZW50VHlwZTtcblxuICByZXR1cm4gdHJ1ZTtcbn07XG4iLCIvLyBMaXN0c1xuXG4ndXNlIHN0cmljdCc7XG5cbnZhciBpc1NwYWNlID0gcmVxdWlyZSgnLi4vY29tbW9uL3V0aWxzJykuaXNTcGFjZTtcblxuXG4vLyBTZWFyY2ggYFstKypdW1xcbiBdYCwgcmV0dXJucyBuZXh0IHBvcyBhZnRlciBtYXJrZXIgb24gc3VjY2Vzc1xuLy8gb3IgLTEgb24gZmFpbC5cbmZ1bmN0aW9uIHNraXBCdWxsZXRMaXN0TWFya2VyKHN0YXRlLCBzdGFydExpbmUpIHtcbiAgdmFyIG1hcmtlciwgcG9zLCBtYXgsIGNoO1xuXG4gIHBvcyA9IHN0YXRlLmJNYXJrc1tzdGFydExpbmVdICsgc3RhdGUudFNoaWZ0W3N0YXJ0TGluZV07XG4gIG1heCA9IHN0YXRlLmVNYXJrc1tzdGFydExpbmVdO1xuXG4gIG1hcmtlciA9IHN0YXRlLnNyYy5jaGFyQ29kZUF0KHBvcysrKTtcbiAgLy8gQ2hlY2sgYnVsbGV0XG4gIGlmIChtYXJrZXIgIT09IDB4MkEvKiAqICovICYmXG4gICAgICBtYXJrZXIgIT09IDB4MkQvKiAtICovICYmXG4gICAgICBtYXJrZXIgIT09IDB4MkIvKiArICovKSB7XG4gICAgcmV0dXJuIC0xO1xuICB9XG5cbiAgaWYgKHBvcyA8IG1heCkge1xuICAgIGNoID0gc3RhdGUuc3JjLmNoYXJDb2RlQXQocG9zKTtcblxuICAgIGlmICghaXNTcGFjZShjaCkpIHtcbiAgICAgIC8vIFwiIC10ZXN0IFwiIC0gaXMgbm90IGEgbGlzdCBpdGVtXG4gICAgICByZXR1cm4gLTE7XG4gICAgfVxuICB9XG5cbiAgcmV0dXJuIHBvcztcbn1cblxuLy8gU2VhcmNoIGBcXGQrWy4pXVtcXG4gXWAsIHJldHVybnMgbmV4dCBwb3MgYWZ0ZXIgbWFya2VyIG9uIHN1Y2Nlc3Ncbi8vIG9yIC0xIG9uIGZhaWwuXG5mdW5jdGlvbiBza2lwT3JkZXJlZExpc3RNYXJrZXIoc3RhdGUsIHN0YXJ0TGluZSkge1xuICB2YXIgY2gsXG4gICAgICBzdGFydCA9IHN0YXRlLmJNYXJrc1tzdGFydExpbmVdICsgc3RhdGUudFNoaWZ0W3N0YXJ0TGluZV0sXG4gICAgICBwb3MgPSBzdGFydCxcbiAgICAgIG1heCA9IHN0YXRlLmVNYXJrc1tzdGFydExpbmVdO1xuXG4gIC8vIExpc3QgbWFya2VyIHNob3VsZCBoYXZlIGF0IGxlYXN0IDIgY2hhcnMgKGRpZ2l0ICsgZG90KVxuICBpZiAocG9zICsgMSA+PSBtYXgpIHsgcmV0dXJuIC0xOyB9XG5cbiAgY2ggPSBzdGF0ZS5zcmMuY2hhckNvZGVBdChwb3MrKyk7XG5cbiAgaWYgKGNoIDwgMHgzMC8qIDAgKi8gfHwgY2ggPiAweDM5LyogOSAqLykgeyByZXR1cm4gLTE7IH1cblxuICBmb3IgKDs7KSB7XG4gICAgLy8gRU9MIC0+IGZhaWxcbiAgICBpZiAocG9zID49IG1heCkgeyByZXR1cm4gLTE7IH1cblxuICAgIGNoID0gc3RhdGUuc3JjLmNoYXJDb2RlQXQocG9zKyspO1xuXG4gICAgaWYgKGNoID49IDB4MzAvKiAwICovICYmIGNoIDw9IDB4MzkvKiA5ICovKSB7XG5cbiAgICAgIC8vIExpc3QgbWFya2VyIHNob3VsZCBoYXZlIG5vIG1vcmUgdGhhbiA5IGRpZ2l0c1xuICAgICAgLy8gKHByZXZlbnRzIGludGVnZXIgb3ZlcmZsb3cgaW4gYnJvd3NlcnMpXG4gICAgICBpZiAocG9zIC0gc3RhcnQgPj0gMTApIHsgcmV0dXJuIC0xOyB9XG5cbiAgICAgIGNvbnRpbnVlO1xuICAgIH1cblxuICAgIC8vIGZvdW5kIHZhbGlkIG1hcmtlclxuICAgIGlmIChjaCA9PT0gMHgyOS8qICkgKi8gfHwgY2ggPT09IDB4MmUvKiAuICovKSB7XG4gICAgICBicmVhaztcbiAgICB9XG5cbiAgICByZXR1cm4gLTE7XG4gIH1cblxuXG4gIGlmIChwb3MgPCBtYXgpIHtcbiAgICBjaCA9IHN0YXRlLnNyYy5jaGFyQ29kZUF0KHBvcyk7XG5cbiAgICBpZiAoIWlzU3BhY2UoY2gpKSB7XG4gICAgICAvLyBcIiAxLnRlc3QgXCIgLSBpcyBub3QgYSBsaXN0IGl0ZW1cbiAgICAgIHJldHVybiAtMTtcbiAgICB9XG4gIH1cbiAgcmV0dXJuIHBvcztcbn1cblxuZnVuY3Rpb24gbWFya1RpZ2h0UGFyYWdyYXBocyhzdGF0ZSwgaWR4KSB7XG4gIHZhciBpLCBsLFxuICAgICAgbGV2ZWwgPSBzdGF0ZS5sZXZlbCArIDI7XG5cbiAgZm9yIChpID0gaWR4ICsgMiwgbCA9IHN0YXRlLnRva2Vucy5sZW5ndGggLSAyOyBpIDwgbDsgaSsrKSB7XG4gICAgaWYgKHN0YXRlLnRva2Vuc1tpXS5sZXZlbCA9PT0gbGV2ZWwgJiYgc3RhdGUudG9rZW5zW2ldLnR5cGUgPT09ICdwYXJhZ3JhcGhfb3BlbicpIHtcbiAgICAgIHN0YXRlLnRva2Vuc1tpICsgMl0uaGlkZGVuID0gdHJ1ZTtcbiAgICAgIHN0YXRlLnRva2Vuc1tpXS5oaWRkZW4gPSB0cnVlO1xuICAgICAgaSArPSAyO1xuICAgIH1cbiAgfVxufVxuXG5cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gbGlzdChzdGF0ZSwgc3RhcnRMaW5lLCBlbmRMaW5lLCBzaWxlbnQpIHtcbiAgdmFyIGNoLFxuICAgICAgY29udGVudFN0YXJ0LFxuICAgICAgaSxcbiAgICAgIGluZGVudCxcbiAgICAgIGluZGVudEFmdGVyTWFya2VyLFxuICAgICAgaW5pdGlhbCxcbiAgICAgIGlzT3JkZXJlZCxcbiAgICAgIGl0ZW1MaW5lcyxcbiAgICAgIGwsXG4gICAgICBsaXN0TGluZXMsXG4gICAgICBsaXN0VG9rSWR4LFxuICAgICAgbWFya2VyQ2hhckNvZGUsXG4gICAgICBtYXJrZXJWYWx1ZSxcbiAgICAgIG1heCxcbiAgICAgIG5leHRMaW5lLFxuICAgICAgb2Zmc2V0LFxuICAgICAgb2xkTGlzdEluZGVudCxcbiAgICAgIG9sZFBhcmVudFR5cGUsXG4gICAgICBvbGRTQ291bnQsXG4gICAgICBvbGRUU2hpZnQsXG4gICAgICBvbGRUaWdodCxcbiAgICAgIHBvcyxcbiAgICAgIHBvc0FmdGVyTWFya2VyLFxuICAgICAgcHJldkVtcHR5RW5kLFxuICAgICAgc3RhcnQsXG4gICAgICB0ZXJtaW5hdGUsXG4gICAgICB0ZXJtaW5hdG9yUnVsZXMsXG4gICAgICB0b2tlbixcbiAgICAgIGlzVGVybWluYXRpbmdQYXJhZ3JhcGggPSBmYWxzZSxcbiAgICAgIHRpZ2h0ID0gdHJ1ZTtcblxuICAvLyBpZiBpdCdzIGluZGVudGVkIG1vcmUgdGhhbiAzIHNwYWNlcywgaXQgc2hvdWxkIGJlIGEgY29kZSBibG9ja1xuICBpZiAoc3RhdGUuc0NvdW50W3N0YXJ0TGluZV0gLSBzdGF0ZS5ibGtJbmRlbnQgPj0gNCkgeyByZXR1cm4gZmFsc2U7IH1cblxuICAvLyBTcGVjaWFsIGNhc2U6XG4gIC8vICAtIGl0ZW0gMVxuICAvLyAgIC0gaXRlbSAyXG4gIC8vICAgIC0gaXRlbSAzXG4gIC8vICAgICAtIGl0ZW0gNFxuICAvLyAgICAgIC0gdGhpcyBvbmUgaXMgYSBwYXJhZ3JhcGggY29udGludWF0aW9uXG4gIGlmIChzdGF0ZS5saXN0SW5kZW50ID49IDAgJiZcbiAgICAgIHN0YXRlLnNDb3VudFtzdGFydExpbmVdIC0gc3RhdGUubGlzdEluZGVudCA+PSA0ICYmXG4gICAgICBzdGF0ZS5zQ291bnRbc3RhcnRMaW5lXSA8IHN0YXRlLmJsa0luZGVudCkge1xuICAgIHJldHVybiBmYWxzZTtcbiAgfVxuXG4gIC8vIGxpbWl0IGNvbmRpdGlvbnMgd2hlbiBsaXN0IGNhbiBpbnRlcnJ1cHRcbiAgLy8gYSBwYXJhZ3JhcGggKHZhbGlkYXRpb24gbW9kZSBvbmx5KVxuICBpZiAoc2lsZW50ICYmIHN0YXRlLnBhcmVudFR5cGUgPT09ICdwYXJhZ3JhcGgnKSB7XG4gICAgLy8gTmV4dCBsaXN0IGl0ZW0gc2hvdWxkIHN0aWxsIHRlcm1pbmF0ZSBwcmV2aW91cyBsaXN0IGl0ZW07XG4gICAgLy9cbiAgICAvLyBUaGlzIGNvZGUgY2FuIGZhaWwgaWYgcGx1Z2lucyB1c2UgYmxrSW5kZW50IGFzIHdlbGwgYXMgbGlzdHMsXG4gICAgLy8gYnV0IEkgaG9wZSB0aGUgc3BlYyBnZXRzIGZpeGVkIGxvbmcgYmVmb3JlIHRoYXQgaGFwcGVucy5cbiAgICAvL1xuICAgIGlmIChzdGF0ZS50U2hpZnRbc3RhcnRMaW5lXSA+PSBzdGF0ZS5ibGtJbmRlbnQpIHtcbiAgICAgIGlzVGVybWluYXRpbmdQYXJhZ3JhcGggPSB0cnVlO1xuICAgIH1cbiAgfVxuXG4gIC8vIERldGVjdCBsaXN0IHR5cGUgYW5kIHBvc2l0aW9uIGFmdGVyIG1hcmtlclxuICBpZiAoKHBvc0FmdGVyTWFya2VyID0gc2tpcE9yZGVyZWRMaXN0TWFya2VyKHN0YXRlLCBzdGFydExpbmUpKSA+PSAwKSB7XG4gICAgaXNPcmRlcmVkID0gdHJ1ZTtcbiAgICBzdGFydCA9IHN0YXRlLmJNYXJrc1tzdGFydExpbmVdICsgc3RhdGUudFNoaWZ0W3N0YXJ0TGluZV07XG4gICAgbWFya2VyVmFsdWUgPSBOdW1iZXIoc3RhdGUuc3JjLnN1YnN0cihzdGFydCwgcG9zQWZ0ZXJNYXJrZXIgLSBzdGFydCAtIDEpKTtcblxuICAgIC8vIElmIHdlJ3JlIHN0YXJ0aW5nIGEgbmV3IG9yZGVyZWQgbGlzdCByaWdodCBhZnRlclxuICAgIC8vIGEgcGFyYWdyYXBoLCBpdCBzaG91bGQgc3RhcnQgd2l0aCAxLlxuICAgIGlmIChpc1Rlcm1pbmF0aW5nUGFyYWdyYXBoICYmIG1hcmtlclZhbHVlICE9PSAxKSByZXR1cm4gZmFsc2U7XG5cbiAgfSBlbHNlIGlmICgocG9zQWZ0ZXJNYXJrZXIgPSBza2lwQnVsbGV0TGlzdE1hcmtlcihzdGF0ZSwgc3RhcnRMaW5lKSkgPj0gMCkge1xuICAgIGlzT3JkZXJlZCA9IGZhbHNlO1xuXG4gIH0gZWxzZSB7XG4gICAgcmV0dXJuIGZhbHNlO1xuICB9XG5cbiAgLy8gSWYgd2UncmUgc3RhcnRpbmcgYSBuZXcgdW5vcmRlcmVkIGxpc3QgcmlnaHQgYWZ0ZXJcbiAgLy8gYSBwYXJhZ3JhcGgsIGZpcnN0IGxpbmUgc2hvdWxkIG5vdCBiZSBlbXB0eS5cbiAgaWYgKGlzVGVybWluYXRpbmdQYXJhZ3JhcGgpIHtcbiAgICBpZiAoc3RhdGUuc2tpcFNwYWNlcyhwb3NBZnRlck1hcmtlcikgPj0gc3RhdGUuZU1hcmtzW3N0YXJ0TGluZV0pIHJldHVybiBmYWxzZTtcbiAgfVxuXG4gIC8vIFdlIHNob3VsZCB0ZXJtaW5hdGUgbGlzdCBvbiBzdHlsZSBjaGFuZ2UuIFJlbWVtYmVyIGZpcnN0IG9uZSB0byBjb21wYXJlLlxuICBtYXJrZXJDaGFyQ29kZSA9IHN0YXRlLnNyYy5jaGFyQ29kZUF0KHBvc0FmdGVyTWFya2VyIC0gMSk7XG5cbiAgLy8gRm9yIHZhbGlkYXRpb24gbW9kZSB3ZSBjYW4gdGVybWluYXRlIGltbWVkaWF0ZWx5XG4gIGlmIChzaWxlbnQpIHsgcmV0dXJuIHRydWU7IH1cblxuICAvLyBTdGFydCBsaXN0XG4gIGxpc3RUb2tJZHggPSBzdGF0ZS50b2tlbnMubGVuZ3RoO1xuXG4gIGlmIChpc09yZGVyZWQpIHtcbiAgICB0b2tlbiAgICAgICA9IHN0YXRlLnB1c2goJ29yZGVyZWRfbGlzdF9vcGVuJywgJ29sJywgMSk7XG4gICAgaWYgKG1hcmtlclZhbHVlICE9PSAxKSB7XG4gICAgICB0b2tlbi5hdHRycyA9IFsgWyAnc3RhcnQnLCBtYXJrZXJWYWx1ZSBdIF07XG4gICAgfVxuXG4gIH0gZWxzZSB7XG4gICAgdG9rZW4gICAgICAgPSBzdGF0ZS5wdXNoKCdidWxsZXRfbGlzdF9vcGVuJywgJ3VsJywgMSk7XG4gIH1cblxuICB0b2tlbi5tYXAgICAgPSBsaXN0TGluZXMgPSBbIHN0YXJ0TGluZSwgMCBdO1xuICB0b2tlbi5tYXJrdXAgPSBTdHJpbmcuZnJvbUNoYXJDb2RlKG1hcmtlckNoYXJDb2RlKTtcblxuICAvL1xuICAvLyBJdGVyYXRlIGxpc3QgaXRlbXNcbiAgLy9cblxuICBuZXh0TGluZSA9IHN0YXJ0TGluZTtcbiAgcHJldkVtcHR5RW5kID0gZmFsc2U7XG4gIHRlcm1pbmF0b3JSdWxlcyA9IHN0YXRlLm1kLmJsb2NrLnJ1bGVyLmdldFJ1bGVzKCdsaXN0Jyk7XG5cbiAgb2xkUGFyZW50VHlwZSA9IHN0YXRlLnBhcmVudFR5cGU7XG4gIHN0YXRlLnBhcmVudFR5cGUgPSAnbGlzdCc7XG5cbiAgd2hpbGUgKG5leHRMaW5lIDwgZW5kTGluZSkge1xuICAgIHBvcyA9IHBvc0FmdGVyTWFya2VyO1xuICAgIG1heCA9IHN0YXRlLmVNYXJrc1tuZXh0TGluZV07XG5cbiAgICBpbml0aWFsID0gb2Zmc2V0ID0gc3RhdGUuc0NvdW50W25leHRMaW5lXSArIHBvc0FmdGVyTWFya2VyIC0gKHN0YXRlLmJNYXJrc1tzdGFydExpbmVdICsgc3RhdGUudFNoaWZ0W3N0YXJ0TGluZV0pO1xuXG4gICAgd2hpbGUgKHBvcyA8IG1heCkge1xuICAgICAgY2ggPSBzdGF0ZS5zcmMuY2hhckNvZGVBdChwb3MpO1xuXG4gICAgICBpZiAoY2ggPT09IDB4MDkpIHtcbiAgICAgICAgb2Zmc2V0ICs9IDQgLSAob2Zmc2V0ICsgc3RhdGUuYnNDb3VudFtuZXh0TGluZV0pICUgNDtcbiAgICAgIH0gZWxzZSBpZiAoY2ggPT09IDB4MjApIHtcbiAgICAgICAgb2Zmc2V0Kys7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBicmVhaztcbiAgICAgIH1cblxuICAgICAgcG9zKys7XG4gICAgfVxuXG4gICAgY29udGVudFN0YXJ0ID0gcG9zO1xuXG4gICAgaWYgKGNvbnRlbnRTdGFydCA+PSBtYXgpIHtcbiAgICAgIC8vIHRyaW1taW5nIHNwYWNlIGluIFwiLSAgICBcXG4gIDNcIiBjYXNlLCBpbmRlbnQgaXMgMSBoZXJlXG4gICAgICBpbmRlbnRBZnRlck1hcmtlciA9IDE7XG4gICAgfSBlbHNlIHtcbiAgICAgIGluZGVudEFmdGVyTWFya2VyID0gb2Zmc2V0IC0gaW5pdGlhbDtcbiAgICB9XG5cbiAgICAvLyBJZiB3ZSBoYXZlIG1vcmUgdGhhbiA0IHNwYWNlcywgdGhlIGluZGVudCBpcyAxXG4gICAgLy8gKHRoZSByZXN0IGlzIGp1c3QgaW5kZW50ZWQgY29kZSBibG9jaylcbiAgICBpZiAoaW5kZW50QWZ0ZXJNYXJrZXIgPiA0KSB7IGluZGVudEFmdGVyTWFya2VyID0gMTsgfVxuXG4gICAgLy8gXCIgIC0gIHRlc3RcIlxuICAgIC8vICBeXl5eXiAtIGNhbGN1bGF0aW5nIHRvdGFsIGxlbmd0aCBvZiB0aGlzIHRoaW5nXG4gICAgaW5kZW50ID0gaW5pdGlhbCArIGluZGVudEFmdGVyTWFya2VyO1xuXG4gICAgLy8gUnVuIHN1YnBhcnNlciAmIHdyaXRlIHRva2Vuc1xuICAgIHRva2VuICAgICAgICA9IHN0YXRlLnB1c2goJ2xpc3RfaXRlbV9vcGVuJywgJ2xpJywgMSk7XG4gICAgdG9rZW4ubWFya3VwID0gU3RyaW5nLmZyb21DaGFyQ29kZShtYXJrZXJDaGFyQ29kZSk7XG4gICAgdG9rZW4ubWFwICAgID0gaXRlbUxpbmVzID0gWyBzdGFydExpbmUsIDAgXTtcblxuICAgIC8vIGNoYW5nZSBjdXJyZW50IHN0YXRlLCB0aGVuIHJlc3RvcmUgaXQgYWZ0ZXIgcGFyc2VyIHN1YmNhbGxcbiAgICBvbGRUaWdodCA9IHN0YXRlLnRpZ2h0O1xuICAgIG9sZFRTaGlmdCA9IHN0YXRlLnRTaGlmdFtzdGFydExpbmVdO1xuICAgIG9sZFNDb3VudCA9IHN0YXRlLnNDb3VudFtzdGFydExpbmVdO1xuXG4gICAgLy8gIC0gZXhhbXBsZSBsaXN0XG4gICAgLy8gXiBsaXN0SW5kZW50IHBvc2l0aW9uIHdpbGwgYmUgaGVyZVxuICAgIC8vICAgXiBibGtJbmRlbnQgcG9zaXRpb24gd2lsbCBiZSBoZXJlXG4gICAgLy9cbiAgICBvbGRMaXN0SW5kZW50ID0gc3RhdGUubGlzdEluZGVudDtcbiAgICBzdGF0ZS5saXN0SW5kZW50ID0gc3RhdGUuYmxrSW5kZW50O1xuICAgIHN0YXRlLmJsa0luZGVudCA9IGluZGVudDtcblxuICAgIHN0YXRlLnRpZ2h0ID0gdHJ1ZTtcbiAgICBzdGF0ZS50U2hpZnRbc3RhcnRMaW5lXSA9IGNvbnRlbnRTdGFydCAtIHN0YXRlLmJNYXJrc1tzdGFydExpbmVdO1xuICAgIHN0YXRlLnNDb3VudFtzdGFydExpbmVdID0gb2Zmc2V0O1xuXG4gICAgaWYgKGNvbnRlbnRTdGFydCA+PSBtYXggJiYgc3RhdGUuaXNFbXB0eShzdGFydExpbmUgKyAxKSkge1xuICAgICAgLy8gd29ya2Fyb3VuZCBmb3IgdGhpcyBjYXNlXG4gICAgICAvLyAobGlzdCBpdGVtIGlzIGVtcHR5LCBsaXN0IHRlcm1pbmF0ZXMgYmVmb3JlIFwiZm9vXCIpOlxuICAgICAgLy8gfn5+fn5+fn5cbiAgICAgIC8vICAgLVxuICAgICAgLy9cbiAgICAgIC8vICAgICBmb29cbiAgICAgIC8vIH5+fn5+fn5+XG4gICAgICBzdGF0ZS5saW5lID0gTWF0aC5taW4oc3RhdGUubGluZSArIDIsIGVuZExpbmUpO1xuICAgIH0gZWxzZSB7XG4gICAgICBzdGF0ZS5tZC5ibG9jay50b2tlbml6ZShzdGF0ZSwgc3RhcnRMaW5lLCBlbmRMaW5lLCB0cnVlKTtcbiAgICB9XG5cbiAgICAvLyBJZiBhbnkgb2YgbGlzdCBpdGVtIGlzIHRpZ2h0LCBtYXJrIGxpc3QgYXMgdGlnaHRcbiAgICBpZiAoIXN0YXRlLnRpZ2h0IHx8IHByZXZFbXB0eUVuZCkge1xuICAgICAgdGlnaHQgPSBmYWxzZTtcbiAgICB9XG4gICAgLy8gSXRlbSBiZWNvbWUgbG9vc2UgaWYgZmluaXNoIHdpdGggZW1wdHkgbGluZSxcbiAgICAvLyBidXQgd2Ugc2hvdWxkIGZpbHRlciBsYXN0IGVsZW1lbnQsIGJlY2F1c2UgaXQgbWVhbnMgbGlzdCBmaW5pc2hcbiAgICBwcmV2RW1wdHlFbmQgPSAoc3RhdGUubGluZSAtIHN0YXJ0TGluZSkgPiAxICYmIHN0YXRlLmlzRW1wdHkoc3RhdGUubGluZSAtIDEpO1xuXG4gICAgc3RhdGUuYmxrSW5kZW50ID0gc3RhdGUubGlzdEluZGVudDtcbiAgICBzdGF0ZS5saXN0SW5kZW50ID0gb2xkTGlzdEluZGVudDtcbiAgICBzdGF0ZS50U2hpZnRbc3RhcnRMaW5lXSA9IG9sZFRTaGlmdDtcbiAgICBzdGF0ZS5zQ291bnRbc3RhcnRMaW5lXSA9IG9sZFNDb3VudDtcbiAgICBzdGF0ZS50aWdodCA9IG9sZFRpZ2h0O1xuXG4gICAgdG9rZW4gICAgICAgID0gc3RhdGUucHVzaCgnbGlzdF9pdGVtX2Nsb3NlJywgJ2xpJywgLTEpO1xuICAgIHRva2VuLm1hcmt1cCA9IFN0cmluZy5mcm9tQ2hhckNvZGUobWFya2VyQ2hhckNvZGUpO1xuXG4gICAgbmV4dExpbmUgPSBzdGFydExpbmUgPSBzdGF0ZS5saW5lO1xuICAgIGl0ZW1MaW5lc1sxXSA9IG5leHRMaW5lO1xuICAgIGNvbnRlbnRTdGFydCA9IHN0YXRlLmJNYXJrc1tzdGFydExpbmVdO1xuXG4gICAgaWYgKG5leHRMaW5lID49IGVuZExpbmUpIHsgYnJlYWs7IH1cblxuICAgIC8vXG4gICAgLy8gVHJ5IHRvIGNoZWNrIGlmIGxpc3QgaXMgdGVybWluYXRlZCBvciBjb250aW51ZWQuXG4gICAgLy9cbiAgICBpZiAoc3RhdGUuc0NvdW50W25leHRMaW5lXSA8IHN0YXRlLmJsa0luZGVudCkgeyBicmVhazsgfVxuXG4gICAgLy8gaWYgaXQncyBpbmRlbnRlZCBtb3JlIHRoYW4gMyBzcGFjZXMsIGl0IHNob3VsZCBiZSBhIGNvZGUgYmxvY2tcbiAgICBpZiAoc3RhdGUuc0NvdW50W3N0YXJ0TGluZV0gLSBzdGF0ZS5ibGtJbmRlbnQgPj0gNCkgeyBicmVhazsgfVxuXG4gICAgLy8gZmFpbCBpZiB0ZXJtaW5hdGluZyBibG9jayBmb3VuZFxuICAgIHRlcm1pbmF0ZSA9IGZhbHNlO1xuICAgIGZvciAoaSA9IDAsIGwgPSB0ZXJtaW5hdG9yUnVsZXMubGVuZ3RoOyBpIDwgbDsgaSsrKSB7XG4gICAgICBpZiAodGVybWluYXRvclJ1bGVzW2ldKHN0YXRlLCBuZXh0TGluZSwgZW5kTGluZSwgdHJ1ZSkpIHtcbiAgICAgICAgdGVybWluYXRlID0gdHJ1ZTtcbiAgICAgICAgYnJlYWs7XG4gICAgICB9XG4gICAgfVxuICAgIGlmICh0ZXJtaW5hdGUpIHsgYnJlYWs7IH1cblxuICAgIC8vIGZhaWwgaWYgbGlzdCBoYXMgYW5vdGhlciB0eXBlXG4gICAgaWYgKGlzT3JkZXJlZCkge1xuICAgICAgcG9zQWZ0ZXJNYXJrZXIgPSBza2lwT3JkZXJlZExpc3RNYXJrZXIoc3RhdGUsIG5leHRMaW5lKTtcbiAgICAgIGlmIChwb3NBZnRlck1hcmtlciA8IDApIHsgYnJlYWs7IH1cbiAgICB9IGVsc2Uge1xuICAgICAgcG9zQWZ0ZXJNYXJrZXIgPSBza2lwQnVsbGV0TGlzdE1hcmtlcihzdGF0ZSwgbmV4dExpbmUpO1xuICAgICAgaWYgKHBvc0FmdGVyTWFya2VyIDwgMCkgeyBicmVhazsgfVxuICAgIH1cblxuICAgIGlmIChtYXJrZXJDaGFyQ29kZSAhPT0gc3RhdGUuc3JjLmNoYXJDb2RlQXQocG9zQWZ0ZXJNYXJrZXIgLSAxKSkgeyBicmVhazsgfVxuICB9XG5cbiAgLy8gRmluYWxpemUgbGlzdFxuICBpZiAoaXNPcmRlcmVkKSB7XG4gICAgdG9rZW4gPSBzdGF0ZS5wdXNoKCdvcmRlcmVkX2xpc3RfY2xvc2UnLCAnb2wnLCAtMSk7XG4gIH0gZWxzZSB7XG4gICAgdG9rZW4gPSBzdGF0ZS5wdXNoKCdidWxsZXRfbGlzdF9jbG9zZScsICd1bCcsIC0xKTtcbiAgfVxuICB0b2tlbi5tYXJrdXAgPSBTdHJpbmcuZnJvbUNoYXJDb2RlKG1hcmtlckNoYXJDb2RlKTtcblxuICBsaXN0TGluZXNbMV0gPSBuZXh0TGluZTtcbiAgc3RhdGUubGluZSA9IG5leHRMaW5lO1xuXG4gIHN0YXRlLnBhcmVudFR5cGUgPSBvbGRQYXJlbnRUeXBlO1xuXG4gIC8vIG1hcmsgcGFyYWdyYXBocyB0aWdodCBpZiBuZWVkZWRcbiAgaWYgKHRpZ2h0KSB7XG4gICAgbWFya1RpZ2h0UGFyYWdyYXBocyhzdGF0ZSwgbGlzdFRva0lkeCk7XG4gIH1cblxuICByZXR1cm4gdHJ1ZTtcbn07XG4iLCIvLyBQYXJhZ3JhcGhcblxuJ3VzZSBzdHJpY3QnO1xuXG5cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gcGFyYWdyYXBoKHN0YXRlLCBzdGFydExpbmUvKiwgZW5kTGluZSovKSB7XG4gIHZhciBjb250ZW50LCB0ZXJtaW5hdGUsIGksIGwsIHRva2VuLCBvbGRQYXJlbnRUeXBlLFxuICAgICAgbmV4dExpbmUgPSBzdGFydExpbmUgKyAxLFxuICAgICAgdGVybWluYXRvclJ1bGVzID0gc3RhdGUubWQuYmxvY2sucnVsZXIuZ2V0UnVsZXMoJ3BhcmFncmFwaCcpLFxuICAgICAgZW5kTGluZSA9IHN0YXRlLmxpbmVNYXg7XG5cbiAgb2xkUGFyZW50VHlwZSA9IHN0YXRlLnBhcmVudFR5cGU7XG4gIHN0YXRlLnBhcmVudFR5cGUgPSAncGFyYWdyYXBoJztcblxuICAvLyBqdW1wIGxpbmUtYnktbGluZSB1bnRpbCBlbXB0eSBvbmUgb3IgRU9GXG4gIGZvciAoOyBuZXh0TGluZSA8IGVuZExpbmUgJiYgIXN0YXRlLmlzRW1wdHkobmV4dExpbmUpOyBuZXh0TGluZSsrKSB7XG4gICAgLy8gdGhpcyB3b3VsZCBiZSBhIGNvZGUgYmxvY2sgbm9ybWFsbHksIGJ1dCBhZnRlciBwYXJhZ3JhcGhcbiAgICAvLyBpdCdzIGNvbnNpZGVyZWQgYSBsYXp5IGNvbnRpbnVhdGlvbiByZWdhcmRsZXNzIG9mIHdoYXQncyB0aGVyZVxuICAgIGlmIChzdGF0ZS5zQ291bnRbbmV4dExpbmVdIC0gc3RhdGUuYmxrSW5kZW50ID4gMykgeyBjb250aW51ZTsgfVxuXG4gICAgLy8gcXVpcmsgZm9yIGJsb2NrcXVvdGVzLCB0aGlzIGxpbmUgc2hvdWxkIGFscmVhZHkgYmUgY2hlY2tlZCBieSB0aGF0IHJ1bGVcbiAgICBpZiAoc3RhdGUuc0NvdW50W25leHRMaW5lXSA8IDApIHsgY29udGludWU7IH1cblxuICAgIC8vIFNvbWUgdGFncyBjYW4gdGVybWluYXRlIHBhcmFncmFwaCB3aXRob3V0IGVtcHR5IGxpbmUuXG4gICAgdGVybWluYXRlID0gZmFsc2U7XG4gICAgZm9yIChpID0gMCwgbCA9IHRlcm1pbmF0b3JSdWxlcy5sZW5ndGg7IGkgPCBsOyBpKyspIHtcbiAgICAgIGlmICh0ZXJtaW5hdG9yUnVsZXNbaV0oc3RhdGUsIG5leHRMaW5lLCBlbmRMaW5lLCB0cnVlKSkge1xuICAgICAgICB0ZXJtaW5hdGUgPSB0cnVlO1xuICAgICAgICBicmVhaztcbiAgICAgIH1cbiAgICB9XG4gICAgaWYgKHRlcm1pbmF0ZSkgeyBicmVhazsgfVxuICB9XG5cbiAgY29udGVudCA9IHN0YXRlLmdldExpbmVzKHN0YXJ0TGluZSwgbmV4dExpbmUsIHN0YXRlLmJsa0luZGVudCwgZmFsc2UpLnRyaW0oKTtcblxuICBzdGF0ZS5saW5lID0gbmV4dExpbmU7XG5cbiAgdG9rZW4gICAgICAgICAgPSBzdGF0ZS5wdXNoKCdwYXJhZ3JhcGhfb3BlbicsICdwJywgMSk7XG4gIHRva2VuLm1hcCAgICAgID0gWyBzdGFydExpbmUsIHN0YXRlLmxpbmUgXTtcblxuICB0b2tlbiAgICAgICAgICA9IHN0YXRlLnB1c2goJ2lubGluZScsICcnLCAwKTtcbiAgdG9rZW4uY29udGVudCAgPSBjb250ZW50O1xuICB0b2tlbi5tYXAgICAgICA9IFsgc3RhcnRMaW5lLCBzdGF0ZS5saW5lIF07XG4gIHRva2VuLmNoaWxkcmVuID0gW107XG5cbiAgdG9rZW4gICAgICAgICAgPSBzdGF0ZS5wdXNoKCdwYXJhZ3JhcGhfY2xvc2UnLCAncCcsIC0xKTtcblxuICBzdGF0ZS5wYXJlbnRUeXBlID0gb2xkUGFyZW50VHlwZTtcblxuICByZXR1cm4gdHJ1ZTtcbn07XG4iLCIndXNlIHN0cmljdCc7XG5cblxudmFyIG5vcm1hbGl6ZVJlZmVyZW5jZSAgID0gcmVxdWlyZSgnLi4vY29tbW9uL3V0aWxzJykubm9ybWFsaXplUmVmZXJlbmNlO1xudmFyIGlzU3BhY2UgICAgICAgICAgICAgID0gcmVxdWlyZSgnLi4vY29tbW9uL3V0aWxzJykuaXNTcGFjZTtcblxuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIHJlZmVyZW5jZShzdGF0ZSwgc3RhcnRMaW5lLCBfZW5kTGluZSwgc2lsZW50KSB7XG4gIHZhciBjaCxcbiAgICAgIGRlc3RFbmRQb3MsXG4gICAgICBkZXN0RW5kTGluZU5vLFxuICAgICAgZW5kTGluZSxcbiAgICAgIGhyZWYsXG4gICAgICBpLFxuICAgICAgbCxcbiAgICAgIGxhYmVsLFxuICAgICAgbGFiZWxFbmQsXG4gICAgICBvbGRQYXJlbnRUeXBlLFxuICAgICAgcmVzLFxuICAgICAgc3RhcnQsXG4gICAgICBzdHIsXG4gICAgICB0ZXJtaW5hdGUsXG4gICAgICB0ZXJtaW5hdG9yUnVsZXMsXG4gICAgICB0aXRsZSxcbiAgICAgIGxpbmVzID0gMCxcbiAgICAgIHBvcyA9IHN0YXRlLmJNYXJrc1tzdGFydExpbmVdICsgc3RhdGUudFNoaWZ0W3N0YXJ0TGluZV0sXG4gICAgICBtYXggPSBzdGF0ZS5lTWFya3Nbc3RhcnRMaW5lXSxcbiAgICAgIG5leHRMaW5lID0gc3RhcnRMaW5lICsgMTtcblxuICAvLyBpZiBpdCdzIGluZGVudGVkIG1vcmUgdGhhbiAzIHNwYWNlcywgaXQgc2hvdWxkIGJlIGEgY29kZSBibG9ja1xuICBpZiAoc3RhdGUuc0NvdW50W3N0YXJ0TGluZV0gLSBzdGF0ZS5ibGtJbmRlbnQgPj0gNCkgeyByZXR1cm4gZmFsc2U7IH1cblxuICBpZiAoc3RhdGUuc3JjLmNoYXJDb2RlQXQocG9zKSAhPT0gMHg1Qi8qIFsgKi8pIHsgcmV0dXJuIGZhbHNlOyB9XG5cbiAgLy8gU2ltcGxlIGNoZWNrIHRvIHF1aWNrbHkgaW50ZXJydXB0IHNjYW4gb24gW2xpbmtdKHVybCkgYXQgdGhlIHN0YXJ0IG9mIGxpbmUuXG4gIC8vIENhbiBiZSB1c2VmdWwgb24gcHJhY3RpY2U6IGh0dHBzOi8vZ2l0aHViLmNvbS9tYXJrZG93bi1pdC9tYXJrZG93bi1pdC9pc3N1ZXMvNTRcbiAgd2hpbGUgKCsrcG9zIDwgbWF4KSB7XG4gICAgaWYgKHN0YXRlLnNyYy5jaGFyQ29kZUF0KHBvcykgPT09IDB4NUQgLyogXSAqLyAmJlxuICAgICAgICBzdGF0ZS5zcmMuY2hhckNvZGVBdChwb3MgLSAxKSAhPT0gMHg1Qy8qIFxcICovKSB7XG4gICAgICBpZiAocG9zICsgMSA9PT0gbWF4KSB7IHJldHVybiBmYWxzZTsgfVxuICAgICAgaWYgKHN0YXRlLnNyYy5jaGFyQ29kZUF0KHBvcyArIDEpICE9PSAweDNBLyogOiAqLykgeyByZXR1cm4gZmFsc2U7IH1cbiAgICAgIGJyZWFrO1xuICAgIH1cbiAgfVxuXG4gIGVuZExpbmUgPSBzdGF0ZS5saW5lTWF4O1xuXG4gIC8vIGp1bXAgbGluZS1ieS1saW5lIHVudGlsIGVtcHR5IG9uZSBvciBFT0ZcbiAgdGVybWluYXRvclJ1bGVzID0gc3RhdGUubWQuYmxvY2sucnVsZXIuZ2V0UnVsZXMoJ3JlZmVyZW5jZScpO1xuXG4gIG9sZFBhcmVudFR5cGUgPSBzdGF0ZS5wYXJlbnRUeXBlO1xuICBzdGF0ZS5wYXJlbnRUeXBlID0gJ3JlZmVyZW5jZSc7XG5cbiAgZm9yICg7IG5leHRMaW5lIDwgZW5kTGluZSAmJiAhc3RhdGUuaXNFbXB0eShuZXh0TGluZSk7IG5leHRMaW5lKyspIHtcbiAgICAvLyB0aGlzIHdvdWxkIGJlIGEgY29kZSBibG9jayBub3JtYWxseSwgYnV0IGFmdGVyIHBhcmFncmFwaFxuICAgIC8vIGl0J3MgY29uc2lkZXJlZCBhIGxhenkgY29udGludWF0aW9uIHJlZ2FyZGxlc3Mgb2Ygd2hhdCdzIHRoZXJlXG4gICAgaWYgKHN0YXRlLnNDb3VudFtuZXh0TGluZV0gLSBzdGF0ZS5ibGtJbmRlbnQgPiAzKSB7IGNvbnRpbnVlOyB9XG5cbiAgICAvLyBxdWlyayBmb3IgYmxvY2txdW90ZXMsIHRoaXMgbGluZSBzaG91bGQgYWxyZWFkeSBiZSBjaGVja2VkIGJ5IHRoYXQgcnVsZVxuICAgIGlmIChzdGF0ZS5zQ291bnRbbmV4dExpbmVdIDwgMCkgeyBjb250aW51ZTsgfVxuXG4gICAgLy8gU29tZSB0YWdzIGNhbiB0ZXJtaW5hdGUgcGFyYWdyYXBoIHdpdGhvdXQgZW1wdHkgbGluZS5cbiAgICB0ZXJtaW5hdGUgPSBmYWxzZTtcbiAgICBmb3IgKGkgPSAwLCBsID0gdGVybWluYXRvclJ1bGVzLmxlbmd0aDsgaSA8IGw7IGkrKykge1xuICAgICAgaWYgKHRlcm1pbmF0b3JSdWxlc1tpXShzdGF0ZSwgbmV4dExpbmUsIGVuZExpbmUsIHRydWUpKSB7XG4gICAgICAgIHRlcm1pbmF0ZSA9IHRydWU7XG4gICAgICAgIGJyZWFrO1xuICAgICAgfVxuICAgIH1cbiAgICBpZiAodGVybWluYXRlKSB7IGJyZWFrOyB9XG4gIH1cblxuICBzdHIgPSBzdGF0ZS5nZXRMaW5lcyhzdGFydExpbmUsIG5leHRMaW5lLCBzdGF0ZS5ibGtJbmRlbnQsIGZhbHNlKS50cmltKCk7XG4gIG1heCA9IHN0ci5sZW5ndGg7XG5cbiAgZm9yIChwb3MgPSAxOyBwb3MgPCBtYXg7IHBvcysrKSB7XG4gICAgY2ggPSBzdHIuY2hhckNvZGVBdChwb3MpO1xuICAgIGlmIChjaCA9PT0gMHg1QiAvKiBbICovKSB7XG4gICAgICByZXR1cm4gZmFsc2U7XG4gICAgfSBlbHNlIGlmIChjaCA9PT0gMHg1RCAvKiBdICovKSB7XG4gICAgICBsYWJlbEVuZCA9IHBvcztcbiAgICAgIGJyZWFrO1xuICAgIH0gZWxzZSBpZiAoY2ggPT09IDB4MEEgLyogXFxuICovKSB7XG4gICAgICBsaW5lcysrO1xuICAgIH0gZWxzZSBpZiAoY2ggPT09IDB4NUMgLyogXFwgKi8pIHtcbiAgICAgIHBvcysrO1xuICAgICAgaWYgKHBvcyA8IG1heCAmJiBzdHIuY2hhckNvZGVBdChwb3MpID09PSAweDBBKSB7XG4gICAgICAgIGxpbmVzKys7XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgaWYgKGxhYmVsRW5kIDwgMCB8fCBzdHIuY2hhckNvZGVBdChsYWJlbEVuZCArIDEpICE9PSAweDNBLyogOiAqLykgeyByZXR1cm4gZmFsc2U7IH1cblxuICAvLyBbbGFiZWxdOiAgIGRlc3RpbmF0aW9uICAgJ3RpdGxlJ1xuICAvLyAgICAgICAgIF5eXiBza2lwIG9wdGlvbmFsIHdoaXRlc3BhY2UgaGVyZVxuICBmb3IgKHBvcyA9IGxhYmVsRW5kICsgMjsgcG9zIDwgbWF4OyBwb3MrKykge1xuICAgIGNoID0gc3RyLmNoYXJDb2RlQXQocG9zKTtcbiAgICBpZiAoY2ggPT09IDB4MEEpIHtcbiAgICAgIGxpbmVzKys7XG4gICAgfSBlbHNlIGlmIChpc1NwYWNlKGNoKSkge1xuICAgICAgLyplc2xpbnQgbm8tZW1wdHk6MCovXG4gICAgfSBlbHNlIHtcbiAgICAgIGJyZWFrO1xuICAgIH1cbiAgfVxuXG4gIC8vIFtsYWJlbF06ICAgZGVzdGluYXRpb24gICAndGl0bGUnXG4gIC8vICAgICAgICAgICAgXl5eXl5eXl5eXl4gcGFyc2UgdGhpc1xuICByZXMgPSBzdGF0ZS5tZC5oZWxwZXJzLnBhcnNlTGlua0Rlc3RpbmF0aW9uKHN0ciwgcG9zLCBtYXgpO1xuICBpZiAoIXJlcy5vaykgeyByZXR1cm4gZmFsc2U7IH1cblxuICBocmVmID0gc3RhdGUubWQubm9ybWFsaXplTGluayhyZXMuc3RyKTtcbiAgaWYgKCFzdGF0ZS5tZC52YWxpZGF0ZUxpbmsoaHJlZikpIHsgcmV0dXJuIGZhbHNlOyB9XG5cbiAgcG9zID0gcmVzLnBvcztcbiAgbGluZXMgKz0gcmVzLmxpbmVzO1xuXG4gIC8vIHNhdmUgY3Vyc29yIHN0YXRlLCB3ZSBjb3VsZCByZXF1aXJlIHRvIHJvbGxiYWNrIGxhdGVyXG4gIGRlc3RFbmRQb3MgPSBwb3M7XG4gIGRlc3RFbmRMaW5lTm8gPSBsaW5lcztcblxuICAvLyBbbGFiZWxdOiAgIGRlc3RpbmF0aW9uICAgJ3RpdGxlJ1xuICAvLyAgICAgICAgICAgICAgICAgICAgICAgXl5eIHNraXBwaW5nIHRob3NlIHNwYWNlc1xuICBzdGFydCA9IHBvcztcbiAgZm9yICg7IHBvcyA8IG1heDsgcG9zKyspIHtcbiAgICBjaCA9IHN0ci5jaGFyQ29kZUF0KHBvcyk7XG4gICAgaWYgKGNoID09PSAweDBBKSB7XG4gICAgICBsaW5lcysrO1xuICAgIH0gZWxzZSBpZiAoaXNTcGFjZShjaCkpIHtcbiAgICAgIC8qZXNsaW50IG5vLWVtcHR5OjAqL1xuICAgIH0gZWxzZSB7XG4gICAgICBicmVhaztcbiAgICB9XG4gIH1cblxuICAvLyBbbGFiZWxdOiAgIGRlc3RpbmF0aW9uICAgJ3RpdGxlJ1xuICAvLyAgICAgICAgICAgICAgICAgICAgICAgICAgXl5eXl5eXiBwYXJzZSB0aGlzXG4gIHJlcyA9IHN0YXRlLm1kLmhlbHBlcnMucGFyc2VMaW5rVGl0bGUoc3RyLCBwb3MsIG1heCk7XG4gIGlmIChwb3MgPCBtYXggJiYgc3RhcnQgIT09IHBvcyAmJiByZXMub2spIHtcbiAgICB0aXRsZSA9IHJlcy5zdHI7XG4gICAgcG9zID0gcmVzLnBvcztcbiAgICBsaW5lcyArPSByZXMubGluZXM7XG4gIH0gZWxzZSB7XG4gICAgdGl0bGUgPSAnJztcbiAgICBwb3MgPSBkZXN0RW5kUG9zO1xuICAgIGxpbmVzID0gZGVzdEVuZExpbmVObztcbiAgfVxuXG4gIC8vIHNraXAgdHJhaWxpbmcgc3BhY2VzIHVudGlsIHRoZSByZXN0IG9mIHRoZSBsaW5lXG4gIHdoaWxlIChwb3MgPCBtYXgpIHtcbiAgICBjaCA9IHN0ci5jaGFyQ29kZUF0KHBvcyk7XG4gICAgaWYgKCFpc1NwYWNlKGNoKSkgeyBicmVhazsgfVxuICAgIHBvcysrO1xuICB9XG5cbiAgaWYgKHBvcyA8IG1heCAmJiBzdHIuY2hhckNvZGVBdChwb3MpICE9PSAweDBBKSB7XG4gICAgaWYgKHRpdGxlKSB7XG4gICAgICAvLyBnYXJiYWdlIGF0IHRoZSBlbmQgb2YgdGhlIGxpbmUgYWZ0ZXIgdGl0bGUsXG4gICAgICAvLyBidXQgaXQgY291bGQgc3RpbGwgYmUgYSB2YWxpZCByZWZlcmVuY2UgaWYgd2Ugcm9sbCBiYWNrXG4gICAgICB0aXRsZSA9ICcnO1xuICAgICAgcG9zID0gZGVzdEVuZFBvcztcbiAgICAgIGxpbmVzID0gZGVzdEVuZExpbmVObztcbiAgICAgIHdoaWxlIChwb3MgPCBtYXgpIHtcbiAgICAgICAgY2ggPSBzdHIuY2hhckNvZGVBdChwb3MpO1xuICAgICAgICBpZiAoIWlzU3BhY2UoY2gpKSB7IGJyZWFrOyB9XG4gICAgICAgIHBvcysrO1xuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIGlmIChwb3MgPCBtYXggJiYgc3RyLmNoYXJDb2RlQXQocG9zKSAhPT0gMHgwQSkge1xuICAgIC8vIGdhcmJhZ2UgYXQgdGhlIGVuZCBvZiB0aGUgbGluZVxuICAgIHJldHVybiBmYWxzZTtcbiAgfVxuXG4gIGxhYmVsID0gbm9ybWFsaXplUmVmZXJlbmNlKHN0ci5zbGljZSgxLCBsYWJlbEVuZCkpO1xuICBpZiAoIWxhYmVsKSB7XG4gICAgLy8gQ29tbW9uTWFyayAwLjIwIGRpc2FsbG93cyBlbXB0eSBsYWJlbHNcbiAgICByZXR1cm4gZmFsc2U7XG4gIH1cblxuICAvLyBSZWZlcmVuY2UgY2FuIG5vdCB0ZXJtaW5hdGUgYW55dGhpbmcuIFRoaXMgY2hlY2sgaXMgZm9yIHNhZmV0eSBvbmx5LlxuICAvKmlzdGFuYnVsIGlnbm9yZSBpZiovXG4gIGlmIChzaWxlbnQpIHsgcmV0dXJuIHRydWU7IH1cblxuICBpZiAodHlwZW9mIHN0YXRlLmVudi5yZWZlcmVuY2VzID09PSAndW5kZWZpbmVkJykge1xuICAgIHN0YXRlLmVudi5yZWZlcmVuY2VzID0ge307XG4gIH1cbiAgaWYgKHR5cGVvZiBzdGF0ZS5lbnYucmVmZXJlbmNlc1tsYWJlbF0gPT09ICd1bmRlZmluZWQnKSB7XG4gICAgc3RhdGUuZW52LnJlZmVyZW5jZXNbbGFiZWxdID0geyB0aXRsZTogdGl0bGUsIGhyZWY6IGhyZWYgfTtcbiAgfVxuXG4gIHN0YXRlLnBhcmVudFR5cGUgPSBvbGRQYXJlbnRUeXBlO1xuXG4gIHN0YXRlLmxpbmUgPSBzdGFydExpbmUgKyBsaW5lcyArIDE7XG4gIHJldHVybiB0cnVlO1xufTtcbiIsIi8vIFBhcnNlciBzdGF0ZSBjbGFzc1xuXG4ndXNlIHN0cmljdCc7XG5cbnZhciBUb2tlbiA9IHJlcXVpcmUoJy4uL3Rva2VuJyk7XG52YXIgaXNTcGFjZSA9IHJlcXVpcmUoJy4uL2NvbW1vbi91dGlscycpLmlzU3BhY2U7XG5cblxuZnVuY3Rpb24gU3RhdGVCbG9jayhzcmMsIG1kLCBlbnYsIHRva2Vucykge1xuICB2YXIgY2gsIHMsIHN0YXJ0LCBwb3MsIGxlbiwgaW5kZW50LCBvZmZzZXQsIGluZGVudF9mb3VuZDtcblxuICB0aGlzLnNyYyA9IHNyYztcblxuICAvLyBsaW5rIHRvIHBhcnNlciBpbnN0YW5jZVxuICB0aGlzLm1kICAgICA9IG1kO1xuXG4gIHRoaXMuZW52ID0gZW52O1xuXG4gIC8vXG4gIC8vIEludGVybmFsIHN0YXRlIHZhcnRpYWJsZXNcbiAgLy9cblxuICB0aGlzLnRva2VucyA9IHRva2VucztcblxuICB0aGlzLmJNYXJrcyA9IFtdOyAgLy8gbGluZSBiZWdpbiBvZmZzZXRzIGZvciBmYXN0IGp1bXBzXG4gIHRoaXMuZU1hcmtzID0gW107ICAvLyBsaW5lIGVuZCBvZmZzZXRzIGZvciBmYXN0IGp1bXBzXG4gIHRoaXMudFNoaWZ0ID0gW107ICAvLyBvZmZzZXRzIG9mIHRoZSBmaXJzdCBub24tc3BhY2UgY2hhcmFjdGVycyAodGFicyBub3QgZXhwYW5kZWQpXG4gIHRoaXMuc0NvdW50ID0gW107ICAvLyBpbmRlbnRzIGZvciBlYWNoIGxpbmUgKHRhYnMgZXhwYW5kZWQpXG5cbiAgLy8gQW4gYW1vdW50IG9mIHZpcnR1YWwgc3BhY2VzICh0YWJzIGV4cGFuZGVkKSBiZXR3ZWVuIGJlZ2lubmluZ1xuICAvLyBvZiBlYWNoIGxpbmUgKGJNYXJrcykgYW5kIHJlYWwgYmVnaW5uaW5nIG9mIHRoYXQgbGluZS5cbiAgLy9cbiAgLy8gSXQgZXhpc3RzIG9ubHkgYXMgYSBoYWNrIGJlY2F1c2UgYmxvY2txdW90ZXMgb3ZlcnJpZGUgYk1hcmtzXG4gIC8vIGxvc2luZyBpbmZvcm1hdGlvbiBpbiB0aGUgcHJvY2Vzcy5cbiAgLy9cbiAgLy8gSXQncyB1c2VkIG9ubHkgd2hlbiBleHBhbmRpbmcgdGFicywgeW91IGNhbiB0aGluayBhYm91dCBpdCBhc1xuICAvLyBhbiBpbml0aWFsIHRhYiBsZW5ndGgsIGUuZy4gYnNDb3VudD0yMSBhcHBsaWVkIHRvIHN0cmluZyBgXFx0MTIzYFxuICAvLyBtZWFucyBmaXJzdCB0YWIgc2hvdWxkIGJlIGV4cGFuZGVkIHRvIDQtMjElNCA9PT0gMyBzcGFjZXMuXG4gIC8vXG4gIHRoaXMuYnNDb3VudCA9IFtdO1xuXG4gIC8vIGJsb2NrIHBhcnNlciB2YXJpYWJsZXNcbiAgdGhpcy5ibGtJbmRlbnQgID0gMDsgLy8gcmVxdWlyZWQgYmxvY2sgY29udGVudCBpbmRlbnQgKGZvciBleGFtcGxlLCBpZiB3ZSBhcmVcbiAgICAgICAgICAgICAgICAgICAgICAgLy8gaW5zaWRlIGEgbGlzdCwgaXQgd291bGQgYmUgcG9zaXRpb25lZCBhZnRlciBsaXN0IG1hcmtlcilcbiAgdGhpcy5saW5lICAgICAgID0gMDsgLy8gbGluZSBpbmRleCBpbiBzcmNcbiAgdGhpcy5saW5lTWF4ICAgID0gMDsgLy8gbGluZXMgY291bnRcbiAgdGhpcy50aWdodCAgICAgID0gZmFsc2U7ICAvLyBsb29zZS90aWdodCBtb2RlIGZvciBsaXN0c1xuICB0aGlzLmRkSW5kZW50ICAgPSAtMTsgLy8gaW5kZW50IG9mIHRoZSBjdXJyZW50IGRkIGJsb2NrICgtMSBpZiB0aGVyZSBpc24ndCBhbnkpXG4gIHRoaXMubGlzdEluZGVudCA9IC0xOyAvLyBpbmRlbnQgb2YgdGhlIGN1cnJlbnQgbGlzdCBibG9jayAoLTEgaWYgdGhlcmUgaXNuJ3QgYW55KVxuXG4gIC8vIGNhbiBiZSAnYmxvY2txdW90ZScsICdsaXN0JywgJ3Jvb3QnLCAncGFyYWdyYXBoJyBvciAncmVmZXJlbmNlJ1xuICAvLyB1c2VkIGluIGxpc3RzIHRvIGRldGVybWluZSBpZiB0aGV5IGludGVycnVwdCBhIHBhcmFncmFwaFxuICB0aGlzLnBhcmVudFR5cGUgPSAncm9vdCc7XG5cbiAgdGhpcy5sZXZlbCA9IDA7XG5cbiAgLy8gcmVuZGVyZXJcbiAgdGhpcy5yZXN1bHQgPSAnJztcblxuICAvLyBDcmVhdGUgY2FjaGVzXG4gIC8vIEdlbmVyYXRlIG1hcmtlcnMuXG4gIHMgPSB0aGlzLnNyYztcbiAgaW5kZW50X2ZvdW5kID0gZmFsc2U7XG5cbiAgZm9yIChzdGFydCA9IHBvcyA9IGluZGVudCA9IG9mZnNldCA9IDAsIGxlbiA9IHMubGVuZ3RoOyBwb3MgPCBsZW47IHBvcysrKSB7XG4gICAgY2ggPSBzLmNoYXJDb2RlQXQocG9zKTtcblxuICAgIGlmICghaW5kZW50X2ZvdW5kKSB7XG4gICAgICBpZiAoaXNTcGFjZShjaCkpIHtcbiAgICAgICAgaW5kZW50Kys7XG5cbiAgICAgICAgaWYgKGNoID09PSAweDA5KSB7XG4gICAgICAgICAgb2Zmc2V0ICs9IDQgLSBvZmZzZXQgJSA0O1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIG9mZnNldCsrO1xuICAgICAgICB9XG4gICAgICAgIGNvbnRpbnVlO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgaW5kZW50X2ZvdW5kID0gdHJ1ZTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICBpZiAoY2ggPT09IDB4MEEgfHwgcG9zID09PSBsZW4gLSAxKSB7XG4gICAgICBpZiAoY2ggIT09IDB4MEEpIHsgcG9zKys7IH1cbiAgICAgIHRoaXMuYk1hcmtzLnB1c2goc3RhcnQpO1xuICAgICAgdGhpcy5lTWFya3MucHVzaChwb3MpO1xuICAgICAgdGhpcy50U2hpZnQucHVzaChpbmRlbnQpO1xuICAgICAgdGhpcy5zQ291bnQucHVzaChvZmZzZXQpO1xuICAgICAgdGhpcy5ic0NvdW50LnB1c2goMCk7XG5cbiAgICAgIGluZGVudF9mb3VuZCA9IGZhbHNlO1xuICAgICAgaW5kZW50ID0gMDtcbiAgICAgIG9mZnNldCA9IDA7XG4gICAgICBzdGFydCA9IHBvcyArIDE7XG4gICAgfVxuICB9XG5cbiAgLy8gUHVzaCBmYWtlIGVudHJ5IHRvIHNpbXBsaWZ5IGNhY2hlIGJvdW5kcyBjaGVja3NcbiAgdGhpcy5iTWFya3MucHVzaChzLmxlbmd0aCk7XG4gIHRoaXMuZU1hcmtzLnB1c2gocy5sZW5ndGgpO1xuICB0aGlzLnRTaGlmdC5wdXNoKDApO1xuICB0aGlzLnNDb3VudC5wdXNoKDApO1xuICB0aGlzLmJzQ291bnQucHVzaCgwKTtcblxuICB0aGlzLmxpbmVNYXggPSB0aGlzLmJNYXJrcy5sZW5ndGggLSAxOyAvLyBkb24ndCBjb3VudCBsYXN0IGZha2UgbGluZVxufVxuXG4vLyBQdXNoIG5ldyB0b2tlbiB0byBcInN0cmVhbVwiLlxuLy9cblN0YXRlQmxvY2sucHJvdG90eXBlLnB1c2ggPSBmdW5jdGlvbiAodHlwZSwgdGFnLCBuZXN0aW5nKSB7XG4gIHZhciB0b2tlbiA9IG5ldyBUb2tlbih0eXBlLCB0YWcsIG5lc3RpbmcpO1xuICB0b2tlbi5ibG9jayA9IHRydWU7XG5cbiAgaWYgKG5lc3RpbmcgPCAwKSB0aGlzLmxldmVsLS07IC8vIGNsb3NpbmcgdGFnXG4gIHRva2VuLmxldmVsID0gdGhpcy5sZXZlbDtcbiAgaWYgKG5lc3RpbmcgPiAwKSB0aGlzLmxldmVsKys7IC8vIG9wZW5pbmcgdGFnXG5cbiAgdGhpcy50b2tlbnMucHVzaCh0b2tlbik7XG4gIHJldHVybiB0b2tlbjtcbn07XG5cblN0YXRlQmxvY2sucHJvdG90eXBlLmlzRW1wdHkgPSBmdW5jdGlvbiBpc0VtcHR5KGxpbmUpIHtcbiAgcmV0dXJuIHRoaXMuYk1hcmtzW2xpbmVdICsgdGhpcy50U2hpZnRbbGluZV0gPj0gdGhpcy5lTWFya3NbbGluZV07XG59O1xuXG5TdGF0ZUJsb2NrLnByb3RvdHlwZS5za2lwRW1wdHlMaW5lcyA9IGZ1bmN0aW9uIHNraXBFbXB0eUxpbmVzKGZyb20pIHtcbiAgZm9yICh2YXIgbWF4ID0gdGhpcy5saW5lTWF4OyBmcm9tIDwgbWF4OyBmcm9tKyspIHtcbiAgICBpZiAodGhpcy5iTWFya3NbZnJvbV0gKyB0aGlzLnRTaGlmdFtmcm9tXSA8IHRoaXMuZU1hcmtzW2Zyb21dKSB7XG4gICAgICBicmVhaztcbiAgICB9XG4gIH1cbiAgcmV0dXJuIGZyb207XG59O1xuXG4vLyBTa2lwIHNwYWNlcyBmcm9tIGdpdmVuIHBvc2l0aW9uLlxuU3RhdGVCbG9jay5wcm90b3R5cGUuc2tpcFNwYWNlcyA9IGZ1bmN0aW9uIHNraXBTcGFjZXMocG9zKSB7XG4gIHZhciBjaDtcblxuICBmb3IgKHZhciBtYXggPSB0aGlzLnNyYy5sZW5ndGg7IHBvcyA8IG1heDsgcG9zKyspIHtcbiAgICBjaCA9IHRoaXMuc3JjLmNoYXJDb2RlQXQocG9zKTtcbiAgICBpZiAoIWlzU3BhY2UoY2gpKSB7IGJyZWFrOyB9XG4gIH1cbiAgcmV0dXJuIHBvcztcbn07XG5cbi8vIFNraXAgc3BhY2VzIGZyb20gZ2l2ZW4gcG9zaXRpb24gaW4gcmV2ZXJzZS5cblN0YXRlQmxvY2sucHJvdG90eXBlLnNraXBTcGFjZXNCYWNrID0gZnVuY3Rpb24gc2tpcFNwYWNlc0JhY2socG9zLCBtaW4pIHtcbiAgaWYgKHBvcyA8PSBtaW4pIHsgcmV0dXJuIHBvczsgfVxuXG4gIHdoaWxlIChwb3MgPiBtaW4pIHtcbiAgICBpZiAoIWlzU3BhY2UodGhpcy5zcmMuY2hhckNvZGVBdCgtLXBvcykpKSB7IHJldHVybiBwb3MgKyAxOyB9XG4gIH1cbiAgcmV0dXJuIHBvcztcbn07XG5cbi8vIFNraXAgY2hhciBjb2RlcyBmcm9tIGdpdmVuIHBvc2l0aW9uXG5TdGF0ZUJsb2NrLnByb3RvdHlwZS5za2lwQ2hhcnMgPSBmdW5jdGlvbiBza2lwQ2hhcnMocG9zLCBjb2RlKSB7XG4gIGZvciAodmFyIG1heCA9IHRoaXMuc3JjLmxlbmd0aDsgcG9zIDwgbWF4OyBwb3MrKykge1xuICAgIGlmICh0aGlzLnNyYy5jaGFyQ29kZUF0KHBvcykgIT09IGNvZGUpIHsgYnJlYWs7IH1cbiAgfVxuICByZXR1cm4gcG9zO1xufTtcblxuLy8gU2tpcCBjaGFyIGNvZGVzIHJldmVyc2UgZnJvbSBnaXZlbiBwb3NpdGlvbiAtIDFcblN0YXRlQmxvY2sucHJvdG90eXBlLnNraXBDaGFyc0JhY2sgPSBmdW5jdGlvbiBza2lwQ2hhcnNCYWNrKHBvcywgY29kZSwgbWluKSB7XG4gIGlmIChwb3MgPD0gbWluKSB7IHJldHVybiBwb3M7IH1cblxuICB3aGlsZSAocG9zID4gbWluKSB7XG4gICAgaWYgKGNvZGUgIT09IHRoaXMuc3JjLmNoYXJDb2RlQXQoLS1wb3MpKSB7IHJldHVybiBwb3MgKyAxOyB9XG4gIH1cbiAgcmV0dXJuIHBvcztcbn07XG5cbi8vIGN1dCBsaW5lcyByYW5nZSBmcm9tIHNvdXJjZS5cblN0YXRlQmxvY2sucHJvdG90eXBlLmdldExpbmVzID0gZnVuY3Rpb24gZ2V0TGluZXMoYmVnaW4sIGVuZCwgaW5kZW50LCBrZWVwTGFzdExGKSB7XG4gIHZhciBpLCBsaW5lSW5kZW50LCBjaCwgZmlyc3QsIGxhc3QsIHF1ZXVlLCBsaW5lU3RhcnQsXG4gICAgICBsaW5lID0gYmVnaW47XG5cbiAgaWYgKGJlZ2luID49IGVuZCkge1xuICAgIHJldHVybiAnJztcbiAgfVxuXG4gIHF1ZXVlID0gbmV3IEFycmF5KGVuZCAtIGJlZ2luKTtcblxuICBmb3IgKGkgPSAwOyBsaW5lIDwgZW5kOyBsaW5lKyssIGkrKykge1xuICAgIGxpbmVJbmRlbnQgPSAwO1xuICAgIGxpbmVTdGFydCA9IGZpcnN0ID0gdGhpcy5iTWFya3NbbGluZV07XG5cbiAgICBpZiAobGluZSArIDEgPCBlbmQgfHwga2VlcExhc3RMRikge1xuICAgICAgLy8gTm8gbmVlZCBmb3IgYm91bmRzIGNoZWNrIGJlY2F1c2Ugd2UgaGF2ZSBmYWtlIGVudHJ5IG9uIHRhaWwuXG4gICAgICBsYXN0ID0gdGhpcy5lTWFya3NbbGluZV0gKyAxO1xuICAgIH0gZWxzZSB7XG4gICAgICBsYXN0ID0gdGhpcy5lTWFya3NbbGluZV07XG4gICAgfVxuXG4gICAgd2hpbGUgKGZpcnN0IDwgbGFzdCAmJiBsaW5lSW5kZW50IDwgaW5kZW50KSB7XG4gICAgICBjaCA9IHRoaXMuc3JjLmNoYXJDb2RlQXQoZmlyc3QpO1xuXG4gICAgICBpZiAoaXNTcGFjZShjaCkpIHtcbiAgICAgICAgaWYgKGNoID09PSAweDA5KSB7XG4gICAgICAgICAgbGluZUluZGVudCArPSA0IC0gKGxpbmVJbmRlbnQgKyB0aGlzLmJzQ291bnRbbGluZV0pICUgNDtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICBsaW5lSW5kZW50Kys7XG4gICAgICAgIH1cbiAgICAgIH0gZWxzZSBpZiAoZmlyc3QgLSBsaW5lU3RhcnQgPCB0aGlzLnRTaGlmdFtsaW5lXSkge1xuICAgICAgICAvLyBwYXRjaGVkIHRTaGlmdCBtYXNrZWQgY2hhcmFjdGVycyB0byBsb29rIGxpa2Ugc3BhY2VzIChibG9ja3F1b3RlcywgbGlzdCBtYXJrZXJzKVxuICAgICAgICBsaW5lSW5kZW50Kys7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBicmVhaztcbiAgICAgIH1cblxuICAgICAgZmlyc3QrKztcbiAgICB9XG5cbiAgICBpZiAobGluZUluZGVudCA+IGluZGVudCkge1xuICAgICAgLy8gcGFydGlhbGx5IGV4cGFuZGluZyB0YWJzIGluIGNvZGUgYmxvY2tzLCBlLmcgJ1xcdFxcdGZvb2JhcidcbiAgICAgIC8vIHdpdGggaW5kZW50PTIgYmVjb21lcyAnICBcXHRmb29iYXInXG4gICAgICBxdWV1ZVtpXSA9IG5ldyBBcnJheShsaW5lSW5kZW50IC0gaW5kZW50ICsgMSkuam9pbignICcpICsgdGhpcy5zcmMuc2xpY2UoZmlyc3QsIGxhc3QpO1xuICAgIH0gZWxzZSB7XG4gICAgICBxdWV1ZVtpXSA9IHRoaXMuc3JjLnNsaWNlKGZpcnN0LCBsYXN0KTtcbiAgICB9XG4gIH1cblxuICByZXR1cm4gcXVldWUuam9pbignJyk7XG59O1xuXG4vLyByZS1leHBvcnQgVG9rZW4gY2xhc3MgdG8gdXNlIGluIGJsb2NrIHJ1bGVzXG5TdGF0ZUJsb2NrLnByb3RvdHlwZS5Ub2tlbiA9IFRva2VuO1xuXG5cbm1vZHVsZS5leHBvcnRzID0gU3RhdGVCbG9jaztcbiIsIi8vIEdGTSB0YWJsZSwgbm9uLXN0YW5kYXJkXG5cbid1c2Ugc3RyaWN0JztcblxudmFyIGlzU3BhY2UgPSByZXF1aXJlKCcuLi9jb21tb24vdXRpbHMnKS5pc1NwYWNlO1xuXG5cbmZ1bmN0aW9uIGdldExpbmUoc3RhdGUsIGxpbmUpIHtcbiAgdmFyIHBvcyA9IHN0YXRlLmJNYXJrc1tsaW5lXSArIHN0YXRlLmJsa0luZGVudCxcbiAgICAgIG1heCA9IHN0YXRlLmVNYXJrc1tsaW5lXTtcblxuICByZXR1cm4gc3RhdGUuc3JjLnN1YnN0cihwb3MsIG1heCAtIHBvcyk7XG59XG5cbmZ1bmN0aW9uIGVzY2FwZWRTcGxpdChzdHIpIHtcbiAgdmFyIHJlc3VsdCA9IFtdLFxuICAgICAgcG9zID0gMCxcbiAgICAgIG1heCA9IHN0ci5sZW5ndGgsXG4gICAgICBjaCxcbiAgICAgIGVzY2FwZXMgPSAwLFxuICAgICAgbGFzdFBvcyA9IDAsXG4gICAgICBiYWNrVGlja2VkID0gZmFsc2UsXG4gICAgICBsYXN0QmFja1RpY2sgPSAwO1xuXG4gIGNoICA9IHN0ci5jaGFyQ29kZUF0KHBvcyk7XG5cbiAgd2hpbGUgKHBvcyA8IG1heCkge1xuICAgIGlmIChjaCA9PT0gMHg2MC8qIGAgKi8pIHtcbiAgICAgIGlmIChiYWNrVGlja2VkKSB7XG4gICAgICAgIC8vIG1ha2UgXFxgIGNsb3NlIGNvZGUgc2VxdWVuY2UsIGJ1dCBub3Qgb3BlbiBpdDtcbiAgICAgICAgLy8gdGhlIHJlYXNvbiBpczogYFxcYCBpcyBjb3JyZWN0IGNvZGUgYmxvY2tcbiAgICAgICAgYmFja1RpY2tlZCA9IGZhbHNlO1xuICAgICAgICBsYXN0QmFja1RpY2sgPSBwb3M7XG4gICAgICB9IGVsc2UgaWYgKGVzY2FwZXMgJSAyID09PSAwKSB7XG4gICAgICAgIGJhY2tUaWNrZWQgPSB0cnVlO1xuICAgICAgICBsYXN0QmFja1RpY2sgPSBwb3M7XG4gICAgICB9XG4gICAgfSBlbHNlIGlmIChjaCA9PT0gMHg3Yy8qIHwgKi8gJiYgKGVzY2FwZXMgJSAyID09PSAwKSAmJiAhYmFja1RpY2tlZCkge1xuICAgICAgcmVzdWx0LnB1c2goc3RyLnN1YnN0cmluZyhsYXN0UG9zLCBwb3MpKTtcbiAgICAgIGxhc3RQb3MgPSBwb3MgKyAxO1xuICAgIH1cblxuICAgIGlmIChjaCA9PT0gMHg1Yy8qIFxcICovKSB7XG4gICAgICBlc2NhcGVzKys7XG4gICAgfSBlbHNlIHtcbiAgICAgIGVzY2FwZXMgPSAwO1xuICAgIH1cblxuICAgIHBvcysrO1xuXG4gICAgLy8gSWYgdGhlcmUgd2FzIGFuIHVuLWNsb3NlZCBiYWNrdGljaywgZ28gYmFjayB0byBqdXN0IGFmdGVyXG4gICAgLy8gdGhlIGxhc3QgYmFja3RpY2ssIGJ1dCBhcyBpZiBpdCB3YXMgYSBub3JtYWwgY2hhcmFjdGVyXG4gICAgaWYgKHBvcyA9PT0gbWF4ICYmIGJhY2tUaWNrZWQpIHtcbiAgICAgIGJhY2tUaWNrZWQgPSBmYWxzZTtcbiAgICAgIHBvcyA9IGxhc3RCYWNrVGljayArIDE7XG4gICAgfVxuXG4gICAgY2ggPSBzdHIuY2hhckNvZGVBdChwb3MpO1xuICB9XG5cbiAgcmVzdWx0LnB1c2goc3RyLnN1YnN0cmluZyhsYXN0UG9zKSk7XG5cbiAgcmV0dXJuIHJlc3VsdDtcbn1cblxuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIHRhYmxlKHN0YXRlLCBzdGFydExpbmUsIGVuZExpbmUsIHNpbGVudCkge1xuICB2YXIgY2gsIGxpbmVUZXh0LCBwb3MsIGksIG5leHRMaW5lLCBjb2x1bW5zLCBjb2x1bW5Db3VudCwgdG9rZW4sXG4gICAgICBhbGlnbnMsIHQsIHRhYmxlTGluZXMsIHRib2R5TGluZXM7XG5cbiAgLy8gc2hvdWxkIGhhdmUgYXQgbGVhc3QgdHdvIGxpbmVzXG4gIGlmIChzdGFydExpbmUgKyAyID4gZW5kTGluZSkgeyByZXR1cm4gZmFsc2U7IH1cblxuICBuZXh0TGluZSA9IHN0YXJ0TGluZSArIDE7XG5cbiAgaWYgKHN0YXRlLnNDb3VudFtuZXh0TGluZV0gPCBzdGF0ZS5ibGtJbmRlbnQpIHsgcmV0dXJuIGZhbHNlOyB9XG5cbiAgLy8gaWYgaXQncyBpbmRlbnRlZCBtb3JlIHRoYW4gMyBzcGFjZXMsIGl0IHNob3VsZCBiZSBhIGNvZGUgYmxvY2tcbiAgaWYgKHN0YXRlLnNDb3VudFtuZXh0TGluZV0gLSBzdGF0ZS5ibGtJbmRlbnQgPj0gNCkgeyByZXR1cm4gZmFsc2U7IH1cblxuICAvLyBmaXJzdCBjaGFyYWN0ZXIgb2YgdGhlIHNlY29uZCBsaW5lIHNob3VsZCBiZSAnfCcsICctJywgJzonLFxuICAvLyBhbmQgbm8gb3RoZXIgY2hhcmFjdGVycyBhcmUgYWxsb3dlZCBidXQgc3BhY2VzO1xuICAvLyBiYXNpY2FsbHksIHRoaXMgaXMgdGhlIGVxdWl2YWxlbnQgb2YgL15bLTp8XVstOnxcXHNdKiQvIHJlZ2V4cFxuXG4gIHBvcyA9IHN0YXRlLmJNYXJrc1tuZXh0TGluZV0gKyBzdGF0ZS50U2hpZnRbbmV4dExpbmVdO1xuICBpZiAocG9zID49IHN0YXRlLmVNYXJrc1tuZXh0TGluZV0pIHsgcmV0dXJuIGZhbHNlOyB9XG5cbiAgY2ggPSBzdGF0ZS5zcmMuY2hhckNvZGVBdChwb3MrKyk7XG4gIGlmIChjaCAhPT0gMHg3Qy8qIHwgKi8gJiYgY2ggIT09IDB4MkQvKiAtICovICYmIGNoICE9PSAweDNBLyogOiAqLykgeyByZXR1cm4gZmFsc2U7IH1cblxuICB3aGlsZSAocG9zIDwgc3RhdGUuZU1hcmtzW25leHRMaW5lXSkge1xuICAgIGNoID0gc3RhdGUuc3JjLmNoYXJDb2RlQXQocG9zKTtcblxuICAgIGlmIChjaCAhPT0gMHg3Qy8qIHwgKi8gJiYgY2ggIT09IDB4MkQvKiAtICovICYmIGNoICE9PSAweDNBLyogOiAqLyAmJiAhaXNTcGFjZShjaCkpIHsgcmV0dXJuIGZhbHNlOyB9XG5cbiAgICBwb3MrKztcbiAgfVxuXG4gIGxpbmVUZXh0ID0gZ2V0TGluZShzdGF0ZSwgc3RhcnRMaW5lICsgMSk7XG5cbiAgY29sdW1ucyA9IGxpbmVUZXh0LnNwbGl0KCd8Jyk7XG4gIGFsaWducyA9IFtdO1xuICBmb3IgKGkgPSAwOyBpIDwgY29sdW1ucy5sZW5ndGg7IGkrKykge1xuICAgIHQgPSBjb2x1bW5zW2ldLnRyaW0oKTtcbiAgICBpZiAoIXQpIHtcbiAgICAgIC8vIGFsbG93IGVtcHR5IGNvbHVtbnMgYmVmb3JlIGFuZCBhZnRlciB0YWJsZSwgYnV0IG5vdCBpbiBiZXR3ZWVuIGNvbHVtbnM7XG4gICAgICAvLyBlLmcuIGFsbG93IGAgfC0tLXwgYCwgZGlzYWxsb3cgYCAtLS18fC0tLSBgXG4gICAgICBpZiAoaSA9PT0gMCB8fCBpID09PSBjb2x1bW5zLmxlbmd0aCAtIDEpIHtcbiAgICAgICAgY29udGludWU7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICB9XG4gICAgfVxuXG4gICAgaWYgKCEvXjo/LSs6PyQvLnRlc3QodCkpIHsgcmV0dXJuIGZhbHNlOyB9XG4gICAgaWYgKHQuY2hhckNvZGVBdCh0Lmxlbmd0aCAtIDEpID09PSAweDNBLyogOiAqLykge1xuICAgICAgYWxpZ25zLnB1c2godC5jaGFyQ29kZUF0KDApID09PSAweDNBLyogOiAqLyA/ICdjZW50ZXInIDogJ3JpZ2h0Jyk7XG4gICAgfSBlbHNlIGlmICh0LmNoYXJDb2RlQXQoMCkgPT09IDB4M0EvKiA6ICovKSB7XG4gICAgICBhbGlnbnMucHVzaCgnbGVmdCcpO1xuICAgIH0gZWxzZSB7XG4gICAgICBhbGlnbnMucHVzaCgnJyk7XG4gICAgfVxuICB9XG5cbiAgbGluZVRleHQgPSBnZXRMaW5lKHN0YXRlLCBzdGFydExpbmUpLnRyaW0oKTtcbiAgaWYgKGxpbmVUZXh0LmluZGV4T2YoJ3wnKSA9PT0gLTEpIHsgcmV0dXJuIGZhbHNlOyB9XG4gIGlmIChzdGF0ZS5zQ291bnRbc3RhcnRMaW5lXSAtIHN0YXRlLmJsa0luZGVudCA+PSA0KSB7IHJldHVybiBmYWxzZTsgfVxuICBjb2x1bW5zID0gZXNjYXBlZFNwbGl0KGxpbmVUZXh0LnJlcGxhY2UoL15cXHx8XFx8JC9nLCAnJykpO1xuXG4gIC8vIGhlYWRlciByb3cgd2lsbCBkZWZpbmUgYW4gYW1vdW50IG9mIGNvbHVtbnMgaW4gdGhlIGVudGlyZSB0YWJsZSxcbiAgLy8gYW5kIGFsaWduIHJvdyBzaG91bGRuJ3QgYmUgc21hbGxlciB0aGFuIHRoYXQgKHRoZSByZXN0IG9mIHRoZSByb3dzIGNhbilcbiAgY29sdW1uQ291bnQgPSBjb2x1bW5zLmxlbmd0aDtcbiAgaWYgKGNvbHVtbkNvdW50ID4gYWxpZ25zLmxlbmd0aCkgeyByZXR1cm4gZmFsc2U7IH1cblxuICBpZiAoc2lsZW50KSB7IHJldHVybiB0cnVlOyB9XG5cbiAgdG9rZW4gICAgID0gc3RhdGUucHVzaCgndGFibGVfb3BlbicsICd0YWJsZScsIDEpO1xuICB0b2tlbi5tYXAgPSB0YWJsZUxpbmVzID0gWyBzdGFydExpbmUsIDAgXTtcblxuICB0b2tlbiAgICAgPSBzdGF0ZS5wdXNoKCd0aGVhZF9vcGVuJywgJ3RoZWFkJywgMSk7XG4gIHRva2VuLm1hcCA9IFsgc3RhcnRMaW5lLCBzdGFydExpbmUgKyAxIF07XG5cbiAgdG9rZW4gICAgID0gc3RhdGUucHVzaCgndHJfb3BlbicsICd0cicsIDEpO1xuICB0b2tlbi5tYXAgPSBbIHN0YXJ0TGluZSwgc3RhcnRMaW5lICsgMSBdO1xuXG4gIGZvciAoaSA9IDA7IGkgPCBjb2x1bW5zLmxlbmd0aDsgaSsrKSB7XG4gICAgdG9rZW4gICAgICAgICAgPSBzdGF0ZS5wdXNoKCd0aF9vcGVuJywgJ3RoJywgMSk7XG4gICAgdG9rZW4ubWFwICAgICAgPSBbIHN0YXJ0TGluZSwgc3RhcnRMaW5lICsgMSBdO1xuICAgIGlmIChhbGlnbnNbaV0pIHtcbiAgICAgIHRva2VuLmF0dHJzICA9IFsgWyAnc3R5bGUnLCAndGV4dC1hbGlnbjonICsgYWxpZ25zW2ldIF0gXTtcbiAgICB9XG5cbiAgICB0b2tlbiAgICAgICAgICA9IHN0YXRlLnB1c2goJ2lubGluZScsICcnLCAwKTtcbiAgICB0b2tlbi5jb250ZW50ICA9IGNvbHVtbnNbaV0udHJpbSgpO1xuICAgIHRva2VuLm1hcCAgICAgID0gWyBzdGFydExpbmUsIHN0YXJ0TGluZSArIDEgXTtcbiAgICB0b2tlbi5jaGlsZHJlbiA9IFtdO1xuXG4gICAgdG9rZW4gICAgICAgICAgPSBzdGF0ZS5wdXNoKCd0aF9jbG9zZScsICd0aCcsIC0xKTtcbiAgfVxuXG4gIHRva2VuICAgICA9IHN0YXRlLnB1c2goJ3RyX2Nsb3NlJywgJ3RyJywgLTEpO1xuICB0b2tlbiAgICAgPSBzdGF0ZS5wdXNoKCd0aGVhZF9jbG9zZScsICd0aGVhZCcsIC0xKTtcblxuICB0b2tlbiAgICAgPSBzdGF0ZS5wdXNoKCd0Ym9keV9vcGVuJywgJ3Rib2R5JywgMSk7XG4gIHRva2VuLm1hcCA9IHRib2R5TGluZXMgPSBbIHN0YXJ0TGluZSArIDIsIDAgXTtcblxuICBmb3IgKG5leHRMaW5lID0gc3RhcnRMaW5lICsgMjsgbmV4dExpbmUgPCBlbmRMaW5lOyBuZXh0TGluZSsrKSB7XG4gICAgaWYgKHN0YXRlLnNDb3VudFtuZXh0TGluZV0gPCBzdGF0ZS5ibGtJbmRlbnQpIHsgYnJlYWs7IH1cblxuICAgIGxpbmVUZXh0ID0gZ2V0TGluZShzdGF0ZSwgbmV4dExpbmUpLnRyaW0oKTtcbiAgICBpZiAobGluZVRleHQuaW5kZXhPZignfCcpID09PSAtMSkgeyBicmVhazsgfVxuICAgIGlmIChzdGF0ZS5zQ291bnRbbmV4dExpbmVdIC0gc3RhdGUuYmxrSW5kZW50ID49IDQpIHsgYnJlYWs7IH1cbiAgICBjb2x1bW5zID0gZXNjYXBlZFNwbGl0KGxpbmVUZXh0LnJlcGxhY2UoL15cXHx8XFx8JC9nLCAnJykpO1xuXG4gICAgdG9rZW4gPSBzdGF0ZS5wdXNoKCd0cl9vcGVuJywgJ3RyJywgMSk7XG4gICAgZm9yIChpID0gMDsgaSA8IGNvbHVtbkNvdW50OyBpKyspIHtcbiAgICAgIHRva2VuICAgICAgICAgID0gc3RhdGUucHVzaCgndGRfb3BlbicsICd0ZCcsIDEpO1xuICAgICAgaWYgKGFsaWduc1tpXSkge1xuICAgICAgICB0b2tlbi5hdHRycyAgPSBbIFsgJ3N0eWxlJywgJ3RleHQtYWxpZ246JyArIGFsaWduc1tpXSBdIF07XG4gICAgICB9XG5cbiAgICAgIHRva2VuICAgICAgICAgID0gc3RhdGUucHVzaCgnaW5saW5lJywgJycsIDApO1xuICAgICAgdG9rZW4uY29udGVudCAgPSBjb2x1bW5zW2ldID8gY29sdW1uc1tpXS50cmltKCkgOiAnJztcbiAgICAgIHRva2VuLmNoaWxkcmVuID0gW107XG5cbiAgICAgIHRva2VuICAgICAgICAgID0gc3RhdGUucHVzaCgndGRfY2xvc2UnLCAndGQnLCAtMSk7XG4gICAgfVxuICAgIHRva2VuID0gc3RhdGUucHVzaCgndHJfY2xvc2UnLCAndHInLCAtMSk7XG4gIH1cbiAgdG9rZW4gPSBzdGF0ZS5wdXNoKCd0Ym9keV9jbG9zZScsICd0Ym9keScsIC0xKTtcbiAgdG9rZW4gPSBzdGF0ZS5wdXNoKCd0YWJsZV9jbG9zZScsICd0YWJsZScsIC0xKTtcblxuICB0YWJsZUxpbmVzWzFdID0gdGJvZHlMaW5lc1sxXSA9IG5leHRMaW5lO1xuICBzdGF0ZS5saW5lID0gbmV4dExpbmU7XG4gIHJldHVybiB0cnVlO1xufTtcbiIsIid1c2Ugc3RyaWN0JztcblxuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIGJsb2NrKHN0YXRlKSB7XG4gIHZhciB0b2tlbjtcblxuICBpZiAoc3RhdGUuaW5saW5lTW9kZSkge1xuICAgIHRva2VuICAgICAgICAgID0gbmV3IHN0YXRlLlRva2VuKCdpbmxpbmUnLCAnJywgMCk7XG4gICAgdG9rZW4uY29udGVudCAgPSBzdGF0ZS5zcmM7XG4gICAgdG9rZW4ubWFwICAgICAgPSBbIDAsIDEgXTtcbiAgICB0b2tlbi5jaGlsZHJlbiA9IFtdO1xuICAgIHN0YXRlLnRva2Vucy5wdXNoKHRva2VuKTtcbiAgfSBlbHNlIHtcbiAgICBzdGF0ZS5tZC5ibG9jay5wYXJzZShzdGF0ZS5zcmMsIHN0YXRlLm1kLCBzdGF0ZS5lbnYsIHN0YXRlLnRva2Vucyk7XG4gIH1cbn07XG4iLCIndXNlIHN0cmljdCc7XG5cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gaW5saW5lKHN0YXRlKSB7XG4gIHZhciB0b2tlbnMgPSBzdGF0ZS50b2tlbnMsIHRvaywgaSwgbDtcblxuICAvLyBQYXJzZSBpbmxpbmVzXG4gIGZvciAoaSA9IDAsIGwgPSB0b2tlbnMubGVuZ3RoOyBpIDwgbDsgaSsrKSB7XG4gICAgdG9rID0gdG9rZW5zW2ldO1xuICAgIGlmICh0b2sudHlwZSA9PT0gJ2lubGluZScpIHtcbiAgICAgIHN0YXRlLm1kLmlubGluZS5wYXJzZSh0b2suY29udGVudCwgc3RhdGUubWQsIHN0YXRlLmVudiwgdG9rLmNoaWxkcmVuKTtcbiAgICB9XG4gIH1cbn07XG4iLCIvLyBSZXBsYWNlIGxpbmstbGlrZSB0ZXh0cyB3aXRoIGxpbmsgbm9kZXMuXG4vL1xuLy8gQ3VycmVudGx5IHJlc3RyaWN0ZWQgYnkgYG1kLnZhbGlkYXRlTGluaygpYCB0byBodHRwL2h0dHBzL2Z0cFxuLy9cbid1c2Ugc3RyaWN0JztcblxuXG52YXIgYXJyYXlSZXBsYWNlQXQgPSByZXF1aXJlKCcuLi9jb21tb24vdXRpbHMnKS5hcnJheVJlcGxhY2VBdDtcblxuXG5mdW5jdGlvbiBpc0xpbmtPcGVuKHN0cikge1xuICByZXR1cm4gL148YVs+XFxzXS9pLnRlc3Qoc3RyKTtcbn1cbmZ1bmN0aW9uIGlzTGlua0Nsb3NlKHN0cikge1xuICByZXR1cm4gL148XFwvYVxccyo+L2kudGVzdChzdHIpO1xufVxuXG5cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gbGlua2lmeShzdGF0ZSkge1xuICB2YXIgaSwgaiwgbCwgdG9rZW5zLCB0b2tlbiwgY3VycmVudFRva2VuLCBub2RlcywgbG4sIHRleHQsIHBvcywgbGFzdFBvcyxcbiAgICAgIGxldmVsLCBodG1sTGlua0xldmVsLCB1cmwsIGZ1bGxVcmwsIHVybFRleHQsXG4gICAgICBibG9ja1Rva2VucyA9IHN0YXRlLnRva2VucyxcbiAgICAgIGxpbmtzO1xuXG4gIGlmICghc3RhdGUubWQub3B0aW9ucy5saW5raWZ5KSB7IHJldHVybjsgfVxuXG4gIGZvciAoaiA9IDAsIGwgPSBibG9ja1Rva2Vucy5sZW5ndGg7IGogPCBsOyBqKyspIHtcbiAgICBpZiAoYmxvY2tUb2tlbnNbal0udHlwZSAhPT0gJ2lubGluZScgfHxcbiAgICAgICAgIXN0YXRlLm1kLmxpbmtpZnkucHJldGVzdChibG9ja1Rva2Vuc1tqXS5jb250ZW50KSkge1xuICAgICAgY29udGludWU7XG4gICAgfVxuXG4gICAgdG9rZW5zID0gYmxvY2tUb2tlbnNbal0uY2hpbGRyZW47XG5cbiAgICBodG1sTGlua0xldmVsID0gMDtcblxuICAgIC8vIFdlIHNjYW4gZnJvbSB0aGUgZW5kLCB0byBrZWVwIHBvc2l0aW9uIHdoZW4gbmV3IHRhZ3MgYWRkZWQuXG4gICAgLy8gVXNlIHJldmVyc2VkIGxvZ2ljIGluIGxpbmtzIHN0YXJ0L2VuZCBtYXRjaFxuICAgIGZvciAoaSA9IHRva2Vucy5sZW5ndGggLSAxOyBpID49IDA7IGktLSkge1xuICAgICAgY3VycmVudFRva2VuID0gdG9rZW5zW2ldO1xuXG4gICAgICAvLyBTa2lwIGNvbnRlbnQgb2YgbWFya2Rvd24gbGlua3NcbiAgICAgIGlmIChjdXJyZW50VG9rZW4udHlwZSA9PT0gJ2xpbmtfY2xvc2UnKSB7XG4gICAgICAgIGktLTtcbiAgICAgICAgd2hpbGUgKHRva2Vuc1tpXS5sZXZlbCAhPT0gY3VycmVudFRva2VuLmxldmVsICYmIHRva2Vuc1tpXS50eXBlICE9PSAnbGlua19vcGVuJykge1xuICAgICAgICAgIGktLTtcbiAgICAgICAgfVxuICAgICAgICBjb250aW51ZTtcbiAgICAgIH1cblxuICAgICAgLy8gU2tpcCBjb250ZW50IG9mIGh0bWwgdGFnIGxpbmtzXG4gICAgICBpZiAoY3VycmVudFRva2VuLnR5cGUgPT09ICdodG1sX2lubGluZScpIHtcbiAgICAgICAgaWYgKGlzTGlua09wZW4oY3VycmVudFRva2VuLmNvbnRlbnQpICYmIGh0bWxMaW5rTGV2ZWwgPiAwKSB7XG4gICAgICAgICAgaHRtbExpbmtMZXZlbC0tO1xuICAgICAgICB9XG4gICAgICAgIGlmIChpc0xpbmtDbG9zZShjdXJyZW50VG9rZW4uY29udGVudCkpIHtcbiAgICAgICAgICBodG1sTGlua0xldmVsKys7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICAgIGlmIChodG1sTGlua0xldmVsID4gMCkgeyBjb250aW51ZTsgfVxuXG4gICAgICBpZiAoY3VycmVudFRva2VuLnR5cGUgPT09ICd0ZXh0JyAmJiBzdGF0ZS5tZC5saW5raWZ5LnRlc3QoY3VycmVudFRva2VuLmNvbnRlbnQpKSB7XG5cbiAgICAgICAgdGV4dCA9IGN1cnJlbnRUb2tlbi5jb250ZW50O1xuICAgICAgICBsaW5rcyA9IHN0YXRlLm1kLmxpbmtpZnkubWF0Y2godGV4dCk7XG5cbiAgICAgICAgLy8gTm93IHNwbGl0IHN0cmluZyB0byBub2Rlc1xuICAgICAgICBub2RlcyA9IFtdO1xuICAgICAgICBsZXZlbCA9IGN1cnJlbnRUb2tlbi5sZXZlbDtcbiAgICAgICAgbGFzdFBvcyA9IDA7XG5cbiAgICAgICAgZm9yIChsbiA9IDA7IGxuIDwgbGlua3MubGVuZ3RoOyBsbisrKSB7XG5cbiAgICAgICAgICB1cmwgPSBsaW5rc1tsbl0udXJsO1xuICAgICAgICAgIGZ1bGxVcmwgPSBzdGF0ZS5tZC5ub3JtYWxpemVMaW5rKHVybCk7XG4gICAgICAgICAgaWYgKCFzdGF0ZS5tZC52YWxpZGF0ZUxpbmsoZnVsbFVybCkpIHsgY29udGludWU7IH1cblxuICAgICAgICAgIHVybFRleHQgPSBsaW5rc1tsbl0udGV4dDtcblxuICAgICAgICAgIC8vIExpbmtpZmllciBtaWdodCBzZW5kIHJhdyBob3N0bmFtZXMgbGlrZSBcImV4YW1wbGUuY29tXCIsIHdoZXJlIHVybFxuICAgICAgICAgIC8vIHN0YXJ0cyB3aXRoIGRvbWFpbiBuYW1lLiBTbyB3ZSBwcmVwZW5kIGh0dHA6Ly8gaW4gdGhvc2UgY2FzZXMsXG4gICAgICAgICAgLy8gYW5kIHJlbW92ZSBpdCBhZnRlcndhcmRzLlxuICAgICAgICAgIC8vXG4gICAgICAgICAgaWYgKCFsaW5rc1tsbl0uc2NoZW1hKSB7XG4gICAgICAgICAgICB1cmxUZXh0ID0gc3RhdGUubWQubm9ybWFsaXplTGlua1RleHQoJ2h0dHA6Ly8nICsgdXJsVGV4dCkucmVwbGFjZSgvXmh0dHA6XFwvXFwvLywgJycpO1xuICAgICAgICAgIH0gZWxzZSBpZiAobGlua3NbbG5dLnNjaGVtYSA9PT0gJ21haWx0bzonICYmICEvXm1haWx0bzovaS50ZXN0KHVybFRleHQpKSB7XG4gICAgICAgICAgICB1cmxUZXh0ID0gc3RhdGUubWQubm9ybWFsaXplTGlua1RleHQoJ21haWx0bzonICsgdXJsVGV4dCkucmVwbGFjZSgvXm1haWx0bzovLCAnJyk7XG4gICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHVybFRleHQgPSBzdGF0ZS5tZC5ub3JtYWxpemVMaW5rVGV4dCh1cmxUZXh0KTtcbiAgICAgICAgICB9XG5cbiAgICAgICAgICBwb3MgPSBsaW5rc1tsbl0uaW5kZXg7XG5cbiAgICAgICAgICBpZiAocG9zID4gbGFzdFBvcykge1xuICAgICAgICAgICAgdG9rZW4gICAgICAgICA9IG5ldyBzdGF0ZS5Ub2tlbigndGV4dCcsICcnLCAwKTtcbiAgICAgICAgICAgIHRva2VuLmNvbnRlbnQgPSB0ZXh0LnNsaWNlKGxhc3RQb3MsIHBvcyk7XG4gICAgICAgICAgICB0b2tlbi5sZXZlbCAgID0gbGV2ZWw7XG4gICAgICAgICAgICBub2Rlcy5wdXNoKHRva2VuKTtcbiAgICAgICAgICB9XG5cbiAgICAgICAgICB0b2tlbiAgICAgICAgID0gbmV3IHN0YXRlLlRva2VuKCdsaW5rX29wZW4nLCAnYScsIDEpO1xuICAgICAgICAgIHRva2VuLmF0dHJzICAgPSBbIFsgJ2hyZWYnLCBmdWxsVXJsIF0gXTtcbiAgICAgICAgICB0b2tlbi5sZXZlbCAgID0gbGV2ZWwrKztcbiAgICAgICAgICB0b2tlbi5tYXJrdXAgID0gJ2xpbmtpZnknO1xuICAgICAgICAgIHRva2VuLmluZm8gICAgPSAnYXV0byc7XG4gICAgICAgICAgbm9kZXMucHVzaCh0b2tlbik7XG5cbiAgICAgICAgICB0b2tlbiAgICAgICAgID0gbmV3IHN0YXRlLlRva2VuKCd0ZXh0JywgJycsIDApO1xuICAgICAgICAgIHRva2VuLmNvbnRlbnQgPSB1cmxUZXh0O1xuICAgICAgICAgIHRva2VuLmxldmVsICAgPSBsZXZlbDtcbiAgICAgICAgICBub2Rlcy5wdXNoKHRva2VuKTtcblxuICAgICAgICAgIHRva2VuICAgICAgICAgPSBuZXcgc3RhdGUuVG9rZW4oJ2xpbmtfY2xvc2UnLCAnYScsIC0xKTtcbiAgICAgICAgICB0b2tlbi5sZXZlbCAgID0gLS1sZXZlbDtcbiAgICAgICAgICB0b2tlbi5tYXJrdXAgID0gJ2xpbmtpZnknO1xuICAgICAgICAgIHRva2VuLmluZm8gICAgPSAnYXV0byc7XG4gICAgICAgICAgbm9kZXMucHVzaCh0b2tlbik7XG5cbiAgICAgICAgICBsYXN0UG9zID0gbGlua3NbbG5dLmxhc3RJbmRleDtcbiAgICAgICAgfVxuICAgICAgICBpZiAobGFzdFBvcyA8IHRleHQubGVuZ3RoKSB7XG4gICAgICAgICAgdG9rZW4gICAgICAgICA9IG5ldyBzdGF0ZS5Ub2tlbigndGV4dCcsICcnLCAwKTtcbiAgICAgICAgICB0b2tlbi5jb250ZW50ID0gdGV4dC5zbGljZShsYXN0UG9zKTtcbiAgICAgICAgICB0b2tlbi5sZXZlbCAgID0gbGV2ZWw7XG4gICAgICAgICAgbm9kZXMucHVzaCh0b2tlbik7XG4gICAgICAgIH1cblxuICAgICAgICAvLyByZXBsYWNlIGN1cnJlbnQgbm9kZVxuICAgICAgICBibG9ja1Rva2Vuc1tqXS5jaGlsZHJlbiA9IHRva2VucyA9IGFycmF5UmVwbGFjZUF0KHRva2VucywgaSwgbm9kZXMpO1xuICAgICAgfVxuICAgIH1cbiAgfVxufTtcbiIsIi8vIE5vcm1hbGl6ZSBpbnB1dCBzdHJpbmdcblxuJ3VzZSBzdHJpY3QnO1xuXG5cbi8vIGh0dHBzOi8vc3BlYy5jb21tb25tYXJrLm9yZy8wLjI5LyNsaW5lLWVuZGluZ1xudmFyIE5FV0xJTkVTX1JFICA9IC9cXHJcXG4/fFxcbi9nO1xudmFyIE5VTExfUkUgICAgICA9IC9cXDAvZztcblxuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIG5vcm1hbGl6ZShzdGF0ZSkge1xuICB2YXIgc3RyO1xuXG4gIC8vIE5vcm1hbGl6ZSBuZXdsaW5lc1xuICBzdHIgPSBzdGF0ZS5zcmMucmVwbGFjZShORVdMSU5FU19SRSwgJ1xcbicpO1xuXG4gIC8vIFJlcGxhY2UgTlVMTCBjaGFyYWN0ZXJzXG4gIHN0ciA9IHN0ci5yZXBsYWNlKE5VTExfUkUsICdcXHVGRkZEJyk7XG5cbiAgc3RhdGUuc3JjID0gc3RyO1xufTtcbiIsIi8vIFNpbXBsZSB0eXBvZ3JhcGhpYyByZXBsYWNlbWVudHNcbi8vXG4vLyAoYykgKEMpIOKGkiDCqVxuLy8gKHRtKSAoVE0pIOKGkiDihKJcbi8vIChyKSAoUikg4oaSIMKuXG4vLyArLSDihpIgwrFcbi8vIChwKSAoUCkgLT4gwqdcbi8vIC4uLiDihpIg4oCmIChhbHNvID8uLi4uIOKGkiA/Li4sICEuLi4uIOKGkiAhLi4pXG4vLyA/Pz8/Pz8/PyDihpIgPz8/LCAhISEhISDihpIgISEhLCBgLCxgIOKGkiBgLGBcbi8vIC0tIOKGkiAmbmRhc2g7LCAtLS0g4oaSICZtZGFzaDtcbi8vXG4ndXNlIHN0cmljdCc7XG5cbi8vIFRPRE86XG4vLyAtIGZyYWN0aW9uYWxzIDEvMiwgMS80LCAzLzQgLT4gwr0sIMK8LCDCvlxuLy8gLSBtaWx0aXBsaWNhdGlvbiAyIHggNCAtPiAyIMOXIDRcblxudmFyIFJBUkVfUkUgPSAvXFwrLXxcXC5cXC58XFw/XFw/XFw/XFw/fCEhISF8LCx8LS0vO1xuXG4vLyBXb3JrYXJvdW5kIGZvciBwaGFudG9tanMgLSBuZWVkIHJlZ2V4IHdpdGhvdXQgL2cgZmxhZyxcbi8vIG9yIHJvb3QgY2hlY2sgd2lsbCBmYWlsIGV2ZXJ5IHNlY29uZCB0aW1lXG52YXIgU0NPUEVEX0FCQlJfVEVTVF9SRSA9IC9cXCgoY3x0bXxyfHApXFwpL2k7XG5cbnZhciBTQ09QRURfQUJCUl9SRSA9IC9cXCgoY3x0bXxyfHApXFwpL2lnO1xudmFyIFNDT1BFRF9BQkJSID0ge1xuICBjOiAnwqknLFxuICByOiAnwq4nLFxuICBwOiAnwqcnLFxuICB0bTogJ+KEoidcbn07XG5cbmZ1bmN0aW9uIHJlcGxhY2VGbihtYXRjaCwgbmFtZSkge1xuICByZXR1cm4gU0NPUEVEX0FCQlJbbmFtZS50b0xvd2VyQ2FzZSgpXTtcbn1cblxuZnVuY3Rpb24gcmVwbGFjZV9zY29wZWQoaW5saW5lVG9rZW5zKSB7XG4gIHZhciBpLCB0b2tlbiwgaW5zaWRlX2F1dG9saW5rID0gMDtcblxuICBmb3IgKGkgPSBpbmxpbmVUb2tlbnMubGVuZ3RoIC0gMTsgaSA+PSAwOyBpLS0pIHtcbiAgICB0b2tlbiA9IGlubGluZVRva2Vuc1tpXTtcblxuICAgIGlmICh0b2tlbi50eXBlID09PSAndGV4dCcgJiYgIWluc2lkZV9hdXRvbGluaykge1xuICAgICAgdG9rZW4uY29udGVudCA9IHRva2VuLmNvbnRlbnQucmVwbGFjZShTQ09QRURfQUJCUl9SRSwgcmVwbGFjZUZuKTtcbiAgICB9XG5cbiAgICBpZiAodG9rZW4udHlwZSA9PT0gJ2xpbmtfb3BlbicgJiYgdG9rZW4uaW5mbyA9PT0gJ2F1dG8nKSB7XG4gICAgICBpbnNpZGVfYXV0b2xpbmstLTtcbiAgICB9XG5cbiAgICBpZiAodG9rZW4udHlwZSA9PT0gJ2xpbmtfY2xvc2UnICYmIHRva2VuLmluZm8gPT09ICdhdXRvJykge1xuICAgICAgaW5zaWRlX2F1dG9saW5rKys7XG4gICAgfVxuICB9XG59XG5cbmZ1bmN0aW9uIHJlcGxhY2VfcmFyZShpbmxpbmVUb2tlbnMpIHtcbiAgdmFyIGksIHRva2VuLCBpbnNpZGVfYXV0b2xpbmsgPSAwO1xuXG4gIGZvciAoaSA9IGlubGluZVRva2Vucy5sZW5ndGggLSAxOyBpID49IDA7IGktLSkge1xuICAgIHRva2VuID0gaW5saW5lVG9rZW5zW2ldO1xuXG4gICAgaWYgKHRva2VuLnR5cGUgPT09ICd0ZXh0JyAmJiAhaW5zaWRlX2F1dG9saW5rKSB7XG4gICAgICBpZiAoUkFSRV9SRS50ZXN0KHRva2VuLmNvbnRlbnQpKSB7XG4gICAgICAgIHRva2VuLmNvbnRlbnQgPSB0b2tlbi5jb250ZW50XG4gICAgICAgICAgLnJlcGxhY2UoL1xcKy0vZywgJ8KxJylcbiAgICAgICAgICAvLyAuLiwgLi4uLCAuLi4uLi4uIC0+IOKAplxuICAgICAgICAgIC8vIGJ1dCA/Li4uLi4gJiAhLi4uLi4gLT4gPy4uICYgIS4uXG4gICAgICAgICAgLnJlcGxhY2UoL1xcLnsyLH0vZywgJ+KApicpLnJlcGxhY2UoLyhbPyFdKeKApi9nLCAnJDEuLicpXG4gICAgICAgICAgLnJlcGxhY2UoLyhbPyFdKXs0LH0vZywgJyQxJDEkMScpLnJlcGxhY2UoLyx7Mix9L2csICcsJylcbiAgICAgICAgICAvLyBlbS1kYXNoXG4gICAgICAgICAgLnJlcGxhY2UoLyhefFteLV0pLS0tKFteLV18JCkvbWcsICckMVxcdTIwMTQkMicpXG4gICAgICAgICAgLy8gZW4tZGFzaFxuICAgICAgICAgIC5yZXBsYWNlKC8oXnxcXHMpLS0oXFxzfCQpL21nLCAnJDFcXHUyMDEzJDInKVxuICAgICAgICAgIC5yZXBsYWNlKC8oXnxbXi1cXHNdKS0tKFteLVxcc118JCkvbWcsICckMVxcdTIwMTMkMicpO1xuICAgICAgfVxuICAgIH1cblxuICAgIGlmICh0b2tlbi50eXBlID09PSAnbGlua19vcGVuJyAmJiB0b2tlbi5pbmZvID09PSAnYXV0bycpIHtcbiAgICAgIGluc2lkZV9hdXRvbGluay0tO1xuICAgIH1cblxuICAgIGlmICh0b2tlbi50eXBlID09PSAnbGlua19jbG9zZScgJiYgdG9rZW4uaW5mbyA9PT0gJ2F1dG8nKSB7XG4gICAgICBpbnNpZGVfYXV0b2xpbmsrKztcbiAgICB9XG4gIH1cbn1cblxuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIHJlcGxhY2Uoc3RhdGUpIHtcbiAgdmFyIGJsa0lkeDtcblxuICBpZiAoIXN0YXRlLm1kLm9wdGlvbnMudHlwb2dyYXBoZXIpIHsgcmV0dXJuOyB9XG5cbiAgZm9yIChibGtJZHggPSBzdGF0ZS50b2tlbnMubGVuZ3RoIC0gMTsgYmxrSWR4ID49IDA7IGJsa0lkeC0tKSB7XG5cbiAgICBpZiAoc3RhdGUudG9rZW5zW2Jsa0lkeF0udHlwZSAhPT0gJ2lubGluZScpIHsgY29udGludWU7IH1cblxuICAgIGlmIChTQ09QRURfQUJCUl9URVNUX1JFLnRlc3Qoc3RhdGUudG9rZW5zW2Jsa0lkeF0uY29udGVudCkpIHtcbiAgICAgIHJlcGxhY2Vfc2NvcGVkKHN0YXRlLnRva2Vuc1tibGtJZHhdLmNoaWxkcmVuKTtcbiAgICB9XG5cbiAgICBpZiAoUkFSRV9SRS50ZXN0KHN0YXRlLnRva2Vuc1tibGtJZHhdLmNvbnRlbnQpKSB7XG4gICAgICByZXBsYWNlX3JhcmUoc3RhdGUudG9rZW5zW2Jsa0lkeF0uY2hpbGRyZW4pO1xuICAgIH1cblxuICB9XG59O1xuIiwiLy8gQ29udmVydCBzdHJhaWdodCBxdW90YXRpb24gbWFya3MgdG8gdHlwb2dyYXBoaWMgb25lc1xuLy9cbid1c2Ugc3RyaWN0JztcblxuXG52YXIgaXNXaGl0ZVNwYWNlICAgPSByZXF1aXJlKCcuLi9jb21tb24vdXRpbHMnKS5pc1doaXRlU3BhY2U7XG52YXIgaXNQdW5jdENoYXIgICAgPSByZXF1aXJlKCcuLi9jb21tb24vdXRpbHMnKS5pc1B1bmN0Q2hhcjtcbnZhciBpc01kQXNjaWlQdW5jdCA9IHJlcXVpcmUoJy4uL2NvbW1vbi91dGlscycpLmlzTWRBc2NpaVB1bmN0O1xuXG52YXIgUVVPVEVfVEVTVF9SRSA9IC9bJ1wiXS87XG52YXIgUVVPVEVfUkUgPSAvWydcIl0vZztcbnZhciBBUE9TVFJPUEhFID0gJ1xcdTIwMTknOyAvKiDigJkgKi9cblxuXG5mdW5jdGlvbiByZXBsYWNlQXQoc3RyLCBpbmRleCwgY2gpIHtcbiAgcmV0dXJuIHN0ci5zdWJzdHIoMCwgaW5kZXgpICsgY2ggKyBzdHIuc3Vic3RyKGluZGV4ICsgMSk7XG59XG5cbmZ1bmN0aW9uIHByb2Nlc3NfaW5saW5lcyh0b2tlbnMsIHN0YXRlKSB7XG4gIHZhciBpLCB0b2tlbiwgdGV4dCwgdCwgcG9zLCBtYXgsIHRoaXNMZXZlbCwgaXRlbSwgbGFzdENoYXIsIG5leHRDaGFyLFxuICAgICAgaXNMYXN0UHVuY3RDaGFyLCBpc05leHRQdW5jdENoYXIsIGlzTGFzdFdoaXRlU3BhY2UsIGlzTmV4dFdoaXRlU3BhY2UsXG4gICAgICBjYW5PcGVuLCBjYW5DbG9zZSwgaiwgaXNTaW5nbGUsIHN0YWNrLCBvcGVuUXVvdGUsIGNsb3NlUXVvdGU7XG5cbiAgc3RhY2sgPSBbXTtcblxuICBmb3IgKGkgPSAwOyBpIDwgdG9rZW5zLmxlbmd0aDsgaSsrKSB7XG4gICAgdG9rZW4gPSB0b2tlbnNbaV07XG5cbiAgICB0aGlzTGV2ZWwgPSB0b2tlbnNbaV0ubGV2ZWw7XG5cbiAgICBmb3IgKGogPSBzdGFjay5sZW5ndGggLSAxOyBqID49IDA7IGotLSkge1xuICAgICAgaWYgKHN0YWNrW2pdLmxldmVsIDw9IHRoaXNMZXZlbCkgeyBicmVhazsgfVxuICAgIH1cbiAgICBzdGFjay5sZW5ndGggPSBqICsgMTtcblxuICAgIGlmICh0b2tlbi50eXBlICE9PSAndGV4dCcpIHsgY29udGludWU7IH1cblxuICAgIHRleHQgPSB0b2tlbi5jb250ZW50O1xuICAgIHBvcyA9IDA7XG4gICAgbWF4ID0gdGV4dC5sZW5ndGg7XG5cbiAgICAvKmVzbGludCBuby1sYWJlbHM6MCxibG9jay1zY29wZWQtdmFyOjAqL1xuICAgIE9VVEVSOlxuICAgIHdoaWxlIChwb3MgPCBtYXgpIHtcbiAgICAgIFFVT1RFX1JFLmxhc3RJbmRleCA9IHBvcztcbiAgICAgIHQgPSBRVU9URV9SRS5leGVjKHRleHQpO1xuICAgICAgaWYgKCF0KSB7IGJyZWFrOyB9XG5cbiAgICAgIGNhbk9wZW4gPSBjYW5DbG9zZSA9IHRydWU7XG4gICAgICBwb3MgPSB0LmluZGV4ICsgMTtcbiAgICAgIGlzU2luZ2xlID0gKHRbMF0gPT09IFwiJ1wiKTtcblxuICAgICAgLy8gRmluZCBwcmV2aW91cyBjaGFyYWN0ZXIsXG4gICAgICAvLyBkZWZhdWx0IHRvIHNwYWNlIGlmIGl0J3MgdGhlIGJlZ2lubmluZyBvZiB0aGUgbGluZVxuICAgICAgLy9cbiAgICAgIGxhc3RDaGFyID0gMHgyMDtcblxuICAgICAgaWYgKHQuaW5kZXggLSAxID49IDApIHtcbiAgICAgICAgbGFzdENoYXIgPSB0ZXh0LmNoYXJDb2RlQXQodC5pbmRleCAtIDEpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgZm9yIChqID0gaSAtIDE7IGogPj0gMDsgai0tKSB7XG4gICAgICAgICAgaWYgKHRva2Vuc1tqXS50eXBlID09PSAnc29mdGJyZWFrJyB8fCB0b2tlbnNbal0udHlwZSA9PT0gJ2hhcmRicmVhaycpIGJyZWFrOyAvLyBsYXN0Q2hhciBkZWZhdWx0cyB0byAweDIwXG4gICAgICAgICAgaWYgKHRva2Vuc1tqXS50eXBlICE9PSAndGV4dCcpIGNvbnRpbnVlO1xuXG4gICAgICAgICAgbGFzdENoYXIgPSB0b2tlbnNbal0uY29udGVudC5jaGFyQ29kZUF0KHRva2Vuc1tqXS5jb250ZW50Lmxlbmd0aCAtIDEpO1xuICAgICAgICAgIGJyZWFrO1xuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIC8vIEZpbmQgbmV4dCBjaGFyYWN0ZXIsXG4gICAgICAvLyBkZWZhdWx0IHRvIHNwYWNlIGlmIGl0J3MgdGhlIGVuZCBvZiB0aGUgbGluZVxuICAgICAgLy9cbiAgICAgIG5leHRDaGFyID0gMHgyMDtcblxuICAgICAgaWYgKHBvcyA8IG1heCkge1xuICAgICAgICBuZXh0Q2hhciA9IHRleHQuY2hhckNvZGVBdChwb3MpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgZm9yIChqID0gaSArIDE7IGogPCB0b2tlbnMubGVuZ3RoOyBqKyspIHtcbiAgICAgICAgICBpZiAodG9rZW5zW2pdLnR5cGUgPT09ICdzb2Z0YnJlYWsnIHx8IHRva2Vuc1tqXS50eXBlID09PSAnaGFyZGJyZWFrJykgYnJlYWs7IC8vIG5leHRDaGFyIGRlZmF1bHRzIHRvIDB4MjBcbiAgICAgICAgICBpZiAodG9rZW5zW2pdLnR5cGUgIT09ICd0ZXh0JykgY29udGludWU7XG5cbiAgICAgICAgICBuZXh0Q2hhciA9IHRva2Vuc1tqXS5jb250ZW50LmNoYXJDb2RlQXQoMCk7XG4gICAgICAgICAgYnJlYWs7XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgaXNMYXN0UHVuY3RDaGFyID0gaXNNZEFzY2lpUHVuY3QobGFzdENoYXIpIHx8IGlzUHVuY3RDaGFyKFN0cmluZy5mcm9tQ2hhckNvZGUobGFzdENoYXIpKTtcbiAgICAgIGlzTmV4dFB1bmN0Q2hhciA9IGlzTWRBc2NpaVB1bmN0KG5leHRDaGFyKSB8fCBpc1B1bmN0Q2hhcihTdHJpbmcuZnJvbUNoYXJDb2RlKG5leHRDaGFyKSk7XG5cbiAgICAgIGlzTGFzdFdoaXRlU3BhY2UgPSBpc1doaXRlU3BhY2UobGFzdENoYXIpO1xuICAgICAgaXNOZXh0V2hpdGVTcGFjZSA9IGlzV2hpdGVTcGFjZShuZXh0Q2hhcik7XG5cbiAgICAgIGlmIChpc05leHRXaGl0ZVNwYWNlKSB7XG4gICAgICAgIGNhbk9wZW4gPSBmYWxzZTtcbiAgICAgIH0gZWxzZSBpZiAoaXNOZXh0UHVuY3RDaGFyKSB7XG4gICAgICAgIGlmICghKGlzTGFzdFdoaXRlU3BhY2UgfHwgaXNMYXN0UHVuY3RDaGFyKSkge1xuICAgICAgICAgIGNhbk9wZW4gPSBmYWxzZTtcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICBpZiAoaXNMYXN0V2hpdGVTcGFjZSkge1xuICAgICAgICBjYW5DbG9zZSA9IGZhbHNlO1xuICAgICAgfSBlbHNlIGlmIChpc0xhc3RQdW5jdENoYXIpIHtcbiAgICAgICAgaWYgKCEoaXNOZXh0V2hpdGVTcGFjZSB8fCBpc05leHRQdW5jdENoYXIpKSB7XG4gICAgICAgICAgY2FuQ2xvc2UgPSBmYWxzZTtcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICBpZiAobmV4dENoYXIgPT09IDB4MjIgLyogXCIgKi8gJiYgdFswXSA9PT0gJ1wiJykge1xuICAgICAgICBpZiAobGFzdENoYXIgPj0gMHgzMCAvKiAwICovICYmIGxhc3RDaGFyIDw9IDB4MzkgLyogOSAqLykge1xuICAgICAgICAgIC8vIHNwZWNpYWwgY2FzZTogMVwiXCIgLSBjb3VudCBmaXJzdCBxdW90ZSBhcyBhbiBpbmNoXG4gICAgICAgICAgY2FuQ2xvc2UgPSBjYW5PcGVuID0gZmFsc2U7XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgaWYgKGNhbk9wZW4gJiYgY2FuQ2xvc2UpIHtcbiAgICAgICAgLy8gdHJlYXQgdGhpcyBhcyB0aGUgbWlkZGxlIG9mIHRoZSB3b3JkXG4gICAgICAgIGNhbk9wZW4gPSBmYWxzZTtcbiAgICAgICAgY2FuQ2xvc2UgPSBpc05leHRQdW5jdENoYXI7XG4gICAgICB9XG5cbiAgICAgIGlmICghY2FuT3BlbiAmJiAhY2FuQ2xvc2UpIHtcbiAgICAgICAgLy8gbWlkZGxlIG9mIHdvcmRcbiAgICAgICAgaWYgKGlzU2luZ2xlKSB7XG4gICAgICAgICAgdG9rZW4uY29udGVudCA9IHJlcGxhY2VBdCh0b2tlbi5jb250ZW50LCB0LmluZGV4LCBBUE9TVFJPUEhFKTtcbiAgICAgICAgfVxuICAgICAgICBjb250aW51ZTtcbiAgICAgIH1cblxuICAgICAgaWYgKGNhbkNsb3NlKSB7XG4gICAgICAgIC8vIHRoaXMgY291bGQgYmUgYSBjbG9zaW5nIHF1b3RlLCByZXdpbmQgdGhlIHN0YWNrIHRvIGdldCBhIG1hdGNoXG4gICAgICAgIGZvciAoaiA9IHN0YWNrLmxlbmd0aCAtIDE7IGogPj0gMDsgai0tKSB7XG4gICAgICAgICAgaXRlbSA9IHN0YWNrW2pdO1xuICAgICAgICAgIGlmIChzdGFja1tqXS5sZXZlbCA8IHRoaXNMZXZlbCkgeyBicmVhazsgfVxuICAgICAgICAgIGlmIChpdGVtLnNpbmdsZSA9PT0gaXNTaW5nbGUgJiYgc3RhY2tbal0ubGV2ZWwgPT09IHRoaXNMZXZlbCkge1xuICAgICAgICAgICAgaXRlbSA9IHN0YWNrW2pdO1xuXG4gICAgICAgICAgICBpZiAoaXNTaW5nbGUpIHtcbiAgICAgICAgICAgICAgb3BlblF1b3RlID0gc3RhdGUubWQub3B0aW9ucy5xdW90ZXNbMl07XG4gICAgICAgICAgICAgIGNsb3NlUXVvdGUgPSBzdGF0ZS5tZC5vcHRpb25zLnF1b3Rlc1szXTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgIG9wZW5RdW90ZSA9IHN0YXRlLm1kLm9wdGlvbnMucXVvdGVzWzBdO1xuICAgICAgICAgICAgICBjbG9zZVF1b3RlID0gc3RhdGUubWQub3B0aW9ucy5xdW90ZXNbMV07XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIC8vIHJlcGxhY2UgdG9rZW4uY29udGVudCAqYmVmb3JlKiB0b2tlbnNbaXRlbS50b2tlbl0uY29udGVudCxcbiAgICAgICAgICAgIC8vIGJlY2F1c2UsIGlmIHRoZXkgYXJlIHBvaW50aW5nIGF0IHRoZSBzYW1lIHRva2VuLCByZXBsYWNlQXRcbiAgICAgICAgICAgIC8vIGNvdWxkIG1lc3MgdXAgaW5kaWNlcyB3aGVuIHF1b3RlIGxlbmd0aCAhPSAxXG4gICAgICAgICAgICB0b2tlbi5jb250ZW50ID0gcmVwbGFjZUF0KHRva2VuLmNvbnRlbnQsIHQuaW5kZXgsIGNsb3NlUXVvdGUpO1xuICAgICAgICAgICAgdG9rZW5zW2l0ZW0udG9rZW5dLmNvbnRlbnQgPSByZXBsYWNlQXQoXG4gICAgICAgICAgICAgIHRva2Vuc1tpdGVtLnRva2VuXS5jb250ZW50LCBpdGVtLnBvcywgb3BlblF1b3RlKTtcblxuICAgICAgICAgICAgcG9zICs9IGNsb3NlUXVvdGUubGVuZ3RoIC0gMTtcbiAgICAgICAgICAgIGlmIChpdGVtLnRva2VuID09PSBpKSB7IHBvcyArPSBvcGVuUXVvdGUubGVuZ3RoIC0gMTsgfVxuXG4gICAgICAgICAgICB0ZXh0ID0gdG9rZW4uY29udGVudDtcbiAgICAgICAgICAgIG1heCA9IHRleHQubGVuZ3RoO1xuXG4gICAgICAgICAgICBzdGFjay5sZW5ndGggPSBqO1xuICAgICAgICAgICAgY29udGludWUgT1VURVI7XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIGlmIChjYW5PcGVuKSB7XG4gICAgICAgIHN0YWNrLnB1c2goe1xuICAgICAgICAgIHRva2VuOiBpLFxuICAgICAgICAgIHBvczogdC5pbmRleCxcbiAgICAgICAgICBzaW5nbGU6IGlzU2luZ2xlLFxuICAgICAgICAgIGxldmVsOiB0aGlzTGV2ZWxcbiAgICAgICAgfSk7XG4gICAgICB9IGVsc2UgaWYgKGNhbkNsb3NlICYmIGlzU2luZ2xlKSB7XG4gICAgICAgIHRva2VuLmNvbnRlbnQgPSByZXBsYWNlQXQodG9rZW4uY29udGVudCwgdC5pbmRleCwgQVBPU1RST1BIRSk7XG4gICAgICB9XG4gICAgfVxuICB9XG59XG5cblxubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiBzbWFydHF1b3RlcyhzdGF0ZSkge1xuICAvKmVzbGludCBtYXgtZGVwdGg6MCovXG4gIHZhciBibGtJZHg7XG5cbiAgaWYgKCFzdGF0ZS5tZC5vcHRpb25zLnR5cG9ncmFwaGVyKSB7IHJldHVybjsgfVxuXG4gIGZvciAoYmxrSWR4ID0gc3RhdGUudG9rZW5zLmxlbmd0aCAtIDE7IGJsa0lkeCA+PSAwOyBibGtJZHgtLSkge1xuXG4gICAgaWYgKHN0YXRlLnRva2Vuc1tibGtJZHhdLnR5cGUgIT09ICdpbmxpbmUnIHx8XG4gICAgICAgICFRVU9URV9URVNUX1JFLnRlc3Qoc3RhdGUudG9rZW5zW2Jsa0lkeF0uY29udGVudCkpIHtcbiAgICAgIGNvbnRpbnVlO1xuICAgIH1cblxuICAgIHByb2Nlc3NfaW5saW5lcyhzdGF0ZS50b2tlbnNbYmxrSWR4XS5jaGlsZHJlbiwgc3RhdGUpO1xuICB9XG59O1xuIiwiLy8gQ29yZSBzdGF0ZSBvYmplY3Rcbi8vXG4ndXNlIHN0cmljdCc7XG5cbnZhciBUb2tlbiA9IHJlcXVpcmUoJy4uL3Rva2VuJyk7XG5cblxuZnVuY3Rpb24gU3RhdGVDb3JlKHNyYywgbWQsIGVudikge1xuICB0aGlzLnNyYyA9IHNyYztcbiAgdGhpcy5lbnYgPSBlbnY7XG4gIHRoaXMudG9rZW5zID0gW107XG4gIHRoaXMuaW5saW5lTW9kZSA9IGZhbHNlO1xuICB0aGlzLm1kID0gbWQ7IC8vIGxpbmsgdG8gcGFyc2VyIGluc3RhbmNlXG59XG5cbi8vIHJlLWV4cG9ydCBUb2tlbiBjbGFzcyB0byB1c2UgaW4gY29yZSBydWxlc1xuU3RhdGVDb3JlLnByb3RvdHlwZS5Ub2tlbiA9IFRva2VuO1xuXG5cbm1vZHVsZS5leHBvcnRzID0gU3RhdGVDb3JlO1xuIiwiLy8gUHJvY2VzcyBhdXRvbGlua3MgJzxwcm90b2NvbDouLi4+J1xuXG4ndXNlIHN0cmljdCc7XG5cblxuLyplc2xpbnQgbWF4LWxlbjowKi9cbnZhciBFTUFJTF9SRSAgICA9IC9ePChbYS16QS1aMC05LiEjJCUmJyorXFwvPT9eX2B7fH1+LV0rQFthLXpBLVowLTldKD86W2EtekEtWjAtOS1dezAsNjF9W2EtekEtWjAtOV0pPyg/OlxcLlthLXpBLVowLTldKD86W2EtekEtWjAtOS1dezAsNjF9W2EtekEtWjAtOV0pPykqKT4vO1xudmFyIEFVVE9MSU5LX1JFID0gL148KFthLXpBLVpdW2EtekEtWjAtOSsuXFwtXXsxLDMxfSk6KFtePD5cXHgwMC1cXHgyMF0qKT4vO1xuXG5cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gYXV0b2xpbmsoc3RhdGUsIHNpbGVudCkge1xuICB2YXIgdGFpbCwgbGlua01hdGNoLCBlbWFpbE1hdGNoLCB1cmwsIGZ1bGxVcmwsIHRva2VuLFxuICAgICAgcG9zID0gc3RhdGUucG9zO1xuXG4gIGlmIChzdGF0ZS5zcmMuY2hhckNvZGVBdChwb3MpICE9PSAweDNDLyogPCAqLykgeyByZXR1cm4gZmFsc2U7IH1cblxuICB0YWlsID0gc3RhdGUuc3JjLnNsaWNlKHBvcyk7XG5cbiAgaWYgKHRhaWwuaW5kZXhPZignPicpIDwgMCkgeyByZXR1cm4gZmFsc2U7IH1cblxuICBpZiAoQVVUT0xJTktfUkUudGVzdCh0YWlsKSkge1xuICAgIGxpbmtNYXRjaCA9IHRhaWwubWF0Y2goQVVUT0xJTktfUkUpO1xuXG4gICAgdXJsID0gbGlua01hdGNoWzBdLnNsaWNlKDEsIC0xKTtcbiAgICBmdWxsVXJsID0gc3RhdGUubWQubm9ybWFsaXplTGluayh1cmwpO1xuICAgIGlmICghc3RhdGUubWQudmFsaWRhdGVMaW5rKGZ1bGxVcmwpKSB7IHJldHVybiBmYWxzZTsgfVxuXG4gICAgaWYgKCFzaWxlbnQpIHtcbiAgICAgIHRva2VuICAgICAgICAgPSBzdGF0ZS5wdXNoKCdsaW5rX29wZW4nLCAnYScsIDEpO1xuICAgICAgdG9rZW4uYXR0cnMgICA9IFsgWyAnaHJlZicsIGZ1bGxVcmwgXSBdO1xuICAgICAgdG9rZW4ubWFya3VwICA9ICdhdXRvbGluayc7XG4gICAgICB0b2tlbi5pbmZvICAgID0gJ2F1dG8nO1xuXG4gICAgICB0b2tlbiAgICAgICAgID0gc3RhdGUucHVzaCgndGV4dCcsICcnLCAwKTtcbiAgICAgIHRva2VuLmNvbnRlbnQgPSBzdGF0ZS5tZC5ub3JtYWxpemVMaW5rVGV4dCh1cmwpO1xuXG4gICAgICB0b2tlbiAgICAgICAgID0gc3RhdGUucHVzaCgnbGlua19jbG9zZScsICdhJywgLTEpO1xuICAgICAgdG9rZW4ubWFya3VwICA9ICdhdXRvbGluayc7XG4gICAgICB0b2tlbi5pbmZvICAgID0gJ2F1dG8nO1xuICAgIH1cblxuICAgIHN0YXRlLnBvcyArPSBsaW5rTWF0Y2hbMF0ubGVuZ3RoO1xuICAgIHJldHVybiB0cnVlO1xuICB9XG5cbiAgaWYgKEVNQUlMX1JFLnRlc3QodGFpbCkpIHtcbiAgICBlbWFpbE1hdGNoID0gdGFpbC5tYXRjaChFTUFJTF9SRSk7XG5cbiAgICB1cmwgPSBlbWFpbE1hdGNoWzBdLnNsaWNlKDEsIC0xKTtcbiAgICBmdWxsVXJsID0gc3RhdGUubWQubm9ybWFsaXplTGluaygnbWFpbHRvOicgKyB1cmwpO1xuICAgIGlmICghc3RhdGUubWQudmFsaWRhdGVMaW5rKGZ1bGxVcmwpKSB7IHJldHVybiBmYWxzZTsgfVxuXG4gICAgaWYgKCFzaWxlbnQpIHtcbiAgICAgIHRva2VuICAgICAgICAgPSBzdGF0ZS5wdXNoKCdsaW5rX29wZW4nLCAnYScsIDEpO1xuICAgICAgdG9rZW4uYXR0cnMgICA9IFsgWyAnaHJlZicsIGZ1bGxVcmwgXSBdO1xuICAgICAgdG9rZW4ubWFya3VwICA9ICdhdXRvbGluayc7XG4gICAgICB0b2tlbi5pbmZvICAgID0gJ2F1dG8nO1xuXG4gICAgICB0b2tlbiAgICAgICAgID0gc3RhdGUucHVzaCgndGV4dCcsICcnLCAwKTtcbiAgICAgIHRva2VuLmNvbnRlbnQgPSBzdGF0ZS5tZC5ub3JtYWxpemVMaW5rVGV4dCh1cmwpO1xuXG4gICAgICB0b2tlbiAgICAgICAgID0gc3RhdGUucHVzaCgnbGlua19jbG9zZScsICdhJywgLTEpO1xuICAgICAgdG9rZW4ubWFya3VwICA9ICdhdXRvbGluayc7XG4gICAgICB0b2tlbi5pbmZvICAgID0gJ2F1dG8nO1xuICAgIH1cblxuICAgIHN0YXRlLnBvcyArPSBlbWFpbE1hdGNoWzBdLmxlbmd0aDtcbiAgICByZXR1cm4gdHJ1ZTtcbiAgfVxuXG4gIHJldHVybiBmYWxzZTtcbn07XG4iLCIvLyBQYXJzZSBiYWNrdGlja3NcblxuJ3VzZSBzdHJpY3QnO1xuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIGJhY2t0aWNrKHN0YXRlLCBzaWxlbnQpIHtcbiAgdmFyIHN0YXJ0LCBtYXgsIG1hcmtlciwgbWF0Y2hTdGFydCwgbWF0Y2hFbmQsIHRva2VuLFxuICAgICAgcG9zID0gc3RhdGUucG9zLFxuICAgICAgY2ggPSBzdGF0ZS5zcmMuY2hhckNvZGVBdChwb3MpO1xuXG4gIGlmIChjaCAhPT0gMHg2MC8qIGAgKi8pIHsgcmV0dXJuIGZhbHNlOyB9XG5cbiAgc3RhcnQgPSBwb3M7XG4gIHBvcysrO1xuICBtYXggPSBzdGF0ZS5wb3NNYXg7XG5cbiAgd2hpbGUgKHBvcyA8IG1heCAmJiBzdGF0ZS5zcmMuY2hhckNvZGVBdChwb3MpID09PSAweDYwLyogYCAqLykgeyBwb3MrKzsgfVxuXG4gIG1hcmtlciA9IHN0YXRlLnNyYy5zbGljZShzdGFydCwgcG9zKTtcblxuICBtYXRjaFN0YXJ0ID0gbWF0Y2hFbmQgPSBwb3M7XG5cbiAgd2hpbGUgKChtYXRjaFN0YXJ0ID0gc3RhdGUuc3JjLmluZGV4T2YoJ2AnLCBtYXRjaEVuZCkpICE9PSAtMSkge1xuICAgIG1hdGNoRW5kID0gbWF0Y2hTdGFydCArIDE7XG5cbiAgICB3aGlsZSAobWF0Y2hFbmQgPCBtYXggJiYgc3RhdGUuc3JjLmNoYXJDb2RlQXQobWF0Y2hFbmQpID09PSAweDYwLyogYCAqLykgeyBtYXRjaEVuZCsrOyB9XG5cbiAgICBpZiAobWF0Y2hFbmQgLSBtYXRjaFN0YXJ0ID09PSBtYXJrZXIubGVuZ3RoKSB7XG4gICAgICBpZiAoIXNpbGVudCkge1xuICAgICAgICB0b2tlbiAgICAgICAgID0gc3RhdGUucHVzaCgnY29kZV9pbmxpbmUnLCAnY29kZScsIDApO1xuICAgICAgICB0b2tlbi5tYXJrdXAgID0gbWFya2VyO1xuICAgICAgICB0b2tlbi5jb250ZW50ID0gc3RhdGUuc3JjLnNsaWNlKHBvcywgbWF0Y2hTdGFydClcbiAgICAgICAgICAucmVwbGFjZSgvXFxuL2csICcgJylcbiAgICAgICAgICAucmVwbGFjZSgvXiAoLispICQvLCAnJDEnKTtcbiAgICAgIH1cbiAgICAgIHN0YXRlLnBvcyA9IG1hdGNoRW5kO1xuICAgICAgcmV0dXJuIHRydWU7XG4gICAgfVxuICB9XG5cbiAgaWYgKCFzaWxlbnQpIHsgc3RhdGUucGVuZGluZyArPSBtYXJrZXI7IH1cbiAgc3RhdGUucG9zICs9IG1hcmtlci5sZW5ndGg7XG4gIHJldHVybiB0cnVlO1xufTtcbiIsIi8vIEZvciBlYWNoIG9wZW5pbmcgZW1waGFzaXMtbGlrZSBtYXJrZXIgZmluZCBhIG1hdGNoaW5nIGNsb3Npbmcgb25lXG4vL1xuJ3VzZSBzdHJpY3QnO1xuXG5cbmZ1bmN0aW9uIHByb2Nlc3NEZWxpbWl0ZXJzKHN0YXRlLCBkZWxpbWl0ZXJzKSB7XG4gIHZhciBjbG9zZXJJZHgsIG9wZW5lcklkeCwgY2xvc2VyLCBvcGVuZXIsIG1pbk9wZW5lcklkeCwgbmV3TWluT3BlbmVySWR4LFxuICAgICAgaXNPZGRNYXRjaCwgbGFzdEp1bXAsXG4gICAgICBvcGVuZXJzQm90dG9tID0ge30sXG4gICAgICBtYXggPSBkZWxpbWl0ZXJzLmxlbmd0aDtcblxuICBmb3IgKGNsb3NlcklkeCA9IDA7IGNsb3NlcklkeCA8IG1heDsgY2xvc2VySWR4KyspIHtcbiAgICBjbG9zZXIgPSBkZWxpbWl0ZXJzW2Nsb3NlcklkeF07XG5cbiAgICAvLyBMZW5ndGggaXMgb25seSB1c2VkIGZvciBlbXBoYXNpcy1zcGVjaWZpYyBcInJ1bGUgb2YgM1wiLFxuICAgIC8vIGlmIGl0J3Mgbm90IGRlZmluZWQgKGluIHN0cmlrZXRocm91Z2ggb3IgM3JkIHBhcnR5IHBsdWdpbnMpLFxuICAgIC8vIHdlIGNhbiBkZWZhdWx0IGl0IHRvIDAgdG8gZGlzYWJsZSB0aG9zZSBjaGVja3MuXG4gICAgLy9cbiAgICBjbG9zZXIubGVuZ3RoID0gY2xvc2VyLmxlbmd0aCB8fCAwO1xuXG4gICAgaWYgKCFjbG9zZXIuY2xvc2UpIGNvbnRpbnVlO1xuXG4gICAgLy8gUHJldmlvdXNseSBjYWxjdWxhdGVkIGxvd2VyIGJvdW5kcyAocHJldmlvdXMgZmFpbHMpXG4gICAgLy8gZm9yIGVhY2ggbWFya2VyIGFuZCBlYWNoIGRlbGltaXRlciBsZW5ndGggbW9kdWxvIDMuXG4gICAgaWYgKCFvcGVuZXJzQm90dG9tLmhhc093blByb3BlcnR5KGNsb3Nlci5tYXJrZXIpKSB7XG4gICAgICBvcGVuZXJzQm90dG9tW2Nsb3Nlci5tYXJrZXJdID0gWyAtMSwgLTEsIC0xIF07XG4gICAgfVxuXG4gICAgbWluT3BlbmVySWR4ID0gb3BlbmVyc0JvdHRvbVtjbG9zZXIubWFya2VyXVtjbG9zZXIubGVuZ3RoICUgM107XG4gICAgbmV3TWluT3BlbmVySWR4ID0gLTE7XG5cbiAgICBvcGVuZXJJZHggPSBjbG9zZXJJZHggLSBjbG9zZXIuanVtcCAtIDE7XG5cbiAgICBmb3IgKDsgb3BlbmVySWR4ID4gbWluT3BlbmVySWR4OyBvcGVuZXJJZHggLT0gb3BlbmVyLmp1bXAgKyAxKSB7XG4gICAgICBvcGVuZXIgPSBkZWxpbWl0ZXJzW29wZW5lcklkeF07XG5cbiAgICAgIGlmIChvcGVuZXIubWFya2VyICE9PSBjbG9zZXIubWFya2VyKSBjb250aW51ZTtcblxuICAgICAgaWYgKG5ld01pbk9wZW5lcklkeCA9PT0gLTEpIG5ld01pbk9wZW5lcklkeCA9IG9wZW5lcklkeDtcblxuICAgICAgaWYgKG9wZW5lci5vcGVuICYmXG4gICAgICAgICAgb3BlbmVyLmVuZCA8IDAgJiZcbiAgICAgICAgICBvcGVuZXIubGV2ZWwgPT09IGNsb3Nlci5sZXZlbCkge1xuXG4gICAgICAgIGlzT2RkTWF0Y2ggPSBmYWxzZTtcblxuICAgICAgICAvLyBmcm9tIHNwZWM6XG4gICAgICAgIC8vXG4gICAgICAgIC8vIElmIG9uZSBvZiB0aGUgZGVsaW1pdGVycyBjYW4gYm90aCBvcGVuIGFuZCBjbG9zZSBlbXBoYXNpcywgdGhlbiB0aGVcbiAgICAgICAgLy8gc3VtIG9mIHRoZSBsZW5ndGhzIG9mIHRoZSBkZWxpbWl0ZXIgcnVucyBjb250YWluaW5nIHRoZSBvcGVuaW5nIGFuZFxuICAgICAgICAvLyBjbG9zaW5nIGRlbGltaXRlcnMgbXVzdCBub3QgYmUgYSBtdWx0aXBsZSBvZiAzIHVubGVzcyBib3RoIGxlbmd0aHNcbiAgICAgICAgLy8gYXJlIG11bHRpcGxlcyBvZiAzLlxuICAgICAgICAvL1xuICAgICAgICBpZiAob3BlbmVyLmNsb3NlIHx8IGNsb3Nlci5vcGVuKSB7XG4gICAgICAgICAgaWYgKChvcGVuZXIubGVuZ3RoICsgY2xvc2VyLmxlbmd0aCkgJSAzID09PSAwKSB7XG4gICAgICAgICAgICBpZiAob3BlbmVyLmxlbmd0aCAlIDMgIT09IDAgfHwgY2xvc2VyLmxlbmd0aCAlIDMgIT09IDApIHtcbiAgICAgICAgICAgICAgaXNPZGRNYXRjaCA9IHRydWU7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICAgICAgaWYgKCFpc09kZE1hdGNoKSB7XG4gICAgICAgICAgLy8gSWYgcHJldmlvdXMgZGVsaW1pdGVyIGNhbm5vdCBiZSBhbiBvcGVuZXIsIHdlIGNhbiBzYWZlbHkgc2tpcFxuICAgICAgICAgIC8vIHRoZSBlbnRpcmUgc2VxdWVuY2UgaW4gZnV0dXJlIGNoZWNrcy4gVGhpcyBpcyByZXF1aXJlZCB0byBtYWtlXG4gICAgICAgICAgLy8gc3VyZSBhbGdvcml0aG0gaGFzIGxpbmVhciBjb21wbGV4aXR5IChzZWUgKl8qXypfKl8qXy4uLiBjYXNlKS5cbiAgICAgICAgICAvL1xuICAgICAgICAgIGxhc3RKdW1wID0gb3BlbmVySWR4ID4gMCAmJiAhZGVsaW1pdGVyc1tvcGVuZXJJZHggLSAxXS5vcGVuID9cbiAgICAgICAgICAgIGRlbGltaXRlcnNbb3BlbmVySWR4IC0gMV0uanVtcCArIDEgOlxuICAgICAgICAgICAgMDtcblxuICAgICAgICAgIGNsb3Nlci5qdW1wICA9IGNsb3NlcklkeCAtIG9wZW5lcklkeCArIGxhc3RKdW1wO1xuICAgICAgICAgIGNsb3Nlci5vcGVuICA9IGZhbHNlO1xuICAgICAgICAgIG9wZW5lci5lbmQgICA9IGNsb3NlcklkeDtcbiAgICAgICAgICBvcGVuZXIuanVtcCAgPSBsYXN0SnVtcDtcbiAgICAgICAgICBvcGVuZXIuY2xvc2UgPSBmYWxzZTtcbiAgICAgICAgICBuZXdNaW5PcGVuZXJJZHggPSAtMTtcbiAgICAgICAgICBicmVhaztcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cblxuICAgIGlmIChuZXdNaW5PcGVuZXJJZHggIT09IC0xKSB7XG4gICAgICAvLyBJZiBtYXRjaCBmb3IgdGhpcyBkZWxpbWl0ZXIgcnVuIGZhaWxlZCwgd2Ugd2FudCB0byBzZXQgbG93ZXIgYm91bmQgZm9yXG4gICAgICAvLyBmdXR1cmUgbG9va3Vwcy4gVGhpcyBpcyByZXF1aXJlZCB0byBtYWtlIHN1cmUgYWxnb3JpdGhtIGhhcyBsaW5lYXJcbiAgICAgIC8vIGNvbXBsZXhpdHkuXG4gICAgICAvL1xuICAgICAgLy8gU2VlIGRldGFpbHMgaGVyZTpcbiAgICAgIC8vIGh0dHBzOi8vZ2l0aHViLmNvbS9jb21tb25tYXJrL2NtYXJrL2lzc3Vlcy8xNzgjaXNzdWVjb21tZW50LTI3MDQxNzQ0MlxuICAgICAgLy9cbiAgICAgIG9wZW5lcnNCb3R0b21bY2xvc2VyLm1hcmtlcl1bKGNsb3Nlci5sZW5ndGggfHwgMCkgJSAzXSA9IG5ld01pbk9wZW5lcklkeDtcbiAgICB9XG4gIH1cbn1cblxuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIGxpbmtfcGFpcnMoc3RhdGUpIHtcbiAgdmFyIGN1cnIsXG4gICAgICB0b2tlbnNfbWV0YSA9IHN0YXRlLnRva2Vuc19tZXRhLFxuICAgICAgbWF4ID0gc3RhdGUudG9rZW5zX21ldGEubGVuZ3RoO1xuXG4gIHByb2Nlc3NEZWxpbWl0ZXJzKHN0YXRlLCBzdGF0ZS5kZWxpbWl0ZXJzKTtcblxuICBmb3IgKGN1cnIgPSAwOyBjdXJyIDwgbWF4OyBjdXJyKyspIHtcbiAgICBpZiAodG9rZW5zX21ldGFbY3Vycl0gJiYgdG9rZW5zX21ldGFbY3Vycl0uZGVsaW1pdGVycykge1xuICAgICAgcHJvY2Vzc0RlbGltaXRlcnMoc3RhdGUsIHRva2Vuc19tZXRhW2N1cnJdLmRlbGltaXRlcnMpO1xuICAgIH1cbiAgfVxufTtcbiIsIi8vIFByb2Nlc3MgKnRoaXMqIGFuZCBfdGhhdF9cbi8vXG4ndXNlIHN0cmljdCc7XG5cblxuLy8gSW5zZXJ0IGVhY2ggbWFya2VyIGFzIGEgc2VwYXJhdGUgdGV4dCB0b2tlbiwgYW5kIGFkZCBpdCB0byBkZWxpbWl0ZXIgbGlzdFxuLy9cbm1vZHVsZS5leHBvcnRzLnRva2VuaXplID0gZnVuY3Rpb24gZW1waGFzaXMoc3RhdGUsIHNpbGVudCkge1xuICB2YXIgaSwgc2Nhbm5lZCwgdG9rZW4sXG4gICAgICBzdGFydCA9IHN0YXRlLnBvcyxcbiAgICAgIG1hcmtlciA9IHN0YXRlLnNyYy5jaGFyQ29kZUF0KHN0YXJ0KTtcblxuICBpZiAoc2lsZW50KSB7IHJldHVybiBmYWxzZTsgfVxuXG4gIGlmIChtYXJrZXIgIT09IDB4NUYgLyogXyAqLyAmJiBtYXJrZXIgIT09IDB4MkEgLyogKiAqLykgeyByZXR1cm4gZmFsc2U7IH1cblxuICBzY2FubmVkID0gc3RhdGUuc2NhbkRlbGltcyhzdGF0ZS5wb3MsIG1hcmtlciA9PT0gMHgyQSk7XG5cbiAgZm9yIChpID0gMDsgaSA8IHNjYW5uZWQubGVuZ3RoOyBpKyspIHtcbiAgICB0b2tlbiAgICAgICAgID0gc3RhdGUucHVzaCgndGV4dCcsICcnLCAwKTtcbiAgICB0b2tlbi5jb250ZW50ID0gU3RyaW5nLmZyb21DaGFyQ29kZShtYXJrZXIpO1xuXG4gICAgc3RhdGUuZGVsaW1pdGVycy5wdXNoKHtcbiAgICAgIC8vIENoYXIgY29kZSBvZiB0aGUgc3RhcnRpbmcgbWFya2VyIChudW1iZXIpLlxuICAgICAgLy9cbiAgICAgIG1hcmtlcjogbWFya2VyLFxuXG4gICAgICAvLyBUb3RhbCBsZW5ndGggb2YgdGhlc2Ugc2VyaWVzIG9mIGRlbGltaXRlcnMuXG4gICAgICAvL1xuICAgICAgbGVuZ3RoOiBzY2FubmVkLmxlbmd0aCxcblxuICAgICAgLy8gQW4gYW1vdW50IG9mIGNoYXJhY3RlcnMgYmVmb3JlIHRoaXMgb25lIHRoYXQncyBlcXVpdmFsZW50IHRvXG4gICAgICAvLyBjdXJyZW50IG9uZS4gSW4gcGxhaW4gRW5nbGlzaDogaWYgdGhpcyBkZWxpbWl0ZXIgZG9lcyBub3Qgb3BlblxuICAgICAgLy8gYW4gZW1waGFzaXMsIG5laXRoZXIgZG8gcHJldmlvdXMgYGp1bXBgIGNoYXJhY3RlcnMuXG4gICAgICAvL1xuICAgICAgLy8gVXNlZCB0byBza2lwIHNlcXVlbmNlcyBsaWtlIFwiKioqKipcIiBpbiBvbmUgc3RlcCwgZm9yIDFzdCBhc3Rlcmlza1xuICAgICAgLy8gdmFsdWUgd2lsbCBiZSAwLCBmb3IgMm5kIGl0J3MgMSBhbmQgc28gb24uXG4gICAgICAvL1xuICAgICAganVtcDogICBpLFxuXG4gICAgICAvLyBBIHBvc2l0aW9uIG9mIHRoZSB0b2tlbiB0aGlzIGRlbGltaXRlciBjb3JyZXNwb25kcyB0by5cbiAgICAgIC8vXG4gICAgICB0b2tlbjogIHN0YXRlLnRva2Vucy5sZW5ndGggLSAxLFxuXG4gICAgICAvLyBJZiB0aGlzIGRlbGltaXRlciBpcyBtYXRjaGVkIGFzIGEgdmFsaWQgb3BlbmVyLCBgZW5kYCB3aWxsIGJlXG4gICAgICAvLyBlcXVhbCB0byBpdHMgcG9zaXRpb24sIG90aGVyd2lzZSBpdCdzIGAtMWAuXG4gICAgICAvL1xuICAgICAgZW5kOiAgICAtMSxcblxuICAgICAgLy8gQm9vbGVhbiBmbGFncyB0aGF0IGRldGVybWluZSBpZiB0aGlzIGRlbGltaXRlciBjb3VsZCBvcGVuIG9yIGNsb3NlXG4gICAgICAvLyBhbiBlbXBoYXNpcy5cbiAgICAgIC8vXG4gICAgICBvcGVuOiAgIHNjYW5uZWQuY2FuX29wZW4sXG4gICAgICBjbG9zZTogIHNjYW5uZWQuY2FuX2Nsb3NlXG4gICAgfSk7XG4gIH1cblxuICBzdGF0ZS5wb3MgKz0gc2Nhbm5lZC5sZW5ndGg7XG5cbiAgcmV0dXJuIHRydWU7XG59O1xuXG5cbmZ1bmN0aW9uIHBvc3RQcm9jZXNzKHN0YXRlLCBkZWxpbWl0ZXJzKSB7XG4gIHZhciBpLFxuICAgICAgc3RhcnREZWxpbSxcbiAgICAgIGVuZERlbGltLFxuICAgICAgdG9rZW4sXG4gICAgICBjaCxcbiAgICAgIGlzU3Ryb25nLFxuICAgICAgbWF4ID0gZGVsaW1pdGVycy5sZW5ndGg7XG5cbiAgZm9yIChpID0gbWF4IC0gMTsgaSA+PSAwOyBpLS0pIHtcbiAgICBzdGFydERlbGltID0gZGVsaW1pdGVyc1tpXTtcblxuICAgIGlmIChzdGFydERlbGltLm1hcmtlciAhPT0gMHg1Ri8qIF8gKi8gJiYgc3RhcnREZWxpbS5tYXJrZXIgIT09IDB4MkEvKiAqICovKSB7XG4gICAgICBjb250aW51ZTtcbiAgICB9XG5cbiAgICAvLyBQcm9jZXNzIG9ubHkgb3BlbmluZyBtYXJrZXJzXG4gICAgaWYgKHN0YXJ0RGVsaW0uZW5kID09PSAtMSkge1xuICAgICAgY29udGludWU7XG4gICAgfVxuXG4gICAgZW5kRGVsaW0gPSBkZWxpbWl0ZXJzW3N0YXJ0RGVsaW0uZW5kXTtcblxuICAgIC8vIElmIHRoZSBwcmV2aW91cyBkZWxpbWl0ZXIgaGFzIHRoZSBzYW1lIG1hcmtlciBhbmQgaXMgYWRqYWNlbnQgdG8gdGhpcyBvbmUsXG4gICAgLy8gbWVyZ2UgdGhvc2UgaW50byBvbmUgc3Ryb25nIGRlbGltaXRlci5cbiAgICAvL1xuICAgIC8vIGA8ZW0+PGVtPndoYXRldmVyPC9lbT48L2VtPmAgLT4gYDxzdHJvbmc+d2hhdGV2ZXI8L3N0cm9uZz5gXG4gICAgLy9cbiAgICBpc1N0cm9uZyA9IGkgPiAwICYmXG4gICAgICAgICAgICAgICBkZWxpbWl0ZXJzW2kgLSAxXS5lbmQgPT09IHN0YXJ0RGVsaW0uZW5kICsgMSAmJlxuICAgICAgICAgICAgICAgZGVsaW1pdGVyc1tpIC0gMV0udG9rZW4gPT09IHN0YXJ0RGVsaW0udG9rZW4gLSAxICYmXG4gICAgICAgICAgICAgICBkZWxpbWl0ZXJzW3N0YXJ0RGVsaW0uZW5kICsgMV0udG9rZW4gPT09IGVuZERlbGltLnRva2VuICsgMSAmJlxuICAgICAgICAgICAgICAgZGVsaW1pdGVyc1tpIC0gMV0ubWFya2VyID09PSBzdGFydERlbGltLm1hcmtlcjtcblxuICAgIGNoID0gU3RyaW5nLmZyb21DaGFyQ29kZShzdGFydERlbGltLm1hcmtlcik7XG5cbiAgICB0b2tlbiAgICAgICAgID0gc3RhdGUudG9rZW5zW3N0YXJ0RGVsaW0udG9rZW5dO1xuICAgIHRva2VuLnR5cGUgICAgPSBpc1N0cm9uZyA/ICdzdHJvbmdfb3BlbicgOiAnZW1fb3Blbic7XG4gICAgdG9rZW4udGFnICAgICA9IGlzU3Ryb25nID8gJ3N0cm9uZycgOiAnZW0nO1xuICAgIHRva2VuLm5lc3RpbmcgPSAxO1xuICAgIHRva2VuLm1hcmt1cCAgPSBpc1N0cm9uZyA/IGNoICsgY2ggOiBjaDtcbiAgICB0b2tlbi5jb250ZW50ID0gJyc7XG5cbiAgICB0b2tlbiAgICAgICAgID0gc3RhdGUudG9rZW5zW2VuZERlbGltLnRva2VuXTtcbiAgICB0b2tlbi50eXBlICAgID0gaXNTdHJvbmcgPyAnc3Ryb25nX2Nsb3NlJyA6ICdlbV9jbG9zZSc7XG4gICAgdG9rZW4udGFnICAgICA9IGlzU3Ryb25nID8gJ3N0cm9uZycgOiAnZW0nO1xuICAgIHRva2VuLm5lc3RpbmcgPSAtMTtcbiAgICB0b2tlbi5tYXJrdXAgID0gaXNTdHJvbmcgPyBjaCArIGNoIDogY2g7XG4gICAgdG9rZW4uY29udGVudCA9ICcnO1xuXG4gICAgaWYgKGlzU3Ryb25nKSB7XG4gICAgICBzdGF0ZS50b2tlbnNbZGVsaW1pdGVyc1tpIC0gMV0udG9rZW5dLmNvbnRlbnQgPSAnJztcbiAgICAgIHN0YXRlLnRva2Vuc1tkZWxpbWl0ZXJzW3N0YXJ0RGVsaW0uZW5kICsgMV0udG9rZW5dLmNvbnRlbnQgPSAnJztcbiAgICAgIGktLTtcbiAgICB9XG4gIH1cbn1cblxuXG4vLyBXYWxrIHRocm91Z2ggZGVsaW1pdGVyIGxpc3QgYW5kIHJlcGxhY2UgdGV4dCB0b2tlbnMgd2l0aCB0YWdzXG4vL1xubW9kdWxlLmV4cG9ydHMucG9zdFByb2Nlc3MgPSBmdW5jdGlvbiBlbXBoYXNpcyhzdGF0ZSkge1xuICB2YXIgY3VycixcbiAgICAgIHRva2Vuc19tZXRhID0gc3RhdGUudG9rZW5zX21ldGEsXG4gICAgICBtYXggPSBzdGF0ZS50b2tlbnNfbWV0YS5sZW5ndGg7XG5cbiAgcG9zdFByb2Nlc3Moc3RhdGUsIHN0YXRlLmRlbGltaXRlcnMpO1xuXG4gIGZvciAoY3VyciA9IDA7IGN1cnIgPCBtYXg7IGN1cnIrKykge1xuICAgIGlmICh0b2tlbnNfbWV0YVtjdXJyXSAmJiB0b2tlbnNfbWV0YVtjdXJyXS5kZWxpbWl0ZXJzKSB7XG4gICAgICBwb3N0UHJvY2VzcyhzdGF0ZSwgdG9rZW5zX21ldGFbY3Vycl0uZGVsaW1pdGVycyk7XG4gICAgfVxuICB9XG59O1xuIiwiLy8gUHJvY2VzcyBodG1sIGVudGl0eSAtICYjMTIzOywgJiN4QUY7LCAmcXVvdDssIC4uLlxuXG4ndXNlIHN0cmljdCc7XG5cbnZhciBlbnRpdGllcyAgICAgICAgICA9IHJlcXVpcmUoJy4uL2NvbW1vbi9lbnRpdGllcycpO1xudmFyIGhhcyAgICAgICAgICAgICAgID0gcmVxdWlyZSgnLi4vY29tbW9uL3V0aWxzJykuaGFzO1xudmFyIGlzVmFsaWRFbnRpdHlDb2RlID0gcmVxdWlyZSgnLi4vY29tbW9uL3V0aWxzJykuaXNWYWxpZEVudGl0eUNvZGU7XG52YXIgZnJvbUNvZGVQb2ludCAgICAgPSByZXF1aXJlKCcuLi9jb21tb24vdXRpbHMnKS5mcm9tQ29kZVBvaW50O1xuXG5cbnZhciBESUdJVEFMX1JFID0gL14mIygoPzp4W2EtZjAtOV17MSw2fXxbMC05XXsxLDd9KSk7L2k7XG52YXIgTkFNRURfUkUgICA9IC9eJihbYS16XVthLXowLTldezEsMzF9KTsvaTtcblxuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIGVudGl0eShzdGF0ZSwgc2lsZW50KSB7XG4gIHZhciBjaCwgY29kZSwgbWF0Y2gsIHBvcyA9IHN0YXRlLnBvcywgbWF4ID0gc3RhdGUucG9zTWF4O1xuXG4gIGlmIChzdGF0ZS5zcmMuY2hhckNvZGVBdChwb3MpICE9PSAweDI2LyogJiAqLykgeyByZXR1cm4gZmFsc2U7IH1cblxuICBpZiAocG9zICsgMSA8IG1heCkge1xuICAgIGNoID0gc3RhdGUuc3JjLmNoYXJDb2RlQXQocG9zICsgMSk7XG5cbiAgICBpZiAoY2ggPT09IDB4MjMgLyogIyAqLykge1xuICAgICAgbWF0Y2ggPSBzdGF0ZS5zcmMuc2xpY2UocG9zKS5tYXRjaChESUdJVEFMX1JFKTtcbiAgICAgIGlmIChtYXRjaCkge1xuICAgICAgICBpZiAoIXNpbGVudCkge1xuICAgICAgICAgIGNvZGUgPSBtYXRjaFsxXVswXS50b0xvd2VyQ2FzZSgpID09PSAneCcgPyBwYXJzZUludChtYXRjaFsxXS5zbGljZSgxKSwgMTYpIDogcGFyc2VJbnQobWF0Y2hbMV0sIDEwKTtcbiAgICAgICAgICBzdGF0ZS5wZW5kaW5nICs9IGlzVmFsaWRFbnRpdHlDb2RlKGNvZGUpID8gZnJvbUNvZGVQb2ludChjb2RlKSA6IGZyb21Db2RlUG9pbnQoMHhGRkZEKTtcbiAgICAgICAgfVxuICAgICAgICBzdGF0ZS5wb3MgKz0gbWF0Y2hbMF0ubGVuZ3RoO1xuICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgIH1cbiAgICB9IGVsc2Uge1xuICAgICAgbWF0Y2ggPSBzdGF0ZS5zcmMuc2xpY2UocG9zKS5tYXRjaChOQU1FRF9SRSk7XG4gICAgICBpZiAobWF0Y2gpIHtcbiAgICAgICAgaWYgKGhhcyhlbnRpdGllcywgbWF0Y2hbMV0pKSB7XG4gICAgICAgICAgaWYgKCFzaWxlbnQpIHsgc3RhdGUucGVuZGluZyArPSBlbnRpdGllc1ttYXRjaFsxXV07IH1cbiAgICAgICAgICBzdGF0ZS5wb3MgKz0gbWF0Y2hbMF0ubGVuZ3RoO1xuICAgICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgaWYgKCFzaWxlbnQpIHsgc3RhdGUucGVuZGluZyArPSAnJic7IH1cbiAgc3RhdGUucG9zKys7XG4gIHJldHVybiB0cnVlO1xufTtcbiIsIi8vIFByb2Nlc3MgZXNjYXBlZCBjaGFycyBhbmQgaGFyZGJyZWFrc1xuXG4ndXNlIHN0cmljdCc7XG5cbnZhciBpc1NwYWNlID0gcmVxdWlyZSgnLi4vY29tbW9uL3V0aWxzJykuaXNTcGFjZTtcblxudmFyIEVTQ0FQRUQgPSBbXTtcblxuZm9yICh2YXIgaSA9IDA7IGkgPCAyNTY7IGkrKykgeyBFU0NBUEVELnB1c2goMCk7IH1cblxuJ1xcXFwhXCIjJCUmXFwnKCkqKywuLzo7PD0+P0BbXV5fYHt8fX4tJ1xuICAuc3BsaXQoJycpLmZvckVhY2goZnVuY3Rpb24gKGNoKSB7IEVTQ0FQRURbY2guY2hhckNvZGVBdCgwKV0gPSAxOyB9KTtcblxuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIGVzY2FwZShzdGF0ZSwgc2lsZW50KSB7XG4gIHZhciBjaCwgcG9zID0gc3RhdGUucG9zLCBtYXggPSBzdGF0ZS5wb3NNYXg7XG5cbiAgaWYgKHN0YXRlLnNyYy5jaGFyQ29kZUF0KHBvcykgIT09IDB4NUMvKiBcXCAqLykgeyByZXR1cm4gZmFsc2U7IH1cblxuICBwb3MrKztcblxuICBpZiAocG9zIDwgbWF4KSB7XG4gICAgY2ggPSBzdGF0ZS5zcmMuY2hhckNvZGVBdChwb3MpO1xuXG4gICAgaWYgKGNoIDwgMjU2ICYmIEVTQ0FQRURbY2hdICE9PSAwKSB7XG4gICAgICBpZiAoIXNpbGVudCkgeyBzdGF0ZS5wZW5kaW5nICs9IHN0YXRlLnNyY1twb3NdOyB9XG4gICAgICBzdGF0ZS5wb3MgKz0gMjtcbiAgICAgIHJldHVybiB0cnVlO1xuICAgIH1cblxuICAgIGlmIChjaCA9PT0gMHgwQSkge1xuICAgICAgaWYgKCFzaWxlbnQpIHtcbiAgICAgICAgc3RhdGUucHVzaCgnaGFyZGJyZWFrJywgJ2JyJywgMCk7XG4gICAgICB9XG5cbiAgICAgIHBvcysrO1xuICAgICAgLy8gc2tpcCBsZWFkaW5nIHdoaXRlc3BhY2VzIGZyb20gbmV4dCBsaW5lXG4gICAgICB3aGlsZSAocG9zIDwgbWF4KSB7XG4gICAgICAgIGNoID0gc3RhdGUuc3JjLmNoYXJDb2RlQXQocG9zKTtcbiAgICAgICAgaWYgKCFpc1NwYWNlKGNoKSkgeyBicmVhazsgfVxuICAgICAgICBwb3MrKztcbiAgICAgIH1cblxuICAgICAgc3RhdGUucG9zID0gcG9zO1xuICAgICAgcmV0dXJuIHRydWU7XG4gICAgfVxuICB9XG5cbiAgaWYgKCFzaWxlbnQpIHsgc3RhdGUucGVuZGluZyArPSAnXFxcXCc7IH1cbiAgc3RhdGUucG9zKys7XG4gIHJldHVybiB0cnVlO1xufTtcbiIsIi8vIFByb2Nlc3MgaHRtbCB0YWdzXG5cbid1c2Ugc3RyaWN0JztcblxuXG52YXIgSFRNTF9UQUdfUkUgPSByZXF1aXJlKCcuLi9jb21tb24vaHRtbF9yZScpLkhUTUxfVEFHX1JFO1xuXG5cbmZ1bmN0aW9uIGlzTGV0dGVyKGNoKSB7XG4gIC8qZXNsaW50IG5vLWJpdHdpc2U6MCovXG4gIHZhciBsYyA9IGNoIHwgMHgyMDsgLy8gdG8gbG93ZXIgY2FzZVxuICByZXR1cm4gKGxjID49IDB4NjEvKiBhICovKSAmJiAobGMgPD0gMHg3YS8qIHogKi8pO1xufVxuXG5cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gaHRtbF9pbmxpbmUoc3RhdGUsIHNpbGVudCkge1xuICB2YXIgY2gsIG1hdGNoLCBtYXgsIHRva2VuLFxuICAgICAgcG9zID0gc3RhdGUucG9zO1xuXG4gIGlmICghc3RhdGUubWQub3B0aW9ucy5odG1sKSB7IHJldHVybiBmYWxzZTsgfVxuXG4gIC8vIENoZWNrIHN0YXJ0XG4gIG1heCA9IHN0YXRlLnBvc01heDtcbiAgaWYgKHN0YXRlLnNyYy5jaGFyQ29kZUF0KHBvcykgIT09IDB4M0MvKiA8ICovIHx8XG4gICAgICBwb3MgKyAyID49IG1heCkge1xuICAgIHJldHVybiBmYWxzZTtcbiAgfVxuXG4gIC8vIFF1aWNrIGZhaWwgb24gc2Vjb25kIGNoYXJcbiAgY2ggPSBzdGF0ZS5zcmMuY2hhckNvZGVBdChwb3MgKyAxKTtcbiAgaWYgKGNoICE9PSAweDIxLyogISAqLyAmJlxuICAgICAgY2ggIT09IDB4M0YvKiA/ICovICYmXG4gICAgICBjaCAhPT0gMHgyRi8qIC8gKi8gJiZcbiAgICAgICFpc0xldHRlcihjaCkpIHtcbiAgICByZXR1cm4gZmFsc2U7XG4gIH1cblxuICBtYXRjaCA9IHN0YXRlLnNyYy5zbGljZShwb3MpLm1hdGNoKEhUTUxfVEFHX1JFKTtcbiAgaWYgKCFtYXRjaCkgeyByZXR1cm4gZmFsc2U7IH1cblxuICBpZiAoIXNpbGVudCkge1xuICAgIHRva2VuICAgICAgICAgPSBzdGF0ZS5wdXNoKCdodG1sX2lubGluZScsICcnLCAwKTtcbiAgICB0b2tlbi5jb250ZW50ID0gc3RhdGUuc3JjLnNsaWNlKHBvcywgcG9zICsgbWF0Y2hbMF0ubGVuZ3RoKTtcbiAgfVxuICBzdGF0ZS5wb3MgKz0gbWF0Y2hbMF0ubGVuZ3RoO1xuICByZXR1cm4gdHJ1ZTtcbn07XG4iLCIvLyBQcm9jZXNzICFbaW1hZ2VdKDxzcmM+IFwidGl0bGVcIilcblxuJ3VzZSBzdHJpY3QnO1xuXG52YXIgbm9ybWFsaXplUmVmZXJlbmNlICAgPSByZXF1aXJlKCcuLi9jb21tb24vdXRpbHMnKS5ub3JtYWxpemVSZWZlcmVuY2U7XG52YXIgaXNTcGFjZSAgICAgICAgICAgICAgPSByZXF1aXJlKCcuLi9jb21tb24vdXRpbHMnKS5pc1NwYWNlO1xuXG5cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gaW1hZ2Uoc3RhdGUsIHNpbGVudCkge1xuICB2YXIgYXR0cnMsXG4gICAgICBjb2RlLFxuICAgICAgY29udGVudCxcbiAgICAgIGxhYmVsLFxuICAgICAgbGFiZWxFbmQsXG4gICAgICBsYWJlbFN0YXJ0LFxuICAgICAgcG9zLFxuICAgICAgcmVmLFxuICAgICAgcmVzLFxuICAgICAgdGl0bGUsXG4gICAgICB0b2tlbixcbiAgICAgIHRva2VucyxcbiAgICAgIHN0YXJ0LFxuICAgICAgaHJlZiA9ICcnLFxuICAgICAgb2xkUG9zID0gc3RhdGUucG9zLFxuICAgICAgbWF4ID0gc3RhdGUucG9zTWF4O1xuXG4gIGlmIChzdGF0ZS5zcmMuY2hhckNvZGVBdChzdGF0ZS5wb3MpICE9PSAweDIxLyogISAqLykgeyByZXR1cm4gZmFsc2U7IH1cbiAgaWYgKHN0YXRlLnNyYy5jaGFyQ29kZUF0KHN0YXRlLnBvcyArIDEpICE9PSAweDVCLyogWyAqLykgeyByZXR1cm4gZmFsc2U7IH1cblxuICBsYWJlbFN0YXJ0ID0gc3RhdGUucG9zICsgMjtcbiAgbGFiZWxFbmQgPSBzdGF0ZS5tZC5oZWxwZXJzLnBhcnNlTGlua0xhYmVsKHN0YXRlLCBzdGF0ZS5wb3MgKyAxLCBmYWxzZSk7XG5cbiAgLy8gcGFyc2VyIGZhaWxlZCB0byBmaW5kICddJywgc28gaXQncyBub3QgYSB2YWxpZCBsaW5rXG4gIGlmIChsYWJlbEVuZCA8IDApIHsgcmV0dXJuIGZhbHNlOyB9XG5cbiAgcG9zID0gbGFiZWxFbmQgKyAxO1xuICBpZiAocG9zIDwgbWF4ICYmIHN0YXRlLnNyYy5jaGFyQ29kZUF0KHBvcykgPT09IDB4MjgvKiAoICovKSB7XG4gICAgLy9cbiAgICAvLyBJbmxpbmUgbGlua1xuICAgIC8vXG5cbiAgICAvLyBbbGlua10oICA8aHJlZj4gIFwidGl0bGVcIiAgKVxuICAgIC8vICAgICAgICBeXiBza2lwcGluZyB0aGVzZSBzcGFjZXNcbiAgICBwb3MrKztcbiAgICBmb3IgKDsgcG9zIDwgbWF4OyBwb3MrKykge1xuICAgICAgY29kZSA9IHN0YXRlLnNyYy5jaGFyQ29kZUF0KHBvcyk7XG4gICAgICBpZiAoIWlzU3BhY2UoY29kZSkgJiYgY29kZSAhPT0gMHgwQSkgeyBicmVhazsgfVxuICAgIH1cbiAgICBpZiAocG9zID49IG1heCkgeyByZXR1cm4gZmFsc2U7IH1cblxuICAgIC8vIFtsaW5rXSggIDxocmVmPiAgXCJ0aXRsZVwiICApXG4gICAgLy8gICAgICAgICAgXl5eXl5eIHBhcnNpbmcgbGluayBkZXN0aW5hdGlvblxuICAgIHN0YXJ0ID0gcG9zO1xuICAgIHJlcyA9IHN0YXRlLm1kLmhlbHBlcnMucGFyc2VMaW5rRGVzdGluYXRpb24oc3RhdGUuc3JjLCBwb3MsIHN0YXRlLnBvc01heCk7XG4gICAgaWYgKHJlcy5vaykge1xuICAgICAgaHJlZiA9IHN0YXRlLm1kLm5vcm1hbGl6ZUxpbmsocmVzLnN0cik7XG4gICAgICBpZiAoc3RhdGUubWQudmFsaWRhdGVMaW5rKGhyZWYpKSB7XG4gICAgICAgIHBvcyA9IHJlcy5wb3M7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBocmVmID0gJyc7XG4gICAgICB9XG4gICAgfVxuXG4gICAgLy8gW2xpbmtdKCAgPGhyZWY+ICBcInRpdGxlXCIgIClcbiAgICAvLyAgICAgICAgICAgICAgICBeXiBza2lwcGluZyB0aGVzZSBzcGFjZXNcbiAgICBzdGFydCA9IHBvcztcbiAgICBmb3IgKDsgcG9zIDwgbWF4OyBwb3MrKykge1xuICAgICAgY29kZSA9IHN0YXRlLnNyYy5jaGFyQ29kZUF0KHBvcyk7XG4gICAgICBpZiAoIWlzU3BhY2UoY29kZSkgJiYgY29kZSAhPT0gMHgwQSkgeyBicmVhazsgfVxuICAgIH1cblxuICAgIC8vIFtsaW5rXSggIDxocmVmPiAgXCJ0aXRsZVwiICApXG4gICAgLy8gICAgICAgICAgICAgICAgICBeXl5eXl5eIHBhcnNpbmcgbGluayB0aXRsZVxuICAgIHJlcyA9IHN0YXRlLm1kLmhlbHBlcnMucGFyc2VMaW5rVGl0bGUoc3RhdGUuc3JjLCBwb3MsIHN0YXRlLnBvc01heCk7XG4gICAgaWYgKHBvcyA8IG1heCAmJiBzdGFydCAhPT0gcG9zICYmIHJlcy5vaykge1xuICAgICAgdGl0bGUgPSByZXMuc3RyO1xuICAgICAgcG9zID0gcmVzLnBvcztcblxuICAgICAgLy8gW2xpbmtdKCAgPGhyZWY+ICBcInRpdGxlXCIgIClcbiAgICAgIC8vICAgICAgICAgICAgICAgICAgICAgICAgIF5eIHNraXBwaW5nIHRoZXNlIHNwYWNlc1xuICAgICAgZm9yICg7IHBvcyA8IG1heDsgcG9zKyspIHtcbiAgICAgICAgY29kZSA9IHN0YXRlLnNyYy5jaGFyQ29kZUF0KHBvcyk7XG4gICAgICAgIGlmICghaXNTcGFjZShjb2RlKSAmJiBjb2RlICE9PSAweDBBKSB7IGJyZWFrOyB9XG4gICAgICB9XG4gICAgfSBlbHNlIHtcbiAgICAgIHRpdGxlID0gJyc7XG4gICAgfVxuXG4gICAgaWYgKHBvcyA+PSBtYXggfHwgc3RhdGUuc3JjLmNoYXJDb2RlQXQocG9zKSAhPT0gMHgyOS8qICkgKi8pIHtcbiAgICAgIHN0YXRlLnBvcyA9IG9sZFBvcztcbiAgICAgIHJldHVybiBmYWxzZTtcbiAgICB9XG4gICAgcG9zKys7XG4gIH0gZWxzZSB7XG4gICAgLy9cbiAgICAvLyBMaW5rIHJlZmVyZW5jZVxuICAgIC8vXG4gICAgaWYgKHR5cGVvZiBzdGF0ZS5lbnYucmVmZXJlbmNlcyA9PT0gJ3VuZGVmaW5lZCcpIHsgcmV0dXJuIGZhbHNlOyB9XG5cbiAgICBpZiAocG9zIDwgbWF4ICYmIHN0YXRlLnNyYy5jaGFyQ29kZUF0KHBvcykgPT09IDB4NUIvKiBbICovKSB7XG4gICAgICBzdGFydCA9IHBvcyArIDE7XG4gICAgICBwb3MgPSBzdGF0ZS5tZC5oZWxwZXJzLnBhcnNlTGlua0xhYmVsKHN0YXRlLCBwb3MpO1xuICAgICAgaWYgKHBvcyA+PSAwKSB7XG4gICAgICAgIGxhYmVsID0gc3RhdGUuc3JjLnNsaWNlKHN0YXJ0LCBwb3MrKyk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBwb3MgPSBsYWJlbEVuZCArIDE7XG4gICAgICB9XG4gICAgfSBlbHNlIHtcbiAgICAgIHBvcyA9IGxhYmVsRW5kICsgMTtcbiAgICB9XG5cbiAgICAvLyBjb3ZlcnMgbGFiZWwgPT09ICcnIGFuZCBsYWJlbCA9PT0gdW5kZWZpbmVkXG4gICAgLy8gKGNvbGxhcHNlZCByZWZlcmVuY2UgbGluayBhbmQgc2hvcnRjdXQgcmVmZXJlbmNlIGxpbmsgcmVzcGVjdGl2ZWx5KVxuICAgIGlmICghbGFiZWwpIHsgbGFiZWwgPSBzdGF0ZS5zcmMuc2xpY2UobGFiZWxTdGFydCwgbGFiZWxFbmQpOyB9XG5cbiAgICByZWYgPSBzdGF0ZS5lbnYucmVmZXJlbmNlc1tub3JtYWxpemVSZWZlcmVuY2UobGFiZWwpXTtcbiAgICBpZiAoIXJlZikge1xuICAgICAgc3RhdGUucG9zID0gb2xkUG9zO1xuICAgICAgcmV0dXJuIGZhbHNlO1xuICAgIH1cbiAgICBocmVmID0gcmVmLmhyZWY7XG4gICAgdGl0bGUgPSByZWYudGl0bGU7XG4gIH1cblxuICAvL1xuICAvLyBXZSBmb3VuZCB0aGUgZW5kIG9mIHRoZSBsaW5rLCBhbmQga25vdyBmb3IgYSBmYWN0IGl0J3MgYSB2YWxpZCBsaW5rO1xuICAvLyBzbyBhbGwgdGhhdCdzIGxlZnQgdG8gZG8gaXMgdG8gY2FsbCB0b2tlbml6ZXIuXG4gIC8vXG4gIGlmICghc2lsZW50KSB7XG4gICAgY29udGVudCA9IHN0YXRlLnNyYy5zbGljZShsYWJlbFN0YXJ0LCBsYWJlbEVuZCk7XG5cbiAgICBzdGF0ZS5tZC5pbmxpbmUucGFyc2UoXG4gICAgICBjb250ZW50LFxuICAgICAgc3RhdGUubWQsXG4gICAgICBzdGF0ZS5lbnYsXG4gICAgICB0b2tlbnMgPSBbXVxuICAgICk7XG5cbiAgICB0b2tlbiAgICAgICAgICA9IHN0YXRlLnB1c2goJ2ltYWdlJywgJ2ltZycsIDApO1xuICAgIHRva2VuLmF0dHJzICAgID0gYXR0cnMgPSBbIFsgJ3NyYycsIGhyZWYgXSwgWyAnYWx0JywgJycgXSBdO1xuICAgIHRva2VuLmNoaWxkcmVuID0gdG9rZW5zO1xuICAgIHRva2VuLmNvbnRlbnQgID0gY29udGVudDtcblxuICAgIGlmICh0aXRsZSkge1xuICAgICAgYXR0cnMucHVzaChbICd0aXRsZScsIHRpdGxlIF0pO1xuICAgIH1cbiAgfVxuXG4gIHN0YXRlLnBvcyA9IHBvcztcbiAgc3RhdGUucG9zTWF4ID0gbWF4O1xuICByZXR1cm4gdHJ1ZTtcbn07XG4iLCIvLyBQcm9jZXNzIFtsaW5rXSg8dG8+IFwic3R1ZmZcIilcblxuJ3VzZSBzdHJpY3QnO1xuXG52YXIgbm9ybWFsaXplUmVmZXJlbmNlICAgPSByZXF1aXJlKCcuLi9jb21tb24vdXRpbHMnKS5ub3JtYWxpemVSZWZlcmVuY2U7XG52YXIgaXNTcGFjZSAgICAgICAgICAgICAgPSByZXF1aXJlKCcuLi9jb21tb24vdXRpbHMnKS5pc1NwYWNlO1xuXG5cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gbGluayhzdGF0ZSwgc2lsZW50KSB7XG4gIHZhciBhdHRycyxcbiAgICAgIGNvZGUsXG4gICAgICBsYWJlbCxcbiAgICAgIGxhYmVsRW5kLFxuICAgICAgbGFiZWxTdGFydCxcbiAgICAgIHBvcyxcbiAgICAgIHJlcyxcbiAgICAgIHJlZixcbiAgICAgIHRpdGxlLFxuICAgICAgdG9rZW4sXG4gICAgICBocmVmID0gJycsXG4gICAgICBvbGRQb3MgPSBzdGF0ZS5wb3MsXG4gICAgICBtYXggPSBzdGF0ZS5wb3NNYXgsXG4gICAgICBzdGFydCA9IHN0YXRlLnBvcyxcbiAgICAgIHBhcnNlUmVmZXJlbmNlID0gdHJ1ZTtcblxuICBpZiAoc3RhdGUuc3JjLmNoYXJDb2RlQXQoc3RhdGUucG9zKSAhPT0gMHg1Qi8qIFsgKi8pIHsgcmV0dXJuIGZhbHNlOyB9XG5cbiAgbGFiZWxTdGFydCA9IHN0YXRlLnBvcyArIDE7XG4gIGxhYmVsRW5kID0gc3RhdGUubWQuaGVscGVycy5wYXJzZUxpbmtMYWJlbChzdGF0ZSwgc3RhdGUucG9zLCB0cnVlKTtcblxuICAvLyBwYXJzZXIgZmFpbGVkIHRvIGZpbmQgJ10nLCBzbyBpdCdzIG5vdCBhIHZhbGlkIGxpbmtcbiAgaWYgKGxhYmVsRW5kIDwgMCkgeyByZXR1cm4gZmFsc2U7IH1cblxuICBwb3MgPSBsYWJlbEVuZCArIDE7XG4gIGlmIChwb3MgPCBtYXggJiYgc3RhdGUuc3JjLmNoYXJDb2RlQXQocG9zKSA9PT0gMHgyOC8qICggKi8pIHtcbiAgICAvL1xuICAgIC8vIElubGluZSBsaW5rXG4gICAgLy9cblxuICAgIC8vIG1pZ2h0IGhhdmUgZm91bmQgYSB2YWxpZCBzaG9ydGN1dCBsaW5rLCBkaXNhYmxlIHJlZmVyZW5jZSBwYXJzaW5nXG4gICAgcGFyc2VSZWZlcmVuY2UgPSBmYWxzZTtcblxuICAgIC8vIFtsaW5rXSggIDxocmVmPiAgXCJ0aXRsZVwiICApXG4gICAgLy8gICAgICAgIF5eIHNraXBwaW5nIHRoZXNlIHNwYWNlc1xuICAgIHBvcysrO1xuICAgIGZvciAoOyBwb3MgPCBtYXg7IHBvcysrKSB7XG4gICAgICBjb2RlID0gc3RhdGUuc3JjLmNoYXJDb2RlQXQocG9zKTtcbiAgICAgIGlmICghaXNTcGFjZShjb2RlKSAmJiBjb2RlICE9PSAweDBBKSB7IGJyZWFrOyB9XG4gICAgfVxuICAgIGlmIChwb3MgPj0gbWF4KSB7IHJldHVybiBmYWxzZTsgfVxuXG4gICAgLy8gW2xpbmtdKCAgPGhyZWY+ICBcInRpdGxlXCIgIClcbiAgICAvLyAgICAgICAgICBeXl5eXl4gcGFyc2luZyBsaW5rIGRlc3RpbmF0aW9uXG4gICAgc3RhcnQgPSBwb3M7XG4gICAgcmVzID0gc3RhdGUubWQuaGVscGVycy5wYXJzZUxpbmtEZXN0aW5hdGlvbihzdGF0ZS5zcmMsIHBvcywgc3RhdGUucG9zTWF4KTtcbiAgICBpZiAocmVzLm9rKSB7XG4gICAgICBocmVmID0gc3RhdGUubWQubm9ybWFsaXplTGluayhyZXMuc3RyKTtcbiAgICAgIGlmIChzdGF0ZS5tZC52YWxpZGF0ZUxpbmsoaHJlZikpIHtcbiAgICAgICAgcG9zID0gcmVzLnBvcztcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGhyZWYgPSAnJztcbiAgICAgIH1cbiAgICB9XG5cbiAgICAvLyBbbGlua10oICA8aHJlZj4gIFwidGl0bGVcIiAgKVxuICAgIC8vICAgICAgICAgICAgICAgIF5eIHNraXBwaW5nIHRoZXNlIHNwYWNlc1xuICAgIHN0YXJ0ID0gcG9zO1xuICAgIGZvciAoOyBwb3MgPCBtYXg7IHBvcysrKSB7XG4gICAgICBjb2RlID0gc3RhdGUuc3JjLmNoYXJDb2RlQXQocG9zKTtcbiAgICAgIGlmICghaXNTcGFjZShjb2RlKSAmJiBjb2RlICE9PSAweDBBKSB7IGJyZWFrOyB9XG4gICAgfVxuXG4gICAgLy8gW2xpbmtdKCAgPGhyZWY+ICBcInRpdGxlXCIgIClcbiAgICAvLyAgICAgICAgICAgICAgICAgIF5eXl5eXl4gcGFyc2luZyBsaW5rIHRpdGxlXG4gICAgcmVzID0gc3RhdGUubWQuaGVscGVycy5wYXJzZUxpbmtUaXRsZShzdGF0ZS5zcmMsIHBvcywgc3RhdGUucG9zTWF4KTtcbiAgICBpZiAocG9zIDwgbWF4ICYmIHN0YXJ0ICE9PSBwb3MgJiYgcmVzLm9rKSB7XG4gICAgICB0aXRsZSA9IHJlcy5zdHI7XG4gICAgICBwb3MgPSByZXMucG9zO1xuXG4gICAgICAvLyBbbGlua10oICA8aHJlZj4gIFwidGl0bGVcIiAgKVxuICAgICAgLy8gICAgICAgICAgICAgICAgICAgICAgICAgXl4gc2tpcHBpbmcgdGhlc2Ugc3BhY2VzXG4gICAgICBmb3IgKDsgcG9zIDwgbWF4OyBwb3MrKykge1xuICAgICAgICBjb2RlID0gc3RhdGUuc3JjLmNoYXJDb2RlQXQocG9zKTtcbiAgICAgICAgaWYgKCFpc1NwYWNlKGNvZGUpICYmIGNvZGUgIT09IDB4MEEpIHsgYnJlYWs7IH1cbiAgICAgIH1cbiAgICB9IGVsc2Uge1xuICAgICAgdGl0bGUgPSAnJztcbiAgICB9XG5cbiAgICBpZiAocG9zID49IG1heCB8fCBzdGF0ZS5zcmMuY2hhckNvZGVBdChwb3MpICE9PSAweDI5LyogKSAqLykge1xuICAgICAgLy8gcGFyc2luZyBhIHZhbGlkIHNob3J0Y3V0IGxpbmsgZmFpbGVkLCBmYWxsYmFjayB0byByZWZlcmVuY2VcbiAgICAgIHBhcnNlUmVmZXJlbmNlID0gdHJ1ZTtcbiAgICB9XG4gICAgcG9zKys7XG4gIH1cblxuICBpZiAocGFyc2VSZWZlcmVuY2UpIHtcbiAgICAvL1xuICAgIC8vIExpbmsgcmVmZXJlbmNlXG4gICAgLy9cbiAgICBpZiAodHlwZW9mIHN0YXRlLmVudi5yZWZlcmVuY2VzID09PSAndW5kZWZpbmVkJykgeyByZXR1cm4gZmFsc2U7IH1cblxuICAgIGlmIChwb3MgPCBtYXggJiYgc3RhdGUuc3JjLmNoYXJDb2RlQXQocG9zKSA9PT0gMHg1Qi8qIFsgKi8pIHtcbiAgICAgIHN0YXJ0ID0gcG9zICsgMTtcbiAgICAgIHBvcyA9IHN0YXRlLm1kLmhlbHBlcnMucGFyc2VMaW5rTGFiZWwoc3RhdGUsIHBvcyk7XG4gICAgICBpZiAocG9zID49IDApIHtcbiAgICAgICAgbGFiZWwgPSBzdGF0ZS5zcmMuc2xpY2Uoc3RhcnQsIHBvcysrKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHBvcyA9IGxhYmVsRW5kICsgMTtcbiAgICAgIH1cbiAgICB9IGVsc2Uge1xuICAgICAgcG9zID0gbGFiZWxFbmQgKyAxO1xuICAgIH1cblxuICAgIC8vIGNvdmVycyBsYWJlbCA9PT0gJycgYW5kIGxhYmVsID09PSB1bmRlZmluZWRcbiAgICAvLyAoY29sbGFwc2VkIHJlZmVyZW5jZSBsaW5rIGFuZCBzaG9ydGN1dCByZWZlcmVuY2UgbGluayByZXNwZWN0aXZlbHkpXG4gICAgaWYgKCFsYWJlbCkgeyBsYWJlbCA9IHN0YXRlLnNyYy5zbGljZShsYWJlbFN0YXJ0LCBsYWJlbEVuZCk7IH1cblxuICAgIHJlZiA9IHN0YXRlLmVudi5yZWZlcmVuY2VzW25vcm1hbGl6ZVJlZmVyZW5jZShsYWJlbCldO1xuICAgIGlmICghcmVmKSB7XG4gICAgICBzdGF0ZS5wb3MgPSBvbGRQb3M7XG4gICAgICByZXR1cm4gZmFsc2U7XG4gICAgfVxuICAgIGhyZWYgPSByZWYuaHJlZjtcbiAgICB0aXRsZSA9IHJlZi50aXRsZTtcbiAgfVxuXG4gIC8vXG4gIC8vIFdlIGZvdW5kIHRoZSBlbmQgb2YgdGhlIGxpbmssIGFuZCBrbm93IGZvciBhIGZhY3QgaXQncyBhIHZhbGlkIGxpbms7XG4gIC8vIHNvIGFsbCB0aGF0J3MgbGVmdCB0byBkbyBpcyB0byBjYWxsIHRva2VuaXplci5cbiAgLy9cbiAgaWYgKCFzaWxlbnQpIHtcbiAgICBzdGF0ZS5wb3MgPSBsYWJlbFN0YXJ0O1xuICAgIHN0YXRlLnBvc01heCA9IGxhYmVsRW5kO1xuXG4gICAgdG9rZW4gICAgICAgID0gc3RhdGUucHVzaCgnbGlua19vcGVuJywgJ2EnLCAxKTtcbiAgICB0b2tlbi5hdHRycyAgPSBhdHRycyA9IFsgWyAnaHJlZicsIGhyZWYgXSBdO1xuICAgIGlmICh0aXRsZSkge1xuICAgICAgYXR0cnMucHVzaChbICd0aXRsZScsIHRpdGxlIF0pO1xuICAgIH1cblxuICAgIHN0YXRlLm1kLmlubGluZS50b2tlbml6ZShzdGF0ZSk7XG5cbiAgICB0b2tlbiAgICAgICAgPSBzdGF0ZS5wdXNoKCdsaW5rX2Nsb3NlJywgJ2EnLCAtMSk7XG4gIH1cblxuICBzdGF0ZS5wb3MgPSBwb3M7XG4gIHN0YXRlLnBvc01heCA9IG1heDtcbiAgcmV0dXJuIHRydWU7XG59O1xuIiwiLy8gUHJvY2Vlc3MgJ1xcbidcblxuJ3VzZSBzdHJpY3QnO1xuXG52YXIgaXNTcGFjZSA9IHJlcXVpcmUoJy4uL2NvbW1vbi91dGlscycpLmlzU3BhY2U7XG5cblxubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiBuZXdsaW5lKHN0YXRlLCBzaWxlbnQpIHtcbiAgdmFyIHBtYXgsIG1heCwgcG9zID0gc3RhdGUucG9zO1xuXG4gIGlmIChzdGF0ZS5zcmMuY2hhckNvZGVBdChwb3MpICE9PSAweDBBLyogXFxuICovKSB7IHJldHVybiBmYWxzZTsgfVxuXG4gIHBtYXggPSBzdGF0ZS5wZW5kaW5nLmxlbmd0aCAtIDE7XG4gIG1heCA9IHN0YXRlLnBvc01heDtcblxuICAvLyAnICBcXG4nIC0+IGhhcmRicmVha1xuICAvLyBMb29rdXAgaW4gcGVuZGluZyBjaGFycyBpcyBiYWQgcHJhY3RpY2UhIERvbid0IGNvcHkgdG8gb3RoZXIgcnVsZXMhXG4gIC8vIFBlbmRpbmcgc3RyaW5nIGlzIHN0b3JlZCBpbiBjb25jYXQgbW9kZSwgaW5kZXhlZCBsb29rdXBzIHdpbGwgY2F1c2VcbiAgLy8gY29udmVydGlvbiB0byBmbGF0IG1vZGUuXG4gIGlmICghc2lsZW50KSB7XG4gICAgaWYgKHBtYXggPj0gMCAmJiBzdGF0ZS5wZW5kaW5nLmNoYXJDb2RlQXQocG1heCkgPT09IDB4MjApIHtcbiAgICAgIGlmIChwbWF4ID49IDEgJiYgc3RhdGUucGVuZGluZy5jaGFyQ29kZUF0KHBtYXggLSAxKSA9PT0gMHgyMCkge1xuICAgICAgICBzdGF0ZS5wZW5kaW5nID0gc3RhdGUucGVuZGluZy5yZXBsYWNlKC8gKyQvLCAnJyk7XG4gICAgICAgIHN0YXRlLnB1c2goJ2hhcmRicmVhaycsICdicicsIDApO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgc3RhdGUucGVuZGluZyA9IHN0YXRlLnBlbmRpbmcuc2xpY2UoMCwgLTEpO1xuICAgICAgICBzdGF0ZS5wdXNoKCdzb2Z0YnJlYWsnLCAnYnInLCAwKTtcbiAgICAgIH1cblxuICAgIH0gZWxzZSB7XG4gICAgICBzdGF0ZS5wdXNoKCdzb2Z0YnJlYWsnLCAnYnInLCAwKTtcbiAgICB9XG4gIH1cblxuICBwb3MrKztcblxuICAvLyBza2lwIGhlYWRpbmcgc3BhY2VzIGZvciBuZXh0IGxpbmVcbiAgd2hpbGUgKHBvcyA8IG1heCAmJiBpc1NwYWNlKHN0YXRlLnNyYy5jaGFyQ29kZUF0KHBvcykpKSB7IHBvcysrOyB9XG5cbiAgc3RhdGUucG9zID0gcG9zO1xuICByZXR1cm4gdHJ1ZTtcbn07XG4iLCIvLyBJbmxpbmUgcGFyc2VyIHN0YXRlXG5cbid1c2Ugc3RyaWN0JztcblxuXG52YXIgVG9rZW4gICAgICAgICAgPSByZXF1aXJlKCcuLi90b2tlbicpO1xudmFyIGlzV2hpdGVTcGFjZSAgID0gcmVxdWlyZSgnLi4vY29tbW9uL3V0aWxzJykuaXNXaGl0ZVNwYWNlO1xudmFyIGlzUHVuY3RDaGFyICAgID0gcmVxdWlyZSgnLi4vY29tbW9uL3V0aWxzJykuaXNQdW5jdENoYXI7XG52YXIgaXNNZEFzY2lpUHVuY3QgPSByZXF1aXJlKCcuLi9jb21tb24vdXRpbHMnKS5pc01kQXNjaWlQdW5jdDtcblxuXG5mdW5jdGlvbiBTdGF0ZUlubGluZShzcmMsIG1kLCBlbnYsIG91dFRva2Vucykge1xuICB0aGlzLnNyYyA9IHNyYztcbiAgdGhpcy5lbnYgPSBlbnY7XG4gIHRoaXMubWQgPSBtZDtcbiAgdGhpcy50b2tlbnMgPSBvdXRUb2tlbnM7XG4gIHRoaXMudG9rZW5zX21ldGEgPSBBcnJheShvdXRUb2tlbnMubGVuZ3RoKTtcblxuICB0aGlzLnBvcyA9IDA7XG4gIHRoaXMucG9zTWF4ID0gdGhpcy5zcmMubGVuZ3RoO1xuICB0aGlzLmxldmVsID0gMDtcbiAgdGhpcy5wZW5kaW5nID0gJyc7XG4gIHRoaXMucGVuZGluZ0xldmVsID0gMDtcblxuICAvLyBTdG9yZXMgeyBzdGFydDogZW5kIH0gcGFpcnMuIFVzZWZ1bCBmb3IgYmFja3RyYWNrXG4gIC8vIG9wdGltaXphdGlvbiBvZiBwYWlycyBwYXJzZSAoZW1waGFzaXMsIHN0cmlrZXMpLlxuICB0aGlzLmNhY2hlID0ge307XG5cbiAgLy8gTGlzdCBvZiBlbXBoYXNpcy1saWtlIGRlbGltaXRlcnMgZm9yIGN1cnJlbnQgdGFnXG4gIHRoaXMuZGVsaW1pdGVycyA9IFtdO1xuXG4gIC8vIFN0YWNrIG9mIGRlbGltaXRlciBsaXN0cyBmb3IgdXBwZXIgbGV2ZWwgdGFnc1xuICB0aGlzLl9wcmV2X2RlbGltaXRlcnMgPSBbXTtcbn1cblxuXG4vLyBGbHVzaCBwZW5kaW5nIHRleHRcbi8vXG5TdGF0ZUlubGluZS5wcm90b3R5cGUucHVzaFBlbmRpbmcgPSBmdW5jdGlvbiAoKSB7XG4gIHZhciB0b2tlbiA9IG5ldyBUb2tlbigndGV4dCcsICcnLCAwKTtcbiAgdG9rZW4uY29udGVudCA9IHRoaXMucGVuZGluZztcbiAgdG9rZW4ubGV2ZWwgPSB0aGlzLnBlbmRpbmdMZXZlbDtcbiAgdGhpcy50b2tlbnMucHVzaCh0b2tlbik7XG4gIHRoaXMucGVuZGluZyA9ICcnO1xuICByZXR1cm4gdG9rZW47XG59O1xuXG5cbi8vIFB1c2ggbmV3IHRva2VuIHRvIFwic3RyZWFtXCIuXG4vLyBJZiBwZW5kaW5nIHRleHQgZXhpc3RzIC0gZmx1c2ggaXQgYXMgdGV4dCB0b2tlblxuLy9cblN0YXRlSW5saW5lLnByb3RvdHlwZS5wdXNoID0gZnVuY3Rpb24gKHR5cGUsIHRhZywgbmVzdGluZykge1xuICBpZiAodGhpcy5wZW5kaW5nKSB7XG4gICAgdGhpcy5wdXNoUGVuZGluZygpO1xuICB9XG5cbiAgdmFyIHRva2VuID0gbmV3IFRva2VuKHR5cGUsIHRhZywgbmVzdGluZyk7XG4gIHZhciB0b2tlbl9tZXRhID0gbnVsbDtcblxuICBpZiAobmVzdGluZyA8IDApIHtcbiAgICAvLyBjbG9zaW5nIHRhZ1xuICAgIHRoaXMubGV2ZWwtLTtcbiAgICB0aGlzLmRlbGltaXRlcnMgPSB0aGlzLl9wcmV2X2RlbGltaXRlcnMucG9wKCk7XG4gIH1cblxuICB0b2tlbi5sZXZlbCA9IHRoaXMubGV2ZWw7XG5cbiAgaWYgKG5lc3RpbmcgPiAwKSB7XG4gICAgLy8gb3BlbmluZyB0YWdcbiAgICB0aGlzLmxldmVsKys7XG4gICAgdGhpcy5fcHJldl9kZWxpbWl0ZXJzLnB1c2godGhpcy5kZWxpbWl0ZXJzKTtcbiAgICB0aGlzLmRlbGltaXRlcnMgPSBbXTtcbiAgICB0b2tlbl9tZXRhID0geyBkZWxpbWl0ZXJzOiB0aGlzLmRlbGltaXRlcnMgfTtcbiAgfVxuXG4gIHRoaXMucGVuZGluZ0xldmVsID0gdGhpcy5sZXZlbDtcbiAgdGhpcy50b2tlbnMucHVzaCh0b2tlbik7XG4gIHRoaXMudG9rZW5zX21ldGEucHVzaCh0b2tlbl9tZXRhKTtcbiAgcmV0dXJuIHRva2VuO1xufTtcblxuXG4vLyBTY2FuIGEgc2VxdWVuY2Ugb2YgZW1waGFzaXMtbGlrZSBtYXJrZXJzLCBhbmQgZGV0ZXJtaW5lIHdoZXRoZXJcbi8vIGl0IGNhbiBzdGFydCBhbiBlbXBoYXNpcyBzZXF1ZW5jZSBvciBlbmQgYW4gZW1waGFzaXMgc2VxdWVuY2UuXG4vL1xuLy8gIC0gc3RhcnQgLSBwb3NpdGlvbiB0byBzY2FuIGZyb20gKGl0IHNob3VsZCBwb2ludCBhdCBhIHZhbGlkIG1hcmtlcik7XG4vLyAgLSBjYW5TcGxpdFdvcmQgLSBkZXRlcm1pbmUgaWYgdGhlc2UgbWFya2VycyBjYW4gYmUgZm91bmQgaW5zaWRlIGEgd29yZFxuLy9cblN0YXRlSW5saW5lLnByb3RvdHlwZS5zY2FuRGVsaW1zID0gZnVuY3Rpb24gKHN0YXJ0LCBjYW5TcGxpdFdvcmQpIHtcbiAgdmFyIHBvcyA9IHN0YXJ0LCBsYXN0Q2hhciwgbmV4dENoYXIsIGNvdW50LCBjYW5fb3BlbiwgY2FuX2Nsb3NlLFxuICAgICAgaXNMYXN0V2hpdGVTcGFjZSwgaXNMYXN0UHVuY3RDaGFyLFxuICAgICAgaXNOZXh0V2hpdGVTcGFjZSwgaXNOZXh0UHVuY3RDaGFyLFxuICAgICAgbGVmdF9mbGFua2luZyA9IHRydWUsXG4gICAgICByaWdodF9mbGFua2luZyA9IHRydWUsXG4gICAgICBtYXggPSB0aGlzLnBvc01heCxcbiAgICAgIG1hcmtlciA9IHRoaXMuc3JjLmNoYXJDb2RlQXQoc3RhcnQpO1xuXG4gIC8vIHRyZWF0IGJlZ2lubmluZyBvZiB0aGUgbGluZSBhcyBhIHdoaXRlc3BhY2VcbiAgbGFzdENoYXIgPSBzdGFydCA+IDAgPyB0aGlzLnNyYy5jaGFyQ29kZUF0KHN0YXJ0IC0gMSkgOiAweDIwO1xuXG4gIHdoaWxlIChwb3MgPCBtYXggJiYgdGhpcy5zcmMuY2hhckNvZGVBdChwb3MpID09PSBtYXJrZXIpIHsgcG9zKys7IH1cblxuICBjb3VudCA9IHBvcyAtIHN0YXJ0O1xuXG4gIC8vIHRyZWF0IGVuZCBvZiB0aGUgbGluZSBhcyBhIHdoaXRlc3BhY2VcbiAgbmV4dENoYXIgPSBwb3MgPCBtYXggPyB0aGlzLnNyYy5jaGFyQ29kZUF0KHBvcykgOiAweDIwO1xuXG4gIGlzTGFzdFB1bmN0Q2hhciA9IGlzTWRBc2NpaVB1bmN0KGxhc3RDaGFyKSB8fCBpc1B1bmN0Q2hhcihTdHJpbmcuZnJvbUNoYXJDb2RlKGxhc3RDaGFyKSk7XG4gIGlzTmV4dFB1bmN0Q2hhciA9IGlzTWRBc2NpaVB1bmN0KG5leHRDaGFyKSB8fCBpc1B1bmN0Q2hhcihTdHJpbmcuZnJvbUNoYXJDb2RlKG5leHRDaGFyKSk7XG5cbiAgaXNMYXN0V2hpdGVTcGFjZSA9IGlzV2hpdGVTcGFjZShsYXN0Q2hhcik7XG4gIGlzTmV4dFdoaXRlU3BhY2UgPSBpc1doaXRlU3BhY2UobmV4dENoYXIpO1xuXG4gIGlmIChpc05leHRXaGl0ZVNwYWNlKSB7XG4gICAgbGVmdF9mbGFua2luZyA9IGZhbHNlO1xuICB9IGVsc2UgaWYgKGlzTmV4dFB1bmN0Q2hhcikge1xuICAgIGlmICghKGlzTGFzdFdoaXRlU3BhY2UgfHwgaXNMYXN0UHVuY3RDaGFyKSkge1xuICAgICAgbGVmdF9mbGFua2luZyA9IGZhbHNlO1xuICAgIH1cbiAgfVxuXG4gIGlmIChpc0xhc3RXaGl0ZVNwYWNlKSB7XG4gICAgcmlnaHRfZmxhbmtpbmcgPSBmYWxzZTtcbiAgfSBlbHNlIGlmIChpc0xhc3RQdW5jdENoYXIpIHtcbiAgICBpZiAoIShpc05leHRXaGl0ZVNwYWNlIHx8IGlzTmV4dFB1bmN0Q2hhcikpIHtcbiAgICAgIHJpZ2h0X2ZsYW5raW5nID0gZmFsc2U7XG4gICAgfVxuICB9XG5cbiAgaWYgKCFjYW5TcGxpdFdvcmQpIHtcbiAgICBjYW5fb3BlbiAgPSBsZWZ0X2ZsYW5raW5nICAmJiAoIXJpZ2h0X2ZsYW5raW5nIHx8IGlzTGFzdFB1bmN0Q2hhcik7XG4gICAgY2FuX2Nsb3NlID0gcmlnaHRfZmxhbmtpbmcgJiYgKCFsZWZ0X2ZsYW5raW5nICB8fCBpc05leHRQdW5jdENoYXIpO1xuICB9IGVsc2Uge1xuICAgIGNhbl9vcGVuICA9IGxlZnRfZmxhbmtpbmc7XG4gICAgY2FuX2Nsb3NlID0gcmlnaHRfZmxhbmtpbmc7XG4gIH1cblxuICByZXR1cm4ge1xuICAgIGNhbl9vcGVuOiAgY2FuX29wZW4sXG4gICAgY2FuX2Nsb3NlOiBjYW5fY2xvc2UsXG4gICAgbGVuZ3RoOiAgICBjb3VudFxuICB9O1xufTtcblxuXG4vLyByZS1leHBvcnQgVG9rZW4gY2xhc3MgdG8gdXNlIGluIGJsb2NrIHJ1bGVzXG5TdGF0ZUlubGluZS5wcm90b3R5cGUuVG9rZW4gPSBUb2tlbjtcblxuXG5tb2R1bGUuZXhwb3J0cyA9IFN0YXRlSW5saW5lO1xuIiwiLy8gfn5zdHJpa2UgdGhyb3VnaH5+XG4vL1xuJ3VzZSBzdHJpY3QnO1xuXG5cbi8vIEluc2VydCBlYWNoIG1hcmtlciBhcyBhIHNlcGFyYXRlIHRleHQgdG9rZW4sIGFuZCBhZGQgaXQgdG8gZGVsaW1pdGVyIGxpc3Rcbi8vXG5tb2R1bGUuZXhwb3J0cy50b2tlbml6ZSA9IGZ1bmN0aW9uIHN0cmlrZXRocm91Z2goc3RhdGUsIHNpbGVudCkge1xuICB2YXIgaSwgc2Nhbm5lZCwgdG9rZW4sIGxlbiwgY2gsXG4gICAgICBzdGFydCA9IHN0YXRlLnBvcyxcbiAgICAgIG1hcmtlciA9IHN0YXRlLnNyYy5jaGFyQ29kZUF0KHN0YXJ0KTtcblxuICBpZiAoc2lsZW50KSB7IHJldHVybiBmYWxzZTsgfVxuXG4gIGlmIChtYXJrZXIgIT09IDB4N0UvKiB+ICovKSB7IHJldHVybiBmYWxzZTsgfVxuXG4gIHNjYW5uZWQgPSBzdGF0ZS5zY2FuRGVsaW1zKHN0YXRlLnBvcywgdHJ1ZSk7XG4gIGxlbiA9IHNjYW5uZWQubGVuZ3RoO1xuICBjaCA9IFN0cmluZy5mcm9tQ2hhckNvZGUobWFya2VyKTtcblxuICBpZiAobGVuIDwgMikgeyByZXR1cm4gZmFsc2U7IH1cblxuICBpZiAobGVuICUgMikge1xuICAgIHRva2VuICAgICAgICAgPSBzdGF0ZS5wdXNoKCd0ZXh0JywgJycsIDApO1xuICAgIHRva2VuLmNvbnRlbnQgPSBjaDtcbiAgICBsZW4tLTtcbiAgfVxuXG4gIGZvciAoaSA9IDA7IGkgPCBsZW47IGkgKz0gMikge1xuICAgIHRva2VuICAgICAgICAgPSBzdGF0ZS5wdXNoKCd0ZXh0JywgJycsIDApO1xuICAgIHRva2VuLmNvbnRlbnQgPSBjaCArIGNoO1xuXG4gICAgc3RhdGUuZGVsaW1pdGVycy5wdXNoKHtcbiAgICAgIG1hcmtlcjogbWFya2VyLFxuICAgICAgbGVuZ3RoOiAwLCAvLyBkaXNhYmxlIFwicnVsZSBvZiAzXCIgbGVuZ3RoIGNoZWNrcyBtZWFudCBmb3IgZW1waGFzaXNcbiAgICAgIGp1bXA6ICAgaSxcbiAgICAgIHRva2VuOiAgc3RhdGUudG9rZW5zLmxlbmd0aCAtIDEsXG4gICAgICBlbmQ6ICAgIC0xLFxuICAgICAgb3BlbjogICBzY2FubmVkLmNhbl9vcGVuLFxuICAgICAgY2xvc2U6ICBzY2FubmVkLmNhbl9jbG9zZVxuICAgIH0pO1xuICB9XG5cbiAgc3RhdGUucG9zICs9IHNjYW5uZWQubGVuZ3RoO1xuXG4gIHJldHVybiB0cnVlO1xufTtcblxuXG5mdW5jdGlvbiBwb3N0UHJvY2VzcyhzdGF0ZSwgZGVsaW1pdGVycykge1xuICB2YXIgaSwgaixcbiAgICAgIHN0YXJ0RGVsaW0sXG4gICAgICBlbmREZWxpbSxcbiAgICAgIHRva2VuLFxuICAgICAgbG9uZU1hcmtlcnMgPSBbXSxcbiAgICAgIG1heCA9IGRlbGltaXRlcnMubGVuZ3RoO1xuXG4gIGZvciAoaSA9IDA7IGkgPCBtYXg7IGkrKykge1xuICAgIHN0YXJ0RGVsaW0gPSBkZWxpbWl0ZXJzW2ldO1xuXG4gICAgaWYgKHN0YXJ0RGVsaW0ubWFya2VyICE9PSAweDdFLyogfiAqLykge1xuICAgICAgY29udGludWU7XG4gICAgfVxuXG4gICAgaWYgKHN0YXJ0RGVsaW0uZW5kID09PSAtMSkge1xuICAgICAgY29udGludWU7XG4gICAgfVxuXG4gICAgZW5kRGVsaW0gPSBkZWxpbWl0ZXJzW3N0YXJ0RGVsaW0uZW5kXTtcblxuICAgIHRva2VuICAgICAgICAgPSBzdGF0ZS50b2tlbnNbc3RhcnREZWxpbS50b2tlbl07XG4gICAgdG9rZW4udHlwZSAgICA9ICdzX29wZW4nO1xuICAgIHRva2VuLnRhZyAgICAgPSAncyc7XG4gICAgdG9rZW4ubmVzdGluZyA9IDE7XG4gICAgdG9rZW4ubWFya3VwICA9ICd+fic7XG4gICAgdG9rZW4uY29udGVudCA9ICcnO1xuXG4gICAgdG9rZW4gICAgICAgICA9IHN0YXRlLnRva2Vuc1tlbmREZWxpbS50b2tlbl07XG4gICAgdG9rZW4udHlwZSAgICA9ICdzX2Nsb3NlJztcbiAgICB0b2tlbi50YWcgICAgID0gJ3MnO1xuICAgIHRva2VuLm5lc3RpbmcgPSAtMTtcbiAgICB0b2tlbi5tYXJrdXAgID0gJ35+JztcbiAgICB0b2tlbi5jb250ZW50ID0gJyc7XG5cbiAgICBpZiAoc3RhdGUudG9rZW5zW2VuZERlbGltLnRva2VuIC0gMV0udHlwZSA9PT0gJ3RleHQnICYmXG4gICAgICAgIHN0YXRlLnRva2Vuc1tlbmREZWxpbS50b2tlbiAtIDFdLmNvbnRlbnQgPT09ICd+Jykge1xuXG4gICAgICBsb25lTWFya2Vycy5wdXNoKGVuZERlbGltLnRva2VuIC0gMSk7XG4gICAgfVxuICB9XG5cbiAgLy8gSWYgYSBtYXJrZXIgc2VxdWVuY2UgaGFzIGFuIG9kZCBudW1iZXIgb2YgY2hhcmFjdGVycywgaXQncyBzcGxpdHRlZFxuICAvLyBsaWtlIHRoaXM6IGB+fn5+fmAgLT4gYH5gICsgYH5+YCArIGB+fmAsIGxlYXZpbmcgb25lIG1hcmtlciBhdCB0aGVcbiAgLy8gc3RhcnQgb2YgdGhlIHNlcXVlbmNlLlxuICAvL1xuICAvLyBTbywgd2UgaGF2ZSB0byBtb3ZlIGFsbCB0aG9zZSBtYXJrZXJzIGFmdGVyIHN1YnNlcXVlbnQgc19jbG9zZSB0YWdzLlxuICAvL1xuICB3aGlsZSAobG9uZU1hcmtlcnMubGVuZ3RoKSB7XG4gICAgaSA9IGxvbmVNYXJrZXJzLnBvcCgpO1xuICAgIGogPSBpICsgMTtcblxuICAgIHdoaWxlIChqIDwgc3RhdGUudG9rZW5zLmxlbmd0aCAmJiBzdGF0ZS50b2tlbnNbal0udHlwZSA9PT0gJ3NfY2xvc2UnKSB7XG4gICAgICBqKys7XG4gICAgfVxuXG4gICAgai0tO1xuXG4gICAgaWYgKGkgIT09IGopIHtcbiAgICAgIHRva2VuID0gc3RhdGUudG9rZW5zW2pdO1xuICAgICAgc3RhdGUudG9rZW5zW2pdID0gc3RhdGUudG9rZW5zW2ldO1xuICAgICAgc3RhdGUudG9rZW5zW2ldID0gdG9rZW47XG4gICAgfVxuICB9XG59XG5cblxuLy8gV2FsayB0aHJvdWdoIGRlbGltaXRlciBsaXN0IGFuZCByZXBsYWNlIHRleHQgdG9rZW5zIHdpdGggdGFnc1xuLy9cbm1vZHVsZS5leHBvcnRzLnBvc3RQcm9jZXNzID0gZnVuY3Rpb24gc3RyaWtldGhyb3VnaChzdGF0ZSkge1xuICB2YXIgY3VycixcbiAgICAgIHRva2Vuc19tZXRhID0gc3RhdGUudG9rZW5zX21ldGEsXG4gICAgICBtYXggPSBzdGF0ZS50b2tlbnNfbWV0YS5sZW5ndGg7XG5cbiAgcG9zdFByb2Nlc3Moc3RhdGUsIHN0YXRlLmRlbGltaXRlcnMpO1xuXG4gIGZvciAoY3VyciA9IDA7IGN1cnIgPCBtYXg7IGN1cnIrKykge1xuICAgIGlmICh0b2tlbnNfbWV0YVtjdXJyXSAmJiB0b2tlbnNfbWV0YVtjdXJyXS5kZWxpbWl0ZXJzKSB7XG4gICAgICBwb3N0UHJvY2VzcyhzdGF0ZSwgdG9rZW5zX21ldGFbY3Vycl0uZGVsaW1pdGVycyk7XG4gICAgfVxuICB9XG59O1xuIiwiLy8gU2tpcCB0ZXh0IGNoYXJhY3RlcnMgZm9yIHRleHQgdG9rZW4sIHBsYWNlIHRob3NlIHRvIHBlbmRpbmcgYnVmZmVyXG4vLyBhbmQgaW5jcmVtZW50IGN1cnJlbnQgcG9zXG5cbid1c2Ugc3RyaWN0JztcblxuXG4vLyBSdWxlIHRvIHNraXAgcHVyZSB0ZXh0XG4vLyAne30kJUB+Kz06JyByZXNlcnZlZCBmb3IgZXh0ZW50aW9uc1xuXG4vLyAhLCBcIiwgIywgJCwgJSwgJiwgJywgKCwgKSwgKiwgKywgLCwgLSwgLiwgLywgOiwgOywgPCwgPSwgPiwgPywgQCwgWywgXFwsIF0sIF4sIF8sIGAsIHssIHwsIH0sIG9yIH5cblxuLy8gISEhISBEb24ndCBjb25mdXNlIHdpdGggXCJNYXJrZG93biBBU0NJSSBQdW5jdHVhdGlvblwiIGNoYXJzXG4vLyBodHRwOi8vc3BlYy5jb21tb25tYXJrLm9yZy8wLjE1LyNhc2NpaS1wdW5jdHVhdGlvbi1jaGFyYWN0ZXJcbmZ1bmN0aW9uIGlzVGVybWluYXRvckNoYXIoY2gpIHtcbiAgc3dpdGNoIChjaCkge1xuICAgIGNhc2UgMHgwQS8qIFxcbiAqLzpcbiAgICBjYXNlIDB4MjEvKiAhICovOlxuICAgIGNhc2UgMHgyMy8qICMgKi86XG4gICAgY2FzZSAweDI0LyogJCAqLzpcbiAgICBjYXNlIDB4MjUvKiAlICovOlxuICAgIGNhc2UgMHgyNi8qICYgKi86XG4gICAgY2FzZSAweDJBLyogKiAqLzpcbiAgICBjYXNlIDB4MkIvKiArICovOlxuICAgIGNhc2UgMHgyRC8qIC0gKi86XG4gICAgY2FzZSAweDNBLyogOiAqLzpcbiAgICBjYXNlIDB4M0MvKiA8ICovOlxuICAgIGNhc2UgMHgzRC8qID0gKi86XG4gICAgY2FzZSAweDNFLyogPiAqLzpcbiAgICBjYXNlIDB4NDAvKiBAICovOlxuICAgIGNhc2UgMHg1Qi8qIFsgKi86XG4gICAgY2FzZSAweDVDLyogXFwgKi86XG4gICAgY2FzZSAweDVELyogXSAqLzpcbiAgICBjYXNlIDB4NUUvKiBeICovOlxuICAgIGNhc2UgMHg1Ri8qIF8gKi86XG4gICAgY2FzZSAweDYwLyogYCAqLzpcbiAgICBjYXNlIDB4N0IvKiB7ICovOlxuICAgIGNhc2UgMHg3RC8qIH0gKi86XG4gICAgY2FzZSAweDdFLyogfiAqLzpcbiAgICAgIHJldHVybiB0cnVlO1xuICAgIGRlZmF1bHQ6XG4gICAgICByZXR1cm4gZmFsc2U7XG4gIH1cbn1cblxubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiB0ZXh0KHN0YXRlLCBzaWxlbnQpIHtcbiAgdmFyIHBvcyA9IHN0YXRlLnBvcztcblxuICB3aGlsZSAocG9zIDwgc3RhdGUucG9zTWF4ICYmICFpc1Rlcm1pbmF0b3JDaGFyKHN0YXRlLnNyYy5jaGFyQ29kZUF0KHBvcykpKSB7XG4gICAgcG9zKys7XG4gIH1cblxuICBpZiAocG9zID09PSBzdGF0ZS5wb3MpIHsgcmV0dXJuIGZhbHNlOyB9XG5cbiAgaWYgKCFzaWxlbnQpIHsgc3RhdGUucGVuZGluZyArPSBzdGF0ZS5zcmMuc2xpY2Uoc3RhdGUucG9zLCBwb3MpOyB9XG5cbiAgc3RhdGUucG9zID0gcG9zO1xuXG4gIHJldHVybiB0cnVlO1xufTtcblxuLy8gQWx0ZXJuYXRpdmUgaW1wbGVtZW50YXRpb24sIGZvciBtZW1vcnkuXG4vL1xuLy8gSXQgY29zdHMgMTAlIG9mIHBlcmZvcm1hbmNlLCBidXQgYWxsb3dzIGV4dGVuZCB0ZXJtaW5hdG9ycyBsaXN0LCBpZiBwbGFjZSBpdFxuLy8gdG8gYFBhcmNlcklubGluZWAgcHJvcGVydHkuIFByb2JhYmx5LCB3aWxsIHN3aXRjaCB0byBpdCBzb21ldGltZSwgc3VjaFxuLy8gZmxleGliaWxpdHkgcmVxdWlyZWQuXG5cbi8qXG52YXIgVEVSTUlOQVRPUl9SRSA9IC9bXFxuISMkJSYqK1xcLTo8PT5AW1xcXFxcXF1eX2B7fX5dLztcblxubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiB0ZXh0KHN0YXRlLCBzaWxlbnQpIHtcbiAgdmFyIHBvcyA9IHN0YXRlLnBvcyxcbiAgICAgIGlkeCA9IHN0YXRlLnNyYy5zbGljZShwb3MpLnNlYXJjaChURVJNSU5BVE9SX1JFKTtcblxuICAvLyBmaXJzdCBjaGFyIGlzIHRlcm1pbmF0b3IgLT4gZW1wdHkgdGV4dFxuICBpZiAoaWR4ID09PSAwKSB7IHJldHVybiBmYWxzZTsgfVxuXG4gIC8vIG5vIHRlcm1pbmF0b3IgLT4gdGV4dCB0aWxsIGVuZCBvZiBzdHJpbmdcbiAgaWYgKGlkeCA8IDApIHtcbiAgICBpZiAoIXNpbGVudCkgeyBzdGF0ZS5wZW5kaW5nICs9IHN0YXRlLnNyYy5zbGljZShwb3MpOyB9XG4gICAgc3RhdGUucG9zID0gc3RhdGUuc3JjLmxlbmd0aDtcbiAgICByZXR1cm4gdHJ1ZTtcbiAgfVxuXG4gIGlmICghc2lsZW50KSB7IHN0YXRlLnBlbmRpbmcgKz0gc3RhdGUuc3JjLnNsaWNlKHBvcywgcG9zICsgaWR4KTsgfVxuXG4gIHN0YXRlLnBvcyArPSBpZHg7XG5cbiAgcmV0dXJuIHRydWU7XG59OyovXG4iLCIvLyBDbGVhbiB1cCB0b2tlbnMgYWZ0ZXIgZW1waGFzaXMgYW5kIHN0cmlrZXRocm91Z2ggcG9zdHByb2Nlc3Npbmc6XG4vLyBtZXJnZSBhZGphY2VudCB0ZXh0IG5vZGVzIGludG8gb25lIGFuZCByZS1jYWxjdWxhdGUgYWxsIHRva2VuIGxldmVsc1xuLy9cbi8vIFRoaXMgaXMgbmVjZXNzYXJ5IGJlY2F1c2UgaW5pdGlhbGx5IGVtcGhhc2lzIGRlbGltaXRlciBtYXJrZXJzICgqLCBfLCB+KVxuLy8gYXJlIHRyZWF0ZWQgYXMgdGhlaXIgb3duIHNlcGFyYXRlIHRleHQgdG9rZW5zLiBUaGVuIGVtcGhhc2lzIHJ1bGUgZWl0aGVyXG4vLyBsZWF2ZXMgdGhlbSBhcyB0ZXh0IChuZWVkZWQgdG8gbWVyZ2Ugd2l0aCBhZGphY2VudCB0ZXh0KSBvciB0dXJucyB0aGVtXG4vLyBpbnRvIG9wZW5pbmcvY2xvc2luZyB0YWdzICh3aGljaCBtZXNzZXMgdXAgbGV2ZWxzIGluc2lkZSkuXG4vL1xuJ3VzZSBzdHJpY3QnO1xuXG5cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gdGV4dF9jb2xsYXBzZShzdGF0ZSkge1xuICB2YXIgY3VyciwgbGFzdCxcbiAgICAgIGxldmVsID0gMCxcbiAgICAgIHRva2VucyA9IHN0YXRlLnRva2VucyxcbiAgICAgIG1heCA9IHN0YXRlLnRva2Vucy5sZW5ndGg7XG5cbiAgZm9yIChjdXJyID0gbGFzdCA9IDA7IGN1cnIgPCBtYXg7IGN1cnIrKykge1xuICAgIC8vIHJlLWNhbGN1bGF0ZSBsZXZlbHMgYWZ0ZXIgZW1waGFzaXMvc3RyaWtldGhyb3VnaCB0dXJucyBzb21lIHRleHQgbm9kZXNcbiAgICAvLyBpbnRvIG9wZW5pbmcvY2xvc2luZyB0YWdzXG4gICAgaWYgKHRva2Vuc1tjdXJyXS5uZXN0aW5nIDwgMCkgbGV2ZWwtLTsgLy8gY2xvc2luZyB0YWdcbiAgICB0b2tlbnNbY3Vycl0ubGV2ZWwgPSBsZXZlbDtcbiAgICBpZiAodG9rZW5zW2N1cnJdLm5lc3RpbmcgPiAwKSBsZXZlbCsrOyAvLyBvcGVuaW5nIHRhZ1xuXG4gICAgaWYgKHRva2Vuc1tjdXJyXS50eXBlID09PSAndGV4dCcgJiZcbiAgICAgICAgY3VyciArIDEgPCBtYXggJiZcbiAgICAgICAgdG9rZW5zW2N1cnIgKyAxXS50eXBlID09PSAndGV4dCcpIHtcblxuICAgICAgLy8gY29sbGFwc2UgdHdvIGFkamFjZW50IHRleHQgbm9kZXNcbiAgICAgIHRva2Vuc1tjdXJyICsgMV0uY29udGVudCA9IHRva2Vuc1tjdXJyXS5jb250ZW50ICsgdG9rZW5zW2N1cnIgKyAxXS5jb250ZW50O1xuICAgIH0gZWxzZSB7XG4gICAgICBpZiAoY3VyciAhPT0gbGFzdCkgeyB0b2tlbnNbbGFzdF0gPSB0b2tlbnNbY3Vycl07IH1cblxuICAgICAgbGFzdCsrO1xuICAgIH1cbiAgfVxuXG4gIGlmIChjdXJyICE9PSBsYXN0KSB7XG4gICAgdG9rZW5zLmxlbmd0aCA9IGxhc3Q7XG4gIH1cbn07XG4iLCIvLyBUb2tlbiBjbGFzc1xuXG4ndXNlIHN0cmljdCc7XG5cblxuLyoqXG4gKiBjbGFzcyBUb2tlblxuICoqL1xuXG4vKipcbiAqIG5ldyBUb2tlbih0eXBlLCB0YWcsIG5lc3RpbmcpXG4gKlxuICogQ3JlYXRlIG5ldyB0b2tlbiBhbmQgZmlsbCBwYXNzZWQgcHJvcGVydGllcy5cbiAqKi9cbmZ1bmN0aW9uIFRva2VuKHR5cGUsIHRhZywgbmVzdGluZykge1xuICAvKipcbiAgICogVG9rZW4jdHlwZSAtPiBTdHJpbmdcbiAgICpcbiAgICogVHlwZSBvZiB0aGUgdG9rZW4gKHN0cmluZywgZS5nLiBcInBhcmFncmFwaF9vcGVuXCIpXG4gICAqKi9cbiAgdGhpcy50eXBlICAgICA9IHR5cGU7XG5cbiAgLyoqXG4gICAqIFRva2VuI3RhZyAtPiBTdHJpbmdcbiAgICpcbiAgICogaHRtbCB0YWcgbmFtZSwgZS5nLiBcInBcIlxuICAgKiovXG4gIHRoaXMudGFnICAgICAgPSB0YWc7XG5cbiAgLyoqXG4gICAqIFRva2VuI2F0dHJzIC0+IEFycmF5XG4gICAqXG4gICAqIEh0bWwgYXR0cmlidXRlcy4gRm9ybWF0OiBgWyBbIG5hbWUxLCB2YWx1ZTEgXSwgWyBuYW1lMiwgdmFsdWUyIF0gXWBcbiAgICoqL1xuICB0aGlzLmF0dHJzICAgID0gbnVsbDtcblxuICAvKipcbiAgICogVG9rZW4jbWFwIC0+IEFycmF5XG4gICAqXG4gICAqIFNvdXJjZSBtYXAgaW5mby4gRm9ybWF0OiBgWyBsaW5lX2JlZ2luLCBsaW5lX2VuZCBdYFxuICAgKiovXG4gIHRoaXMubWFwICAgICAgPSBudWxsO1xuXG4gIC8qKlxuICAgKiBUb2tlbiNuZXN0aW5nIC0+IE51bWJlclxuICAgKlxuICAgKiBMZXZlbCBjaGFuZ2UgKG51bWJlciBpbiB7LTEsIDAsIDF9IHNldCksIHdoZXJlOlxuICAgKlxuICAgKiAtICBgMWAgbWVhbnMgdGhlIHRhZyBpcyBvcGVuaW5nXG4gICAqIC0gIGAwYCBtZWFucyB0aGUgdGFnIGlzIHNlbGYtY2xvc2luZ1xuICAgKiAtIGAtMWAgbWVhbnMgdGhlIHRhZyBpcyBjbG9zaW5nXG4gICAqKi9cbiAgdGhpcy5uZXN0aW5nICA9IG5lc3Rpbmc7XG5cbiAgLyoqXG4gICAqIFRva2VuI2xldmVsIC0+IE51bWJlclxuICAgKlxuICAgKiBuZXN0aW5nIGxldmVsLCB0aGUgc2FtZSBhcyBgc3RhdGUubGV2ZWxgXG4gICAqKi9cbiAgdGhpcy5sZXZlbCAgICA9IDA7XG5cbiAgLyoqXG4gICAqIFRva2VuI2NoaWxkcmVuIC0+IEFycmF5XG4gICAqXG4gICAqIEFuIGFycmF5IG9mIGNoaWxkIG5vZGVzIChpbmxpbmUgYW5kIGltZyB0b2tlbnMpXG4gICAqKi9cbiAgdGhpcy5jaGlsZHJlbiA9IG51bGw7XG5cbiAgLyoqXG4gICAqIFRva2VuI2NvbnRlbnQgLT4gU3RyaW5nXG4gICAqXG4gICAqIEluIGEgY2FzZSBvZiBzZWxmLWNsb3NpbmcgdGFnIChjb2RlLCBodG1sLCBmZW5jZSwgZXRjLiksXG4gICAqIGl0IGhhcyBjb250ZW50cyBvZiB0aGlzIHRhZy5cbiAgICoqL1xuICB0aGlzLmNvbnRlbnQgID0gJyc7XG5cbiAgLyoqXG4gICAqIFRva2VuI21hcmt1cCAtPiBTdHJpbmdcbiAgICpcbiAgICogJyonIG9yICdfJyBmb3IgZW1waGFzaXMsIGZlbmNlIHN0cmluZyBmb3IgZmVuY2UsIGV0Yy5cbiAgICoqL1xuICB0aGlzLm1hcmt1cCAgID0gJyc7XG5cbiAgLyoqXG4gICAqIFRva2VuI2luZm8gLT4gU3RyaW5nXG4gICAqXG4gICAqIGZlbmNlIGluZm9zdHJpbmdcbiAgICoqL1xuICB0aGlzLmluZm8gICAgID0gJyc7XG5cbiAgLyoqXG4gICAqIFRva2VuI21ldGEgLT4gT2JqZWN0XG4gICAqXG4gICAqIEEgcGxhY2UgZm9yIHBsdWdpbnMgdG8gc3RvcmUgYW4gYXJiaXRyYXJ5IGRhdGFcbiAgICoqL1xuICB0aGlzLm1ldGEgICAgID0gbnVsbDtcblxuICAvKipcbiAgICogVG9rZW4jYmxvY2sgLT4gQm9vbGVhblxuICAgKlxuICAgKiBUcnVlIGZvciBibG9jay1sZXZlbCB0b2tlbnMsIGZhbHNlIGZvciBpbmxpbmUgdG9rZW5zLlxuICAgKiBVc2VkIGluIHJlbmRlcmVyIHRvIGNhbGN1bGF0ZSBsaW5lIGJyZWFrc1xuICAgKiovXG4gIHRoaXMuYmxvY2sgICAgPSBmYWxzZTtcblxuICAvKipcbiAgICogVG9rZW4jaGlkZGVuIC0+IEJvb2xlYW5cbiAgICpcbiAgICogSWYgaXQncyB0cnVlLCBpZ25vcmUgdGhpcyBlbGVtZW50IHdoZW4gcmVuZGVyaW5nLiBVc2VkIGZvciB0aWdodCBsaXN0c1xuICAgKiB0byBoaWRlIHBhcmFncmFwaHMuXG4gICAqKi9cbiAgdGhpcy5oaWRkZW4gICA9IGZhbHNlO1xufVxuXG5cbi8qKlxuICogVG9rZW4uYXR0ckluZGV4KG5hbWUpIC0+IE51bWJlclxuICpcbiAqIFNlYXJjaCBhdHRyaWJ1dGUgaW5kZXggYnkgbmFtZS5cbiAqKi9cblRva2VuLnByb3RvdHlwZS5hdHRySW5kZXggPSBmdW5jdGlvbiBhdHRySW5kZXgobmFtZSkge1xuICB2YXIgYXR0cnMsIGksIGxlbjtcblxuICBpZiAoIXRoaXMuYXR0cnMpIHsgcmV0dXJuIC0xOyB9XG5cbiAgYXR0cnMgPSB0aGlzLmF0dHJzO1xuXG4gIGZvciAoaSA9IDAsIGxlbiA9IGF0dHJzLmxlbmd0aDsgaSA8IGxlbjsgaSsrKSB7XG4gICAgaWYgKGF0dHJzW2ldWzBdID09PSBuYW1lKSB7IHJldHVybiBpOyB9XG4gIH1cbiAgcmV0dXJuIC0xO1xufTtcblxuXG4vKipcbiAqIFRva2VuLmF0dHJQdXNoKGF0dHJEYXRhKVxuICpcbiAqIEFkZCBgWyBuYW1lLCB2YWx1ZSBdYCBhdHRyaWJ1dGUgdG8gbGlzdC4gSW5pdCBhdHRycyBpZiBuZWNlc3NhcnlcbiAqKi9cblRva2VuLnByb3RvdHlwZS5hdHRyUHVzaCA9IGZ1bmN0aW9uIGF0dHJQdXNoKGF0dHJEYXRhKSB7XG4gIGlmICh0aGlzLmF0dHJzKSB7XG4gICAgdGhpcy5hdHRycy5wdXNoKGF0dHJEYXRhKTtcbiAgfSBlbHNlIHtcbiAgICB0aGlzLmF0dHJzID0gWyBhdHRyRGF0YSBdO1xuICB9XG59O1xuXG5cbi8qKlxuICogVG9rZW4uYXR0clNldChuYW1lLCB2YWx1ZSlcbiAqXG4gKiBTZXQgYG5hbWVgIGF0dHJpYnV0ZSB0byBgdmFsdWVgLiBPdmVycmlkZSBvbGQgdmFsdWUgaWYgZXhpc3RzLlxuICoqL1xuVG9rZW4ucHJvdG90eXBlLmF0dHJTZXQgPSBmdW5jdGlvbiBhdHRyU2V0KG5hbWUsIHZhbHVlKSB7XG4gIHZhciBpZHggPSB0aGlzLmF0dHJJbmRleChuYW1lKSxcbiAgICAgIGF0dHJEYXRhID0gWyBuYW1lLCB2YWx1ZSBdO1xuXG4gIGlmIChpZHggPCAwKSB7XG4gICAgdGhpcy5hdHRyUHVzaChhdHRyRGF0YSk7XG4gIH0gZWxzZSB7XG4gICAgdGhpcy5hdHRyc1tpZHhdID0gYXR0ckRhdGE7XG4gIH1cbn07XG5cblxuLyoqXG4gKiBUb2tlbi5hdHRyR2V0KG5hbWUpXG4gKlxuICogR2V0IHRoZSB2YWx1ZSBvZiBhdHRyaWJ1dGUgYG5hbWVgLCBvciBudWxsIGlmIGl0IGRvZXMgbm90IGV4aXN0LlxuICoqL1xuVG9rZW4ucHJvdG90eXBlLmF0dHJHZXQgPSBmdW5jdGlvbiBhdHRyR2V0KG5hbWUpIHtcbiAgdmFyIGlkeCA9IHRoaXMuYXR0ckluZGV4KG5hbWUpLCB2YWx1ZSA9IG51bGw7XG4gIGlmIChpZHggPj0gMCkge1xuICAgIHZhbHVlID0gdGhpcy5hdHRyc1tpZHhdWzFdO1xuICB9XG4gIHJldHVybiB2YWx1ZTtcbn07XG5cblxuLyoqXG4gKiBUb2tlbi5hdHRySm9pbihuYW1lLCB2YWx1ZSlcbiAqXG4gKiBKb2luIHZhbHVlIHRvIGV4aXN0aW5nIGF0dHJpYnV0ZSB2aWEgc3BhY2UuIE9yIGNyZWF0ZSBuZXcgYXR0cmlidXRlIGlmIG5vdFxuICogZXhpc3RzLiBVc2VmdWwgdG8gb3BlcmF0ZSB3aXRoIHRva2VuIGNsYXNzZXMuXG4gKiovXG5Ub2tlbi5wcm90b3R5cGUuYXR0ckpvaW4gPSBmdW5jdGlvbiBhdHRySm9pbihuYW1lLCB2YWx1ZSkge1xuICB2YXIgaWR4ID0gdGhpcy5hdHRySW5kZXgobmFtZSk7XG5cbiAgaWYgKGlkeCA8IDApIHtcbiAgICB0aGlzLmF0dHJQdXNoKFsgbmFtZSwgdmFsdWUgXSk7XG4gIH0gZWxzZSB7XG4gICAgdGhpcy5hdHRyc1tpZHhdWzFdID0gdGhpcy5hdHRyc1tpZHhdWzFdICsgJyAnICsgdmFsdWU7XG4gIH1cbn07XG5cblxubW9kdWxlLmV4cG9ydHMgPSBUb2tlbjtcbiIsIlxuJ3VzZSBzdHJpY3QnO1xuXG5cbi8qIGVzbGludC1kaXNhYmxlIG5vLWJpdHdpc2UgKi9cblxudmFyIGRlY29kZUNhY2hlID0ge307XG5cbmZ1bmN0aW9uIGdldERlY29kZUNhY2hlKGV4Y2x1ZGUpIHtcbiAgdmFyIGksIGNoLCBjYWNoZSA9IGRlY29kZUNhY2hlW2V4Y2x1ZGVdO1xuICBpZiAoY2FjaGUpIHsgcmV0dXJuIGNhY2hlOyB9XG5cbiAgY2FjaGUgPSBkZWNvZGVDYWNoZVtleGNsdWRlXSA9IFtdO1xuXG4gIGZvciAoaSA9IDA7IGkgPCAxMjg7IGkrKykge1xuICAgIGNoID0gU3RyaW5nLmZyb21DaGFyQ29kZShpKTtcbiAgICBjYWNoZS5wdXNoKGNoKTtcbiAgfVxuXG4gIGZvciAoaSA9IDA7IGkgPCBleGNsdWRlLmxlbmd0aDsgaSsrKSB7XG4gICAgY2ggPSBleGNsdWRlLmNoYXJDb2RlQXQoaSk7XG4gICAgY2FjaGVbY2hdID0gJyUnICsgKCcwJyArIGNoLnRvU3RyaW5nKDE2KS50b1VwcGVyQ2FzZSgpKS5zbGljZSgtMik7XG4gIH1cblxuICByZXR1cm4gY2FjaGU7XG59XG5cblxuLy8gRGVjb2RlIHBlcmNlbnQtZW5jb2RlZCBzdHJpbmcuXG4vL1xuZnVuY3Rpb24gZGVjb2RlKHN0cmluZywgZXhjbHVkZSkge1xuICB2YXIgY2FjaGU7XG5cbiAgaWYgKHR5cGVvZiBleGNsdWRlICE9PSAnc3RyaW5nJykge1xuICAgIGV4Y2x1ZGUgPSBkZWNvZGUuZGVmYXVsdENoYXJzO1xuICB9XG5cbiAgY2FjaGUgPSBnZXREZWNvZGVDYWNoZShleGNsdWRlKTtcblxuICByZXR1cm4gc3RyaW5nLnJlcGxhY2UoLyglW2EtZjAtOV17Mn0pKy9naSwgZnVuY3Rpb24oc2VxKSB7XG4gICAgdmFyIGksIGwsIGIxLCBiMiwgYjMsIGI0LCBjaHIsXG4gICAgICAgIHJlc3VsdCA9ICcnO1xuXG4gICAgZm9yIChpID0gMCwgbCA9IHNlcS5sZW5ndGg7IGkgPCBsOyBpICs9IDMpIHtcbiAgICAgIGIxID0gcGFyc2VJbnQoc2VxLnNsaWNlKGkgKyAxLCBpICsgMyksIDE2KTtcblxuICAgICAgaWYgKGIxIDwgMHg4MCkge1xuICAgICAgICByZXN1bHQgKz0gY2FjaGVbYjFdO1xuICAgICAgICBjb250aW51ZTtcbiAgICAgIH1cblxuICAgICAgaWYgKChiMSAmIDB4RTApID09PSAweEMwICYmIChpICsgMyA8IGwpKSB7XG4gICAgICAgIC8vIDExMHh4eHh4IDEweHh4eHh4XG4gICAgICAgIGIyID0gcGFyc2VJbnQoc2VxLnNsaWNlKGkgKyA0LCBpICsgNiksIDE2KTtcblxuICAgICAgICBpZiAoKGIyICYgMHhDMCkgPT09IDB4ODApIHtcbiAgICAgICAgICBjaHIgPSAoKGIxIDw8IDYpICYgMHg3QzApIHwgKGIyICYgMHgzRik7XG5cbiAgICAgICAgICBpZiAoY2hyIDwgMHg4MCkge1xuICAgICAgICAgICAgcmVzdWx0ICs9ICdcXHVmZmZkXFx1ZmZmZCc7XG4gICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHJlc3VsdCArPSBTdHJpbmcuZnJvbUNoYXJDb2RlKGNocik7XG4gICAgICAgICAgfVxuXG4gICAgICAgICAgaSArPSAzO1xuICAgICAgICAgIGNvbnRpbnVlO1xuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIGlmICgoYjEgJiAweEYwKSA9PT0gMHhFMCAmJiAoaSArIDYgPCBsKSkge1xuICAgICAgICAvLyAxMTEweHh4eCAxMHh4eHh4eCAxMHh4eHh4eFxuICAgICAgICBiMiA9IHBhcnNlSW50KHNlcS5zbGljZShpICsgNCwgaSArIDYpLCAxNik7XG4gICAgICAgIGIzID0gcGFyc2VJbnQoc2VxLnNsaWNlKGkgKyA3LCBpICsgOSksIDE2KTtcblxuICAgICAgICBpZiAoKGIyICYgMHhDMCkgPT09IDB4ODAgJiYgKGIzICYgMHhDMCkgPT09IDB4ODApIHtcbiAgICAgICAgICBjaHIgPSAoKGIxIDw8IDEyKSAmIDB4RjAwMCkgfCAoKGIyIDw8IDYpICYgMHhGQzApIHwgKGIzICYgMHgzRik7XG5cbiAgICAgICAgICBpZiAoY2hyIDwgMHg4MDAgfHwgKGNociA+PSAweEQ4MDAgJiYgY2hyIDw9IDB4REZGRikpIHtcbiAgICAgICAgICAgIHJlc3VsdCArPSAnXFx1ZmZmZFxcdWZmZmRcXHVmZmZkJztcbiAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgcmVzdWx0ICs9IFN0cmluZy5mcm9tQ2hhckNvZGUoY2hyKTtcbiAgICAgICAgICB9XG5cbiAgICAgICAgICBpICs9IDY7XG4gICAgICAgICAgY29udGludWU7XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgaWYgKChiMSAmIDB4RjgpID09PSAweEYwICYmIChpICsgOSA8IGwpKSB7XG4gICAgICAgIC8vIDExMTExMHh4IDEweHh4eHh4IDEweHh4eHh4IDEweHh4eHh4XG4gICAgICAgIGIyID0gcGFyc2VJbnQoc2VxLnNsaWNlKGkgKyA0LCBpICsgNiksIDE2KTtcbiAgICAgICAgYjMgPSBwYXJzZUludChzZXEuc2xpY2UoaSArIDcsIGkgKyA5KSwgMTYpO1xuICAgICAgICBiNCA9IHBhcnNlSW50KHNlcS5zbGljZShpICsgMTAsIGkgKyAxMiksIDE2KTtcblxuICAgICAgICBpZiAoKGIyICYgMHhDMCkgPT09IDB4ODAgJiYgKGIzICYgMHhDMCkgPT09IDB4ODAgJiYgKGI0ICYgMHhDMCkgPT09IDB4ODApIHtcbiAgICAgICAgICBjaHIgPSAoKGIxIDw8IDE4KSAmIDB4MUMwMDAwKSB8ICgoYjIgPDwgMTIpICYgMHgzRjAwMCkgfCAoKGIzIDw8IDYpICYgMHhGQzApIHwgKGI0ICYgMHgzRik7XG5cbiAgICAgICAgICBpZiAoY2hyIDwgMHgxMDAwMCB8fCBjaHIgPiAweDEwRkZGRikge1xuICAgICAgICAgICAgcmVzdWx0ICs9ICdcXHVmZmZkXFx1ZmZmZFxcdWZmZmRcXHVmZmZkJztcbiAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgY2hyIC09IDB4MTAwMDA7XG4gICAgICAgICAgICByZXN1bHQgKz0gU3RyaW5nLmZyb21DaGFyQ29kZSgweEQ4MDAgKyAoY2hyID4+IDEwKSwgMHhEQzAwICsgKGNociAmIDB4M0ZGKSk7XG4gICAgICAgICAgfVxuXG4gICAgICAgICAgaSArPSA5O1xuICAgICAgICAgIGNvbnRpbnVlO1xuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIHJlc3VsdCArPSAnXFx1ZmZmZCc7XG4gICAgfVxuXG4gICAgcmV0dXJuIHJlc3VsdDtcbiAgfSk7XG59XG5cblxuZGVjb2RlLmRlZmF1bHRDaGFycyAgID0gJzsvPzpAJj0rJCwjJztcbmRlY29kZS5jb21wb25lbnRDaGFycyA9ICcnO1xuXG5cbm1vZHVsZS5leHBvcnRzID0gZGVjb2RlO1xuIiwiXG4ndXNlIHN0cmljdCc7XG5cblxudmFyIGVuY29kZUNhY2hlID0ge307XG5cblxuLy8gQ3JlYXRlIGEgbG9va3VwIGFycmF5IHdoZXJlIGFueXRoaW5nIGJ1dCBjaGFyYWN0ZXJzIGluIGBjaGFyc2Agc3RyaW5nXG4vLyBhbmQgYWxwaGFudW1lcmljIGNoYXJzIGlzIHBlcmNlbnQtZW5jb2RlZC5cbi8vXG5mdW5jdGlvbiBnZXRFbmNvZGVDYWNoZShleGNsdWRlKSB7XG4gIHZhciBpLCBjaCwgY2FjaGUgPSBlbmNvZGVDYWNoZVtleGNsdWRlXTtcbiAgaWYgKGNhY2hlKSB7IHJldHVybiBjYWNoZTsgfVxuXG4gIGNhY2hlID0gZW5jb2RlQ2FjaGVbZXhjbHVkZV0gPSBbXTtcblxuICBmb3IgKGkgPSAwOyBpIDwgMTI4OyBpKyspIHtcbiAgICBjaCA9IFN0cmluZy5mcm9tQ2hhckNvZGUoaSk7XG5cbiAgICBpZiAoL15bMC05YS16XSQvaS50ZXN0KGNoKSkge1xuICAgICAgLy8gYWx3YXlzIGFsbG93IHVuZW5jb2RlZCBhbHBoYW51bWVyaWMgY2hhcmFjdGVyc1xuICAgICAgY2FjaGUucHVzaChjaCk7XG4gICAgfSBlbHNlIHtcbiAgICAgIGNhY2hlLnB1c2goJyUnICsgKCcwJyArIGkudG9TdHJpbmcoMTYpLnRvVXBwZXJDYXNlKCkpLnNsaWNlKC0yKSk7XG4gICAgfVxuICB9XG5cbiAgZm9yIChpID0gMDsgaSA8IGV4Y2x1ZGUubGVuZ3RoOyBpKyspIHtcbiAgICBjYWNoZVtleGNsdWRlLmNoYXJDb2RlQXQoaSldID0gZXhjbHVkZVtpXTtcbiAgfVxuXG4gIHJldHVybiBjYWNoZTtcbn1cblxuXG4vLyBFbmNvZGUgdW5zYWZlIGNoYXJhY3RlcnMgd2l0aCBwZXJjZW50LWVuY29kaW5nLCBza2lwcGluZyBhbHJlYWR5XG4vLyBlbmNvZGVkIHNlcXVlbmNlcy5cbi8vXG4vLyAgLSBzdHJpbmcgICAgICAgLSBzdHJpbmcgdG8gZW5jb2RlXG4vLyAgLSBleGNsdWRlICAgICAgLSBsaXN0IG9mIGNoYXJhY3RlcnMgdG8gaWdub3JlIChpbiBhZGRpdGlvbiB0byBhLXpBLVowLTkpXG4vLyAgLSBrZWVwRXNjYXBlZCAgLSBkb24ndCBlbmNvZGUgJyUnIGluIGEgY29ycmVjdCBlc2NhcGUgc2VxdWVuY2UgKGRlZmF1bHQ6IHRydWUpXG4vL1xuZnVuY3Rpb24gZW5jb2RlKHN0cmluZywgZXhjbHVkZSwga2VlcEVzY2FwZWQpIHtcbiAgdmFyIGksIGwsIGNvZGUsIG5leHRDb2RlLCBjYWNoZSxcbiAgICAgIHJlc3VsdCA9ICcnO1xuXG4gIGlmICh0eXBlb2YgZXhjbHVkZSAhPT0gJ3N0cmluZycpIHtcbiAgICAvLyBlbmNvZGUoc3RyaW5nLCBrZWVwRXNjYXBlZClcbiAgICBrZWVwRXNjYXBlZCAgPSBleGNsdWRlO1xuICAgIGV4Y2x1ZGUgPSBlbmNvZGUuZGVmYXVsdENoYXJzO1xuICB9XG5cbiAgaWYgKHR5cGVvZiBrZWVwRXNjYXBlZCA9PT0gJ3VuZGVmaW5lZCcpIHtcbiAgICBrZWVwRXNjYXBlZCA9IHRydWU7XG4gIH1cblxuICBjYWNoZSA9IGdldEVuY29kZUNhY2hlKGV4Y2x1ZGUpO1xuXG4gIGZvciAoaSA9IDAsIGwgPSBzdHJpbmcubGVuZ3RoOyBpIDwgbDsgaSsrKSB7XG4gICAgY29kZSA9IHN0cmluZy5jaGFyQ29kZUF0KGkpO1xuXG4gICAgaWYgKGtlZXBFc2NhcGVkICYmIGNvZGUgPT09IDB4MjUgLyogJSAqLyAmJiBpICsgMiA8IGwpIHtcbiAgICAgIGlmICgvXlswLTlhLWZdezJ9JC9pLnRlc3Qoc3RyaW5nLnNsaWNlKGkgKyAxLCBpICsgMykpKSB7XG4gICAgICAgIHJlc3VsdCArPSBzdHJpbmcuc2xpY2UoaSwgaSArIDMpO1xuICAgICAgICBpICs9IDI7XG4gICAgICAgIGNvbnRpbnVlO1xuICAgICAgfVxuICAgIH1cblxuICAgIGlmIChjb2RlIDwgMTI4KSB7XG4gICAgICByZXN1bHQgKz0gY2FjaGVbY29kZV07XG4gICAgICBjb250aW51ZTtcbiAgICB9XG5cbiAgICBpZiAoY29kZSA+PSAweEQ4MDAgJiYgY29kZSA8PSAweERGRkYpIHtcbiAgICAgIGlmIChjb2RlID49IDB4RDgwMCAmJiBjb2RlIDw9IDB4REJGRiAmJiBpICsgMSA8IGwpIHtcbiAgICAgICAgbmV4dENvZGUgPSBzdHJpbmcuY2hhckNvZGVBdChpICsgMSk7XG4gICAgICAgIGlmIChuZXh0Q29kZSA+PSAweERDMDAgJiYgbmV4dENvZGUgPD0gMHhERkZGKSB7XG4gICAgICAgICAgcmVzdWx0ICs9IGVuY29kZVVSSUNvbXBvbmVudChzdHJpbmdbaV0gKyBzdHJpbmdbaSArIDFdKTtcbiAgICAgICAgICBpKys7XG4gICAgICAgICAgY29udGludWU7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICAgIHJlc3VsdCArPSAnJUVGJUJGJUJEJztcbiAgICAgIGNvbnRpbnVlO1xuICAgIH1cblxuICAgIHJlc3VsdCArPSBlbmNvZGVVUklDb21wb25lbnQoc3RyaW5nW2ldKTtcbiAgfVxuXG4gIHJldHVybiByZXN1bHQ7XG59XG5cbmVuY29kZS5kZWZhdWx0Q2hhcnMgICA9IFwiOy8/OkAmPSskLC1fLiF+KicoKSNcIjtcbmVuY29kZS5jb21wb25lbnRDaGFycyA9IFwiLV8uIX4qJygpXCI7XG5cblxubW9kdWxlLmV4cG9ydHMgPSBlbmNvZGU7XG4iLCJcbid1c2Ugc3RyaWN0JztcblxuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIGZvcm1hdCh1cmwpIHtcbiAgdmFyIHJlc3VsdCA9ICcnO1xuXG4gIHJlc3VsdCArPSB1cmwucHJvdG9jb2wgfHwgJyc7XG4gIHJlc3VsdCArPSB1cmwuc2xhc2hlcyA/ICcvLycgOiAnJztcbiAgcmVzdWx0ICs9IHVybC5hdXRoID8gdXJsLmF1dGggKyAnQCcgOiAnJztcblxuICBpZiAodXJsLmhvc3RuYW1lICYmIHVybC5ob3N0bmFtZS5pbmRleE9mKCc6JykgIT09IC0xKSB7XG4gICAgLy8gaXB2NiBhZGRyZXNzXG4gICAgcmVzdWx0ICs9ICdbJyArIHVybC5ob3N0bmFtZSArICddJztcbiAgfSBlbHNlIHtcbiAgICByZXN1bHQgKz0gdXJsLmhvc3RuYW1lIHx8ICcnO1xuICB9XG5cbiAgcmVzdWx0ICs9IHVybC5wb3J0ID8gJzonICsgdXJsLnBvcnQgOiAnJztcbiAgcmVzdWx0ICs9IHVybC5wYXRobmFtZSB8fCAnJztcbiAgcmVzdWx0ICs9IHVybC5zZWFyY2ggfHwgJyc7XG4gIHJlc3VsdCArPSB1cmwuaGFzaCB8fCAnJztcblxuICByZXR1cm4gcmVzdWx0O1xufTtcbiIsIid1c2Ugc3RyaWN0JztcblxuXG5tb2R1bGUuZXhwb3J0cy5lbmNvZGUgPSByZXF1aXJlKCcuL2VuY29kZScpO1xubW9kdWxlLmV4cG9ydHMuZGVjb2RlID0gcmVxdWlyZSgnLi9kZWNvZGUnKTtcbm1vZHVsZS5leHBvcnRzLmZvcm1hdCA9IHJlcXVpcmUoJy4vZm9ybWF0Jyk7XG5tb2R1bGUuZXhwb3J0cy5wYXJzZSAgPSByZXF1aXJlKCcuL3BhcnNlJyk7XG4iLCIvLyBDb3B5cmlnaHQgSm95ZW50LCBJbmMuIGFuZCBvdGhlciBOb2RlIGNvbnRyaWJ1dG9ycy5cbi8vXG4vLyBQZXJtaXNzaW9uIGlzIGhlcmVieSBncmFudGVkLCBmcmVlIG9mIGNoYXJnZSwgdG8gYW55IHBlcnNvbiBvYnRhaW5pbmcgYVxuLy8gY29weSBvZiB0aGlzIHNvZnR3YXJlIGFuZCBhc3NvY2lhdGVkIGRvY3VtZW50YXRpb24gZmlsZXMgKHRoZVxuLy8gXCJTb2Z0d2FyZVwiKSwgdG8gZGVhbCBpbiB0aGUgU29mdHdhcmUgd2l0aG91dCByZXN0cmljdGlvbiwgaW5jbHVkaW5nXG4vLyB3aXRob3V0IGxpbWl0YXRpb24gdGhlIHJpZ2h0cyB0byB1c2UsIGNvcHksIG1vZGlmeSwgbWVyZ2UsIHB1Ymxpc2gsXG4vLyBkaXN0cmlidXRlLCBzdWJsaWNlbnNlLCBhbmQvb3Igc2VsbCBjb3BpZXMgb2YgdGhlIFNvZnR3YXJlLCBhbmQgdG8gcGVybWl0XG4vLyBwZXJzb25zIHRvIHdob20gdGhlIFNvZnR3YXJlIGlzIGZ1cm5pc2hlZCB0byBkbyBzbywgc3ViamVjdCB0byB0aGVcbi8vIGZvbGxvd2luZyBjb25kaXRpb25zOlxuLy9cbi8vIFRoZSBhYm92ZSBjb3B5cmlnaHQgbm90aWNlIGFuZCB0aGlzIHBlcm1pc3Npb24gbm90aWNlIHNoYWxsIGJlIGluY2x1ZGVkXG4vLyBpbiBhbGwgY29waWVzIG9yIHN1YnN0YW50aWFsIHBvcnRpb25zIG9mIHRoZSBTb2Z0d2FyZS5cbi8vXG4vLyBUSEUgU09GVFdBUkUgSVMgUFJPVklERUQgXCJBUyBJU1wiLCBXSVRIT1VUIFdBUlJBTlRZIE9GIEFOWSBLSU5ELCBFWFBSRVNTXG4vLyBPUiBJTVBMSUVELCBJTkNMVURJTkcgQlVUIE5PVCBMSU1JVEVEIFRPIFRIRSBXQVJSQU5USUVTIE9GXG4vLyBNRVJDSEFOVEFCSUxJVFksIEZJVE5FU1MgRk9SIEEgUEFSVElDVUxBUiBQVVJQT1NFIEFORCBOT05JTkZSSU5HRU1FTlQuIElOXG4vLyBOTyBFVkVOVCBTSEFMTCBUSEUgQVVUSE9SUyBPUiBDT1BZUklHSFQgSE9MREVSUyBCRSBMSUFCTEUgRk9SIEFOWSBDTEFJTSxcbi8vIERBTUFHRVMgT1IgT1RIRVIgTElBQklMSVRZLCBXSEVUSEVSIElOIEFOIEFDVElPTiBPRiBDT05UUkFDVCwgVE9SVCBPUlxuLy8gT1RIRVJXSVNFLCBBUklTSU5HIEZST00sIE9VVCBPRiBPUiBJTiBDT05ORUNUSU9OIFdJVEggVEhFIFNPRlRXQVJFIE9SIFRIRVxuLy8gVVNFIE9SIE9USEVSIERFQUxJTkdTIElOIFRIRSBTT0ZUV0FSRS5cblxuJ3VzZSBzdHJpY3QnO1xuXG4vL1xuLy8gQ2hhbmdlcyBmcm9tIGpveWVudC9ub2RlOlxuLy9cbi8vIDEuIE5vIGxlYWRpbmcgc2xhc2ggaW4gcGF0aHMsXG4vLyAgICBlLmcuIGluIGB1cmwucGFyc2UoJ2h0dHA6Ly9mb28/YmFyJylgIHBhdGhuYW1lIGlzIGBgLCBub3QgYC9gXG4vL1xuLy8gMi4gQmFja3NsYXNoZXMgYXJlIG5vdCByZXBsYWNlZCB3aXRoIHNsYXNoZXMsXG4vLyAgICBzbyBgaHR0cDpcXFxcZXhhbXBsZS5vcmdcXGAgaXMgdHJlYXRlZCBsaWtlIGEgcmVsYXRpdmUgcGF0aFxuLy9cbi8vIDMuIFRyYWlsaW5nIGNvbG9uIGlzIHRyZWF0ZWQgbGlrZSBhIHBhcnQgb2YgdGhlIHBhdGgsXG4vLyAgICBpLmUuIGluIGBodHRwOi8vZXhhbXBsZS5vcmc6Zm9vYCBwYXRobmFtZSBpcyBgOmZvb2Bcbi8vXG4vLyA0LiBOb3RoaW5nIGlzIFVSTC1lbmNvZGVkIGluIHRoZSByZXN1bHRpbmcgb2JqZWN0LFxuLy8gICAgKGluIGpveWVudC9ub2RlIHNvbWUgY2hhcnMgaW4gYXV0aCBhbmQgcGF0aHMgYXJlIGVuY29kZWQpXG4vL1xuLy8gNS4gYHVybC5wYXJzZSgpYCBkb2VzIG5vdCBoYXZlIGBwYXJzZVF1ZXJ5U3RyaW5nYCBhcmd1bWVudFxuLy9cbi8vIDYuIFJlbW92ZWQgZXh0cmFuZW91cyByZXN1bHQgcHJvcGVydGllczogYGhvc3RgLCBgcGF0aGAsIGBxdWVyeWAsIGV0Yy4sXG4vLyAgICB3aGljaCBjYW4gYmUgY29uc3RydWN0ZWQgdXNpbmcgb3RoZXIgcGFydHMgb2YgdGhlIHVybC5cbi8vXG5cblxuZnVuY3Rpb24gVXJsKCkge1xuICB0aGlzLnByb3RvY29sID0gbnVsbDtcbiAgdGhpcy5zbGFzaGVzID0gbnVsbDtcbiAgdGhpcy5hdXRoID0gbnVsbDtcbiAgdGhpcy5wb3J0ID0gbnVsbDtcbiAgdGhpcy5ob3N0bmFtZSA9IG51bGw7XG4gIHRoaXMuaGFzaCA9IG51bGw7XG4gIHRoaXMuc2VhcmNoID0gbnVsbDtcbiAgdGhpcy5wYXRobmFtZSA9IG51bGw7XG59XG5cbi8vIFJlZmVyZW5jZTogUkZDIDM5ODYsIFJGQyAxODA4LCBSRkMgMjM5NlxuXG4vLyBkZWZpbmUgdGhlc2UgaGVyZSBzbyBhdCBsZWFzdCB0aGV5IG9ubHkgaGF2ZSB0byBiZVxuLy8gY29tcGlsZWQgb25jZSBvbiB0aGUgZmlyc3QgbW9kdWxlIGxvYWQuXG52YXIgcHJvdG9jb2xQYXR0ZXJuID0gL14oW2EtejAtOS4rLV0rOikvaSxcbiAgICBwb3J0UGF0dGVybiA9IC86WzAtOV0qJC8sXG5cbiAgICAvLyBTcGVjaWFsIGNhc2UgZm9yIGEgc2ltcGxlIHBhdGggVVJMXG4gICAgc2ltcGxlUGF0aFBhdHRlcm4gPSAvXihcXC9cXC8/KD8hXFwvKVteXFw/XFxzXSopKFxcP1teXFxzXSopPyQvLFxuXG4gICAgLy8gUkZDIDIzOTY6IGNoYXJhY3RlcnMgcmVzZXJ2ZWQgZm9yIGRlbGltaXRpbmcgVVJMcy5cbiAgICAvLyBXZSBhY3R1YWxseSBqdXN0IGF1dG8tZXNjYXBlIHRoZXNlLlxuICAgIGRlbGltcyA9IFsgJzwnLCAnPicsICdcIicsICdgJywgJyAnLCAnXFxyJywgJ1xcbicsICdcXHQnIF0sXG5cbiAgICAvLyBSRkMgMjM5NjogY2hhcmFjdGVycyBub3QgYWxsb3dlZCBmb3IgdmFyaW91cyByZWFzb25zLlxuICAgIHVud2lzZSA9IFsgJ3snLCAnfScsICd8JywgJ1xcXFwnLCAnXicsICdgJyBdLmNvbmNhdChkZWxpbXMpLFxuXG4gICAgLy8gQWxsb3dlZCBieSBSRkNzLCBidXQgY2F1c2Ugb2YgWFNTIGF0dGFja3MuICBBbHdheXMgZXNjYXBlIHRoZXNlLlxuICAgIGF1dG9Fc2NhcGUgPSBbICdcXCcnIF0uY29uY2F0KHVud2lzZSksXG4gICAgLy8gQ2hhcmFjdGVycyB0aGF0IGFyZSBuZXZlciBldmVyIGFsbG93ZWQgaW4gYSBob3N0bmFtZS5cbiAgICAvLyBOb3RlIHRoYXQgYW55IGludmFsaWQgY2hhcnMgYXJlIGFsc28gaGFuZGxlZCwgYnV0IHRoZXNlXG4gICAgLy8gYXJlIHRoZSBvbmVzIHRoYXQgYXJlICpleHBlY3RlZCogdG8gYmUgc2Vlbiwgc28gd2UgZmFzdC1wYXRoXG4gICAgLy8gdGhlbS5cbiAgICBub25Ib3N0Q2hhcnMgPSBbICclJywgJy8nLCAnPycsICc7JywgJyMnIF0uY29uY2F0KGF1dG9Fc2NhcGUpLFxuICAgIGhvc3RFbmRpbmdDaGFycyA9IFsgJy8nLCAnPycsICcjJyBdLFxuICAgIGhvc3RuYW1lTWF4TGVuID0gMjU1LFxuICAgIGhvc3RuYW1lUGFydFBhdHRlcm4gPSAvXlsrYS16MC05QS1aXy1dezAsNjN9JC8sXG4gICAgaG9zdG5hbWVQYXJ0U3RhcnQgPSAvXihbK2EtejAtOUEtWl8tXXswLDYzfSkoLiopJC8sXG4gICAgLy8gcHJvdG9jb2xzIHRoYXQgY2FuIGFsbG93IFwidW5zYWZlXCIgYW5kIFwidW53aXNlXCIgY2hhcnMuXG4gICAgLyogZXNsaW50LWRpc2FibGUgbm8tc2NyaXB0LXVybCAqL1xuICAgIC8vIHByb3RvY29scyB0aGF0IG5ldmVyIGhhdmUgYSBob3N0bmFtZS5cbiAgICBob3N0bGVzc1Byb3RvY29sID0ge1xuICAgICAgJ2phdmFzY3JpcHQnOiB0cnVlLFxuICAgICAgJ2phdmFzY3JpcHQ6JzogdHJ1ZVxuICAgIH0sXG4gICAgLy8gcHJvdG9jb2xzIHRoYXQgYWx3YXlzIGNvbnRhaW4gYSAvLyBiaXQuXG4gICAgc2xhc2hlZFByb3RvY29sID0ge1xuICAgICAgJ2h0dHAnOiB0cnVlLFxuICAgICAgJ2h0dHBzJzogdHJ1ZSxcbiAgICAgICdmdHAnOiB0cnVlLFxuICAgICAgJ2dvcGhlcic6IHRydWUsXG4gICAgICAnZmlsZSc6IHRydWUsXG4gICAgICAnaHR0cDonOiB0cnVlLFxuICAgICAgJ2h0dHBzOic6IHRydWUsXG4gICAgICAnZnRwOic6IHRydWUsXG4gICAgICAnZ29waGVyOic6IHRydWUsXG4gICAgICAnZmlsZTonOiB0cnVlXG4gICAgfTtcbiAgICAvKiBlc2xpbnQtZW5hYmxlIG5vLXNjcmlwdC11cmwgKi9cblxuZnVuY3Rpb24gdXJsUGFyc2UodXJsLCBzbGFzaGVzRGVub3RlSG9zdCkge1xuICBpZiAodXJsICYmIHVybCBpbnN0YW5jZW9mIFVybCkgeyByZXR1cm4gdXJsOyB9XG5cbiAgdmFyIHUgPSBuZXcgVXJsKCk7XG4gIHUucGFyc2UodXJsLCBzbGFzaGVzRGVub3RlSG9zdCk7XG4gIHJldHVybiB1O1xufVxuXG5VcmwucHJvdG90eXBlLnBhcnNlID0gZnVuY3Rpb24odXJsLCBzbGFzaGVzRGVub3RlSG9zdCkge1xuICB2YXIgaSwgbCwgbG93ZXJQcm90bywgaGVjLCBzbGFzaGVzLFxuICAgICAgcmVzdCA9IHVybDtcblxuICAvLyB0cmltIGJlZm9yZSBwcm9jZWVkaW5nLlxuICAvLyBUaGlzIGlzIHRvIHN1cHBvcnQgcGFyc2Ugc3R1ZmYgbGlrZSBcIiAgaHR0cDovL2Zvby5jb20gIFxcblwiXG4gIHJlc3QgPSByZXN0LnRyaW0oKTtcblxuICBpZiAoIXNsYXNoZXNEZW5vdGVIb3N0ICYmIHVybC5zcGxpdCgnIycpLmxlbmd0aCA9PT0gMSkge1xuICAgIC8vIFRyeSBmYXN0IHBhdGggcmVnZXhwXG4gICAgdmFyIHNpbXBsZVBhdGggPSBzaW1wbGVQYXRoUGF0dGVybi5leGVjKHJlc3QpO1xuICAgIGlmIChzaW1wbGVQYXRoKSB7XG4gICAgICB0aGlzLnBhdGhuYW1lID0gc2ltcGxlUGF0aFsxXTtcbiAgICAgIGlmIChzaW1wbGVQYXRoWzJdKSB7XG4gICAgICAgIHRoaXMuc2VhcmNoID0gc2ltcGxlUGF0aFsyXTtcbiAgICAgIH1cbiAgICAgIHJldHVybiB0aGlzO1xuICAgIH1cbiAgfVxuXG4gIHZhciBwcm90byA9IHByb3RvY29sUGF0dGVybi5leGVjKHJlc3QpO1xuICBpZiAocHJvdG8pIHtcbiAgICBwcm90byA9IHByb3RvWzBdO1xuICAgIGxvd2VyUHJvdG8gPSBwcm90by50b0xvd2VyQ2FzZSgpO1xuICAgIHRoaXMucHJvdG9jb2wgPSBwcm90bztcbiAgICByZXN0ID0gcmVzdC5zdWJzdHIocHJvdG8ubGVuZ3RoKTtcbiAgfVxuXG4gIC8vIGZpZ3VyZSBvdXQgaWYgaXQncyBnb3QgYSBob3N0XG4gIC8vIHVzZXJAc2VydmVyIGlzICphbHdheXMqIGludGVycHJldGVkIGFzIGEgaG9zdG5hbWUsIGFuZCB1cmxcbiAgLy8gcmVzb2x1dGlvbiB3aWxsIHRyZWF0IC8vZm9vL2JhciBhcyBob3N0PWZvbyxwYXRoPWJhciBiZWNhdXNlIHRoYXQnc1xuICAvLyBob3cgdGhlIGJyb3dzZXIgcmVzb2x2ZXMgcmVsYXRpdmUgVVJMcy5cbiAgaWYgKHNsYXNoZXNEZW5vdGVIb3N0IHx8IHByb3RvIHx8IHJlc3QubWF0Y2goL15cXC9cXC9bXkBcXC9dK0BbXkBcXC9dKy8pKSB7XG4gICAgc2xhc2hlcyA9IHJlc3Quc3Vic3RyKDAsIDIpID09PSAnLy8nO1xuICAgIGlmIChzbGFzaGVzICYmICEocHJvdG8gJiYgaG9zdGxlc3NQcm90b2NvbFtwcm90b10pKSB7XG4gICAgICByZXN0ID0gcmVzdC5zdWJzdHIoMik7XG4gICAgICB0aGlzLnNsYXNoZXMgPSB0cnVlO1xuICAgIH1cbiAgfVxuXG4gIGlmICghaG9zdGxlc3NQcm90b2NvbFtwcm90b10gJiZcbiAgICAgIChzbGFzaGVzIHx8IChwcm90byAmJiAhc2xhc2hlZFByb3RvY29sW3Byb3RvXSkpKSB7XG5cbiAgICAvLyB0aGVyZSdzIGEgaG9zdG5hbWUuXG4gICAgLy8gdGhlIGZpcnN0IGluc3RhbmNlIG9mIC8sID8sIDssIG9yICMgZW5kcyB0aGUgaG9zdC5cbiAgICAvL1xuICAgIC8vIElmIHRoZXJlIGlzIGFuIEAgaW4gdGhlIGhvc3RuYW1lLCB0aGVuIG5vbi1ob3N0IGNoYXJzICphcmUqIGFsbG93ZWRcbiAgICAvLyB0byB0aGUgbGVmdCBvZiB0aGUgbGFzdCBAIHNpZ24sIHVubGVzcyBzb21lIGhvc3QtZW5kaW5nIGNoYXJhY3RlclxuICAgIC8vIGNvbWVzICpiZWZvcmUqIHRoZSBALXNpZ24uXG4gICAgLy8gVVJMcyBhcmUgb2Jub3hpb3VzLlxuICAgIC8vXG4gICAgLy8gZXg6XG4gICAgLy8gaHR0cDovL2FAYkBjLyA9PiB1c2VyOmFAYiBob3N0OmNcbiAgICAvLyBodHRwOi8vYUBiP0BjID0+IHVzZXI6YSBob3N0OmMgcGF0aDovP0BjXG5cbiAgICAvLyB2MC4xMiBUT0RPKGlzYWFjcyk6IFRoaXMgaXMgbm90IHF1aXRlIGhvdyBDaHJvbWUgZG9lcyB0aGluZ3MuXG4gICAgLy8gUmV2aWV3IG91ciB0ZXN0IGNhc2UgYWdhaW5zdCBicm93c2VycyBtb3JlIGNvbXByZWhlbnNpdmVseS5cblxuICAgIC8vIGZpbmQgdGhlIGZpcnN0IGluc3RhbmNlIG9mIGFueSBob3N0RW5kaW5nQ2hhcnNcbiAgICB2YXIgaG9zdEVuZCA9IC0xO1xuICAgIGZvciAoaSA9IDA7IGkgPCBob3N0RW5kaW5nQ2hhcnMubGVuZ3RoOyBpKyspIHtcbiAgICAgIGhlYyA9IHJlc3QuaW5kZXhPZihob3N0RW5kaW5nQ2hhcnNbaV0pO1xuICAgICAgaWYgKGhlYyAhPT0gLTEgJiYgKGhvc3RFbmQgPT09IC0xIHx8IGhlYyA8IGhvc3RFbmQpKSB7XG4gICAgICAgIGhvc3RFbmQgPSBoZWM7XG4gICAgICB9XG4gICAgfVxuXG4gICAgLy8gYXQgdGhpcyBwb2ludCwgZWl0aGVyIHdlIGhhdmUgYW4gZXhwbGljaXQgcG9pbnQgd2hlcmUgdGhlXG4gICAgLy8gYXV0aCBwb3J0aW9uIGNhbm5vdCBnbyBwYXN0LCBvciB0aGUgbGFzdCBAIGNoYXIgaXMgdGhlIGRlY2lkZXIuXG4gICAgdmFyIGF1dGgsIGF0U2lnbjtcbiAgICBpZiAoaG9zdEVuZCA9PT0gLTEpIHtcbiAgICAgIC8vIGF0U2lnbiBjYW4gYmUgYW55d2hlcmUuXG4gICAgICBhdFNpZ24gPSByZXN0Lmxhc3RJbmRleE9mKCdAJyk7XG4gICAgfSBlbHNlIHtcbiAgICAgIC8vIGF0U2lnbiBtdXN0IGJlIGluIGF1dGggcG9ydGlvbi5cbiAgICAgIC8vIGh0dHA6Ly9hQGIvY0BkID0+IGhvc3Q6YiBhdXRoOmEgcGF0aDovY0BkXG4gICAgICBhdFNpZ24gPSByZXN0Lmxhc3RJbmRleE9mKCdAJywgaG9zdEVuZCk7XG4gICAgfVxuXG4gICAgLy8gTm93IHdlIGhhdmUgYSBwb3J0aW9uIHdoaWNoIGlzIGRlZmluaXRlbHkgdGhlIGF1dGguXG4gICAgLy8gUHVsbCB0aGF0IG9mZi5cbiAgICBpZiAoYXRTaWduICE9PSAtMSkge1xuICAgICAgYXV0aCA9IHJlc3Quc2xpY2UoMCwgYXRTaWduKTtcbiAgICAgIHJlc3QgPSByZXN0LnNsaWNlKGF0U2lnbiArIDEpO1xuICAgICAgdGhpcy5hdXRoID0gYXV0aDtcbiAgICB9XG5cbiAgICAvLyB0aGUgaG9zdCBpcyB0aGUgcmVtYWluaW5nIHRvIHRoZSBsZWZ0IG9mIHRoZSBmaXJzdCBub24taG9zdCBjaGFyXG4gICAgaG9zdEVuZCA9IC0xO1xuICAgIGZvciAoaSA9IDA7IGkgPCBub25Ib3N0Q2hhcnMubGVuZ3RoOyBpKyspIHtcbiAgICAgIGhlYyA9IHJlc3QuaW5kZXhPZihub25Ib3N0Q2hhcnNbaV0pO1xuICAgICAgaWYgKGhlYyAhPT0gLTEgJiYgKGhvc3RFbmQgPT09IC0xIHx8IGhlYyA8IGhvc3RFbmQpKSB7XG4gICAgICAgIGhvc3RFbmQgPSBoZWM7XG4gICAgICB9XG4gICAgfVxuICAgIC8vIGlmIHdlIHN0aWxsIGhhdmUgbm90IGhpdCBpdCwgdGhlbiB0aGUgZW50aXJlIHRoaW5nIGlzIGEgaG9zdC5cbiAgICBpZiAoaG9zdEVuZCA9PT0gLTEpIHtcbiAgICAgIGhvc3RFbmQgPSByZXN0Lmxlbmd0aDtcbiAgICB9XG5cbiAgICBpZiAocmVzdFtob3N0RW5kIC0gMV0gPT09ICc6JykgeyBob3N0RW5kLS07IH1cbiAgICB2YXIgaG9zdCA9IHJlc3Quc2xpY2UoMCwgaG9zdEVuZCk7XG4gICAgcmVzdCA9IHJlc3Quc2xpY2UoaG9zdEVuZCk7XG5cbiAgICAvLyBwdWxsIG91dCBwb3J0LlxuICAgIHRoaXMucGFyc2VIb3N0KGhvc3QpO1xuXG4gICAgLy8gd2UndmUgaW5kaWNhdGVkIHRoYXQgdGhlcmUgaXMgYSBob3N0bmFtZSxcbiAgICAvLyBzbyBldmVuIGlmIGl0J3MgZW1wdHksIGl0IGhhcyB0byBiZSBwcmVzZW50LlxuICAgIHRoaXMuaG9zdG5hbWUgPSB0aGlzLmhvc3RuYW1lIHx8ICcnO1xuXG4gICAgLy8gaWYgaG9zdG5hbWUgYmVnaW5zIHdpdGggWyBhbmQgZW5kcyB3aXRoIF1cbiAgICAvLyBhc3N1bWUgdGhhdCBpdCdzIGFuIElQdjYgYWRkcmVzcy5cbiAgICB2YXIgaXB2Nkhvc3RuYW1lID0gdGhpcy5ob3N0bmFtZVswXSA9PT0gJ1snICYmXG4gICAgICAgIHRoaXMuaG9zdG5hbWVbdGhpcy5ob3N0bmFtZS5sZW5ndGggLSAxXSA9PT0gJ10nO1xuXG4gICAgLy8gdmFsaWRhdGUgYSBsaXR0bGUuXG4gICAgaWYgKCFpcHY2SG9zdG5hbWUpIHtcbiAgICAgIHZhciBob3N0cGFydHMgPSB0aGlzLmhvc3RuYW1lLnNwbGl0KC9cXC4vKTtcbiAgICAgIGZvciAoaSA9IDAsIGwgPSBob3N0cGFydHMubGVuZ3RoOyBpIDwgbDsgaSsrKSB7XG4gICAgICAgIHZhciBwYXJ0ID0gaG9zdHBhcnRzW2ldO1xuICAgICAgICBpZiAoIXBhcnQpIHsgY29udGludWU7IH1cbiAgICAgICAgaWYgKCFwYXJ0Lm1hdGNoKGhvc3RuYW1lUGFydFBhdHRlcm4pKSB7XG4gICAgICAgICAgdmFyIG5ld3BhcnQgPSAnJztcbiAgICAgICAgICBmb3IgKHZhciBqID0gMCwgayA9IHBhcnQubGVuZ3RoOyBqIDwgazsgaisrKSB7XG4gICAgICAgICAgICBpZiAocGFydC5jaGFyQ29kZUF0KGopID4gMTI3KSB7XG4gICAgICAgICAgICAgIC8vIHdlIHJlcGxhY2Ugbm9uLUFTQ0lJIGNoYXIgd2l0aCBhIHRlbXBvcmFyeSBwbGFjZWhvbGRlclxuICAgICAgICAgICAgICAvLyB3ZSBuZWVkIHRoaXMgdG8gbWFrZSBzdXJlIHNpemUgb2YgaG9zdG5hbWUgaXMgbm90XG4gICAgICAgICAgICAgIC8vIGJyb2tlbiBieSByZXBsYWNpbmcgbm9uLUFTQ0lJIGJ5IG5vdGhpbmdcbiAgICAgICAgICAgICAgbmV3cGFydCArPSAneCc7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICBuZXdwYXJ0ICs9IHBhcnRbal07XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICAgIC8vIHdlIHRlc3QgYWdhaW4gd2l0aCBBU0NJSSBjaGFyIG9ubHlcbiAgICAgICAgICBpZiAoIW5ld3BhcnQubWF0Y2goaG9zdG5hbWVQYXJ0UGF0dGVybikpIHtcbiAgICAgICAgICAgIHZhciB2YWxpZFBhcnRzID0gaG9zdHBhcnRzLnNsaWNlKDAsIGkpO1xuICAgICAgICAgICAgdmFyIG5vdEhvc3QgPSBob3N0cGFydHMuc2xpY2UoaSArIDEpO1xuICAgICAgICAgICAgdmFyIGJpdCA9IHBhcnQubWF0Y2goaG9zdG5hbWVQYXJ0U3RhcnQpO1xuICAgICAgICAgICAgaWYgKGJpdCkge1xuICAgICAgICAgICAgICB2YWxpZFBhcnRzLnB1c2goYml0WzFdKTtcbiAgICAgICAgICAgICAgbm90SG9zdC51bnNoaWZ0KGJpdFsyXSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZiAobm90SG9zdC5sZW5ndGgpIHtcbiAgICAgICAgICAgICAgcmVzdCA9IG5vdEhvc3Quam9pbignLicpICsgcmVzdDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHRoaXMuaG9zdG5hbWUgPSB2YWxpZFBhcnRzLmpvaW4oJy4nKTtcbiAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cblxuICAgIGlmICh0aGlzLmhvc3RuYW1lLmxlbmd0aCA+IGhvc3RuYW1lTWF4TGVuKSB7XG4gICAgICB0aGlzLmhvc3RuYW1lID0gJyc7XG4gICAgfVxuXG4gICAgLy8gc3RyaXAgWyBhbmQgXSBmcm9tIHRoZSBob3N0bmFtZVxuICAgIC8vIHRoZSBob3N0IGZpZWxkIHN0aWxsIHJldGFpbnMgdGhlbSwgdGhvdWdoXG4gICAgaWYgKGlwdjZIb3N0bmFtZSkge1xuICAgICAgdGhpcy5ob3N0bmFtZSA9IHRoaXMuaG9zdG5hbWUuc3Vic3RyKDEsIHRoaXMuaG9zdG5hbWUubGVuZ3RoIC0gMik7XG4gICAgfVxuICB9XG5cbiAgLy8gY2hvcCBvZmYgZnJvbSB0aGUgdGFpbCBmaXJzdC5cbiAgdmFyIGhhc2ggPSByZXN0LmluZGV4T2YoJyMnKTtcbiAgaWYgKGhhc2ggIT09IC0xKSB7XG4gICAgLy8gZ290IGEgZnJhZ21lbnQgc3RyaW5nLlxuICAgIHRoaXMuaGFzaCA9IHJlc3Quc3Vic3RyKGhhc2gpO1xuICAgIHJlc3QgPSByZXN0LnNsaWNlKDAsIGhhc2gpO1xuICB9XG4gIHZhciBxbSA9IHJlc3QuaW5kZXhPZignPycpO1xuICBpZiAocW0gIT09IC0xKSB7XG4gICAgdGhpcy5zZWFyY2ggPSByZXN0LnN1YnN0cihxbSk7XG4gICAgcmVzdCA9IHJlc3Quc2xpY2UoMCwgcW0pO1xuICB9XG4gIGlmIChyZXN0KSB7IHRoaXMucGF0aG5hbWUgPSByZXN0OyB9XG4gIGlmIChzbGFzaGVkUHJvdG9jb2xbbG93ZXJQcm90b10gJiZcbiAgICAgIHRoaXMuaG9zdG5hbWUgJiYgIXRoaXMucGF0aG5hbWUpIHtcbiAgICB0aGlzLnBhdGhuYW1lID0gJyc7XG4gIH1cblxuICByZXR1cm4gdGhpcztcbn07XG5cblVybC5wcm90b3R5cGUucGFyc2VIb3N0ID0gZnVuY3Rpb24oaG9zdCkge1xuICB2YXIgcG9ydCA9IHBvcnRQYXR0ZXJuLmV4ZWMoaG9zdCk7XG4gIGlmIChwb3J0KSB7XG4gICAgcG9ydCA9IHBvcnRbMF07XG4gICAgaWYgKHBvcnQgIT09ICc6Jykge1xuICAgICAgdGhpcy5wb3J0ID0gcG9ydC5zdWJzdHIoMSk7XG4gICAgfVxuICAgIGhvc3QgPSBob3N0LnN1YnN0cigwLCBob3N0Lmxlbmd0aCAtIHBvcnQubGVuZ3RoKTtcbiAgfVxuICBpZiAoaG9zdCkgeyB0aGlzLmhvc3RuYW1lID0gaG9zdDsgfVxufTtcblxubW9kdWxlLmV4cG9ydHMgPSB1cmxQYXJzZTtcbiIsIi8qISBodHRwczovL210aHMuYmUvcHVueWNvZGUgdjEuNC4xIGJ5IEBtYXRoaWFzICovXG47KGZ1bmN0aW9uKHJvb3QpIHtcblxuXHQvKiogRGV0ZWN0IGZyZWUgdmFyaWFibGVzICovXG5cdHZhciBmcmVlRXhwb3J0cyA9IHR5cGVvZiBleHBvcnRzID09ICdvYmplY3QnICYmIGV4cG9ydHMgJiZcblx0XHQhZXhwb3J0cy5ub2RlVHlwZSAmJiBleHBvcnRzO1xuXHR2YXIgZnJlZU1vZHVsZSA9IHR5cGVvZiBtb2R1bGUgPT0gJ29iamVjdCcgJiYgbW9kdWxlICYmXG5cdFx0IW1vZHVsZS5ub2RlVHlwZSAmJiBtb2R1bGU7XG5cdHZhciBmcmVlR2xvYmFsID0gdHlwZW9mIGdsb2JhbCA9PSAnb2JqZWN0JyAmJiBnbG9iYWw7XG5cdGlmIChcblx0XHRmcmVlR2xvYmFsLmdsb2JhbCA9PT0gZnJlZUdsb2JhbCB8fFxuXHRcdGZyZWVHbG9iYWwud2luZG93ID09PSBmcmVlR2xvYmFsIHx8XG5cdFx0ZnJlZUdsb2JhbC5zZWxmID09PSBmcmVlR2xvYmFsXG5cdCkge1xuXHRcdHJvb3QgPSBmcmVlR2xvYmFsO1xuXHR9XG5cblx0LyoqXG5cdCAqIFRoZSBgcHVueWNvZGVgIG9iamVjdC5cblx0ICogQG5hbWUgcHVueWNvZGVcblx0ICogQHR5cGUgT2JqZWN0XG5cdCAqL1xuXHR2YXIgcHVueWNvZGUsXG5cblx0LyoqIEhpZ2hlc3QgcG9zaXRpdmUgc2lnbmVkIDMyLWJpdCBmbG9hdCB2YWx1ZSAqL1xuXHRtYXhJbnQgPSAyMTQ3NDgzNjQ3LCAvLyBha2EuIDB4N0ZGRkZGRkYgb3IgMl4zMS0xXG5cblx0LyoqIEJvb3RzdHJpbmcgcGFyYW1ldGVycyAqL1xuXHRiYXNlID0gMzYsXG5cdHRNaW4gPSAxLFxuXHR0TWF4ID0gMjYsXG5cdHNrZXcgPSAzOCxcblx0ZGFtcCA9IDcwMCxcblx0aW5pdGlhbEJpYXMgPSA3Mixcblx0aW5pdGlhbE4gPSAxMjgsIC8vIDB4ODBcblx0ZGVsaW1pdGVyID0gJy0nLCAvLyAnXFx4MkQnXG5cblx0LyoqIFJlZ3VsYXIgZXhwcmVzc2lvbnMgKi9cblx0cmVnZXhQdW55Y29kZSA9IC9eeG4tLS8sXG5cdHJlZ2V4Tm9uQVNDSUkgPSAvW15cXHgyMC1cXHg3RV0vLCAvLyB1bnByaW50YWJsZSBBU0NJSSBjaGFycyArIG5vbi1BU0NJSSBjaGFyc1xuXHRyZWdleFNlcGFyYXRvcnMgPSAvW1xceDJFXFx1MzAwMlxcdUZGMEVcXHVGRjYxXS9nLCAvLyBSRkMgMzQ5MCBzZXBhcmF0b3JzXG5cblx0LyoqIEVycm9yIG1lc3NhZ2VzICovXG5cdGVycm9ycyA9IHtcblx0XHQnb3ZlcmZsb3cnOiAnT3ZlcmZsb3c6IGlucHV0IG5lZWRzIHdpZGVyIGludGVnZXJzIHRvIHByb2Nlc3MnLFxuXHRcdCdub3QtYmFzaWMnOiAnSWxsZWdhbCBpbnB1dCA+PSAweDgwIChub3QgYSBiYXNpYyBjb2RlIHBvaW50KScsXG5cdFx0J2ludmFsaWQtaW5wdXQnOiAnSW52YWxpZCBpbnB1dCdcblx0fSxcblxuXHQvKiogQ29udmVuaWVuY2Ugc2hvcnRjdXRzICovXG5cdGJhc2VNaW51c1RNaW4gPSBiYXNlIC0gdE1pbixcblx0Zmxvb3IgPSBNYXRoLmZsb29yLFxuXHRzdHJpbmdGcm9tQ2hhckNvZGUgPSBTdHJpbmcuZnJvbUNoYXJDb2RlLFxuXG5cdC8qKiBUZW1wb3JhcnkgdmFyaWFibGUgKi9cblx0a2V5O1xuXG5cdC8qLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0qL1xuXG5cdC8qKlxuXHQgKiBBIGdlbmVyaWMgZXJyb3IgdXRpbGl0eSBmdW5jdGlvbi5cblx0ICogQHByaXZhdGVcblx0ICogQHBhcmFtIHtTdHJpbmd9IHR5cGUgVGhlIGVycm9yIHR5cGUuXG5cdCAqIEByZXR1cm5zIHtFcnJvcn0gVGhyb3dzIGEgYFJhbmdlRXJyb3JgIHdpdGggdGhlIGFwcGxpY2FibGUgZXJyb3IgbWVzc2FnZS5cblx0ICovXG5cdGZ1bmN0aW9uIGVycm9yKHR5cGUpIHtcblx0XHR0aHJvdyBuZXcgUmFuZ2VFcnJvcihlcnJvcnNbdHlwZV0pO1xuXHR9XG5cblx0LyoqXG5cdCAqIEEgZ2VuZXJpYyBgQXJyYXkjbWFwYCB1dGlsaXR5IGZ1bmN0aW9uLlxuXHQgKiBAcHJpdmF0ZVxuXHQgKiBAcGFyYW0ge0FycmF5fSBhcnJheSBUaGUgYXJyYXkgdG8gaXRlcmF0ZSBvdmVyLlxuXHQgKiBAcGFyYW0ge0Z1bmN0aW9ufSBjYWxsYmFjayBUaGUgZnVuY3Rpb24gdGhhdCBnZXRzIGNhbGxlZCBmb3IgZXZlcnkgYXJyYXlcblx0ICogaXRlbS5cblx0ICogQHJldHVybnMge0FycmF5fSBBIG5ldyBhcnJheSBvZiB2YWx1ZXMgcmV0dXJuZWQgYnkgdGhlIGNhbGxiYWNrIGZ1bmN0aW9uLlxuXHQgKi9cblx0ZnVuY3Rpb24gbWFwKGFycmF5LCBmbikge1xuXHRcdHZhciBsZW5ndGggPSBhcnJheS5sZW5ndGg7XG5cdFx0dmFyIHJlc3VsdCA9IFtdO1xuXHRcdHdoaWxlIChsZW5ndGgtLSkge1xuXHRcdFx0cmVzdWx0W2xlbmd0aF0gPSBmbihhcnJheVtsZW5ndGhdKTtcblx0XHR9XG5cdFx0cmV0dXJuIHJlc3VsdDtcblx0fVxuXG5cdC8qKlxuXHQgKiBBIHNpbXBsZSBgQXJyYXkjbWFwYC1saWtlIHdyYXBwZXIgdG8gd29yayB3aXRoIGRvbWFpbiBuYW1lIHN0cmluZ3Mgb3IgZW1haWxcblx0ICogYWRkcmVzc2VzLlxuXHQgKiBAcHJpdmF0ZVxuXHQgKiBAcGFyYW0ge1N0cmluZ30gZG9tYWluIFRoZSBkb21haW4gbmFtZSBvciBlbWFpbCBhZGRyZXNzLlxuXHQgKiBAcGFyYW0ge0Z1bmN0aW9ufSBjYWxsYmFjayBUaGUgZnVuY3Rpb24gdGhhdCBnZXRzIGNhbGxlZCBmb3IgZXZlcnlcblx0ICogY2hhcmFjdGVyLlxuXHQgKiBAcmV0dXJucyB7QXJyYXl9IEEgbmV3IHN0cmluZyBvZiBjaGFyYWN0ZXJzIHJldHVybmVkIGJ5IHRoZSBjYWxsYmFja1xuXHQgKiBmdW5jdGlvbi5cblx0ICovXG5cdGZ1bmN0aW9uIG1hcERvbWFpbihzdHJpbmcsIGZuKSB7XG5cdFx0dmFyIHBhcnRzID0gc3RyaW5nLnNwbGl0KCdAJyk7XG5cdFx0dmFyIHJlc3VsdCA9ICcnO1xuXHRcdGlmIChwYXJ0cy5sZW5ndGggPiAxKSB7XG5cdFx0XHQvLyBJbiBlbWFpbCBhZGRyZXNzZXMsIG9ubHkgdGhlIGRvbWFpbiBuYW1lIHNob3VsZCBiZSBwdW55Y29kZWQuIExlYXZlXG5cdFx0XHQvLyB0aGUgbG9jYWwgcGFydCAoaS5lLiBldmVyeXRoaW5nIHVwIHRvIGBAYCkgaW50YWN0LlxuXHRcdFx0cmVzdWx0ID0gcGFydHNbMF0gKyAnQCc7XG5cdFx0XHRzdHJpbmcgPSBwYXJ0c1sxXTtcblx0XHR9XG5cdFx0Ly8gQXZvaWQgYHNwbGl0KHJlZ2V4KWAgZm9yIElFOCBjb21wYXRpYmlsaXR5LiBTZWUgIzE3LlxuXHRcdHN0cmluZyA9IHN0cmluZy5yZXBsYWNlKHJlZ2V4U2VwYXJhdG9ycywgJ1xceDJFJyk7XG5cdFx0dmFyIGxhYmVscyA9IHN0cmluZy5zcGxpdCgnLicpO1xuXHRcdHZhciBlbmNvZGVkID0gbWFwKGxhYmVscywgZm4pLmpvaW4oJy4nKTtcblx0XHRyZXR1cm4gcmVzdWx0ICsgZW5jb2RlZDtcblx0fVxuXG5cdC8qKlxuXHQgKiBDcmVhdGVzIGFuIGFycmF5IGNvbnRhaW5pbmcgdGhlIG51bWVyaWMgY29kZSBwb2ludHMgb2YgZWFjaCBVbmljb2RlXG5cdCAqIGNoYXJhY3RlciBpbiB0aGUgc3RyaW5nLiBXaGlsZSBKYXZhU2NyaXB0IHVzZXMgVUNTLTIgaW50ZXJuYWxseSxcblx0ICogdGhpcyBmdW5jdGlvbiB3aWxsIGNvbnZlcnQgYSBwYWlyIG9mIHN1cnJvZ2F0ZSBoYWx2ZXMgKGVhY2ggb2Ygd2hpY2hcblx0ICogVUNTLTIgZXhwb3NlcyBhcyBzZXBhcmF0ZSBjaGFyYWN0ZXJzKSBpbnRvIGEgc2luZ2xlIGNvZGUgcG9pbnQsXG5cdCAqIG1hdGNoaW5nIFVURi0xNi5cblx0ICogQHNlZSBgcHVueWNvZGUudWNzMi5lbmNvZGVgXG5cdCAqIEBzZWUgPGh0dHBzOi8vbWF0aGlhc2J5bmVucy5iZS9ub3Rlcy9qYXZhc2NyaXB0LWVuY29kaW5nPlxuXHQgKiBAbWVtYmVyT2YgcHVueWNvZGUudWNzMlxuXHQgKiBAbmFtZSBkZWNvZGVcblx0ICogQHBhcmFtIHtTdHJpbmd9IHN0cmluZyBUaGUgVW5pY29kZSBpbnB1dCBzdHJpbmcgKFVDUy0yKS5cblx0ICogQHJldHVybnMge0FycmF5fSBUaGUgbmV3IGFycmF5IG9mIGNvZGUgcG9pbnRzLlxuXHQgKi9cblx0ZnVuY3Rpb24gdWNzMmRlY29kZShzdHJpbmcpIHtcblx0XHR2YXIgb3V0cHV0ID0gW10sXG5cdFx0ICAgIGNvdW50ZXIgPSAwLFxuXHRcdCAgICBsZW5ndGggPSBzdHJpbmcubGVuZ3RoLFxuXHRcdCAgICB2YWx1ZSxcblx0XHQgICAgZXh0cmE7XG5cdFx0d2hpbGUgKGNvdW50ZXIgPCBsZW5ndGgpIHtcblx0XHRcdHZhbHVlID0gc3RyaW5nLmNoYXJDb2RlQXQoY291bnRlcisrKTtcblx0XHRcdGlmICh2YWx1ZSA+PSAweEQ4MDAgJiYgdmFsdWUgPD0gMHhEQkZGICYmIGNvdW50ZXIgPCBsZW5ndGgpIHtcblx0XHRcdFx0Ly8gaGlnaCBzdXJyb2dhdGUsIGFuZCB0aGVyZSBpcyBhIG5leHQgY2hhcmFjdGVyXG5cdFx0XHRcdGV4dHJhID0gc3RyaW5nLmNoYXJDb2RlQXQoY291bnRlcisrKTtcblx0XHRcdFx0aWYgKChleHRyYSAmIDB4RkMwMCkgPT0gMHhEQzAwKSB7IC8vIGxvdyBzdXJyb2dhdGVcblx0XHRcdFx0XHRvdXRwdXQucHVzaCgoKHZhbHVlICYgMHgzRkYpIDw8IDEwKSArIChleHRyYSAmIDB4M0ZGKSArIDB4MTAwMDApO1xuXHRcdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRcdC8vIHVubWF0Y2hlZCBzdXJyb2dhdGU7IG9ubHkgYXBwZW5kIHRoaXMgY29kZSB1bml0LCBpbiBjYXNlIHRoZSBuZXh0XG5cdFx0XHRcdFx0Ly8gY29kZSB1bml0IGlzIHRoZSBoaWdoIHN1cnJvZ2F0ZSBvZiBhIHN1cnJvZ2F0ZSBwYWlyXG5cdFx0XHRcdFx0b3V0cHV0LnB1c2godmFsdWUpO1xuXHRcdFx0XHRcdGNvdW50ZXItLTtcblx0XHRcdFx0fVxuXHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0b3V0cHV0LnB1c2godmFsdWUpO1xuXHRcdFx0fVxuXHRcdH1cblx0XHRyZXR1cm4gb3V0cHV0O1xuXHR9XG5cblx0LyoqXG5cdCAqIENyZWF0ZXMgYSBzdHJpbmcgYmFzZWQgb24gYW4gYXJyYXkgb2YgbnVtZXJpYyBjb2RlIHBvaW50cy5cblx0ICogQHNlZSBgcHVueWNvZGUudWNzMi5kZWNvZGVgXG5cdCAqIEBtZW1iZXJPZiBwdW55Y29kZS51Y3MyXG5cdCAqIEBuYW1lIGVuY29kZVxuXHQgKiBAcGFyYW0ge0FycmF5fSBjb2RlUG9pbnRzIFRoZSBhcnJheSBvZiBudW1lcmljIGNvZGUgcG9pbnRzLlxuXHQgKiBAcmV0dXJucyB7U3RyaW5nfSBUaGUgbmV3IFVuaWNvZGUgc3RyaW5nIChVQ1MtMikuXG5cdCAqL1xuXHRmdW5jdGlvbiB1Y3MyZW5jb2RlKGFycmF5KSB7XG5cdFx0cmV0dXJuIG1hcChhcnJheSwgZnVuY3Rpb24odmFsdWUpIHtcblx0XHRcdHZhciBvdXRwdXQgPSAnJztcblx0XHRcdGlmICh2YWx1ZSA+IDB4RkZGRikge1xuXHRcdFx0XHR2YWx1ZSAtPSAweDEwMDAwO1xuXHRcdFx0XHRvdXRwdXQgKz0gc3RyaW5nRnJvbUNoYXJDb2RlKHZhbHVlID4+PiAxMCAmIDB4M0ZGIHwgMHhEODAwKTtcblx0XHRcdFx0dmFsdWUgPSAweERDMDAgfCB2YWx1ZSAmIDB4M0ZGO1xuXHRcdFx0fVxuXHRcdFx0b3V0cHV0ICs9IHN0cmluZ0Zyb21DaGFyQ29kZSh2YWx1ZSk7XG5cdFx0XHRyZXR1cm4gb3V0cHV0O1xuXHRcdH0pLmpvaW4oJycpO1xuXHR9XG5cblx0LyoqXG5cdCAqIENvbnZlcnRzIGEgYmFzaWMgY29kZSBwb2ludCBpbnRvIGEgZGlnaXQvaW50ZWdlci5cblx0ICogQHNlZSBgZGlnaXRUb0Jhc2ljKClgXG5cdCAqIEBwcml2YXRlXG5cdCAqIEBwYXJhbSB7TnVtYmVyfSBjb2RlUG9pbnQgVGhlIGJhc2ljIG51bWVyaWMgY29kZSBwb2ludCB2YWx1ZS5cblx0ICogQHJldHVybnMge051bWJlcn0gVGhlIG51bWVyaWMgdmFsdWUgb2YgYSBiYXNpYyBjb2RlIHBvaW50IChmb3IgdXNlIGluXG5cdCAqIHJlcHJlc2VudGluZyBpbnRlZ2VycykgaW4gdGhlIHJhbmdlIGAwYCB0byBgYmFzZSAtIDFgLCBvciBgYmFzZWAgaWZcblx0ICogdGhlIGNvZGUgcG9pbnQgZG9lcyBub3QgcmVwcmVzZW50IGEgdmFsdWUuXG5cdCAqL1xuXHRmdW5jdGlvbiBiYXNpY1RvRGlnaXQoY29kZVBvaW50KSB7XG5cdFx0aWYgKGNvZGVQb2ludCAtIDQ4IDwgMTApIHtcblx0XHRcdHJldHVybiBjb2RlUG9pbnQgLSAyMjtcblx0XHR9XG5cdFx0aWYgKGNvZGVQb2ludCAtIDY1IDwgMjYpIHtcblx0XHRcdHJldHVybiBjb2RlUG9pbnQgLSA2NTtcblx0XHR9XG5cdFx0aWYgKGNvZGVQb2ludCAtIDk3IDwgMjYpIHtcblx0XHRcdHJldHVybiBjb2RlUG9pbnQgLSA5Nztcblx0XHR9XG5cdFx0cmV0dXJuIGJhc2U7XG5cdH1cblxuXHQvKipcblx0ICogQ29udmVydHMgYSBkaWdpdC9pbnRlZ2VyIGludG8gYSBiYXNpYyBjb2RlIHBvaW50LlxuXHQgKiBAc2VlIGBiYXNpY1RvRGlnaXQoKWBcblx0ICogQHByaXZhdGVcblx0ICogQHBhcmFtIHtOdW1iZXJ9IGRpZ2l0IFRoZSBudW1lcmljIHZhbHVlIG9mIGEgYmFzaWMgY29kZSBwb2ludC5cblx0ICogQHJldHVybnMge051bWJlcn0gVGhlIGJhc2ljIGNvZGUgcG9pbnQgd2hvc2UgdmFsdWUgKHdoZW4gdXNlZCBmb3Jcblx0ICogcmVwcmVzZW50aW5nIGludGVnZXJzKSBpcyBgZGlnaXRgLCB3aGljaCBuZWVkcyB0byBiZSBpbiB0aGUgcmFuZ2Vcblx0ICogYDBgIHRvIGBiYXNlIC0gMWAuIElmIGBmbGFnYCBpcyBub24temVybywgdGhlIHVwcGVyY2FzZSBmb3JtIGlzXG5cdCAqIHVzZWQ7IGVsc2UsIHRoZSBsb3dlcmNhc2UgZm9ybSBpcyB1c2VkLiBUaGUgYmVoYXZpb3IgaXMgdW5kZWZpbmVkXG5cdCAqIGlmIGBmbGFnYCBpcyBub24temVybyBhbmQgYGRpZ2l0YCBoYXMgbm8gdXBwZXJjYXNlIGZvcm0uXG5cdCAqL1xuXHRmdW5jdGlvbiBkaWdpdFRvQmFzaWMoZGlnaXQsIGZsYWcpIHtcblx0XHQvLyAgMC4uMjUgbWFwIHRvIEFTQ0lJIGEuLnogb3IgQS4uWlxuXHRcdC8vIDI2Li4zNSBtYXAgdG8gQVNDSUkgMC4uOVxuXHRcdHJldHVybiBkaWdpdCArIDIyICsgNzUgKiAoZGlnaXQgPCAyNikgLSAoKGZsYWcgIT0gMCkgPDwgNSk7XG5cdH1cblxuXHQvKipcblx0ICogQmlhcyBhZGFwdGF0aW9uIGZ1bmN0aW9uIGFzIHBlciBzZWN0aW9uIDMuNCBvZiBSRkMgMzQ5Mi5cblx0ICogaHR0cHM6Ly90b29scy5pZXRmLm9yZy9odG1sL3JmYzM0OTIjc2VjdGlvbi0zLjRcblx0ICogQHByaXZhdGVcblx0ICovXG5cdGZ1bmN0aW9uIGFkYXB0KGRlbHRhLCBudW1Qb2ludHMsIGZpcnN0VGltZSkge1xuXHRcdHZhciBrID0gMDtcblx0XHRkZWx0YSA9IGZpcnN0VGltZSA/IGZsb29yKGRlbHRhIC8gZGFtcCkgOiBkZWx0YSA+PiAxO1xuXHRcdGRlbHRhICs9IGZsb29yKGRlbHRhIC8gbnVtUG9pbnRzKTtcblx0XHRmb3IgKC8qIG5vIGluaXRpYWxpemF0aW9uICovOyBkZWx0YSA+IGJhc2VNaW51c1RNaW4gKiB0TWF4ID4+IDE7IGsgKz0gYmFzZSkge1xuXHRcdFx0ZGVsdGEgPSBmbG9vcihkZWx0YSAvIGJhc2VNaW51c1RNaW4pO1xuXHRcdH1cblx0XHRyZXR1cm4gZmxvb3IoayArIChiYXNlTWludXNUTWluICsgMSkgKiBkZWx0YSAvIChkZWx0YSArIHNrZXcpKTtcblx0fVxuXG5cdC8qKlxuXHQgKiBDb252ZXJ0cyBhIFB1bnljb2RlIHN0cmluZyBvZiBBU0NJSS1vbmx5IHN5bWJvbHMgdG8gYSBzdHJpbmcgb2YgVW5pY29kZVxuXHQgKiBzeW1ib2xzLlxuXHQgKiBAbWVtYmVyT2YgcHVueWNvZGVcblx0ICogQHBhcmFtIHtTdHJpbmd9IGlucHV0IFRoZSBQdW55Y29kZSBzdHJpbmcgb2YgQVNDSUktb25seSBzeW1ib2xzLlxuXHQgKiBAcmV0dXJucyB7U3RyaW5nfSBUaGUgcmVzdWx0aW5nIHN0cmluZyBvZiBVbmljb2RlIHN5bWJvbHMuXG5cdCAqL1xuXHRmdW5jdGlvbiBkZWNvZGUoaW5wdXQpIHtcblx0XHQvLyBEb24ndCB1c2UgVUNTLTJcblx0XHR2YXIgb3V0cHV0ID0gW10sXG5cdFx0ICAgIGlucHV0TGVuZ3RoID0gaW5wdXQubGVuZ3RoLFxuXHRcdCAgICBvdXQsXG5cdFx0ICAgIGkgPSAwLFxuXHRcdCAgICBuID0gaW5pdGlhbE4sXG5cdFx0ICAgIGJpYXMgPSBpbml0aWFsQmlhcyxcblx0XHQgICAgYmFzaWMsXG5cdFx0ICAgIGosXG5cdFx0ICAgIGluZGV4LFxuXHRcdCAgICBvbGRpLFxuXHRcdCAgICB3LFxuXHRcdCAgICBrLFxuXHRcdCAgICBkaWdpdCxcblx0XHQgICAgdCxcblx0XHQgICAgLyoqIENhY2hlZCBjYWxjdWxhdGlvbiByZXN1bHRzICovXG5cdFx0ICAgIGJhc2VNaW51c1Q7XG5cblx0XHQvLyBIYW5kbGUgdGhlIGJhc2ljIGNvZGUgcG9pbnRzOiBsZXQgYGJhc2ljYCBiZSB0aGUgbnVtYmVyIG9mIGlucHV0IGNvZGVcblx0XHQvLyBwb2ludHMgYmVmb3JlIHRoZSBsYXN0IGRlbGltaXRlciwgb3IgYDBgIGlmIHRoZXJlIGlzIG5vbmUsIHRoZW4gY29weVxuXHRcdC8vIHRoZSBmaXJzdCBiYXNpYyBjb2RlIHBvaW50cyB0byB0aGUgb3V0cHV0LlxuXG5cdFx0YmFzaWMgPSBpbnB1dC5sYXN0SW5kZXhPZihkZWxpbWl0ZXIpO1xuXHRcdGlmIChiYXNpYyA8IDApIHtcblx0XHRcdGJhc2ljID0gMDtcblx0XHR9XG5cblx0XHRmb3IgKGogPSAwOyBqIDwgYmFzaWM7ICsraikge1xuXHRcdFx0Ly8gaWYgaXQncyBub3QgYSBiYXNpYyBjb2RlIHBvaW50XG5cdFx0XHRpZiAoaW5wdXQuY2hhckNvZGVBdChqKSA+PSAweDgwKSB7XG5cdFx0XHRcdGVycm9yKCdub3QtYmFzaWMnKTtcblx0XHRcdH1cblx0XHRcdG91dHB1dC5wdXNoKGlucHV0LmNoYXJDb2RlQXQoaikpO1xuXHRcdH1cblxuXHRcdC8vIE1haW4gZGVjb2RpbmcgbG9vcDogc3RhcnQganVzdCBhZnRlciB0aGUgbGFzdCBkZWxpbWl0ZXIgaWYgYW55IGJhc2ljIGNvZGVcblx0XHQvLyBwb2ludHMgd2VyZSBjb3BpZWQ7IHN0YXJ0IGF0IHRoZSBiZWdpbm5pbmcgb3RoZXJ3aXNlLlxuXG5cdFx0Zm9yIChpbmRleCA9IGJhc2ljID4gMCA/IGJhc2ljICsgMSA6IDA7IGluZGV4IDwgaW5wdXRMZW5ndGg7IC8qIG5vIGZpbmFsIGV4cHJlc3Npb24gKi8pIHtcblxuXHRcdFx0Ly8gYGluZGV4YCBpcyB0aGUgaW5kZXggb2YgdGhlIG5leHQgY2hhcmFjdGVyIHRvIGJlIGNvbnN1bWVkLlxuXHRcdFx0Ly8gRGVjb2RlIGEgZ2VuZXJhbGl6ZWQgdmFyaWFibGUtbGVuZ3RoIGludGVnZXIgaW50byBgZGVsdGFgLFxuXHRcdFx0Ly8gd2hpY2ggZ2V0cyBhZGRlZCB0byBgaWAuIFRoZSBvdmVyZmxvdyBjaGVja2luZyBpcyBlYXNpZXJcblx0XHRcdC8vIGlmIHdlIGluY3JlYXNlIGBpYCBhcyB3ZSBnbywgdGhlbiBzdWJ0cmFjdCBvZmYgaXRzIHN0YXJ0aW5nXG5cdFx0XHQvLyB2YWx1ZSBhdCB0aGUgZW5kIHRvIG9idGFpbiBgZGVsdGFgLlxuXHRcdFx0Zm9yIChvbGRpID0gaSwgdyA9IDEsIGsgPSBiYXNlOyAvKiBubyBjb25kaXRpb24gKi87IGsgKz0gYmFzZSkge1xuXG5cdFx0XHRcdGlmIChpbmRleCA+PSBpbnB1dExlbmd0aCkge1xuXHRcdFx0XHRcdGVycm9yKCdpbnZhbGlkLWlucHV0Jyk7XG5cdFx0XHRcdH1cblxuXHRcdFx0XHRkaWdpdCA9IGJhc2ljVG9EaWdpdChpbnB1dC5jaGFyQ29kZUF0KGluZGV4KyspKTtcblxuXHRcdFx0XHRpZiAoZGlnaXQgPj0gYmFzZSB8fCBkaWdpdCA+IGZsb29yKChtYXhJbnQgLSBpKSAvIHcpKSB7XG5cdFx0XHRcdFx0ZXJyb3IoJ292ZXJmbG93Jyk7XG5cdFx0XHRcdH1cblxuXHRcdFx0XHRpICs9IGRpZ2l0ICogdztcblx0XHRcdFx0dCA9IGsgPD0gYmlhcyA/IHRNaW4gOiAoayA+PSBiaWFzICsgdE1heCA/IHRNYXggOiBrIC0gYmlhcyk7XG5cblx0XHRcdFx0aWYgKGRpZ2l0IDwgdCkge1xuXHRcdFx0XHRcdGJyZWFrO1xuXHRcdFx0XHR9XG5cblx0XHRcdFx0YmFzZU1pbnVzVCA9IGJhc2UgLSB0O1xuXHRcdFx0XHRpZiAodyA+IGZsb29yKG1heEludCAvIGJhc2VNaW51c1QpKSB7XG5cdFx0XHRcdFx0ZXJyb3IoJ292ZXJmbG93Jyk7XG5cdFx0XHRcdH1cblxuXHRcdFx0XHR3ICo9IGJhc2VNaW51c1Q7XG5cblx0XHRcdH1cblxuXHRcdFx0b3V0ID0gb3V0cHV0Lmxlbmd0aCArIDE7XG5cdFx0XHRiaWFzID0gYWRhcHQoaSAtIG9sZGksIG91dCwgb2xkaSA9PSAwKTtcblxuXHRcdFx0Ly8gYGlgIHdhcyBzdXBwb3NlZCB0byB3cmFwIGFyb3VuZCBmcm9tIGBvdXRgIHRvIGAwYCxcblx0XHRcdC8vIGluY3JlbWVudGluZyBgbmAgZWFjaCB0aW1lLCBzbyB3ZSdsbCBmaXggdGhhdCBub3c6XG5cdFx0XHRpZiAoZmxvb3IoaSAvIG91dCkgPiBtYXhJbnQgLSBuKSB7XG5cdFx0XHRcdGVycm9yKCdvdmVyZmxvdycpO1xuXHRcdFx0fVxuXG5cdFx0XHRuICs9IGZsb29yKGkgLyBvdXQpO1xuXHRcdFx0aSAlPSBvdXQ7XG5cblx0XHRcdC8vIEluc2VydCBgbmAgYXQgcG9zaXRpb24gYGlgIG9mIHRoZSBvdXRwdXRcblx0XHRcdG91dHB1dC5zcGxpY2UoaSsrLCAwLCBuKTtcblxuXHRcdH1cblxuXHRcdHJldHVybiB1Y3MyZW5jb2RlKG91dHB1dCk7XG5cdH1cblxuXHQvKipcblx0ICogQ29udmVydHMgYSBzdHJpbmcgb2YgVW5pY29kZSBzeW1ib2xzIChlLmcuIGEgZG9tYWluIG5hbWUgbGFiZWwpIHRvIGFcblx0ICogUHVueWNvZGUgc3RyaW5nIG9mIEFTQ0lJLW9ubHkgc3ltYm9scy5cblx0ICogQG1lbWJlck9mIHB1bnljb2RlXG5cdCAqIEBwYXJhbSB7U3RyaW5nfSBpbnB1dCBUaGUgc3RyaW5nIG9mIFVuaWNvZGUgc3ltYm9scy5cblx0ICogQHJldHVybnMge1N0cmluZ30gVGhlIHJlc3VsdGluZyBQdW55Y29kZSBzdHJpbmcgb2YgQVNDSUktb25seSBzeW1ib2xzLlxuXHQgKi9cblx0ZnVuY3Rpb24gZW5jb2RlKGlucHV0KSB7XG5cdFx0dmFyIG4sXG5cdFx0ICAgIGRlbHRhLFxuXHRcdCAgICBoYW5kbGVkQ1BDb3VudCxcblx0XHQgICAgYmFzaWNMZW5ndGgsXG5cdFx0ICAgIGJpYXMsXG5cdFx0ICAgIGosXG5cdFx0ICAgIG0sXG5cdFx0ICAgIHEsXG5cdFx0ICAgIGssXG5cdFx0ICAgIHQsXG5cdFx0ICAgIGN1cnJlbnRWYWx1ZSxcblx0XHQgICAgb3V0cHV0ID0gW10sXG5cdFx0ICAgIC8qKiBgaW5wdXRMZW5ndGhgIHdpbGwgaG9sZCB0aGUgbnVtYmVyIG9mIGNvZGUgcG9pbnRzIGluIGBpbnB1dGAuICovXG5cdFx0ICAgIGlucHV0TGVuZ3RoLFxuXHRcdCAgICAvKiogQ2FjaGVkIGNhbGN1bGF0aW9uIHJlc3VsdHMgKi9cblx0XHQgICAgaGFuZGxlZENQQ291bnRQbHVzT25lLFxuXHRcdCAgICBiYXNlTWludXNULFxuXHRcdCAgICBxTWludXNUO1xuXG5cdFx0Ly8gQ29udmVydCB0aGUgaW5wdXQgaW4gVUNTLTIgdG8gVW5pY29kZVxuXHRcdGlucHV0ID0gdWNzMmRlY29kZShpbnB1dCk7XG5cblx0XHQvLyBDYWNoZSB0aGUgbGVuZ3RoXG5cdFx0aW5wdXRMZW5ndGggPSBpbnB1dC5sZW5ndGg7XG5cblx0XHQvLyBJbml0aWFsaXplIHRoZSBzdGF0ZVxuXHRcdG4gPSBpbml0aWFsTjtcblx0XHRkZWx0YSA9IDA7XG5cdFx0YmlhcyA9IGluaXRpYWxCaWFzO1xuXG5cdFx0Ly8gSGFuZGxlIHRoZSBiYXNpYyBjb2RlIHBvaW50c1xuXHRcdGZvciAoaiA9IDA7IGogPCBpbnB1dExlbmd0aDsgKytqKSB7XG5cdFx0XHRjdXJyZW50VmFsdWUgPSBpbnB1dFtqXTtcblx0XHRcdGlmIChjdXJyZW50VmFsdWUgPCAweDgwKSB7XG5cdFx0XHRcdG91dHB1dC5wdXNoKHN0cmluZ0Zyb21DaGFyQ29kZShjdXJyZW50VmFsdWUpKTtcblx0XHRcdH1cblx0XHR9XG5cblx0XHRoYW5kbGVkQ1BDb3VudCA9IGJhc2ljTGVuZ3RoID0gb3V0cHV0Lmxlbmd0aDtcblxuXHRcdC8vIGBoYW5kbGVkQ1BDb3VudGAgaXMgdGhlIG51bWJlciBvZiBjb2RlIHBvaW50cyB0aGF0IGhhdmUgYmVlbiBoYW5kbGVkO1xuXHRcdC8vIGBiYXNpY0xlbmd0aGAgaXMgdGhlIG51bWJlciBvZiBiYXNpYyBjb2RlIHBvaW50cy5cblxuXHRcdC8vIEZpbmlzaCB0aGUgYmFzaWMgc3RyaW5nIC0gaWYgaXQgaXMgbm90IGVtcHR5IC0gd2l0aCBhIGRlbGltaXRlclxuXHRcdGlmIChiYXNpY0xlbmd0aCkge1xuXHRcdFx0b3V0cHV0LnB1c2goZGVsaW1pdGVyKTtcblx0XHR9XG5cblx0XHQvLyBNYWluIGVuY29kaW5nIGxvb3A6XG5cdFx0d2hpbGUgKGhhbmRsZWRDUENvdW50IDwgaW5wdXRMZW5ndGgpIHtcblxuXHRcdFx0Ly8gQWxsIG5vbi1iYXNpYyBjb2RlIHBvaW50cyA8IG4gaGF2ZSBiZWVuIGhhbmRsZWQgYWxyZWFkeS4gRmluZCB0aGUgbmV4dFxuXHRcdFx0Ly8gbGFyZ2VyIG9uZTpcblx0XHRcdGZvciAobSA9IG1heEludCwgaiA9IDA7IGogPCBpbnB1dExlbmd0aDsgKytqKSB7XG5cdFx0XHRcdGN1cnJlbnRWYWx1ZSA9IGlucHV0W2pdO1xuXHRcdFx0XHRpZiAoY3VycmVudFZhbHVlID49IG4gJiYgY3VycmVudFZhbHVlIDwgbSkge1xuXHRcdFx0XHRcdG0gPSBjdXJyZW50VmFsdWU7XG5cdFx0XHRcdH1cblx0XHRcdH1cblxuXHRcdFx0Ly8gSW5jcmVhc2UgYGRlbHRhYCBlbm91Z2ggdG8gYWR2YW5jZSB0aGUgZGVjb2RlcidzIDxuLGk+IHN0YXRlIHRvIDxtLDA+LFxuXHRcdFx0Ly8gYnV0IGd1YXJkIGFnYWluc3Qgb3ZlcmZsb3dcblx0XHRcdGhhbmRsZWRDUENvdW50UGx1c09uZSA9IGhhbmRsZWRDUENvdW50ICsgMTtcblx0XHRcdGlmIChtIC0gbiA+IGZsb29yKChtYXhJbnQgLSBkZWx0YSkgLyBoYW5kbGVkQ1BDb3VudFBsdXNPbmUpKSB7XG5cdFx0XHRcdGVycm9yKCdvdmVyZmxvdycpO1xuXHRcdFx0fVxuXG5cdFx0XHRkZWx0YSArPSAobSAtIG4pICogaGFuZGxlZENQQ291bnRQbHVzT25lO1xuXHRcdFx0biA9IG07XG5cblx0XHRcdGZvciAoaiA9IDA7IGogPCBpbnB1dExlbmd0aDsgKytqKSB7XG5cdFx0XHRcdGN1cnJlbnRWYWx1ZSA9IGlucHV0W2pdO1xuXG5cdFx0XHRcdGlmIChjdXJyZW50VmFsdWUgPCBuICYmICsrZGVsdGEgPiBtYXhJbnQpIHtcblx0XHRcdFx0XHRlcnJvcignb3ZlcmZsb3cnKTtcblx0XHRcdFx0fVxuXG5cdFx0XHRcdGlmIChjdXJyZW50VmFsdWUgPT0gbikge1xuXHRcdFx0XHRcdC8vIFJlcHJlc2VudCBkZWx0YSBhcyBhIGdlbmVyYWxpemVkIHZhcmlhYmxlLWxlbmd0aCBpbnRlZ2VyXG5cdFx0XHRcdFx0Zm9yIChxID0gZGVsdGEsIGsgPSBiYXNlOyAvKiBubyBjb25kaXRpb24gKi87IGsgKz0gYmFzZSkge1xuXHRcdFx0XHRcdFx0dCA9IGsgPD0gYmlhcyA/IHRNaW4gOiAoayA+PSBiaWFzICsgdE1heCA/IHRNYXggOiBrIC0gYmlhcyk7XG5cdFx0XHRcdFx0XHRpZiAocSA8IHQpIHtcblx0XHRcdFx0XHRcdFx0YnJlYWs7XG5cdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0XHRxTWludXNUID0gcSAtIHQ7XG5cdFx0XHRcdFx0XHRiYXNlTWludXNUID0gYmFzZSAtIHQ7XG5cdFx0XHRcdFx0XHRvdXRwdXQucHVzaChcblx0XHRcdFx0XHRcdFx0c3RyaW5nRnJvbUNoYXJDb2RlKGRpZ2l0VG9CYXNpYyh0ICsgcU1pbnVzVCAlIGJhc2VNaW51c1QsIDApKVxuXHRcdFx0XHRcdFx0KTtcblx0XHRcdFx0XHRcdHEgPSBmbG9vcihxTWludXNUIC8gYmFzZU1pbnVzVCk7XG5cdFx0XHRcdFx0fVxuXG5cdFx0XHRcdFx0b3V0cHV0LnB1c2goc3RyaW5nRnJvbUNoYXJDb2RlKGRpZ2l0VG9CYXNpYyhxLCAwKSkpO1xuXHRcdFx0XHRcdGJpYXMgPSBhZGFwdChkZWx0YSwgaGFuZGxlZENQQ291bnRQbHVzT25lLCBoYW5kbGVkQ1BDb3VudCA9PSBiYXNpY0xlbmd0aCk7XG5cdFx0XHRcdFx0ZGVsdGEgPSAwO1xuXHRcdFx0XHRcdCsraGFuZGxlZENQQ291bnQ7XG5cdFx0XHRcdH1cblx0XHRcdH1cblxuXHRcdFx0KytkZWx0YTtcblx0XHRcdCsrbjtcblxuXHRcdH1cblx0XHRyZXR1cm4gb3V0cHV0LmpvaW4oJycpO1xuXHR9XG5cblx0LyoqXG5cdCAqIENvbnZlcnRzIGEgUHVueWNvZGUgc3RyaW5nIHJlcHJlc2VudGluZyBhIGRvbWFpbiBuYW1lIG9yIGFuIGVtYWlsIGFkZHJlc3Ncblx0ICogdG8gVW5pY29kZS4gT25seSB0aGUgUHVueWNvZGVkIHBhcnRzIG9mIHRoZSBpbnB1dCB3aWxsIGJlIGNvbnZlcnRlZCwgaS5lLlxuXHQgKiBpdCBkb2Vzbid0IG1hdHRlciBpZiB5b3UgY2FsbCBpdCBvbiBhIHN0cmluZyB0aGF0IGhhcyBhbHJlYWR5IGJlZW5cblx0ICogY29udmVydGVkIHRvIFVuaWNvZGUuXG5cdCAqIEBtZW1iZXJPZiBwdW55Y29kZVxuXHQgKiBAcGFyYW0ge1N0cmluZ30gaW5wdXQgVGhlIFB1bnljb2RlZCBkb21haW4gbmFtZSBvciBlbWFpbCBhZGRyZXNzIHRvXG5cdCAqIGNvbnZlcnQgdG8gVW5pY29kZS5cblx0ICogQHJldHVybnMge1N0cmluZ30gVGhlIFVuaWNvZGUgcmVwcmVzZW50YXRpb24gb2YgdGhlIGdpdmVuIFB1bnljb2RlXG5cdCAqIHN0cmluZy5cblx0ICovXG5cdGZ1bmN0aW9uIHRvVW5pY29kZShpbnB1dCkge1xuXHRcdHJldHVybiBtYXBEb21haW4oaW5wdXQsIGZ1bmN0aW9uKHN0cmluZykge1xuXHRcdFx0cmV0dXJuIHJlZ2V4UHVueWNvZGUudGVzdChzdHJpbmcpXG5cdFx0XHRcdD8gZGVjb2RlKHN0cmluZy5zbGljZSg0KS50b0xvd2VyQ2FzZSgpKVxuXHRcdFx0XHQ6IHN0cmluZztcblx0XHR9KTtcblx0fVxuXG5cdC8qKlxuXHQgKiBDb252ZXJ0cyBhIFVuaWNvZGUgc3RyaW5nIHJlcHJlc2VudGluZyBhIGRvbWFpbiBuYW1lIG9yIGFuIGVtYWlsIGFkZHJlc3MgdG9cblx0ICogUHVueWNvZGUuIE9ubHkgdGhlIG5vbi1BU0NJSSBwYXJ0cyBvZiB0aGUgZG9tYWluIG5hbWUgd2lsbCBiZSBjb252ZXJ0ZWQsXG5cdCAqIGkuZS4gaXQgZG9lc24ndCBtYXR0ZXIgaWYgeW91IGNhbGwgaXQgd2l0aCBhIGRvbWFpbiB0aGF0J3MgYWxyZWFkeSBpblxuXHQgKiBBU0NJSS5cblx0ICogQG1lbWJlck9mIHB1bnljb2RlXG5cdCAqIEBwYXJhbSB7U3RyaW5nfSBpbnB1dCBUaGUgZG9tYWluIG5hbWUgb3IgZW1haWwgYWRkcmVzcyB0byBjb252ZXJ0LCBhcyBhXG5cdCAqIFVuaWNvZGUgc3RyaW5nLlxuXHQgKiBAcmV0dXJucyB7U3RyaW5nfSBUaGUgUHVueWNvZGUgcmVwcmVzZW50YXRpb24gb2YgdGhlIGdpdmVuIGRvbWFpbiBuYW1lIG9yXG5cdCAqIGVtYWlsIGFkZHJlc3MuXG5cdCAqL1xuXHRmdW5jdGlvbiB0b0FTQ0lJKGlucHV0KSB7XG5cdFx0cmV0dXJuIG1hcERvbWFpbihpbnB1dCwgZnVuY3Rpb24oc3RyaW5nKSB7XG5cdFx0XHRyZXR1cm4gcmVnZXhOb25BU0NJSS50ZXN0KHN0cmluZylcblx0XHRcdFx0PyAneG4tLScgKyBlbmNvZGUoc3RyaW5nKVxuXHRcdFx0XHQ6IHN0cmluZztcblx0XHR9KTtcblx0fVxuXG5cdC8qLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0qL1xuXG5cdC8qKiBEZWZpbmUgdGhlIHB1YmxpYyBBUEkgKi9cblx0cHVueWNvZGUgPSB7XG5cdFx0LyoqXG5cdFx0ICogQSBzdHJpbmcgcmVwcmVzZW50aW5nIHRoZSBjdXJyZW50IFB1bnljb2RlLmpzIHZlcnNpb24gbnVtYmVyLlxuXHRcdCAqIEBtZW1iZXJPZiBwdW55Y29kZVxuXHRcdCAqIEB0eXBlIFN0cmluZ1xuXHRcdCAqL1xuXHRcdCd2ZXJzaW9uJzogJzEuNC4xJyxcblx0XHQvKipcblx0XHQgKiBBbiBvYmplY3Qgb2YgbWV0aG9kcyB0byBjb252ZXJ0IGZyb20gSmF2YVNjcmlwdCdzIGludGVybmFsIGNoYXJhY3RlclxuXHRcdCAqIHJlcHJlc2VudGF0aW9uIChVQ1MtMikgdG8gVW5pY29kZSBjb2RlIHBvaW50cywgYW5kIGJhY2suXG5cdFx0ICogQHNlZSA8aHR0cHM6Ly9tYXRoaWFzYnluZW5zLmJlL25vdGVzL2phdmFzY3JpcHQtZW5jb2Rpbmc+XG5cdFx0ICogQG1lbWJlck9mIHB1bnljb2RlXG5cdFx0ICogQHR5cGUgT2JqZWN0XG5cdFx0ICovXG5cdFx0J3VjczInOiB7XG5cdFx0XHQnZGVjb2RlJzogdWNzMmRlY29kZSxcblx0XHRcdCdlbmNvZGUnOiB1Y3MyZW5jb2RlXG5cdFx0fSxcblx0XHQnZGVjb2RlJzogZGVjb2RlLFxuXHRcdCdlbmNvZGUnOiBlbmNvZGUsXG5cdFx0J3RvQVNDSUknOiB0b0FTQ0lJLFxuXHRcdCd0b1VuaWNvZGUnOiB0b1VuaWNvZGVcblx0fTtcblxuXHQvKiogRXhwb3NlIGBwdW55Y29kZWAgKi9cblx0Ly8gU29tZSBBTUQgYnVpbGQgb3B0aW1pemVycywgbGlrZSByLmpzLCBjaGVjayBmb3Igc3BlY2lmaWMgY29uZGl0aW9uIHBhdHRlcm5zXG5cdC8vIGxpa2UgdGhlIGZvbGxvd2luZzpcblx0aWYgKFxuXHRcdHR5cGVvZiBkZWZpbmUgPT0gJ2Z1bmN0aW9uJyAmJlxuXHRcdHR5cGVvZiBkZWZpbmUuYW1kID09ICdvYmplY3QnICYmXG5cdFx0ZGVmaW5lLmFtZFxuXHQpIHtcblx0XHRkZWZpbmUoJ3B1bnljb2RlJywgZnVuY3Rpb24oKSB7XG5cdFx0XHRyZXR1cm4gcHVueWNvZGU7XG5cdFx0fSk7XG5cdH0gZWxzZSBpZiAoZnJlZUV4cG9ydHMgJiYgZnJlZU1vZHVsZSkge1xuXHRcdGlmIChtb2R1bGUuZXhwb3J0cyA9PSBmcmVlRXhwb3J0cykge1xuXHRcdFx0Ly8gaW4gTm9kZS5qcywgaW8uanMsIG9yIFJpbmdvSlMgdjAuOC4wK1xuXHRcdFx0ZnJlZU1vZHVsZS5leHBvcnRzID0gcHVueWNvZGU7XG5cdFx0fSBlbHNlIHtcblx0XHRcdC8vIGluIE5hcndoYWwgb3IgUmluZ29KUyB2MC43LjAtXG5cdFx0XHRmb3IgKGtleSBpbiBwdW55Y29kZSkge1xuXHRcdFx0XHRwdW55Y29kZS5oYXNPd25Qcm9wZXJ0eShrZXkpICYmIChmcmVlRXhwb3J0c1trZXldID0gcHVueWNvZGVba2V5XSk7XG5cdFx0XHR9XG5cdFx0fVxuXHR9IGVsc2Uge1xuXHRcdC8vIGluIFJoaW5vIG9yIGEgd2ViIGJyb3dzZXJcblx0XHRyb290LnB1bnljb2RlID0gcHVueWNvZGU7XG5cdH1cblxufSh0aGlzKSk7XG4iLCJtb2R1bGUuZXhwb3J0cz0vW1xcMC1cXHgxRlxceDdGLVxceDlGXS8iLCJtb2R1bGUuZXhwb3J0cz0vW1xceEFEXFx1MDYwMC1cXHUwNjA1XFx1MDYxQ1xcdTA2RERcXHUwNzBGXFx1MDhFMlxcdTE4MEVcXHUyMDBCLVxcdTIwMEZcXHUyMDJBLVxcdTIwMkVcXHUyMDYwLVxcdTIwNjRcXHUyMDY2LVxcdTIwNkZcXHVGRUZGXFx1RkZGOS1cXHVGRkZCXXxcXHVEODA0W1xcdURDQkRcXHVEQ0NEXXxcXHVEODJGW1xcdURDQTAtXFx1RENBM118XFx1RDgzNFtcXHVERDczLVxcdUREN0FdfFxcdURCNDBbXFx1REMwMVxcdURDMjAtXFx1REM3Rl0vIiwibW9kdWxlLmV4cG9ydHM9L1shLSMlLVxcKiwtXFwvOjtcXD9AXFxbLVxcXV9cXHtcXH1cXHhBMVxceEE3XFx4QUJcXHhCNlxceEI3XFx4QkJcXHhCRlxcdTAzN0VcXHUwMzg3XFx1MDU1QS1cXHUwNTVGXFx1MDU4OVxcdTA1OEFcXHUwNUJFXFx1MDVDMFxcdTA1QzNcXHUwNUM2XFx1MDVGM1xcdTA1RjRcXHUwNjA5XFx1MDYwQVxcdTA2MENcXHUwNjBEXFx1MDYxQlxcdTA2MUVcXHUwNjFGXFx1MDY2QS1cXHUwNjZEXFx1MDZENFxcdTA3MDAtXFx1MDcwRFxcdTA3RjctXFx1MDdGOVxcdTA4MzAtXFx1MDgzRVxcdTA4NUVcXHUwOTY0XFx1MDk2NVxcdTA5NzBcXHUwOUZEXFx1MEE3NlxcdTBBRjBcXHUwQzg0XFx1MERGNFxcdTBFNEZcXHUwRTVBXFx1MEU1QlxcdTBGMDQtXFx1MEYxMlxcdTBGMTRcXHUwRjNBLVxcdTBGM0RcXHUwRjg1XFx1MEZEMC1cXHUwRkQ0XFx1MEZEOVxcdTBGREFcXHUxMDRBLVxcdTEwNEZcXHUxMEZCXFx1MTM2MC1cXHUxMzY4XFx1MTQwMFxcdTE2NkRcXHUxNjZFXFx1MTY5QlxcdTE2OUNcXHUxNkVCLVxcdTE2RURcXHUxNzM1XFx1MTczNlxcdTE3RDQtXFx1MTdENlxcdTE3RDgtXFx1MTdEQVxcdTE4MDAtXFx1MTgwQVxcdTE5NDRcXHUxOTQ1XFx1MUExRVxcdTFBMUZcXHUxQUEwLVxcdTFBQTZcXHUxQUE4LVxcdTFBQURcXHUxQjVBLVxcdTFCNjBcXHUxQkZDLVxcdTFCRkZcXHUxQzNCLVxcdTFDM0ZcXHUxQzdFXFx1MUM3RlxcdTFDQzAtXFx1MUNDN1xcdTFDRDNcXHUyMDEwLVxcdTIwMjdcXHUyMDMwLVxcdTIwNDNcXHUyMDQ1LVxcdTIwNTFcXHUyMDUzLVxcdTIwNUVcXHUyMDdEXFx1MjA3RVxcdTIwOERcXHUyMDhFXFx1MjMwOC1cXHUyMzBCXFx1MjMyOVxcdTIzMkFcXHUyNzY4LVxcdTI3NzVcXHUyN0M1XFx1MjdDNlxcdTI3RTYtXFx1MjdFRlxcdTI5ODMtXFx1Mjk5OFxcdTI5RDgtXFx1MjlEQlxcdTI5RkNcXHUyOUZEXFx1MkNGOS1cXHUyQ0ZDXFx1MkNGRVxcdTJDRkZcXHUyRDcwXFx1MkUwMC1cXHUyRTJFXFx1MkUzMC1cXHUyRTRFXFx1MzAwMS1cXHUzMDAzXFx1MzAwOC1cXHUzMDExXFx1MzAxNC1cXHUzMDFGXFx1MzAzMFxcdTMwM0RcXHUzMEEwXFx1MzBGQlxcdUE0RkVcXHVBNEZGXFx1QTYwRC1cXHVBNjBGXFx1QTY3M1xcdUE2N0VcXHVBNkYyLVxcdUE2RjdcXHVBODc0LVxcdUE4NzdcXHVBOENFXFx1QThDRlxcdUE4RjgtXFx1QThGQVxcdUE4RkNcXHVBOTJFXFx1QTkyRlxcdUE5NUZcXHVBOUMxLVxcdUE5Q0RcXHVBOURFXFx1QTlERlxcdUFBNUMtXFx1QUE1RlxcdUFBREVcXHVBQURGXFx1QUFGMFxcdUFBRjFcXHVBQkVCXFx1RkQzRVxcdUZEM0ZcXHVGRTEwLVxcdUZFMTlcXHVGRTMwLVxcdUZFNTJcXHVGRTU0LVxcdUZFNjFcXHVGRTYzXFx1RkU2OFxcdUZFNkFcXHVGRTZCXFx1RkYwMS1cXHVGRjAzXFx1RkYwNS1cXHVGRjBBXFx1RkYwQy1cXHVGRjBGXFx1RkYxQVxcdUZGMUJcXHVGRjFGXFx1RkYyMFxcdUZGM0ItXFx1RkYzRFxcdUZGM0ZcXHVGRjVCXFx1RkY1RFxcdUZGNUYtXFx1RkY2NV18XFx1RDgwMFtcXHVERDAwLVxcdUREMDJcXHVERjlGXFx1REZEMF18XFx1RDgwMVxcdURENkZ8XFx1RDgwMltcXHVEQzU3XFx1REQxRlxcdUREM0ZcXHVERTUwLVxcdURFNThcXHVERTdGXFx1REVGMC1cXHVERUY2XFx1REYzOS1cXHVERjNGXFx1REY5OS1cXHVERjlDXXxcXHVEODAzW1xcdURGNTUtXFx1REY1OV18XFx1RDgwNFtcXHVEQzQ3LVxcdURDNERcXHVEQ0JCXFx1RENCQ1xcdURDQkUtXFx1RENDMVxcdURENDAtXFx1REQ0M1xcdURENzRcXHVERDc1XFx1RERDNS1cXHVEREM4XFx1RERDRFxcdUREREJcXHVERERELVxcdUREREZcXHVERTM4LVxcdURFM0RcXHVERUE5XXxcXHVEODA1W1xcdURDNEItXFx1REM0RlxcdURDNUJcXHVEQzVEXFx1RENDNlxcdUREQzEtXFx1REREN1xcdURFNDEtXFx1REU0M1xcdURFNjAtXFx1REU2Q1xcdURGM0MtXFx1REYzRV18XFx1RDgwNltcXHVEQzNCXFx1REUzRi1cXHVERTQ2XFx1REU5QS1cXHVERTlDXFx1REU5RS1cXHVERUEyXXxcXHVEODA3W1xcdURDNDEtXFx1REM0NVxcdURDNzBcXHVEQzcxXFx1REVGN1xcdURFRjhdfFxcdUQ4MDlbXFx1REM3MC1cXHVEQzc0XXxcXHVEODFBW1xcdURFNkVcXHVERTZGXFx1REVGNVxcdURGMzctXFx1REYzQlxcdURGNDRdfFxcdUQ4MUJbXFx1REU5Ny1cXHVERTlBXXxcXHVEODJGXFx1REM5RnxcXHVEODM2W1xcdURFODctXFx1REU4Ql18XFx1RDgzQVtcXHVERDVFXFx1REQ1Rl0vIiwibW9kdWxlLmV4cG9ydHM9L1sgXFx4QTBcXHUxNjgwXFx1MjAwMC1cXHUyMDBBXFx1MjAyOFxcdTIwMjlcXHUyMDJGXFx1MjA1RlxcdTMwMDBdLyIsIid1c2Ugc3RyaWN0JztcblxuZXhwb3J0cy5BbnkgPSByZXF1aXJlKCcuL3Byb3BlcnRpZXMvQW55L3JlZ2V4Jyk7XG5leHBvcnRzLkNjICA9IHJlcXVpcmUoJy4vY2F0ZWdvcmllcy9DYy9yZWdleCcpO1xuZXhwb3J0cy5DZiAgPSByZXF1aXJlKCcuL2NhdGVnb3JpZXMvQ2YvcmVnZXgnKTtcbmV4cG9ydHMuUCAgID0gcmVxdWlyZSgnLi9jYXRlZ29yaWVzL1AvcmVnZXgnKTtcbmV4cG9ydHMuWiAgID0gcmVxdWlyZSgnLi9jYXRlZ29yaWVzL1ovcmVnZXgnKTtcbiIsIm1vZHVsZS5leHBvcnRzPS9bXFwwLVxcdUQ3RkZcXHVFMDAwLVxcdUZGRkZdfFtcXHVEODAwLVxcdURCRkZdW1xcdURDMDAtXFx1REZGRl18W1xcdUQ4MDAtXFx1REJGRl0oPyFbXFx1REMwMC1cXHVERkZGXSl8KD86W15cXHVEODAwLVxcdURCRkZdfF4pW1xcdURDMDAtXFx1REZGRl0vIiwiLyogZ2xvYmFscyBfX1ZVRV9TU1JfQ09OVEVYVF9fICovXG5cbi8vIElNUE9SVEFOVDogRG8gTk9UIHVzZSBFUzIwMTUgZmVhdHVyZXMgaW4gdGhpcyBmaWxlIChleGNlcHQgZm9yIG1vZHVsZXMpLlxuLy8gVGhpcyBtb2R1bGUgaXMgYSBydW50aW1lIHV0aWxpdHkgZm9yIGNsZWFuZXIgY29tcG9uZW50IG1vZHVsZSBvdXRwdXQgYW5kIHdpbGxcbi8vIGJlIGluY2x1ZGVkIGluIHRoZSBmaW5hbCB3ZWJwYWNrIHVzZXIgYnVuZGxlLlxuXG5leHBvcnQgZGVmYXVsdCBmdW5jdGlvbiBub3JtYWxpemVDb21wb25lbnQgKFxuICBzY3JpcHRFeHBvcnRzLFxuICByZW5kZXIsXG4gIHN0YXRpY1JlbmRlckZucyxcbiAgZnVuY3Rpb25hbFRlbXBsYXRlLFxuICBpbmplY3RTdHlsZXMsXG4gIHNjb3BlSWQsXG4gIG1vZHVsZUlkZW50aWZpZXIsIC8qIHNlcnZlciBvbmx5ICovXG4gIHNoYWRvd01vZGUgLyogdnVlLWNsaSBvbmx5ICovXG4pIHtcbiAgLy8gVnVlLmV4dGVuZCBjb25zdHJ1Y3RvciBleHBvcnQgaW50ZXJvcFxuICB2YXIgb3B0aW9ucyA9IHR5cGVvZiBzY3JpcHRFeHBvcnRzID09PSAnZnVuY3Rpb24nXG4gICAgPyBzY3JpcHRFeHBvcnRzLm9wdGlvbnNcbiAgICA6IHNjcmlwdEV4cG9ydHNcblxuICAvLyByZW5kZXIgZnVuY3Rpb25zXG4gIGlmIChyZW5kZXIpIHtcbiAgICBvcHRpb25zLnJlbmRlciA9IHJlbmRlclxuICAgIG9wdGlvbnMuc3RhdGljUmVuZGVyRm5zID0gc3RhdGljUmVuZGVyRm5zXG4gICAgb3B0aW9ucy5fY29tcGlsZWQgPSB0cnVlXG4gIH1cblxuICAvLyBmdW5jdGlvbmFsIHRlbXBsYXRlXG4gIGlmIChmdW5jdGlvbmFsVGVtcGxhdGUpIHtcbiAgICBvcHRpb25zLmZ1bmN0aW9uYWwgPSB0cnVlXG4gIH1cblxuICAvLyBzY29wZWRJZFxuICBpZiAoc2NvcGVJZCkge1xuICAgIG9wdGlvbnMuX3Njb3BlSWQgPSAnZGF0YS12LScgKyBzY29wZUlkXG4gIH1cblxuICB2YXIgaG9va1xuICBpZiAobW9kdWxlSWRlbnRpZmllcikgeyAvLyBzZXJ2ZXIgYnVpbGRcbiAgICBob29rID0gZnVuY3Rpb24gKGNvbnRleHQpIHtcbiAgICAgIC8vIDIuMyBpbmplY3Rpb25cbiAgICAgIGNvbnRleHQgPVxuICAgICAgICBjb250ZXh0IHx8IC8vIGNhY2hlZCBjYWxsXG4gICAgICAgICh0aGlzLiR2bm9kZSAmJiB0aGlzLiR2bm9kZS5zc3JDb250ZXh0KSB8fCAvLyBzdGF0ZWZ1bFxuICAgICAgICAodGhpcy5wYXJlbnQgJiYgdGhpcy5wYXJlbnQuJHZub2RlICYmIHRoaXMucGFyZW50LiR2bm9kZS5zc3JDb250ZXh0KSAvLyBmdW5jdGlvbmFsXG4gICAgICAvLyAyLjIgd2l0aCBydW5Jbk5ld0NvbnRleHQ6IHRydWVcbiAgICAgIGlmICghY29udGV4dCAmJiB0eXBlb2YgX19WVUVfU1NSX0NPTlRFWFRfXyAhPT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgICAgY29udGV4dCA9IF9fVlVFX1NTUl9DT05URVhUX19cbiAgICAgIH1cbiAgICAgIC8vIGluamVjdCBjb21wb25lbnQgc3R5bGVzXG4gICAgICBpZiAoaW5qZWN0U3R5bGVzKSB7XG4gICAgICAgIGluamVjdFN0eWxlcy5jYWxsKHRoaXMsIGNvbnRleHQpXG4gICAgICB9XG4gICAgICAvLyByZWdpc3RlciBjb21wb25lbnQgbW9kdWxlIGlkZW50aWZpZXIgZm9yIGFzeW5jIGNodW5rIGluZmVycmVuY2VcbiAgICAgIGlmIChjb250ZXh0ICYmIGNvbnRleHQuX3JlZ2lzdGVyZWRDb21wb25lbnRzKSB7XG4gICAgICAgIGNvbnRleHQuX3JlZ2lzdGVyZWRDb21wb25lbnRzLmFkZChtb2R1bGVJZGVudGlmaWVyKVxuICAgICAgfVxuICAgIH1cbiAgICAvLyB1c2VkIGJ5IHNzciBpbiBjYXNlIGNvbXBvbmVudCBpcyBjYWNoZWQgYW5kIGJlZm9yZUNyZWF0ZVxuICAgIC8vIG5ldmVyIGdldHMgY2FsbGVkXG4gICAgb3B0aW9ucy5fc3NyUmVnaXN0ZXIgPSBob29rXG4gIH0gZWxzZSBpZiAoaW5qZWN0U3R5bGVzKSB7XG4gICAgaG9vayA9IHNoYWRvd01vZGVcbiAgICAgID8gZnVuY3Rpb24gKCkge1xuICAgICAgICBpbmplY3RTdHlsZXMuY2FsbChcbiAgICAgICAgICB0aGlzLFxuICAgICAgICAgIChvcHRpb25zLmZ1bmN0aW9uYWwgPyB0aGlzLnBhcmVudCA6IHRoaXMpLiRyb290LiRvcHRpb25zLnNoYWRvd1Jvb3RcbiAgICAgICAgKVxuICAgICAgfVxuICAgICAgOiBpbmplY3RTdHlsZXNcbiAgfVxuXG4gIGlmIChob29rKSB7XG4gICAgaWYgKG9wdGlvbnMuZnVuY3Rpb25hbCkge1xuICAgICAgLy8gZm9yIHRlbXBsYXRlLW9ubHkgaG90LXJlbG9hZCBiZWNhdXNlIGluIHRoYXQgY2FzZSB0aGUgcmVuZGVyIGZuIGRvZXNuJ3RcbiAgICAgIC8vIGdvIHRocm91Z2ggdGhlIG5vcm1hbGl6ZXJcbiAgICAgIG9wdGlvbnMuX2luamVjdFN0eWxlcyA9IGhvb2tcbiAgICAgIC8vIHJlZ2lzdGVyIGZvciBmdW5jdGlvbmFsIGNvbXBvbmVudCBpbiB2dWUgZmlsZVxuICAgICAgdmFyIG9yaWdpbmFsUmVuZGVyID0gb3B0aW9ucy5yZW5kZXJcbiAgICAgIG9wdGlvbnMucmVuZGVyID0gZnVuY3Rpb24gcmVuZGVyV2l0aFN0eWxlSW5qZWN0aW9uIChoLCBjb250ZXh0KSB7XG4gICAgICAgIGhvb2suY2FsbChjb250ZXh0KVxuICAgICAgICByZXR1cm4gb3JpZ2luYWxSZW5kZXIoaCwgY29udGV4dClcbiAgICAgIH1cbiAgICB9IGVsc2Uge1xuICAgICAgLy8gaW5qZWN0IGNvbXBvbmVudCByZWdpc3RyYXRpb24gYXMgYmVmb3JlQ3JlYXRlIGhvb2tcbiAgICAgIHZhciBleGlzdGluZyA9IG9wdGlvbnMuYmVmb3JlQ3JlYXRlXG4gICAgICBvcHRpb25zLmJlZm9yZUNyZWF0ZSA9IGV4aXN0aW5nXG4gICAgICAgID8gW10uY29uY2F0KGV4aXN0aW5nLCBob29rKVxuICAgICAgICA6IFtob29rXVxuICAgIH1cbiAgfVxuXG4gIHJldHVybiB7XG4gICAgZXhwb3J0czogc2NyaXB0RXhwb3J0cyxcbiAgICBvcHRpb25zOiBvcHRpb25zXG4gIH1cbn1cbiIsIi8qIGdsb2JhbHMgX193ZWJwYWNrX2FtZF9vcHRpb25zX18gKi9cbm1vZHVsZS5leHBvcnRzID0gX193ZWJwYWNrX2FtZF9vcHRpb25zX187XG4iLCJ2YXIgZztcblxuLy8gVGhpcyB3b3JrcyBpbiBub24tc3RyaWN0IG1vZGVcbmcgPSAoZnVuY3Rpb24oKSB7XG5cdHJldHVybiB0aGlzO1xufSkoKTtcblxudHJ5IHtcblx0Ly8gVGhpcyB3b3JrcyBpZiBldmFsIGlzIGFsbG93ZWQgKHNlZSBDU1ApXG5cdGcgPSBnIHx8IG5ldyBGdW5jdGlvbihcInJldHVybiB0aGlzXCIpKCk7XG59IGNhdGNoIChlKSB7XG5cdC8vIFRoaXMgd29ya3MgaWYgdGhlIHdpbmRvdyByZWZlcmVuY2UgaXMgYXZhaWxhYmxlXG5cdGlmICh0eXBlb2Ygd2luZG93ID09PSBcIm9iamVjdFwiKSBnID0gd2luZG93O1xufVxuXG4vLyBnIGNhbiBzdGlsbCBiZSB1bmRlZmluZWQsIGJ1dCBub3RoaW5nIHRvIGRvIGFib3V0IGl0Li4uXG4vLyBXZSByZXR1cm4gdW5kZWZpbmVkLCBpbnN0ZWFkIG9mIG5vdGhpbmcgaGVyZSwgc28gaXQnc1xuLy8gZWFzaWVyIHRvIGhhbmRsZSB0aGlzIGNhc2UuIGlmKCFnbG9iYWwpIHsgLi4ufVxuXG5tb2R1bGUuZXhwb3J0cyA9IGc7XG4iLCJtb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uKG1vZHVsZSkge1xuXHRpZiAoIW1vZHVsZS53ZWJwYWNrUG9seWZpbGwpIHtcblx0XHRtb2R1bGUuZGVwcmVjYXRlID0gZnVuY3Rpb24oKSB7fTtcblx0XHRtb2R1bGUucGF0aHMgPSBbXTtcblx0XHQvLyBtb2R1bGUucGFyZW50ID0gdW5kZWZpbmVkIGJ5IGRlZmF1bHRcblx0XHRpZiAoIW1vZHVsZS5jaGlsZHJlbikgbW9kdWxlLmNoaWxkcmVuID0gW107XG5cdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KG1vZHVsZSwgXCJsb2FkZWRcIiwge1xuXHRcdFx0ZW51bWVyYWJsZTogdHJ1ZSxcblx0XHRcdGdldDogZnVuY3Rpb24oKSB7XG5cdFx0XHRcdHJldHVybiBtb2R1bGUubDtcblx0XHRcdH1cblx0XHR9KTtcblx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkobW9kdWxlLCBcImlkXCIsIHtcblx0XHRcdGVudW1lcmFibGU6IHRydWUsXG5cdFx0XHRnZXQ6IGZ1bmN0aW9uKCkge1xuXHRcdFx0XHRyZXR1cm4gbW9kdWxlLmk7XG5cdFx0XHR9XG5cdFx0fSk7XG5cdFx0bW9kdWxlLndlYnBhY2tQb2x5ZmlsbCA9IDE7XG5cdH1cblx0cmV0dXJuIG1vZHVsZTtcbn07XG4iLCJ2YXIgcmVuZGVyLCBzdGF0aWNSZW5kZXJGbnNcbmltcG9ydCBzY3JpcHQgZnJvbSBcIi4vTWFya2Rvd25CbG9jay52dWU/dnVlJnR5cGU9c2NyaXB0Jmxhbmc9anMmXCJcbmV4cG9ydCAqIGZyb20gXCIuL01hcmtkb3duQmxvY2sudnVlP3Z1ZSZ0eXBlPXNjcmlwdCZsYW5nPWpzJlwiXG5cblxuLyogbm9ybWFsaXplIGNvbXBvbmVudCAqL1xuaW1wb3J0IG5vcm1hbGl6ZXIgZnJvbSBcIiEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvcnVudGltZS9jb21wb25lbnROb3JtYWxpemVyLmpzXCJcbnZhciBjb21wb25lbnQgPSBub3JtYWxpemVyKFxuICBzY3JpcHQsXG4gIHJlbmRlcixcbiAgc3RhdGljUmVuZGVyRm5zLFxuICBmYWxzZSxcbiAgbnVsbCxcbiAgbnVsbCxcbiAgbnVsbFxuICBcbilcblxuLyogaG90IHJlbG9hZCAqL1xuaWYgKG1vZHVsZS5ob3QpIHtcbiAgdmFyIGFwaSA9IHJlcXVpcmUoXCJEOlxcXFxVc2Vyc1xcXFxkYW5pXFxcXERlc2t0b3BcXFxcQG1vbm9ncmlkXFxcXG5vZGVfbW9kdWxlc1xcXFx2dWUtaG90LXJlbG9hZC1hcGlcXFxcZGlzdFxcXFxpbmRleC5qc1wiKVxuICBhcGkuaW5zdGFsbChyZXF1aXJlKCd2dWUnKSlcbiAgaWYgKGFwaS5jb21wYXRpYmxlKSB7XG4gICAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICAgIGlmICghYXBpLmlzUmVjb3JkZWQoJzM2ZDBmOWRlJykpIHtcbiAgICAgIGFwaS5jcmVhdGVSZWNvcmQoJzM2ZDBmOWRlJywgY29tcG9uZW50Lm9wdGlvbnMpXG4gICAgfSBlbHNlIHtcbiAgICAgIGFwaS5yZWxvYWQoJzM2ZDBmOWRlJywgY29tcG9uZW50Lm9wdGlvbnMpXG4gICAgfVxuICAgIFxuICB9XG59XG5jb21wb25lbnQub3B0aW9ucy5fX2ZpbGUgPSBcInNyYy9jb21wb25lbnRzL01hcmtkb3duQmxvY2sudnVlXCJcbmV4cG9ydCBkZWZhdWx0IGNvbXBvbmVudC5leHBvcnRzIiwiaW1wb3J0IG1vZCBmcm9tIFwiLSEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvYmFiZWwtbG9hZGVyL2xpYi9pbmRleC5qcyEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvaW5kZXguanM/P3Z1ZS1sb2FkZXItb3B0aW9ucyEuL01hcmtkb3duQmxvY2sudnVlP3Z1ZSZ0eXBlPXNjcmlwdCZsYW5nPWpzJlwiOyBleHBvcnQgZGVmYXVsdCBtb2Q7IGV4cG9ydCAqIGZyb20gXCItIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9iYWJlbC1sb2FkZXIvbGliL2luZGV4LmpzIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9pbmRleC5qcz8/dnVlLWxvYWRlci1vcHRpb25zIS4vTWFya2Rvd25CbG9jay52dWU/dnVlJnR5cGU9c2NyaXB0Jmxhbmc9anMmXCIiLCJtb2R1bGUuZXhwb3J0cyA9IF9fV0VCUEFDS19FWFRFUk5BTF9NT0RVTEVfdnVlX187Il0sInNvdXJjZVJvb3QiOiIifQ==