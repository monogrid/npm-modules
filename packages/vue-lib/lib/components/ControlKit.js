(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("@monogrid/js-utils"));
	else if(typeof define === 'function' && define.amd)
		define("vue-lib", ["@monogrid/js-utils"], factory);
	else if(typeof exports === 'object')
		exports["vue-lib"] = factory(require("@monogrid/js-utils"));
	else
		root["vue-lib"] = factory(root["@monogrid/js-utils"]);
})(window, function(__WEBPACK_EXTERNAL_MODULE__monogrid_js_utils__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/components/ControlKit.vue");
/******/ })
/************************************************************************/
/******/ ({

/***/ "../../node_modules/@babel/runtime/helpers/typeof.js":
/*!*************************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/@babel/runtime/helpers/typeof.js ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _typeof(obj) {
  "@babel/helpers - typeof";

  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    module.exports = _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    module.exports = _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}

module.exports = _typeof;

/***/ }),

/***/ "../../node_modules/babel-loader/lib/index.js!../../node_modules/vue-loader/lib/index.js?!./src/components/ControlKit.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************************************************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/babel-loader/lib!D:/Users/dani/Desktop/@monogrid/node_modules/vue-loader/lib??vue-loader-options!./src/components/ControlKit.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_helpers_typeof__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/typeof */ "../../node_modules/@babel/runtime/helpers/typeof.js");
/* harmony import */ var _babel_runtime_helpers_typeof__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_typeof__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _monogrid_js_utils__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @monogrid/js-utils */ "@monogrid/js-utils");
/* harmony import */ var _monogrid_js_utils__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_monogrid_js_utils__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var lodash_find__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! lodash/find */ "../../node_modules/lodash/find.js");
/* harmony import */ var lodash_find__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(lodash_find__WEBPACK_IMPORTED_MODULE_2__);



var defaultSliderSettings = {
  min: -10,
  max: 10,
  step: 0.01
};
/**
 * A self contained interface able to modify a target object
 * based on a model object.
 *
 * It automatically infers the control types and supports
 * infinitely nested subObjects
 */

/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'ControlKit',
  functional: true,
  props: {
    /**
     * a settings model object with properties
     * to be modified in the target object.
     *
     * It can have sub-objects and sub-properties.
     */
    model: {
      required: true
    },

    /**
     * The target object to modify.
     */
    target: {
      required: false
    },

    /**
     * An array of additional controls to use.
     * TODO: Documentation
     */
    additionalControls: {
      type: Array,
      required: false
    },

    /**
     * Root title displayed
     */
    title: {
      type: String,
      required: false,
      default: 'Options'
    },

    /**
     * Object containing a definition of settings
     * TODO: Documentation
     */
    settings: {
      type: Object,
      required: false,
      default: function _default() {
        return {};
      }
    }
  },
  render: function render(h, context) {
    var sentenceCase = function sentenceCase(text) {
      var result = text.replace(/([A-Z](?![A-Z]))/g, ' $1');
      return String(result.charAt(0).toUpperCase() + result.slice(1)).trim();
    };

    var getNumberSlider = function getNumberSlider(h, target, prop, path) {
      var val = target[prop];
      var setting;

      if (context.props.settings.sliders) {
        setting = lodash_find__WEBPACK_IMPORTED_MODULE_2___default()(context.props.settings.sliders, function (o) {
          return path.match(o.match);
        });
      }

      if (!setting) {
        setting = defaultSliderSettings;
      }

      return h("div", {
        "class": "number-slider-container"
      }, [h("input", {
        "attrs": {
          "id": path,
          "type": "range",
          "min": setting.min,
          "max": setting.max,
          "step": setting.step
        },
        "class": "number-slider",
        "domProps": {
          "value": val
        },
        "on": {
          "input": function input(e) {
            target[prop] = parseFloat(e.target.value);
          },
          "change": function change(e) {
            target[prop] = parseFloat(e.target.value);
          }
        }
      }), h("input", {
        "class": "number-input",
        "domProps": {
          "value": val
        },
        "attrs": {
          "step": setting.step,
          "type": "number"
        },
        "on": {
          "change": function change(e) {
            target[prop] = parseFloat(e.target.value);
          },
          "input": function input(e) {
            target[prop] = parseFloat(e.target.value);
          }
        }
      })]); //
    };

    var parseModel = function parseModel(h, model, output) {
      var title = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : 'Options';
      var target = arguments.length > 4 ? arguments[4] : undefined;
      var path = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : 'model';
      var children = [];

      var _loop = function _loop(prop) {
        if (prop.indexOf('_') === 0) return "continue"; // skip "private" props

        switch (_babel_runtime_helpers_typeof__WEBPACK_IMPORTED_MODULE_0___default()(model[prop])) {
          case 'string':
            children.push(h("div", {
              "class": "item"
            }, [h("label", {
              "attrs": {
                "for": path + '.' + prop
              },
              "class": "item-title"
            }, [sentenceCase(prop)]), h("div", {
              "class": "item-content"
            }, [h("input", {
              "attrs": {
                "id": path + '.' + prop
              },
              "domProps": {
                "value": target[prop]
              },
              "on": {
                "change": function change(event) {
                  target[prop] = event.target.value;
                }
              }
            })])])); //

            break;

          case 'boolean':
            children.push(h("div", {
              "class": "item"
            }, [h("label", {
              "attrs": {
                "for": path + '.' + prop
              },
              "class": "item-title"
            }, [sentenceCase(prop)]), h("div", {
              "class": "item-content"
            }, [h("input", {
              "attrs": {
                "id": path + '.' + prop,
                "type": "checkbox"
              },
              "domProps": {
                "checked": target[prop]
              },
              "on": {
                "change": function change(event) {
                  target[prop] = event.target.checked;
                }
              }
            })])])); //

            break;

          case 'number':
            children.push(h("div", {
              "class": "item"
            }, [h("label", {
              "attrs": {
                "for": path + '.' + prop
              },
              "class": "item-title"
            }, [sentenceCase(prop)]), h("div", {
              "class": "item-content"
            }, [getNumberSlider(h, target, prop, path + '.' + prop)])])); //

            break;

          case 'function':
            children.push(h("div", {
              "class": "item"
            }, [h("label", {
              "attrs": {
                "for": path + '.' + prop
              },
              "class": "item-title"
            }, ["Execute"]), h("div", {
              "class": "item-content"
            }, [h("button", {
              "attrs": {
                "id": path + '.' + prop
              },
              "on": {
                "click": target[prop] ? target[prop] : model[prop]
              }
            }, [sentenceCase(prop)])])])); //

            break;

          case 'object':
            if (_monogrid_js_utils__WEBPACK_IMPORTED_MODULE_1__["ObjectUtils"].isPlainObject(model[prop])) {
              parseModel(h, model[prop], children, prop, target[prop], path + '.' + prop);
            } else {
              if (context.props.additionalControls && context.props.additionalControls.length > 0) {
                for (var i = 0; i < context.props.additionalControls.length; i++) {
                  var controls = context.props.additionalControls[i];

                  for (var j = 0; j < controls.length; j++) {
                    if (controls[j].match(model[prop])) {
                      children.push(h("div", {
                        "class": "item"
                      }, [h("label", {
                        "attrs": {
                          "for": path + '.' + prop
                        },
                        "class": "item-title"
                      }, [sentenceCase(prop)]), h("div", {
                        "class": "item-content"
                      }, [controls[j].getControl(h, target, prop, path + '.' + prop)])])); //

                      break;
                    }
                  }
                }
              }
            }

            break;
        }
      };

      for (var prop in model) {
        var _ret = _loop(prop);

        if (_ret === "continue") continue;
      }

      output.push(h("div", {
        "class": "wrap-collabsible wrap-collabsible-panel"
      }, [h("input", {
        "attrs": {
          "id": path,
          "type": "checkbox"
        },
        "class": "toggle"
      }), h("label", {
        "attrs": {
          "for": path
        },
        "class": "lbl-toggle lbl-toggle-panel"
      }, [sentenceCase(title)]), h("div", {
        "class": "collapsible-content collapsible-content-panel"
      }, [children])])); //
    };

    var elements = [];
    parseModel(h, context.props.model, elements, context.props.title, context.props.target ? context.props.target : context.props.model, context.props.target ? 'target' : 'model');
    return h("div", {
      "class": "control-panel"
    }, [elements]);
  }
});

/***/ }),

/***/ "../../node_modules/css-loader/dist/cjs.js?!../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../node_modules/postcss-loader/src/index.js?!../../node_modules/sass-loader/dist/cjs.js?!../../node_modules/vue-loader/lib/index.js?!./src/components/ControlKit.vue?vue&type=style&index=0&lang=scss&":
/*!***************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/css-loader/dist/cjs.js??ref--7-1!D:/Users/dani/Desktop/@monogrid/node_modules/vue-loader/lib/loaders/stylePostLoader.js!D:/Users/dani/Desktop/@monogrid/node_modules/postcss-loader/src??ref--7-2!D:/Users/dani/Desktop/@monogrid/node_modules/sass-loader/dist/cjs.js??ref--7-3!D:/Users/dani/Desktop/@monogrid/node_modules/vue-loader/lib??vue-loader-options!./src/components/ControlKit.vue?vue&type=style&index=0&lang=scss& ***!
  \***************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../node_modules/css-loader/dist/runtime/api.js */ "../../node_modules/css-loader/dist/runtime/api.js");
exports = ___CSS_LOADER_API_IMPORT___(false);
// Module
exports.push([module.i, ".control-panel {\n  background: #000;\n  -webkit-box-sizing: border-box;\n          box-sizing: border-box;\n  color: #c3cedf;\n  font-family: monospace;\n  font-size: 11px;\n  line-height: 1;\n  max-height: 100%;\n  overflow-x: hidden;\n  overflow-y: auto;\n  position: absolute;\n  right: 0;\n  top: 0;\n  width: 370px;\n  /*\n    collapsible style\n  */\n  /* stylelint-disable */\n  /* stylelint-enable */\n  /*\n    collapsible panel style\n  */\n  /* stylelint-disable */\n  /* stylelint-enable */\n  /*\n    items style\n  */\n  /*\n    Number slider style\n  */\n  /*\n    Button input style\n  */\n  /* stylelint-disable */\n  /*\n    Range input style\n  */\n}\n.control-panel .wrap-collabsible {\n  -webkit-box-sizing: border-box;\n          box-sizing: border-box;\n}\n.control-panel .wrap-collabsible > input[type=checkbox] {\n  display: none;\n}\n.control-panel label {\n  -webkit-box-sizing: border-box;\n          box-sizing: border-box;\n  cursor: pointer;\n  display: block;\n}\n.control-panel .collapsible-content {\n  -webkit-box-sizing: border-box;\n          box-sizing: border-box;\n  display: none;\n  height: 0;\n  overflow: hidden;\n}\n.control-panel .toggle:checked + .lbl-toggle + .collapsible-content {\n  display: block;\n  height: auto;\n}\n.control-panel .wrap-collabsible-panel {\n  margin-bottom: 2px;\n  margin-top: 2px;\n  padding-left: 5px;\n  padding-right: 5px;\n  width: 100%;\n}\n.control-panel .lbl-toggle-panel {\n  background: #051e42;\n  border-bottom: 1px solid rgba(104, 140, 194, 0.25);\n  border-left: 1px solid rgba(104, 140, 194, 0.25);\n  border-radius: 2px;\n  border-right: 1px solid rgba(104, 140, 194, 0.25);\n  border-top: 1px solid rgba(104, 140, 194, 0.25);\n  color: #c3cedf;\n  font-weight: bold;\n  padding: 4px;\n}\n.control-panel .lbl-toggle-panel::before {\n  border-bottom: 5px solid transparent;\n  border-left: 5px solid currentColor;\n  border-top: 5px solid transparent;\n  content: \" \";\n  display: inline-block;\n  margin-right: 0.7rem;\n  -webkit-transform: translateY(-2px);\n          transform: translateY(-2px);\n  -webkit-transition: -webkit-transform 0.2s ease-out;\n  transition: -webkit-transform 0.2s ease-out;\n  transition: transform 0.2s ease-out;\n  transition: transform 0.2s ease-out, -webkit-transform 0.2s ease-out;\n  vertical-align: middle;\n}\n.control-panel .collapsible-content-panel {\n  background: rgba(8, 18, 32, 0.45);\n  border-bottom-left-radius: 2px;\n  border-bottom-right-radius: 2px;\n  width: 100%;\n}\n.control-panel .toggle:checked + .lbl-toggle-panel + .collapsible-content-panel {\n  border-bottom: 1px solid rgba(104, 140, 194, 0.25);\n  border-left: 1px solid rgba(104, 140, 194, 0.25);\n  border-right: 1px solid rgba(104, 140, 194, 0.25);\n}\n.control-panel .toggle:checked + .lbl-toggle-panel {\n  border-bottom: 0;\n  border-bottom-left-radius: 0;\n  border-bottom-right-radius: 0;\n}\n.control-panel .toggle:checked + .lbl-toggle-panel::before {\n  -webkit-transform: rotate(90deg) translateX(-3px);\n          transform: rotate(90deg) translateX(-3px);\n}\n.control-panel .vc-chrome-body,\n.control-panel .vc-chrome {\n  background: transparent;\n  -webkit-box-shadow: none;\n          box-shadow: none;\n}\n.control-panel .item {\n  -webkit-box-sizing: border-box;\n          box-sizing: border-box;\n  padding-bottom: 1px;\n  padding-top: 1px;\n  width: 100%;\n}\n.control-panel .set-border-color {\n  border: 1px solid #688cc2;\n}\n.control-panel .item-title,\n.control-panel .item-content {\n  -webkit-box-sizing: border-box;\n          box-sizing: border-box;\n  display: inline-block;\n  vertical-align: middle;\n}\n.control-panel .item-content {\n  padding: 0 5px;\n  width: 65%;\n}\n.control-panel .item-title {\n  text-align: right;\n  width: 35%;\n}\n.control-panel .number-slider-container {\n  -webkit-box-sizing: border-box;\n          box-sizing: border-box;\n  position: relative;\n  white-space: nowrap;\n  width: 100%;\n}\n.control-panel .number-slider-container .number-slider,\n.control-panel .number-slider-container .number-input {\n  display: inline-block;\n  position: relative;\n}\n.control-panel .number-slider-container .number-slider {\n  margin: 0;\n  margin-right: 10px;\n  padding: 0;\n  width: 70%;\n}\n.control-panel .number-slider-container .number-input {\n  font-size: 11px;\n  height: 11px;\n  top: -4px;\n  width: 20%;\n}\n.control-panel button {\n  background: #051e42;\n  border: 1px solid rgba(104, 140, 194, 0.25);\n  border-radius: 2px;\n  color: #c3cedf;\n  cursor: pointer;\n  font-size: 11px;\n  padding: 5px 10px;\n}\n.control-panel input,\n.control-panel .vc-input__input {\n  background: #081220;\n  border: 1px solid #688cc2;\n  color: #c3cedf;\n  /* stylelint-enable */\n  font-family: monospace;\n  padding: 1px;\n}\n.control-panel input[type=checkbox] {\n  -webkit-appearance: none;\n     -moz-appearance: none;\n          appearance: none;\n  background: #081220;\n  border: 1px solid #688cc2;\n  cursor: pointer;\n  height: 15px;\n  margin: 0;\n  width: 15px;\n}\n.control-panel input[type=checkbox]::after {\n  background: #c3cedf;\n  -webkit-box-sizing: border-box;\n          box-sizing: border-box;\n  content: \"\";\n  display: none;\n  height: 100%;\n  width: 100%;\n}\n.control-panel input[type=checkbox]:checked::after {\n  display: block;\n}\n.control-panel input[type=range] {\n  -webkit-appearance: none;\n     -moz-appearance: none;\n          appearance: none;\n  background: none;\n  border: 0;\n  margin: 18px 0;\n}\n.control-panel input[type=range]::-webkit-slider-runnable-track {\n  background: #081220;\n  border: 1px solid #688cc2;\n  cursor: pointer;\n  height: 15px;\n}\n.control-panel input[type=range]::-moz-range-track {\n  background: #081220;\n  border: 1px solid #688cc2;\n  cursor: pointer;\n  height: 15px;\n}\n.control-panel input[type=range]::-ms-track {\n  background: transparent;\n  border: 0;\n  color: transparent;\n  cursor: pointer;\n  height: 15px;\n}\n.control-panel input[type=range]::-webkit-slider-thumb {\n  -webkit-appearance: none;\n          appearance: none;\n  background: #051e42;\n  border: 1px solid #c3cedf;\n  border-radius: 1px;\n  cursor: pointer;\n  height: 11px;\n  margin-top: 1px;\n  width: 7px;\n}\n.control-panel input[type=range]::-moz-range-thumb {\n  background: #051e42;\n  border: 1px solid #c3cedf;\n  border-radius: 1px;\n  cursor: pointer;\n  height: 11px;\n  width: 7px;\n}\n.control-panel input[type=range]::-ms-thumb {\n  background: #051e42;\n  border: 1px solid #c3cedf;\n  border-radius: 1px;\n  cursor: pointer;\n  height: 11px;\n  width: 7px;\n}\n.control-panel input[type=range]::-ms-fill-lower {\n  background: #081220;\n  border: 1px solid #688cc2;\n}\n.control-panel input[type=range]::-ms-fill-upper {\n  background: #081220;\n  border: 1px solid #688cc2;\n}\n.control-panel input[type=range]:focus {\n  outline: none;\n}\n.control-panel input[type=range]:focus::-webkit-slider-runnable-track {\n  background: #051e42;\n}\n.control-panel input[type=range]:focus::-ms-fill-lower {\n  background: #051e42;\n}\n.control-panel input[type=range]:focus::-ms-fill-upper {\n  background: #051e42;\n}", ""]);
// Exports
module.exports = exports;


/***/ }),

/***/ "../../node_modules/css-loader/dist/runtime/api.js":
/*!***********************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/css-loader/dist/runtime/api.js ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/*
  MIT License http://www.opensource.org/licenses/mit-license.php
  Author Tobias Koppers @sokra
*/
// css base code, injected by the css-loader
// eslint-disable-next-line func-names

module.exports = function (useSourceMap) {
  var list = []; // return the list of modules as css string

  list.toString = function toString() {
    return this.map(function (item) {
      var content = cssWithMappingToString(item, useSourceMap);

      if (item[2]) {
        return "@media ".concat(item[2], " {").concat(content, "}");
      }

      return content;
    }).join('');
  }; // import a list of modules into the list
  // eslint-disable-next-line func-names


  list.i = function (modules, mediaQuery, dedupe) {
    if (typeof modules === 'string') {
      // eslint-disable-next-line no-param-reassign
      modules = [[null, modules, '']];
    }

    var alreadyImportedModules = {};

    if (dedupe) {
      for (var i = 0; i < this.length; i++) {
        // eslint-disable-next-line prefer-destructuring
        var id = this[i][0];

        if (id != null) {
          alreadyImportedModules[id] = true;
        }
      }
    }

    for (var _i = 0; _i < modules.length; _i++) {
      var item = [].concat(modules[_i]);

      if (dedupe && alreadyImportedModules[item[0]]) {
        // eslint-disable-next-line no-continue
        continue;
      }

      if (mediaQuery) {
        if (!item[2]) {
          item[2] = mediaQuery;
        } else {
          item[2] = "".concat(mediaQuery, " and ").concat(item[2]);
        }
      }

      list.push(item);
    }
  };

  return list;
};

function cssWithMappingToString(item, useSourceMap) {
  var content = item[1] || ''; // eslint-disable-next-line prefer-destructuring

  var cssMapping = item[3];

  if (!cssMapping) {
    return content;
  }

  if (useSourceMap && typeof btoa === 'function') {
    var sourceMapping = toComment(cssMapping);
    var sourceURLs = cssMapping.sources.map(function (source) {
      return "/*# sourceURL=".concat(cssMapping.sourceRoot || '').concat(source, " */");
    });
    return [content].concat(sourceURLs).concat([sourceMapping]).join('\n');
  }

  return [content].join('\n');
} // Adapted from convert-source-map (MIT)


function toComment(sourceMap) {
  // eslint-disable-next-line no-undef
  var base64 = btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap))));
  var data = "sourceMappingURL=data:application/json;charset=utf-8;base64,".concat(base64);
  return "/*# ".concat(data, " */");
}

/***/ }),

/***/ "../../node_modules/lodash/_DataView.js":
/*!************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_DataView.js ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var getNative = __webpack_require__(/*! ./_getNative */ "../../node_modules/lodash/_getNative.js"),
    root = __webpack_require__(/*! ./_root */ "../../node_modules/lodash/_root.js");
/* Built-in method references that are verified to be native. */


var DataView = getNative(root, 'DataView');
module.exports = DataView;

/***/ }),

/***/ "../../node_modules/lodash/_Hash.js":
/*!********************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_Hash.js ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var hashClear = __webpack_require__(/*! ./_hashClear */ "../../node_modules/lodash/_hashClear.js"),
    hashDelete = __webpack_require__(/*! ./_hashDelete */ "../../node_modules/lodash/_hashDelete.js"),
    hashGet = __webpack_require__(/*! ./_hashGet */ "../../node_modules/lodash/_hashGet.js"),
    hashHas = __webpack_require__(/*! ./_hashHas */ "../../node_modules/lodash/_hashHas.js"),
    hashSet = __webpack_require__(/*! ./_hashSet */ "../../node_modules/lodash/_hashSet.js");
/**
 * Creates a hash object.
 *
 * @private
 * @constructor
 * @param {Array} [entries] The key-value pairs to cache.
 */


function Hash(entries) {
  var index = -1,
      length = entries == null ? 0 : entries.length;
  this.clear();

  while (++index < length) {
    var entry = entries[index];
    this.set(entry[0], entry[1]);
  }
} // Add methods to `Hash`.


Hash.prototype.clear = hashClear;
Hash.prototype['delete'] = hashDelete;
Hash.prototype.get = hashGet;
Hash.prototype.has = hashHas;
Hash.prototype.set = hashSet;
module.exports = Hash;

/***/ }),

/***/ "../../node_modules/lodash/_ListCache.js":
/*!*************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_ListCache.js ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var listCacheClear = __webpack_require__(/*! ./_listCacheClear */ "../../node_modules/lodash/_listCacheClear.js"),
    listCacheDelete = __webpack_require__(/*! ./_listCacheDelete */ "../../node_modules/lodash/_listCacheDelete.js"),
    listCacheGet = __webpack_require__(/*! ./_listCacheGet */ "../../node_modules/lodash/_listCacheGet.js"),
    listCacheHas = __webpack_require__(/*! ./_listCacheHas */ "../../node_modules/lodash/_listCacheHas.js"),
    listCacheSet = __webpack_require__(/*! ./_listCacheSet */ "../../node_modules/lodash/_listCacheSet.js");
/**
 * Creates an list cache object.
 *
 * @private
 * @constructor
 * @param {Array} [entries] The key-value pairs to cache.
 */


function ListCache(entries) {
  var index = -1,
      length = entries == null ? 0 : entries.length;
  this.clear();

  while (++index < length) {
    var entry = entries[index];
    this.set(entry[0], entry[1]);
  }
} // Add methods to `ListCache`.


ListCache.prototype.clear = listCacheClear;
ListCache.prototype['delete'] = listCacheDelete;
ListCache.prototype.get = listCacheGet;
ListCache.prototype.has = listCacheHas;
ListCache.prototype.set = listCacheSet;
module.exports = ListCache;

/***/ }),

/***/ "../../node_modules/lodash/_Map.js":
/*!*******************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_Map.js ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var getNative = __webpack_require__(/*! ./_getNative */ "../../node_modules/lodash/_getNative.js"),
    root = __webpack_require__(/*! ./_root */ "../../node_modules/lodash/_root.js");
/* Built-in method references that are verified to be native. */


var Map = getNative(root, 'Map');
module.exports = Map;

/***/ }),

/***/ "../../node_modules/lodash/_MapCache.js":
/*!************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_MapCache.js ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var mapCacheClear = __webpack_require__(/*! ./_mapCacheClear */ "../../node_modules/lodash/_mapCacheClear.js"),
    mapCacheDelete = __webpack_require__(/*! ./_mapCacheDelete */ "../../node_modules/lodash/_mapCacheDelete.js"),
    mapCacheGet = __webpack_require__(/*! ./_mapCacheGet */ "../../node_modules/lodash/_mapCacheGet.js"),
    mapCacheHas = __webpack_require__(/*! ./_mapCacheHas */ "../../node_modules/lodash/_mapCacheHas.js"),
    mapCacheSet = __webpack_require__(/*! ./_mapCacheSet */ "../../node_modules/lodash/_mapCacheSet.js");
/**
 * Creates a map cache object to store key-value pairs.
 *
 * @private
 * @constructor
 * @param {Array} [entries] The key-value pairs to cache.
 */


function MapCache(entries) {
  var index = -1,
      length = entries == null ? 0 : entries.length;
  this.clear();

  while (++index < length) {
    var entry = entries[index];
    this.set(entry[0], entry[1]);
  }
} // Add methods to `MapCache`.


MapCache.prototype.clear = mapCacheClear;
MapCache.prototype['delete'] = mapCacheDelete;
MapCache.prototype.get = mapCacheGet;
MapCache.prototype.has = mapCacheHas;
MapCache.prototype.set = mapCacheSet;
module.exports = MapCache;

/***/ }),

/***/ "../../node_modules/lodash/_Promise.js":
/*!***********************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_Promise.js ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var getNative = __webpack_require__(/*! ./_getNative */ "../../node_modules/lodash/_getNative.js"),
    root = __webpack_require__(/*! ./_root */ "../../node_modules/lodash/_root.js");
/* Built-in method references that are verified to be native. */


var Promise = getNative(root, 'Promise');
module.exports = Promise;

/***/ }),

/***/ "../../node_modules/lodash/_Set.js":
/*!*******************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_Set.js ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var getNative = __webpack_require__(/*! ./_getNative */ "../../node_modules/lodash/_getNative.js"),
    root = __webpack_require__(/*! ./_root */ "../../node_modules/lodash/_root.js");
/* Built-in method references that are verified to be native. */


var Set = getNative(root, 'Set');
module.exports = Set;

/***/ }),

/***/ "../../node_modules/lodash/_SetCache.js":
/*!************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_SetCache.js ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var MapCache = __webpack_require__(/*! ./_MapCache */ "../../node_modules/lodash/_MapCache.js"),
    setCacheAdd = __webpack_require__(/*! ./_setCacheAdd */ "../../node_modules/lodash/_setCacheAdd.js"),
    setCacheHas = __webpack_require__(/*! ./_setCacheHas */ "../../node_modules/lodash/_setCacheHas.js");
/**
 *
 * Creates an array cache object to store unique values.
 *
 * @private
 * @constructor
 * @param {Array} [values] The values to cache.
 */


function SetCache(values) {
  var index = -1,
      length = values == null ? 0 : values.length;
  this.__data__ = new MapCache();

  while (++index < length) {
    this.add(values[index]);
  }
} // Add methods to `SetCache`.


SetCache.prototype.add = SetCache.prototype.push = setCacheAdd;
SetCache.prototype.has = setCacheHas;
module.exports = SetCache;

/***/ }),

/***/ "../../node_modules/lodash/_Stack.js":
/*!*********************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_Stack.js ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var ListCache = __webpack_require__(/*! ./_ListCache */ "../../node_modules/lodash/_ListCache.js"),
    stackClear = __webpack_require__(/*! ./_stackClear */ "../../node_modules/lodash/_stackClear.js"),
    stackDelete = __webpack_require__(/*! ./_stackDelete */ "../../node_modules/lodash/_stackDelete.js"),
    stackGet = __webpack_require__(/*! ./_stackGet */ "../../node_modules/lodash/_stackGet.js"),
    stackHas = __webpack_require__(/*! ./_stackHas */ "../../node_modules/lodash/_stackHas.js"),
    stackSet = __webpack_require__(/*! ./_stackSet */ "../../node_modules/lodash/_stackSet.js");
/**
 * Creates a stack cache object to store key-value pairs.
 *
 * @private
 * @constructor
 * @param {Array} [entries] The key-value pairs to cache.
 */


function Stack(entries) {
  var data = this.__data__ = new ListCache(entries);
  this.size = data.size;
} // Add methods to `Stack`.


Stack.prototype.clear = stackClear;
Stack.prototype['delete'] = stackDelete;
Stack.prototype.get = stackGet;
Stack.prototype.has = stackHas;
Stack.prototype.set = stackSet;
module.exports = Stack;

/***/ }),

/***/ "../../node_modules/lodash/_Symbol.js":
/*!**********************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_Symbol.js ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var root = __webpack_require__(/*! ./_root */ "../../node_modules/lodash/_root.js");
/** Built-in value references. */


var _Symbol = root.Symbol;
module.exports = _Symbol;

/***/ }),

/***/ "../../node_modules/lodash/_Uint8Array.js":
/*!**************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_Uint8Array.js ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var root = __webpack_require__(/*! ./_root */ "../../node_modules/lodash/_root.js");
/** Built-in value references. */


var Uint8Array = root.Uint8Array;
module.exports = Uint8Array;

/***/ }),

/***/ "../../node_modules/lodash/_WeakMap.js":
/*!***********************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_WeakMap.js ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var getNative = __webpack_require__(/*! ./_getNative */ "../../node_modules/lodash/_getNative.js"),
    root = __webpack_require__(/*! ./_root */ "../../node_modules/lodash/_root.js");
/* Built-in method references that are verified to be native. */


var WeakMap = getNative(root, 'WeakMap');
module.exports = WeakMap;

/***/ }),

/***/ "../../node_modules/lodash/_arrayFilter.js":
/*!***************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_arrayFilter.js ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/**
 * A specialized version of `_.filter` for arrays without support for
 * iteratee shorthands.
 *
 * @private
 * @param {Array} [array] The array to iterate over.
 * @param {Function} predicate The function invoked per iteration.
 * @returns {Array} Returns the new filtered array.
 */
function arrayFilter(array, predicate) {
  var index = -1,
      length = array == null ? 0 : array.length,
      resIndex = 0,
      result = [];

  while (++index < length) {
    var value = array[index];

    if (predicate(value, index, array)) {
      result[resIndex++] = value;
    }
  }

  return result;
}

module.exports = arrayFilter;

/***/ }),

/***/ "../../node_modules/lodash/_arrayLikeKeys.js":
/*!*****************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_arrayLikeKeys.js ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var baseTimes = __webpack_require__(/*! ./_baseTimes */ "../../node_modules/lodash/_baseTimes.js"),
    isArguments = __webpack_require__(/*! ./isArguments */ "../../node_modules/lodash/isArguments.js"),
    isArray = __webpack_require__(/*! ./isArray */ "../../node_modules/lodash/isArray.js"),
    isBuffer = __webpack_require__(/*! ./isBuffer */ "../../node_modules/lodash/isBuffer.js"),
    isIndex = __webpack_require__(/*! ./_isIndex */ "../../node_modules/lodash/_isIndex.js"),
    isTypedArray = __webpack_require__(/*! ./isTypedArray */ "../../node_modules/lodash/isTypedArray.js");
/** Used for built-in method references. */


var objectProto = Object.prototype;
/** Used to check objects for own properties. */

var hasOwnProperty = objectProto.hasOwnProperty;
/**
 * Creates an array of the enumerable property names of the array-like `value`.
 *
 * @private
 * @param {*} value The value to query.
 * @param {boolean} inherited Specify returning inherited property names.
 * @returns {Array} Returns the array of property names.
 */

function arrayLikeKeys(value, inherited) {
  var isArr = isArray(value),
      isArg = !isArr && isArguments(value),
      isBuff = !isArr && !isArg && isBuffer(value),
      isType = !isArr && !isArg && !isBuff && isTypedArray(value),
      skipIndexes = isArr || isArg || isBuff || isType,
      result = skipIndexes ? baseTimes(value.length, String) : [],
      length = result.length;

  for (var key in value) {
    if ((inherited || hasOwnProperty.call(value, key)) && !(skipIndexes && ( // Safari 9 has enumerable `arguments.length` in strict mode.
    key == 'length' || // Node.js 0.10 has enumerable non-index properties on buffers.
    isBuff && (key == 'offset' || key == 'parent') || // PhantomJS 2 has enumerable non-index properties on typed arrays.
    isType && (key == 'buffer' || key == 'byteLength' || key == 'byteOffset') || // Skip index properties.
    isIndex(key, length)))) {
      result.push(key);
    }
  }

  return result;
}

module.exports = arrayLikeKeys;

/***/ }),

/***/ "../../node_modules/lodash/_arrayMap.js":
/*!************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_arrayMap.js ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/**
 * A specialized version of `_.map` for arrays without support for iteratee
 * shorthands.
 *
 * @private
 * @param {Array} [array] The array to iterate over.
 * @param {Function} iteratee The function invoked per iteration.
 * @returns {Array} Returns the new mapped array.
 */
function arrayMap(array, iteratee) {
  var index = -1,
      length = array == null ? 0 : array.length,
      result = Array(length);

  while (++index < length) {
    result[index] = iteratee(array[index], index, array);
  }

  return result;
}

module.exports = arrayMap;

/***/ }),

/***/ "../../node_modules/lodash/_arrayPush.js":
/*!*************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_arrayPush.js ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/**
 * Appends the elements of `values` to `array`.
 *
 * @private
 * @param {Array} array The array to modify.
 * @param {Array} values The values to append.
 * @returns {Array} Returns `array`.
 */
function arrayPush(array, values) {
  var index = -1,
      length = values.length,
      offset = array.length;

  while (++index < length) {
    array[offset + index] = values[index];
  }

  return array;
}

module.exports = arrayPush;

/***/ }),

/***/ "../../node_modules/lodash/_arraySome.js":
/*!*************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_arraySome.js ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/**
 * A specialized version of `_.some` for arrays without support for iteratee
 * shorthands.
 *
 * @private
 * @param {Array} [array] The array to iterate over.
 * @param {Function} predicate The function invoked per iteration.
 * @returns {boolean} Returns `true` if any element passes the predicate check,
 *  else `false`.
 */
function arraySome(array, predicate) {
  var index = -1,
      length = array == null ? 0 : array.length;

  while (++index < length) {
    if (predicate(array[index], index, array)) {
      return true;
    }
  }

  return false;
}

module.exports = arraySome;

/***/ }),

/***/ "../../node_modules/lodash/_assocIndexOf.js":
/*!****************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_assocIndexOf.js ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var eq = __webpack_require__(/*! ./eq */ "../../node_modules/lodash/eq.js");
/**
 * Gets the index at which the `key` is found in `array` of key-value pairs.
 *
 * @private
 * @param {Array} array The array to inspect.
 * @param {*} key The key to search for.
 * @returns {number} Returns the index of the matched value, else `-1`.
 */


function assocIndexOf(array, key) {
  var length = array.length;

  while (length--) {
    if (eq(array[length][0], key)) {
      return length;
    }
  }

  return -1;
}

module.exports = assocIndexOf;

/***/ }),

/***/ "../../node_modules/lodash/_baseFindIndex.js":
/*!*****************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_baseFindIndex.js ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/**
 * The base implementation of `_.findIndex` and `_.findLastIndex` without
 * support for iteratee shorthands.
 *
 * @private
 * @param {Array} array The array to inspect.
 * @param {Function} predicate The function invoked per iteration.
 * @param {number} fromIndex The index to search from.
 * @param {boolean} [fromRight] Specify iterating from right to left.
 * @returns {number} Returns the index of the matched value, else `-1`.
 */
function baseFindIndex(array, predicate, fromIndex, fromRight) {
  var length = array.length,
      index = fromIndex + (fromRight ? 1 : -1);

  while (fromRight ? index-- : ++index < length) {
    if (predicate(array[index], index, array)) {
      return index;
    }
  }

  return -1;
}

module.exports = baseFindIndex;

/***/ }),

/***/ "../../node_modules/lodash/_baseGet.js":
/*!***********************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_baseGet.js ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var castPath = __webpack_require__(/*! ./_castPath */ "../../node_modules/lodash/_castPath.js"),
    toKey = __webpack_require__(/*! ./_toKey */ "../../node_modules/lodash/_toKey.js");
/**
 * The base implementation of `_.get` without support for default values.
 *
 * @private
 * @param {Object} object The object to query.
 * @param {Array|string} path The path of the property to get.
 * @returns {*} Returns the resolved value.
 */


function baseGet(object, path) {
  path = castPath(path, object);
  var index = 0,
      length = path.length;

  while (object != null && index < length) {
    object = object[toKey(path[index++])];
  }

  return index && index == length ? object : undefined;
}

module.exports = baseGet;

/***/ }),

/***/ "../../node_modules/lodash/_baseGetAllKeys.js":
/*!******************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_baseGetAllKeys.js ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var arrayPush = __webpack_require__(/*! ./_arrayPush */ "../../node_modules/lodash/_arrayPush.js"),
    isArray = __webpack_require__(/*! ./isArray */ "../../node_modules/lodash/isArray.js");
/**
 * The base implementation of `getAllKeys` and `getAllKeysIn` which uses
 * `keysFunc` and `symbolsFunc` to get the enumerable property names and
 * symbols of `object`.
 *
 * @private
 * @param {Object} object The object to query.
 * @param {Function} keysFunc The function to get the keys of `object`.
 * @param {Function} symbolsFunc The function to get the symbols of `object`.
 * @returns {Array} Returns the array of property names and symbols.
 */


function baseGetAllKeys(object, keysFunc, symbolsFunc) {
  var result = keysFunc(object);
  return isArray(object) ? result : arrayPush(result, symbolsFunc(object));
}

module.exports = baseGetAllKeys;

/***/ }),

/***/ "../../node_modules/lodash/_baseGetTag.js":
/*!**************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_baseGetTag.js ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var _Symbol = __webpack_require__(/*! ./_Symbol */ "../../node_modules/lodash/_Symbol.js"),
    getRawTag = __webpack_require__(/*! ./_getRawTag */ "../../node_modules/lodash/_getRawTag.js"),
    objectToString = __webpack_require__(/*! ./_objectToString */ "../../node_modules/lodash/_objectToString.js");
/** `Object#toString` result references. */


var nullTag = '[object Null]',
    undefinedTag = '[object Undefined]';
/** Built-in value references. */

var symToStringTag = _Symbol ? _Symbol.toStringTag : undefined;
/**
 * The base implementation of `getTag` without fallbacks for buggy environments.
 *
 * @private
 * @param {*} value The value to query.
 * @returns {string} Returns the `toStringTag`.
 */

function baseGetTag(value) {
  if (value == null) {
    return value === undefined ? undefinedTag : nullTag;
  }

  return symToStringTag && symToStringTag in Object(value) ? getRawTag(value) : objectToString(value);
}

module.exports = baseGetTag;

/***/ }),

/***/ "../../node_modules/lodash/_baseHasIn.js":
/*!*************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_baseHasIn.js ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/**
 * The base implementation of `_.hasIn` without support for deep paths.
 *
 * @private
 * @param {Object} [object] The object to query.
 * @param {Array|string} key The key to check.
 * @returns {boolean} Returns `true` if `key` exists, else `false`.
 */
function baseHasIn(object, key) {
  return object != null && key in Object(object);
}

module.exports = baseHasIn;

/***/ }),

/***/ "../../node_modules/lodash/_baseIsArguments.js":
/*!*******************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_baseIsArguments.js ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var baseGetTag = __webpack_require__(/*! ./_baseGetTag */ "../../node_modules/lodash/_baseGetTag.js"),
    isObjectLike = __webpack_require__(/*! ./isObjectLike */ "../../node_modules/lodash/isObjectLike.js");
/** `Object#toString` result references. */


var argsTag = '[object Arguments]';
/**
 * The base implementation of `_.isArguments`.
 *
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is an `arguments` object,
 */

function baseIsArguments(value) {
  return isObjectLike(value) && baseGetTag(value) == argsTag;
}

module.exports = baseIsArguments;

/***/ }),

/***/ "../../node_modules/lodash/_baseIsEqual.js":
/*!***************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_baseIsEqual.js ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var baseIsEqualDeep = __webpack_require__(/*! ./_baseIsEqualDeep */ "../../node_modules/lodash/_baseIsEqualDeep.js"),
    isObjectLike = __webpack_require__(/*! ./isObjectLike */ "../../node_modules/lodash/isObjectLike.js");
/**
 * The base implementation of `_.isEqual` which supports partial comparisons
 * and tracks traversed objects.
 *
 * @private
 * @param {*} value The value to compare.
 * @param {*} other The other value to compare.
 * @param {boolean} bitmask The bitmask flags.
 *  1 - Unordered comparison
 *  2 - Partial comparison
 * @param {Function} [customizer] The function to customize comparisons.
 * @param {Object} [stack] Tracks traversed `value` and `other` objects.
 * @returns {boolean} Returns `true` if the values are equivalent, else `false`.
 */


function baseIsEqual(value, other, bitmask, customizer, stack) {
  if (value === other) {
    return true;
  }

  if (value == null || other == null || !isObjectLike(value) && !isObjectLike(other)) {
    return value !== value && other !== other;
  }

  return baseIsEqualDeep(value, other, bitmask, customizer, baseIsEqual, stack);
}

module.exports = baseIsEqual;

/***/ }),

/***/ "../../node_modules/lodash/_baseIsEqualDeep.js":
/*!*******************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_baseIsEqualDeep.js ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var Stack = __webpack_require__(/*! ./_Stack */ "../../node_modules/lodash/_Stack.js"),
    equalArrays = __webpack_require__(/*! ./_equalArrays */ "../../node_modules/lodash/_equalArrays.js"),
    equalByTag = __webpack_require__(/*! ./_equalByTag */ "../../node_modules/lodash/_equalByTag.js"),
    equalObjects = __webpack_require__(/*! ./_equalObjects */ "../../node_modules/lodash/_equalObjects.js"),
    getTag = __webpack_require__(/*! ./_getTag */ "../../node_modules/lodash/_getTag.js"),
    isArray = __webpack_require__(/*! ./isArray */ "../../node_modules/lodash/isArray.js"),
    isBuffer = __webpack_require__(/*! ./isBuffer */ "../../node_modules/lodash/isBuffer.js"),
    isTypedArray = __webpack_require__(/*! ./isTypedArray */ "../../node_modules/lodash/isTypedArray.js");
/** Used to compose bitmasks for value comparisons. */


var COMPARE_PARTIAL_FLAG = 1;
/** `Object#toString` result references. */

var argsTag = '[object Arguments]',
    arrayTag = '[object Array]',
    objectTag = '[object Object]';
/** Used for built-in method references. */

var objectProto = Object.prototype;
/** Used to check objects for own properties. */

var hasOwnProperty = objectProto.hasOwnProperty;
/**
 * A specialized version of `baseIsEqual` for arrays and objects which performs
 * deep comparisons and tracks traversed objects enabling objects with circular
 * references to be compared.
 *
 * @private
 * @param {Object} object The object to compare.
 * @param {Object} other The other object to compare.
 * @param {number} bitmask The bitmask flags. See `baseIsEqual` for more details.
 * @param {Function} customizer The function to customize comparisons.
 * @param {Function} equalFunc The function to determine equivalents of values.
 * @param {Object} [stack] Tracks traversed `object` and `other` objects.
 * @returns {boolean} Returns `true` if the objects are equivalent, else `false`.
 */

function baseIsEqualDeep(object, other, bitmask, customizer, equalFunc, stack) {
  var objIsArr = isArray(object),
      othIsArr = isArray(other),
      objTag = objIsArr ? arrayTag : getTag(object),
      othTag = othIsArr ? arrayTag : getTag(other);
  objTag = objTag == argsTag ? objectTag : objTag;
  othTag = othTag == argsTag ? objectTag : othTag;
  var objIsObj = objTag == objectTag,
      othIsObj = othTag == objectTag,
      isSameTag = objTag == othTag;

  if (isSameTag && isBuffer(object)) {
    if (!isBuffer(other)) {
      return false;
    }

    objIsArr = true;
    objIsObj = false;
  }

  if (isSameTag && !objIsObj) {
    stack || (stack = new Stack());
    return objIsArr || isTypedArray(object) ? equalArrays(object, other, bitmask, customizer, equalFunc, stack) : equalByTag(object, other, objTag, bitmask, customizer, equalFunc, stack);
  }

  if (!(bitmask & COMPARE_PARTIAL_FLAG)) {
    var objIsWrapped = objIsObj && hasOwnProperty.call(object, '__wrapped__'),
        othIsWrapped = othIsObj && hasOwnProperty.call(other, '__wrapped__');

    if (objIsWrapped || othIsWrapped) {
      var objUnwrapped = objIsWrapped ? object.value() : object,
          othUnwrapped = othIsWrapped ? other.value() : other;
      stack || (stack = new Stack());
      return equalFunc(objUnwrapped, othUnwrapped, bitmask, customizer, stack);
    }
  }

  if (!isSameTag) {
    return false;
  }

  stack || (stack = new Stack());
  return equalObjects(object, other, bitmask, customizer, equalFunc, stack);
}

module.exports = baseIsEqualDeep;

/***/ }),

/***/ "../../node_modules/lodash/_baseIsMatch.js":
/*!***************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_baseIsMatch.js ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var Stack = __webpack_require__(/*! ./_Stack */ "../../node_modules/lodash/_Stack.js"),
    baseIsEqual = __webpack_require__(/*! ./_baseIsEqual */ "../../node_modules/lodash/_baseIsEqual.js");
/** Used to compose bitmasks for value comparisons. */


var COMPARE_PARTIAL_FLAG = 1,
    COMPARE_UNORDERED_FLAG = 2;
/**
 * The base implementation of `_.isMatch` without support for iteratee shorthands.
 *
 * @private
 * @param {Object} object The object to inspect.
 * @param {Object} source The object of property values to match.
 * @param {Array} matchData The property names, values, and compare flags to match.
 * @param {Function} [customizer] The function to customize comparisons.
 * @returns {boolean} Returns `true` if `object` is a match, else `false`.
 */

function baseIsMatch(object, source, matchData, customizer) {
  var index = matchData.length,
      length = index,
      noCustomizer = !customizer;

  if (object == null) {
    return !length;
  }

  object = Object(object);

  while (index--) {
    var data = matchData[index];

    if (noCustomizer && data[2] ? data[1] !== object[data[0]] : !(data[0] in object)) {
      return false;
    }
  }

  while (++index < length) {
    data = matchData[index];
    var key = data[0],
        objValue = object[key],
        srcValue = data[1];

    if (noCustomizer && data[2]) {
      if (objValue === undefined && !(key in object)) {
        return false;
      }
    } else {
      var stack = new Stack();

      if (customizer) {
        var result = customizer(objValue, srcValue, key, object, source, stack);
      }

      if (!(result === undefined ? baseIsEqual(srcValue, objValue, COMPARE_PARTIAL_FLAG | COMPARE_UNORDERED_FLAG, customizer, stack) : result)) {
        return false;
      }
    }
  }

  return true;
}

module.exports = baseIsMatch;

/***/ }),

/***/ "../../node_modules/lodash/_baseIsNative.js":
/*!****************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_baseIsNative.js ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var isFunction = __webpack_require__(/*! ./isFunction */ "../../node_modules/lodash/isFunction.js"),
    isMasked = __webpack_require__(/*! ./_isMasked */ "../../node_modules/lodash/_isMasked.js"),
    isObject = __webpack_require__(/*! ./isObject */ "../../node_modules/lodash/isObject.js"),
    toSource = __webpack_require__(/*! ./_toSource */ "../../node_modules/lodash/_toSource.js");
/**
 * Used to match `RegExp`
 * [syntax characters](http://ecma-international.org/ecma-262/7.0/#sec-patterns).
 */


var reRegExpChar = /[\\^$.*+?()[\]{}|]/g;
/** Used to detect host constructors (Safari). */

var reIsHostCtor = /^\[object .+?Constructor\]$/;
/** Used for built-in method references. */

var funcProto = Function.prototype,
    objectProto = Object.prototype;
/** Used to resolve the decompiled source of functions. */

var funcToString = funcProto.toString;
/** Used to check objects for own properties. */

var hasOwnProperty = objectProto.hasOwnProperty;
/** Used to detect if a method is native. */

var reIsNative = RegExp('^' + funcToString.call(hasOwnProperty).replace(reRegExpChar, '\\$&').replace(/hasOwnProperty|(function).*?(?=\\\()| for .+?(?=\\\])/g, '$1.*?') + '$');
/**
 * The base implementation of `_.isNative` without bad shim checks.
 *
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a native function,
 *  else `false`.
 */

function baseIsNative(value) {
  if (!isObject(value) || isMasked(value)) {
    return false;
  }

  var pattern = isFunction(value) ? reIsNative : reIsHostCtor;
  return pattern.test(toSource(value));
}

module.exports = baseIsNative;

/***/ }),

/***/ "../../node_modules/lodash/_baseIsTypedArray.js":
/*!********************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_baseIsTypedArray.js ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var baseGetTag = __webpack_require__(/*! ./_baseGetTag */ "../../node_modules/lodash/_baseGetTag.js"),
    isLength = __webpack_require__(/*! ./isLength */ "../../node_modules/lodash/isLength.js"),
    isObjectLike = __webpack_require__(/*! ./isObjectLike */ "../../node_modules/lodash/isObjectLike.js");
/** `Object#toString` result references. */


var argsTag = '[object Arguments]',
    arrayTag = '[object Array]',
    boolTag = '[object Boolean]',
    dateTag = '[object Date]',
    errorTag = '[object Error]',
    funcTag = '[object Function]',
    mapTag = '[object Map]',
    numberTag = '[object Number]',
    objectTag = '[object Object]',
    regexpTag = '[object RegExp]',
    setTag = '[object Set]',
    stringTag = '[object String]',
    weakMapTag = '[object WeakMap]';
var arrayBufferTag = '[object ArrayBuffer]',
    dataViewTag = '[object DataView]',
    float32Tag = '[object Float32Array]',
    float64Tag = '[object Float64Array]',
    int8Tag = '[object Int8Array]',
    int16Tag = '[object Int16Array]',
    int32Tag = '[object Int32Array]',
    uint8Tag = '[object Uint8Array]',
    uint8ClampedTag = '[object Uint8ClampedArray]',
    uint16Tag = '[object Uint16Array]',
    uint32Tag = '[object Uint32Array]';
/** Used to identify `toStringTag` values of typed arrays. */

var typedArrayTags = {};
typedArrayTags[float32Tag] = typedArrayTags[float64Tag] = typedArrayTags[int8Tag] = typedArrayTags[int16Tag] = typedArrayTags[int32Tag] = typedArrayTags[uint8Tag] = typedArrayTags[uint8ClampedTag] = typedArrayTags[uint16Tag] = typedArrayTags[uint32Tag] = true;
typedArrayTags[argsTag] = typedArrayTags[arrayTag] = typedArrayTags[arrayBufferTag] = typedArrayTags[boolTag] = typedArrayTags[dataViewTag] = typedArrayTags[dateTag] = typedArrayTags[errorTag] = typedArrayTags[funcTag] = typedArrayTags[mapTag] = typedArrayTags[numberTag] = typedArrayTags[objectTag] = typedArrayTags[regexpTag] = typedArrayTags[setTag] = typedArrayTags[stringTag] = typedArrayTags[weakMapTag] = false;
/**
 * The base implementation of `_.isTypedArray` without Node.js optimizations.
 *
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a typed array, else `false`.
 */

function baseIsTypedArray(value) {
  return isObjectLike(value) && isLength(value.length) && !!typedArrayTags[baseGetTag(value)];
}

module.exports = baseIsTypedArray;

/***/ }),

/***/ "../../node_modules/lodash/_baseIteratee.js":
/*!****************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_baseIteratee.js ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var _typeof = __webpack_require__(/*! @babel/runtime/helpers/typeof */ "../../node_modules/@babel/runtime/helpers/typeof.js");

var baseMatches = __webpack_require__(/*! ./_baseMatches */ "../../node_modules/lodash/_baseMatches.js"),
    baseMatchesProperty = __webpack_require__(/*! ./_baseMatchesProperty */ "../../node_modules/lodash/_baseMatchesProperty.js"),
    identity = __webpack_require__(/*! ./identity */ "../../node_modules/lodash/identity.js"),
    isArray = __webpack_require__(/*! ./isArray */ "../../node_modules/lodash/isArray.js"),
    property = __webpack_require__(/*! ./property */ "../../node_modules/lodash/property.js");
/**
 * The base implementation of `_.iteratee`.
 *
 * @private
 * @param {*} [value=_.identity] The value to convert to an iteratee.
 * @returns {Function} Returns the iteratee.
 */


function baseIteratee(value) {
  // Don't store the `typeof` result in a variable to avoid a JIT bug in Safari 9.
  // See https://bugs.webkit.org/show_bug.cgi?id=156034 for more details.
  if (typeof value == 'function') {
    return value;
  }

  if (value == null) {
    return identity;
  }

  if (_typeof(value) == 'object') {
    return isArray(value) ? baseMatchesProperty(value[0], value[1]) : baseMatches(value);
  }

  return property(value);
}

module.exports = baseIteratee;

/***/ }),

/***/ "../../node_modules/lodash/_baseKeys.js":
/*!************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_baseKeys.js ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var isPrototype = __webpack_require__(/*! ./_isPrototype */ "../../node_modules/lodash/_isPrototype.js"),
    nativeKeys = __webpack_require__(/*! ./_nativeKeys */ "../../node_modules/lodash/_nativeKeys.js");
/** Used for built-in method references. */


var objectProto = Object.prototype;
/** Used to check objects for own properties. */

var hasOwnProperty = objectProto.hasOwnProperty;
/**
 * The base implementation of `_.keys` which doesn't treat sparse arrays as dense.
 *
 * @private
 * @param {Object} object The object to query.
 * @returns {Array} Returns the array of property names.
 */

function baseKeys(object) {
  if (!isPrototype(object)) {
    return nativeKeys(object);
  }

  var result = [];

  for (var key in Object(object)) {
    if (hasOwnProperty.call(object, key) && key != 'constructor') {
      result.push(key);
    }
  }

  return result;
}

module.exports = baseKeys;

/***/ }),

/***/ "../../node_modules/lodash/_baseMatches.js":
/*!***************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_baseMatches.js ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var baseIsMatch = __webpack_require__(/*! ./_baseIsMatch */ "../../node_modules/lodash/_baseIsMatch.js"),
    getMatchData = __webpack_require__(/*! ./_getMatchData */ "../../node_modules/lodash/_getMatchData.js"),
    matchesStrictComparable = __webpack_require__(/*! ./_matchesStrictComparable */ "../../node_modules/lodash/_matchesStrictComparable.js");
/**
 * The base implementation of `_.matches` which doesn't clone `source`.
 *
 * @private
 * @param {Object} source The object of property values to match.
 * @returns {Function} Returns the new spec function.
 */


function baseMatches(source) {
  var matchData = getMatchData(source);

  if (matchData.length == 1 && matchData[0][2]) {
    return matchesStrictComparable(matchData[0][0], matchData[0][1]);
  }

  return function (object) {
    return object === source || baseIsMatch(object, source, matchData);
  };
}

module.exports = baseMatches;

/***/ }),

/***/ "../../node_modules/lodash/_baseMatchesProperty.js":
/*!***********************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_baseMatchesProperty.js ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var baseIsEqual = __webpack_require__(/*! ./_baseIsEqual */ "../../node_modules/lodash/_baseIsEqual.js"),
    get = __webpack_require__(/*! ./get */ "../../node_modules/lodash/get.js"),
    hasIn = __webpack_require__(/*! ./hasIn */ "../../node_modules/lodash/hasIn.js"),
    isKey = __webpack_require__(/*! ./_isKey */ "../../node_modules/lodash/_isKey.js"),
    isStrictComparable = __webpack_require__(/*! ./_isStrictComparable */ "../../node_modules/lodash/_isStrictComparable.js"),
    matchesStrictComparable = __webpack_require__(/*! ./_matchesStrictComparable */ "../../node_modules/lodash/_matchesStrictComparable.js"),
    toKey = __webpack_require__(/*! ./_toKey */ "../../node_modules/lodash/_toKey.js");
/** Used to compose bitmasks for value comparisons. */


var COMPARE_PARTIAL_FLAG = 1,
    COMPARE_UNORDERED_FLAG = 2;
/**
 * The base implementation of `_.matchesProperty` which doesn't clone `srcValue`.
 *
 * @private
 * @param {string} path The path of the property to get.
 * @param {*} srcValue The value to match.
 * @returns {Function} Returns the new spec function.
 */

function baseMatchesProperty(path, srcValue) {
  if (isKey(path) && isStrictComparable(srcValue)) {
    return matchesStrictComparable(toKey(path), srcValue);
  }

  return function (object) {
    var objValue = get(object, path);
    return objValue === undefined && objValue === srcValue ? hasIn(object, path) : baseIsEqual(srcValue, objValue, COMPARE_PARTIAL_FLAG | COMPARE_UNORDERED_FLAG);
  };
}

module.exports = baseMatchesProperty;

/***/ }),

/***/ "../../node_modules/lodash/_baseProperty.js":
/*!****************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_baseProperty.js ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/**
 * The base implementation of `_.property` without support for deep paths.
 *
 * @private
 * @param {string} key The key of the property to get.
 * @returns {Function} Returns the new accessor function.
 */
function baseProperty(key) {
  return function (object) {
    return object == null ? undefined : object[key];
  };
}

module.exports = baseProperty;

/***/ }),

/***/ "../../node_modules/lodash/_basePropertyDeep.js":
/*!********************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_basePropertyDeep.js ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var baseGet = __webpack_require__(/*! ./_baseGet */ "../../node_modules/lodash/_baseGet.js");
/**
 * A specialized version of `baseProperty` which supports deep paths.
 *
 * @private
 * @param {Array|string} path The path of the property to get.
 * @returns {Function} Returns the new accessor function.
 */


function basePropertyDeep(path) {
  return function (object) {
    return baseGet(object, path);
  };
}

module.exports = basePropertyDeep;

/***/ }),

/***/ "../../node_modules/lodash/_baseTimes.js":
/*!*************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_baseTimes.js ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/**
 * The base implementation of `_.times` without support for iteratee shorthands
 * or max array length checks.
 *
 * @private
 * @param {number} n The number of times to invoke `iteratee`.
 * @param {Function} iteratee The function invoked per iteration.
 * @returns {Array} Returns the array of results.
 */
function baseTimes(n, iteratee) {
  var index = -1,
      result = Array(n);

  while (++index < n) {
    result[index] = iteratee(index);
  }

  return result;
}

module.exports = baseTimes;

/***/ }),

/***/ "../../node_modules/lodash/_baseToString.js":
/*!****************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_baseToString.js ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var _Symbol = __webpack_require__(/*! ./_Symbol */ "../../node_modules/lodash/_Symbol.js"),
    arrayMap = __webpack_require__(/*! ./_arrayMap */ "../../node_modules/lodash/_arrayMap.js"),
    isArray = __webpack_require__(/*! ./isArray */ "../../node_modules/lodash/isArray.js"),
    isSymbol = __webpack_require__(/*! ./isSymbol */ "../../node_modules/lodash/isSymbol.js");
/** Used as references for various `Number` constants. */


var INFINITY = 1 / 0;
/** Used to convert symbols to primitives and strings. */

var symbolProto = _Symbol ? _Symbol.prototype : undefined,
    symbolToString = symbolProto ? symbolProto.toString : undefined;
/**
 * The base implementation of `_.toString` which doesn't convert nullish
 * values to empty strings.
 *
 * @private
 * @param {*} value The value to process.
 * @returns {string} Returns the string.
 */

function baseToString(value) {
  // Exit early for strings to avoid a performance hit in some environments.
  if (typeof value == 'string') {
    return value;
  }

  if (isArray(value)) {
    // Recursively convert values (susceptible to call stack limits).
    return arrayMap(value, baseToString) + '';
  }

  if (isSymbol(value)) {
    return symbolToString ? symbolToString.call(value) : '';
  }

  var result = value + '';
  return result == '0' && 1 / value == -INFINITY ? '-0' : result;
}

module.exports = baseToString;

/***/ }),

/***/ "../../node_modules/lodash/_baseUnary.js":
/*!*************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_baseUnary.js ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/**
 * The base implementation of `_.unary` without support for storing metadata.
 *
 * @private
 * @param {Function} func The function to cap arguments for.
 * @returns {Function} Returns the new capped function.
 */
function baseUnary(func) {
  return function (value) {
    return func(value);
  };
}

module.exports = baseUnary;

/***/ }),

/***/ "../../node_modules/lodash/_cacheHas.js":
/*!************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_cacheHas.js ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/**
 * Checks if a `cache` value for `key` exists.
 *
 * @private
 * @param {Object} cache The cache to query.
 * @param {string} key The key of the entry to check.
 * @returns {boolean} Returns `true` if an entry for `key` exists, else `false`.
 */
function cacheHas(cache, key) {
  return cache.has(key);
}

module.exports = cacheHas;

/***/ }),

/***/ "../../node_modules/lodash/_castPath.js":
/*!************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_castPath.js ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var isArray = __webpack_require__(/*! ./isArray */ "../../node_modules/lodash/isArray.js"),
    isKey = __webpack_require__(/*! ./_isKey */ "../../node_modules/lodash/_isKey.js"),
    stringToPath = __webpack_require__(/*! ./_stringToPath */ "../../node_modules/lodash/_stringToPath.js"),
    toString = __webpack_require__(/*! ./toString */ "../../node_modules/lodash/toString.js");
/**
 * Casts `value` to a path array if it's not one.
 *
 * @private
 * @param {*} value The value to inspect.
 * @param {Object} [object] The object to query keys on.
 * @returns {Array} Returns the cast property path array.
 */


function castPath(value, object) {
  if (isArray(value)) {
    return value;
  }

  return isKey(value, object) ? [value] : stringToPath(toString(value));
}

module.exports = castPath;

/***/ }),

/***/ "../../node_modules/lodash/_coreJsData.js":
/*!**************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_coreJsData.js ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var root = __webpack_require__(/*! ./_root */ "../../node_modules/lodash/_root.js");
/** Used to detect overreaching core-js shims. */


var coreJsData = root['__core-js_shared__'];
module.exports = coreJsData;

/***/ }),

/***/ "../../node_modules/lodash/_createFind.js":
/*!**************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_createFind.js ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var baseIteratee = __webpack_require__(/*! ./_baseIteratee */ "../../node_modules/lodash/_baseIteratee.js"),
    isArrayLike = __webpack_require__(/*! ./isArrayLike */ "../../node_modules/lodash/isArrayLike.js"),
    keys = __webpack_require__(/*! ./keys */ "../../node_modules/lodash/keys.js");
/**
 * Creates a `_.find` or `_.findLast` function.
 *
 * @private
 * @param {Function} findIndexFunc The function to find the collection index.
 * @returns {Function} Returns the new find function.
 */


function createFind(findIndexFunc) {
  return function (collection, predicate, fromIndex) {
    var iterable = Object(collection);

    if (!isArrayLike(collection)) {
      var iteratee = baseIteratee(predicate, 3);
      collection = keys(collection);

      predicate = function predicate(key) {
        return iteratee(iterable[key], key, iterable);
      };
    }

    var index = findIndexFunc(collection, predicate, fromIndex);
    return index > -1 ? iterable[iteratee ? collection[index] : index] : undefined;
  };
}

module.exports = createFind;

/***/ }),

/***/ "../../node_modules/lodash/_equalArrays.js":
/*!***************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_equalArrays.js ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var SetCache = __webpack_require__(/*! ./_SetCache */ "../../node_modules/lodash/_SetCache.js"),
    arraySome = __webpack_require__(/*! ./_arraySome */ "../../node_modules/lodash/_arraySome.js"),
    cacheHas = __webpack_require__(/*! ./_cacheHas */ "../../node_modules/lodash/_cacheHas.js");
/** Used to compose bitmasks for value comparisons. */


var COMPARE_PARTIAL_FLAG = 1,
    COMPARE_UNORDERED_FLAG = 2;
/**
 * A specialized version of `baseIsEqualDeep` for arrays with support for
 * partial deep comparisons.
 *
 * @private
 * @param {Array} array The array to compare.
 * @param {Array} other The other array to compare.
 * @param {number} bitmask The bitmask flags. See `baseIsEqual` for more details.
 * @param {Function} customizer The function to customize comparisons.
 * @param {Function} equalFunc The function to determine equivalents of values.
 * @param {Object} stack Tracks traversed `array` and `other` objects.
 * @returns {boolean} Returns `true` if the arrays are equivalent, else `false`.
 */

function equalArrays(array, other, bitmask, customizer, equalFunc, stack) {
  var isPartial = bitmask & COMPARE_PARTIAL_FLAG,
      arrLength = array.length,
      othLength = other.length;

  if (arrLength != othLength && !(isPartial && othLength > arrLength)) {
    return false;
  } // Assume cyclic values are equal.


  var stacked = stack.get(array);

  if (stacked && stack.get(other)) {
    return stacked == other;
  }

  var index = -1,
      result = true,
      seen = bitmask & COMPARE_UNORDERED_FLAG ? new SetCache() : undefined;
  stack.set(array, other);
  stack.set(other, array); // Ignore non-index properties.

  while (++index < arrLength) {
    var arrValue = array[index],
        othValue = other[index];

    if (customizer) {
      var compared = isPartial ? customizer(othValue, arrValue, index, other, array, stack) : customizer(arrValue, othValue, index, array, other, stack);
    }

    if (compared !== undefined) {
      if (compared) {
        continue;
      }

      result = false;
      break;
    } // Recursively compare arrays (susceptible to call stack limits).


    if (seen) {
      if (!arraySome(other, function (othValue, othIndex) {
        if (!cacheHas(seen, othIndex) && (arrValue === othValue || equalFunc(arrValue, othValue, bitmask, customizer, stack))) {
          return seen.push(othIndex);
        }
      })) {
        result = false;
        break;
      }
    } else if (!(arrValue === othValue || equalFunc(arrValue, othValue, bitmask, customizer, stack))) {
      result = false;
      break;
    }
  }

  stack['delete'](array);
  stack['delete'](other);
  return result;
}

module.exports = equalArrays;

/***/ }),

/***/ "../../node_modules/lodash/_equalByTag.js":
/*!**************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_equalByTag.js ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var _Symbol = __webpack_require__(/*! ./_Symbol */ "../../node_modules/lodash/_Symbol.js"),
    Uint8Array = __webpack_require__(/*! ./_Uint8Array */ "../../node_modules/lodash/_Uint8Array.js"),
    eq = __webpack_require__(/*! ./eq */ "../../node_modules/lodash/eq.js"),
    equalArrays = __webpack_require__(/*! ./_equalArrays */ "../../node_modules/lodash/_equalArrays.js"),
    mapToArray = __webpack_require__(/*! ./_mapToArray */ "../../node_modules/lodash/_mapToArray.js"),
    setToArray = __webpack_require__(/*! ./_setToArray */ "../../node_modules/lodash/_setToArray.js");
/** Used to compose bitmasks for value comparisons. */


var COMPARE_PARTIAL_FLAG = 1,
    COMPARE_UNORDERED_FLAG = 2;
/** `Object#toString` result references. */

var boolTag = '[object Boolean]',
    dateTag = '[object Date]',
    errorTag = '[object Error]',
    mapTag = '[object Map]',
    numberTag = '[object Number]',
    regexpTag = '[object RegExp]',
    setTag = '[object Set]',
    stringTag = '[object String]',
    symbolTag = '[object Symbol]';
var arrayBufferTag = '[object ArrayBuffer]',
    dataViewTag = '[object DataView]';
/** Used to convert symbols to primitives and strings. */

var symbolProto = _Symbol ? _Symbol.prototype : undefined,
    symbolValueOf = symbolProto ? symbolProto.valueOf : undefined;
/**
 * A specialized version of `baseIsEqualDeep` for comparing objects of
 * the same `toStringTag`.
 *
 * **Note:** This function only supports comparing values with tags of
 * `Boolean`, `Date`, `Error`, `Number`, `RegExp`, or `String`.
 *
 * @private
 * @param {Object} object The object to compare.
 * @param {Object} other The other object to compare.
 * @param {string} tag The `toStringTag` of the objects to compare.
 * @param {number} bitmask The bitmask flags. See `baseIsEqual` for more details.
 * @param {Function} customizer The function to customize comparisons.
 * @param {Function} equalFunc The function to determine equivalents of values.
 * @param {Object} stack Tracks traversed `object` and `other` objects.
 * @returns {boolean} Returns `true` if the objects are equivalent, else `false`.
 */

function equalByTag(object, other, tag, bitmask, customizer, equalFunc, stack) {
  switch (tag) {
    case dataViewTag:
      if (object.byteLength != other.byteLength || object.byteOffset != other.byteOffset) {
        return false;
      }

      object = object.buffer;
      other = other.buffer;

    case arrayBufferTag:
      if (object.byteLength != other.byteLength || !equalFunc(new Uint8Array(object), new Uint8Array(other))) {
        return false;
      }

      return true;

    case boolTag:
    case dateTag:
    case numberTag:
      // Coerce booleans to `1` or `0` and dates to milliseconds.
      // Invalid dates are coerced to `NaN`.
      return eq(+object, +other);

    case errorTag:
      return object.name == other.name && object.message == other.message;

    case regexpTag:
    case stringTag:
      // Coerce regexes to strings and treat strings, primitives and objects,
      // as equal. See http://www.ecma-international.org/ecma-262/7.0/#sec-regexp.prototype.tostring
      // for more details.
      return object == other + '';

    case mapTag:
      var convert = mapToArray;

    case setTag:
      var isPartial = bitmask & COMPARE_PARTIAL_FLAG;
      convert || (convert = setToArray);

      if (object.size != other.size && !isPartial) {
        return false;
      } // Assume cyclic values are equal.


      var stacked = stack.get(object);

      if (stacked) {
        return stacked == other;
      }

      bitmask |= COMPARE_UNORDERED_FLAG; // Recursively compare objects (susceptible to call stack limits).

      stack.set(object, other);
      var result = equalArrays(convert(object), convert(other), bitmask, customizer, equalFunc, stack);
      stack['delete'](object);
      return result;

    case symbolTag:
      if (symbolValueOf) {
        return symbolValueOf.call(object) == symbolValueOf.call(other);
      }

  }

  return false;
}

module.exports = equalByTag;

/***/ }),

/***/ "../../node_modules/lodash/_equalObjects.js":
/*!****************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_equalObjects.js ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var getAllKeys = __webpack_require__(/*! ./_getAllKeys */ "../../node_modules/lodash/_getAllKeys.js");
/** Used to compose bitmasks for value comparisons. */


var COMPARE_PARTIAL_FLAG = 1;
/** Used for built-in method references. */

var objectProto = Object.prototype;
/** Used to check objects for own properties. */

var hasOwnProperty = objectProto.hasOwnProperty;
/**
 * A specialized version of `baseIsEqualDeep` for objects with support for
 * partial deep comparisons.
 *
 * @private
 * @param {Object} object The object to compare.
 * @param {Object} other The other object to compare.
 * @param {number} bitmask The bitmask flags. See `baseIsEqual` for more details.
 * @param {Function} customizer The function to customize comparisons.
 * @param {Function} equalFunc The function to determine equivalents of values.
 * @param {Object} stack Tracks traversed `object` and `other` objects.
 * @returns {boolean} Returns `true` if the objects are equivalent, else `false`.
 */

function equalObjects(object, other, bitmask, customizer, equalFunc, stack) {
  var isPartial = bitmask & COMPARE_PARTIAL_FLAG,
      objProps = getAllKeys(object),
      objLength = objProps.length,
      othProps = getAllKeys(other),
      othLength = othProps.length;

  if (objLength != othLength && !isPartial) {
    return false;
  }

  var index = objLength;

  while (index--) {
    var key = objProps[index];

    if (!(isPartial ? key in other : hasOwnProperty.call(other, key))) {
      return false;
    }
  } // Assume cyclic values are equal.


  var stacked = stack.get(object);

  if (stacked && stack.get(other)) {
    return stacked == other;
  }

  var result = true;
  stack.set(object, other);
  stack.set(other, object);
  var skipCtor = isPartial;

  while (++index < objLength) {
    key = objProps[index];
    var objValue = object[key],
        othValue = other[key];

    if (customizer) {
      var compared = isPartial ? customizer(othValue, objValue, key, other, object, stack) : customizer(objValue, othValue, key, object, other, stack);
    } // Recursively compare objects (susceptible to call stack limits).


    if (!(compared === undefined ? objValue === othValue || equalFunc(objValue, othValue, bitmask, customizer, stack) : compared)) {
      result = false;
      break;
    }

    skipCtor || (skipCtor = key == 'constructor');
  }

  if (result && !skipCtor) {
    var objCtor = object.constructor,
        othCtor = other.constructor; // Non `Object` object instances with different constructors are not equal.

    if (objCtor != othCtor && 'constructor' in object && 'constructor' in other && !(typeof objCtor == 'function' && objCtor instanceof objCtor && typeof othCtor == 'function' && othCtor instanceof othCtor)) {
      result = false;
    }
  }

  stack['delete'](object);
  stack['delete'](other);
  return result;
}

module.exports = equalObjects;

/***/ }),

/***/ "../../node_modules/lodash/_freeGlobal.js":
/*!**************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_freeGlobal.js ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {var _typeof = __webpack_require__(/*! @babel/runtime/helpers/typeof */ "../../node_modules/@babel/runtime/helpers/typeof.js");

/** Detect free variable `global` from Node.js. */
var freeGlobal = (typeof global === "undefined" ? "undefined" : _typeof(global)) == 'object' && global && global.Object === Object && global;
module.exports = freeGlobal;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../webpack/buildin/global.js */ "../../node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "../../node_modules/lodash/_getAllKeys.js":
/*!**************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_getAllKeys.js ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var baseGetAllKeys = __webpack_require__(/*! ./_baseGetAllKeys */ "../../node_modules/lodash/_baseGetAllKeys.js"),
    getSymbols = __webpack_require__(/*! ./_getSymbols */ "../../node_modules/lodash/_getSymbols.js"),
    keys = __webpack_require__(/*! ./keys */ "../../node_modules/lodash/keys.js");
/**
 * Creates an array of own enumerable property names and symbols of `object`.
 *
 * @private
 * @param {Object} object The object to query.
 * @returns {Array} Returns the array of property names and symbols.
 */


function getAllKeys(object) {
  return baseGetAllKeys(object, keys, getSymbols);
}

module.exports = getAllKeys;

/***/ }),

/***/ "../../node_modules/lodash/_getMapData.js":
/*!**************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_getMapData.js ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var isKeyable = __webpack_require__(/*! ./_isKeyable */ "../../node_modules/lodash/_isKeyable.js");
/**
 * Gets the data for `map`.
 *
 * @private
 * @param {Object} map The map to query.
 * @param {string} key The reference key.
 * @returns {*} Returns the map data.
 */


function getMapData(map, key) {
  var data = map.__data__;
  return isKeyable(key) ? data[typeof key == 'string' ? 'string' : 'hash'] : data.map;
}

module.exports = getMapData;

/***/ }),

/***/ "../../node_modules/lodash/_getMatchData.js":
/*!****************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_getMatchData.js ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var isStrictComparable = __webpack_require__(/*! ./_isStrictComparable */ "../../node_modules/lodash/_isStrictComparable.js"),
    keys = __webpack_require__(/*! ./keys */ "../../node_modules/lodash/keys.js");
/**
 * Gets the property names, values, and compare flags of `object`.
 *
 * @private
 * @param {Object} object The object to query.
 * @returns {Array} Returns the match data of `object`.
 */


function getMatchData(object) {
  var result = keys(object),
      length = result.length;

  while (length--) {
    var key = result[length],
        value = object[key];
    result[length] = [key, value, isStrictComparable(value)];
  }

  return result;
}

module.exports = getMatchData;

/***/ }),

/***/ "../../node_modules/lodash/_getNative.js":
/*!*************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_getNative.js ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var baseIsNative = __webpack_require__(/*! ./_baseIsNative */ "../../node_modules/lodash/_baseIsNative.js"),
    getValue = __webpack_require__(/*! ./_getValue */ "../../node_modules/lodash/_getValue.js");
/**
 * Gets the native function at `key` of `object`.
 *
 * @private
 * @param {Object} object The object to query.
 * @param {string} key The key of the method to get.
 * @returns {*} Returns the function if it's native, else `undefined`.
 */


function getNative(object, key) {
  var value = getValue(object, key);
  return baseIsNative(value) ? value : undefined;
}

module.exports = getNative;

/***/ }),

/***/ "../../node_modules/lodash/_getRawTag.js":
/*!*************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_getRawTag.js ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var _Symbol = __webpack_require__(/*! ./_Symbol */ "../../node_modules/lodash/_Symbol.js");
/** Used for built-in method references. */


var objectProto = Object.prototype;
/** Used to check objects for own properties. */

var hasOwnProperty = objectProto.hasOwnProperty;
/**
 * Used to resolve the
 * [`toStringTag`](http://ecma-international.org/ecma-262/7.0/#sec-object.prototype.tostring)
 * of values.
 */

var nativeObjectToString = objectProto.toString;
/** Built-in value references. */

var symToStringTag = _Symbol ? _Symbol.toStringTag : undefined;
/**
 * A specialized version of `baseGetTag` which ignores `Symbol.toStringTag` values.
 *
 * @private
 * @param {*} value The value to query.
 * @returns {string} Returns the raw `toStringTag`.
 */

function getRawTag(value) {
  var isOwn = hasOwnProperty.call(value, symToStringTag),
      tag = value[symToStringTag];

  try {
    value[symToStringTag] = undefined;
    var unmasked = true;
  } catch (e) {}

  var result = nativeObjectToString.call(value);

  if (unmasked) {
    if (isOwn) {
      value[symToStringTag] = tag;
    } else {
      delete value[symToStringTag];
    }
  }

  return result;
}

module.exports = getRawTag;

/***/ }),

/***/ "../../node_modules/lodash/_getSymbols.js":
/*!**************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_getSymbols.js ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var arrayFilter = __webpack_require__(/*! ./_arrayFilter */ "../../node_modules/lodash/_arrayFilter.js"),
    stubArray = __webpack_require__(/*! ./stubArray */ "../../node_modules/lodash/stubArray.js");
/** Used for built-in method references. */


var objectProto = Object.prototype;
/** Built-in value references. */

var propertyIsEnumerable = objectProto.propertyIsEnumerable;
/* Built-in method references for those with the same name as other `lodash` methods. */

var nativeGetSymbols = Object.getOwnPropertySymbols;
/**
 * Creates an array of the own enumerable symbols of `object`.
 *
 * @private
 * @param {Object} object The object to query.
 * @returns {Array} Returns the array of symbols.
 */

var getSymbols = !nativeGetSymbols ? stubArray : function (object) {
  if (object == null) {
    return [];
  }

  object = Object(object);
  return arrayFilter(nativeGetSymbols(object), function (symbol) {
    return propertyIsEnumerable.call(object, symbol);
  });
};
module.exports = getSymbols;

/***/ }),

/***/ "../../node_modules/lodash/_getTag.js":
/*!**********************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_getTag.js ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var DataView = __webpack_require__(/*! ./_DataView */ "../../node_modules/lodash/_DataView.js"),
    Map = __webpack_require__(/*! ./_Map */ "../../node_modules/lodash/_Map.js"),
    Promise = __webpack_require__(/*! ./_Promise */ "../../node_modules/lodash/_Promise.js"),
    Set = __webpack_require__(/*! ./_Set */ "../../node_modules/lodash/_Set.js"),
    WeakMap = __webpack_require__(/*! ./_WeakMap */ "../../node_modules/lodash/_WeakMap.js"),
    baseGetTag = __webpack_require__(/*! ./_baseGetTag */ "../../node_modules/lodash/_baseGetTag.js"),
    toSource = __webpack_require__(/*! ./_toSource */ "../../node_modules/lodash/_toSource.js");
/** `Object#toString` result references. */


var mapTag = '[object Map]',
    objectTag = '[object Object]',
    promiseTag = '[object Promise]',
    setTag = '[object Set]',
    weakMapTag = '[object WeakMap]';
var dataViewTag = '[object DataView]';
/** Used to detect maps, sets, and weakmaps. */

var dataViewCtorString = toSource(DataView),
    mapCtorString = toSource(Map),
    promiseCtorString = toSource(Promise),
    setCtorString = toSource(Set),
    weakMapCtorString = toSource(WeakMap);
/**
 * Gets the `toStringTag` of `value`.
 *
 * @private
 * @param {*} value The value to query.
 * @returns {string} Returns the `toStringTag`.
 */

var getTag = baseGetTag; // Fallback for data views, maps, sets, and weak maps in IE 11 and promises in Node.js < 6.

if (DataView && getTag(new DataView(new ArrayBuffer(1))) != dataViewTag || Map && getTag(new Map()) != mapTag || Promise && getTag(Promise.resolve()) != promiseTag || Set && getTag(new Set()) != setTag || WeakMap && getTag(new WeakMap()) != weakMapTag) {
  getTag = function getTag(value) {
    var result = baseGetTag(value),
        Ctor = result == objectTag ? value.constructor : undefined,
        ctorString = Ctor ? toSource(Ctor) : '';

    if (ctorString) {
      switch (ctorString) {
        case dataViewCtorString:
          return dataViewTag;

        case mapCtorString:
          return mapTag;

        case promiseCtorString:
          return promiseTag;

        case setCtorString:
          return setTag;

        case weakMapCtorString:
          return weakMapTag;
      }
    }

    return result;
  };
}

module.exports = getTag;

/***/ }),

/***/ "../../node_modules/lodash/_getValue.js":
/*!************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_getValue.js ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/**
 * Gets the value at `key` of `object`.
 *
 * @private
 * @param {Object} [object] The object to query.
 * @param {string} key The key of the property to get.
 * @returns {*} Returns the property value.
 */
function getValue(object, key) {
  return object == null ? undefined : object[key];
}

module.exports = getValue;

/***/ }),

/***/ "../../node_modules/lodash/_hasPath.js":
/*!***********************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_hasPath.js ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var castPath = __webpack_require__(/*! ./_castPath */ "../../node_modules/lodash/_castPath.js"),
    isArguments = __webpack_require__(/*! ./isArguments */ "../../node_modules/lodash/isArguments.js"),
    isArray = __webpack_require__(/*! ./isArray */ "../../node_modules/lodash/isArray.js"),
    isIndex = __webpack_require__(/*! ./_isIndex */ "../../node_modules/lodash/_isIndex.js"),
    isLength = __webpack_require__(/*! ./isLength */ "../../node_modules/lodash/isLength.js"),
    toKey = __webpack_require__(/*! ./_toKey */ "../../node_modules/lodash/_toKey.js");
/**
 * Checks if `path` exists on `object`.
 *
 * @private
 * @param {Object} object The object to query.
 * @param {Array|string} path The path to check.
 * @param {Function} hasFunc The function to check properties.
 * @returns {boolean} Returns `true` if `path` exists, else `false`.
 */


function hasPath(object, path, hasFunc) {
  path = castPath(path, object);
  var index = -1,
      length = path.length,
      result = false;

  while (++index < length) {
    var key = toKey(path[index]);

    if (!(result = object != null && hasFunc(object, key))) {
      break;
    }

    object = object[key];
  }

  if (result || ++index != length) {
    return result;
  }

  length = object == null ? 0 : object.length;
  return !!length && isLength(length) && isIndex(key, length) && (isArray(object) || isArguments(object));
}

module.exports = hasPath;

/***/ }),

/***/ "../../node_modules/lodash/_hashClear.js":
/*!*************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_hashClear.js ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var nativeCreate = __webpack_require__(/*! ./_nativeCreate */ "../../node_modules/lodash/_nativeCreate.js");
/**
 * Removes all key-value entries from the hash.
 *
 * @private
 * @name clear
 * @memberOf Hash
 */


function hashClear() {
  this.__data__ = nativeCreate ? nativeCreate(null) : {};
  this.size = 0;
}

module.exports = hashClear;

/***/ }),

/***/ "../../node_modules/lodash/_hashDelete.js":
/*!**************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_hashDelete.js ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/**
 * Removes `key` and its value from the hash.
 *
 * @private
 * @name delete
 * @memberOf Hash
 * @param {Object} hash The hash to modify.
 * @param {string} key The key of the value to remove.
 * @returns {boolean} Returns `true` if the entry was removed, else `false`.
 */
function hashDelete(key) {
  var result = this.has(key) && delete this.__data__[key];
  this.size -= result ? 1 : 0;
  return result;
}

module.exports = hashDelete;

/***/ }),

/***/ "../../node_modules/lodash/_hashGet.js":
/*!***********************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_hashGet.js ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var nativeCreate = __webpack_require__(/*! ./_nativeCreate */ "../../node_modules/lodash/_nativeCreate.js");
/** Used to stand-in for `undefined` hash values. */


var HASH_UNDEFINED = '__lodash_hash_undefined__';
/** Used for built-in method references. */

var objectProto = Object.prototype;
/** Used to check objects for own properties. */

var hasOwnProperty = objectProto.hasOwnProperty;
/**
 * Gets the hash value for `key`.
 *
 * @private
 * @name get
 * @memberOf Hash
 * @param {string} key The key of the value to get.
 * @returns {*} Returns the entry value.
 */

function hashGet(key) {
  var data = this.__data__;

  if (nativeCreate) {
    var result = data[key];
    return result === HASH_UNDEFINED ? undefined : result;
  }

  return hasOwnProperty.call(data, key) ? data[key] : undefined;
}

module.exports = hashGet;

/***/ }),

/***/ "../../node_modules/lodash/_hashHas.js":
/*!***********************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_hashHas.js ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var nativeCreate = __webpack_require__(/*! ./_nativeCreate */ "../../node_modules/lodash/_nativeCreate.js");
/** Used for built-in method references. */


var objectProto = Object.prototype;
/** Used to check objects for own properties. */

var hasOwnProperty = objectProto.hasOwnProperty;
/**
 * Checks if a hash value for `key` exists.
 *
 * @private
 * @name has
 * @memberOf Hash
 * @param {string} key The key of the entry to check.
 * @returns {boolean} Returns `true` if an entry for `key` exists, else `false`.
 */

function hashHas(key) {
  var data = this.__data__;
  return nativeCreate ? data[key] !== undefined : hasOwnProperty.call(data, key);
}

module.exports = hashHas;

/***/ }),

/***/ "../../node_modules/lodash/_hashSet.js":
/*!***********************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_hashSet.js ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var nativeCreate = __webpack_require__(/*! ./_nativeCreate */ "../../node_modules/lodash/_nativeCreate.js");
/** Used to stand-in for `undefined` hash values. */


var HASH_UNDEFINED = '__lodash_hash_undefined__';
/**
 * Sets the hash `key` to `value`.
 *
 * @private
 * @name set
 * @memberOf Hash
 * @param {string} key The key of the value to set.
 * @param {*} value The value to set.
 * @returns {Object} Returns the hash instance.
 */

function hashSet(key, value) {
  var data = this.__data__;
  this.size += this.has(key) ? 0 : 1;
  data[key] = nativeCreate && value === undefined ? HASH_UNDEFINED : value;
  return this;
}

module.exports = hashSet;

/***/ }),

/***/ "../../node_modules/lodash/_isIndex.js":
/*!***********************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_isIndex.js ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var _typeof = __webpack_require__(/*! @babel/runtime/helpers/typeof */ "../../node_modules/@babel/runtime/helpers/typeof.js");

/** Used as references for various `Number` constants. */
var MAX_SAFE_INTEGER = 9007199254740991;
/** Used to detect unsigned integer values. */

var reIsUint = /^(?:0|[1-9]\d*)$/;
/**
 * Checks if `value` is a valid array-like index.
 *
 * @private
 * @param {*} value The value to check.
 * @param {number} [length=MAX_SAFE_INTEGER] The upper bounds of a valid index.
 * @returns {boolean} Returns `true` if `value` is a valid index, else `false`.
 */

function isIndex(value, length) {
  var type = _typeof(value);

  length = length == null ? MAX_SAFE_INTEGER : length;
  return !!length && (type == 'number' || type != 'symbol' && reIsUint.test(value)) && value > -1 && value % 1 == 0 && value < length;
}

module.exports = isIndex;

/***/ }),

/***/ "../../node_modules/lodash/_isKey.js":
/*!*********************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_isKey.js ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var _typeof = __webpack_require__(/*! @babel/runtime/helpers/typeof */ "../../node_modules/@babel/runtime/helpers/typeof.js");

var isArray = __webpack_require__(/*! ./isArray */ "../../node_modules/lodash/isArray.js"),
    isSymbol = __webpack_require__(/*! ./isSymbol */ "../../node_modules/lodash/isSymbol.js");
/** Used to match property names within property paths. */


var reIsDeepProp = /\.|\[(?:[^[\]]*|(["'])(?:(?!\1)[^\\]|\\.)*?\1)\]/,
    reIsPlainProp = /^\w*$/;
/**
 * Checks if `value` is a property name and not a property path.
 *
 * @private
 * @param {*} value The value to check.
 * @param {Object} [object] The object to query keys on.
 * @returns {boolean} Returns `true` if `value` is a property name, else `false`.
 */

function isKey(value, object) {
  if (isArray(value)) {
    return false;
  }

  var type = _typeof(value);

  if (type == 'number' || type == 'symbol' || type == 'boolean' || value == null || isSymbol(value)) {
    return true;
  }

  return reIsPlainProp.test(value) || !reIsDeepProp.test(value) || object != null && value in Object(object);
}

module.exports = isKey;

/***/ }),

/***/ "../../node_modules/lodash/_isKeyable.js":
/*!*************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_isKeyable.js ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var _typeof = __webpack_require__(/*! @babel/runtime/helpers/typeof */ "../../node_modules/@babel/runtime/helpers/typeof.js");

/**
 * Checks if `value` is suitable for use as unique object key.
 *
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is suitable, else `false`.
 */
function isKeyable(value) {
  var type = _typeof(value);

  return type == 'string' || type == 'number' || type == 'symbol' || type == 'boolean' ? value !== '__proto__' : value === null;
}

module.exports = isKeyable;

/***/ }),

/***/ "../../node_modules/lodash/_isMasked.js":
/*!************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_isMasked.js ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var coreJsData = __webpack_require__(/*! ./_coreJsData */ "../../node_modules/lodash/_coreJsData.js");
/** Used to detect methods masquerading as native. */


var maskSrcKey = function () {
  var uid = /[^.]+$/.exec(coreJsData && coreJsData.keys && coreJsData.keys.IE_PROTO || '');
  return uid ? 'Symbol(src)_1.' + uid : '';
}();
/**
 * Checks if `func` has its source masked.
 *
 * @private
 * @param {Function} func The function to check.
 * @returns {boolean} Returns `true` if `func` is masked, else `false`.
 */


function isMasked(func) {
  return !!maskSrcKey && maskSrcKey in func;
}

module.exports = isMasked;

/***/ }),

/***/ "../../node_modules/lodash/_isPrototype.js":
/*!***************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_isPrototype.js ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/** Used for built-in method references. */
var objectProto = Object.prototype;
/**
 * Checks if `value` is likely a prototype object.
 *
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a prototype, else `false`.
 */

function isPrototype(value) {
  var Ctor = value && value.constructor,
      proto = typeof Ctor == 'function' && Ctor.prototype || objectProto;
  return value === proto;
}

module.exports = isPrototype;

/***/ }),

/***/ "../../node_modules/lodash/_isStrictComparable.js":
/*!**********************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_isStrictComparable.js ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var isObject = __webpack_require__(/*! ./isObject */ "../../node_modules/lodash/isObject.js");
/**
 * Checks if `value` is suitable for strict equality comparisons, i.e. `===`.
 *
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` if suitable for strict
 *  equality comparisons, else `false`.
 */


function isStrictComparable(value) {
  return value === value && !isObject(value);
}

module.exports = isStrictComparable;

/***/ }),

/***/ "../../node_modules/lodash/_listCacheClear.js":
/*!******************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_listCacheClear.js ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/**
 * Removes all key-value entries from the list cache.
 *
 * @private
 * @name clear
 * @memberOf ListCache
 */
function listCacheClear() {
  this.__data__ = [];
  this.size = 0;
}

module.exports = listCacheClear;

/***/ }),

/***/ "../../node_modules/lodash/_listCacheDelete.js":
/*!*******************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_listCacheDelete.js ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var assocIndexOf = __webpack_require__(/*! ./_assocIndexOf */ "../../node_modules/lodash/_assocIndexOf.js");
/** Used for built-in method references. */


var arrayProto = Array.prototype;
/** Built-in value references. */

var splice = arrayProto.splice;
/**
 * Removes `key` and its value from the list cache.
 *
 * @private
 * @name delete
 * @memberOf ListCache
 * @param {string} key The key of the value to remove.
 * @returns {boolean} Returns `true` if the entry was removed, else `false`.
 */

function listCacheDelete(key) {
  var data = this.__data__,
      index = assocIndexOf(data, key);

  if (index < 0) {
    return false;
  }

  var lastIndex = data.length - 1;

  if (index == lastIndex) {
    data.pop();
  } else {
    splice.call(data, index, 1);
  }

  --this.size;
  return true;
}

module.exports = listCacheDelete;

/***/ }),

/***/ "../../node_modules/lodash/_listCacheGet.js":
/*!****************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_listCacheGet.js ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var assocIndexOf = __webpack_require__(/*! ./_assocIndexOf */ "../../node_modules/lodash/_assocIndexOf.js");
/**
 * Gets the list cache value for `key`.
 *
 * @private
 * @name get
 * @memberOf ListCache
 * @param {string} key The key of the value to get.
 * @returns {*} Returns the entry value.
 */


function listCacheGet(key) {
  var data = this.__data__,
      index = assocIndexOf(data, key);
  return index < 0 ? undefined : data[index][1];
}

module.exports = listCacheGet;

/***/ }),

/***/ "../../node_modules/lodash/_listCacheHas.js":
/*!****************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_listCacheHas.js ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var assocIndexOf = __webpack_require__(/*! ./_assocIndexOf */ "../../node_modules/lodash/_assocIndexOf.js");
/**
 * Checks if a list cache value for `key` exists.
 *
 * @private
 * @name has
 * @memberOf ListCache
 * @param {string} key The key of the entry to check.
 * @returns {boolean} Returns `true` if an entry for `key` exists, else `false`.
 */


function listCacheHas(key) {
  return assocIndexOf(this.__data__, key) > -1;
}

module.exports = listCacheHas;

/***/ }),

/***/ "../../node_modules/lodash/_listCacheSet.js":
/*!****************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_listCacheSet.js ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var assocIndexOf = __webpack_require__(/*! ./_assocIndexOf */ "../../node_modules/lodash/_assocIndexOf.js");
/**
 * Sets the list cache `key` to `value`.
 *
 * @private
 * @name set
 * @memberOf ListCache
 * @param {string} key The key of the value to set.
 * @param {*} value The value to set.
 * @returns {Object} Returns the list cache instance.
 */


function listCacheSet(key, value) {
  var data = this.__data__,
      index = assocIndexOf(data, key);

  if (index < 0) {
    ++this.size;
    data.push([key, value]);
  } else {
    data[index][1] = value;
  }

  return this;
}

module.exports = listCacheSet;

/***/ }),

/***/ "../../node_modules/lodash/_mapCacheClear.js":
/*!*****************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_mapCacheClear.js ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var Hash = __webpack_require__(/*! ./_Hash */ "../../node_modules/lodash/_Hash.js"),
    ListCache = __webpack_require__(/*! ./_ListCache */ "../../node_modules/lodash/_ListCache.js"),
    Map = __webpack_require__(/*! ./_Map */ "../../node_modules/lodash/_Map.js");
/**
 * Removes all key-value entries from the map.
 *
 * @private
 * @name clear
 * @memberOf MapCache
 */


function mapCacheClear() {
  this.size = 0;
  this.__data__ = {
    'hash': new Hash(),
    'map': new (Map || ListCache)(),
    'string': new Hash()
  };
}

module.exports = mapCacheClear;

/***/ }),

/***/ "../../node_modules/lodash/_mapCacheDelete.js":
/*!******************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_mapCacheDelete.js ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var getMapData = __webpack_require__(/*! ./_getMapData */ "../../node_modules/lodash/_getMapData.js");
/**
 * Removes `key` and its value from the map.
 *
 * @private
 * @name delete
 * @memberOf MapCache
 * @param {string} key The key of the value to remove.
 * @returns {boolean} Returns `true` if the entry was removed, else `false`.
 */


function mapCacheDelete(key) {
  var result = getMapData(this, key)['delete'](key);
  this.size -= result ? 1 : 0;
  return result;
}

module.exports = mapCacheDelete;

/***/ }),

/***/ "../../node_modules/lodash/_mapCacheGet.js":
/*!***************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_mapCacheGet.js ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var getMapData = __webpack_require__(/*! ./_getMapData */ "../../node_modules/lodash/_getMapData.js");
/**
 * Gets the map value for `key`.
 *
 * @private
 * @name get
 * @memberOf MapCache
 * @param {string} key The key of the value to get.
 * @returns {*} Returns the entry value.
 */


function mapCacheGet(key) {
  return getMapData(this, key).get(key);
}

module.exports = mapCacheGet;

/***/ }),

/***/ "../../node_modules/lodash/_mapCacheHas.js":
/*!***************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_mapCacheHas.js ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var getMapData = __webpack_require__(/*! ./_getMapData */ "../../node_modules/lodash/_getMapData.js");
/**
 * Checks if a map value for `key` exists.
 *
 * @private
 * @name has
 * @memberOf MapCache
 * @param {string} key The key of the entry to check.
 * @returns {boolean} Returns `true` if an entry for `key` exists, else `false`.
 */


function mapCacheHas(key) {
  return getMapData(this, key).has(key);
}

module.exports = mapCacheHas;

/***/ }),

/***/ "../../node_modules/lodash/_mapCacheSet.js":
/*!***************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_mapCacheSet.js ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var getMapData = __webpack_require__(/*! ./_getMapData */ "../../node_modules/lodash/_getMapData.js");
/**
 * Sets the map `key` to `value`.
 *
 * @private
 * @name set
 * @memberOf MapCache
 * @param {string} key The key of the value to set.
 * @param {*} value The value to set.
 * @returns {Object} Returns the map cache instance.
 */


function mapCacheSet(key, value) {
  var data = getMapData(this, key),
      size = data.size;
  data.set(key, value);
  this.size += data.size == size ? 0 : 1;
  return this;
}

module.exports = mapCacheSet;

/***/ }),

/***/ "../../node_modules/lodash/_mapToArray.js":
/*!**************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_mapToArray.js ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/**
 * Converts `map` to its key-value pairs.
 *
 * @private
 * @param {Object} map The map to convert.
 * @returns {Array} Returns the key-value pairs.
 */
function mapToArray(map) {
  var index = -1,
      result = Array(map.size);
  map.forEach(function (value, key) {
    result[++index] = [key, value];
  });
  return result;
}

module.exports = mapToArray;

/***/ }),

/***/ "../../node_modules/lodash/_matchesStrictComparable.js":
/*!***************************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_matchesStrictComparable.js ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/**
 * A specialized version of `matchesProperty` for source values suitable
 * for strict equality comparisons, i.e. `===`.
 *
 * @private
 * @param {string} key The key of the property to get.
 * @param {*} srcValue The value to match.
 * @returns {Function} Returns the new spec function.
 */
function matchesStrictComparable(key, srcValue) {
  return function (object) {
    if (object == null) {
      return false;
    }

    return object[key] === srcValue && (srcValue !== undefined || key in Object(object));
  };
}

module.exports = matchesStrictComparable;

/***/ }),

/***/ "../../node_modules/lodash/_memoizeCapped.js":
/*!*****************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_memoizeCapped.js ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var memoize = __webpack_require__(/*! ./memoize */ "../../node_modules/lodash/memoize.js");
/** Used as the maximum memoize cache size. */


var MAX_MEMOIZE_SIZE = 500;
/**
 * A specialized version of `_.memoize` which clears the memoized function's
 * cache when it exceeds `MAX_MEMOIZE_SIZE`.
 *
 * @private
 * @param {Function} func The function to have its output memoized.
 * @returns {Function} Returns the new memoized function.
 */

function memoizeCapped(func) {
  var result = memoize(func, function (key) {
    if (cache.size === MAX_MEMOIZE_SIZE) {
      cache.clear();
    }

    return key;
  });
  var cache = result.cache;
  return result;
}

module.exports = memoizeCapped;

/***/ }),

/***/ "../../node_modules/lodash/_nativeCreate.js":
/*!****************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_nativeCreate.js ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var getNative = __webpack_require__(/*! ./_getNative */ "../../node_modules/lodash/_getNative.js");
/* Built-in method references that are verified to be native. */


var nativeCreate = getNative(Object, 'create');
module.exports = nativeCreate;

/***/ }),

/***/ "../../node_modules/lodash/_nativeKeys.js":
/*!**************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_nativeKeys.js ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var overArg = __webpack_require__(/*! ./_overArg */ "../../node_modules/lodash/_overArg.js");
/* Built-in method references for those with the same name as other `lodash` methods. */


var nativeKeys = overArg(Object.keys, Object);
module.exports = nativeKeys;

/***/ }),

/***/ "../../node_modules/lodash/_nodeUtil.js":
/*!************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_nodeUtil.js ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(module) {var _typeof = __webpack_require__(/*! @babel/runtime/helpers/typeof */ "../../node_modules/@babel/runtime/helpers/typeof.js");

var freeGlobal = __webpack_require__(/*! ./_freeGlobal */ "../../node_modules/lodash/_freeGlobal.js");
/** Detect free variable `exports`. */


var freeExports = ( false ? undefined : _typeof(exports)) == 'object' && exports && !exports.nodeType && exports;
/** Detect free variable `module`. */

var freeModule = freeExports && ( false ? undefined : _typeof(module)) == 'object' && module && !module.nodeType && module;
/** Detect the popular CommonJS extension `module.exports`. */

var moduleExports = freeModule && freeModule.exports === freeExports;
/** Detect free variable `process` from Node.js. */

var freeProcess = moduleExports && freeGlobal.process;
/** Used to access faster Node.js helpers. */

var nodeUtil = function () {
  try {
    // Use `util.types` for Node.js 10+.
    var types = freeModule && freeModule.require && freeModule.require('util').types;

    if (types) {
      return types;
    } // Legacy `process.binding('util')` for Node.js < 10.


    return freeProcess && freeProcess.binding && freeProcess.binding('util');
  } catch (e) {}
}();

module.exports = nodeUtil;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../webpack/buildin/module.js */ "../../node_modules/webpack/buildin/module.js")(module)))

/***/ }),

/***/ "../../node_modules/lodash/_objectToString.js":
/*!******************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_objectToString.js ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/** Used for built-in method references. */
var objectProto = Object.prototype;
/**
 * Used to resolve the
 * [`toStringTag`](http://ecma-international.org/ecma-262/7.0/#sec-object.prototype.tostring)
 * of values.
 */

var nativeObjectToString = objectProto.toString;
/**
 * Converts `value` to a string using `Object.prototype.toString`.
 *
 * @private
 * @param {*} value The value to convert.
 * @returns {string} Returns the converted string.
 */

function objectToString(value) {
  return nativeObjectToString.call(value);
}

module.exports = objectToString;

/***/ }),

/***/ "../../node_modules/lodash/_overArg.js":
/*!***********************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_overArg.js ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/**
 * Creates a unary function that invokes `func` with its argument transformed.
 *
 * @private
 * @param {Function} func The function to wrap.
 * @param {Function} transform The argument transform.
 * @returns {Function} Returns the new function.
 */
function overArg(func, transform) {
  return function (arg) {
    return func(transform(arg));
  };
}

module.exports = overArg;

/***/ }),

/***/ "../../node_modules/lodash/_root.js":
/*!********************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_root.js ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var _typeof = __webpack_require__(/*! @babel/runtime/helpers/typeof */ "../../node_modules/@babel/runtime/helpers/typeof.js");

var freeGlobal = __webpack_require__(/*! ./_freeGlobal */ "../../node_modules/lodash/_freeGlobal.js");
/** Detect free variable `self`. */


var freeSelf = (typeof self === "undefined" ? "undefined" : _typeof(self)) == 'object' && self && self.Object === Object && self;
/** Used as a reference to the global object. */

var root = freeGlobal || freeSelf || Function('return this')();
module.exports = root;

/***/ }),

/***/ "../../node_modules/lodash/_setCacheAdd.js":
/*!***************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_setCacheAdd.js ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/** Used to stand-in for `undefined` hash values. */
var HASH_UNDEFINED = '__lodash_hash_undefined__';
/**
 * Adds `value` to the array cache.
 *
 * @private
 * @name add
 * @memberOf SetCache
 * @alias push
 * @param {*} value The value to cache.
 * @returns {Object} Returns the cache instance.
 */

function setCacheAdd(value) {
  this.__data__.set(value, HASH_UNDEFINED);

  return this;
}

module.exports = setCacheAdd;

/***/ }),

/***/ "../../node_modules/lodash/_setCacheHas.js":
/*!***************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_setCacheHas.js ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/**
 * Checks if `value` is in the array cache.
 *
 * @private
 * @name has
 * @memberOf SetCache
 * @param {*} value The value to search for.
 * @returns {number} Returns `true` if `value` is found, else `false`.
 */
function setCacheHas(value) {
  return this.__data__.has(value);
}

module.exports = setCacheHas;

/***/ }),

/***/ "../../node_modules/lodash/_setToArray.js":
/*!**************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_setToArray.js ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/**
 * Converts `set` to an array of its values.
 *
 * @private
 * @param {Object} set The set to convert.
 * @returns {Array} Returns the values.
 */
function setToArray(set) {
  var index = -1,
      result = Array(set.size);
  set.forEach(function (value) {
    result[++index] = value;
  });
  return result;
}

module.exports = setToArray;

/***/ }),

/***/ "../../node_modules/lodash/_stackClear.js":
/*!**************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_stackClear.js ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var ListCache = __webpack_require__(/*! ./_ListCache */ "../../node_modules/lodash/_ListCache.js");
/**
 * Removes all key-value entries from the stack.
 *
 * @private
 * @name clear
 * @memberOf Stack
 */


function stackClear() {
  this.__data__ = new ListCache();
  this.size = 0;
}

module.exports = stackClear;

/***/ }),

/***/ "../../node_modules/lodash/_stackDelete.js":
/*!***************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_stackDelete.js ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/**
 * Removes `key` and its value from the stack.
 *
 * @private
 * @name delete
 * @memberOf Stack
 * @param {string} key The key of the value to remove.
 * @returns {boolean} Returns `true` if the entry was removed, else `false`.
 */
function stackDelete(key) {
  var data = this.__data__,
      result = data['delete'](key);
  this.size = data.size;
  return result;
}

module.exports = stackDelete;

/***/ }),

/***/ "../../node_modules/lodash/_stackGet.js":
/*!************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_stackGet.js ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/**
 * Gets the stack value for `key`.
 *
 * @private
 * @name get
 * @memberOf Stack
 * @param {string} key The key of the value to get.
 * @returns {*} Returns the entry value.
 */
function stackGet(key) {
  return this.__data__.get(key);
}

module.exports = stackGet;

/***/ }),

/***/ "../../node_modules/lodash/_stackHas.js":
/*!************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_stackHas.js ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/**
 * Checks if a stack value for `key` exists.
 *
 * @private
 * @name has
 * @memberOf Stack
 * @param {string} key The key of the entry to check.
 * @returns {boolean} Returns `true` if an entry for `key` exists, else `false`.
 */
function stackHas(key) {
  return this.__data__.has(key);
}

module.exports = stackHas;

/***/ }),

/***/ "../../node_modules/lodash/_stackSet.js":
/*!************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_stackSet.js ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var ListCache = __webpack_require__(/*! ./_ListCache */ "../../node_modules/lodash/_ListCache.js"),
    Map = __webpack_require__(/*! ./_Map */ "../../node_modules/lodash/_Map.js"),
    MapCache = __webpack_require__(/*! ./_MapCache */ "../../node_modules/lodash/_MapCache.js");
/** Used as the size to enable large array optimizations. */


var LARGE_ARRAY_SIZE = 200;
/**
 * Sets the stack `key` to `value`.
 *
 * @private
 * @name set
 * @memberOf Stack
 * @param {string} key The key of the value to set.
 * @param {*} value The value to set.
 * @returns {Object} Returns the stack cache instance.
 */

function stackSet(key, value) {
  var data = this.__data__;

  if (data instanceof ListCache) {
    var pairs = data.__data__;

    if (!Map || pairs.length < LARGE_ARRAY_SIZE - 1) {
      pairs.push([key, value]);
      this.size = ++data.size;
      return this;
    }

    data = this.__data__ = new MapCache(pairs);
  }

  data.set(key, value);
  this.size = data.size;
  return this;
}

module.exports = stackSet;

/***/ }),

/***/ "../../node_modules/lodash/_stringToPath.js":
/*!****************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_stringToPath.js ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var memoizeCapped = __webpack_require__(/*! ./_memoizeCapped */ "../../node_modules/lodash/_memoizeCapped.js");
/** Used to match property names within property paths. */


var rePropName = /[^.[\]]+|\[(?:(-?\d+(?:\.\d+)?)|(["'])((?:(?!\2)[^\\]|\\.)*?)\2)\]|(?=(?:\.|\[\])(?:\.|\[\]|$))/g;
/** Used to match backslashes in property paths. */

var reEscapeChar = /\\(\\)?/g;
/**
 * Converts `string` to a property path array.
 *
 * @private
 * @param {string} string The string to convert.
 * @returns {Array} Returns the property path array.
 */

var stringToPath = memoizeCapped(function (string) {
  var result = [];

  if (string.charCodeAt(0) === 46
  /* . */
  ) {
      result.push('');
    }

  string.replace(rePropName, function (match, number, quote, subString) {
    result.push(quote ? subString.replace(reEscapeChar, '$1') : number || match);
  });
  return result;
});
module.exports = stringToPath;

/***/ }),

/***/ "../../node_modules/lodash/_toKey.js":
/*!*********************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_toKey.js ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var isSymbol = __webpack_require__(/*! ./isSymbol */ "../../node_modules/lodash/isSymbol.js");
/** Used as references for various `Number` constants. */


var INFINITY = 1 / 0;
/**
 * Converts `value` to a string key if it's not a string or symbol.
 *
 * @private
 * @param {*} value The value to inspect.
 * @returns {string|symbol} Returns the key.
 */

function toKey(value) {
  if (typeof value == 'string' || isSymbol(value)) {
    return value;
  }

  var result = value + '';
  return result == '0' && 1 / value == -INFINITY ? '-0' : result;
}

module.exports = toKey;

/***/ }),

/***/ "../../node_modules/lodash/_toSource.js":
/*!************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_toSource.js ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/** Used for built-in method references. */
var funcProto = Function.prototype;
/** Used to resolve the decompiled source of functions. */

var funcToString = funcProto.toString;
/**
 * Converts `func` to its source code.
 *
 * @private
 * @param {Function} func The function to convert.
 * @returns {string} Returns the source code.
 */

function toSource(func) {
  if (func != null) {
    try {
      return funcToString.call(func);
    } catch (e) {}

    try {
      return func + '';
    } catch (e) {}
  }

  return '';
}

module.exports = toSource;

/***/ }),

/***/ "../../node_modules/lodash/eq.js":
/*!*****************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/eq.js ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/**
 * Performs a
 * [`SameValueZero`](http://ecma-international.org/ecma-262/7.0/#sec-samevaluezero)
 * comparison between two values to determine if they are equivalent.
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to compare.
 * @param {*} other The other value to compare.
 * @returns {boolean} Returns `true` if the values are equivalent, else `false`.
 * @example
 *
 * var object = { 'a': 1 };
 * var other = { 'a': 1 };
 *
 * _.eq(object, object);
 * // => true
 *
 * _.eq(object, other);
 * // => false
 *
 * _.eq('a', 'a');
 * // => true
 *
 * _.eq('a', Object('a'));
 * // => false
 *
 * _.eq(NaN, NaN);
 * // => true
 */
function eq(value, other) {
  return value === other || value !== value && other !== other;
}

module.exports = eq;

/***/ }),

/***/ "../../node_modules/lodash/find.js":
/*!*******************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/find.js ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var createFind = __webpack_require__(/*! ./_createFind */ "../../node_modules/lodash/_createFind.js"),
    findIndex = __webpack_require__(/*! ./findIndex */ "../../node_modules/lodash/findIndex.js");
/**
 * Iterates over elements of `collection`, returning the first element
 * `predicate` returns truthy for. The predicate is invoked with three
 * arguments: (value, index|key, collection).
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Collection
 * @param {Array|Object} collection The collection to inspect.
 * @param {Function} [predicate=_.identity] The function invoked per iteration.
 * @param {number} [fromIndex=0] The index to search from.
 * @returns {*} Returns the matched element, else `undefined`.
 * @example
 *
 * var users = [
 *   { 'user': 'barney',  'age': 36, 'active': true },
 *   { 'user': 'fred',    'age': 40, 'active': false },
 *   { 'user': 'pebbles', 'age': 1,  'active': true }
 * ];
 *
 * _.find(users, function(o) { return o.age < 40; });
 * // => object for 'barney'
 *
 * // The `_.matches` iteratee shorthand.
 * _.find(users, { 'age': 1, 'active': true });
 * // => object for 'pebbles'
 *
 * // The `_.matchesProperty` iteratee shorthand.
 * _.find(users, ['active', false]);
 * // => object for 'fred'
 *
 * // The `_.property` iteratee shorthand.
 * _.find(users, 'active');
 * // => object for 'barney'
 */


var find = createFind(findIndex);
module.exports = find;

/***/ }),

/***/ "../../node_modules/lodash/findIndex.js":
/*!************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/findIndex.js ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var baseFindIndex = __webpack_require__(/*! ./_baseFindIndex */ "../../node_modules/lodash/_baseFindIndex.js"),
    baseIteratee = __webpack_require__(/*! ./_baseIteratee */ "../../node_modules/lodash/_baseIteratee.js"),
    toInteger = __webpack_require__(/*! ./toInteger */ "../../node_modules/lodash/toInteger.js");
/* Built-in method references for those with the same name as other `lodash` methods. */


var nativeMax = Math.max;
/**
 * This method is like `_.find` except that it returns the index of the first
 * element `predicate` returns truthy for instead of the element itself.
 *
 * @static
 * @memberOf _
 * @since 1.1.0
 * @category Array
 * @param {Array} array The array to inspect.
 * @param {Function} [predicate=_.identity] The function invoked per iteration.
 * @param {number} [fromIndex=0] The index to search from.
 * @returns {number} Returns the index of the found element, else `-1`.
 * @example
 *
 * var users = [
 *   { 'user': 'barney',  'active': false },
 *   { 'user': 'fred',    'active': false },
 *   { 'user': 'pebbles', 'active': true }
 * ];
 *
 * _.findIndex(users, function(o) { return o.user == 'barney'; });
 * // => 0
 *
 * // The `_.matches` iteratee shorthand.
 * _.findIndex(users, { 'user': 'fred', 'active': false });
 * // => 1
 *
 * // The `_.matchesProperty` iteratee shorthand.
 * _.findIndex(users, ['active', false]);
 * // => 0
 *
 * // The `_.property` iteratee shorthand.
 * _.findIndex(users, 'active');
 * // => 2
 */

function findIndex(array, predicate, fromIndex) {
  var length = array == null ? 0 : array.length;

  if (!length) {
    return -1;
  }

  var index = fromIndex == null ? 0 : toInteger(fromIndex);

  if (index < 0) {
    index = nativeMax(length + index, 0);
  }

  return baseFindIndex(array, baseIteratee(predicate, 3), index);
}

module.exports = findIndex;

/***/ }),

/***/ "../../node_modules/lodash/get.js":
/*!******************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/get.js ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var baseGet = __webpack_require__(/*! ./_baseGet */ "../../node_modules/lodash/_baseGet.js");
/**
 * Gets the value at `path` of `object`. If the resolved value is
 * `undefined`, the `defaultValue` is returned in its place.
 *
 * @static
 * @memberOf _
 * @since 3.7.0
 * @category Object
 * @param {Object} object The object to query.
 * @param {Array|string} path The path of the property to get.
 * @param {*} [defaultValue] The value returned for `undefined` resolved values.
 * @returns {*} Returns the resolved value.
 * @example
 *
 * var object = { 'a': [{ 'b': { 'c': 3 } }] };
 *
 * _.get(object, 'a[0].b.c');
 * // => 3
 *
 * _.get(object, ['a', '0', 'b', 'c']);
 * // => 3
 *
 * _.get(object, 'a.b.c', 'default');
 * // => 'default'
 */


function get(object, path, defaultValue) {
  var result = object == null ? undefined : baseGet(object, path);
  return result === undefined ? defaultValue : result;
}

module.exports = get;

/***/ }),

/***/ "../../node_modules/lodash/hasIn.js":
/*!********************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/hasIn.js ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var baseHasIn = __webpack_require__(/*! ./_baseHasIn */ "../../node_modules/lodash/_baseHasIn.js"),
    hasPath = __webpack_require__(/*! ./_hasPath */ "../../node_modules/lodash/_hasPath.js");
/**
 * Checks if `path` is a direct or inherited property of `object`.
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Object
 * @param {Object} object The object to query.
 * @param {Array|string} path The path to check.
 * @returns {boolean} Returns `true` if `path` exists, else `false`.
 * @example
 *
 * var object = _.create({ 'a': _.create({ 'b': 2 }) });
 *
 * _.hasIn(object, 'a');
 * // => true
 *
 * _.hasIn(object, 'a.b');
 * // => true
 *
 * _.hasIn(object, ['a', 'b']);
 * // => true
 *
 * _.hasIn(object, 'b');
 * // => false
 */


function hasIn(object, path) {
  return object != null && hasPath(object, path, baseHasIn);
}

module.exports = hasIn;

/***/ }),

/***/ "../../node_modules/lodash/identity.js":
/*!***********************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/identity.js ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/**
 * This method returns the first argument it receives.
 *
 * @static
 * @since 0.1.0
 * @memberOf _
 * @category Util
 * @param {*} value Any value.
 * @returns {*} Returns `value`.
 * @example
 *
 * var object = { 'a': 1 };
 *
 * console.log(_.identity(object) === object);
 * // => true
 */
function identity(value) {
  return value;
}

module.exports = identity;

/***/ }),

/***/ "../../node_modules/lodash/isArguments.js":
/*!**************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/isArguments.js ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var baseIsArguments = __webpack_require__(/*! ./_baseIsArguments */ "../../node_modules/lodash/_baseIsArguments.js"),
    isObjectLike = __webpack_require__(/*! ./isObjectLike */ "../../node_modules/lodash/isObjectLike.js");
/** Used for built-in method references. */


var objectProto = Object.prototype;
/** Used to check objects for own properties. */

var hasOwnProperty = objectProto.hasOwnProperty;
/** Built-in value references. */

var propertyIsEnumerable = objectProto.propertyIsEnumerable;
/**
 * Checks if `value` is likely an `arguments` object.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is an `arguments` object,
 *  else `false`.
 * @example
 *
 * _.isArguments(function() { return arguments; }());
 * // => true
 *
 * _.isArguments([1, 2, 3]);
 * // => false
 */

var isArguments = baseIsArguments(function () {
  return arguments;
}()) ? baseIsArguments : function (value) {
  return isObjectLike(value) && hasOwnProperty.call(value, 'callee') && !propertyIsEnumerable.call(value, 'callee');
};
module.exports = isArguments;

/***/ }),

/***/ "../../node_modules/lodash/isArray.js":
/*!**********************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/isArray.js ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/**
 * Checks if `value` is classified as an `Array` object.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is an array, else `false`.
 * @example
 *
 * _.isArray([1, 2, 3]);
 * // => true
 *
 * _.isArray(document.body.children);
 * // => false
 *
 * _.isArray('abc');
 * // => false
 *
 * _.isArray(_.noop);
 * // => false
 */
var isArray = Array.isArray;
module.exports = isArray;

/***/ }),

/***/ "../../node_modules/lodash/isArrayLike.js":
/*!**************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/isArrayLike.js ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var isFunction = __webpack_require__(/*! ./isFunction */ "../../node_modules/lodash/isFunction.js"),
    isLength = __webpack_require__(/*! ./isLength */ "../../node_modules/lodash/isLength.js");
/**
 * Checks if `value` is array-like. A value is considered array-like if it's
 * not a function and has a `value.length` that's an integer greater than or
 * equal to `0` and less than or equal to `Number.MAX_SAFE_INTEGER`.
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is array-like, else `false`.
 * @example
 *
 * _.isArrayLike([1, 2, 3]);
 * // => true
 *
 * _.isArrayLike(document.body.children);
 * // => true
 *
 * _.isArrayLike('abc');
 * // => true
 *
 * _.isArrayLike(_.noop);
 * // => false
 */


function isArrayLike(value) {
  return value != null && isLength(value.length) && !isFunction(value);
}

module.exports = isArrayLike;

/***/ }),

/***/ "../../node_modules/lodash/isBuffer.js":
/*!***********************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/isBuffer.js ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(module) {var _typeof = __webpack_require__(/*! @babel/runtime/helpers/typeof */ "../../node_modules/@babel/runtime/helpers/typeof.js");

var root = __webpack_require__(/*! ./_root */ "../../node_modules/lodash/_root.js"),
    stubFalse = __webpack_require__(/*! ./stubFalse */ "../../node_modules/lodash/stubFalse.js");
/** Detect free variable `exports`. */


var freeExports = ( false ? undefined : _typeof(exports)) == 'object' && exports && !exports.nodeType && exports;
/** Detect free variable `module`. */

var freeModule = freeExports && ( false ? undefined : _typeof(module)) == 'object' && module && !module.nodeType && module;
/** Detect the popular CommonJS extension `module.exports`. */

var moduleExports = freeModule && freeModule.exports === freeExports;
/** Built-in value references. */

var Buffer = moduleExports ? root.Buffer : undefined;
/* Built-in method references for those with the same name as other `lodash` methods. */

var nativeIsBuffer = Buffer ? Buffer.isBuffer : undefined;
/**
 * Checks if `value` is a buffer.
 *
 * @static
 * @memberOf _
 * @since 4.3.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a buffer, else `false`.
 * @example
 *
 * _.isBuffer(new Buffer(2));
 * // => true
 *
 * _.isBuffer(new Uint8Array(2));
 * // => false
 */

var isBuffer = nativeIsBuffer || stubFalse;
module.exports = isBuffer;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../webpack/buildin/module.js */ "../../node_modules/webpack/buildin/module.js")(module)))

/***/ }),

/***/ "../../node_modules/lodash/isFunction.js":
/*!*************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/isFunction.js ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var baseGetTag = __webpack_require__(/*! ./_baseGetTag */ "../../node_modules/lodash/_baseGetTag.js"),
    isObject = __webpack_require__(/*! ./isObject */ "../../node_modules/lodash/isObject.js");
/** `Object#toString` result references. */


var asyncTag = '[object AsyncFunction]',
    funcTag = '[object Function]',
    genTag = '[object GeneratorFunction]',
    proxyTag = '[object Proxy]';
/**
 * Checks if `value` is classified as a `Function` object.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a function, else `false`.
 * @example
 *
 * _.isFunction(_);
 * // => true
 *
 * _.isFunction(/abc/);
 * // => false
 */

function isFunction(value) {
  if (!isObject(value)) {
    return false;
  } // The use of `Object#toString` avoids issues with the `typeof` operator
  // in Safari 9 which returns 'object' for typed arrays and other constructors.


  var tag = baseGetTag(value);
  return tag == funcTag || tag == genTag || tag == asyncTag || tag == proxyTag;
}

module.exports = isFunction;

/***/ }),

/***/ "../../node_modules/lodash/isLength.js":
/*!***********************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/isLength.js ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/** Used as references for various `Number` constants. */
var MAX_SAFE_INTEGER = 9007199254740991;
/**
 * Checks if `value` is a valid array-like length.
 *
 * **Note:** This method is loosely based on
 * [`ToLength`](http://ecma-international.org/ecma-262/7.0/#sec-tolength).
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a valid length, else `false`.
 * @example
 *
 * _.isLength(3);
 * // => true
 *
 * _.isLength(Number.MIN_VALUE);
 * // => false
 *
 * _.isLength(Infinity);
 * // => false
 *
 * _.isLength('3');
 * // => false
 */

function isLength(value) {
  return typeof value == 'number' && value > -1 && value % 1 == 0 && value <= MAX_SAFE_INTEGER;
}

module.exports = isLength;

/***/ }),

/***/ "../../node_modules/lodash/isObject.js":
/*!***********************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/isObject.js ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var _typeof = __webpack_require__(/*! @babel/runtime/helpers/typeof */ "../../node_modules/@babel/runtime/helpers/typeof.js");

/**
 * Checks if `value` is the
 * [language type](http://www.ecma-international.org/ecma-262/7.0/#sec-ecmascript-language-types)
 * of `Object`. (e.g. arrays, functions, objects, regexes, `new Number(0)`, and `new String('')`)
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is an object, else `false`.
 * @example
 *
 * _.isObject({});
 * // => true
 *
 * _.isObject([1, 2, 3]);
 * // => true
 *
 * _.isObject(_.noop);
 * // => true
 *
 * _.isObject(null);
 * // => false
 */
function isObject(value) {
  var type = _typeof(value);

  return value != null && (type == 'object' || type == 'function');
}

module.exports = isObject;

/***/ }),

/***/ "../../node_modules/lodash/isObjectLike.js":
/*!***************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/isObjectLike.js ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var _typeof = __webpack_require__(/*! @babel/runtime/helpers/typeof */ "../../node_modules/@babel/runtime/helpers/typeof.js");

/**
 * Checks if `value` is object-like. A value is object-like if it's not `null`
 * and has a `typeof` result of "object".
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is object-like, else `false`.
 * @example
 *
 * _.isObjectLike({});
 * // => true
 *
 * _.isObjectLike([1, 2, 3]);
 * // => true
 *
 * _.isObjectLike(_.noop);
 * // => false
 *
 * _.isObjectLike(null);
 * // => false
 */
function isObjectLike(value) {
  return value != null && _typeof(value) == 'object';
}

module.exports = isObjectLike;

/***/ }),

/***/ "../../node_modules/lodash/isSymbol.js":
/*!***********************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/isSymbol.js ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var _typeof = __webpack_require__(/*! @babel/runtime/helpers/typeof */ "../../node_modules/@babel/runtime/helpers/typeof.js");

var baseGetTag = __webpack_require__(/*! ./_baseGetTag */ "../../node_modules/lodash/_baseGetTag.js"),
    isObjectLike = __webpack_require__(/*! ./isObjectLike */ "../../node_modules/lodash/isObjectLike.js");
/** `Object#toString` result references. */


var symbolTag = '[object Symbol]';
/**
 * Checks if `value` is classified as a `Symbol` primitive or object.
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a symbol, else `false`.
 * @example
 *
 * _.isSymbol(Symbol.iterator);
 * // => true
 *
 * _.isSymbol('abc');
 * // => false
 */

function isSymbol(value) {
  return _typeof(value) == 'symbol' || isObjectLike(value) && baseGetTag(value) == symbolTag;
}

module.exports = isSymbol;

/***/ }),

/***/ "../../node_modules/lodash/isTypedArray.js":
/*!***************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/isTypedArray.js ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var baseIsTypedArray = __webpack_require__(/*! ./_baseIsTypedArray */ "../../node_modules/lodash/_baseIsTypedArray.js"),
    baseUnary = __webpack_require__(/*! ./_baseUnary */ "../../node_modules/lodash/_baseUnary.js"),
    nodeUtil = __webpack_require__(/*! ./_nodeUtil */ "../../node_modules/lodash/_nodeUtil.js");
/* Node.js helper references. */


var nodeIsTypedArray = nodeUtil && nodeUtil.isTypedArray;
/**
 * Checks if `value` is classified as a typed array.
 *
 * @static
 * @memberOf _
 * @since 3.0.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a typed array, else `false`.
 * @example
 *
 * _.isTypedArray(new Uint8Array);
 * // => true
 *
 * _.isTypedArray([]);
 * // => false
 */

var isTypedArray = nodeIsTypedArray ? baseUnary(nodeIsTypedArray) : baseIsTypedArray;
module.exports = isTypedArray;

/***/ }),

/***/ "../../node_modules/lodash/keys.js":
/*!*******************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/keys.js ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var arrayLikeKeys = __webpack_require__(/*! ./_arrayLikeKeys */ "../../node_modules/lodash/_arrayLikeKeys.js"),
    baseKeys = __webpack_require__(/*! ./_baseKeys */ "../../node_modules/lodash/_baseKeys.js"),
    isArrayLike = __webpack_require__(/*! ./isArrayLike */ "../../node_modules/lodash/isArrayLike.js");
/**
 * Creates an array of the own enumerable property names of `object`.
 *
 * **Note:** Non-object values are coerced to objects. See the
 * [ES spec](http://ecma-international.org/ecma-262/7.0/#sec-object.keys)
 * for more details.
 *
 * @static
 * @since 0.1.0
 * @memberOf _
 * @category Object
 * @param {Object} object The object to query.
 * @returns {Array} Returns the array of property names.
 * @example
 *
 * function Foo() {
 *   this.a = 1;
 *   this.b = 2;
 * }
 *
 * Foo.prototype.c = 3;
 *
 * _.keys(new Foo);
 * // => ['a', 'b'] (iteration order is not guaranteed)
 *
 * _.keys('hi');
 * // => ['0', '1']
 */


function keys(object) {
  return isArrayLike(object) ? arrayLikeKeys(object) : baseKeys(object);
}

module.exports = keys;

/***/ }),

/***/ "../../node_modules/lodash/memoize.js":
/*!**********************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/memoize.js ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var MapCache = __webpack_require__(/*! ./_MapCache */ "../../node_modules/lodash/_MapCache.js");
/** Error message constants. */


var FUNC_ERROR_TEXT = 'Expected a function';
/**
 * Creates a function that memoizes the result of `func`. If `resolver` is
 * provided, it determines the cache key for storing the result based on the
 * arguments provided to the memoized function. By default, the first argument
 * provided to the memoized function is used as the map cache key. The `func`
 * is invoked with the `this` binding of the memoized function.
 *
 * **Note:** The cache is exposed as the `cache` property on the memoized
 * function. Its creation may be customized by replacing the `_.memoize.Cache`
 * constructor with one whose instances implement the
 * [`Map`](http://ecma-international.org/ecma-262/7.0/#sec-properties-of-the-map-prototype-object)
 * method interface of `clear`, `delete`, `get`, `has`, and `set`.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Function
 * @param {Function} func The function to have its output memoized.
 * @param {Function} [resolver] The function to resolve the cache key.
 * @returns {Function} Returns the new memoized function.
 * @example
 *
 * var object = { 'a': 1, 'b': 2 };
 * var other = { 'c': 3, 'd': 4 };
 *
 * var values = _.memoize(_.values);
 * values(object);
 * // => [1, 2]
 *
 * values(other);
 * // => [3, 4]
 *
 * object.a = 2;
 * values(object);
 * // => [1, 2]
 *
 * // Modify the result cache.
 * values.cache.set(object, ['a', 'b']);
 * values(object);
 * // => ['a', 'b']
 *
 * // Replace `_.memoize.Cache`.
 * _.memoize.Cache = WeakMap;
 */

function memoize(func, resolver) {
  if (typeof func != 'function' || resolver != null && typeof resolver != 'function') {
    throw new TypeError(FUNC_ERROR_TEXT);
  }

  var memoized = function memoized() {
    var args = arguments,
        key = resolver ? resolver.apply(this, args) : args[0],
        cache = memoized.cache;

    if (cache.has(key)) {
      return cache.get(key);
    }

    var result = func.apply(this, args);
    memoized.cache = cache.set(key, result) || cache;
    return result;
  };

  memoized.cache = new (memoize.Cache || MapCache)();
  return memoized;
} // Expose `MapCache`.


memoize.Cache = MapCache;
module.exports = memoize;

/***/ }),

/***/ "../../node_modules/lodash/property.js":
/*!***********************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/property.js ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var baseProperty = __webpack_require__(/*! ./_baseProperty */ "../../node_modules/lodash/_baseProperty.js"),
    basePropertyDeep = __webpack_require__(/*! ./_basePropertyDeep */ "../../node_modules/lodash/_basePropertyDeep.js"),
    isKey = __webpack_require__(/*! ./_isKey */ "../../node_modules/lodash/_isKey.js"),
    toKey = __webpack_require__(/*! ./_toKey */ "../../node_modules/lodash/_toKey.js");
/**
 * Creates a function that returns the value at `path` of a given object.
 *
 * @static
 * @memberOf _
 * @since 2.4.0
 * @category Util
 * @param {Array|string} path The path of the property to get.
 * @returns {Function} Returns the new accessor function.
 * @example
 *
 * var objects = [
 *   { 'a': { 'b': 2 } },
 *   { 'a': { 'b': 1 } }
 * ];
 *
 * _.map(objects, _.property('a.b'));
 * // => [2, 1]
 *
 * _.map(_.sortBy(objects, _.property(['a', 'b'])), 'a.b');
 * // => [1, 2]
 */


function property(path) {
  return isKey(path) ? baseProperty(toKey(path)) : basePropertyDeep(path);
}

module.exports = property;

/***/ }),

/***/ "../../node_modules/lodash/stubArray.js":
/*!************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/stubArray.js ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/**
 * This method returns a new empty array.
 *
 * @static
 * @memberOf _
 * @since 4.13.0
 * @category Util
 * @returns {Array} Returns the new empty array.
 * @example
 *
 * var arrays = _.times(2, _.stubArray);
 *
 * console.log(arrays);
 * // => [[], []]
 *
 * console.log(arrays[0] === arrays[1]);
 * // => false
 */
function stubArray() {
  return [];
}

module.exports = stubArray;

/***/ }),

/***/ "../../node_modules/lodash/stubFalse.js":
/*!************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/stubFalse.js ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/**
 * This method returns `false`.
 *
 * @static
 * @memberOf _
 * @since 4.13.0
 * @category Util
 * @returns {boolean} Returns `false`.
 * @example
 *
 * _.times(2, _.stubFalse);
 * // => [false, false]
 */
function stubFalse() {
  return false;
}

module.exports = stubFalse;

/***/ }),

/***/ "../../node_modules/lodash/toFinite.js":
/*!***********************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/toFinite.js ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var toNumber = __webpack_require__(/*! ./toNumber */ "../../node_modules/lodash/toNumber.js");
/** Used as references for various `Number` constants. */


var INFINITY = 1 / 0,
    MAX_INTEGER = 1.7976931348623157e+308;
/**
 * Converts `value` to a finite number.
 *
 * @static
 * @memberOf _
 * @since 4.12.0
 * @category Lang
 * @param {*} value The value to convert.
 * @returns {number} Returns the converted number.
 * @example
 *
 * _.toFinite(3.2);
 * // => 3.2
 *
 * _.toFinite(Number.MIN_VALUE);
 * // => 5e-324
 *
 * _.toFinite(Infinity);
 * // => 1.7976931348623157e+308
 *
 * _.toFinite('3.2');
 * // => 3.2
 */

function toFinite(value) {
  if (!value) {
    return value === 0 ? value : 0;
  }

  value = toNumber(value);

  if (value === INFINITY || value === -INFINITY) {
    var sign = value < 0 ? -1 : 1;
    return sign * MAX_INTEGER;
  }

  return value === value ? value : 0;
}

module.exports = toFinite;

/***/ }),

/***/ "../../node_modules/lodash/toInteger.js":
/*!************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/toInteger.js ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var toFinite = __webpack_require__(/*! ./toFinite */ "../../node_modules/lodash/toFinite.js");
/**
 * Converts `value` to an integer.
 *
 * **Note:** This method is loosely based on
 * [`ToInteger`](http://www.ecma-international.org/ecma-262/7.0/#sec-tointeger).
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to convert.
 * @returns {number} Returns the converted integer.
 * @example
 *
 * _.toInteger(3.2);
 * // => 3
 *
 * _.toInteger(Number.MIN_VALUE);
 * // => 0
 *
 * _.toInteger(Infinity);
 * // => 1.7976931348623157e+308
 *
 * _.toInteger('3.2');
 * // => 3
 */


function toInteger(value) {
  var result = toFinite(value),
      remainder = result % 1;
  return result === result ? remainder ? result - remainder : result : 0;
}

module.exports = toInteger;

/***/ }),

/***/ "../../node_modules/lodash/toNumber.js":
/*!***********************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/toNumber.js ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var isObject = __webpack_require__(/*! ./isObject */ "../../node_modules/lodash/isObject.js"),
    isSymbol = __webpack_require__(/*! ./isSymbol */ "../../node_modules/lodash/isSymbol.js");
/** Used as references for various `Number` constants. */


var NAN = 0 / 0;
/** Used to match leading and trailing whitespace. */

var reTrim = /^\s+|\s+$/g;
/** Used to detect bad signed hexadecimal string values. */

var reIsBadHex = /^[-+]0x[0-9a-f]+$/i;
/** Used to detect binary string values. */

var reIsBinary = /^0b[01]+$/i;
/** Used to detect octal string values. */

var reIsOctal = /^0o[0-7]+$/i;
/** Built-in method references without a dependency on `root`. */

var freeParseInt = parseInt;
/**
 * Converts `value` to a number.
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to process.
 * @returns {number} Returns the number.
 * @example
 *
 * _.toNumber(3.2);
 * // => 3.2
 *
 * _.toNumber(Number.MIN_VALUE);
 * // => 5e-324
 *
 * _.toNumber(Infinity);
 * // => Infinity
 *
 * _.toNumber('3.2');
 * // => 3.2
 */

function toNumber(value) {
  if (typeof value == 'number') {
    return value;
  }

  if (isSymbol(value)) {
    return NAN;
  }

  if (isObject(value)) {
    var other = typeof value.valueOf == 'function' ? value.valueOf() : value;
    value = isObject(other) ? other + '' : other;
  }

  if (typeof value != 'string') {
    return value === 0 ? value : +value;
  }

  value = value.replace(reTrim, '');
  var isBinary = reIsBinary.test(value);
  return isBinary || reIsOctal.test(value) ? freeParseInt(value.slice(2), isBinary ? 2 : 8) : reIsBadHex.test(value) ? NAN : +value;
}

module.exports = toNumber;

/***/ }),

/***/ "../../node_modules/lodash/toString.js":
/*!***********************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/toString.js ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var baseToString = __webpack_require__(/*! ./_baseToString */ "../../node_modules/lodash/_baseToString.js");
/**
 * Converts `value` to a string. An empty string is returned for `null`
 * and `undefined` values. The sign of `-0` is preserved.
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to convert.
 * @returns {string} Returns the converted string.
 * @example
 *
 * _.toString(null);
 * // => ''
 *
 * _.toString(-0);
 * // => '-0'
 *
 * _.toString([1, 2, 3]);
 * // => '1,2,3'
 */


function toString(value) {
  return value == null ? '' : baseToString(value);
}

module.exports = toString;

/***/ }),

/***/ "../../node_modules/vue-loader/lib/runtime/componentNormalizer.js":
/*!**************************************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/vue-loader/lib/runtime/componentNormalizer.js ***!
  \**************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return normalizeComponent; });
/* globals __VUE_SSR_CONTEXT__ */

// IMPORTANT: Do NOT use ES2015 features in this file (except for modules).
// This module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle.

function normalizeComponent (
  scriptExports,
  render,
  staticRenderFns,
  functionalTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier, /* server only */
  shadowMode /* vue-cli only */
) {
  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (render) {
    options.render = render
    options.staticRenderFns = staticRenderFns
    options._compiled = true
  }

  // functional template
  if (functionalTemplate) {
    options.functional = true
  }

  // scopedId
  if (scopeId) {
    options._scopeId = 'data-v-' + scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = shadowMode
      ? function () {
        injectStyles.call(
          this,
          (options.functional ? this.parent : this).$root.$options.shadowRoot
        )
      }
      : injectStyles
  }

  if (hook) {
    if (options.functional) {
      // for template-only hot-reload because in that case the render fn doesn't
      // go through the normalizer
      options._injectStyles = hook
      // register for functional component in vue file
      var originalRender = options.render
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return originalRender(h, context)
      }
    } else {
      // inject component registration as beforeCreate hook
      var existing = options.beforeCreate
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    }
  }

  return {
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ "../../node_modules/vue-style-loader/index.js!../../node_modules/css-loader/dist/cjs.js?!../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../node_modules/postcss-loader/src/index.js?!../../node_modules/sass-loader/dist/cjs.js?!../../node_modules/vue-loader/lib/index.js?!./src/components/ControlKit.vue?vue&type=style&index=0&lang=scss&":
/*!*****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/vue-style-loader!D:/Users/dani/Desktop/@monogrid/node_modules/css-loader/dist/cjs.js??ref--7-1!D:/Users/dani/Desktop/@monogrid/node_modules/vue-loader/lib/loaders/stylePostLoader.js!D:/Users/dani/Desktop/@monogrid/node_modules/postcss-loader/src??ref--7-2!D:/Users/dani/Desktop/@monogrid/node_modules/sass-loader/dist/cjs.js??ref--7-3!D:/Users/dani/Desktop/@monogrid/node_modules/vue-loader/lib??vue-loader-options!./src/components/ControlKit.vue?vue&type=style&index=0&lang=scss& ***!
  \*****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(/*! !../../../../node_modules/css-loader/dist/cjs.js??ref--7-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--7-2!../../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../../node_modules/vue-loader/lib??vue-loader-options!./ControlKit.vue?vue&type=style&index=0&lang=scss& */ "../../node_modules/css-loader/dist/cjs.js?!../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../node_modules/postcss-loader/src/index.js?!../../node_modules/sass-loader/dist/cjs.js?!../../node_modules/vue-loader/lib/index.js?!./src/components/ControlKit.vue?vue&type=style&index=0&lang=scss&");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var add = __webpack_require__(/*! ../../../../node_modules/vue-style-loader/lib/addStylesClient.js */ "../../node_modules/vue-style-loader/lib/addStylesClient.js").default
var update = add("6c0ba396", content, false, {});
// Hot Module Replacement
if(false) {}

/***/ }),

/***/ "../../node_modules/vue-style-loader/lib/addStylesClient.js":
/*!********************************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/vue-style-loader/lib/addStylesClient.js ***!
  \********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return addStylesClient; });
/* harmony import */ var _listToStyles__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./listToStyles */ "../../node_modules/vue-style-loader/lib/listToStyles.js");
/*
  MIT License http://www.opensource.org/licenses/mit-license.php
  Author Tobias Koppers @sokra
  Modified by Evan You @yyx990803
*/



var hasDocument = typeof document !== 'undefined'

if (typeof DEBUG !== 'undefined' && DEBUG) {
  if (!hasDocument) {
    throw new Error(
    'vue-style-loader cannot be used in a non-browser environment. ' +
    "Use { target: 'node' } in your Webpack config to indicate a server-rendering environment."
  ) }
}

/*
type StyleObject = {
  id: number;
  parts: Array<StyleObjectPart>
}

type StyleObjectPart = {
  css: string;
  media: string;
  sourceMap: ?string
}
*/

var stylesInDom = {/*
  [id: number]: {
    id: number,
    refs: number,
    parts: Array<(obj?: StyleObjectPart) => void>
  }
*/}

var head = hasDocument && (document.head || document.getElementsByTagName('head')[0])
var singletonElement = null
var singletonCounter = 0
var isProduction = false
var noop = function () {}
var options = null
var ssrIdKey = 'data-vue-ssr-id'

// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
// tags it will allow on a page
var isOldIE = typeof navigator !== 'undefined' && /msie [6-9]\b/.test(navigator.userAgent.toLowerCase())

function addStylesClient (parentId, list, _isProduction, _options) {
  isProduction = _isProduction

  options = _options || {}

  var styles = Object(_listToStyles__WEBPACK_IMPORTED_MODULE_0__["default"])(parentId, list)
  addStylesToDom(styles)

  return function update (newList) {
    var mayRemove = []
    for (var i = 0; i < styles.length; i++) {
      var item = styles[i]
      var domStyle = stylesInDom[item.id]
      domStyle.refs--
      mayRemove.push(domStyle)
    }
    if (newList) {
      styles = Object(_listToStyles__WEBPACK_IMPORTED_MODULE_0__["default"])(parentId, newList)
      addStylesToDom(styles)
    } else {
      styles = []
    }
    for (var i = 0; i < mayRemove.length; i++) {
      var domStyle = mayRemove[i]
      if (domStyle.refs === 0) {
        for (var j = 0; j < domStyle.parts.length; j++) {
          domStyle.parts[j]()
        }
        delete stylesInDom[domStyle.id]
      }
    }
  }
}

function addStylesToDom (styles /* Array<StyleObject> */) {
  for (var i = 0; i < styles.length; i++) {
    var item = styles[i]
    var domStyle = stylesInDom[item.id]
    if (domStyle) {
      domStyle.refs++
      for (var j = 0; j < domStyle.parts.length; j++) {
        domStyle.parts[j](item.parts[j])
      }
      for (; j < item.parts.length; j++) {
        domStyle.parts.push(addStyle(item.parts[j]))
      }
      if (domStyle.parts.length > item.parts.length) {
        domStyle.parts.length = item.parts.length
      }
    } else {
      var parts = []
      for (var j = 0; j < item.parts.length; j++) {
        parts.push(addStyle(item.parts[j]))
      }
      stylesInDom[item.id] = { id: item.id, refs: 1, parts: parts }
    }
  }
}

function createStyleElement () {
  var styleElement = document.createElement('style')
  styleElement.type = 'text/css'
  head.appendChild(styleElement)
  return styleElement
}

function addStyle (obj /* StyleObjectPart */) {
  var update, remove
  var styleElement = document.querySelector('style[' + ssrIdKey + '~="' + obj.id + '"]')

  if (styleElement) {
    if (isProduction) {
      // has SSR styles and in production mode.
      // simply do nothing.
      return noop
    } else {
      // has SSR styles but in dev mode.
      // for some reason Chrome can't handle source map in server-rendered
      // style tags - source maps in <style> only works if the style tag is
      // created and inserted dynamically. So we remove the server rendered
      // styles and inject new ones.
      styleElement.parentNode.removeChild(styleElement)
    }
  }

  if (isOldIE) {
    // use singleton mode for IE9.
    var styleIndex = singletonCounter++
    styleElement = singletonElement || (singletonElement = createStyleElement())
    update = applyToSingletonTag.bind(null, styleElement, styleIndex, false)
    remove = applyToSingletonTag.bind(null, styleElement, styleIndex, true)
  } else {
    // use multi-style-tag mode in all other cases
    styleElement = createStyleElement()
    update = applyToTag.bind(null, styleElement)
    remove = function () {
      styleElement.parentNode.removeChild(styleElement)
    }
  }

  update(obj)

  return function updateStyle (newObj /* StyleObjectPart */) {
    if (newObj) {
      if (newObj.css === obj.css &&
          newObj.media === obj.media &&
          newObj.sourceMap === obj.sourceMap) {
        return
      }
      update(obj = newObj)
    } else {
      remove()
    }
  }
}

var replaceText = (function () {
  var textStore = []

  return function (index, replacement) {
    textStore[index] = replacement
    return textStore.filter(Boolean).join('\n')
  }
})()

function applyToSingletonTag (styleElement, index, remove, obj) {
  var css = remove ? '' : obj.css

  if (styleElement.styleSheet) {
    styleElement.styleSheet.cssText = replaceText(index, css)
  } else {
    var cssNode = document.createTextNode(css)
    var childNodes = styleElement.childNodes
    if (childNodes[index]) styleElement.removeChild(childNodes[index])
    if (childNodes.length) {
      styleElement.insertBefore(cssNode, childNodes[index])
    } else {
      styleElement.appendChild(cssNode)
    }
  }
}

function applyToTag (styleElement, obj) {
  var css = obj.css
  var media = obj.media
  var sourceMap = obj.sourceMap

  if (media) {
    styleElement.setAttribute('media', media)
  }
  if (options.ssrId) {
    styleElement.setAttribute(ssrIdKey, obj.id)
  }

  if (sourceMap) {
    // https://developer.chrome.com/devtools/docs/javascript-debugging
    // this makes source maps inside style tags work properly in Chrome
    css += '\n/*# sourceURL=' + sourceMap.sources[0] + ' */'
    // http://stackoverflow.com/a/26603875
    css += '\n/*# sourceMappingURL=data:application/json;base64,' + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + ' */'
  }

  if (styleElement.styleSheet) {
    styleElement.styleSheet.cssText = css
  } else {
    while (styleElement.firstChild) {
      styleElement.removeChild(styleElement.firstChild)
    }
    styleElement.appendChild(document.createTextNode(css))
  }
}


/***/ }),

/***/ "../../node_modules/vue-style-loader/lib/listToStyles.js":
/*!*****************************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/vue-style-loader/lib/listToStyles.js ***!
  \*****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return listToStyles; });
/**
 * Translates the list format produced by css-loader into something
 * easier to manipulate.
 */
function listToStyles(parentId, list) {
  var styles = [];
  var newStyles = {};

  for (var i = 0; i < list.length; i++) {
    var item = list[i];
    var id = item[0];
    var css = item[1];
    var media = item[2];
    var sourceMap = item[3];
    var part = {
      id: parentId + ':' + i,
      css: css,
      media: media,
      sourceMap: sourceMap
    };

    if (!newStyles[id]) {
      styles.push(newStyles[id] = {
        id: id,
        parts: [part]
      });
    } else {
      newStyles[id].parts.push(part);
    }
  }

  return styles;
}

/***/ }),

/***/ "../../node_modules/webpack/buildin/global.js":
/*!***********************************!*\
  !*** (webpack)/buildin/global.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var _typeof = __webpack_require__(/*! @babel/runtime/helpers/typeof */ "../../node_modules/@babel/runtime/helpers/typeof.js");

var g; // This works in non-strict mode

g = function () {
  return this;
}();

try {
  // This works if eval is allowed (see CSP)
  g = g || new Function("return this")();
} catch (e) {
  // This works if the window reference is available
  if ((typeof window === "undefined" ? "undefined" : _typeof(window)) === "object") g = window;
} // g can still be undefined, but nothing to do about it...
// We return undefined, instead of nothing here, so it's
// easier to handle this case. if(!global) { ...}


module.exports = g;

/***/ }),

/***/ "../../node_modules/webpack/buildin/module.js":
/*!***********************************!*\
  !*** (webpack)/buildin/module.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = function (module) {
  if (!module.webpackPolyfill) {
    module.deprecate = function () {};

    module.paths = []; // module.parent = undefined by default

    if (!module.children) module.children = [];
    Object.defineProperty(module, "loaded", {
      enumerable: true,
      get: function get() {
        return module.l;
      }
    });
    Object.defineProperty(module, "id", {
      enumerable: true,
      get: function get() {
        return module.i;
      }
    });
    module.webpackPolyfill = 1;
  }

  return module;
};

/***/ }),

/***/ "./src/components/ControlKit.vue":
/*!***************************************!*\
  !*** ./src/components/ControlKit.vue ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ControlKit_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ControlKit.vue?vue&type=script&lang=js& */ "./src/components/ControlKit.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _ControlKit_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ControlKit.vue?vue&type=style&index=0&lang=scss& */ "./src/components/ControlKit.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "../../node_modules/vue-loader/lib/runtime/componentNormalizer.js");
var render, staticRenderFns





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _ControlKit_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"],
  render,
  staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "src/components/ControlKit.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./src/components/ControlKit.vue?vue&type=script&lang=js&":
/*!****************************************************************!*\
  !*** ./src/components/ControlKit.vue?vue&type=script&lang=js& ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_loader_lib_index_js_vue_loader_options_ControlKit_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib!../../../../node_modules/vue-loader/lib??vue-loader-options!./ControlKit.vue?vue&type=script&lang=js& */ "../../node_modules/babel-loader/lib/index.js!../../node_modules/vue-loader/lib/index.js?!./src/components/ControlKit.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_node_modules_vue_loader_lib_index_js_vue_loader_options_ControlKit_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./src/components/ControlKit.vue?vue&type=style&index=0&lang=scss&":
/*!*************************************************************************!*\
  !*** ./src/components/ControlKit.vue?vue&type=style&index=0&lang=scss& ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_node_modules_css_loader_dist_cjs_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_ControlKit_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-style-loader!../../../../node_modules/css-loader/dist/cjs.js??ref--7-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--7-2!../../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../../node_modules/vue-loader/lib??vue-loader-options!./ControlKit.vue?vue&type=style&index=0&lang=scss& */ "../../node_modules/vue-style-loader/index.js!../../node_modules/css-loader/dist/cjs.js?!../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../node_modules/postcss-loader/src/index.js?!../../node_modules/sass-loader/dist/cjs.js?!../../node_modules/vue-loader/lib/index.js?!./src/components/ControlKit.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_vue_style_loader_index_js_node_modules_css_loader_dist_cjs_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_ControlKit_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_node_modules_css_loader_dist_cjs_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_ControlKit_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_node_modules_css_loader_dist_cjs_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_ControlKit_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_node_modules_css_loader_dist_cjs_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_ControlKit_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_vue_style_loader_index_js_node_modules_css_loader_dist_cjs_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_ControlKit_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "@monogrid/js-utils":
/*!*************************************!*\
  !*** external "@monogrid/js-utils" ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE__monogrid_js_utils__;

/***/ })

/******/ });
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly92dWUtbGliL3dlYnBhY2svdW5pdmVyc2FsTW9kdWxlRGVmaW5pdGlvbiIsIndlYnBhY2s6Ly92dWUtbGliL3dlYnBhY2svYm9vdHN0cmFwIiwid2VicGFjazovL3Z1ZS1saWIvRDovVXNlcnMvZGFuaS9EZXNrdG9wL0Btb25vZ3JpZC9ub2RlX21vZHVsZXMvQGJhYmVsL3J1bnRpbWUvaGVscGVycy90eXBlb2YuanMiLCJ3ZWJwYWNrOi8vdnVlLWxpYi9zcmMvY29tcG9uZW50cy9Db250cm9sS2l0LnZ1ZSIsIndlYnBhY2s6Ly92dWUtbGliLy4vc3JjL2NvbXBvbmVudHMvQ29udHJvbEtpdC52dWU/MGY5YSIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvZGlzdC9ydW50aW1lL2FwaS5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL2xvZGFzaC9fRGF0YVZpZXcuanMiLCJ3ZWJwYWNrOi8vdnVlLWxpYi9EOi9Vc2Vycy9kYW5pL0Rlc2t0b3AvQG1vbm9ncmlkL25vZGVfbW9kdWxlcy9sb2Rhc2gvX0hhc2guanMiLCJ3ZWJwYWNrOi8vdnVlLWxpYi9EOi9Vc2Vycy9kYW5pL0Rlc2t0b3AvQG1vbm9ncmlkL25vZGVfbW9kdWxlcy9sb2Rhc2gvX0xpc3RDYWNoZS5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL2xvZGFzaC9fTWFwLmpzIiwid2VicGFjazovL3Z1ZS1saWIvRDovVXNlcnMvZGFuaS9EZXNrdG9wL0Btb25vZ3JpZC9ub2RlX21vZHVsZXMvbG9kYXNoL19NYXBDYWNoZS5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL2xvZGFzaC9fUHJvbWlzZS5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL2xvZGFzaC9fU2V0LmpzIiwid2VicGFjazovL3Z1ZS1saWIvRDovVXNlcnMvZGFuaS9EZXNrdG9wL0Btb25vZ3JpZC9ub2RlX21vZHVsZXMvbG9kYXNoL19TZXRDYWNoZS5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL2xvZGFzaC9fU3RhY2suanMiLCJ3ZWJwYWNrOi8vdnVlLWxpYi9EOi9Vc2Vycy9kYW5pL0Rlc2t0b3AvQG1vbm9ncmlkL25vZGVfbW9kdWxlcy9sb2Rhc2gvX1N5bWJvbC5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL2xvZGFzaC9fVWludDhBcnJheS5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL2xvZGFzaC9fV2Vha01hcC5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL2xvZGFzaC9fYXJyYXlGaWx0ZXIuanMiLCJ3ZWJwYWNrOi8vdnVlLWxpYi9EOi9Vc2Vycy9kYW5pL0Rlc2t0b3AvQG1vbm9ncmlkL25vZGVfbW9kdWxlcy9sb2Rhc2gvX2FycmF5TGlrZUtleXMuanMiLCJ3ZWJwYWNrOi8vdnVlLWxpYi9EOi9Vc2Vycy9kYW5pL0Rlc2t0b3AvQG1vbm9ncmlkL25vZGVfbW9kdWxlcy9sb2Rhc2gvX2FycmF5TWFwLmpzIiwid2VicGFjazovL3Z1ZS1saWIvRDovVXNlcnMvZGFuaS9EZXNrdG9wL0Btb25vZ3JpZC9ub2RlX21vZHVsZXMvbG9kYXNoL19hcnJheVB1c2guanMiLCJ3ZWJwYWNrOi8vdnVlLWxpYi9EOi9Vc2Vycy9kYW5pL0Rlc2t0b3AvQG1vbm9ncmlkL25vZGVfbW9kdWxlcy9sb2Rhc2gvX2FycmF5U29tZS5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL2xvZGFzaC9fYXNzb2NJbmRleE9mLmpzIiwid2VicGFjazovL3Z1ZS1saWIvRDovVXNlcnMvZGFuaS9EZXNrdG9wL0Btb25vZ3JpZC9ub2RlX21vZHVsZXMvbG9kYXNoL19iYXNlRmluZEluZGV4LmpzIiwid2VicGFjazovL3Z1ZS1saWIvRDovVXNlcnMvZGFuaS9EZXNrdG9wL0Btb25vZ3JpZC9ub2RlX21vZHVsZXMvbG9kYXNoL19iYXNlR2V0LmpzIiwid2VicGFjazovL3Z1ZS1saWIvRDovVXNlcnMvZGFuaS9EZXNrdG9wL0Btb25vZ3JpZC9ub2RlX21vZHVsZXMvbG9kYXNoL19iYXNlR2V0QWxsS2V5cy5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL2xvZGFzaC9fYmFzZUdldFRhZy5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL2xvZGFzaC9fYmFzZUhhc0luLmpzIiwid2VicGFjazovL3Z1ZS1saWIvRDovVXNlcnMvZGFuaS9EZXNrdG9wL0Btb25vZ3JpZC9ub2RlX21vZHVsZXMvbG9kYXNoL19iYXNlSXNBcmd1bWVudHMuanMiLCJ3ZWJwYWNrOi8vdnVlLWxpYi9EOi9Vc2Vycy9kYW5pL0Rlc2t0b3AvQG1vbm9ncmlkL25vZGVfbW9kdWxlcy9sb2Rhc2gvX2Jhc2VJc0VxdWFsLmpzIiwid2VicGFjazovL3Z1ZS1saWIvRDovVXNlcnMvZGFuaS9EZXNrdG9wL0Btb25vZ3JpZC9ub2RlX21vZHVsZXMvbG9kYXNoL19iYXNlSXNFcXVhbERlZXAuanMiLCJ3ZWJwYWNrOi8vdnVlLWxpYi9EOi9Vc2Vycy9kYW5pL0Rlc2t0b3AvQG1vbm9ncmlkL25vZGVfbW9kdWxlcy9sb2Rhc2gvX2Jhc2VJc01hdGNoLmpzIiwid2VicGFjazovL3Z1ZS1saWIvRDovVXNlcnMvZGFuaS9EZXNrdG9wL0Btb25vZ3JpZC9ub2RlX21vZHVsZXMvbG9kYXNoL19iYXNlSXNOYXRpdmUuanMiLCJ3ZWJwYWNrOi8vdnVlLWxpYi9EOi9Vc2Vycy9kYW5pL0Rlc2t0b3AvQG1vbm9ncmlkL25vZGVfbW9kdWxlcy9sb2Rhc2gvX2Jhc2VJc1R5cGVkQXJyYXkuanMiLCJ3ZWJwYWNrOi8vdnVlLWxpYi9EOi9Vc2Vycy9kYW5pL0Rlc2t0b3AvQG1vbm9ncmlkL25vZGVfbW9kdWxlcy9sb2Rhc2gvX2Jhc2VJdGVyYXRlZS5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL2xvZGFzaC9fYmFzZUtleXMuanMiLCJ3ZWJwYWNrOi8vdnVlLWxpYi9EOi9Vc2Vycy9kYW5pL0Rlc2t0b3AvQG1vbm9ncmlkL25vZGVfbW9kdWxlcy9sb2Rhc2gvX2Jhc2VNYXRjaGVzLmpzIiwid2VicGFjazovL3Z1ZS1saWIvRDovVXNlcnMvZGFuaS9EZXNrdG9wL0Btb25vZ3JpZC9ub2RlX21vZHVsZXMvbG9kYXNoL19iYXNlTWF0Y2hlc1Byb3BlcnR5LmpzIiwid2VicGFjazovL3Z1ZS1saWIvRDovVXNlcnMvZGFuaS9EZXNrdG9wL0Btb25vZ3JpZC9ub2RlX21vZHVsZXMvbG9kYXNoL19iYXNlUHJvcGVydHkuanMiLCJ3ZWJwYWNrOi8vdnVlLWxpYi9EOi9Vc2Vycy9kYW5pL0Rlc2t0b3AvQG1vbm9ncmlkL25vZGVfbW9kdWxlcy9sb2Rhc2gvX2Jhc2VQcm9wZXJ0eURlZXAuanMiLCJ3ZWJwYWNrOi8vdnVlLWxpYi9EOi9Vc2Vycy9kYW5pL0Rlc2t0b3AvQG1vbm9ncmlkL25vZGVfbW9kdWxlcy9sb2Rhc2gvX2Jhc2VUaW1lcy5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL2xvZGFzaC9fYmFzZVRvU3RyaW5nLmpzIiwid2VicGFjazovL3Z1ZS1saWIvRDovVXNlcnMvZGFuaS9EZXNrdG9wL0Btb25vZ3JpZC9ub2RlX21vZHVsZXMvbG9kYXNoL19iYXNlVW5hcnkuanMiLCJ3ZWJwYWNrOi8vdnVlLWxpYi9EOi9Vc2Vycy9kYW5pL0Rlc2t0b3AvQG1vbm9ncmlkL25vZGVfbW9kdWxlcy9sb2Rhc2gvX2NhY2hlSGFzLmpzIiwid2VicGFjazovL3Z1ZS1saWIvRDovVXNlcnMvZGFuaS9EZXNrdG9wL0Btb25vZ3JpZC9ub2RlX21vZHVsZXMvbG9kYXNoL19jYXN0UGF0aC5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL2xvZGFzaC9fY29yZUpzRGF0YS5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL2xvZGFzaC9fY3JlYXRlRmluZC5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL2xvZGFzaC9fZXF1YWxBcnJheXMuanMiLCJ3ZWJwYWNrOi8vdnVlLWxpYi9EOi9Vc2Vycy9kYW5pL0Rlc2t0b3AvQG1vbm9ncmlkL25vZGVfbW9kdWxlcy9sb2Rhc2gvX2VxdWFsQnlUYWcuanMiLCJ3ZWJwYWNrOi8vdnVlLWxpYi9EOi9Vc2Vycy9kYW5pL0Rlc2t0b3AvQG1vbm9ncmlkL25vZGVfbW9kdWxlcy9sb2Rhc2gvX2VxdWFsT2JqZWN0cy5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL2xvZGFzaC9fZnJlZUdsb2JhbC5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL2xvZGFzaC9fZ2V0QWxsS2V5cy5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL2xvZGFzaC9fZ2V0TWFwRGF0YS5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL2xvZGFzaC9fZ2V0TWF0Y2hEYXRhLmpzIiwid2VicGFjazovL3Z1ZS1saWIvRDovVXNlcnMvZGFuaS9EZXNrdG9wL0Btb25vZ3JpZC9ub2RlX21vZHVsZXMvbG9kYXNoL19nZXROYXRpdmUuanMiLCJ3ZWJwYWNrOi8vdnVlLWxpYi9EOi9Vc2Vycy9kYW5pL0Rlc2t0b3AvQG1vbm9ncmlkL25vZGVfbW9kdWxlcy9sb2Rhc2gvX2dldFJhd1RhZy5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL2xvZGFzaC9fZ2V0U3ltYm9scy5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL2xvZGFzaC9fZ2V0VGFnLmpzIiwid2VicGFjazovL3Z1ZS1saWIvRDovVXNlcnMvZGFuaS9EZXNrdG9wL0Btb25vZ3JpZC9ub2RlX21vZHVsZXMvbG9kYXNoL19nZXRWYWx1ZS5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL2xvZGFzaC9faGFzUGF0aC5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL2xvZGFzaC9faGFzaENsZWFyLmpzIiwid2VicGFjazovL3Z1ZS1saWIvRDovVXNlcnMvZGFuaS9EZXNrdG9wL0Btb25vZ3JpZC9ub2RlX21vZHVsZXMvbG9kYXNoL19oYXNoRGVsZXRlLmpzIiwid2VicGFjazovL3Z1ZS1saWIvRDovVXNlcnMvZGFuaS9EZXNrdG9wL0Btb25vZ3JpZC9ub2RlX21vZHVsZXMvbG9kYXNoL19oYXNoR2V0LmpzIiwid2VicGFjazovL3Z1ZS1saWIvRDovVXNlcnMvZGFuaS9EZXNrdG9wL0Btb25vZ3JpZC9ub2RlX21vZHVsZXMvbG9kYXNoL19oYXNoSGFzLmpzIiwid2VicGFjazovL3Z1ZS1saWIvRDovVXNlcnMvZGFuaS9EZXNrdG9wL0Btb25vZ3JpZC9ub2RlX21vZHVsZXMvbG9kYXNoL19oYXNoU2V0LmpzIiwid2VicGFjazovL3Z1ZS1saWIvRDovVXNlcnMvZGFuaS9EZXNrdG9wL0Btb25vZ3JpZC9ub2RlX21vZHVsZXMvbG9kYXNoL19pc0luZGV4LmpzIiwid2VicGFjazovL3Z1ZS1saWIvRDovVXNlcnMvZGFuaS9EZXNrdG9wL0Btb25vZ3JpZC9ub2RlX21vZHVsZXMvbG9kYXNoL19pc0tleS5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL2xvZGFzaC9faXNLZXlhYmxlLmpzIiwid2VicGFjazovL3Z1ZS1saWIvRDovVXNlcnMvZGFuaS9EZXNrdG9wL0Btb25vZ3JpZC9ub2RlX21vZHVsZXMvbG9kYXNoL19pc01hc2tlZC5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL2xvZGFzaC9faXNQcm90b3R5cGUuanMiLCJ3ZWJwYWNrOi8vdnVlLWxpYi9EOi9Vc2Vycy9kYW5pL0Rlc2t0b3AvQG1vbm9ncmlkL25vZGVfbW9kdWxlcy9sb2Rhc2gvX2lzU3RyaWN0Q29tcGFyYWJsZS5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL2xvZGFzaC9fbGlzdENhY2hlQ2xlYXIuanMiLCJ3ZWJwYWNrOi8vdnVlLWxpYi9EOi9Vc2Vycy9kYW5pL0Rlc2t0b3AvQG1vbm9ncmlkL25vZGVfbW9kdWxlcy9sb2Rhc2gvX2xpc3RDYWNoZURlbGV0ZS5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL2xvZGFzaC9fbGlzdENhY2hlR2V0LmpzIiwid2VicGFjazovL3Z1ZS1saWIvRDovVXNlcnMvZGFuaS9EZXNrdG9wL0Btb25vZ3JpZC9ub2RlX21vZHVsZXMvbG9kYXNoL19saXN0Q2FjaGVIYXMuanMiLCJ3ZWJwYWNrOi8vdnVlLWxpYi9EOi9Vc2Vycy9kYW5pL0Rlc2t0b3AvQG1vbm9ncmlkL25vZGVfbW9kdWxlcy9sb2Rhc2gvX2xpc3RDYWNoZVNldC5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL2xvZGFzaC9fbWFwQ2FjaGVDbGVhci5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL2xvZGFzaC9fbWFwQ2FjaGVEZWxldGUuanMiLCJ3ZWJwYWNrOi8vdnVlLWxpYi9EOi9Vc2Vycy9kYW5pL0Rlc2t0b3AvQG1vbm9ncmlkL25vZGVfbW9kdWxlcy9sb2Rhc2gvX21hcENhY2hlR2V0LmpzIiwid2VicGFjazovL3Z1ZS1saWIvRDovVXNlcnMvZGFuaS9EZXNrdG9wL0Btb25vZ3JpZC9ub2RlX21vZHVsZXMvbG9kYXNoL19tYXBDYWNoZUhhcy5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL2xvZGFzaC9fbWFwQ2FjaGVTZXQuanMiLCJ3ZWJwYWNrOi8vdnVlLWxpYi9EOi9Vc2Vycy9kYW5pL0Rlc2t0b3AvQG1vbm9ncmlkL25vZGVfbW9kdWxlcy9sb2Rhc2gvX21hcFRvQXJyYXkuanMiLCJ3ZWJwYWNrOi8vdnVlLWxpYi9EOi9Vc2Vycy9kYW5pL0Rlc2t0b3AvQG1vbm9ncmlkL25vZGVfbW9kdWxlcy9sb2Rhc2gvX21hdGNoZXNTdHJpY3RDb21wYXJhYmxlLmpzIiwid2VicGFjazovL3Z1ZS1saWIvRDovVXNlcnMvZGFuaS9EZXNrdG9wL0Btb25vZ3JpZC9ub2RlX21vZHVsZXMvbG9kYXNoL19tZW1vaXplQ2FwcGVkLmpzIiwid2VicGFjazovL3Z1ZS1saWIvRDovVXNlcnMvZGFuaS9EZXNrdG9wL0Btb25vZ3JpZC9ub2RlX21vZHVsZXMvbG9kYXNoL19uYXRpdmVDcmVhdGUuanMiLCJ3ZWJwYWNrOi8vdnVlLWxpYi9EOi9Vc2Vycy9kYW5pL0Rlc2t0b3AvQG1vbm9ncmlkL25vZGVfbW9kdWxlcy9sb2Rhc2gvX25hdGl2ZUtleXMuanMiLCJ3ZWJwYWNrOi8vdnVlLWxpYi9EOi9Vc2Vycy9kYW5pL0Rlc2t0b3AvQG1vbm9ncmlkL25vZGVfbW9kdWxlcy9sb2Rhc2gvX25vZGVVdGlsLmpzIiwid2VicGFjazovL3Z1ZS1saWIvRDovVXNlcnMvZGFuaS9EZXNrdG9wL0Btb25vZ3JpZC9ub2RlX21vZHVsZXMvbG9kYXNoL19vYmplY3RUb1N0cmluZy5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL2xvZGFzaC9fb3ZlckFyZy5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL2xvZGFzaC9fcm9vdC5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL2xvZGFzaC9fc2V0Q2FjaGVBZGQuanMiLCJ3ZWJwYWNrOi8vdnVlLWxpYi9EOi9Vc2Vycy9kYW5pL0Rlc2t0b3AvQG1vbm9ncmlkL25vZGVfbW9kdWxlcy9sb2Rhc2gvX3NldENhY2hlSGFzLmpzIiwid2VicGFjazovL3Z1ZS1saWIvRDovVXNlcnMvZGFuaS9EZXNrdG9wL0Btb25vZ3JpZC9ub2RlX21vZHVsZXMvbG9kYXNoL19zZXRUb0FycmF5LmpzIiwid2VicGFjazovL3Z1ZS1saWIvRDovVXNlcnMvZGFuaS9EZXNrdG9wL0Btb25vZ3JpZC9ub2RlX21vZHVsZXMvbG9kYXNoL19zdGFja0NsZWFyLmpzIiwid2VicGFjazovL3Z1ZS1saWIvRDovVXNlcnMvZGFuaS9EZXNrdG9wL0Btb25vZ3JpZC9ub2RlX21vZHVsZXMvbG9kYXNoL19zdGFja0RlbGV0ZS5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL2xvZGFzaC9fc3RhY2tHZXQuanMiLCJ3ZWJwYWNrOi8vdnVlLWxpYi9EOi9Vc2Vycy9kYW5pL0Rlc2t0b3AvQG1vbm9ncmlkL25vZGVfbW9kdWxlcy9sb2Rhc2gvX3N0YWNrSGFzLmpzIiwid2VicGFjazovL3Z1ZS1saWIvRDovVXNlcnMvZGFuaS9EZXNrdG9wL0Btb25vZ3JpZC9ub2RlX21vZHVsZXMvbG9kYXNoL19zdGFja1NldC5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL2xvZGFzaC9fc3RyaW5nVG9QYXRoLmpzIiwid2VicGFjazovL3Z1ZS1saWIvRDovVXNlcnMvZGFuaS9EZXNrdG9wL0Btb25vZ3JpZC9ub2RlX21vZHVsZXMvbG9kYXNoL190b0tleS5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL2xvZGFzaC9fdG9Tb3VyY2UuanMiLCJ3ZWJwYWNrOi8vdnVlLWxpYi9EOi9Vc2Vycy9kYW5pL0Rlc2t0b3AvQG1vbm9ncmlkL25vZGVfbW9kdWxlcy9sb2Rhc2gvZXEuanMiLCJ3ZWJwYWNrOi8vdnVlLWxpYi9EOi9Vc2Vycy9kYW5pL0Rlc2t0b3AvQG1vbm9ncmlkL25vZGVfbW9kdWxlcy9sb2Rhc2gvZmluZC5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL2xvZGFzaC9maW5kSW5kZXguanMiLCJ3ZWJwYWNrOi8vdnVlLWxpYi9EOi9Vc2Vycy9kYW5pL0Rlc2t0b3AvQG1vbm9ncmlkL25vZGVfbW9kdWxlcy9sb2Rhc2gvZ2V0LmpzIiwid2VicGFjazovL3Z1ZS1saWIvRDovVXNlcnMvZGFuaS9EZXNrdG9wL0Btb25vZ3JpZC9ub2RlX21vZHVsZXMvbG9kYXNoL2hhc0luLmpzIiwid2VicGFjazovL3Z1ZS1saWIvRDovVXNlcnMvZGFuaS9EZXNrdG9wL0Btb25vZ3JpZC9ub2RlX21vZHVsZXMvbG9kYXNoL2lkZW50aXR5LmpzIiwid2VicGFjazovL3Z1ZS1saWIvRDovVXNlcnMvZGFuaS9EZXNrdG9wL0Btb25vZ3JpZC9ub2RlX21vZHVsZXMvbG9kYXNoL2lzQXJndW1lbnRzLmpzIiwid2VicGFjazovL3Z1ZS1saWIvRDovVXNlcnMvZGFuaS9EZXNrdG9wL0Btb25vZ3JpZC9ub2RlX21vZHVsZXMvbG9kYXNoL2lzQXJyYXkuanMiLCJ3ZWJwYWNrOi8vdnVlLWxpYi9EOi9Vc2Vycy9kYW5pL0Rlc2t0b3AvQG1vbm9ncmlkL25vZGVfbW9kdWxlcy9sb2Rhc2gvaXNBcnJheUxpa2UuanMiLCJ3ZWJwYWNrOi8vdnVlLWxpYi9EOi9Vc2Vycy9kYW5pL0Rlc2t0b3AvQG1vbm9ncmlkL25vZGVfbW9kdWxlcy9sb2Rhc2gvaXNCdWZmZXIuanMiLCJ3ZWJwYWNrOi8vdnVlLWxpYi9EOi9Vc2Vycy9kYW5pL0Rlc2t0b3AvQG1vbm9ncmlkL25vZGVfbW9kdWxlcy9sb2Rhc2gvaXNGdW5jdGlvbi5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL2xvZGFzaC9pc0xlbmd0aC5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL2xvZGFzaC9pc09iamVjdC5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL2xvZGFzaC9pc09iamVjdExpa2UuanMiLCJ3ZWJwYWNrOi8vdnVlLWxpYi9EOi9Vc2Vycy9kYW5pL0Rlc2t0b3AvQG1vbm9ncmlkL25vZGVfbW9kdWxlcy9sb2Rhc2gvaXNTeW1ib2wuanMiLCJ3ZWJwYWNrOi8vdnVlLWxpYi9EOi9Vc2Vycy9kYW5pL0Rlc2t0b3AvQG1vbm9ncmlkL25vZGVfbW9kdWxlcy9sb2Rhc2gvaXNUeXBlZEFycmF5LmpzIiwid2VicGFjazovL3Z1ZS1saWIvRDovVXNlcnMvZGFuaS9EZXNrdG9wL0Btb25vZ3JpZC9ub2RlX21vZHVsZXMvbG9kYXNoL2tleXMuanMiLCJ3ZWJwYWNrOi8vdnVlLWxpYi9EOi9Vc2Vycy9kYW5pL0Rlc2t0b3AvQG1vbm9ncmlkL25vZGVfbW9kdWxlcy9sb2Rhc2gvbWVtb2l6ZS5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL2xvZGFzaC9wcm9wZXJ0eS5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL2xvZGFzaC9zdHViQXJyYXkuanMiLCJ3ZWJwYWNrOi8vdnVlLWxpYi9EOi9Vc2Vycy9kYW5pL0Rlc2t0b3AvQG1vbm9ncmlkL25vZGVfbW9kdWxlcy9sb2Rhc2gvc3R1YkZhbHNlLmpzIiwid2VicGFjazovL3Z1ZS1saWIvRDovVXNlcnMvZGFuaS9EZXNrdG9wL0Btb25vZ3JpZC9ub2RlX21vZHVsZXMvbG9kYXNoL3RvRmluaXRlLmpzIiwid2VicGFjazovL3Z1ZS1saWIvRDovVXNlcnMvZGFuaS9EZXNrdG9wL0Btb25vZ3JpZC9ub2RlX21vZHVsZXMvbG9kYXNoL3RvSW50ZWdlci5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL2xvZGFzaC90b051bWJlci5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL2xvZGFzaC90b1N0cmluZy5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3J1bnRpbWUvY29tcG9uZW50Tm9ybWFsaXplci5qcyIsIndlYnBhY2s6Ly92dWUtbGliLy4vc3JjL2NvbXBvbmVudHMvQ29udHJvbEtpdC52dWU/NDgzYyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL3Z1ZS1zdHlsZS1sb2FkZXIvbGliL2FkZFN0eWxlc0NsaWVudC5qcyIsIndlYnBhY2s6Ly92dWUtbGliL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL3Z1ZS1zdHlsZS1sb2FkZXIvbGliL2xpc3RUb1N0eWxlcy5qcyIsIndlYnBhY2s6Ly92dWUtbGliLyh3ZWJwYWNrKS9idWlsZGluL2dsb2JhbC5qcyIsIndlYnBhY2s6Ly92dWUtbGliLyh3ZWJwYWNrKS9idWlsZGluL21vZHVsZS5qcyIsIndlYnBhY2s6Ly92dWUtbGliLy4vc3JjL2NvbXBvbmVudHMvQ29udHJvbEtpdC52dWUiLCJ3ZWJwYWNrOi8vdnVlLWxpYi8uL3NyYy9jb21wb25lbnRzL0NvbnRyb2xLaXQudnVlPzY1OGEiLCJ3ZWJwYWNrOi8vdnVlLWxpYi8uL3NyYy9jb21wb25lbnRzL0NvbnRyb2xLaXQudnVlP2VkZDgiLCJ3ZWJwYWNrOi8vdnVlLWxpYi9leHRlcm5hbCBcIkBtb25vZ3JpZC9qcy11dGlsc1wiIl0sIm5hbWVzIjpbIl90eXBlb2YiLCJvYmoiLCJTeW1ib2wiLCJpdGVyYXRvciIsIm1vZHVsZSIsImV4cG9ydHMiLCJjb25zdHJ1Y3RvciIsInByb3RvdHlwZSIsInVzZVNvdXJjZU1hcCIsImxpc3QiLCJ0b1N0cmluZyIsIm1hcCIsIml0ZW0iLCJjb250ZW50IiwiY3NzV2l0aE1hcHBpbmdUb1N0cmluZyIsImNvbmNhdCIsImpvaW4iLCJpIiwibW9kdWxlcyIsIm1lZGlhUXVlcnkiLCJkZWR1cGUiLCJhbHJlYWR5SW1wb3J0ZWRNb2R1bGVzIiwibGVuZ3RoIiwiaWQiLCJfaSIsInB1c2giLCJjc3NNYXBwaW5nIiwiYnRvYSIsInNvdXJjZU1hcHBpbmciLCJ0b0NvbW1lbnQiLCJzb3VyY2VVUkxzIiwic291cmNlcyIsInNvdXJjZSIsInNvdXJjZVJvb3QiLCJzb3VyY2VNYXAiLCJiYXNlNjQiLCJ1bmVzY2FwZSIsImVuY29kZVVSSUNvbXBvbmVudCIsIkpTT04iLCJzdHJpbmdpZnkiLCJkYXRhIiwiZ2V0TmF0aXZlIiwicmVxdWlyZSIsInJvb3QiLCJEYXRhVmlldyIsImhhc2hDbGVhciIsImhhc2hEZWxldGUiLCJoYXNoR2V0IiwiaGFzaEhhcyIsImhhc2hTZXQiLCJIYXNoIiwiZW50cmllcyIsImluZGV4IiwiY2xlYXIiLCJlbnRyeSIsInNldCIsImdldCIsImhhcyIsImxpc3RDYWNoZUNsZWFyIiwibGlzdENhY2hlRGVsZXRlIiwibGlzdENhY2hlR2V0IiwibGlzdENhY2hlSGFzIiwibGlzdENhY2hlU2V0IiwiTGlzdENhY2hlIiwiTWFwIiwibWFwQ2FjaGVDbGVhciIsIm1hcENhY2hlRGVsZXRlIiwibWFwQ2FjaGVHZXQiLCJtYXBDYWNoZUhhcyIsIm1hcENhY2hlU2V0IiwiTWFwQ2FjaGUiLCJQcm9taXNlIiwiU2V0Iiwic2V0Q2FjaGVBZGQiLCJzZXRDYWNoZUhhcyIsIlNldENhY2hlIiwidmFsdWVzIiwiX19kYXRhX18iLCJhZGQiLCJzdGFja0NsZWFyIiwic3RhY2tEZWxldGUiLCJzdGFja0dldCIsInN0YWNrSGFzIiwic3RhY2tTZXQiLCJTdGFjayIsInNpemUiLCJVaW50OEFycmF5IiwiV2Vha01hcCIsImFycmF5RmlsdGVyIiwiYXJyYXkiLCJwcmVkaWNhdGUiLCJyZXNJbmRleCIsInJlc3VsdCIsInZhbHVlIiwiYmFzZVRpbWVzIiwiaXNBcmd1bWVudHMiLCJpc0FycmF5IiwiaXNCdWZmZXIiLCJpc0luZGV4IiwiaXNUeXBlZEFycmF5Iiwib2JqZWN0UHJvdG8iLCJPYmplY3QiLCJoYXNPd25Qcm9wZXJ0eSIsImFycmF5TGlrZUtleXMiLCJpbmhlcml0ZWQiLCJpc0FyciIsImlzQXJnIiwiaXNCdWZmIiwiaXNUeXBlIiwic2tpcEluZGV4ZXMiLCJTdHJpbmciLCJrZXkiLCJjYWxsIiwiYXJyYXlNYXAiLCJpdGVyYXRlZSIsIkFycmF5IiwiYXJyYXlQdXNoIiwib2Zmc2V0IiwiYXJyYXlTb21lIiwiZXEiLCJhc3NvY0luZGV4T2YiLCJiYXNlRmluZEluZGV4IiwiZnJvbUluZGV4IiwiZnJvbVJpZ2h0IiwiY2FzdFBhdGgiLCJ0b0tleSIsImJhc2VHZXQiLCJvYmplY3QiLCJwYXRoIiwidW5kZWZpbmVkIiwiYmFzZUdldEFsbEtleXMiLCJrZXlzRnVuYyIsInN5bWJvbHNGdW5jIiwiZ2V0UmF3VGFnIiwib2JqZWN0VG9TdHJpbmciLCJudWxsVGFnIiwidW5kZWZpbmVkVGFnIiwic3ltVG9TdHJpbmdUYWciLCJ0b1N0cmluZ1RhZyIsImJhc2VHZXRUYWciLCJiYXNlSGFzSW4iLCJpc09iamVjdExpa2UiLCJhcmdzVGFnIiwiYmFzZUlzQXJndW1lbnRzIiwiYmFzZUlzRXF1YWxEZWVwIiwiYmFzZUlzRXF1YWwiLCJvdGhlciIsImJpdG1hc2siLCJjdXN0b21pemVyIiwic3RhY2siLCJlcXVhbEFycmF5cyIsImVxdWFsQnlUYWciLCJlcXVhbE9iamVjdHMiLCJnZXRUYWciLCJDT01QQVJFX1BBUlRJQUxfRkxBRyIsImFycmF5VGFnIiwib2JqZWN0VGFnIiwiZXF1YWxGdW5jIiwib2JqSXNBcnIiLCJvdGhJc0FyciIsIm9ialRhZyIsIm90aFRhZyIsIm9iaklzT2JqIiwib3RoSXNPYmoiLCJpc1NhbWVUYWciLCJvYmpJc1dyYXBwZWQiLCJvdGhJc1dyYXBwZWQiLCJvYmpVbndyYXBwZWQiLCJvdGhVbndyYXBwZWQiLCJDT01QQVJFX1VOT1JERVJFRF9GTEFHIiwiYmFzZUlzTWF0Y2giLCJtYXRjaERhdGEiLCJub0N1c3RvbWl6ZXIiLCJvYmpWYWx1ZSIsInNyY1ZhbHVlIiwiaXNGdW5jdGlvbiIsImlzTWFza2VkIiwiaXNPYmplY3QiLCJ0b1NvdXJjZSIsInJlUmVnRXhwQ2hhciIsInJlSXNIb3N0Q3RvciIsImZ1bmNQcm90byIsIkZ1bmN0aW9uIiwiZnVuY1RvU3RyaW5nIiwicmVJc05hdGl2ZSIsIlJlZ0V4cCIsInJlcGxhY2UiLCJiYXNlSXNOYXRpdmUiLCJwYXR0ZXJuIiwidGVzdCIsImlzTGVuZ3RoIiwiYm9vbFRhZyIsImRhdGVUYWciLCJlcnJvclRhZyIsImZ1bmNUYWciLCJtYXBUYWciLCJudW1iZXJUYWciLCJyZWdleHBUYWciLCJzZXRUYWciLCJzdHJpbmdUYWciLCJ3ZWFrTWFwVGFnIiwiYXJyYXlCdWZmZXJUYWciLCJkYXRhVmlld1RhZyIsImZsb2F0MzJUYWciLCJmbG9hdDY0VGFnIiwiaW50OFRhZyIsImludDE2VGFnIiwiaW50MzJUYWciLCJ1aW50OFRhZyIsInVpbnQ4Q2xhbXBlZFRhZyIsInVpbnQxNlRhZyIsInVpbnQzMlRhZyIsInR5cGVkQXJyYXlUYWdzIiwiYmFzZUlzVHlwZWRBcnJheSIsImJhc2VNYXRjaGVzIiwiYmFzZU1hdGNoZXNQcm9wZXJ0eSIsImlkZW50aXR5IiwicHJvcGVydHkiLCJiYXNlSXRlcmF0ZWUiLCJpc1Byb3RvdHlwZSIsIm5hdGl2ZUtleXMiLCJiYXNlS2V5cyIsImdldE1hdGNoRGF0YSIsIm1hdGNoZXNTdHJpY3RDb21wYXJhYmxlIiwiaGFzSW4iLCJpc0tleSIsImlzU3RyaWN0Q29tcGFyYWJsZSIsImJhc2VQcm9wZXJ0eSIsImJhc2VQcm9wZXJ0eURlZXAiLCJuIiwiaXNTeW1ib2wiLCJJTkZJTklUWSIsInN5bWJvbFByb3RvIiwic3ltYm9sVG9TdHJpbmciLCJiYXNlVG9TdHJpbmciLCJiYXNlVW5hcnkiLCJmdW5jIiwiY2FjaGVIYXMiLCJjYWNoZSIsInN0cmluZ1RvUGF0aCIsImNvcmVKc0RhdGEiLCJpc0FycmF5TGlrZSIsImtleXMiLCJjcmVhdGVGaW5kIiwiZmluZEluZGV4RnVuYyIsImNvbGxlY3Rpb24iLCJpdGVyYWJsZSIsImlzUGFydGlhbCIsImFyckxlbmd0aCIsIm90aExlbmd0aCIsInN0YWNrZWQiLCJzZWVuIiwiYXJyVmFsdWUiLCJvdGhWYWx1ZSIsImNvbXBhcmVkIiwib3RoSW5kZXgiLCJtYXBUb0FycmF5Iiwic2V0VG9BcnJheSIsInN5bWJvbFRhZyIsInN5bWJvbFZhbHVlT2YiLCJ2YWx1ZU9mIiwidGFnIiwiYnl0ZUxlbmd0aCIsImJ5dGVPZmZzZXQiLCJidWZmZXIiLCJuYW1lIiwibWVzc2FnZSIsImNvbnZlcnQiLCJnZXRBbGxLZXlzIiwib2JqUHJvcHMiLCJvYmpMZW5ndGgiLCJvdGhQcm9wcyIsInNraXBDdG9yIiwib2JqQ3RvciIsIm90aEN0b3IiLCJmcmVlR2xvYmFsIiwiZ2xvYmFsIiwiZ2V0U3ltYm9scyIsImlzS2V5YWJsZSIsImdldE1hcERhdGEiLCJnZXRWYWx1ZSIsIm5hdGl2ZU9iamVjdFRvU3RyaW5nIiwiaXNPd24iLCJ1bm1hc2tlZCIsImUiLCJzdHViQXJyYXkiLCJwcm9wZXJ0eUlzRW51bWVyYWJsZSIsIm5hdGl2ZUdldFN5bWJvbHMiLCJnZXRPd25Qcm9wZXJ0eVN5bWJvbHMiLCJzeW1ib2wiLCJwcm9taXNlVGFnIiwiZGF0YVZpZXdDdG9yU3RyaW5nIiwibWFwQ3RvclN0cmluZyIsInByb21pc2VDdG9yU3RyaW5nIiwic2V0Q3RvclN0cmluZyIsIndlYWtNYXBDdG9yU3RyaW5nIiwiQXJyYXlCdWZmZXIiLCJyZXNvbHZlIiwiQ3RvciIsImN0b3JTdHJpbmciLCJoYXNQYXRoIiwiaGFzRnVuYyIsIm5hdGl2ZUNyZWF0ZSIsIkhBU0hfVU5ERUZJTkVEIiwiTUFYX1NBRkVfSU5URUdFUiIsInJlSXNVaW50IiwidHlwZSIsInJlSXNEZWVwUHJvcCIsInJlSXNQbGFpblByb3AiLCJtYXNrU3JjS2V5IiwidWlkIiwiZXhlYyIsIklFX1BST1RPIiwicHJvdG8iLCJhcnJheVByb3RvIiwic3BsaWNlIiwibGFzdEluZGV4IiwicG9wIiwiZm9yRWFjaCIsIm1lbW9pemUiLCJNQVhfTUVNT0laRV9TSVpFIiwibWVtb2l6ZUNhcHBlZCIsIm92ZXJBcmciLCJmcmVlRXhwb3J0cyIsIm5vZGVUeXBlIiwiZnJlZU1vZHVsZSIsIm1vZHVsZUV4cG9ydHMiLCJmcmVlUHJvY2VzcyIsInByb2Nlc3MiLCJub2RlVXRpbCIsInR5cGVzIiwiYmluZGluZyIsInRyYW5zZm9ybSIsImFyZyIsImZyZWVTZWxmIiwic2VsZiIsIkxBUkdFX0FSUkFZX1NJWkUiLCJwYWlycyIsInJlUHJvcE5hbWUiLCJyZUVzY2FwZUNoYXIiLCJzdHJpbmciLCJjaGFyQ29kZUF0IiwibWF0Y2giLCJudW1iZXIiLCJxdW90ZSIsInN1YlN0cmluZyIsImZpbmRJbmRleCIsImZpbmQiLCJ0b0ludGVnZXIiLCJuYXRpdmVNYXgiLCJNYXRoIiwibWF4IiwiZGVmYXVsdFZhbHVlIiwiYXJndW1lbnRzIiwic3R1YkZhbHNlIiwiQnVmZmVyIiwibmF0aXZlSXNCdWZmZXIiLCJhc3luY1RhZyIsImdlblRhZyIsInByb3h5VGFnIiwibm9kZUlzVHlwZWRBcnJheSIsIkZVTkNfRVJST1JfVEVYVCIsInJlc29sdmVyIiwiVHlwZUVycm9yIiwibWVtb2l6ZWQiLCJhcmdzIiwiYXBwbHkiLCJDYWNoZSIsInRvTnVtYmVyIiwiTUFYX0lOVEVHRVIiLCJ0b0Zpbml0ZSIsInNpZ24iLCJyZW1haW5kZXIiLCJOQU4iLCJyZVRyaW0iLCJyZUlzQmFkSGV4IiwicmVJc0JpbmFyeSIsInJlSXNPY3RhbCIsImZyZWVQYXJzZUludCIsInBhcnNlSW50IiwiaXNCaW5hcnkiLCJzbGljZSIsImxpc3RUb1N0eWxlcyIsInBhcmVudElkIiwic3R5bGVzIiwibmV3U3R5bGVzIiwiY3NzIiwibWVkaWEiLCJwYXJ0IiwicGFydHMiLCJnIiwid2luZG93Iiwid2VicGFja1BvbHlmaWxsIiwiZGVwcmVjYXRlIiwicGF0aHMiLCJjaGlsZHJlbiIsImRlZmluZVByb3BlcnR5IiwiZW51bWVyYWJsZSIsImwiXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUM7QUFDRCxPO1FDVkE7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7OztRQUdBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSwwQ0FBMEMsZ0NBQWdDO1FBQzFFO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0Esd0RBQXdELGtCQUFrQjtRQUMxRTtRQUNBLGlEQUFpRCxjQUFjO1FBQy9EOztRQUVBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQSx5Q0FBeUMsaUNBQWlDO1FBQzFFLGdIQUFnSCxtQkFBbUIsRUFBRTtRQUNySTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLDJCQUEyQiwwQkFBMEIsRUFBRTtRQUN2RCxpQ0FBaUMsZUFBZTtRQUNoRDtRQUNBO1FBQ0E7O1FBRUE7UUFDQSxzREFBc0QsK0RBQStEOztRQUVySDtRQUNBOzs7UUFHQTtRQUNBOzs7Ozs7Ozs7Ozs7QUNsRkEsU0FBU0EsT0FBVCxDQUFpQkMsR0FBakIsRUFBc0I7QUFDcEI7O0FBRUEsTUFBSSxPQUFPQyxNQUFQLEtBQWtCLFVBQWxCLElBQWdDLE9BQU9BLE1BQU0sQ0FBQ0MsUUFBZCxLQUEyQixRQUEvRCxFQUF5RTtBQUN2RUMsVUFBTSxDQUFDQyxPQUFQLEdBQWlCTCxPQUFPLEdBQUcsU0FBU0EsT0FBVCxDQUFpQkMsR0FBakIsRUFBc0I7QUFDL0MsYUFBTyxPQUFPQSxHQUFkO0FBQ0QsS0FGRDtBQUdELEdBSkQsTUFJTztBQUNMRyxVQUFNLENBQUNDLE9BQVAsR0FBaUJMLE9BQU8sR0FBRyxTQUFTQSxPQUFULENBQWlCQyxHQUFqQixFQUFzQjtBQUMvQyxhQUFPQSxHQUFHLElBQUksT0FBT0MsTUFBUCxLQUFrQixVQUF6QixJQUF1Q0QsR0FBRyxDQUFDSyxXQUFKLEtBQW9CSixNQUEzRCxJQUFxRUQsR0FBRyxLQUFLQyxNQUFNLENBQUNLLFNBQXBGLEdBQWdHLFFBQWhHLEdBQTJHLE9BQU9OLEdBQXpIO0FBQ0QsS0FGRDtBQUdEOztBQUVELFNBQU9ELE9BQU8sQ0FBQ0MsR0FBRCxDQUFkO0FBQ0Q7O0FBRURHLE1BQU0sQ0FBQ0MsT0FBUCxHQUFpQkwsT0FBakIsQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNmQTtBQUNBO0FBRUE7QUFDQSxVQURBO0FBRUEsU0FGQTtBQUdBO0FBSEE7QUFLQTs7Ozs7Ozs7QUFPQTtBQUNBLG9CQURBO0FBR0Esa0JBSEE7QUFLQTtBQUNBOzs7Ozs7QUFNQTtBQUNBO0FBREEsS0FQQTs7QUFVQTs7O0FBR0E7QUFDQTtBQURBLEtBYkE7O0FBZ0JBOzs7O0FBSUE7QUFDQSxpQkFEQTtBQUVBO0FBRkEsS0FwQkE7O0FBd0JBOzs7QUFHQTtBQUNBLGtCQURBO0FBRUEscUJBRkE7QUFHQTtBQUhBLEtBM0JBOztBQWdDQTs7OztBQUlBO0FBQ0Esa0JBREE7QUFFQSxxQkFGQTtBQUdBLGFBSEEsc0JBR0E7QUFDQTtBQUNBO0FBTEE7QUFwQ0EsR0FMQTtBQWtEQSxRQWxEQSxrQkFrREEsQ0FsREEsRUFrREEsT0FsREEsRUFrREE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUhBOztBQUtBO0FBQ0E7QUFFQTs7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUZBO0FBR0E7O0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQUEsaUJBQ0E7QUFEQTtBQUFBO0FBQUEsZ0JBR0EsSUFIQTtBQUFBLGtCQUtBLE9BTEE7QUFBQSxpQkFNQSxXQU5BO0FBQUEsaUJBT0EsV0FQQTtBQUFBLGtCQVFBO0FBUkE7QUFBQSxpQkFJQSxlQUpBO0FBQUE7QUFBQSxtQkFTQTtBQVRBO0FBQUE7QUFBQSxtQkFVQTtBQUFBO0FBQUEsV0FWQTtBQUFBLG9CQVdBO0FBQUE7QUFBQTtBQVhBO0FBQUE7QUFBQSxpQkFjQSxjQWRBO0FBQUE7QUFBQSxtQkFlQTtBQWZBO0FBQUE7QUFBQSxrQkFnQkEsWUFoQkE7QUFBQSxrQkFpQkE7QUFqQkE7QUFBQTtBQUFBLG9CQWtCQTtBQUFBO0FBQUEsV0FsQkE7QUFBQSxtQkFtQkE7QUFBQTtBQUFBO0FBbkJBO0FBQUEsV0FiQSxDQW1DQTtBQUNBLEtBcENBOztBQXFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBOztBQURBLGlDQUVBLElBRkE7QUFHQSx1REFIQSxDQUdBOztBQUNBLGdCQUFnQixxRUFBaEI7QUFDQTtBQUNBO0FBQUEsdUJBQ0E7QUFEQTtBQUFBO0FBQUEsdUJBRUE7QUFGQTtBQUFBLHVCQUVBO0FBRkEsZ0JBRUEsa0JBRkE7QUFBQSx1QkFHQTtBQUhBO0FBQUE7QUFBQSxzQkFJQTtBQUpBO0FBQUE7QUFBQSx5QkFJQTtBQUpBO0FBQUE7QUFBQSwwQkFJQTtBQUFBO0FBQUE7QUFKQTtBQUFBLG9CQURBLENBUUE7O0FBQ0E7O0FBQ0E7QUFDQTtBQUFBLHVCQUNBO0FBREE7QUFBQTtBQUFBLHVCQUVBO0FBRkE7QUFBQSx1QkFFQTtBQUZBLGdCQUVBLGtCQUZBO0FBQUEsdUJBR0E7QUFIQTtBQUFBO0FBQUEsc0JBSUEsaUJBSkE7QUFBQSx3QkFJQTtBQUpBO0FBQUE7QUFBQSwyQkFJQTtBQUpBO0FBQUE7QUFBQSwwQkFJQTtBQUFBO0FBQUE7QUFKQTtBQUFBLG9CQURBLENBUUE7O0FBQ0E7O0FBQ0E7QUFDQTtBQUFBLHVCQUNBO0FBREE7QUFBQTtBQUFBLHVCQUVBO0FBRkE7QUFBQSx1QkFFQTtBQUZBLGdCQUVBLGtCQUZBO0FBQUEsdUJBR0E7QUFIQSxnQkFHQSxtREFIQSxNQURBLENBTUE7O0FBQ0E7O0FBQ0E7QUFDQTtBQUFBLHVCQUNBO0FBREE7QUFBQTtBQUFBLHVCQUVBO0FBRkE7QUFBQSx1QkFFQTtBQUZBO0FBQUEsdUJBR0E7QUFIQTtBQUFBO0FBQUEsc0JBSUE7QUFKQTtBQUFBO0FBQUEseUJBSUE7QUFKQTtBQUFBLGdCQUlBLGtCQUpBLFFBREEsQ0FRQTs7QUFDQTs7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUZBLE1BRUE7QUFDQTtBQUNBO0FBQ0E7O0FBQ0E7QUFDQTtBQUNBO0FBQUEsaUNBQ0E7QUFEQTtBQUFBO0FBQUEsaUNBRUE7QUFGQTtBQUFBLGlDQUVBO0FBRkEsMEJBRUEsa0JBRkE7QUFBQSxpQ0FHQTtBQUhBLDBCQUdBLDBEQUhBLE1BREEsQ0FNQTs7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBQ0E7QUE1REE7QUFKQTs7QUFFQTtBQUFBOztBQUFBLGlDQUNBO0FBK0RBOztBQUNBO0FBQUEsaUJBQ0E7QUFEQTtBQUFBO0FBQUEsZ0JBRUEsSUFGQTtBQUFBLGtCQUVBO0FBRkE7QUFBQSxpQkFFQTtBQUZBO0FBQUE7QUFBQSxpQkFHQTtBQUhBO0FBQUEsaUJBR0E7QUFIQSxVQUdBLG1CQUhBO0FBQUEsaUJBSUE7QUFKQSxVQUtBLFFBTEEsTUFuRUEsQ0EyRUE7QUFDQSxLQTVFQTs7QUE2RUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBN0tBLEc7Ozs7Ozs7Ozs7O0FDaEJBO0FBQ0Esa0NBQWtDLG1CQUFPLENBQUMsa0hBQXlEO0FBQ25HO0FBQ0E7QUFDQSxjQUFjLFFBQVMsbUJBQW1CLHFCQUFxQixtQ0FBbUMsbUNBQW1DLG1CQUFtQiwyQkFBMkIsb0JBQW9CLG1CQUFtQixxQkFBcUIsdUJBQXVCLHFCQUFxQix1QkFBdUIsYUFBYSxXQUFXLGlCQUFpQiw2VkFBNlYsb0NBQW9DLG1DQUFtQyxtQ0FBbUMsR0FBRywyREFBMkQsa0JBQWtCLEdBQUcsd0JBQXdCLG1DQUFtQyxtQ0FBbUMsb0JBQW9CLG1CQUFtQixHQUFHLHVDQUF1QyxtQ0FBbUMsbUNBQW1DLGtCQUFrQixjQUFjLHFCQUFxQixHQUFHLHVFQUF1RSxtQkFBbUIsaUJBQWlCLEdBQUcsMENBQTBDLHVCQUF1QixvQkFBb0Isc0JBQXNCLHVCQUF1QixnQkFBZ0IsR0FBRyxvQ0FBb0Msd0JBQXdCLHVEQUF1RCxxREFBcUQsdUJBQXVCLHNEQUFzRCxvREFBb0QsbUJBQW1CLHNCQUFzQixpQkFBaUIsR0FBRyw0Q0FBNEMseUNBQXlDLHdDQUF3QyxzQ0FBc0MsbUJBQW1CLDBCQUEwQix5QkFBeUIsd0NBQXdDLHdDQUF3Qyx3REFBd0QsZ0RBQWdELHdDQUF3Qyx5RUFBeUUsMkJBQTJCLEdBQUcsNkNBQTZDLHNDQUFzQyxtQ0FBbUMsb0NBQW9DLGdCQUFnQixHQUFHLG1GQUFtRix1REFBdUQscURBQXFELHNEQUFzRCxHQUFHLHNEQUFzRCxxQkFBcUIsaUNBQWlDLGtDQUFrQyxHQUFHLDhEQUE4RCxzREFBc0Qsc0RBQXNELEdBQUcsOERBQThELDRCQUE0Qiw2QkFBNkIsNkJBQTZCLEdBQUcsd0JBQXdCLG1DQUFtQyxtQ0FBbUMsd0JBQXdCLHFCQUFxQixnQkFBZ0IsR0FBRyxvQ0FBb0MsOEJBQThCLEdBQUcsNkRBQTZELG1DQUFtQyxtQ0FBbUMsMEJBQTBCLDJCQUEyQixHQUFHLGdDQUFnQyxtQkFBbUIsZUFBZSxHQUFHLDhCQUE4QixzQkFBc0IsZUFBZSxHQUFHLDJDQUEyQyxtQ0FBbUMsbUNBQW1DLHVCQUF1Qix3QkFBd0IsZ0JBQWdCLEdBQUcsa0hBQWtILDBCQUEwQix1QkFBdUIsR0FBRywwREFBMEQsY0FBYyx1QkFBdUIsZUFBZSxlQUFlLEdBQUcseURBQXlELG9CQUFvQixpQkFBaUIsY0FBYyxlQUFlLEdBQUcseUJBQXlCLHdCQUF3QixnREFBZ0QsdUJBQXVCLG1CQUFtQixvQkFBb0Isb0JBQW9CLHNCQUFzQixHQUFHLDBEQUEwRCx3QkFBd0IsOEJBQThCLG1CQUFtQixxREFBcUQsaUJBQWlCLEdBQUcsdUNBQXVDLDZCQUE2Qiw2QkFBNkIsNkJBQTZCLHdCQUF3Qiw4QkFBOEIsb0JBQW9CLGlCQUFpQixjQUFjLGdCQUFnQixHQUFHLDhDQUE4Qyx3QkFBd0IsbUNBQW1DLG1DQUFtQyxrQkFBa0Isa0JBQWtCLGlCQUFpQixnQkFBZ0IsR0FBRyxzREFBc0QsbUJBQW1CLEdBQUcsb0NBQW9DLDZCQUE2Qiw2QkFBNkIsNkJBQTZCLHFCQUFxQixjQUFjLG1CQUFtQixHQUFHLG1FQUFtRSx3QkFBd0IsOEJBQThCLG9CQUFvQixpQkFBaUIsR0FBRyxzREFBc0Qsd0JBQXdCLDhCQUE4QixvQkFBb0IsaUJBQWlCLEdBQUcsK0NBQStDLDRCQUE0QixjQUFjLHVCQUF1QixvQkFBb0IsaUJBQWlCLEdBQUcsMERBQTBELDZCQUE2Qiw2QkFBNkIsd0JBQXdCLDhCQUE4Qix1QkFBdUIsb0JBQW9CLGlCQUFpQixvQkFBb0IsZUFBZSxHQUFHLHNEQUFzRCx3QkFBd0IsOEJBQThCLHVCQUF1QixvQkFBb0IsaUJBQWlCLGVBQWUsR0FBRywrQ0FBK0Msd0JBQXdCLDhCQUE4Qix1QkFBdUIsb0JBQW9CLGlCQUFpQixlQUFlLEdBQUcsb0RBQW9ELHdCQUF3Qiw4QkFBOEIsR0FBRyxvREFBb0Qsd0JBQXdCLDhCQUE4QixHQUFHLDBDQUEwQyxrQkFBa0IsR0FBRyx5RUFBeUUsd0JBQXdCLEdBQUcsMERBQTBELHdCQUF3QixHQUFHLDBEQUEwRCx3QkFBd0IsR0FBRztBQUN4Z087QUFDQTs7Ozs7Ozs7Ozs7OztBQ05hO0FBRWI7Ozs7QUFJQTtBQUNBOztBQUNBSSxNQUFNLENBQUNDLE9BQVAsR0FBaUIsVUFBVUcsWUFBVixFQUF3QjtBQUN2QyxNQUFJQyxJQUFJLEdBQUcsRUFBWCxDQUR1QyxDQUN4Qjs7QUFFZkEsTUFBSSxDQUFDQyxRQUFMLEdBQWdCLFNBQVNBLFFBQVQsR0FBb0I7QUFDbEMsV0FBTyxLQUFLQyxHQUFMLENBQVMsVUFBVUMsSUFBVixFQUFnQjtBQUM5QixVQUFJQyxPQUFPLEdBQUdDLHNCQUFzQixDQUFDRixJQUFELEVBQU9KLFlBQVAsQ0FBcEM7O0FBRUEsVUFBSUksSUFBSSxDQUFDLENBQUQsQ0FBUixFQUFhO0FBQ1gsZUFBTyxVQUFVRyxNQUFWLENBQWlCSCxJQUFJLENBQUMsQ0FBRCxDQUFyQixFQUEwQixJQUExQixFQUFnQ0csTUFBaEMsQ0FBdUNGLE9BQXZDLEVBQWdELEdBQWhELENBQVA7QUFDRDs7QUFFRCxhQUFPQSxPQUFQO0FBQ0QsS0FSTSxFQVFKRyxJQVJJLENBUUMsRUFSRCxDQUFQO0FBU0QsR0FWRCxDQUh1QyxDQWFwQztBQUNIOzs7QUFHQVAsTUFBSSxDQUFDUSxDQUFMLEdBQVMsVUFBVUMsT0FBVixFQUFtQkMsVUFBbkIsRUFBK0JDLE1BQS9CLEVBQXVDO0FBQzlDLFFBQUksT0FBT0YsT0FBUCxLQUFtQixRQUF2QixFQUFpQztBQUMvQjtBQUNBQSxhQUFPLEdBQUcsQ0FBQyxDQUFDLElBQUQsRUFBT0EsT0FBUCxFQUFnQixFQUFoQixDQUFELENBQVY7QUFDRDs7QUFFRCxRQUFJRyxzQkFBc0IsR0FBRyxFQUE3Qjs7QUFFQSxRQUFJRCxNQUFKLEVBQVk7QUFDVixXQUFLLElBQUlILENBQUMsR0FBRyxDQUFiLEVBQWdCQSxDQUFDLEdBQUcsS0FBS0ssTUFBekIsRUFBaUNMLENBQUMsRUFBbEMsRUFBc0M7QUFDcEM7QUFDQSxZQUFJTSxFQUFFLEdBQUcsS0FBS04sQ0FBTCxFQUFRLENBQVIsQ0FBVDs7QUFFQSxZQUFJTSxFQUFFLElBQUksSUFBVixFQUFnQjtBQUNkRixnQ0FBc0IsQ0FBQ0UsRUFBRCxDQUF0QixHQUE2QixJQUE3QjtBQUNEO0FBQ0Y7QUFDRjs7QUFFRCxTQUFLLElBQUlDLEVBQUUsR0FBRyxDQUFkLEVBQWlCQSxFQUFFLEdBQUdOLE9BQU8sQ0FBQ0ksTUFBOUIsRUFBc0NFLEVBQUUsRUFBeEMsRUFBNEM7QUFDMUMsVUFBSVosSUFBSSxHQUFHLEdBQUdHLE1BQUgsQ0FBVUcsT0FBTyxDQUFDTSxFQUFELENBQWpCLENBQVg7O0FBRUEsVUFBSUosTUFBTSxJQUFJQyxzQkFBc0IsQ0FBQ1QsSUFBSSxDQUFDLENBQUQsQ0FBTCxDQUFwQyxFQUErQztBQUM3QztBQUNBO0FBQ0Q7O0FBRUQsVUFBSU8sVUFBSixFQUFnQjtBQUNkLFlBQUksQ0FBQ1AsSUFBSSxDQUFDLENBQUQsQ0FBVCxFQUFjO0FBQ1pBLGNBQUksQ0FBQyxDQUFELENBQUosR0FBVU8sVUFBVjtBQUNELFNBRkQsTUFFTztBQUNMUCxjQUFJLENBQUMsQ0FBRCxDQUFKLEdBQVUsR0FBR0csTUFBSCxDQUFVSSxVQUFWLEVBQXNCLE9BQXRCLEVBQStCSixNQUEvQixDQUFzQ0gsSUFBSSxDQUFDLENBQUQsQ0FBMUMsQ0FBVjtBQUNEO0FBQ0Y7O0FBRURILFVBQUksQ0FBQ2dCLElBQUwsQ0FBVWIsSUFBVjtBQUNEO0FBQ0YsR0FyQ0Q7O0FBdUNBLFNBQU9ILElBQVA7QUFDRCxDQXpERDs7QUEyREEsU0FBU0ssc0JBQVQsQ0FBZ0NGLElBQWhDLEVBQXNDSixZQUF0QyxFQUFvRDtBQUNsRCxNQUFJSyxPQUFPLEdBQUdELElBQUksQ0FBQyxDQUFELENBQUosSUFBVyxFQUF6QixDQURrRCxDQUNyQjs7QUFFN0IsTUFBSWMsVUFBVSxHQUFHZCxJQUFJLENBQUMsQ0FBRCxDQUFyQjs7QUFFQSxNQUFJLENBQUNjLFVBQUwsRUFBaUI7QUFDZixXQUFPYixPQUFQO0FBQ0Q7O0FBRUQsTUFBSUwsWUFBWSxJQUFJLE9BQU9tQixJQUFQLEtBQWdCLFVBQXBDLEVBQWdEO0FBQzlDLFFBQUlDLGFBQWEsR0FBR0MsU0FBUyxDQUFDSCxVQUFELENBQTdCO0FBQ0EsUUFBSUksVUFBVSxHQUFHSixVQUFVLENBQUNLLE9BQVgsQ0FBbUJwQixHQUFuQixDQUF1QixVQUFVcUIsTUFBVixFQUFrQjtBQUN4RCxhQUFPLGlCQUFpQmpCLE1BQWpCLENBQXdCVyxVQUFVLENBQUNPLFVBQVgsSUFBeUIsRUFBakQsRUFBcURsQixNQUFyRCxDQUE0RGlCLE1BQTVELEVBQW9FLEtBQXBFLENBQVA7QUFDRCxLQUZnQixDQUFqQjtBQUdBLFdBQU8sQ0FBQ25CLE9BQUQsRUFBVUUsTUFBVixDQUFpQmUsVUFBakIsRUFBNkJmLE1BQTdCLENBQW9DLENBQUNhLGFBQUQsQ0FBcEMsRUFBcURaLElBQXJELENBQTBELElBQTFELENBQVA7QUFDRDs7QUFFRCxTQUFPLENBQUNILE9BQUQsRUFBVUcsSUFBVixDQUFlLElBQWYsQ0FBUDtBQUNELEMsQ0FBQzs7O0FBR0YsU0FBU2EsU0FBVCxDQUFtQkssU0FBbkIsRUFBOEI7QUFDNUI7QUFDQSxNQUFJQyxNQUFNLEdBQUdSLElBQUksQ0FBQ1MsUUFBUSxDQUFDQyxrQkFBa0IsQ0FBQ0MsSUFBSSxDQUFDQyxTQUFMLENBQWVMLFNBQWYsQ0FBRCxDQUFuQixDQUFULENBQWpCO0FBQ0EsTUFBSU0sSUFBSSxHQUFHLCtEQUErRHpCLE1BQS9ELENBQXNFb0IsTUFBdEUsQ0FBWDtBQUNBLFNBQU8sT0FBT3BCLE1BQVAsQ0FBY3lCLElBQWQsRUFBb0IsS0FBcEIsQ0FBUDtBQUNELEM7Ozs7Ozs7Ozs7O0FDN0ZELElBQUlDLFNBQVMsR0FBR0MsbUJBQU8sQ0FBQyw2REFBRCxDQUF2QjtBQUFBLElBQ0lDLElBQUksR0FBR0QsbUJBQU8sQ0FBQyxtREFBRCxDQURsQjtBQUdBOzs7QUFDQSxJQUFJRSxRQUFRLEdBQUdILFNBQVMsQ0FBQ0UsSUFBRCxFQUFPLFVBQVAsQ0FBeEI7QUFFQXZDLE1BQU0sQ0FBQ0MsT0FBUCxHQUFpQnVDLFFBQWpCLEM7Ozs7Ozs7Ozs7O0FDTkEsSUFBSUMsU0FBUyxHQUFHSCxtQkFBTyxDQUFDLDZEQUFELENBQXZCO0FBQUEsSUFDSUksVUFBVSxHQUFHSixtQkFBTyxDQUFDLCtEQUFELENBRHhCO0FBQUEsSUFFSUssT0FBTyxHQUFHTCxtQkFBTyxDQUFDLHlEQUFELENBRnJCO0FBQUEsSUFHSU0sT0FBTyxHQUFHTixtQkFBTyxDQUFDLHlEQUFELENBSHJCO0FBQUEsSUFJSU8sT0FBTyxHQUFHUCxtQkFBTyxDQUFDLHlEQUFELENBSnJCO0FBTUE7Ozs7Ozs7OztBQU9BLFNBQVNRLElBQVQsQ0FBY0MsT0FBZCxFQUF1QjtBQUNyQixNQUFJQyxLQUFLLEdBQUcsQ0FBQyxDQUFiO0FBQUEsTUFDSTlCLE1BQU0sR0FBRzZCLE9BQU8sSUFBSSxJQUFYLEdBQWtCLENBQWxCLEdBQXNCQSxPQUFPLENBQUM3QixNQUQzQztBQUdBLE9BQUsrQixLQUFMOztBQUNBLFNBQU8sRUFBRUQsS0FBRixHQUFVOUIsTUFBakIsRUFBeUI7QUFDdkIsUUFBSWdDLEtBQUssR0FBR0gsT0FBTyxDQUFDQyxLQUFELENBQW5CO0FBQ0EsU0FBS0csR0FBTCxDQUFTRCxLQUFLLENBQUMsQ0FBRCxDQUFkLEVBQW1CQSxLQUFLLENBQUMsQ0FBRCxDQUF4QjtBQUNEO0FBQ0YsQyxDQUVEOzs7QUFDQUosSUFBSSxDQUFDM0MsU0FBTCxDQUFlOEMsS0FBZixHQUF1QlIsU0FBdkI7QUFDQUssSUFBSSxDQUFDM0MsU0FBTCxDQUFlLFFBQWYsSUFBMkJ1QyxVQUEzQjtBQUNBSSxJQUFJLENBQUMzQyxTQUFMLENBQWVpRCxHQUFmLEdBQXFCVCxPQUFyQjtBQUNBRyxJQUFJLENBQUMzQyxTQUFMLENBQWVrRCxHQUFmLEdBQXFCVCxPQUFyQjtBQUNBRSxJQUFJLENBQUMzQyxTQUFMLENBQWVnRCxHQUFmLEdBQXFCTixPQUFyQjtBQUVBN0MsTUFBTSxDQUFDQyxPQUFQLEdBQWlCNkMsSUFBakIsQzs7Ozs7Ozs7Ozs7QUMvQkEsSUFBSVEsY0FBYyxHQUFHaEIsbUJBQU8sQ0FBQyx1RUFBRCxDQUE1QjtBQUFBLElBQ0lpQixlQUFlLEdBQUdqQixtQkFBTyxDQUFDLHlFQUFELENBRDdCO0FBQUEsSUFFSWtCLFlBQVksR0FBR2xCLG1CQUFPLENBQUMsbUVBQUQsQ0FGMUI7QUFBQSxJQUdJbUIsWUFBWSxHQUFHbkIsbUJBQU8sQ0FBQyxtRUFBRCxDQUgxQjtBQUFBLElBSUlvQixZQUFZLEdBQUdwQixtQkFBTyxDQUFDLG1FQUFELENBSjFCO0FBTUE7Ozs7Ozs7OztBQU9BLFNBQVNxQixTQUFULENBQW1CWixPQUFuQixFQUE0QjtBQUMxQixNQUFJQyxLQUFLLEdBQUcsQ0FBQyxDQUFiO0FBQUEsTUFDSTlCLE1BQU0sR0FBRzZCLE9BQU8sSUFBSSxJQUFYLEdBQWtCLENBQWxCLEdBQXNCQSxPQUFPLENBQUM3QixNQUQzQztBQUdBLE9BQUsrQixLQUFMOztBQUNBLFNBQU8sRUFBRUQsS0FBRixHQUFVOUIsTUFBakIsRUFBeUI7QUFDdkIsUUFBSWdDLEtBQUssR0FBR0gsT0FBTyxDQUFDQyxLQUFELENBQW5CO0FBQ0EsU0FBS0csR0FBTCxDQUFTRCxLQUFLLENBQUMsQ0FBRCxDQUFkLEVBQW1CQSxLQUFLLENBQUMsQ0FBRCxDQUF4QjtBQUNEO0FBQ0YsQyxDQUVEOzs7QUFDQVMsU0FBUyxDQUFDeEQsU0FBVixDQUFvQjhDLEtBQXBCLEdBQTRCSyxjQUE1QjtBQUNBSyxTQUFTLENBQUN4RCxTQUFWLENBQW9CLFFBQXBCLElBQWdDb0QsZUFBaEM7QUFDQUksU0FBUyxDQUFDeEQsU0FBVixDQUFvQmlELEdBQXBCLEdBQTBCSSxZQUExQjtBQUNBRyxTQUFTLENBQUN4RCxTQUFWLENBQW9Ca0QsR0FBcEIsR0FBMEJJLFlBQTFCO0FBQ0FFLFNBQVMsQ0FBQ3hELFNBQVYsQ0FBb0JnRCxHQUFwQixHQUEwQk8sWUFBMUI7QUFFQTFELE1BQU0sQ0FBQ0MsT0FBUCxHQUFpQjBELFNBQWpCLEM7Ozs7Ozs7Ozs7O0FDL0JBLElBQUl0QixTQUFTLEdBQUdDLG1CQUFPLENBQUMsNkRBQUQsQ0FBdkI7QUFBQSxJQUNJQyxJQUFJLEdBQUdELG1CQUFPLENBQUMsbURBQUQsQ0FEbEI7QUFHQTs7O0FBQ0EsSUFBSXNCLEdBQUcsR0FBR3ZCLFNBQVMsQ0FBQ0UsSUFBRCxFQUFPLEtBQVAsQ0FBbkI7QUFFQXZDLE1BQU0sQ0FBQ0MsT0FBUCxHQUFpQjJELEdBQWpCLEM7Ozs7Ozs7Ozs7O0FDTkEsSUFBSUMsYUFBYSxHQUFHdkIsbUJBQU8sQ0FBQyxxRUFBRCxDQUEzQjtBQUFBLElBQ0l3QixjQUFjLEdBQUd4QixtQkFBTyxDQUFDLHVFQUFELENBRDVCO0FBQUEsSUFFSXlCLFdBQVcsR0FBR3pCLG1CQUFPLENBQUMsaUVBQUQsQ0FGekI7QUFBQSxJQUdJMEIsV0FBVyxHQUFHMUIsbUJBQU8sQ0FBQyxpRUFBRCxDQUh6QjtBQUFBLElBSUkyQixXQUFXLEdBQUczQixtQkFBTyxDQUFDLGlFQUFELENBSnpCO0FBTUE7Ozs7Ozs7OztBQU9BLFNBQVM0QixRQUFULENBQWtCbkIsT0FBbEIsRUFBMkI7QUFDekIsTUFBSUMsS0FBSyxHQUFHLENBQUMsQ0FBYjtBQUFBLE1BQ0k5QixNQUFNLEdBQUc2QixPQUFPLElBQUksSUFBWCxHQUFrQixDQUFsQixHQUFzQkEsT0FBTyxDQUFDN0IsTUFEM0M7QUFHQSxPQUFLK0IsS0FBTDs7QUFDQSxTQUFPLEVBQUVELEtBQUYsR0FBVTlCLE1BQWpCLEVBQXlCO0FBQ3ZCLFFBQUlnQyxLQUFLLEdBQUdILE9BQU8sQ0FBQ0MsS0FBRCxDQUFuQjtBQUNBLFNBQUtHLEdBQUwsQ0FBU0QsS0FBSyxDQUFDLENBQUQsQ0FBZCxFQUFtQkEsS0FBSyxDQUFDLENBQUQsQ0FBeEI7QUFDRDtBQUNGLEMsQ0FFRDs7O0FBQ0FnQixRQUFRLENBQUMvRCxTQUFULENBQW1COEMsS0FBbkIsR0FBMkJZLGFBQTNCO0FBQ0FLLFFBQVEsQ0FBQy9ELFNBQVQsQ0FBbUIsUUFBbkIsSUFBK0IyRCxjQUEvQjtBQUNBSSxRQUFRLENBQUMvRCxTQUFULENBQW1CaUQsR0FBbkIsR0FBeUJXLFdBQXpCO0FBQ0FHLFFBQVEsQ0FBQy9ELFNBQVQsQ0FBbUJrRCxHQUFuQixHQUF5QlcsV0FBekI7QUFDQUUsUUFBUSxDQUFDL0QsU0FBVCxDQUFtQmdELEdBQW5CLEdBQXlCYyxXQUF6QjtBQUVBakUsTUFBTSxDQUFDQyxPQUFQLEdBQWlCaUUsUUFBakIsQzs7Ozs7Ozs7Ozs7QUMvQkEsSUFBSTdCLFNBQVMsR0FBR0MsbUJBQU8sQ0FBQyw2REFBRCxDQUF2QjtBQUFBLElBQ0lDLElBQUksR0FBR0QsbUJBQU8sQ0FBQyxtREFBRCxDQURsQjtBQUdBOzs7QUFDQSxJQUFJNkIsT0FBTyxHQUFHOUIsU0FBUyxDQUFDRSxJQUFELEVBQU8sU0FBUCxDQUF2QjtBQUVBdkMsTUFBTSxDQUFDQyxPQUFQLEdBQWlCa0UsT0FBakIsQzs7Ozs7Ozs7Ozs7QUNOQSxJQUFJOUIsU0FBUyxHQUFHQyxtQkFBTyxDQUFDLDZEQUFELENBQXZCO0FBQUEsSUFDSUMsSUFBSSxHQUFHRCxtQkFBTyxDQUFDLG1EQUFELENBRGxCO0FBR0E7OztBQUNBLElBQUk4QixHQUFHLEdBQUcvQixTQUFTLENBQUNFLElBQUQsRUFBTyxLQUFQLENBQW5CO0FBRUF2QyxNQUFNLENBQUNDLE9BQVAsR0FBaUJtRSxHQUFqQixDOzs7Ozs7Ozs7OztBQ05BLElBQUlGLFFBQVEsR0FBRzVCLG1CQUFPLENBQUMsMkRBQUQsQ0FBdEI7QUFBQSxJQUNJK0IsV0FBVyxHQUFHL0IsbUJBQU8sQ0FBQyxpRUFBRCxDQUR6QjtBQUFBLElBRUlnQyxXQUFXLEdBQUdoQyxtQkFBTyxDQUFDLGlFQUFELENBRnpCO0FBSUE7Ozs7Ozs7Ozs7QUFRQSxTQUFTaUMsUUFBVCxDQUFrQkMsTUFBbEIsRUFBMEI7QUFDeEIsTUFBSXhCLEtBQUssR0FBRyxDQUFDLENBQWI7QUFBQSxNQUNJOUIsTUFBTSxHQUFHc0QsTUFBTSxJQUFJLElBQVYsR0FBaUIsQ0FBakIsR0FBcUJBLE1BQU0sQ0FBQ3RELE1BRHpDO0FBR0EsT0FBS3VELFFBQUwsR0FBZ0IsSUFBSVAsUUFBSixFQUFoQjs7QUFDQSxTQUFPLEVBQUVsQixLQUFGLEdBQVU5QixNQUFqQixFQUF5QjtBQUN2QixTQUFLd0QsR0FBTCxDQUFTRixNQUFNLENBQUN4QixLQUFELENBQWY7QUFDRDtBQUNGLEMsQ0FFRDs7O0FBQ0F1QixRQUFRLENBQUNwRSxTQUFULENBQW1CdUUsR0FBbkIsR0FBeUJILFFBQVEsQ0FBQ3BFLFNBQVQsQ0FBbUJrQixJQUFuQixHQUEwQmdELFdBQW5EO0FBQ0FFLFFBQVEsQ0FBQ3BFLFNBQVQsQ0FBbUJrRCxHQUFuQixHQUF5QmlCLFdBQXpCO0FBRUF0RSxNQUFNLENBQUNDLE9BQVAsR0FBaUJzRSxRQUFqQixDOzs7Ozs7Ozs7OztBQzFCQSxJQUFJWixTQUFTLEdBQUdyQixtQkFBTyxDQUFDLDZEQUFELENBQXZCO0FBQUEsSUFDSXFDLFVBQVUsR0FBR3JDLG1CQUFPLENBQUMsK0RBQUQsQ0FEeEI7QUFBQSxJQUVJc0MsV0FBVyxHQUFHdEMsbUJBQU8sQ0FBQyxpRUFBRCxDQUZ6QjtBQUFBLElBR0l1QyxRQUFRLEdBQUd2QyxtQkFBTyxDQUFDLDJEQUFELENBSHRCO0FBQUEsSUFJSXdDLFFBQVEsR0FBR3hDLG1CQUFPLENBQUMsMkRBQUQsQ0FKdEI7QUFBQSxJQUtJeUMsUUFBUSxHQUFHekMsbUJBQU8sQ0FBQywyREFBRCxDQUx0QjtBQU9BOzs7Ozs7Ozs7QUFPQSxTQUFTMEMsS0FBVCxDQUFlakMsT0FBZixFQUF3QjtBQUN0QixNQUFJWCxJQUFJLEdBQUcsS0FBS3FDLFFBQUwsR0FBZ0IsSUFBSWQsU0FBSixDQUFjWixPQUFkLENBQTNCO0FBQ0EsT0FBS2tDLElBQUwsR0FBWTdDLElBQUksQ0FBQzZDLElBQWpCO0FBQ0QsQyxDQUVEOzs7QUFDQUQsS0FBSyxDQUFDN0UsU0FBTixDQUFnQjhDLEtBQWhCLEdBQXdCMEIsVUFBeEI7QUFDQUssS0FBSyxDQUFDN0UsU0FBTixDQUFnQixRQUFoQixJQUE0QnlFLFdBQTVCO0FBQ0FJLEtBQUssQ0FBQzdFLFNBQU4sQ0FBZ0JpRCxHQUFoQixHQUFzQnlCLFFBQXRCO0FBQ0FHLEtBQUssQ0FBQzdFLFNBQU4sQ0FBZ0JrRCxHQUFoQixHQUFzQnlCLFFBQXRCO0FBQ0FFLEtBQUssQ0FBQzdFLFNBQU4sQ0FBZ0JnRCxHQUFoQixHQUFzQjRCLFFBQXRCO0FBRUEvRSxNQUFNLENBQUNDLE9BQVAsR0FBaUIrRSxLQUFqQixDOzs7Ozs7Ozs7OztBQzFCQSxJQUFJekMsSUFBSSxHQUFHRCxtQkFBTyxDQUFDLG1EQUFELENBQWxCO0FBRUE7OztBQUNBLElBQUl4QyxPQUFNLEdBQUd5QyxJQUFJLENBQUN6QyxNQUFsQjtBQUVBRSxNQUFNLENBQUNDLE9BQVAsR0FBaUJILE9BQWpCLEM7Ozs7Ozs7Ozs7O0FDTEEsSUFBSXlDLElBQUksR0FBR0QsbUJBQU8sQ0FBQyxtREFBRCxDQUFsQjtBQUVBOzs7QUFDQSxJQUFJNEMsVUFBVSxHQUFHM0MsSUFBSSxDQUFDMkMsVUFBdEI7QUFFQWxGLE1BQU0sQ0FBQ0MsT0FBUCxHQUFpQmlGLFVBQWpCLEM7Ozs7Ozs7Ozs7O0FDTEEsSUFBSTdDLFNBQVMsR0FBR0MsbUJBQU8sQ0FBQyw2REFBRCxDQUF2QjtBQUFBLElBQ0lDLElBQUksR0FBR0QsbUJBQU8sQ0FBQyxtREFBRCxDQURsQjtBQUdBOzs7QUFDQSxJQUFJNkMsT0FBTyxHQUFHOUMsU0FBUyxDQUFDRSxJQUFELEVBQU8sU0FBUCxDQUF2QjtBQUVBdkMsTUFBTSxDQUFDQyxPQUFQLEdBQWlCa0YsT0FBakIsQzs7Ozs7Ozs7Ozs7QUNOQTs7Ozs7Ozs7O0FBU0EsU0FBU0MsV0FBVCxDQUFxQkMsS0FBckIsRUFBNEJDLFNBQTVCLEVBQXVDO0FBQ3JDLE1BQUl0QyxLQUFLLEdBQUcsQ0FBQyxDQUFiO0FBQUEsTUFDSTlCLE1BQU0sR0FBR21FLEtBQUssSUFBSSxJQUFULEdBQWdCLENBQWhCLEdBQW9CQSxLQUFLLENBQUNuRSxNQUR2QztBQUFBLE1BRUlxRSxRQUFRLEdBQUcsQ0FGZjtBQUFBLE1BR0lDLE1BQU0sR0FBRyxFQUhiOztBQUtBLFNBQU8sRUFBRXhDLEtBQUYsR0FBVTlCLE1BQWpCLEVBQXlCO0FBQ3ZCLFFBQUl1RSxLQUFLLEdBQUdKLEtBQUssQ0FBQ3JDLEtBQUQsQ0FBakI7O0FBQ0EsUUFBSXNDLFNBQVMsQ0FBQ0csS0FBRCxFQUFRekMsS0FBUixFQUFlcUMsS0FBZixDQUFiLEVBQW9DO0FBQ2xDRyxZQUFNLENBQUNELFFBQVEsRUFBVCxDQUFOLEdBQXFCRSxLQUFyQjtBQUNEO0FBQ0Y7O0FBQ0QsU0FBT0QsTUFBUDtBQUNEOztBQUVEeEYsTUFBTSxDQUFDQyxPQUFQLEdBQWlCbUYsV0FBakIsQzs7Ozs7Ozs7Ozs7QUN4QkEsSUFBSU0sU0FBUyxHQUFHcEQsbUJBQU8sQ0FBQyw2REFBRCxDQUF2QjtBQUFBLElBQ0lxRCxXQUFXLEdBQUdyRCxtQkFBTyxDQUFDLCtEQUFELENBRHpCO0FBQUEsSUFFSXNELE9BQU8sR0FBR3RELG1CQUFPLENBQUMsdURBQUQsQ0FGckI7QUFBQSxJQUdJdUQsUUFBUSxHQUFHdkQsbUJBQU8sQ0FBQyx5REFBRCxDQUh0QjtBQUFBLElBSUl3RCxPQUFPLEdBQUd4RCxtQkFBTyxDQUFDLHlEQUFELENBSnJCO0FBQUEsSUFLSXlELFlBQVksR0FBR3pELG1CQUFPLENBQUMsaUVBQUQsQ0FMMUI7QUFPQTs7O0FBQ0EsSUFBSTBELFdBQVcsR0FBR0MsTUFBTSxDQUFDOUYsU0FBekI7QUFFQTs7QUFDQSxJQUFJK0YsY0FBYyxHQUFHRixXQUFXLENBQUNFLGNBQWpDO0FBRUE7Ozs7Ozs7OztBQVFBLFNBQVNDLGFBQVQsQ0FBdUJWLEtBQXZCLEVBQThCVyxTQUE5QixFQUF5QztBQUN2QyxNQUFJQyxLQUFLLEdBQUdULE9BQU8sQ0FBQ0gsS0FBRCxDQUFuQjtBQUFBLE1BQ0lhLEtBQUssR0FBRyxDQUFDRCxLQUFELElBQVVWLFdBQVcsQ0FBQ0YsS0FBRCxDQURqQztBQUFBLE1BRUljLE1BQU0sR0FBRyxDQUFDRixLQUFELElBQVUsQ0FBQ0MsS0FBWCxJQUFvQlQsUUFBUSxDQUFDSixLQUFELENBRnpDO0FBQUEsTUFHSWUsTUFBTSxHQUFHLENBQUNILEtBQUQsSUFBVSxDQUFDQyxLQUFYLElBQW9CLENBQUNDLE1BQXJCLElBQStCUixZQUFZLENBQUNOLEtBQUQsQ0FIeEQ7QUFBQSxNQUlJZ0IsV0FBVyxHQUFHSixLQUFLLElBQUlDLEtBQVQsSUFBa0JDLE1BQWxCLElBQTRCQyxNQUo5QztBQUFBLE1BS0loQixNQUFNLEdBQUdpQixXQUFXLEdBQUdmLFNBQVMsQ0FBQ0QsS0FBSyxDQUFDdkUsTUFBUCxFQUFld0YsTUFBZixDQUFaLEdBQXFDLEVBTDdEO0FBQUEsTUFNSXhGLE1BQU0sR0FBR3NFLE1BQU0sQ0FBQ3RFLE1BTnBCOztBQVFBLE9BQUssSUFBSXlGLEdBQVQsSUFBZ0JsQixLQUFoQixFQUF1QjtBQUNyQixRQUFJLENBQUNXLFNBQVMsSUFBSUYsY0FBYyxDQUFDVSxJQUFmLENBQW9CbkIsS0FBcEIsRUFBMkJrQixHQUEzQixDQUFkLEtBQ0EsRUFBRUYsV0FBVyxNQUNWO0FBQ0FFLE9BQUcsSUFBSSxRQUFQLElBQ0E7QUFDQ0osVUFBTSxLQUFLSSxHQUFHLElBQUksUUFBUCxJQUFtQkEsR0FBRyxJQUFJLFFBQS9CLENBRlAsSUFHQTtBQUNDSCxVQUFNLEtBQUtHLEdBQUcsSUFBSSxRQUFQLElBQW1CQSxHQUFHLElBQUksWUFBMUIsSUFBMENBLEdBQUcsSUFBSSxZQUF0RCxDQUpQLElBS0E7QUFDQWIsV0FBTyxDQUFDYSxHQUFELEVBQU16RixNQUFOLENBUkcsQ0FBYixDQURKLEVBVVE7QUFDTnNFLFlBQU0sQ0FBQ25FLElBQVAsQ0FBWXNGLEdBQVo7QUFDRDtBQUNGOztBQUNELFNBQU9uQixNQUFQO0FBQ0Q7O0FBRUR4RixNQUFNLENBQUNDLE9BQVAsR0FBaUJrRyxhQUFqQixDOzs7Ozs7Ozs7OztBQ2hEQTs7Ozs7Ozs7O0FBU0EsU0FBU1UsUUFBVCxDQUFrQnhCLEtBQWxCLEVBQXlCeUIsUUFBekIsRUFBbUM7QUFDakMsTUFBSTlELEtBQUssR0FBRyxDQUFDLENBQWI7QUFBQSxNQUNJOUIsTUFBTSxHQUFHbUUsS0FBSyxJQUFJLElBQVQsR0FBZ0IsQ0FBaEIsR0FBb0JBLEtBQUssQ0FBQ25FLE1BRHZDO0FBQUEsTUFFSXNFLE1BQU0sR0FBR3VCLEtBQUssQ0FBQzdGLE1BQUQsQ0FGbEI7O0FBSUEsU0FBTyxFQUFFOEIsS0FBRixHQUFVOUIsTUFBakIsRUFBeUI7QUFDdkJzRSxVQUFNLENBQUN4QyxLQUFELENBQU4sR0FBZ0I4RCxRQUFRLENBQUN6QixLQUFLLENBQUNyQyxLQUFELENBQU4sRUFBZUEsS0FBZixFQUFzQnFDLEtBQXRCLENBQXhCO0FBQ0Q7O0FBQ0QsU0FBT0csTUFBUDtBQUNEOztBQUVEeEYsTUFBTSxDQUFDQyxPQUFQLEdBQWlCNEcsUUFBakIsQzs7Ozs7Ozs7Ozs7QUNwQkE7Ozs7Ozs7O0FBUUEsU0FBU0csU0FBVCxDQUFtQjNCLEtBQW5CLEVBQTBCYixNQUExQixFQUFrQztBQUNoQyxNQUFJeEIsS0FBSyxHQUFHLENBQUMsQ0FBYjtBQUFBLE1BQ0k5QixNQUFNLEdBQUdzRCxNQUFNLENBQUN0RCxNQURwQjtBQUFBLE1BRUkrRixNQUFNLEdBQUc1QixLQUFLLENBQUNuRSxNQUZuQjs7QUFJQSxTQUFPLEVBQUU4QixLQUFGLEdBQVU5QixNQUFqQixFQUF5QjtBQUN2Qm1FLFNBQUssQ0FBQzRCLE1BQU0sR0FBR2pFLEtBQVYsQ0FBTCxHQUF3QndCLE1BQU0sQ0FBQ3hCLEtBQUQsQ0FBOUI7QUFDRDs7QUFDRCxTQUFPcUMsS0FBUDtBQUNEOztBQUVEckYsTUFBTSxDQUFDQyxPQUFQLEdBQWlCK0csU0FBakIsQzs7Ozs7Ozs7Ozs7QUNuQkE7Ozs7Ozs7Ozs7QUFVQSxTQUFTRSxTQUFULENBQW1CN0IsS0FBbkIsRUFBMEJDLFNBQTFCLEVBQXFDO0FBQ25DLE1BQUl0QyxLQUFLLEdBQUcsQ0FBQyxDQUFiO0FBQUEsTUFDSTlCLE1BQU0sR0FBR21FLEtBQUssSUFBSSxJQUFULEdBQWdCLENBQWhCLEdBQW9CQSxLQUFLLENBQUNuRSxNQUR2Qzs7QUFHQSxTQUFPLEVBQUU4QixLQUFGLEdBQVU5QixNQUFqQixFQUF5QjtBQUN2QixRQUFJb0UsU0FBUyxDQUFDRCxLQUFLLENBQUNyQyxLQUFELENBQU4sRUFBZUEsS0FBZixFQUFzQnFDLEtBQXRCLENBQWIsRUFBMkM7QUFDekMsYUFBTyxJQUFQO0FBQ0Q7QUFDRjs7QUFDRCxTQUFPLEtBQVA7QUFDRDs7QUFFRHJGLE1BQU0sQ0FBQ0MsT0FBUCxHQUFpQmlILFNBQWpCLEM7Ozs7Ozs7Ozs7O0FDdEJBLElBQUlDLEVBQUUsR0FBRzdFLG1CQUFPLENBQUMsNkNBQUQsQ0FBaEI7QUFFQTs7Ozs7Ozs7OztBQVFBLFNBQVM4RSxZQUFULENBQXNCL0IsS0FBdEIsRUFBNkJzQixHQUE3QixFQUFrQztBQUNoQyxNQUFJekYsTUFBTSxHQUFHbUUsS0FBSyxDQUFDbkUsTUFBbkI7O0FBQ0EsU0FBT0EsTUFBTSxFQUFiLEVBQWlCO0FBQ2YsUUFBSWlHLEVBQUUsQ0FBQzlCLEtBQUssQ0FBQ25FLE1BQUQsQ0FBTCxDQUFjLENBQWQsQ0FBRCxFQUFtQnlGLEdBQW5CLENBQU4sRUFBK0I7QUFDN0IsYUFBT3pGLE1BQVA7QUFDRDtBQUNGOztBQUNELFNBQU8sQ0FBQyxDQUFSO0FBQ0Q7O0FBRURsQixNQUFNLENBQUNDLE9BQVAsR0FBaUJtSCxZQUFqQixDOzs7Ozs7Ozs7OztBQ3BCQTs7Ozs7Ozs7Ozs7QUFXQSxTQUFTQyxhQUFULENBQXVCaEMsS0FBdkIsRUFBOEJDLFNBQTlCLEVBQXlDZ0MsU0FBekMsRUFBb0RDLFNBQXBELEVBQStEO0FBQzdELE1BQUlyRyxNQUFNLEdBQUdtRSxLQUFLLENBQUNuRSxNQUFuQjtBQUFBLE1BQ0k4QixLQUFLLEdBQUdzRSxTQUFTLElBQUlDLFNBQVMsR0FBRyxDQUFILEdBQU8sQ0FBQyxDQUFyQixDQURyQjs7QUFHQSxTQUFRQSxTQUFTLEdBQUd2RSxLQUFLLEVBQVIsR0FBYSxFQUFFQSxLQUFGLEdBQVU5QixNQUF4QyxFQUFpRDtBQUMvQyxRQUFJb0UsU0FBUyxDQUFDRCxLQUFLLENBQUNyQyxLQUFELENBQU4sRUFBZUEsS0FBZixFQUFzQnFDLEtBQXRCLENBQWIsRUFBMkM7QUFDekMsYUFBT3JDLEtBQVA7QUFDRDtBQUNGOztBQUNELFNBQU8sQ0FBQyxDQUFSO0FBQ0Q7O0FBRURoRCxNQUFNLENBQUNDLE9BQVAsR0FBaUJvSCxhQUFqQixDOzs7Ozs7Ozs7OztBQ3ZCQSxJQUFJRyxRQUFRLEdBQUdsRixtQkFBTyxDQUFDLDJEQUFELENBQXRCO0FBQUEsSUFDSW1GLEtBQUssR0FBR25GLG1CQUFPLENBQUMscURBQUQsQ0FEbkI7QUFHQTs7Ozs7Ozs7OztBQVFBLFNBQVNvRixPQUFULENBQWlCQyxNQUFqQixFQUF5QkMsSUFBekIsRUFBK0I7QUFDN0JBLE1BQUksR0FBR0osUUFBUSxDQUFDSSxJQUFELEVBQU9ELE1BQVAsQ0FBZjtBQUVBLE1BQUkzRSxLQUFLLEdBQUcsQ0FBWjtBQUFBLE1BQ0k5QixNQUFNLEdBQUcwRyxJQUFJLENBQUMxRyxNQURsQjs7QUFHQSxTQUFPeUcsTUFBTSxJQUFJLElBQVYsSUFBa0IzRSxLQUFLLEdBQUc5QixNQUFqQyxFQUF5QztBQUN2Q3lHLFVBQU0sR0FBR0EsTUFBTSxDQUFDRixLQUFLLENBQUNHLElBQUksQ0FBQzVFLEtBQUssRUFBTixDQUFMLENBQU4sQ0FBZjtBQUNEOztBQUNELFNBQVFBLEtBQUssSUFBSUEsS0FBSyxJQUFJOUIsTUFBbkIsR0FBNkJ5RyxNQUE3QixHQUFzQ0UsU0FBN0M7QUFDRDs7QUFFRDdILE1BQU0sQ0FBQ0MsT0FBUCxHQUFpQnlILE9BQWpCLEM7Ozs7Ozs7Ozs7O0FDdkJBLElBQUlWLFNBQVMsR0FBRzFFLG1CQUFPLENBQUMsNkRBQUQsQ0FBdkI7QUFBQSxJQUNJc0QsT0FBTyxHQUFHdEQsbUJBQU8sQ0FBQyx1REFBRCxDQURyQjtBQUdBOzs7Ozs7Ozs7Ozs7O0FBV0EsU0FBU3dGLGNBQVQsQ0FBd0JILE1BQXhCLEVBQWdDSSxRQUFoQyxFQUEwQ0MsV0FBMUMsRUFBdUQ7QUFDckQsTUFBSXhDLE1BQU0sR0FBR3VDLFFBQVEsQ0FBQ0osTUFBRCxDQUFyQjtBQUNBLFNBQU8vQixPQUFPLENBQUMrQixNQUFELENBQVAsR0FBa0JuQyxNQUFsQixHQUEyQndCLFNBQVMsQ0FBQ3hCLE1BQUQsRUFBU3dDLFdBQVcsQ0FBQ0wsTUFBRCxDQUFwQixDQUEzQztBQUNEOztBQUVEM0gsTUFBTSxDQUFDQyxPQUFQLEdBQWlCNkgsY0FBakIsQzs7Ozs7Ozs7Ozs7QUNuQkEsSUFBSWhJLE9BQU0sR0FBR3dDLG1CQUFPLENBQUMsdURBQUQsQ0FBcEI7QUFBQSxJQUNJMkYsU0FBUyxHQUFHM0YsbUJBQU8sQ0FBQyw2REFBRCxDQUR2QjtBQUFBLElBRUk0RixjQUFjLEdBQUc1RixtQkFBTyxDQUFDLHVFQUFELENBRjVCO0FBSUE7OztBQUNBLElBQUk2RixPQUFPLEdBQUcsZUFBZDtBQUFBLElBQ0lDLFlBQVksR0FBRyxvQkFEbkI7QUFHQTs7QUFDQSxJQUFJQyxjQUFjLEdBQUd2SSxPQUFNLEdBQUdBLE9BQU0sQ0FBQ3dJLFdBQVYsR0FBd0JULFNBQW5EO0FBRUE7Ozs7Ozs7O0FBT0EsU0FBU1UsVUFBVCxDQUFvQjlDLEtBQXBCLEVBQTJCO0FBQ3pCLE1BQUlBLEtBQUssSUFBSSxJQUFiLEVBQW1CO0FBQ2pCLFdBQU9BLEtBQUssS0FBS29DLFNBQVYsR0FBc0JPLFlBQXRCLEdBQXFDRCxPQUE1QztBQUNEOztBQUNELFNBQVFFLGNBQWMsSUFBSUEsY0FBYyxJQUFJcEMsTUFBTSxDQUFDUixLQUFELENBQTNDLEdBQ0h3QyxTQUFTLENBQUN4QyxLQUFELENBRE4sR0FFSHlDLGNBQWMsQ0FBQ3pDLEtBQUQsQ0FGbEI7QUFHRDs7QUFFRHpGLE1BQU0sQ0FBQ0MsT0FBUCxHQUFpQnNJLFVBQWpCLEM7Ozs7Ozs7Ozs7O0FDM0JBOzs7Ozs7OztBQVFBLFNBQVNDLFNBQVQsQ0FBbUJiLE1BQW5CLEVBQTJCaEIsR0FBM0IsRUFBZ0M7QUFDOUIsU0FBT2dCLE1BQU0sSUFBSSxJQUFWLElBQWtCaEIsR0FBRyxJQUFJVixNQUFNLENBQUMwQixNQUFELENBQXRDO0FBQ0Q7O0FBRUQzSCxNQUFNLENBQUNDLE9BQVAsR0FBaUJ1SSxTQUFqQixDOzs7Ozs7Ozs7OztBQ1pBLElBQUlELFVBQVUsR0FBR2pHLG1CQUFPLENBQUMsK0RBQUQsQ0FBeEI7QUFBQSxJQUNJbUcsWUFBWSxHQUFHbkcsbUJBQU8sQ0FBQyxpRUFBRCxDQUQxQjtBQUdBOzs7QUFDQSxJQUFJb0csT0FBTyxHQUFHLG9CQUFkO0FBRUE7Ozs7Ozs7O0FBT0EsU0FBU0MsZUFBVCxDQUF5QmxELEtBQXpCLEVBQWdDO0FBQzlCLFNBQU9nRCxZQUFZLENBQUNoRCxLQUFELENBQVosSUFBdUI4QyxVQUFVLENBQUM5QyxLQUFELENBQVYsSUFBcUJpRCxPQUFuRDtBQUNEOztBQUVEMUksTUFBTSxDQUFDQyxPQUFQLEdBQWlCMEksZUFBakIsQzs7Ozs7Ozs7Ozs7QUNqQkEsSUFBSUMsZUFBZSxHQUFHdEcsbUJBQU8sQ0FBQyx5RUFBRCxDQUE3QjtBQUFBLElBQ0ltRyxZQUFZLEdBQUduRyxtQkFBTyxDQUFDLGlFQUFELENBRDFCO0FBR0E7Ozs7Ozs7Ozs7Ozs7Ozs7QUFjQSxTQUFTdUcsV0FBVCxDQUFxQnBELEtBQXJCLEVBQTRCcUQsS0FBNUIsRUFBbUNDLE9BQW5DLEVBQTRDQyxVQUE1QyxFQUF3REMsS0FBeEQsRUFBK0Q7QUFDN0QsTUFBSXhELEtBQUssS0FBS3FELEtBQWQsRUFBcUI7QUFDbkIsV0FBTyxJQUFQO0FBQ0Q7O0FBQ0QsTUFBSXJELEtBQUssSUFBSSxJQUFULElBQWlCcUQsS0FBSyxJQUFJLElBQTFCLElBQW1DLENBQUNMLFlBQVksQ0FBQ2hELEtBQUQsQ0FBYixJQUF3QixDQUFDZ0QsWUFBWSxDQUFDSyxLQUFELENBQTVFLEVBQXNGO0FBQ3BGLFdBQU9yRCxLQUFLLEtBQUtBLEtBQVYsSUFBbUJxRCxLQUFLLEtBQUtBLEtBQXBDO0FBQ0Q7O0FBQ0QsU0FBT0YsZUFBZSxDQUFDbkQsS0FBRCxFQUFRcUQsS0FBUixFQUFlQyxPQUFmLEVBQXdCQyxVQUF4QixFQUFvQ0gsV0FBcEMsRUFBaURJLEtBQWpELENBQXRCO0FBQ0Q7O0FBRURqSixNQUFNLENBQUNDLE9BQVAsR0FBaUI0SSxXQUFqQixDOzs7Ozs7Ozs7OztBQzNCQSxJQUFJN0QsS0FBSyxHQUFHMUMsbUJBQU8sQ0FBQyxxREFBRCxDQUFuQjtBQUFBLElBQ0k0RyxXQUFXLEdBQUc1RyxtQkFBTyxDQUFDLGlFQUFELENBRHpCO0FBQUEsSUFFSTZHLFVBQVUsR0FBRzdHLG1CQUFPLENBQUMsK0RBQUQsQ0FGeEI7QUFBQSxJQUdJOEcsWUFBWSxHQUFHOUcsbUJBQU8sQ0FBQyxtRUFBRCxDQUgxQjtBQUFBLElBSUkrRyxNQUFNLEdBQUcvRyxtQkFBTyxDQUFDLHVEQUFELENBSnBCO0FBQUEsSUFLSXNELE9BQU8sR0FBR3RELG1CQUFPLENBQUMsdURBQUQsQ0FMckI7QUFBQSxJQU1JdUQsUUFBUSxHQUFHdkQsbUJBQU8sQ0FBQyx5REFBRCxDQU50QjtBQUFBLElBT0l5RCxZQUFZLEdBQUd6RCxtQkFBTyxDQUFDLGlFQUFELENBUDFCO0FBU0E7OztBQUNBLElBQUlnSCxvQkFBb0IsR0FBRyxDQUEzQjtBQUVBOztBQUNBLElBQUlaLE9BQU8sR0FBRyxvQkFBZDtBQUFBLElBQ0lhLFFBQVEsR0FBRyxnQkFEZjtBQUFBLElBRUlDLFNBQVMsR0FBRyxpQkFGaEI7QUFJQTs7QUFDQSxJQUFJeEQsV0FBVyxHQUFHQyxNQUFNLENBQUM5RixTQUF6QjtBQUVBOztBQUNBLElBQUkrRixjQUFjLEdBQUdGLFdBQVcsQ0FBQ0UsY0FBakM7QUFFQTs7Ozs7Ozs7Ozs7Ozs7O0FBY0EsU0FBUzBDLGVBQVQsQ0FBeUJqQixNQUF6QixFQUFpQ21CLEtBQWpDLEVBQXdDQyxPQUF4QyxFQUFpREMsVUFBakQsRUFBNkRTLFNBQTdELEVBQXdFUixLQUF4RSxFQUErRTtBQUM3RSxNQUFJUyxRQUFRLEdBQUc5RCxPQUFPLENBQUMrQixNQUFELENBQXRCO0FBQUEsTUFDSWdDLFFBQVEsR0FBRy9ELE9BQU8sQ0FBQ2tELEtBQUQsQ0FEdEI7QUFBQSxNQUVJYyxNQUFNLEdBQUdGLFFBQVEsR0FBR0gsUUFBSCxHQUFjRixNQUFNLENBQUMxQixNQUFELENBRnpDO0FBQUEsTUFHSWtDLE1BQU0sR0FBR0YsUUFBUSxHQUFHSixRQUFILEdBQWNGLE1BQU0sQ0FBQ1AsS0FBRCxDQUh6QztBQUtBYyxRQUFNLEdBQUdBLE1BQU0sSUFBSWxCLE9BQVYsR0FBb0JjLFNBQXBCLEdBQWdDSSxNQUF6QztBQUNBQyxRQUFNLEdBQUdBLE1BQU0sSUFBSW5CLE9BQVYsR0FBb0JjLFNBQXBCLEdBQWdDSyxNQUF6QztBQUVBLE1BQUlDLFFBQVEsR0FBR0YsTUFBTSxJQUFJSixTQUF6QjtBQUFBLE1BQ0lPLFFBQVEsR0FBR0YsTUFBTSxJQUFJTCxTQUR6QjtBQUFBLE1BRUlRLFNBQVMsR0FBR0osTUFBTSxJQUFJQyxNQUYxQjs7QUFJQSxNQUFJRyxTQUFTLElBQUluRSxRQUFRLENBQUM4QixNQUFELENBQXpCLEVBQW1DO0FBQ2pDLFFBQUksQ0FBQzlCLFFBQVEsQ0FBQ2lELEtBQUQsQ0FBYixFQUFzQjtBQUNwQixhQUFPLEtBQVA7QUFDRDs7QUFDRFksWUFBUSxHQUFHLElBQVg7QUFDQUksWUFBUSxHQUFHLEtBQVg7QUFDRDs7QUFDRCxNQUFJRSxTQUFTLElBQUksQ0FBQ0YsUUFBbEIsRUFBNEI7QUFDMUJiLFNBQUssS0FBS0EsS0FBSyxHQUFHLElBQUlqRSxLQUFKLEVBQWIsQ0FBTDtBQUNBLFdBQVEwRSxRQUFRLElBQUkzRCxZQUFZLENBQUM0QixNQUFELENBQXpCLEdBQ0h1QixXQUFXLENBQUN2QixNQUFELEVBQVNtQixLQUFULEVBQWdCQyxPQUFoQixFQUF5QkMsVUFBekIsRUFBcUNTLFNBQXJDLEVBQWdEUixLQUFoRCxDQURSLEdBRUhFLFVBQVUsQ0FBQ3hCLE1BQUQsRUFBU21CLEtBQVQsRUFBZ0JjLE1BQWhCLEVBQXdCYixPQUF4QixFQUFpQ0MsVUFBakMsRUFBNkNTLFNBQTdDLEVBQXdEUixLQUF4RCxDQUZkO0FBR0Q7O0FBQ0QsTUFBSSxFQUFFRixPQUFPLEdBQUdPLG9CQUFaLENBQUosRUFBdUM7QUFDckMsUUFBSVcsWUFBWSxHQUFHSCxRQUFRLElBQUk1RCxjQUFjLENBQUNVLElBQWYsQ0FBb0JlLE1BQXBCLEVBQTRCLGFBQTVCLENBQS9CO0FBQUEsUUFDSXVDLFlBQVksR0FBR0gsUUFBUSxJQUFJN0QsY0FBYyxDQUFDVSxJQUFmLENBQW9Ca0MsS0FBcEIsRUFBMkIsYUFBM0IsQ0FEL0I7O0FBR0EsUUFBSW1CLFlBQVksSUFBSUMsWUFBcEIsRUFBa0M7QUFDaEMsVUFBSUMsWUFBWSxHQUFHRixZQUFZLEdBQUd0QyxNQUFNLENBQUNsQyxLQUFQLEVBQUgsR0FBb0JrQyxNQUFuRDtBQUFBLFVBQ0l5QyxZQUFZLEdBQUdGLFlBQVksR0FBR3BCLEtBQUssQ0FBQ3JELEtBQU4sRUFBSCxHQUFtQnFELEtBRGxEO0FBR0FHLFdBQUssS0FBS0EsS0FBSyxHQUFHLElBQUlqRSxLQUFKLEVBQWIsQ0FBTDtBQUNBLGFBQU95RSxTQUFTLENBQUNVLFlBQUQsRUFBZUMsWUFBZixFQUE2QnJCLE9BQTdCLEVBQXNDQyxVQUF0QyxFQUFrREMsS0FBbEQsQ0FBaEI7QUFDRDtBQUNGOztBQUNELE1BQUksQ0FBQ2UsU0FBTCxFQUFnQjtBQUNkLFdBQU8sS0FBUDtBQUNEOztBQUNEZixPQUFLLEtBQUtBLEtBQUssR0FBRyxJQUFJakUsS0FBSixFQUFiLENBQUw7QUFDQSxTQUFPb0UsWUFBWSxDQUFDekIsTUFBRCxFQUFTbUIsS0FBVCxFQUFnQkMsT0FBaEIsRUFBeUJDLFVBQXpCLEVBQXFDUyxTQUFyQyxFQUFnRFIsS0FBaEQsQ0FBbkI7QUFDRDs7QUFFRGpKLE1BQU0sQ0FBQ0MsT0FBUCxHQUFpQjJJLGVBQWpCLEM7Ozs7Ozs7Ozs7O0FDbEZBLElBQUk1RCxLQUFLLEdBQUcxQyxtQkFBTyxDQUFDLHFEQUFELENBQW5CO0FBQUEsSUFDSXVHLFdBQVcsR0FBR3ZHLG1CQUFPLENBQUMsaUVBQUQsQ0FEekI7QUFHQTs7O0FBQ0EsSUFBSWdILG9CQUFvQixHQUFHLENBQTNCO0FBQUEsSUFDSWUsc0JBQXNCLEdBQUcsQ0FEN0I7QUFHQTs7Ozs7Ozs7Ozs7QUFVQSxTQUFTQyxXQUFULENBQXFCM0MsTUFBckIsRUFBNkIvRixNQUE3QixFQUFxQzJJLFNBQXJDLEVBQWdEdkIsVUFBaEQsRUFBNEQ7QUFDMUQsTUFBSWhHLEtBQUssR0FBR3VILFNBQVMsQ0FBQ3JKLE1BQXRCO0FBQUEsTUFDSUEsTUFBTSxHQUFHOEIsS0FEYjtBQUFBLE1BRUl3SCxZQUFZLEdBQUcsQ0FBQ3hCLFVBRnBCOztBQUlBLE1BQUlyQixNQUFNLElBQUksSUFBZCxFQUFvQjtBQUNsQixXQUFPLENBQUN6RyxNQUFSO0FBQ0Q7O0FBQ0R5RyxRQUFNLEdBQUcxQixNQUFNLENBQUMwQixNQUFELENBQWY7O0FBQ0EsU0FBTzNFLEtBQUssRUFBWixFQUFnQjtBQUNkLFFBQUlaLElBQUksR0FBR21JLFNBQVMsQ0FBQ3ZILEtBQUQsQ0FBcEI7O0FBQ0EsUUFBS3dILFlBQVksSUFBSXBJLElBQUksQ0FBQyxDQUFELENBQXJCLEdBQ0lBLElBQUksQ0FBQyxDQUFELENBQUosS0FBWXVGLE1BQU0sQ0FBQ3ZGLElBQUksQ0FBQyxDQUFELENBQUwsQ0FEdEIsR0FFSSxFQUFFQSxJQUFJLENBQUMsQ0FBRCxDQUFKLElBQVd1RixNQUFiLENBRlIsRUFHTTtBQUNKLGFBQU8sS0FBUDtBQUNEO0FBQ0Y7O0FBQ0QsU0FBTyxFQUFFM0UsS0FBRixHQUFVOUIsTUFBakIsRUFBeUI7QUFDdkJrQixRQUFJLEdBQUdtSSxTQUFTLENBQUN2SCxLQUFELENBQWhCO0FBQ0EsUUFBSTJELEdBQUcsR0FBR3ZFLElBQUksQ0FBQyxDQUFELENBQWQ7QUFBQSxRQUNJcUksUUFBUSxHQUFHOUMsTUFBTSxDQUFDaEIsR0FBRCxDQURyQjtBQUFBLFFBRUkrRCxRQUFRLEdBQUd0SSxJQUFJLENBQUMsQ0FBRCxDQUZuQjs7QUFJQSxRQUFJb0ksWUFBWSxJQUFJcEksSUFBSSxDQUFDLENBQUQsQ0FBeEIsRUFBNkI7QUFDM0IsVUFBSXFJLFFBQVEsS0FBSzVDLFNBQWIsSUFBMEIsRUFBRWxCLEdBQUcsSUFBSWdCLE1BQVQsQ0FBOUIsRUFBZ0Q7QUFDOUMsZUFBTyxLQUFQO0FBQ0Q7QUFDRixLQUpELE1BSU87QUFDTCxVQUFJc0IsS0FBSyxHQUFHLElBQUlqRSxLQUFKLEVBQVo7O0FBQ0EsVUFBSWdFLFVBQUosRUFBZ0I7QUFDZCxZQUFJeEQsTUFBTSxHQUFHd0QsVUFBVSxDQUFDeUIsUUFBRCxFQUFXQyxRQUFYLEVBQXFCL0QsR0FBckIsRUFBMEJnQixNQUExQixFQUFrQy9GLE1BQWxDLEVBQTBDcUgsS0FBMUMsQ0FBdkI7QUFDRDs7QUFDRCxVQUFJLEVBQUV6RCxNQUFNLEtBQUtxQyxTQUFYLEdBQ0VnQixXQUFXLENBQUM2QixRQUFELEVBQVdELFFBQVgsRUFBcUJuQixvQkFBb0IsR0FBR2Usc0JBQTVDLEVBQW9FckIsVUFBcEUsRUFBZ0ZDLEtBQWhGLENBRGIsR0FFRXpELE1BRkosQ0FBSixFQUdPO0FBQ0wsZUFBTyxLQUFQO0FBQ0Q7QUFDRjtBQUNGOztBQUNELFNBQU8sSUFBUDtBQUNEOztBQUVEeEYsTUFBTSxDQUFDQyxPQUFQLEdBQWlCcUssV0FBakIsQzs7Ozs7Ozs7Ozs7QUM3REEsSUFBSUssVUFBVSxHQUFHckksbUJBQU8sQ0FBQyw2REFBRCxDQUF4QjtBQUFBLElBQ0lzSSxRQUFRLEdBQUd0SSxtQkFBTyxDQUFDLDJEQUFELENBRHRCO0FBQUEsSUFFSXVJLFFBQVEsR0FBR3ZJLG1CQUFPLENBQUMseURBQUQsQ0FGdEI7QUFBQSxJQUdJd0ksUUFBUSxHQUFHeEksbUJBQU8sQ0FBQywyREFBRCxDQUh0QjtBQUtBOzs7Ozs7QUFJQSxJQUFJeUksWUFBWSxHQUFHLHFCQUFuQjtBQUVBOztBQUNBLElBQUlDLFlBQVksR0FBRyw2QkFBbkI7QUFFQTs7QUFDQSxJQUFJQyxTQUFTLEdBQUdDLFFBQVEsQ0FBQy9LLFNBQXpCO0FBQUEsSUFDSTZGLFdBQVcsR0FBR0MsTUFBTSxDQUFDOUYsU0FEekI7QUFHQTs7QUFDQSxJQUFJZ0wsWUFBWSxHQUFHRixTQUFTLENBQUMzSyxRQUE3QjtBQUVBOztBQUNBLElBQUk0RixjQUFjLEdBQUdGLFdBQVcsQ0FBQ0UsY0FBakM7QUFFQTs7QUFDQSxJQUFJa0YsVUFBVSxHQUFHQyxNQUFNLENBQUMsTUFDdEJGLFlBQVksQ0FBQ3ZFLElBQWIsQ0FBa0JWLGNBQWxCLEVBQWtDb0YsT0FBbEMsQ0FBMENQLFlBQTFDLEVBQXdELE1BQXhELEVBQ0NPLE9BREQsQ0FDUyx3REFEVCxFQUNtRSxPQURuRSxDQURzQixHQUV3RCxHQUZ6RCxDQUF2QjtBQUtBOzs7Ozs7Ozs7QUFRQSxTQUFTQyxZQUFULENBQXNCOUYsS0FBdEIsRUFBNkI7QUFDM0IsTUFBSSxDQUFDb0YsUUFBUSxDQUFDcEYsS0FBRCxDQUFULElBQW9CbUYsUUFBUSxDQUFDbkYsS0FBRCxDQUFoQyxFQUF5QztBQUN2QyxXQUFPLEtBQVA7QUFDRDs7QUFDRCxNQUFJK0YsT0FBTyxHQUFHYixVQUFVLENBQUNsRixLQUFELENBQVYsR0FBb0IyRixVQUFwQixHQUFpQ0osWUFBL0M7QUFDQSxTQUFPUSxPQUFPLENBQUNDLElBQVIsQ0FBYVgsUUFBUSxDQUFDckYsS0FBRCxDQUFyQixDQUFQO0FBQ0Q7O0FBRUR6RixNQUFNLENBQUNDLE9BQVAsR0FBaUJzTCxZQUFqQixDOzs7Ozs7Ozs7OztBQzlDQSxJQUFJaEQsVUFBVSxHQUFHakcsbUJBQU8sQ0FBQywrREFBRCxDQUF4QjtBQUFBLElBQ0lvSixRQUFRLEdBQUdwSixtQkFBTyxDQUFDLHlEQUFELENBRHRCO0FBQUEsSUFFSW1HLFlBQVksR0FBR25HLG1CQUFPLENBQUMsaUVBQUQsQ0FGMUI7QUFJQTs7O0FBQ0EsSUFBSW9HLE9BQU8sR0FBRyxvQkFBZDtBQUFBLElBQ0lhLFFBQVEsR0FBRyxnQkFEZjtBQUFBLElBRUlvQyxPQUFPLEdBQUcsa0JBRmQ7QUFBQSxJQUdJQyxPQUFPLEdBQUcsZUFIZDtBQUFBLElBSUlDLFFBQVEsR0FBRyxnQkFKZjtBQUFBLElBS0lDLE9BQU8sR0FBRyxtQkFMZDtBQUFBLElBTUlDLE1BQU0sR0FBRyxjQU5iO0FBQUEsSUFPSUMsU0FBUyxHQUFHLGlCQVBoQjtBQUFBLElBUUl4QyxTQUFTLEdBQUcsaUJBUmhCO0FBQUEsSUFTSXlDLFNBQVMsR0FBRyxpQkFUaEI7QUFBQSxJQVVJQyxNQUFNLEdBQUcsY0FWYjtBQUFBLElBV0lDLFNBQVMsR0FBRyxpQkFYaEI7QUFBQSxJQVlJQyxVQUFVLEdBQUcsa0JBWmpCO0FBY0EsSUFBSUMsY0FBYyxHQUFHLHNCQUFyQjtBQUFBLElBQ0lDLFdBQVcsR0FBRyxtQkFEbEI7QUFBQSxJQUVJQyxVQUFVLEdBQUcsdUJBRmpCO0FBQUEsSUFHSUMsVUFBVSxHQUFHLHVCQUhqQjtBQUFBLElBSUlDLE9BQU8sR0FBRyxvQkFKZDtBQUFBLElBS0lDLFFBQVEsR0FBRyxxQkFMZjtBQUFBLElBTUlDLFFBQVEsR0FBRyxxQkFOZjtBQUFBLElBT0lDLFFBQVEsR0FBRyxxQkFQZjtBQUFBLElBUUlDLGVBQWUsR0FBRyw0QkFSdEI7QUFBQSxJQVNJQyxTQUFTLEdBQUcsc0JBVGhCO0FBQUEsSUFVSUMsU0FBUyxHQUFHLHNCQVZoQjtBQVlBOztBQUNBLElBQUlDLGNBQWMsR0FBRyxFQUFyQjtBQUNBQSxjQUFjLENBQUNULFVBQUQsQ0FBZCxHQUE2QlMsY0FBYyxDQUFDUixVQUFELENBQWQsR0FDN0JRLGNBQWMsQ0FBQ1AsT0FBRCxDQUFkLEdBQTBCTyxjQUFjLENBQUNOLFFBQUQsQ0FBZCxHQUMxQk0sY0FBYyxDQUFDTCxRQUFELENBQWQsR0FBMkJLLGNBQWMsQ0FBQ0osUUFBRCxDQUFkLEdBQzNCSSxjQUFjLENBQUNILGVBQUQsQ0FBZCxHQUFrQ0csY0FBYyxDQUFDRixTQUFELENBQWQsR0FDbENFLGNBQWMsQ0FBQ0QsU0FBRCxDQUFkLEdBQTRCLElBSjVCO0FBS0FDLGNBQWMsQ0FBQ3RFLE9BQUQsQ0FBZCxHQUEwQnNFLGNBQWMsQ0FBQ3pELFFBQUQsQ0FBZCxHQUMxQnlELGNBQWMsQ0FBQ1gsY0FBRCxDQUFkLEdBQWlDVyxjQUFjLENBQUNyQixPQUFELENBQWQsR0FDakNxQixjQUFjLENBQUNWLFdBQUQsQ0FBZCxHQUE4QlUsY0FBYyxDQUFDcEIsT0FBRCxDQUFkLEdBQzlCb0IsY0FBYyxDQUFDbkIsUUFBRCxDQUFkLEdBQTJCbUIsY0FBYyxDQUFDbEIsT0FBRCxDQUFkLEdBQzNCa0IsY0FBYyxDQUFDakIsTUFBRCxDQUFkLEdBQXlCaUIsY0FBYyxDQUFDaEIsU0FBRCxDQUFkLEdBQ3pCZ0IsY0FBYyxDQUFDeEQsU0FBRCxDQUFkLEdBQTRCd0QsY0FBYyxDQUFDZixTQUFELENBQWQsR0FDNUJlLGNBQWMsQ0FBQ2QsTUFBRCxDQUFkLEdBQXlCYyxjQUFjLENBQUNiLFNBQUQsQ0FBZCxHQUN6QmEsY0FBYyxDQUFDWixVQUFELENBQWQsR0FBNkIsS0FQN0I7QUFTQTs7Ozs7Ozs7QUFPQSxTQUFTYSxnQkFBVCxDQUEwQnhILEtBQTFCLEVBQWlDO0FBQy9CLFNBQU9nRCxZQUFZLENBQUNoRCxLQUFELENBQVosSUFDTGlHLFFBQVEsQ0FBQ2pHLEtBQUssQ0FBQ3ZFLE1BQVAsQ0FESCxJQUNxQixDQUFDLENBQUM4TCxjQUFjLENBQUN6RSxVQUFVLENBQUM5QyxLQUFELENBQVgsQ0FENUM7QUFFRDs7QUFFRHpGLE1BQU0sQ0FBQ0MsT0FBUCxHQUFpQmdOLGdCQUFqQixDOzs7Ozs7Ozs7Ozs7O0FDM0RBLElBQUlDLFdBQVcsR0FBRzVLLG1CQUFPLENBQUMsaUVBQUQsQ0FBekI7QUFBQSxJQUNJNkssbUJBQW1CLEdBQUc3SyxtQkFBTyxDQUFDLGlGQUFELENBRGpDO0FBQUEsSUFFSThLLFFBQVEsR0FBRzlLLG1CQUFPLENBQUMseURBQUQsQ0FGdEI7QUFBQSxJQUdJc0QsT0FBTyxHQUFHdEQsbUJBQU8sQ0FBQyx1REFBRCxDQUhyQjtBQUFBLElBSUkrSyxRQUFRLEdBQUcvSyxtQkFBTyxDQUFDLHlEQUFELENBSnRCO0FBTUE7Ozs7Ozs7OztBQU9BLFNBQVNnTCxZQUFULENBQXNCN0gsS0FBdEIsRUFBNkI7QUFDM0I7QUFDQTtBQUNBLE1BQUksT0FBT0EsS0FBUCxJQUFnQixVQUFwQixFQUFnQztBQUM5QixXQUFPQSxLQUFQO0FBQ0Q7O0FBQ0QsTUFBSUEsS0FBSyxJQUFJLElBQWIsRUFBbUI7QUFDakIsV0FBTzJILFFBQVA7QUFDRDs7QUFDRCxNQUFJLFFBQU8zSCxLQUFQLEtBQWdCLFFBQXBCLEVBQThCO0FBQzVCLFdBQU9HLE9BQU8sQ0FBQ0gsS0FBRCxDQUFQLEdBQ0gwSCxtQkFBbUIsQ0FBQzFILEtBQUssQ0FBQyxDQUFELENBQU4sRUFBV0EsS0FBSyxDQUFDLENBQUQsQ0FBaEIsQ0FEaEIsR0FFSHlILFdBQVcsQ0FBQ3pILEtBQUQsQ0FGZjtBQUdEOztBQUNELFNBQU80SCxRQUFRLENBQUM1SCxLQUFELENBQWY7QUFDRDs7QUFFRHpGLE1BQU0sQ0FBQ0MsT0FBUCxHQUFpQnFOLFlBQWpCLEM7Ozs7Ozs7Ozs7O0FDOUJBLElBQUlDLFdBQVcsR0FBR2pMLG1CQUFPLENBQUMsaUVBQUQsQ0FBekI7QUFBQSxJQUNJa0wsVUFBVSxHQUFHbEwsbUJBQU8sQ0FBQywrREFBRCxDQUR4QjtBQUdBOzs7QUFDQSxJQUFJMEQsV0FBVyxHQUFHQyxNQUFNLENBQUM5RixTQUF6QjtBQUVBOztBQUNBLElBQUkrRixjQUFjLEdBQUdGLFdBQVcsQ0FBQ0UsY0FBakM7QUFFQTs7Ozs7Ozs7QUFPQSxTQUFTdUgsUUFBVCxDQUFrQjlGLE1BQWxCLEVBQTBCO0FBQ3hCLE1BQUksQ0FBQzRGLFdBQVcsQ0FBQzVGLE1BQUQsQ0FBaEIsRUFBMEI7QUFDeEIsV0FBTzZGLFVBQVUsQ0FBQzdGLE1BQUQsQ0FBakI7QUFDRDs7QUFDRCxNQUFJbkMsTUFBTSxHQUFHLEVBQWI7O0FBQ0EsT0FBSyxJQUFJbUIsR0FBVCxJQUFnQlYsTUFBTSxDQUFDMEIsTUFBRCxDQUF0QixFQUFnQztBQUM5QixRQUFJekIsY0FBYyxDQUFDVSxJQUFmLENBQW9CZSxNQUFwQixFQUE0QmhCLEdBQTVCLEtBQW9DQSxHQUFHLElBQUksYUFBL0MsRUFBOEQ7QUFDNURuQixZQUFNLENBQUNuRSxJQUFQLENBQVlzRixHQUFaO0FBQ0Q7QUFDRjs7QUFDRCxTQUFPbkIsTUFBUDtBQUNEOztBQUVEeEYsTUFBTSxDQUFDQyxPQUFQLEdBQWlCd04sUUFBakIsQzs7Ozs7Ozs7Ozs7QUM3QkEsSUFBSW5ELFdBQVcsR0FBR2hJLG1CQUFPLENBQUMsaUVBQUQsQ0FBekI7QUFBQSxJQUNJb0wsWUFBWSxHQUFHcEwsbUJBQU8sQ0FBQyxtRUFBRCxDQUQxQjtBQUFBLElBRUlxTCx1QkFBdUIsR0FBR3JMLG1CQUFPLENBQUMseUZBQUQsQ0FGckM7QUFJQTs7Ozs7Ozs7O0FBT0EsU0FBUzRLLFdBQVQsQ0FBcUJ0TCxNQUFyQixFQUE2QjtBQUMzQixNQUFJMkksU0FBUyxHQUFHbUQsWUFBWSxDQUFDOUwsTUFBRCxDQUE1Qjs7QUFDQSxNQUFJMkksU0FBUyxDQUFDckosTUFBVixJQUFvQixDQUFwQixJQUF5QnFKLFNBQVMsQ0FBQyxDQUFELENBQVQsQ0FBYSxDQUFiLENBQTdCLEVBQThDO0FBQzVDLFdBQU9vRCx1QkFBdUIsQ0FBQ3BELFNBQVMsQ0FBQyxDQUFELENBQVQsQ0FBYSxDQUFiLENBQUQsRUFBa0JBLFNBQVMsQ0FBQyxDQUFELENBQVQsQ0FBYSxDQUFiLENBQWxCLENBQTlCO0FBQ0Q7O0FBQ0QsU0FBTyxVQUFTNUMsTUFBVCxFQUFpQjtBQUN0QixXQUFPQSxNQUFNLEtBQUsvRixNQUFYLElBQXFCMEksV0FBVyxDQUFDM0MsTUFBRCxFQUFTL0YsTUFBVCxFQUFpQjJJLFNBQWpCLENBQXZDO0FBQ0QsR0FGRDtBQUdEOztBQUVEdkssTUFBTSxDQUFDQyxPQUFQLEdBQWlCaU4sV0FBakIsQzs7Ozs7Ozs7Ozs7QUNyQkEsSUFBSXJFLFdBQVcsR0FBR3ZHLG1CQUFPLENBQUMsaUVBQUQsQ0FBekI7QUFBQSxJQUNJYyxHQUFHLEdBQUdkLG1CQUFPLENBQUMsK0NBQUQsQ0FEakI7QUFBQSxJQUVJc0wsS0FBSyxHQUFHdEwsbUJBQU8sQ0FBQyxtREFBRCxDQUZuQjtBQUFBLElBR0l1TCxLQUFLLEdBQUd2TCxtQkFBTyxDQUFDLHFEQUFELENBSG5CO0FBQUEsSUFJSXdMLGtCQUFrQixHQUFHeEwsbUJBQU8sQ0FBQywrRUFBRCxDQUpoQztBQUFBLElBS0lxTCx1QkFBdUIsR0FBR3JMLG1CQUFPLENBQUMseUZBQUQsQ0FMckM7QUFBQSxJQU1JbUYsS0FBSyxHQUFHbkYsbUJBQU8sQ0FBQyxxREFBRCxDQU5uQjtBQVFBOzs7QUFDQSxJQUFJZ0gsb0JBQW9CLEdBQUcsQ0FBM0I7QUFBQSxJQUNJZSxzQkFBc0IsR0FBRyxDQUQ3QjtBQUdBOzs7Ozs7Ozs7QUFRQSxTQUFTOEMsbUJBQVQsQ0FBNkJ2RixJQUE3QixFQUFtQzhDLFFBQW5DLEVBQTZDO0FBQzNDLE1BQUltRCxLQUFLLENBQUNqRyxJQUFELENBQUwsSUFBZWtHLGtCQUFrQixDQUFDcEQsUUFBRCxDQUFyQyxFQUFpRDtBQUMvQyxXQUFPaUQsdUJBQXVCLENBQUNsRyxLQUFLLENBQUNHLElBQUQsQ0FBTixFQUFjOEMsUUFBZCxDQUE5QjtBQUNEOztBQUNELFNBQU8sVUFBUy9DLE1BQVQsRUFBaUI7QUFDdEIsUUFBSThDLFFBQVEsR0FBR3JILEdBQUcsQ0FBQ3VFLE1BQUQsRUFBU0MsSUFBVCxDQUFsQjtBQUNBLFdBQVE2QyxRQUFRLEtBQUs1QyxTQUFiLElBQTBCNEMsUUFBUSxLQUFLQyxRQUF4QyxHQUNIa0QsS0FBSyxDQUFDakcsTUFBRCxFQUFTQyxJQUFULENBREYsR0FFSGlCLFdBQVcsQ0FBQzZCLFFBQUQsRUFBV0QsUUFBWCxFQUFxQm5CLG9CQUFvQixHQUFHZSxzQkFBNUMsQ0FGZjtBQUdELEdBTEQ7QUFNRDs7QUFFRHJLLE1BQU0sQ0FBQ0MsT0FBUCxHQUFpQmtOLG1CQUFqQixDOzs7Ozs7Ozs7OztBQ2hDQTs7Ozs7OztBQU9BLFNBQVNZLFlBQVQsQ0FBc0JwSCxHQUF0QixFQUEyQjtBQUN6QixTQUFPLFVBQVNnQixNQUFULEVBQWlCO0FBQ3RCLFdBQU9BLE1BQU0sSUFBSSxJQUFWLEdBQWlCRSxTQUFqQixHQUE2QkYsTUFBTSxDQUFDaEIsR0FBRCxDQUExQztBQUNELEdBRkQ7QUFHRDs7QUFFRDNHLE1BQU0sQ0FBQ0MsT0FBUCxHQUFpQjhOLFlBQWpCLEM7Ozs7Ozs7Ozs7O0FDYkEsSUFBSXJHLE9BQU8sR0FBR3BGLG1CQUFPLENBQUMseURBQUQsQ0FBckI7QUFFQTs7Ozs7Ozs7O0FBT0EsU0FBUzBMLGdCQUFULENBQTBCcEcsSUFBMUIsRUFBZ0M7QUFDOUIsU0FBTyxVQUFTRCxNQUFULEVBQWlCO0FBQ3RCLFdBQU9ELE9BQU8sQ0FBQ0MsTUFBRCxFQUFTQyxJQUFULENBQWQ7QUFDRCxHQUZEO0FBR0Q7O0FBRUQ1SCxNQUFNLENBQUNDLE9BQVAsR0FBaUIrTixnQkFBakIsQzs7Ozs7Ozs7Ozs7QUNmQTs7Ozs7Ozs7O0FBU0EsU0FBU3RJLFNBQVQsQ0FBbUJ1SSxDQUFuQixFQUFzQm5ILFFBQXRCLEVBQWdDO0FBQzlCLE1BQUk5RCxLQUFLLEdBQUcsQ0FBQyxDQUFiO0FBQUEsTUFDSXdDLE1BQU0sR0FBR3VCLEtBQUssQ0FBQ2tILENBQUQsQ0FEbEI7O0FBR0EsU0FBTyxFQUFFakwsS0FBRixHQUFVaUwsQ0FBakIsRUFBb0I7QUFDbEJ6SSxVQUFNLENBQUN4QyxLQUFELENBQU4sR0FBZ0I4RCxRQUFRLENBQUM5RCxLQUFELENBQXhCO0FBQ0Q7O0FBQ0QsU0FBT3dDLE1BQVA7QUFDRDs7QUFFRHhGLE1BQU0sQ0FBQ0MsT0FBUCxHQUFpQnlGLFNBQWpCLEM7Ozs7Ozs7Ozs7O0FDbkJBLElBQUk1RixPQUFNLEdBQUd3QyxtQkFBTyxDQUFDLHVEQUFELENBQXBCO0FBQUEsSUFDSXVFLFFBQVEsR0FBR3ZFLG1CQUFPLENBQUMsMkRBQUQsQ0FEdEI7QUFBQSxJQUVJc0QsT0FBTyxHQUFHdEQsbUJBQU8sQ0FBQyx1REFBRCxDQUZyQjtBQUFBLElBR0k0TCxRQUFRLEdBQUc1TCxtQkFBTyxDQUFDLHlEQUFELENBSHRCO0FBS0E7OztBQUNBLElBQUk2TCxRQUFRLEdBQUcsSUFBSSxDQUFuQjtBQUVBOztBQUNBLElBQUlDLFdBQVcsR0FBR3RPLE9BQU0sR0FBR0EsT0FBTSxDQUFDSyxTQUFWLEdBQXNCMEgsU0FBOUM7QUFBQSxJQUNJd0csY0FBYyxHQUFHRCxXQUFXLEdBQUdBLFdBQVcsQ0FBQzlOLFFBQWYsR0FBMEJ1SCxTQUQxRDtBQUdBOzs7Ozs7Ozs7QUFRQSxTQUFTeUcsWUFBVCxDQUFzQjdJLEtBQXRCLEVBQTZCO0FBQzNCO0FBQ0EsTUFBSSxPQUFPQSxLQUFQLElBQWdCLFFBQXBCLEVBQThCO0FBQzVCLFdBQU9BLEtBQVA7QUFDRDs7QUFDRCxNQUFJRyxPQUFPLENBQUNILEtBQUQsQ0FBWCxFQUFvQjtBQUNsQjtBQUNBLFdBQU9vQixRQUFRLENBQUNwQixLQUFELEVBQVE2SSxZQUFSLENBQVIsR0FBZ0MsRUFBdkM7QUFDRDs7QUFDRCxNQUFJSixRQUFRLENBQUN6SSxLQUFELENBQVosRUFBcUI7QUFDbkIsV0FBTzRJLGNBQWMsR0FBR0EsY0FBYyxDQUFDekgsSUFBZixDQUFvQm5CLEtBQXBCLENBQUgsR0FBZ0MsRUFBckQ7QUFDRDs7QUFDRCxNQUFJRCxNQUFNLEdBQUlDLEtBQUssR0FBRyxFQUF0QjtBQUNBLFNBQVFELE1BQU0sSUFBSSxHQUFWLElBQWtCLElBQUlDLEtBQUwsSUFBZSxDQUFDMEksUUFBbEMsR0FBOEMsSUFBOUMsR0FBcUQzSSxNQUE1RDtBQUNEOztBQUVEeEYsTUFBTSxDQUFDQyxPQUFQLEdBQWlCcU8sWUFBakIsQzs7Ozs7Ozs7Ozs7QUNwQ0E7Ozs7Ozs7QUFPQSxTQUFTQyxTQUFULENBQW1CQyxJQUFuQixFQUF5QjtBQUN2QixTQUFPLFVBQVMvSSxLQUFULEVBQWdCO0FBQ3JCLFdBQU8rSSxJQUFJLENBQUMvSSxLQUFELENBQVg7QUFDRCxHQUZEO0FBR0Q7O0FBRUR6RixNQUFNLENBQUNDLE9BQVAsR0FBaUJzTyxTQUFqQixDOzs7Ozs7Ozs7OztBQ2JBOzs7Ozs7OztBQVFBLFNBQVNFLFFBQVQsQ0FBa0JDLEtBQWxCLEVBQXlCL0gsR0FBekIsRUFBOEI7QUFDNUIsU0FBTytILEtBQUssQ0FBQ3JMLEdBQU4sQ0FBVXNELEdBQVYsQ0FBUDtBQUNEOztBQUVEM0csTUFBTSxDQUFDQyxPQUFQLEdBQWlCd08sUUFBakIsQzs7Ozs7Ozs7Ozs7QUNaQSxJQUFJN0ksT0FBTyxHQUFHdEQsbUJBQU8sQ0FBQyx1REFBRCxDQUFyQjtBQUFBLElBQ0l1TCxLQUFLLEdBQUd2TCxtQkFBTyxDQUFDLHFEQUFELENBRG5CO0FBQUEsSUFFSXFNLFlBQVksR0FBR3JNLG1CQUFPLENBQUMsbUVBQUQsQ0FGMUI7QUFBQSxJQUdJaEMsUUFBUSxHQUFHZ0MsbUJBQU8sQ0FBQyx5REFBRCxDQUh0QjtBQUtBOzs7Ozs7Ozs7O0FBUUEsU0FBU2tGLFFBQVQsQ0FBa0IvQixLQUFsQixFQUF5QmtDLE1BQXpCLEVBQWlDO0FBQy9CLE1BQUkvQixPQUFPLENBQUNILEtBQUQsQ0FBWCxFQUFvQjtBQUNsQixXQUFPQSxLQUFQO0FBQ0Q7O0FBQ0QsU0FBT29JLEtBQUssQ0FBQ3BJLEtBQUQsRUFBUWtDLE1BQVIsQ0FBTCxHQUF1QixDQUFDbEMsS0FBRCxDQUF2QixHQUFpQ2tKLFlBQVksQ0FBQ3JPLFFBQVEsQ0FBQ21GLEtBQUQsQ0FBVCxDQUFwRDtBQUNEOztBQUVEekYsTUFBTSxDQUFDQyxPQUFQLEdBQWlCdUgsUUFBakIsQzs7Ozs7Ozs7Ozs7QUNwQkEsSUFBSWpGLElBQUksR0FBR0QsbUJBQU8sQ0FBQyxtREFBRCxDQUFsQjtBQUVBOzs7QUFDQSxJQUFJc00sVUFBVSxHQUFHck0sSUFBSSxDQUFDLG9CQUFELENBQXJCO0FBRUF2QyxNQUFNLENBQUNDLE9BQVAsR0FBaUIyTyxVQUFqQixDOzs7Ozs7Ozs7OztBQ0xBLElBQUl0QixZQUFZLEdBQUdoTCxtQkFBTyxDQUFDLG1FQUFELENBQTFCO0FBQUEsSUFDSXVNLFdBQVcsR0FBR3ZNLG1CQUFPLENBQUMsK0RBQUQsQ0FEekI7QUFBQSxJQUVJd00sSUFBSSxHQUFHeE0sbUJBQU8sQ0FBQyxpREFBRCxDQUZsQjtBQUlBOzs7Ozs7Ozs7QUFPQSxTQUFTeU0sVUFBVCxDQUFvQkMsYUFBcEIsRUFBbUM7QUFDakMsU0FBTyxVQUFTQyxVQUFULEVBQXFCM0osU0FBckIsRUFBZ0NnQyxTQUFoQyxFQUEyQztBQUNoRCxRQUFJNEgsUUFBUSxHQUFHakosTUFBTSxDQUFDZ0osVUFBRCxDQUFyQjs7QUFDQSxRQUFJLENBQUNKLFdBQVcsQ0FBQ0ksVUFBRCxDQUFoQixFQUE4QjtBQUM1QixVQUFJbkksUUFBUSxHQUFHd0csWUFBWSxDQUFDaEksU0FBRCxFQUFZLENBQVosQ0FBM0I7QUFDQTJKLGdCQUFVLEdBQUdILElBQUksQ0FBQ0csVUFBRCxDQUFqQjs7QUFDQTNKLGVBQVMsR0FBRyxtQkFBU3FCLEdBQVQsRUFBYztBQUFFLGVBQU9HLFFBQVEsQ0FBQ29JLFFBQVEsQ0FBQ3ZJLEdBQUQsQ0FBVCxFQUFnQkEsR0FBaEIsRUFBcUJ1SSxRQUFyQixDQUFmO0FBQWdELE9BQTVFO0FBQ0Q7O0FBQ0QsUUFBSWxNLEtBQUssR0FBR2dNLGFBQWEsQ0FBQ0MsVUFBRCxFQUFhM0osU0FBYixFQUF3QmdDLFNBQXhCLENBQXpCO0FBQ0EsV0FBT3RFLEtBQUssR0FBRyxDQUFDLENBQVQsR0FBYWtNLFFBQVEsQ0FBQ3BJLFFBQVEsR0FBR21JLFVBQVUsQ0FBQ2pNLEtBQUQsQ0FBYixHQUF1QkEsS0FBaEMsQ0FBckIsR0FBOEQ2RSxTQUFyRTtBQUNELEdBVEQ7QUFVRDs7QUFFRDdILE1BQU0sQ0FBQ0MsT0FBUCxHQUFpQjhPLFVBQWpCLEM7Ozs7Ozs7Ozs7O0FDeEJBLElBQUl4SyxRQUFRLEdBQUdqQyxtQkFBTyxDQUFDLDJEQUFELENBQXRCO0FBQUEsSUFDSTRFLFNBQVMsR0FBRzVFLG1CQUFPLENBQUMsNkRBQUQsQ0FEdkI7QUFBQSxJQUVJbU0sUUFBUSxHQUFHbk0sbUJBQU8sQ0FBQywyREFBRCxDQUZ0QjtBQUlBOzs7QUFDQSxJQUFJZ0gsb0JBQW9CLEdBQUcsQ0FBM0I7QUFBQSxJQUNJZSxzQkFBc0IsR0FBRyxDQUQ3QjtBQUdBOzs7Ozs7Ozs7Ozs7OztBQWFBLFNBQVNuQixXQUFULENBQXFCN0QsS0FBckIsRUFBNEJ5RCxLQUE1QixFQUFtQ0MsT0FBbkMsRUFBNENDLFVBQTVDLEVBQXdEUyxTQUF4RCxFQUFtRVIsS0FBbkUsRUFBMEU7QUFDeEUsTUFBSWtHLFNBQVMsR0FBR3BHLE9BQU8sR0FBR08sb0JBQTFCO0FBQUEsTUFDSThGLFNBQVMsR0FBRy9KLEtBQUssQ0FBQ25FLE1BRHRCO0FBQUEsTUFFSW1PLFNBQVMsR0FBR3ZHLEtBQUssQ0FBQzVILE1BRnRCOztBQUlBLE1BQUlrTyxTQUFTLElBQUlDLFNBQWIsSUFBMEIsRUFBRUYsU0FBUyxJQUFJRSxTQUFTLEdBQUdELFNBQTNCLENBQTlCLEVBQXFFO0FBQ25FLFdBQU8sS0FBUDtBQUNELEdBUHVFLENBUXhFOzs7QUFDQSxNQUFJRSxPQUFPLEdBQUdyRyxLQUFLLENBQUM3RixHQUFOLENBQVVpQyxLQUFWLENBQWQ7O0FBQ0EsTUFBSWlLLE9BQU8sSUFBSXJHLEtBQUssQ0FBQzdGLEdBQU4sQ0FBVTBGLEtBQVYsQ0FBZixFQUFpQztBQUMvQixXQUFPd0csT0FBTyxJQUFJeEcsS0FBbEI7QUFDRDs7QUFDRCxNQUFJOUYsS0FBSyxHQUFHLENBQUMsQ0FBYjtBQUFBLE1BQ0l3QyxNQUFNLEdBQUcsSUFEYjtBQUFBLE1BRUkrSixJQUFJLEdBQUl4RyxPQUFPLEdBQUdzQixzQkFBWCxHQUFxQyxJQUFJOUYsUUFBSixFQUFyQyxHQUFvRHNELFNBRi9EO0FBSUFvQixPQUFLLENBQUM5RixHQUFOLENBQVVrQyxLQUFWLEVBQWlCeUQsS0FBakI7QUFDQUcsT0FBSyxDQUFDOUYsR0FBTixDQUFVMkYsS0FBVixFQUFpQnpELEtBQWpCLEVBbEJ3RSxDQW9CeEU7O0FBQ0EsU0FBTyxFQUFFckMsS0FBRixHQUFVb00sU0FBakIsRUFBNEI7QUFDMUIsUUFBSUksUUFBUSxHQUFHbkssS0FBSyxDQUFDckMsS0FBRCxDQUFwQjtBQUFBLFFBQ0l5TSxRQUFRLEdBQUczRyxLQUFLLENBQUM5RixLQUFELENBRHBCOztBQUdBLFFBQUlnRyxVQUFKLEVBQWdCO0FBQ2QsVUFBSTBHLFFBQVEsR0FBR1AsU0FBUyxHQUNwQm5HLFVBQVUsQ0FBQ3lHLFFBQUQsRUFBV0QsUUFBWCxFQUFxQnhNLEtBQXJCLEVBQTRCOEYsS0FBNUIsRUFBbUN6RCxLQUFuQyxFQUEwQzRELEtBQTFDLENBRFUsR0FFcEJELFVBQVUsQ0FBQ3dHLFFBQUQsRUFBV0MsUUFBWCxFQUFxQnpNLEtBQXJCLEVBQTRCcUMsS0FBNUIsRUFBbUN5RCxLQUFuQyxFQUEwQ0csS0FBMUMsQ0FGZDtBQUdEOztBQUNELFFBQUl5RyxRQUFRLEtBQUs3SCxTQUFqQixFQUE0QjtBQUMxQixVQUFJNkgsUUFBSixFQUFjO0FBQ1o7QUFDRDs7QUFDRGxLLFlBQU0sR0FBRyxLQUFUO0FBQ0E7QUFDRCxLQWZ5QixDQWdCMUI7OztBQUNBLFFBQUkrSixJQUFKLEVBQVU7QUFDUixVQUFJLENBQUNySSxTQUFTLENBQUM0QixLQUFELEVBQVEsVUFBUzJHLFFBQVQsRUFBbUJFLFFBQW5CLEVBQTZCO0FBQzdDLFlBQUksQ0FBQ2xCLFFBQVEsQ0FBQ2MsSUFBRCxFQUFPSSxRQUFQLENBQVQsS0FDQ0gsUUFBUSxLQUFLQyxRQUFiLElBQXlCaEcsU0FBUyxDQUFDK0YsUUFBRCxFQUFXQyxRQUFYLEVBQXFCMUcsT0FBckIsRUFBOEJDLFVBQTlCLEVBQTBDQyxLQUExQyxDQURuQyxDQUFKLEVBQzBGO0FBQ3hGLGlCQUFPc0csSUFBSSxDQUFDbE8sSUFBTCxDQUFVc08sUUFBVixDQUFQO0FBQ0Q7QUFDRixPQUxTLENBQWQsRUFLUTtBQUNObkssY0FBTSxHQUFHLEtBQVQ7QUFDQTtBQUNEO0FBQ0YsS0FWRCxNQVVPLElBQUksRUFDTGdLLFFBQVEsS0FBS0MsUUFBYixJQUNFaEcsU0FBUyxDQUFDK0YsUUFBRCxFQUFXQyxRQUFYLEVBQXFCMUcsT0FBckIsRUFBOEJDLFVBQTlCLEVBQTBDQyxLQUExQyxDQUZOLENBQUosRUFHQTtBQUNMekQsWUFBTSxHQUFHLEtBQVQ7QUFDQTtBQUNEO0FBQ0Y7O0FBQ0R5RCxPQUFLLENBQUMsUUFBRCxDQUFMLENBQWdCNUQsS0FBaEI7QUFDQTRELE9BQUssQ0FBQyxRQUFELENBQUwsQ0FBZ0JILEtBQWhCO0FBQ0EsU0FBT3RELE1BQVA7QUFDRDs7QUFFRHhGLE1BQU0sQ0FBQ0MsT0FBUCxHQUFpQmlKLFdBQWpCLEM7Ozs7Ozs7Ozs7O0FDbEZBLElBQUlwSixPQUFNLEdBQUd3QyxtQkFBTyxDQUFDLHVEQUFELENBQXBCO0FBQUEsSUFDSTRDLFVBQVUsR0FBRzVDLG1CQUFPLENBQUMsK0RBQUQsQ0FEeEI7QUFBQSxJQUVJNkUsRUFBRSxHQUFHN0UsbUJBQU8sQ0FBQyw2Q0FBRCxDQUZoQjtBQUFBLElBR0k0RyxXQUFXLEdBQUc1RyxtQkFBTyxDQUFDLGlFQUFELENBSHpCO0FBQUEsSUFJSXNOLFVBQVUsR0FBR3ROLG1CQUFPLENBQUMsK0RBQUQsQ0FKeEI7QUFBQSxJQUtJdU4sVUFBVSxHQUFHdk4sbUJBQU8sQ0FBQywrREFBRCxDQUx4QjtBQU9BOzs7QUFDQSxJQUFJZ0gsb0JBQW9CLEdBQUcsQ0FBM0I7QUFBQSxJQUNJZSxzQkFBc0IsR0FBRyxDQUQ3QjtBQUdBOztBQUNBLElBQUlzQixPQUFPLEdBQUcsa0JBQWQ7QUFBQSxJQUNJQyxPQUFPLEdBQUcsZUFEZDtBQUFBLElBRUlDLFFBQVEsR0FBRyxnQkFGZjtBQUFBLElBR0lFLE1BQU0sR0FBRyxjQUhiO0FBQUEsSUFJSUMsU0FBUyxHQUFHLGlCQUpoQjtBQUFBLElBS0lDLFNBQVMsR0FBRyxpQkFMaEI7QUFBQSxJQU1JQyxNQUFNLEdBQUcsY0FOYjtBQUFBLElBT0lDLFNBQVMsR0FBRyxpQkFQaEI7QUFBQSxJQVFJMkQsU0FBUyxHQUFHLGlCQVJoQjtBQVVBLElBQUl6RCxjQUFjLEdBQUcsc0JBQXJCO0FBQUEsSUFDSUMsV0FBVyxHQUFHLG1CQURsQjtBQUdBOztBQUNBLElBQUk4QixXQUFXLEdBQUd0TyxPQUFNLEdBQUdBLE9BQU0sQ0FBQ0ssU0FBVixHQUFzQjBILFNBQTlDO0FBQUEsSUFDSWtJLGFBQWEsR0FBRzNCLFdBQVcsR0FBR0EsV0FBVyxDQUFDNEIsT0FBZixHQUF5Qm5JLFNBRHhEO0FBR0E7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWlCQSxTQUFTc0IsVUFBVCxDQUFvQnhCLE1BQXBCLEVBQTRCbUIsS0FBNUIsRUFBbUNtSCxHQUFuQyxFQUF3Q2xILE9BQXhDLEVBQWlEQyxVQUFqRCxFQUE2RFMsU0FBN0QsRUFBd0VSLEtBQXhFLEVBQStFO0FBQzdFLFVBQVFnSCxHQUFSO0FBQ0UsU0FBSzNELFdBQUw7QUFDRSxVQUFLM0UsTUFBTSxDQUFDdUksVUFBUCxJQUFxQnBILEtBQUssQ0FBQ29ILFVBQTVCLElBQ0N2SSxNQUFNLENBQUN3SSxVQUFQLElBQXFCckgsS0FBSyxDQUFDcUgsVUFEaEMsRUFDNkM7QUFDM0MsZUFBTyxLQUFQO0FBQ0Q7O0FBQ0R4SSxZQUFNLEdBQUdBLE1BQU0sQ0FBQ3lJLE1BQWhCO0FBQ0F0SCxXQUFLLEdBQUdBLEtBQUssQ0FBQ3NILE1BQWQ7O0FBRUYsU0FBSy9ELGNBQUw7QUFDRSxVQUFLMUUsTUFBTSxDQUFDdUksVUFBUCxJQUFxQnBILEtBQUssQ0FBQ29ILFVBQTVCLElBQ0EsQ0FBQ3pHLFNBQVMsQ0FBQyxJQUFJdkUsVUFBSixDQUFleUMsTUFBZixDQUFELEVBQXlCLElBQUl6QyxVQUFKLENBQWU0RCxLQUFmLENBQXpCLENBRGQsRUFDK0Q7QUFDN0QsZUFBTyxLQUFQO0FBQ0Q7O0FBQ0QsYUFBTyxJQUFQOztBQUVGLFNBQUs2QyxPQUFMO0FBQ0EsU0FBS0MsT0FBTDtBQUNBLFNBQUtJLFNBQUw7QUFDRTtBQUNBO0FBQ0EsYUFBTzdFLEVBQUUsQ0FBQyxDQUFDUSxNQUFGLEVBQVUsQ0FBQ21CLEtBQVgsQ0FBVDs7QUFFRixTQUFLK0MsUUFBTDtBQUNFLGFBQU9sRSxNQUFNLENBQUMwSSxJQUFQLElBQWV2SCxLQUFLLENBQUN1SCxJQUFyQixJQUE2QjFJLE1BQU0sQ0FBQzJJLE9BQVAsSUFBa0J4SCxLQUFLLENBQUN3SCxPQUE1RDs7QUFFRixTQUFLckUsU0FBTDtBQUNBLFNBQUtFLFNBQUw7QUFDRTtBQUNBO0FBQ0E7QUFDQSxhQUFPeEUsTUFBTSxJQUFLbUIsS0FBSyxHQUFHLEVBQTFCOztBQUVGLFNBQUtpRCxNQUFMO0FBQ0UsVUFBSXdFLE9BQU8sR0FBR1gsVUFBZDs7QUFFRixTQUFLMUQsTUFBTDtBQUNFLFVBQUlpRCxTQUFTLEdBQUdwRyxPQUFPLEdBQUdPLG9CQUExQjtBQUNBaUgsYUFBTyxLQUFLQSxPQUFPLEdBQUdWLFVBQWYsQ0FBUDs7QUFFQSxVQUFJbEksTUFBTSxDQUFDMUMsSUFBUCxJQUFlNkQsS0FBSyxDQUFDN0QsSUFBckIsSUFBNkIsQ0FBQ2tLLFNBQWxDLEVBQTZDO0FBQzNDLGVBQU8sS0FBUDtBQUNELE9BTkgsQ0FPRTs7O0FBQ0EsVUFBSUcsT0FBTyxHQUFHckcsS0FBSyxDQUFDN0YsR0FBTixDQUFVdUUsTUFBVixDQUFkOztBQUNBLFVBQUkySCxPQUFKLEVBQWE7QUFDWCxlQUFPQSxPQUFPLElBQUl4RyxLQUFsQjtBQUNEOztBQUNEQyxhQUFPLElBQUlzQixzQkFBWCxDQVpGLENBY0U7O0FBQ0FwQixXQUFLLENBQUM5RixHQUFOLENBQVV3RSxNQUFWLEVBQWtCbUIsS0FBbEI7QUFDQSxVQUFJdEQsTUFBTSxHQUFHMEQsV0FBVyxDQUFDcUgsT0FBTyxDQUFDNUksTUFBRCxDQUFSLEVBQWtCNEksT0FBTyxDQUFDekgsS0FBRCxDQUF6QixFQUFrQ0MsT0FBbEMsRUFBMkNDLFVBQTNDLEVBQXVEUyxTQUF2RCxFQUFrRVIsS0FBbEUsQ0FBeEI7QUFDQUEsV0FBSyxDQUFDLFFBQUQsQ0FBTCxDQUFnQnRCLE1BQWhCO0FBQ0EsYUFBT25DLE1BQVA7O0FBRUYsU0FBS3NLLFNBQUw7QUFDRSxVQUFJQyxhQUFKLEVBQW1CO0FBQ2pCLGVBQU9BLGFBQWEsQ0FBQ25KLElBQWQsQ0FBbUJlLE1BQW5CLEtBQThCb0ksYUFBYSxDQUFDbkosSUFBZCxDQUFtQmtDLEtBQW5CLENBQXJDO0FBQ0Q7O0FBM0RMOztBQTZEQSxTQUFPLEtBQVA7QUFDRDs7QUFFRDlJLE1BQU0sQ0FBQ0MsT0FBUCxHQUFpQmtKLFVBQWpCLEM7Ozs7Ozs7Ozs7O0FDL0dBLElBQUlxSCxVQUFVLEdBQUdsTyxtQkFBTyxDQUFDLCtEQUFELENBQXhCO0FBRUE7OztBQUNBLElBQUlnSCxvQkFBb0IsR0FBRyxDQUEzQjtBQUVBOztBQUNBLElBQUl0RCxXQUFXLEdBQUdDLE1BQU0sQ0FBQzlGLFNBQXpCO0FBRUE7O0FBQ0EsSUFBSStGLGNBQWMsR0FBR0YsV0FBVyxDQUFDRSxjQUFqQztBQUVBOzs7Ozs7Ozs7Ozs7OztBQWFBLFNBQVNrRCxZQUFULENBQXNCekIsTUFBdEIsRUFBOEJtQixLQUE5QixFQUFxQ0MsT0FBckMsRUFBOENDLFVBQTlDLEVBQTBEUyxTQUExRCxFQUFxRVIsS0FBckUsRUFBNEU7QUFDMUUsTUFBSWtHLFNBQVMsR0FBR3BHLE9BQU8sR0FBR08sb0JBQTFCO0FBQUEsTUFDSW1ILFFBQVEsR0FBR0QsVUFBVSxDQUFDN0ksTUFBRCxDQUR6QjtBQUFBLE1BRUkrSSxTQUFTLEdBQUdELFFBQVEsQ0FBQ3ZQLE1BRnpCO0FBQUEsTUFHSXlQLFFBQVEsR0FBR0gsVUFBVSxDQUFDMUgsS0FBRCxDQUh6QjtBQUFBLE1BSUl1RyxTQUFTLEdBQUdzQixRQUFRLENBQUN6UCxNQUp6Qjs7QUFNQSxNQUFJd1AsU0FBUyxJQUFJckIsU0FBYixJQUEwQixDQUFDRixTQUEvQixFQUEwQztBQUN4QyxXQUFPLEtBQVA7QUFDRDs7QUFDRCxNQUFJbk0sS0FBSyxHQUFHME4sU0FBWjs7QUFDQSxTQUFPMU4sS0FBSyxFQUFaLEVBQWdCO0FBQ2QsUUFBSTJELEdBQUcsR0FBRzhKLFFBQVEsQ0FBQ3pOLEtBQUQsQ0FBbEI7O0FBQ0EsUUFBSSxFQUFFbU0sU0FBUyxHQUFHeEksR0FBRyxJQUFJbUMsS0FBVixHQUFrQjVDLGNBQWMsQ0FBQ1UsSUFBZixDQUFvQmtDLEtBQXBCLEVBQTJCbkMsR0FBM0IsQ0FBN0IsQ0FBSixFQUFtRTtBQUNqRSxhQUFPLEtBQVA7QUFDRDtBQUNGLEdBaEJ5RSxDQWlCMUU7OztBQUNBLE1BQUkySSxPQUFPLEdBQUdyRyxLQUFLLENBQUM3RixHQUFOLENBQVV1RSxNQUFWLENBQWQ7O0FBQ0EsTUFBSTJILE9BQU8sSUFBSXJHLEtBQUssQ0FBQzdGLEdBQU4sQ0FBVTBGLEtBQVYsQ0FBZixFQUFpQztBQUMvQixXQUFPd0csT0FBTyxJQUFJeEcsS0FBbEI7QUFDRDs7QUFDRCxNQUFJdEQsTUFBTSxHQUFHLElBQWI7QUFDQXlELE9BQUssQ0FBQzlGLEdBQU4sQ0FBVXdFLE1BQVYsRUFBa0JtQixLQUFsQjtBQUNBRyxPQUFLLENBQUM5RixHQUFOLENBQVUyRixLQUFWLEVBQWlCbkIsTUFBakI7QUFFQSxNQUFJaUosUUFBUSxHQUFHekIsU0FBZjs7QUFDQSxTQUFPLEVBQUVuTSxLQUFGLEdBQVUwTixTQUFqQixFQUE0QjtBQUMxQi9KLE9BQUcsR0FBRzhKLFFBQVEsQ0FBQ3pOLEtBQUQsQ0FBZDtBQUNBLFFBQUl5SCxRQUFRLEdBQUc5QyxNQUFNLENBQUNoQixHQUFELENBQXJCO0FBQUEsUUFDSThJLFFBQVEsR0FBRzNHLEtBQUssQ0FBQ25DLEdBQUQsQ0FEcEI7O0FBR0EsUUFBSXFDLFVBQUosRUFBZ0I7QUFDZCxVQUFJMEcsUUFBUSxHQUFHUCxTQUFTLEdBQ3BCbkcsVUFBVSxDQUFDeUcsUUFBRCxFQUFXaEYsUUFBWCxFQUFxQjlELEdBQXJCLEVBQTBCbUMsS0FBMUIsRUFBaUNuQixNQUFqQyxFQUF5Q3NCLEtBQXpDLENBRFUsR0FFcEJELFVBQVUsQ0FBQ3lCLFFBQUQsRUFBV2dGLFFBQVgsRUFBcUI5SSxHQUFyQixFQUEwQmdCLE1BQTFCLEVBQWtDbUIsS0FBbEMsRUFBeUNHLEtBQXpDLENBRmQ7QUFHRCxLQVR5QixDQVUxQjs7O0FBQ0EsUUFBSSxFQUFFeUcsUUFBUSxLQUFLN0gsU0FBYixHQUNHNEMsUUFBUSxLQUFLZ0YsUUFBYixJQUF5QmhHLFNBQVMsQ0FBQ2dCLFFBQUQsRUFBV2dGLFFBQVgsRUFBcUIxRyxPQUFyQixFQUE4QkMsVUFBOUIsRUFBMENDLEtBQTFDLENBRHJDLEdBRUV5RyxRQUZKLENBQUosRUFHTztBQUNMbEssWUFBTSxHQUFHLEtBQVQ7QUFDQTtBQUNEOztBQUNEb0wsWUFBUSxLQUFLQSxRQUFRLEdBQUdqSyxHQUFHLElBQUksYUFBdkIsQ0FBUjtBQUNEOztBQUNELE1BQUluQixNQUFNLElBQUksQ0FBQ29MLFFBQWYsRUFBeUI7QUFDdkIsUUFBSUMsT0FBTyxHQUFHbEosTUFBTSxDQUFDekgsV0FBckI7QUFBQSxRQUNJNFEsT0FBTyxHQUFHaEksS0FBSyxDQUFDNUksV0FEcEIsQ0FEdUIsQ0FJdkI7O0FBQ0EsUUFBSTJRLE9BQU8sSUFBSUMsT0FBWCxJQUNDLGlCQUFpQm5KLE1BQWpCLElBQTJCLGlCQUFpQm1CLEtBRDdDLElBRUEsRUFBRSxPQUFPK0gsT0FBUCxJQUFrQixVQUFsQixJQUFnQ0EsT0FBTyxZQUFZQSxPQUFuRCxJQUNBLE9BQU9DLE9BQVAsSUFBa0IsVUFEbEIsSUFDZ0NBLE9BQU8sWUFBWUEsT0FEckQsQ0FGSixFQUdtRTtBQUNqRXRMLFlBQU0sR0FBRyxLQUFUO0FBQ0Q7QUFDRjs7QUFDRHlELE9BQUssQ0FBQyxRQUFELENBQUwsQ0FBZ0J0QixNQUFoQjtBQUNBc0IsT0FBSyxDQUFDLFFBQUQsQ0FBTCxDQUFnQkgsS0FBaEI7QUFDQSxTQUFPdEQsTUFBUDtBQUNEOztBQUVEeEYsTUFBTSxDQUFDQyxPQUFQLEdBQWlCbUosWUFBakIsQzs7Ozs7Ozs7Ozs7OztBQ3hGQTtBQUNBLElBQUkySCxVQUFVLEdBQUcsUUFBT0MsTUFBUCx5Q0FBT0EsTUFBUCxNQUFpQixRQUFqQixJQUE2QkEsTUFBN0IsSUFBdUNBLE1BQU0sQ0FBQy9LLE1BQVAsS0FBa0JBLE1BQXpELElBQW1FK0ssTUFBcEY7QUFFQWhSLE1BQU0sQ0FBQ0MsT0FBUCxHQUFpQjhRLFVBQWpCLEM7Ozs7Ozs7Ozs7OztBQ0hBLElBQUlqSixjQUFjLEdBQUd4RixtQkFBTyxDQUFDLHVFQUFELENBQTVCO0FBQUEsSUFDSTJPLFVBQVUsR0FBRzNPLG1CQUFPLENBQUMsK0RBQUQsQ0FEeEI7QUFBQSxJQUVJd00sSUFBSSxHQUFHeE0sbUJBQU8sQ0FBQyxpREFBRCxDQUZsQjtBQUlBOzs7Ozs7Ozs7QUFPQSxTQUFTa08sVUFBVCxDQUFvQjdJLE1BQXBCLEVBQTRCO0FBQzFCLFNBQU9HLGNBQWMsQ0FBQ0gsTUFBRCxFQUFTbUgsSUFBVCxFQUFlbUMsVUFBZixDQUFyQjtBQUNEOztBQUVEalIsTUFBTSxDQUFDQyxPQUFQLEdBQWlCdVEsVUFBakIsQzs7Ozs7Ozs7Ozs7QUNmQSxJQUFJVSxTQUFTLEdBQUc1TyxtQkFBTyxDQUFDLDZEQUFELENBQXZCO0FBRUE7Ozs7Ozs7Ozs7QUFRQSxTQUFTNk8sVUFBVCxDQUFvQjVRLEdBQXBCLEVBQXlCb0csR0FBekIsRUFBOEI7QUFDNUIsTUFBSXZFLElBQUksR0FBRzdCLEdBQUcsQ0FBQ2tFLFFBQWY7QUFDQSxTQUFPeU0sU0FBUyxDQUFDdkssR0FBRCxDQUFULEdBQ0h2RSxJQUFJLENBQUMsT0FBT3VFLEdBQVAsSUFBYyxRQUFkLEdBQXlCLFFBQXpCLEdBQW9DLE1BQXJDLENBREQsR0FFSHZFLElBQUksQ0FBQzdCLEdBRlQ7QUFHRDs7QUFFRFAsTUFBTSxDQUFDQyxPQUFQLEdBQWlCa1IsVUFBakIsQzs7Ozs7Ozs7Ozs7QUNqQkEsSUFBSXJELGtCQUFrQixHQUFHeEwsbUJBQU8sQ0FBQywrRUFBRCxDQUFoQztBQUFBLElBQ0l3TSxJQUFJLEdBQUd4TSxtQkFBTyxDQUFDLGlEQUFELENBRGxCO0FBR0E7Ozs7Ozs7OztBQU9BLFNBQVNvTCxZQUFULENBQXNCL0YsTUFBdEIsRUFBOEI7QUFDNUIsTUFBSW5DLE1BQU0sR0FBR3NKLElBQUksQ0FBQ25ILE1BQUQsQ0FBakI7QUFBQSxNQUNJekcsTUFBTSxHQUFHc0UsTUFBTSxDQUFDdEUsTUFEcEI7O0FBR0EsU0FBT0EsTUFBTSxFQUFiLEVBQWlCO0FBQ2YsUUFBSXlGLEdBQUcsR0FBR25CLE1BQU0sQ0FBQ3RFLE1BQUQsQ0FBaEI7QUFBQSxRQUNJdUUsS0FBSyxHQUFHa0MsTUFBTSxDQUFDaEIsR0FBRCxDQURsQjtBQUdBbkIsVUFBTSxDQUFDdEUsTUFBRCxDQUFOLEdBQWlCLENBQUN5RixHQUFELEVBQU1sQixLQUFOLEVBQWFxSSxrQkFBa0IsQ0FBQ3JJLEtBQUQsQ0FBL0IsQ0FBakI7QUFDRDs7QUFDRCxTQUFPRCxNQUFQO0FBQ0Q7O0FBRUR4RixNQUFNLENBQUNDLE9BQVAsR0FBaUJ5TixZQUFqQixDOzs7Ozs7Ozs7OztBQ3ZCQSxJQUFJbkMsWUFBWSxHQUFHakosbUJBQU8sQ0FBQyxtRUFBRCxDQUExQjtBQUFBLElBQ0k4TyxRQUFRLEdBQUc5TyxtQkFBTyxDQUFDLDJEQUFELENBRHRCO0FBR0E7Ozs7Ozs7Ozs7QUFRQSxTQUFTRCxTQUFULENBQW1Cc0YsTUFBbkIsRUFBMkJoQixHQUEzQixFQUFnQztBQUM5QixNQUFJbEIsS0FBSyxHQUFHMkwsUUFBUSxDQUFDekosTUFBRCxFQUFTaEIsR0FBVCxDQUFwQjtBQUNBLFNBQU80RSxZQUFZLENBQUM5RixLQUFELENBQVosR0FBc0JBLEtBQXRCLEdBQThCb0MsU0FBckM7QUFDRDs7QUFFRDdILE1BQU0sQ0FBQ0MsT0FBUCxHQUFpQm9DLFNBQWpCLEM7Ozs7Ozs7Ozs7O0FDaEJBLElBQUl2QyxPQUFNLEdBQUd3QyxtQkFBTyxDQUFDLHVEQUFELENBQXBCO0FBRUE7OztBQUNBLElBQUkwRCxXQUFXLEdBQUdDLE1BQU0sQ0FBQzlGLFNBQXpCO0FBRUE7O0FBQ0EsSUFBSStGLGNBQWMsR0FBR0YsV0FBVyxDQUFDRSxjQUFqQztBQUVBOzs7Ozs7QUFLQSxJQUFJbUwsb0JBQW9CLEdBQUdyTCxXQUFXLENBQUMxRixRQUF2QztBQUVBOztBQUNBLElBQUkrSCxjQUFjLEdBQUd2SSxPQUFNLEdBQUdBLE9BQU0sQ0FBQ3dJLFdBQVYsR0FBd0JULFNBQW5EO0FBRUE7Ozs7Ozs7O0FBT0EsU0FBU0ksU0FBVCxDQUFtQnhDLEtBQW5CLEVBQTBCO0FBQ3hCLE1BQUk2TCxLQUFLLEdBQUdwTCxjQUFjLENBQUNVLElBQWYsQ0FBb0JuQixLQUFwQixFQUEyQjRDLGNBQTNCLENBQVo7QUFBQSxNQUNJNEgsR0FBRyxHQUFHeEssS0FBSyxDQUFDNEMsY0FBRCxDQURmOztBQUdBLE1BQUk7QUFDRjVDLFNBQUssQ0FBQzRDLGNBQUQsQ0FBTCxHQUF3QlIsU0FBeEI7QUFDQSxRQUFJMEosUUFBUSxHQUFHLElBQWY7QUFDRCxHQUhELENBR0UsT0FBT0MsQ0FBUCxFQUFVLENBQUU7O0FBRWQsTUFBSWhNLE1BQU0sR0FBRzZMLG9CQUFvQixDQUFDekssSUFBckIsQ0FBMEJuQixLQUExQixDQUFiOztBQUNBLE1BQUk4TCxRQUFKLEVBQWM7QUFDWixRQUFJRCxLQUFKLEVBQVc7QUFDVDdMLFdBQUssQ0FBQzRDLGNBQUQsQ0FBTCxHQUF3QjRILEdBQXhCO0FBQ0QsS0FGRCxNQUVPO0FBQ0wsYUFBT3hLLEtBQUssQ0FBQzRDLGNBQUQsQ0FBWjtBQUNEO0FBQ0Y7O0FBQ0QsU0FBTzdDLE1BQVA7QUFDRDs7QUFFRHhGLE1BQU0sQ0FBQ0MsT0FBUCxHQUFpQmdJLFNBQWpCLEM7Ozs7Ozs7Ozs7O0FDN0NBLElBQUk3QyxXQUFXLEdBQUc5QyxtQkFBTyxDQUFDLGlFQUFELENBQXpCO0FBQUEsSUFDSW1QLFNBQVMsR0FBR25QLG1CQUFPLENBQUMsMkRBQUQsQ0FEdkI7QUFHQTs7O0FBQ0EsSUFBSTBELFdBQVcsR0FBR0MsTUFBTSxDQUFDOUYsU0FBekI7QUFFQTs7QUFDQSxJQUFJdVIsb0JBQW9CLEdBQUcxTCxXQUFXLENBQUMwTCxvQkFBdkM7QUFFQTs7QUFDQSxJQUFJQyxnQkFBZ0IsR0FBRzFMLE1BQU0sQ0FBQzJMLHFCQUE5QjtBQUVBOzs7Ozs7OztBQU9BLElBQUlYLFVBQVUsR0FBRyxDQUFDVSxnQkFBRCxHQUFvQkYsU0FBcEIsR0FBZ0MsVUFBUzlKLE1BQVQsRUFBaUI7QUFDaEUsTUFBSUEsTUFBTSxJQUFJLElBQWQsRUFBb0I7QUFDbEIsV0FBTyxFQUFQO0FBQ0Q7O0FBQ0RBLFFBQU0sR0FBRzFCLE1BQU0sQ0FBQzBCLE1BQUQsQ0FBZjtBQUNBLFNBQU92QyxXQUFXLENBQUN1TSxnQkFBZ0IsQ0FBQ2hLLE1BQUQsQ0FBakIsRUFBMkIsVUFBU2tLLE1BQVQsRUFBaUI7QUFDNUQsV0FBT0gsb0JBQW9CLENBQUM5SyxJQUFyQixDQUEwQmUsTUFBMUIsRUFBa0NrSyxNQUFsQyxDQUFQO0FBQ0QsR0FGaUIsQ0FBbEI7QUFHRCxDQVJEO0FBVUE3UixNQUFNLENBQUNDLE9BQVAsR0FBaUJnUixVQUFqQixDOzs7Ozs7Ozs7OztBQzdCQSxJQUFJek8sUUFBUSxHQUFHRixtQkFBTyxDQUFDLDJEQUFELENBQXRCO0FBQUEsSUFDSXNCLEdBQUcsR0FBR3RCLG1CQUFPLENBQUMsaURBQUQsQ0FEakI7QUFBQSxJQUVJNkIsT0FBTyxHQUFHN0IsbUJBQU8sQ0FBQyx5REFBRCxDQUZyQjtBQUFBLElBR0k4QixHQUFHLEdBQUc5QixtQkFBTyxDQUFDLGlEQUFELENBSGpCO0FBQUEsSUFJSTZDLE9BQU8sR0FBRzdDLG1CQUFPLENBQUMseURBQUQsQ0FKckI7QUFBQSxJQUtJaUcsVUFBVSxHQUFHakcsbUJBQU8sQ0FBQywrREFBRCxDQUx4QjtBQUFBLElBTUl3SSxRQUFRLEdBQUd4SSxtQkFBTyxDQUFDLDJEQUFELENBTnRCO0FBUUE7OztBQUNBLElBQUl5SixNQUFNLEdBQUcsY0FBYjtBQUFBLElBQ0l2QyxTQUFTLEdBQUcsaUJBRGhCO0FBQUEsSUFFSXNJLFVBQVUsR0FBRyxrQkFGakI7QUFBQSxJQUdJNUYsTUFBTSxHQUFHLGNBSGI7QUFBQSxJQUlJRSxVQUFVLEdBQUcsa0JBSmpCO0FBTUEsSUFBSUUsV0FBVyxHQUFHLG1CQUFsQjtBQUVBOztBQUNBLElBQUl5RixrQkFBa0IsR0FBR2pILFFBQVEsQ0FBQ3RJLFFBQUQsQ0FBakM7QUFBQSxJQUNJd1AsYUFBYSxHQUFHbEgsUUFBUSxDQUFDbEgsR0FBRCxDQUQ1QjtBQUFBLElBRUlxTyxpQkFBaUIsR0FBR25ILFFBQVEsQ0FBQzNHLE9BQUQsQ0FGaEM7QUFBQSxJQUdJK04sYUFBYSxHQUFHcEgsUUFBUSxDQUFDMUcsR0FBRCxDQUg1QjtBQUFBLElBSUkrTixpQkFBaUIsR0FBR3JILFFBQVEsQ0FBQzNGLE9BQUQsQ0FKaEM7QUFNQTs7Ozs7Ozs7QUFPQSxJQUFJa0UsTUFBTSxHQUFHZCxVQUFiLEMsQ0FFQTs7QUFDQSxJQUFLL0YsUUFBUSxJQUFJNkcsTUFBTSxDQUFDLElBQUk3RyxRQUFKLENBQWEsSUFBSTRQLFdBQUosQ0FBZ0IsQ0FBaEIsQ0FBYixDQUFELENBQU4sSUFBNEM5RixXQUF6RCxJQUNDMUksR0FBRyxJQUFJeUYsTUFBTSxDQUFDLElBQUl6RixHQUFKLEVBQUQsQ0FBTixJQUFtQm1JLE1BRDNCLElBRUM1SCxPQUFPLElBQUlrRixNQUFNLENBQUNsRixPQUFPLENBQUNrTyxPQUFSLEVBQUQsQ0FBTixJQUE2QlAsVUFGekMsSUFHQzFOLEdBQUcsSUFBSWlGLE1BQU0sQ0FBQyxJQUFJakYsR0FBSixFQUFELENBQU4sSUFBbUI4SCxNQUgzQixJQUlDL0csT0FBTyxJQUFJa0UsTUFBTSxDQUFDLElBQUlsRSxPQUFKLEVBQUQsQ0FBTixJQUF1QmlILFVBSnZDLEVBSW9EO0FBQ2xEL0MsUUFBTSxHQUFHLGdCQUFTNUQsS0FBVCxFQUFnQjtBQUN2QixRQUFJRCxNQUFNLEdBQUcrQyxVQUFVLENBQUM5QyxLQUFELENBQXZCO0FBQUEsUUFDSTZNLElBQUksR0FBRzlNLE1BQU0sSUFBSWdFLFNBQVYsR0FBc0IvRCxLQUFLLENBQUN2RixXQUE1QixHQUEwQzJILFNBRHJEO0FBQUEsUUFFSTBLLFVBQVUsR0FBR0QsSUFBSSxHQUFHeEgsUUFBUSxDQUFDd0gsSUFBRCxDQUFYLEdBQW9CLEVBRnpDOztBQUlBLFFBQUlDLFVBQUosRUFBZ0I7QUFDZCxjQUFRQSxVQUFSO0FBQ0UsYUFBS1Isa0JBQUw7QUFBeUIsaUJBQU96RixXQUFQOztBQUN6QixhQUFLMEYsYUFBTDtBQUFvQixpQkFBT2pHLE1BQVA7O0FBQ3BCLGFBQUtrRyxpQkFBTDtBQUF3QixpQkFBT0gsVUFBUDs7QUFDeEIsYUFBS0ksYUFBTDtBQUFvQixpQkFBT2hHLE1BQVA7O0FBQ3BCLGFBQUtpRyxpQkFBTDtBQUF3QixpQkFBTy9GLFVBQVA7QUFMMUI7QUFPRDs7QUFDRCxXQUFPNUcsTUFBUDtBQUNELEdBZkQ7QUFnQkQ7O0FBRUR4RixNQUFNLENBQUNDLE9BQVAsR0FBaUJvSixNQUFqQixDOzs7Ozs7Ozs7OztBQ3pEQTs7Ozs7Ozs7QUFRQSxTQUFTK0gsUUFBVCxDQUFrQnpKLE1BQWxCLEVBQTBCaEIsR0FBMUIsRUFBK0I7QUFDN0IsU0FBT2dCLE1BQU0sSUFBSSxJQUFWLEdBQWlCRSxTQUFqQixHQUE2QkYsTUFBTSxDQUFDaEIsR0FBRCxDQUExQztBQUNEOztBQUVEM0csTUFBTSxDQUFDQyxPQUFQLEdBQWlCbVIsUUFBakIsQzs7Ozs7Ozs7Ozs7QUNaQSxJQUFJNUosUUFBUSxHQUFHbEYsbUJBQU8sQ0FBQywyREFBRCxDQUF0QjtBQUFBLElBQ0lxRCxXQUFXLEdBQUdyRCxtQkFBTyxDQUFDLCtEQUFELENBRHpCO0FBQUEsSUFFSXNELE9BQU8sR0FBR3RELG1CQUFPLENBQUMsdURBQUQsQ0FGckI7QUFBQSxJQUdJd0QsT0FBTyxHQUFHeEQsbUJBQU8sQ0FBQyx5REFBRCxDQUhyQjtBQUFBLElBSUlvSixRQUFRLEdBQUdwSixtQkFBTyxDQUFDLHlEQUFELENBSnRCO0FBQUEsSUFLSW1GLEtBQUssR0FBR25GLG1CQUFPLENBQUMscURBQUQsQ0FMbkI7QUFPQTs7Ozs7Ozs7Ozs7QUFTQSxTQUFTa1EsT0FBVCxDQUFpQjdLLE1BQWpCLEVBQXlCQyxJQUF6QixFQUErQjZLLE9BQS9CLEVBQXdDO0FBQ3RDN0ssTUFBSSxHQUFHSixRQUFRLENBQUNJLElBQUQsRUFBT0QsTUFBUCxDQUFmO0FBRUEsTUFBSTNFLEtBQUssR0FBRyxDQUFDLENBQWI7QUFBQSxNQUNJOUIsTUFBTSxHQUFHMEcsSUFBSSxDQUFDMUcsTUFEbEI7QUFBQSxNQUVJc0UsTUFBTSxHQUFHLEtBRmI7O0FBSUEsU0FBTyxFQUFFeEMsS0FBRixHQUFVOUIsTUFBakIsRUFBeUI7QUFDdkIsUUFBSXlGLEdBQUcsR0FBR2MsS0FBSyxDQUFDRyxJQUFJLENBQUM1RSxLQUFELENBQUwsQ0FBZjs7QUFDQSxRQUFJLEVBQUV3QyxNQUFNLEdBQUdtQyxNQUFNLElBQUksSUFBVixJQUFrQjhLLE9BQU8sQ0FBQzlLLE1BQUQsRUFBU2hCLEdBQVQsQ0FBcEMsQ0FBSixFQUF3RDtBQUN0RDtBQUNEOztBQUNEZ0IsVUFBTSxHQUFHQSxNQUFNLENBQUNoQixHQUFELENBQWY7QUFDRDs7QUFDRCxNQUFJbkIsTUFBTSxJQUFJLEVBQUV4QyxLQUFGLElBQVc5QixNQUF6QixFQUFpQztBQUMvQixXQUFPc0UsTUFBUDtBQUNEOztBQUNEdEUsUUFBTSxHQUFHeUcsTUFBTSxJQUFJLElBQVYsR0FBaUIsQ0FBakIsR0FBcUJBLE1BQU0sQ0FBQ3pHLE1BQXJDO0FBQ0EsU0FBTyxDQUFDLENBQUNBLE1BQUYsSUFBWXdLLFFBQVEsQ0FBQ3hLLE1BQUQsQ0FBcEIsSUFBZ0M0RSxPQUFPLENBQUNhLEdBQUQsRUFBTXpGLE1BQU4sQ0FBdkMsS0FDSjBFLE9BQU8sQ0FBQytCLE1BQUQsQ0FBUCxJQUFtQmhDLFdBQVcsQ0FBQ2dDLE1BQUQsQ0FEMUIsQ0FBUDtBQUVEOztBQUVEM0gsTUFBTSxDQUFDQyxPQUFQLEdBQWlCdVMsT0FBakIsQzs7Ozs7Ozs7Ozs7QUN0Q0EsSUFBSUUsWUFBWSxHQUFHcFEsbUJBQU8sQ0FBQyxtRUFBRCxDQUExQjtBQUVBOzs7Ozs7Ozs7QUFPQSxTQUFTRyxTQUFULEdBQXFCO0FBQ25CLE9BQUtnQyxRQUFMLEdBQWdCaU8sWUFBWSxHQUFHQSxZQUFZLENBQUMsSUFBRCxDQUFmLEdBQXdCLEVBQXBEO0FBQ0EsT0FBS3pOLElBQUwsR0FBWSxDQUFaO0FBQ0Q7O0FBRURqRixNQUFNLENBQUNDLE9BQVAsR0FBaUJ3QyxTQUFqQixDOzs7Ozs7Ozs7OztBQ2RBOzs7Ozs7Ozs7O0FBVUEsU0FBU0MsVUFBVCxDQUFvQmlFLEdBQXBCLEVBQXlCO0FBQ3ZCLE1BQUluQixNQUFNLEdBQUcsS0FBS25DLEdBQUwsQ0FBU3NELEdBQVQsS0FBaUIsT0FBTyxLQUFLbEMsUUFBTCxDQUFja0MsR0FBZCxDQUFyQztBQUNBLE9BQUsxQixJQUFMLElBQWFPLE1BQU0sR0FBRyxDQUFILEdBQU8sQ0FBMUI7QUFDQSxTQUFPQSxNQUFQO0FBQ0Q7O0FBRUR4RixNQUFNLENBQUNDLE9BQVAsR0FBaUJ5QyxVQUFqQixDOzs7Ozs7Ozs7OztBQ2hCQSxJQUFJZ1EsWUFBWSxHQUFHcFEsbUJBQU8sQ0FBQyxtRUFBRCxDQUExQjtBQUVBOzs7QUFDQSxJQUFJcVEsY0FBYyxHQUFHLDJCQUFyQjtBQUVBOztBQUNBLElBQUkzTSxXQUFXLEdBQUdDLE1BQU0sQ0FBQzlGLFNBQXpCO0FBRUE7O0FBQ0EsSUFBSStGLGNBQWMsR0FBR0YsV0FBVyxDQUFDRSxjQUFqQztBQUVBOzs7Ozs7Ozs7O0FBU0EsU0FBU3ZELE9BQVQsQ0FBaUJnRSxHQUFqQixFQUFzQjtBQUNwQixNQUFJdkUsSUFBSSxHQUFHLEtBQUtxQyxRQUFoQjs7QUFDQSxNQUFJaU8sWUFBSixFQUFrQjtBQUNoQixRQUFJbE4sTUFBTSxHQUFHcEQsSUFBSSxDQUFDdUUsR0FBRCxDQUFqQjtBQUNBLFdBQU9uQixNQUFNLEtBQUttTixjQUFYLEdBQTRCOUssU0FBNUIsR0FBd0NyQyxNQUEvQztBQUNEOztBQUNELFNBQU9VLGNBQWMsQ0FBQ1UsSUFBZixDQUFvQnhFLElBQXBCLEVBQTBCdUUsR0FBMUIsSUFBaUN2RSxJQUFJLENBQUN1RSxHQUFELENBQXJDLEdBQTZDa0IsU0FBcEQ7QUFDRDs7QUFFRDdILE1BQU0sQ0FBQ0MsT0FBUCxHQUFpQjBDLE9BQWpCLEM7Ozs7Ozs7Ozs7O0FDN0JBLElBQUkrUCxZQUFZLEdBQUdwUSxtQkFBTyxDQUFDLG1FQUFELENBQTFCO0FBRUE7OztBQUNBLElBQUkwRCxXQUFXLEdBQUdDLE1BQU0sQ0FBQzlGLFNBQXpCO0FBRUE7O0FBQ0EsSUFBSStGLGNBQWMsR0FBR0YsV0FBVyxDQUFDRSxjQUFqQztBQUVBOzs7Ozs7Ozs7O0FBU0EsU0FBU3RELE9BQVQsQ0FBaUIrRCxHQUFqQixFQUFzQjtBQUNwQixNQUFJdkUsSUFBSSxHQUFHLEtBQUtxQyxRQUFoQjtBQUNBLFNBQU9pTyxZQUFZLEdBQUl0USxJQUFJLENBQUN1RSxHQUFELENBQUosS0FBY2tCLFNBQWxCLEdBQStCM0IsY0FBYyxDQUFDVSxJQUFmLENBQW9CeEUsSUFBcEIsRUFBMEJ1RSxHQUExQixDQUFsRDtBQUNEOztBQUVEM0csTUFBTSxDQUFDQyxPQUFQLEdBQWlCMkMsT0FBakIsQzs7Ozs7Ozs7Ozs7QUN0QkEsSUFBSThQLFlBQVksR0FBR3BRLG1CQUFPLENBQUMsbUVBQUQsQ0FBMUI7QUFFQTs7O0FBQ0EsSUFBSXFRLGNBQWMsR0FBRywyQkFBckI7QUFFQTs7Ozs7Ozs7Ozs7QUFVQSxTQUFTOVAsT0FBVCxDQUFpQjhELEdBQWpCLEVBQXNCbEIsS0FBdEIsRUFBNkI7QUFDM0IsTUFBSXJELElBQUksR0FBRyxLQUFLcUMsUUFBaEI7QUFDQSxPQUFLUSxJQUFMLElBQWEsS0FBSzVCLEdBQUwsQ0FBU3NELEdBQVQsSUFBZ0IsQ0FBaEIsR0FBb0IsQ0FBakM7QUFDQXZFLE1BQUksQ0FBQ3VFLEdBQUQsQ0FBSixHQUFhK0wsWUFBWSxJQUFJak4sS0FBSyxLQUFLb0MsU0FBM0IsR0FBd0M4SyxjQUF4QyxHQUF5RGxOLEtBQXJFO0FBQ0EsU0FBTyxJQUFQO0FBQ0Q7O0FBRUR6RixNQUFNLENBQUNDLE9BQVAsR0FBaUI0QyxPQUFqQixDOzs7Ozs7Ozs7Ozs7O0FDdEJBO0FBQ0EsSUFBSStQLGdCQUFnQixHQUFHLGdCQUF2QjtBQUVBOztBQUNBLElBQUlDLFFBQVEsR0FBRyxrQkFBZjtBQUVBOzs7Ozs7Ozs7QUFRQSxTQUFTL00sT0FBVCxDQUFpQkwsS0FBakIsRUFBd0J2RSxNQUF4QixFQUFnQztBQUM5QixNQUFJNFIsSUFBSSxXQUFVck4sS0FBVixDQUFSOztBQUNBdkUsUUFBTSxHQUFHQSxNQUFNLElBQUksSUFBVixHQUFpQjBSLGdCQUFqQixHQUFvQzFSLE1BQTdDO0FBRUEsU0FBTyxDQUFDLENBQUNBLE1BQUYsS0FDSjRSLElBQUksSUFBSSxRQUFSLElBQ0VBLElBQUksSUFBSSxRQUFSLElBQW9CRCxRQUFRLENBQUNwSCxJQUFULENBQWNoRyxLQUFkLENBRmxCLEtBR0FBLEtBQUssR0FBRyxDQUFDLENBQVQsSUFBY0EsS0FBSyxHQUFHLENBQVIsSUFBYSxDQUEzQixJQUFnQ0EsS0FBSyxHQUFHdkUsTUFIL0M7QUFJRDs7QUFFRGxCLE1BQU0sQ0FBQ0MsT0FBUCxHQUFpQjZGLE9BQWpCLEM7Ozs7Ozs7Ozs7Ozs7QUN4QkEsSUFBSUYsT0FBTyxHQUFHdEQsbUJBQU8sQ0FBQyx1REFBRCxDQUFyQjtBQUFBLElBQ0k0TCxRQUFRLEdBQUc1TCxtQkFBTyxDQUFDLHlEQUFELENBRHRCO0FBR0E7OztBQUNBLElBQUl5USxZQUFZLEdBQUcsa0RBQW5CO0FBQUEsSUFDSUMsYUFBYSxHQUFHLE9BRHBCO0FBR0E7Ozs7Ozs7OztBQVFBLFNBQVNuRixLQUFULENBQWVwSSxLQUFmLEVBQXNCa0MsTUFBdEIsRUFBOEI7QUFDNUIsTUFBSS9CLE9BQU8sQ0FBQ0gsS0FBRCxDQUFYLEVBQW9CO0FBQ2xCLFdBQU8sS0FBUDtBQUNEOztBQUNELE1BQUlxTixJQUFJLFdBQVVyTixLQUFWLENBQVI7O0FBQ0EsTUFBSXFOLElBQUksSUFBSSxRQUFSLElBQW9CQSxJQUFJLElBQUksUUFBNUIsSUFBd0NBLElBQUksSUFBSSxTQUFoRCxJQUNBck4sS0FBSyxJQUFJLElBRFQsSUFDaUJ5SSxRQUFRLENBQUN6SSxLQUFELENBRDdCLEVBQ3NDO0FBQ3BDLFdBQU8sSUFBUDtBQUNEOztBQUNELFNBQU91TixhQUFhLENBQUN2SCxJQUFkLENBQW1CaEcsS0FBbkIsS0FBNkIsQ0FBQ3NOLFlBQVksQ0FBQ3RILElBQWIsQ0FBa0JoRyxLQUFsQixDQUE5QixJQUNKa0MsTUFBTSxJQUFJLElBQVYsSUFBa0JsQyxLQUFLLElBQUlRLE1BQU0sQ0FBQzBCLE1BQUQsQ0FEcEM7QUFFRDs7QUFFRDNILE1BQU0sQ0FBQ0MsT0FBUCxHQUFpQjROLEtBQWpCLEM7Ozs7Ozs7Ozs7Ozs7QUM1QkE7Ozs7Ozs7QUFPQSxTQUFTcUQsU0FBVCxDQUFtQnpMLEtBQW5CLEVBQTBCO0FBQ3hCLE1BQUlxTixJQUFJLFdBQVVyTixLQUFWLENBQVI7O0FBQ0EsU0FBUXFOLElBQUksSUFBSSxRQUFSLElBQW9CQSxJQUFJLElBQUksUUFBNUIsSUFBd0NBLElBQUksSUFBSSxRQUFoRCxJQUE0REEsSUFBSSxJQUFJLFNBQXJFLEdBQ0ZyTixLQUFLLEtBQUssV0FEUixHQUVGQSxLQUFLLEtBQUssSUFGZjtBQUdEOztBQUVEekYsTUFBTSxDQUFDQyxPQUFQLEdBQWlCaVIsU0FBakIsQzs7Ozs7Ozs7Ozs7QUNkQSxJQUFJdEMsVUFBVSxHQUFHdE0sbUJBQU8sQ0FBQywrREFBRCxDQUF4QjtBQUVBOzs7QUFDQSxJQUFJMlEsVUFBVSxHQUFJLFlBQVc7QUFDM0IsTUFBSUMsR0FBRyxHQUFHLFNBQVNDLElBQVQsQ0FBY3ZFLFVBQVUsSUFBSUEsVUFBVSxDQUFDRSxJQUF6QixJQUFpQ0YsVUFBVSxDQUFDRSxJQUFYLENBQWdCc0UsUUFBakQsSUFBNkQsRUFBM0UsQ0FBVjtBQUNBLFNBQU9GLEdBQUcsR0FBSSxtQkFBbUJBLEdBQXZCLEdBQThCLEVBQXhDO0FBQ0QsQ0FIaUIsRUFBbEI7QUFLQTs7Ozs7Ozs7O0FBT0EsU0FBU3RJLFFBQVQsQ0FBa0I0RCxJQUFsQixFQUF3QjtBQUN0QixTQUFPLENBQUMsQ0FBQ3lFLFVBQUYsSUFBaUJBLFVBQVUsSUFBSXpFLElBQXRDO0FBQ0Q7O0FBRUR4TyxNQUFNLENBQUNDLE9BQVAsR0FBaUIySyxRQUFqQixDOzs7Ozs7Ozs7OztBQ25CQTtBQUNBLElBQUk1RSxXQUFXLEdBQUdDLE1BQU0sQ0FBQzlGLFNBQXpCO0FBRUE7Ozs7Ozs7O0FBT0EsU0FBU29OLFdBQVQsQ0FBcUI5SCxLQUFyQixFQUE0QjtBQUMxQixNQUFJNk0sSUFBSSxHQUFHN00sS0FBSyxJQUFJQSxLQUFLLENBQUN2RixXQUExQjtBQUFBLE1BQ0ltVCxLQUFLLEdBQUksT0FBT2YsSUFBUCxJQUFlLFVBQWYsSUFBNkJBLElBQUksQ0FBQ25TLFNBQW5DLElBQWlENkYsV0FEN0Q7QUFHQSxTQUFPUCxLQUFLLEtBQUs0TixLQUFqQjtBQUNEOztBQUVEclQsTUFBTSxDQUFDQyxPQUFQLEdBQWlCc04sV0FBakIsQzs7Ozs7Ozs7Ozs7QUNqQkEsSUFBSTFDLFFBQVEsR0FBR3ZJLG1CQUFPLENBQUMseURBQUQsQ0FBdEI7QUFFQTs7Ozs7Ozs7OztBQVFBLFNBQVN3TCxrQkFBVCxDQUE0QnJJLEtBQTVCLEVBQW1DO0FBQ2pDLFNBQU9BLEtBQUssS0FBS0EsS0FBVixJQUFtQixDQUFDb0YsUUFBUSxDQUFDcEYsS0FBRCxDQUFuQztBQUNEOztBQUVEekYsTUFBTSxDQUFDQyxPQUFQLEdBQWlCNk4sa0JBQWpCLEM7Ozs7Ozs7Ozs7O0FDZEE7Ozs7Ozs7QUFPQSxTQUFTeEssY0FBVCxHQUEwQjtBQUN4QixPQUFLbUIsUUFBTCxHQUFnQixFQUFoQjtBQUNBLE9BQUtRLElBQUwsR0FBWSxDQUFaO0FBQ0Q7O0FBRURqRixNQUFNLENBQUNDLE9BQVAsR0FBaUJxRCxjQUFqQixDOzs7Ozs7Ozs7OztBQ1pBLElBQUk4RCxZQUFZLEdBQUc5RSxtQkFBTyxDQUFDLG1FQUFELENBQTFCO0FBRUE7OztBQUNBLElBQUlnUixVQUFVLEdBQUd2TSxLQUFLLENBQUM1RyxTQUF2QjtBQUVBOztBQUNBLElBQUlvVCxNQUFNLEdBQUdELFVBQVUsQ0FBQ0MsTUFBeEI7QUFFQTs7Ozs7Ozs7OztBQVNBLFNBQVNoUSxlQUFULENBQXlCb0QsR0FBekIsRUFBOEI7QUFDNUIsTUFBSXZFLElBQUksR0FBRyxLQUFLcUMsUUFBaEI7QUFBQSxNQUNJekIsS0FBSyxHQUFHb0UsWUFBWSxDQUFDaEYsSUFBRCxFQUFPdUUsR0FBUCxDQUR4Qjs7QUFHQSxNQUFJM0QsS0FBSyxHQUFHLENBQVosRUFBZTtBQUNiLFdBQU8sS0FBUDtBQUNEOztBQUNELE1BQUl3USxTQUFTLEdBQUdwUixJQUFJLENBQUNsQixNQUFMLEdBQWMsQ0FBOUI7O0FBQ0EsTUFBSThCLEtBQUssSUFBSXdRLFNBQWIsRUFBd0I7QUFDdEJwUixRQUFJLENBQUNxUixHQUFMO0FBQ0QsR0FGRCxNQUVPO0FBQ0xGLFVBQU0sQ0FBQzNNLElBQVAsQ0FBWXhFLElBQVosRUFBa0JZLEtBQWxCLEVBQXlCLENBQXpCO0FBQ0Q7O0FBQ0QsSUFBRSxLQUFLaUMsSUFBUDtBQUNBLFNBQU8sSUFBUDtBQUNEOztBQUVEakYsTUFBTSxDQUFDQyxPQUFQLEdBQWlCc0QsZUFBakIsQzs7Ozs7Ozs7Ozs7QUNsQ0EsSUFBSTZELFlBQVksR0FBRzlFLG1CQUFPLENBQUMsbUVBQUQsQ0FBMUI7QUFFQTs7Ozs7Ozs7Ozs7QUFTQSxTQUFTa0IsWUFBVCxDQUFzQm1ELEdBQXRCLEVBQTJCO0FBQ3pCLE1BQUl2RSxJQUFJLEdBQUcsS0FBS3FDLFFBQWhCO0FBQUEsTUFDSXpCLEtBQUssR0FBR29FLFlBQVksQ0FBQ2hGLElBQUQsRUFBT3VFLEdBQVAsQ0FEeEI7QUFHQSxTQUFPM0QsS0FBSyxHQUFHLENBQVIsR0FBWTZFLFNBQVosR0FBd0J6RixJQUFJLENBQUNZLEtBQUQsQ0FBSixDQUFZLENBQVosQ0FBL0I7QUFDRDs7QUFFRGhELE1BQU0sQ0FBQ0MsT0FBUCxHQUFpQnVELFlBQWpCLEM7Ozs7Ozs7Ozs7O0FDbEJBLElBQUk0RCxZQUFZLEdBQUc5RSxtQkFBTyxDQUFDLG1FQUFELENBQTFCO0FBRUE7Ozs7Ozs7Ozs7O0FBU0EsU0FBU21CLFlBQVQsQ0FBc0JrRCxHQUF0QixFQUEyQjtBQUN6QixTQUFPUyxZQUFZLENBQUMsS0FBSzNDLFFBQU4sRUFBZ0JrQyxHQUFoQixDQUFaLEdBQW1DLENBQUMsQ0FBM0M7QUFDRDs7QUFFRDNHLE1BQU0sQ0FBQ0MsT0FBUCxHQUFpQndELFlBQWpCLEM7Ozs7Ozs7Ozs7O0FDZkEsSUFBSTJELFlBQVksR0FBRzlFLG1CQUFPLENBQUMsbUVBQUQsQ0FBMUI7QUFFQTs7Ozs7Ozs7Ozs7O0FBVUEsU0FBU29CLFlBQVQsQ0FBc0JpRCxHQUF0QixFQUEyQmxCLEtBQTNCLEVBQWtDO0FBQ2hDLE1BQUlyRCxJQUFJLEdBQUcsS0FBS3FDLFFBQWhCO0FBQUEsTUFDSXpCLEtBQUssR0FBR29FLFlBQVksQ0FBQ2hGLElBQUQsRUFBT3VFLEdBQVAsQ0FEeEI7O0FBR0EsTUFBSTNELEtBQUssR0FBRyxDQUFaLEVBQWU7QUFDYixNQUFFLEtBQUtpQyxJQUFQO0FBQ0E3QyxRQUFJLENBQUNmLElBQUwsQ0FBVSxDQUFDc0YsR0FBRCxFQUFNbEIsS0FBTixDQUFWO0FBQ0QsR0FIRCxNQUdPO0FBQ0xyRCxRQUFJLENBQUNZLEtBQUQsQ0FBSixDQUFZLENBQVosSUFBaUJ5QyxLQUFqQjtBQUNEOztBQUNELFNBQU8sSUFBUDtBQUNEOztBQUVEekYsTUFBTSxDQUFDQyxPQUFQLEdBQWlCeUQsWUFBakIsQzs7Ozs7Ozs7Ozs7QUN6QkEsSUFBSVosSUFBSSxHQUFHUixtQkFBTyxDQUFDLG1EQUFELENBQWxCO0FBQUEsSUFDSXFCLFNBQVMsR0FBR3JCLG1CQUFPLENBQUMsNkRBQUQsQ0FEdkI7QUFBQSxJQUVJc0IsR0FBRyxHQUFHdEIsbUJBQU8sQ0FBQyxpREFBRCxDQUZqQjtBQUlBOzs7Ozs7Ozs7QUFPQSxTQUFTdUIsYUFBVCxHQUF5QjtBQUN2QixPQUFLb0IsSUFBTCxHQUFZLENBQVo7QUFDQSxPQUFLUixRQUFMLEdBQWdCO0FBQ2QsWUFBUSxJQUFJM0IsSUFBSixFQURNO0FBRWQsV0FBTyxLQUFLYyxHQUFHLElBQUlELFNBQVosR0FGTztBQUdkLGNBQVUsSUFBSWIsSUFBSjtBQUhJLEdBQWhCO0FBS0Q7O0FBRUQ5QyxNQUFNLENBQUNDLE9BQVAsR0FBaUI0RCxhQUFqQixDOzs7Ozs7Ozs7OztBQ3BCQSxJQUFJc04sVUFBVSxHQUFHN08sbUJBQU8sQ0FBQywrREFBRCxDQUF4QjtBQUVBOzs7Ozs7Ozs7OztBQVNBLFNBQVN3QixjQUFULENBQXdCNkMsR0FBeEIsRUFBNkI7QUFDM0IsTUFBSW5CLE1BQU0sR0FBRzJMLFVBQVUsQ0FBQyxJQUFELEVBQU94SyxHQUFQLENBQVYsQ0FBc0IsUUFBdEIsRUFBZ0NBLEdBQWhDLENBQWI7QUFDQSxPQUFLMUIsSUFBTCxJQUFhTyxNQUFNLEdBQUcsQ0FBSCxHQUFPLENBQTFCO0FBQ0EsU0FBT0EsTUFBUDtBQUNEOztBQUVEeEYsTUFBTSxDQUFDQyxPQUFQLEdBQWlCNkQsY0FBakIsQzs7Ozs7Ozs7Ozs7QUNqQkEsSUFBSXFOLFVBQVUsR0FBRzdPLG1CQUFPLENBQUMsK0RBQUQsQ0FBeEI7QUFFQTs7Ozs7Ozs7Ozs7QUFTQSxTQUFTeUIsV0FBVCxDQUFxQjRDLEdBQXJCLEVBQTBCO0FBQ3hCLFNBQU93SyxVQUFVLENBQUMsSUFBRCxFQUFPeEssR0FBUCxDQUFWLENBQXNCdkQsR0FBdEIsQ0FBMEJ1RCxHQUExQixDQUFQO0FBQ0Q7O0FBRUQzRyxNQUFNLENBQUNDLE9BQVAsR0FBaUI4RCxXQUFqQixDOzs7Ozs7Ozs7OztBQ2ZBLElBQUlvTixVQUFVLEdBQUc3TyxtQkFBTyxDQUFDLCtEQUFELENBQXhCO0FBRUE7Ozs7Ozs7Ozs7O0FBU0EsU0FBUzBCLFdBQVQsQ0FBcUIyQyxHQUFyQixFQUEwQjtBQUN4QixTQUFPd0ssVUFBVSxDQUFDLElBQUQsRUFBT3hLLEdBQVAsQ0FBVixDQUFzQnRELEdBQXRCLENBQTBCc0QsR0FBMUIsQ0FBUDtBQUNEOztBQUVEM0csTUFBTSxDQUFDQyxPQUFQLEdBQWlCK0QsV0FBakIsQzs7Ozs7Ozs7Ozs7QUNmQSxJQUFJbU4sVUFBVSxHQUFHN08sbUJBQU8sQ0FBQywrREFBRCxDQUF4QjtBQUVBOzs7Ozs7Ozs7Ozs7QUFVQSxTQUFTMkIsV0FBVCxDQUFxQjBDLEdBQXJCLEVBQTBCbEIsS0FBMUIsRUFBaUM7QUFDL0IsTUFBSXJELElBQUksR0FBRytPLFVBQVUsQ0FBQyxJQUFELEVBQU94SyxHQUFQLENBQXJCO0FBQUEsTUFDSTFCLElBQUksR0FBRzdDLElBQUksQ0FBQzZDLElBRGhCO0FBR0E3QyxNQUFJLENBQUNlLEdBQUwsQ0FBU3dELEdBQVQsRUFBY2xCLEtBQWQ7QUFDQSxPQUFLUixJQUFMLElBQWE3QyxJQUFJLENBQUM2QyxJQUFMLElBQWFBLElBQWIsR0FBb0IsQ0FBcEIsR0FBd0IsQ0FBckM7QUFDQSxTQUFPLElBQVA7QUFDRDs7QUFFRGpGLE1BQU0sQ0FBQ0MsT0FBUCxHQUFpQmdFLFdBQWpCLEM7Ozs7Ozs7Ozs7O0FDckJBOzs7Ozs7O0FBT0EsU0FBUzJMLFVBQVQsQ0FBb0JyUCxHQUFwQixFQUF5QjtBQUN2QixNQUFJeUMsS0FBSyxHQUFHLENBQUMsQ0FBYjtBQUFBLE1BQ0l3QyxNQUFNLEdBQUd1QixLQUFLLENBQUN4RyxHQUFHLENBQUMwRSxJQUFMLENBRGxCO0FBR0ExRSxLQUFHLENBQUNtVCxPQUFKLENBQVksVUFBU2pPLEtBQVQsRUFBZ0JrQixHQUFoQixFQUFxQjtBQUMvQm5CLFVBQU0sQ0FBQyxFQUFFeEMsS0FBSCxDQUFOLEdBQWtCLENBQUMyRCxHQUFELEVBQU1sQixLQUFOLENBQWxCO0FBQ0QsR0FGRDtBQUdBLFNBQU9ELE1BQVA7QUFDRDs7QUFFRHhGLE1BQU0sQ0FBQ0MsT0FBUCxHQUFpQjJQLFVBQWpCLEM7Ozs7Ozs7Ozs7O0FDakJBOzs7Ozs7Ozs7QUFTQSxTQUFTakMsdUJBQVQsQ0FBaUNoSCxHQUFqQyxFQUFzQytELFFBQXRDLEVBQWdEO0FBQzlDLFNBQU8sVUFBUy9DLE1BQVQsRUFBaUI7QUFDdEIsUUFBSUEsTUFBTSxJQUFJLElBQWQsRUFBb0I7QUFDbEIsYUFBTyxLQUFQO0FBQ0Q7O0FBQ0QsV0FBT0EsTUFBTSxDQUFDaEIsR0FBRCxDQUFOLEtBQWdCK0QsUUFBaEIsS0FDSkEsUUFBUSxLQUFLN0MsU0FBYixJQUEyQmxCLEdBQUcsSUFBSVYsTUFBTSxDQUFDMEIsTUFBRCxDQURwQyxDQUFQO0FBRUQsR0FORDtBQU9EOztBQUVEM0gsTUFBTSxDQUFDQyxPQUFQLEdBQWlCME4sdUJBQWpCLEM7Ozs7Ozs7Ozs7O0FDbkJBLElBQUlnRyxPQUFPLEdBQUdyUixtQkFBTyxDQUFDLHVEQUFELENBQXJCO0FBRUE7OztBQUNBLElBQUlzUixnQkFBZ0IsR0FBRyxHQUF2QjtBQUVBOzs7Ozs7Ozs7QUFRQSxTQUFTQyxhQUFULENBQXVCckYsSUFBdkIsRUFBNkI7QUFDM0IsTUFBSWhKLE1BQU0sR0FBR21PLE9BQU8sQ0FBQ25GLElBQUQsRUFBTyxVQUFTN0gsR0FBVCxFQUFjO0FBQ3ZDLFFBQUkrSCxLQUFLLENBQUN6SixJQUFOLEtBQWUyTyxnQkFBbkIsRUFBcUM7QUFDbkNsRixXQUFLLENBQUN6TCxLQUFOO0FBQ0Q7O0FBQ0QsV0FBTzBELEdBQVA7QUFDRCxHQUxtQixDQUFwQjtBQU9BLE1BQUkrSCxLQUFLLEdBQUdsSixNQUFNLENBQUNrSixLQUFuQjtBQUNBLFNBQU9sSixNQUFQO0FBQ0Q7O0FBRUR4RixNQUFNLENBQUNDLE9BQVAsR0FBaUI0VCxhQUFqQixDOzs7Ozs7Ozs7OztBQ3pCQSxJQUFJeFIsU0FBUyxHQUFHQyxtQkFBTyxDQUFDLDZEQUFELENBQXZCO0FBRUE7OztBQUNBLElBQUlvUSxZQUFZLEdBQUdyUSxTQUFTLENBQUM0RCxNQUFELEVBQVMsUUFBVCxDQUE1QjtBQUVBakcsTUFBTSxDQUFDQyxPQUFQLEdBQWlCeVMsWUFBakIsQzs7Ozs7Ozs7Ozs7QUNMQSxJQUFJb0IsT0FBTyxHQUFHeFIsbUJBQU8sQ0FBQyx5REFBRCxDQUFyQjtBQUVBOzs7QUFDQSxJQUFJa0wsVUFBVSxHQUFHc0csT0FBTyxDQUFDN04sTUFBTSxDQUFDNkksSUFBUixFQUFjN0ksTUFBZCxDQUF4QjtBQUVBakcsTUFBTSxDQUFDQyxPQUFQLEdBQWlCdU4sVUFBakIsQzs7Ozs7Ozs7Ozs7OztBQ0xBLElBQUl1RCxVQUFVLEdBQUd6TyxtQkFBTyxDQUFDLCtEQUFELENBQXhCO0FBRUE7OztBQUNBLElBQUl5UixXQUFXLEdBQUcsOEJBQU85VCxPQUFQLE1BQWtCLFFBQWxCLElBQThCQSxPQUE5QixJQUF5QyxDQUFDQSxPQUFPLENBQUMrVCxRQUFsRCxJQUE4RC9ULE9BQWhGO0FBRUE7O0FBQ0EsSUFBSWdVLFVBQVUsR0FBR0YsV0FBVyxJQUFJLDhCQUFPL1QsTUFBUCxNQUFpQixRQUFoQyxJQUE0Q0EsTUFBNUMsSUFBc0QsQ0FBQ0EsTUFBTSxDQUFDZ1UsUUFBOUQsSUFBMEVoVSxNQUEzRjtBQUVBOztBQUNBLElBQUlrVSxhQUFhLEdBQUdELFVBQVUsSUFBSUEsVUFBVSxDQUFDaFUsT0FBWCxLQUF1QjhULFdBQXpEO0FBRUE7O0FBQ0EsSUFBSUksV0FBVyxHQUFHRCxhQUFhLElBQUluRCxVQUFVLENBQUNxRCxPQUE5QztBQUVBOztBQUNBLElBQUlDLFFBQVEsR0FBSSxZQUFXO0FBQ3pCLE1BQUk7QUFDRjtBQUNBLFFBQUlDLEtBQUssR0FBR0wsVUFBVSxJQUFJQSxVQUFVLENBQUMzUixPQUF6QixJQUFvQzJSLFVBQVUsQ0FBQzNSLE9BQVgsQ0FBbUIsTUFBbkIsRUFBMkJnUyxLQUEzRTs7QUFFQSxRQUFJQSxLQUFKLEVBQVc7QUFDVCxhQUFPQSxLQUFQO0FBQ0QsS0FOQyxDQVFGOzs7QUFDQSxXQUFPSCxXQUFXLElBQUlBLFdBQVcsQ0FBQ0ksT0FBM0IsSUFBc0NKLFdBQVcsQ0FBQ0ksT0FBWixDQUFvQixNQUFwQixDQUE3QztBQUNELEdBVkQsQ0FVRSxPQUFPL0MsQ0FBUCxFQUFVLENBQUU7QUFDZixDQVplLEVBQWhCOztBQWNBeFIsTUFBTSxDQUFDQyxPQUFQLEdBQWlCb1UsUUFBakIsQzs7Ozs7Ozs7Ozs7O0FDN0JBO0FBQ0EsSUFBSXJPLFdBQVcsR0FBR0MsTUFBTSxDQUFDOUYsU0FBekI7QUFFQTs7Ozs7O0FBS0EsSUFBSWtSLG9CQUFvQixHQUFHckwsV0FBVyxDQUFDMUYsUUFBdkM7QUFFQTs7Ozs7Ozs7QUFPQSxTQUFTNEgsY0FBVCxDQUF3QnpDLEtBQXhCLEVBQStCO0FBQzdCLFNBQU80TCxvQkFBb0IsQ0FBQ3pLLElBQXJCLENBQTBCbkIsS0FBMUIsQ0FBUDtBQUNEOztBQUVEekYsTUFBTSxDQUFDQyxPQUFQLEdBQWlCaUksY0FBakIsQzs7Ozs7Ozs7Ozs7QUNyQkE7Ozs7Ozs7O0FBUUEsU0FBUzRMLE9BQVQsQ0FBaUJ0RixJQUFqQixFQUF1QmdHLFNBQXZCLEVBQWtDO0FBQ2hDLFNBQU8sVUFBU0MsR0FBVCxFQUFjO0FBQ25CLFdBQU9qRyxJQUFJLENBQUNnRyxTQUFTLENBQUNDLEdBQUQsQ0FBVixDQUFYO0FBQ0QsR0FGRDtBQUdEOztBQUVEelUsTUFBTSxDQUFDQyxPQUFQLEdBQWlCNlQsT0FBakIsQzs7Ozs7Ozs7Ozs7OztBQ2RBLElBQUkvQyxVQUFVLEdBQUd6TyxtQkFBTyxDQUFDLCtEQUFELENBQXhCO0FBRUE7OztBQUNBLElBQUlvUyxRQUFRLEdBQUcsUUFBT0MsSUFBUCx5Q0FBT0EsSUFBUCxNQUFlLFFBQWYsSUFBMkJBLElBQTNCLElBQW1DQSxJQUFJLENBQUMxTyxNQUFMLEtBQWdCQSxNQUFuRCxJQUE2RDBPLElBQTVFO0FBRUE7O0FBQ0EsSUFBSXBTLElBQUksR0FBR3dPLFVBQVUsSUFBSTJELFFBQWQsSUFBMEJ4SixRQUFRLENBQUMsYUFBRCxDQUFSLEVBQXJDO0FBRUFsTCxNQUFNLENBQUNDLE9BQVAsR0FBaUJzQyxJQUFqQixDOzs7Ozs7Ozs7OztBQ1JBO0FBQ0EsSUFBSW9RLGNBQWMsR0FBRywyQkFBckI7QUFFQTs7Ozs7Ozs7Ozs7QUFVQSxTQUFTdE8sV0FBVCxDQUFxQm9CLEtBQXJCLEVBQTRCO0FBQzFCLE9BQUtoQixRQUFMLENBQWN0QixHQUFkLENBQWtCc0MsS0FBbEIsRUFBeUJrTixjQUF6Qjs7QUFDQSxTQUFPLElBQVA7QUFDRDs7QUFFRDNTLE1BQU0sQ0FBQ0MsT0FBUCxHQUFpQm9FLFdBQWpCLEM7Ozs7Ozs7Ozs7O0FDbEJBOzs7Ozs7Ozs7QUFTQSxTQUFTQyxXQUFULENBQXFCbUIsS0FBckIsRUFBNEI7QUFDMUIsU0FBTyxLQUFLaEIsUUFBTCxDQUFjcEIsR0FBZCxDQUFrQm9DLEtBQWxCLENBQVA7QUFDRDs7QUFFRHpGLE1BQU0sQ0FBQ0MsT0FBUCxHQUFpQnFFLFdBQWpCLEM7Ozs7Ozs7Ozs7O0FDYkE7Ozs7Ozs7QUFPQSxTQUFTdUwsVUFBVCxDQUFvQjFNLEdBQXBCLEVBQXlCO0FBQ3ZCLE1BQUlILEtBQUssR0FBRyxDQUFDLENBQWI7QUFBQSxNQUNJd0MsTUFBTSxHQUFHdUIsS0FBSyxDQUFDNUQsR0FBRyxDQUFDOEIsSUFBTCxDQURsQjtBQUdBOUIsS0FBRyxDQUFDdVEsT0FBSixDQUFZLFVBQVNqTyxLQUFULEVBQWdCO0FBQzFCRCxVQUFNLENBQUMsRUFBRXhDLEtBQUgsQ0FBTixHQUFrQnlDLEtBQWxCO0FBQ0QsR0FGRDtBQUdBLFNBQU9ELE1BQVA7QUFDRDs7QUFFRHhGLE1BQU0sQ0FBQ0MsT0FBUCxHQUFpQjRQLFVBQWpCLEM7Ozs7Ozs7Ozs7O0FDakJBLElBQUlsTSxTQUFTLEdBQUdyQixtQkFBTyxDQUFDLDZEQUFELENBQXZCO0FBRUE7Ozs7Ozs7OztBQU9BLFNBQVNxQyxVQUFULEdBQXNCO0FBQ3BCLE9BQUtGLFFBQUwsR0FBZ0IsSUFBSWQsU0FBSixFQUFoQjtBQUNBLE9BQUtzQixJQUFMLEdBQVksQ0FBWjtBQUNEOztBQUVEakYsTUFBTSxDQUFDQyxPQUFQLEdBQWlCMEUsVUFBakIsQzs7Ozs7Ozs7Ozs7QUNkQTs7Ozs7Ozs7O0FBU0EsU0FBU0MsV0FBVCxDQUFxQitCLEdBQXJCLEVBQTBCO0FBQ3hCLE1BQUl2RSxJQUFJLEdBQUcsS0FBS3FDLFFBQWhCO0FBQUEsTUFDSWUsTUFBTSxHQUFHcEQsSUFBSSxDQUFDLFFBQUQsQ0FBSixDQUFldUUsR0FBZixDQURiO0FBR0EsT0FBSzFCLElBQUwsR0FBWTdDLElBQUksQ0FBQzZDLElBQWpCO0FBQ0EsU0FBT08sTUFBUDtBQUNEOztBQUVEeEYsTUFBTSxDQUFDQyxPQUFQLEdBQWlCMkUsV0FBakIsQzs7Ozs7Ozs7Ozs7QUNqQkE7Ozs7Ozs7OztBQVNBLFNBQVNDLFFBQVQsQ0FBa0I4QixHQUFsQixFQUF1QjtBQUNyQixTQUFPLEtBQUtsQyxRQUFMLENBQWNyQixHQUFkLENBQWtCdUQsR0FBbEIsQ0FBUDtBQUNEOztBQUVEM0csTUFBTSxDQUFDQyxPQUFQLEdBQWlCNEUsUUFBakIsQzs7Ozs7Ozs7Ozs7QUNiQTs7Ozs7Ozs7O0FBU0EsU0FBU0MsUUFBVCxDQUFrQjZCLEdBQWxCLEVBQXVCO0FBQ3JCLFNBQU8sS0FBS2xDLFFBQUwsQ0FBY3BCLEdBQWQsQ0FBa0JzRCxHQUFsQixDQUFQO0FBQ0Q7O0FBRUQzRyxNQUFNLENBQUNDLE9BQVAsR0FBaUI2RSxRQUFqQixDOzs7Ozs7Ozs7OztBQ2JBLElBQUluQixTQUFTLEdBQUdyQixtQkFBTyxDQUFDLDZEQUFELENBQXZCO0FBQUEsSUFDSXNCLEdBQUcsR0FBR3RCLG1CQUFPLENBQUMsaURBQUQsQ0FEakI7QUFBQSxJQUVJNEIsUUFBUSxHQUFHNUIsbUJBQU8sQ0FBQywyREFBRCxDQUZ0QjtBQUlBOzs7QUFDQSxJQUFJc1MsZ0JBQWdCLEdBQUcsR0FBdkI7QUFFQTs7Ozs7Ozs7Ozs7QUFVQSxTQUFTN1AsUUFBVCxDQUFrQjRCLEdBQWxCLEVBQXVCbEIsS0FBdkIsRUFBOEI7QUFDNUIsTUFBSXJELElBQUksR0FBRyxLQUFLcUMsUUFBaEI7O0FBQ0EsTUFBSXJDLElBQUksWUFBWXVCLFNBQXBCLEVBQStCO0FBQzdCLFFBQUlrUixLQUFLLEdBQUd6UyxJQUFJLENBQUNxQyxRQUFqQjs7QUFDQSxRQUFJLENBQUNiLEdBQUQsSUFBU2lSLEtBQUssQ0FBQzNULE1BQU4sR0FBZTBULGdCQUFnQixHQUFHLENBQS9DLEVBQW1EO0FBQ2pEQyxXQUFLLENBQUN4VCxJQUFOLENBQVcsQ0FBQ3NGLEdBQUQsRUFBTWxCLEtBQU4sQ0FBWDtBQUNBLFdBQUtSLElBQUwsR0FBWSxFQUFFN0MsSUFBSSxDQUFDNkMsSUFBbkI7QUFDQSxhQUFPLElBQVA7QUFDRDs7QUFDRDdDLFFBQUksR0FBRyxLQUFLcUMsUUFBTCxHQUFnQixJQUFJUCxRQUFKLENBQWEyUSxLQUFiLENBQXZCO0FBQ0Q7O0FBQ0R6UyxNQUFJLENBQUNlLEdBQUwsQ0FBU3dELEdBQVQsRUFBY2xCLEtBQWQ7QUFDQSxPQUFLUixJQUFMLEdBQVk3QyxJQUFJLENBQUM2QyxJQUFqQjtBQUNBLFNBQU8sSUFBUDtBQUNEOztBQUVEakYsTUFBTSxDQUFDQyxPQUFQLEdBQWlCOEUsUUFBakIsQzs7Ozs7Ozs7Ozs7QUNqQ0EsSUFBSThPLGFBQWEsR0FBR3ZSLG1CQUFPLENBQUMscUVBQUQsQ0FBM0I7QUFFQTs7O0FBQ0EsSUFBSXdTLFVBQVUsR0FBRyxrR0FBakI7QUFFQTs7QUFDQSxJQUFJQyxZQUFZLEdBQUcsVUFBbkI7QUFFQTs7Ozs7Ozs7QUFPQSxJQUFJcEcsWUFBWSxHQUFHa0YsYUFBYSxDQUFDLFVBQVNtQixNQUFULEVBQWlCO0FBQ2hELE1BQUl4UCxNQUFNLEdBQUcsRUFBYjs7QUFDQSxNQUFJd1AsTUFBTSxDQUFDQyxVQUFQLENBQWtCLENBQWxCLE1BQXlCO0FBQUc7QUFBaEMsSUFBeUM7QUFDdkN6UCxZQUFNLENBQUNuRSxJQUFQLENBQVksRUFBWjtBQUNEOztBQUNEMlQsUUFBTSxDQUFDMUosT0FBUCxDQUFld0osVUFBZixFQUEyQixVQUFTSSxLQUFULEVBQWdCQyxNQUFoQixFQUF3QkMsS0FBeEIsRUFBK0JDLFNBQS9CLEVBQTBDO0FBQ25FN1AsVUFBTSxDQUFDbkUsSUFBUCxDQUFZK1QsS0FBSyxHQUFHQyxTQUFTLENBQUMvSixPQUFWLENBQWtCeUosWUFBbEIsRUFBZ0MsSUFBaEMsQ0FBSCxHQUE0Q0ksTUFBTSxJQUFJRCxLQUF2RTtBQUNELEdBRkQ7QUFHQSxTQUFPMVAsTUFBUDtBQUNELENBVCtCLENBQWhDO0FBV0F4RixNQUFNLENBQUNDLE9BQVAsR0FBaUIwTyxZQUFqQixDOzs7Ozs7Ozs7OztBQzFCQSxJQUFJVCxRQUFRLEdBQUc1TCxtQkFBTyxDQUFDLHlEQUFELENBQXRCO0FBRUE7OztBQUNBLElBQUk2TCxRQUFRLEdBQUcsSUFBSSxDQUFuQjtBQUVBOzs7Ozs7OztBQU9BLFNBQVMxRyxLQUFULENBQWVoQyxLQUFmLEVBQXNCO0FBQ3BCLE1BQUksT0FBT0EsS0FBUCxJQUFnQixRQUFoQixJQUE0QnlJLFFBQVEsQ0FBQ3pJLEtBQUQsQ0FBeEMsRUFBaUQ7QUFDL0MsV0FBT0EsS0FBUDtBQUNEOztBQUNELE1BQUlELE1BQU0sR0FBSUMsS0FBSyxHQUFHLEVBQXRCO0FBQ0EsU0FBUUQsTUFBTSxJQUFJLEdBQVYsSUFBa0IsSUFBSUMsS0FBTCxJQUFlLENBQUMwSSxRQUFsQyxHQUE4QyxJQUE5QyxHQUFxRDNJLE1BQTVEO0FBQ0Q7O0FBRUR4RixNQUFNLENBQUNDLE9BQVAsR0FBaUJ3SCxLQUFqQixDOzs7Ozs7Ozs7OztBQ3BCQTtBQUNBLElBQUl3RCxTQUFTLEdBQUdDLFFBQVEsQ0FBQy9LLFNBQXpCO0FBRUE7O0FBQ0EsSUFBSWdMLFlBQVksR0FBR0YsU0FBUyxDQUFDM0ssUUFBN0I7QUFFQTs7Ozs7Ozs7QUFPQSxTQUFTd0ssUUFBVCxDQUFrQjBELElBQWxCLEVBQXdCO0FBQ3RCLE1BQUlBLElBQUksSUFBSSxJQUFaLEVBQWtCO0FBQ2hCLFFBQUk7QUFDRixhQUFPckQsWUFBWSxDQUFDdkUsSUFBYixDQUFrQjRILElBQWxCLENBQVA7QUFDRCxLQUZELENBRUUsT0FBT2dELENBQVAsRUFBVSxDQUFFOztBQUNkLFFBQUk7QUFDRixhQUFRaEQsSUFBSSxHQUFHLEVBQWY7QUFDRCxLQUZELENBRUUsT0FBT2dELENBQVAsRUFBVSxDQUFFO0FBQ2Y7O0FBQ0QsU0FBTyxFQUFQO0FBQ0Q7O0FBRUR4UixNQUFNLENBQUNDLE9BQVAsR0FBaUI2SyxRQUFqQixDOzs7Ozs7Ozs7OztBQ3pCQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFnQ0EsU0FBUzNELEVBQVQsQ0FBWTFCLEtBQVosRUFBbUJxRCxLQUFuQixFQUEwQjtBQUN4QixTQUFPckQsS0FBSyxLQUFLcUQsS0FBVixJQUFvQnJELEtBQUssS0FBS0EsS0FBVixJQUFtQnFELEtBQUssS0FBS0EsS0FBeEQ7QUFDRDs7QUFFRDlJLE1BQU0sQ0FBQ0MsT0FBUCxHQUFpQmtILEVBQWpCLEM7Ozs7Ozs7Ozs7O0FDcENBLElBQUk0SCxVQUFVLEdBQUd6TSxtQkFBTyxDQUFDLCtEQUFELENBQXhCO0FBQUEsSUFDSWdULFNBQVMsR0FBR2hULG1CQUFPLENBQUMsMkRBQUQsQ0FEdkI7QUFHQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFvQ0EsSUFBSWlULElBQUksR0FBR3hHLFVBQVUsQ0FBQ3VHLFNBQUQsQ0FBckI7QUFFQXRWLE1BQU0sQ0FBQ0MsT0FBUCxHQUFpQnNWLElBQWpCLEM7Ozs7Ozs7Ozs7O0FDekNBLElBQUlsTyxhQUFhLEdBQUcvRSxtQkFBTyxDQUFDLHFFQUFELENBQTNCO0FBQUEsSUFDSWdMLFlBQVksR0FBR2hMLG1CQUFPLENBQUMsbUVBQUQsQ0FEMUI7QUFBQSxJQUVJa1QsU0FBUyxHQUFHbFQsbUJBQU8sQ0FBQywyREFBRCxDQUZ2QjtBQUlBOzs7QUFDQSxJQUFJbVQsU0FBUyxHQUFHQyxJQUFJLENBQUNDLEdBQXJCO0FBRUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1DQSxTQUFTTCxTQUFULENBQW1CalEsS0FBbkIsRUFBMEJDLFNBQTFCLEVBQXFDZ0MsU0FBckMsRUFBZ0Q7QUFDOUMsTUFBSXBHLE1BQU0sR0FBR21FLEtBQUssSUFBSSxJQUFULEdBQWdCLENBQWhCLEdBQW9CQSxLQUFLLENBQUNuRSxNQUF2Qzs7QUFDQSxNQUFJLENBQUNBLE1BQUwsRUFBYTtBQUNYLFdBQU8sQ0FBQyxDQUFSO0FBQ0Q7O0FBQ0QsTUFBSThCLEtBQUssR0FBR3NFLFNBQVMsSUFBSSxJQUFiLEdBQW9CLENBQXBCLEdBQXdCa08sU0FBUyxDQUFDbE8sU0FBRCxDQUE3Qzs7QUFDQSxNQUFJdEUsS0FBSyxHQUFHLENBQVosRUFBZTtBQUNiQSxTQUFLLEdBQUd5UyxTQUFTLENBQUN2VSxNQUFNLEdBQUc4QixLQUFWLEVBQWlCLENBQWpCLENBQWpCO0FBQ0Q7O0FBQ0QsU0FBT3FFLGFBQWEsQ0FBQ2hDLEtBQUQsRUFBUWlJLFlBQVksQ0FBQ2hJLFNBQUQsRUFBWSxDQUFaLENBQXBCLEVBQW9DdEMsS0FBcEMsQ0FBcEI7QUFDRDs7QUFFRGhELE1BQU0sQ0FBQ0MsT0FBUCxHQUFpQnFWLFNBQWpCLEM7Ozs7Ozs7Ozs7O0FDdERBLElBQUk1TixPQUFPLEdBQUdwRixtQkFBTyxDQUFDLHlEQUFELENBQXJCO0FBRUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQXlCQSxTQUFTYyxHQUFULENBQWF1RSxNQUFiLEVBQXFCQyxJQUFyQixFQUEyQmdPLFlBQTNCLEVBQXlDO0FBQ3ZDLE1BQUlwUSxNQUFNLEdBQUdtQyxNQUFNLElBQUksSUFBVixHQUFpQkUsU0FBakIsR0FBNkJILE9BQU8sQ0FBQ0MsTUFBRCxFQUFTQyxJQUFULENBQWpEO0FBQ0EsU0FBT3BDLE1BQU0sS0FBS3FDLFNBQVgsR0FBdUIrTixZQUF2QixHQUFzQ3BRLE1BQTdDO0FBQ0Q7O0FBRUR4RixNQUFNLENBQUNDLE9BQVAsR0FBaUJtRCxHQUFqQixDOzs7Ozs7Ozs7OztBQ2hDQSxJQUFJb0YsU0FBUyxHQUFHbEcsbUJBQU8sQ0FBQyw2REFBRCxDQUF2QjtBQUFBLElBQ0lrUSxPQUFPLEdBQUdsUSxtQkFBTyxDQUFDLHlEQUFELENBRHJCO0FBR0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUEwQkEsU0FBU3NMLEtBQVQsQ0FBZWpHLE1BQWYsRUFBdUJDLElBQXZCLEVBQTZCO0FBQzNCLFNBQU9ELE1BQU0sSUFBSSxJQUFWLElBQWtCNkssT0FBTyxDQUFDN0ssTUFBRCxFQUFTQyxJQUFULEVBQWVZLFNBQWYsQ0FBaEM7QUFDRDs7QUFFRHhJLE1BQU0sQ0FBQ0MsT0FBUCxHQUFpQjJOLEtBQWpCLEM7Ozs7Ozs7Ozs7O0FDakNBOzs7Ozs7Ozs7Ozs7Ozs7O0FBZ0JBLFNBQVNSLFFBQVQsQ0FBa0IzSCxLQUFsQixFQUF5QjtBQUN2QixTQUFPQSxLQUFQO0FBQ0Q7O0FBRUR6RixNQUFNLENBQUNDLE9BQVAsR0FBaUJtTixRQUFqQixDOzs7Ozs7Ozs7OztBQ3BCQSxJQUFJekUsZUFBZSxHQUFHckcsbUJBQU8sQ0FBQyx5RUFBRCxDQUE3QjtBQUFBLElBQ0ltRyxZQUFZLEdBQUduRyxtQkFBTyxDQUFDLGlFQUFELENBRDFCO0FBR0E7OztBQUNBLElBQUkwRCxXQUFXLEdBQUdDLE1BQU0sQ0FBQzlGLFNBQXpCO0FBRUE7O0FBQ0EsSUFBSStGLGNBQWMsR0FBR0YsV0FBVyxDQUFDRSxjQUFqQztBQUVBOztBQUNBLElBQUl3TCxvQkFBb0IsR0FBRzFMLFdBQVcsQ0FBQzBMLG9CQUF2QztBQUVBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBa0JBLElBQUkvTCxXQUFXLEdBQUdnRCxlQUFlLENBQUMsWUFBVztBQUFFLFNBQU9rTixTQUFQO0FBQW1CLENBQWhDLEVBQUQsQ0FBZixHQUFzRGxOLGVBQXRELEdBQXdFLFVBQVNsRCxLQUFULEVBQWdCO0FBQ3hHLFNBQU9nRCxZQUFZLENBQUNoRCxLQUFELENBQVosSUFBdUJTLGNBQWMsQ0FBQ1UsSUFBZixDQUFvQm5CLEtBQXBCLEVBQTJCLFFBQTNCLENBQXZCLElBQ0wsQ0FBQ2lNLG9CQUFvQixDQUFDOUssSUFBckIsQ0FBMEJuQixLQUExQixFQUFpQyxRQUFqQyxDQURIO0FBRUQsQ0FIRDtBQUtBekYsTUFBTSxDQUFDQyxPQUFQLEdBQWlCMEYsV0FBakIsQzs7Ozs7Ozs7Ozs7QUNuQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBdUJBLElBQUlDLE9BQU8sR0FBR21CLEtBQUssQ0FBQ25CLE9BQXBCO0FBRUE1RixNQUFNLENBQUNDLE9BQVAsR0FBaUIyRixPQUFqQixDOzs7Ozs7Ozs7OztBQ3pCQSxJQUFJK0UsVUFBVSxHQUFHckksbUJBQU8sQ0FBQyw2REFBRCxDQUF4QjtBQUFBLElBQ0lvSixRQUFRLEdBQUdwSixtQkFBTyxDQUFDLHlEQUFELENBRHRCO0FBR0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQXlCQSxTQUFTdU0sV0FBVCxDQUFxQnBKLEtBQXJCLEVBQTRCO0FBQzFCLFNBQU9BLEtBQUssSUFBSSxJQUFULElBQWlCaUcsUUFBUSxDQUFDakcsS0FBSyxDQUFDdkUsTUFBUCxDQUF6QixJQUEyQyxDQUFDeUosVUFBVSxDQUFDbEYsS0FBRCxDQUE3RDtBQUNEOztBQUVEekYsTUFBTSxDQUFDQyxPQUFQLEdBQWlCNE8sV0FBakIsQzs7Ozs7Ozs7Ozs7OztBQ2hDQSxJQUFJdE0sSUFBSSxHQUFHRCxtQkFBTyxDQUFDLG1EQUFELENBQWxCO0FBQUEsSUFDSXdULFNBQVMsR0FBR3hULG1CQUFPLENBQUMsMkRBQUQsQ0FEdkI7QUFHQTs7O0FBQ0EsSUFBSXlSLFdBQVcsR0FBRyw4QkFBTzlULE9BQVAsTUFBa0IsUUFBbEIsSUFBOEJBLE9BQTlCLElBQXlDLENBQUNBLE9BQU8sQ0FBQytULFFBQWxELElBQThEL1QsT0FBaEY7QUFFQTs7QUFDQSxJQUFJZ1UsVUFBVSxHQUFHRixXQUFXLElBQUksOEJBQU8vVCxNQUFQLE1BQWlCLFFBQWhDLElBQTRDQSxNQUE1QyxJQUFzRCxDQUFDQSxNQUFNLENBQUNnVSxRQUE5RCxJQUEwRWhVLE1BQTNGO0FBRUE7O0FBQ0EsSUFBSWtVLGFBQWEsR0FBR0QsVUFBVSxJQUFJQSxVQUFVLENBQUNoVSxPQUFYLEtBQXVCOFQsV0FBekQ7QUFFQTs7QUFDQSxJQUFJZ0MsTUFBTSxHQUFHN0IsYUFBYSxHQUFHM1IsSUFBSSxDQUFDd1QsTUFBUixHQUFpQmxPLFNBQTNDO0FBRUE7O0FBQ0EsSUFBSW1PLGNBQWMsR0FBR0QsTUFBTSxHQUFHQSxNQUFNLENBQUNsUSxRQUFWLEdBQXFCZ0MsU0FBaEQ7QUFFQTs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLElBQUloQyxRQUFRLEdBQUdtUSxjQUFjLElBQUlGLFNBQWpDO0FBRUE5VixNQUFNLENBQUNDLE9BQVAsR0FBaUI0RixRQUFqQixDOzs7Ozs7Ozs7Ozs7QUNyQ0EsSUFBSTBDLFVBQVUsR0FBR2pHLG1CQUFPLENBQUMsK0RBQUQsQ0FBeEI7QUFBQSxJQUNJdUksUUFBUSxHQUFHdkksbUJBQU8sQ0FBQyx5REFBRCxDQUR0QjtBQUdBOzs7QUFDQSxJQUFJMlQsUUFBUSxHQUFHLHdCQUFmO0FBQUEsSUFDSW5LLE9BQU8sR0FBRyxtQkFEZDtBQUFBLElBRUlvSyxNQUFNLEdBQUcsNEJBRmI7QUFBQSxJQUdJQyxRQUFRLEdBQUcsZ0JBSGY7QUFLQTs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLFNBQVN4TCxVQUFULENBQW9CbEYsS0FBcEIsRUFBMkI7QUFDekIsTUFBSSxDQUFDb0YsUUFBUSxDQUFDcEYsS0FBRCxDQUFiLEVBQXNCO0FBQ3BCLFdBQU8sS0FBUDtBQUNELEdBSHdCLENBSXpCO0FBQ0E7OztBQUNBLE1BQUl3SyxHQUFHLEdBQUcxSCxVQUFVLENBQUM5QyxLQUFELENBQXBCO0FBQ0EsU0FBT3dLLEdBQUcsSUFBSW5FLE9BQVAsSUFBa0JtRSxHQUFHLElBQUlpRyxNQUF6QixJQUFtQ2pHLEdBQUcsSUFBSWdHLFFBQTFDLElBQXNEaEcsR0FBRyxJQUFJa0csUUFBcEU7QUFDRDs7QUFFRG5XLE1BQU0sQ0FBQ0MsT0FBUCxHQUFpQjBLLFVBQWpCLEM7Ozs7Ozs7Ozs7O0FDcENBO0FBQ0EsSUFBSWlJLGdCQUFnQixHQUFHLGdCQUF2QjtBQUVBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUEwQkEsU0FBU2xILFFBQVQsQ0FBa0JqRyxLQUFsQixFQUF5QjtBQUN2QixTQUFPLE9BQU9BLEtBQVAsSUFBZ0IsUUFBaEIsSUFDTEEsS0FBSyxHQUFHLENBQUMsQ0FESixJQUNTQSxLQUFLLEdBQUcsQ0FBUixJQUFhLENBRHRCLElBQzJCQSxLQUFLLElBQUltTixnQkFEM0M7QUFFRDs7QUFFRDVTLE1BQU0sQ0FBQ0MsT0FBUCxHQUFpQnlMLFFBQWpCLEM7Ozs7Ozs7Ozs7Ozs7QUNsQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUF5QkEsU0FBU2IsUUFBVCxDQUFrQnBGLEtBQWxCLEVBQXlCO0FBQ3ZCLE1BQUlxTixJQUFJLFdBQVVyTixLQUFWLENBQVI7O0FBQ0EsU0FBT0EsS0FBSyxJQUFJLElBQVQsS0FBa0JxTixJQUFJLElBQUksUUFBUixJQUFvQkEsSUFBSSxJQUFJLFVBQTlDLENBQVA7QUFDRDs7QUFFRDlTLE1BQU0sQ0FBQ0MsT0FBUCxHQUFpQjRLLFFBQWpCLEM7Ozs7Ozs7Ozs7Ozs7QUM5QkE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQXdCQSxTQUFTcEMsWUFBVCxDQUFzQmhELEtBQXRCLEVBQTZCO0FBQzNCLFNBQU9BLEtBQUssSUFBSSxJQUFULElBQWlCLFFBQU9BLEtBQVAsS0FBZ0IsUUFBeEM7QUFDRDs7QUFFRHpGLE1BQU0sQ0FBQ0MsT0FBUCxHQUFpQndJLFlBQWpCLEM7Ozs7Ozs7Ozs7Ozs7QUM1QkEsSUFBSUYsVUFBVSxHQUFHakcsbUJBQU8sQ0FBQywrREFBRCxDQUF4QjtBQUFBLElBQ0ltRyxZQUFZLEdBQUduRyxtQkFBTyxDQUFDLGlFQUFELENBRDFCO0FBR0E7OztBQUNBLElBQUl3TixTQUFTLEdBQUcsaUJBQWhCO0FBRUE7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWlCQSxTQUFTNUIsUUFBVCxDQUFrQnpJLEtBQWxCLEVBQXlCO0FBQ3ZCLFNBQU8sUUFBT0EsS0FBUCxLQUFnQixRQUFoQixJQUNKZ0QsWUFBWSxDQUFDaEQsS0FBRCxDQUFaLElBQXVCOEMsVUFBVSxDQUFDOUMsS0FBRCxDQUFWLElBQXFCcUssU0FEL0M7QUFFRDs7QUFFRDlQLE1BQU0sQ0FBQ0MsT0FBUCxHQUFpQmlPLFFBQWpCLEM7Ozs7Ozs7Ozs7O0FDNUJBLElBQUlqQixnQkFBZ0IsR0FBRzNLLG1CQUFPLENBQUMsMkVBQUQsQ0FBOUI7QUFBQSxJQUNJaU0sU0FBUyxHQUFHak0sbUJBQU8sQ0FBQyw2REFBRCxDQUR2QjtBQUFBLElBRUkrUixRQUFRLEdBQUcvUixtQkFBTyxDQUFDLDJEQUFELENBRnRCO0FBSUE7OztBQUNBLElBQUk4VCxnQkFBZ0IsR0FBRy9CLFFBQVEsSUFBSUEsUUFBUSxDQUFDdE8sWUFBNUM7QUFFQTs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLElBQUlBLFlBQVksR0FBR3FRLGdCQUFnQixHQUFHN0gsU0FBUyxDQUFDNkgsZ0JBQUQsQ0FBWixHQUFpQ25KLGdCQUFwRTtBQUVBak4sTUFBTSxDQUFDQyxPQUFQLEdBQWlCOEYsWUFBakIsQzs7Ozs7Ozs7Ozs7QUMxQkEsSUFBSUksYUFBYSxHQUFHN0QsbUJBQU8sQ0FBQyxxRUFBRCxDQUEzQjtBQUFBLElBQ0ltTCxRQUFRLEdBQUduTCxtQkFBTyxDQUFDLDJEQUFELENBRHRCO0FBQUEsSUFFSXVNLFdBQVcsR0FBR3ZNLG1CQUFPLENBQUMsK0RBQUQsQ0FGekI7QUFJQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBNEJBLFNBQVN3TSxJQUFULENBQWNuSCxNQUFkLEVBQXNCO0FBQ3BCLFNBQU9rSCxXQUFXLENBQUNsSCxNQUFELENBQVgsR0FBc0J4QixhQUFhLENBQUN3QixNQUFELENBQW5DLEdBQThDOEYsUUFBUSxDQUFDOUYsTUFBRCxDQUE3RDtBQUNEOztBQUVEM0gsTUFBTSxDQUFDQyxPQUFQLEdBQWlCNk8sSUFBakIsQzs7Ozs7Ozs7Ozs7QUNwQ0EsSUFBSTVLLFFBQVEsR0FBRzVCLG1CQUFPLENBQUMsMkRBQUQsQ0FBdEI7QUFFQTs7O0FBQ0EsSUFBSStULGVBQWUsR0FBRyxxQkFBdEI7QUFFQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBNENBLFNBQVMxQyxPQUFULENBQWlCbkYsSUFBakIsRUFBdUI4SCxRQUF2QixFQUFpQztBQUMvQixNQUFJLE9BQU85SCxJQUFQLElBQWUsVUFBZixJQUE4QjhILFFBQVEsSUFBSSxJQUFaLElBQW9CLE9BQU9BLFFBQVAsSUFBbUIsVUFBekUsRUFBc0Y7QUFDcEYsVUFBTSxJQUFJQyxTQUFKLENBQWNGLGVBQWQsQ0FBTjtBQUNEOztBQUNELE1BQUlHLFFBQVEsR0FBRyxTQUFYQSxRQUFXLEdBQVc7QUFDeEIsUUFBSUMsSUFBSSxHQUFHWixTQUFYO0FBQUEsUUFDSWxQLEdBQUcsR0FBRzJQLFFBQVEsR0FBR0EsUUFBUSxDQUFDSSxLQUFULENBQWUsSUFBZixFQUFxQkQsSUFBckIsQ0FBSCxHQUFnQ0EsSUFBSSxDQUFDLENBQUQsQ0FEdEQ7QUFBQSxRQUVJL0gsS0FBSyxHQUFHOEgsUUFBUSxDQUFDOUgsS0FGckI7O0FBSUEsUUFBSUEsS0FBSyxDQUFDckwsR0FBTixDQUFVc0QsR0FBVixDQUFKLEVBQW9CO0FBQ2xCLGFBQU8rSCxLQUFLLENBQUN0TCxHQUFOLENBQVV1RCxHQUFWLENBQVA7QUFDRDs7QUFDRCxRQUFJbkIsTUFBTSxHQUFHZ0osSUFBSSxDQUFDa0ksS0FBTCxDQUFXLElBQVgsRUFBaUJELElBQWpCLENBQWI7QUFDQUQsWUFBUSxDQUFDOUgsS0FBVCxHQUFpQkEsS0FBSyxDQUFDdkwsR0FBTixDQUFVd0QsR0FBVixFQUFlbkIsTUFBZixLQUEwQmtKLEtBQTNDO0FBQ0EsV0FBT2xKLE1BQVA7QUFDRCxHQVhEOztBQVlBZ1IsVUFBUSxDQUFDOUgsS0FBVCxHQUFpQixLQUFLaUYsT0FBTyxDQUFDZ0QsS0FBUixJQUFpQnpTLFFBQXRCLEdBQWpCO0FBQ0EsU0FBT3NTLFFBQVA7QUFDRCxDLENBRUQ7OztBQUNBN0MsT0FBTyxDQUFDZ0QsS0FBUixHQUFnQnpTLFFBQWhCO0FBRUFsRSxNQUFNLENBQUNDLE9BQVAsR0FBaUIwVCxPQUFqQixDOzs7Ozs7Ozs7OztBQ3hFQSxJQUFJNUYsWUFBWSxHQUFHekwsbUJBQU8sQ0FBQyxtRUFBRCxDQUExQjtBQUFBLElBQ0kwTCxnQkFBZ0IsR0FBRzFMLG1CQUFPLENBQUMsMkVBQUQsQ0FEOUI7QUFBQSxJQUVJdUwsS0FBSyxHQUFHdkwsbUJBQU8sQ0FBQyxxREFBRCxDQUZuQjtBQUFBLElBR0ltRixLQUFLLEdBQUduRixtQkFBTyxDQUFDLHFEQUFELENBSG5CO0FBS0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQXNCQSxTQUFTK0ssUUFBVCxDQUFrQnpGLElBQWxCLEVBQXdCO0FBQ3RCLFNBQU9pRyxLQUFLLENBQUNqRyxJQUFELENBQUwsR0FBY21HLFlBQVksQ0FBQ3RHLEtBQUssQ0FBQ0csSUFBRCxDQUFOLENBQTFCLEdBQTBDb0csZ0JBQWdCLENBQUNwRyxJQUFELENBQWpFO0FBQ0Q7O0FBRUQ1SCxNQUFNLENBQUNDLE9BQVAsR0FBaUJvTixRQUFqQixDOzs7Ozs7Ozs7OztBQy9CQTs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBa0JBLFNBQVNvRSxTQUFULEdBQXFCO0FBQ25CLFNBQU8sRUFBUDtBQUNEOztBQUVEelIsTUFBTSxDQUFDQyxPQUFQLEdBQWlCd1IsU0FBakIsQzs7Ozs7Ozs7Ozs7QUN0QkE7Ozs7Ozs7Ozs7Ozs7QUFhQSxTQUFTcUUsU0FBVCxHQUFxQjtBQUNuQixTQUFPLEtBQVA7QUFDRDs7QUFFRDlWLE1BQU0sQ0FBQ0MsT0FBUCxHQUFpQjZWLFNBQWpCLEM7Ozs7Ozs7Ozs7O0FDakJBLElBQUljLFFBQVEsR0FBR3RVLG1CQUFPLENBQUMseURBQUQsQ0FBdEI7QUFFQTs7O0FBQ0EsSUFBSTZMLFFBQVEsR0FBRyxJQUFJLENBQW5CO0FBQUEsSUFDSTBJLFdBQVcsR0FBRyx1QkFEbEI7QUFHQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBdUJBLFNBQVNDLFFBQVQsQ0FBa0JyUixLQUFsQixFQUF5QjtBQUN2QixNQUFJLENBQUNBLEtBQUwsRUFBWTtBQUNWLFdBQU9BLEtBQUssS0FBSyxDQUFWLEdBQWNBLEtBQWQsR0FBc0IsQ0FBN0I7QUFDRDs7QUFDREEsT0FBSyxHQUFHbVIsUUFBUSxDQUFDblIsS0FBRCxDQUFoQjs7QUFDQSxNQUFJQSxLQUFLLEtBQUswSSxRQUFWLElBQXNCMUksS0FBSyxLQUFLLENBQUMwSSxRQUFyQyxFQUErQztBQUM3QyxRQUFJNEksSUFBSSxHQUFJdFIsS0FBSyxHQUFHLENBQVIsR0FBWSxDQUFDLENBQWIsR0FBaUIsQ0FBN0I7QUFDQSxXQUFPc1IsSUFBSSxHQUFHRixXQUFkO0FBQ0Q7O0FBQ0QsU0FBT3BSLEtBQUssS0FBS0EsS0FBVixHQUFrQkEsS0FBbEIsR0FBMEIsQ0FBakM7QUFDRDs7QUFFRHpGLE1BQU0sQ0FBQ0MsT0FBUCxHQUFpQjZXLFFBQWpCLEM7Ozs7Ozs7Ozs7O0FDekNBLElBQUlBLFFBQVEsR0FBR3hVLG1CQUFPLENBQUMseURBQUQsQ0FBdEI7QUFFQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQTBCQSxTQUFTa1QsU0FBVCxDQUFtQi9QLEtBQW5CLEVBQTBCO0FBQ3hCLE1BQUlELE1BQU0sR0FBR3NSLFFBQVEsQ0FBQ3JSLEtBQUQsQ0FBckI7QUFBQSxNQUNJdVIsU0FBUyxHQUFHeFIsTUFBTSxHQUFHLENBRHpCO0FBR0EsU0FBT0EsTUFBTSxLQUFLQSxNQUFYLEdBQXFCd1IsU0FBUyxHQUFHeFIsTUFBTSxHQUFHd1IsU0FBWixHQUF3QnhSLE1BQXRELEdBQWdFLENBQXZFO0FBQ0Q7O0FBRUR4RixNQUFNLENBQUNDLE9BQVAsR0FBaUJ1VixTQUFqQixDOzs7Ozs7Ozs7OztBQ25DQSxJQUFJM0ssUUFBUSxHQUFHdkksbUJBQU8sQ0FBQyx5REFBRCxDQUF0QjtBQUFBLElBQ0k0TCxRQUFRLEdBQUc1TCxtQkFBTyxDQUFDLHlEQUFELENBRHRCO0FBR0E7OztBQUNBLElBQUkyVSxHQUFHLEdBQUcsSUFBSSxDQUFkO0FBRUE7O0FBQ0EsSUFBSUMsTUFBTSxHQUFHLFlBQWI7QUFFQTs7QUFDQSxJQUFJQyxVQUFVLEdBQUcsb0JBQWpCO0FBRUE7O0FBQ0EsSUFBSUMsVUFBVSxHQUFHLFlBQWpCO0FBRUE7O0FBQ0EsSUFBSUMsU0FBUyxHQUFHLGFBQWhCO0FBRUE7O0FBQ0EsSUFBSUMsWUFBWSxHQUFHQyxRQUFuQjtBQUVBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUF1QkEsU0FBU1gsUUFBVCxDQUFrQm5SLEtBQWxCLEVBQXlCO0FBQ3ZCLE1BQUksT0FBT0EsS0FBUCxJQUFnQixRQUFwQixFQUE4QjtBQUM1QixXQUFPQSxLQUFQO0FBQ0Q7O0FBQ0QsTUFBSXlJLFFBQVEsQ0FBQ3pJLEtBQUQsQ0FBWixFQUFxQjtBQUNuQixXQUFPd1IsR0FBUDtBQUNEOztBQUNELE1BQUlwTSxRQUFRLENBQUNwRixLQUFELENBQVosRUFBcUI7QUFDbkIsUUFBSXFELEtBQUssR0FBRyxPQUFPckQsS0FBSyxDQUFDdUssT0FBYixJQUF3QixVQUF4QixHQUFxQ3ZLLEtBQUssQ0FBQ3VLLE9BQU4sRUFBckMsR0FBdUR2SyxLQUFuRTtBQUNBQSxTQUFLLEdBQUdvRixRQUFRLENBQUMvQixLQUFELENBQVIsR0FBbUJBLEtBQUssR0FBRyxFQUEzQixHQUFpQ0EsS0FBekM7QUFDRDs7QUFDRCxNQUFJLE9BQU9yRCxLQUFQLElBQWdCLFFBQXBCLEVBQThCO0FBQzVCLFdBQU9BLEtBQUssS0FBSyxDQUFWLEdBQWNBLEtBQWQsR0FBc0IsQ0FBQ0EsS0FBOUI7QUFDRDs7QUFDREEsT0FBSyxHQUFHQSxLQUFLLENBQUM2RixPQUFOLENBQWM0TCxNQUFkLEVBQXNCLEVBQXRCLENBQVI7QUFDQSxNQUFJTSxRQUFRLEdBQUdKLFVBQVUsQ0FBQzNMLElBQVgsQ0FBZ0JoRyxLQUFoQixDQUFmO0FBQ0EsU0FBUStSLFFBQVEsSUFBSUgsU0FBUyxDQUFDNUwsSUFBVixDQUFlaEcsS0FBZixDQUFiLEdBQ0g2UixZQUFZLENBQUM3UixLQUFLLENBQUNnUyxLQUFOLENBQVksQ0FBWixDQUFELEVBQWlCRCxRQUFRLEdBQUcsQ0FBSCxHQUFPLENBQWhDLENBRFQsR0FFRkwsVUFBVSxDQUFDMUwsSUFBWCxDQUFnQmhHLEtBQWhCLElBQXlCd1IsR0FBekIsR0FBK0IsQ0FBQ3hSLEtBRnJDO0FBR0Q7O0FBRUR6RixNQUFNLENBQUNDLE9BQVAsR0FBaUIyVyxRQUFqQixDOzs7Ozs7Ozs7OztBQ2pFQSxJQUFJdEksWUFBWSxHQUFHaE0sbUJBQU8sQ0FBQyxtRUFBRCxDQUExQjtBQUVBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQXFCQSxTQUFTaEMsUUFBVCxDQUFrQm1GLEtBQWxCLEVBQXlCO0FBQ3ZCLFNBQU9BLEtBQUssSUFBSSxJQUFULEdBQWdCLEVBQWhCLEdBQXFCNkksWUFBWSxDQUFDN0ksS0FBRCxDQUF4QztBQUNEOztBQUVEekYsTUFBTSxDQUFDQyxPQUFQLEdBQWlCSyxRQUFqQixDOzs7Ozs7Ozs7Ozs7QUMzQkE7QUFBQTtBQUFBOztBQUVBO0FBQ0E7QUFDQTs7QUFFZTtBQUNmO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLHlCQUF5QjtBQUN6QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7QUNqR0E7O0FBRUE7QUFDQSxjQUFjLG1CQUFPLENBQUMsd3BCQUFrWDtBQUN4WSw0Q0FBNEMsUUFBUztBQUNyRDtBQUNBO0FBQ0EsVUFBVSxtQkFBTyxDQUFDLG9JQUFtRTtBQUNyRiwrQ0FBK0M7QUFDL0M7QUFDQSxHQUFHLEtBQVUsRUFBRSxFOzs7Ozs7Ozs7Ozs7QUNWZjtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUV5Qzs7QUFFekM7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxVQUFVLGlCQUFpQjtBQUMzQjtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLG1CQUFtQjtBQUNuQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVlO0FBQ2Y7O0FBRUE7O0FBRUEsZUFBZSw2REFBWTtBQUMzQjs7QUFFQTtBQUNBO0FBQ0EsbUJBQW1CLG1CQUFtQjtBQUN0QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxlQUFlLDZEQUFZO0FBQzNCO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxtQkFBbUIsc0JBQXNCO0FBQ3pDO0FBQ0E7QUFDQSx1QkFBdUIsMkJBQTJCO0FBQ2xEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0EsaUJBQWlCLG1CQUFtQjtBQUNwQztBQUNBO0FBQ0E7QUFDQTtBQUNBLHFCQUFxQiwyQkFBMkI7QUFDaEQ7QUFDQTtBQUNBLFlBQVksdUJBQXVCO0FBQ25DO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQSxxQkFBcUIsdUJBQXVCO0FBQzVDO0FBQ0E7QUFDQSw4QkFBOEI7QUFDOUI7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDOztBQUVEO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EseURBQXlEO0FBQ3pEOztBQUVBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7O0FDN05BO0FBQUE7QUFBQTs7OztBQUllLFNBQVNvWCxZQUFULENBQXVCQyxRQUF2QixFQUFpQ3RYLElBQWpDLEVBQXVDO0FBQ3BELE1BQUl1WCxNQUFNLEdBQUcsRUFBYjtBQUNBLE1BQUlDLFNBQVMsR0FBRyxFQUFoQjs7QUFDQSxPQUFLLElBQUloWCxDQUFDLEdBQUcsQ0FBYixFQUFnQkEsQ0FBQyxHQUFHUixJQUFJLENBQUNhLE1BQXpCLEVBQWlDTCxDQUFDLEVBQWxDLEVBQXNDO0FBQ3BDLFFBQUlMLElBQUksR0FBR0gsSUFBSSxDQUFDUSxDQUFELENBQWY7QUFDQSxRQUFJTSxFQUFFLEdBQUdYLElBQUksQ0FBQyxDQUFELENBQWI7QUFDQSxRQUFJc1gsR0FBRyxHQUFHdFgsSUFBSSxDQUFDLENBQUQsQ0FBZDtBQUNBLFFBQUl1WCxLQUFLLEdBQUd2WCxJQUFJLENBQUMsQ0FBRCxDQUFoQjtBQUNBLFFBQUlzQixTQUFTLEdBQUd0QixJQUFJLENBQUMsQ0FBRCxDQUFwQjtBQUNBLFFBQUl3WCxJQUFJLEdBQUc7QUFDVDdXLFFBQUUsRUFBRXdXLFFBQVEsR0FBRyxHQUFYLEdBQWlCOVcsQ0FEWjtBQUVUaVgsU0FBRyxFQUFFQSxHQUZJO0FBR1RDLFdBQUssRUFBRUEsS0FIRTtBQUlUalcsZUFBUyxFQUFFQTtBQUpGLEtBQVg7O0FBTUEsUUFBSSxDQUFDK1YsU0FBUyxDQUFDMVcsRUFBRCxDQUFkLEVBQW9CO0FBQ2xCeVcsWUFBTSxDQUFDdlcsSUFBUCxDQUFZd1csU0FBUyxDQUFDMVcsRUFBRCxDQUFULEdBQWdCO0FBQUVBLFVBQUUsRUFBRUEsRUFBTjtBQUFVOFcsYUFBSyxFQUFFLENBQUNELElBQUQ7QUFBakIsT0FBNUI7QUFDRCxLQUZELE1BRU87QUFDTEgsZUFBUyxDQUFDMVcsRUFBRCxDQUFULENBQWM4VyxLQUFkLENBQW9CNVcsSUFBcEIsQ0FBeUIyVyxJQUF6QjtBQUNEO0FBQ0Y7O0FBQ0QsU0FBT0osTUFBUDtBQUNELEM7Ozs7Ozs7Ozs7Ozs7QUMxQkQsSUFBSU0sQ0FBSixDLENBRUE7O0FBQ0FBLENBQUMsR0FBSSxZQUFXO0FBQ2YsU0FBTyxJQUFQO0FBQ0EsQ0FGRyxFQUFKOztBQUlBLElBQUk7QUFDSDtBQUNBQSxHQUFDLEdBQUdBLENBQUMsSUFBSSxJQUFJaE4sUUFBSixDQUFhLGFBQWIsR0FBVDtBQUNBLENBSEQsQ0FHRSxPQUFPc0csQ0FBUCxFQUFVO0FBQ1g7QUFDQSxNQUFJLFFBQU8yRyxNQUFQLHlDQUFPQSxNQUFQLE9BQWtCLFFBQXRCLEVBQWdDRCxDQUFDLEdBQUdDLE1BQUo7QUFDaEMsQyxDQUVEO0FBQ0E7QUFDQTs7O0FBRUFuWSxNQUFNLENBQUNDLE9BQVAsR0FBaUJpWSxDQUFqQixDOzs7Ozs7Ozs7OztBQ25CQWxZLE1BQU0sQ0FBQ0MsT0FBUCxHQUFpQixVQUFTRCxNQUFULEVBQWlCO0FBQ2pDLE1BQUksQ0FBQ0EsTUFBTSxDQUFDb1ksZUFBWixFQUE2QjtBQUM1QnBZLFVBQU0sQ0FBQ3FZLFNBQVAsR0FBbUIsWUFBVyxDQUFFLENBQWhDOztBQUNBclksVUFBTSxDQUFDc1ksS0FBUCxHQUFlLEVBQWYsQ0FGNEIsQ0FHNUI7O0FBQ0EsUUFBSSxDQUFDdFksTUFBTSxDQUFDdVksUUFBWixFQUFzQnZZLE1BQU0sQ0FBQ3VZLFFBQVAsR0FBa0IsRUFBbEI7QUFDdEJ0UyxVQUFNLENBQUN1UyxjQUFQLENBQXNCeFksTUFBdEIsRUFBOEIsUUFBOUIsRUFBd0M7QUFDdkN5WSxnQkFBVSxFQUFFLElBRDJCO0FBRXZDclYsU0FBRyxFQUFFLGVBQVc7QUFDZixlQUFPcEQsTUFBTSxDQUFDMFksQ0FBZDtBQUNBO0FBSnNDLEtBQXhDO0FBTUF6UyxVQUFNLENBQUN1UyxjQUFQLENBQXNCeFksTUFBdEIsRUFBOEIsSUFBOUIsRUFBb0M7QUFDbkN5WSxnQkFBVSxFQUFFLElBRHVCO0FBRW5DclYsU0FBRyxFQUFFLGVBQVc7QUFDZixlQUFPcEQsTUFBTSxDQUFDYSxDQUFkO0FBQ0E7QUFKa0MsS0FBcEM7QUFNQWIsVUFBTSxDQUFDb1ksZUFBUCxHQUF5QixDQUF6QjtBQUNBOztBQUNELFNBQU9wWSxNQUFQO0FBQ0EsQ0FyQkQsQzs7Ozs7Ozs7Ozs7O0FDQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUM4RDtBQUNMO0FBQ2M7OztBQUd2RTtBQUNnRztBQUNoRyxnQkFBZ0IsMkdBQVU7QUFDMUIsRUFBRSxnRkFBTTtBQUNSO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBLElBQUksS0FBVSxFQUFFLFlBWWY7QUFDRDtBQUNlLGdGOzs7Ozs7Ozs7Ozs7QUNsQ2Y7QUFBQTtBQUFBLHdDQUFzTCxDQUFnQiw4T0FBRyxFQUFDLEM7Ozs7Ozs7Ozs7OztBQ0ExTTtBQUFBO0FBQUE7QUFBQTtBQUFzYixDQUFnQix1YkFBRyxFQUFDLEM7Ozs7Ozs7Ozs7O0FDQTFjLGdFIiwiZmlsZSI6IkNvbnRyb2xLaXQuanMiLCJzb3VyY2VzQ29udGVudCI6WyIoZnVuY3Rpb24gd2VicGFja1VuaXZlcnNhbE1vZHVsZURlZmluaXRpb24ocm9vdCwgZmFjdG9yeSkge1xuXHRpZih0eXBlb2YgZXhwb3J0cyA9PT0gJ29iamVjdCcgJiYgdHlwZW9mIG1vZHVsZSA9PT0gJ29iamVjdCcpXG5cdFx0bW9kdWxlLmV4cG9ydHMgPSBmYWN0b3J5KHJlcXVpcmUoXCJAbW9ub2dyaWQvanMtdXRpbHNcIikpO1xuXHRlbHNlIGlmKHR5cGVvZiBkZWZpbmUgPT09ICdmdW5jdGlvbicgJiYgZGVmaW5lLmFtZClcblx0XHRkZWZpbmUoXCJ2dWUtbGliXCIsIFtcIkBtb25vZ3JpZC9qcy11dGlsc1wiXSwgZmFjdG9yeSk7XG5cdGVsc2UgaWYodHlwZW9mIGV4cG9ydHMgPT09ICdvYmplY3QnKVxuXHRcdGV4cG9ydHNbXCJ2dWUtbGliXCJdID0gZmFjdG9yeShyZXF1aXJlKFwiQG1vbm9ncmlkL2pzLXV0aWxzXCIpKTtcblx0ZWxzZVxuXHRcdHJvb3RbXCJ2dWUtbGliXCJdID0gZmFjdG9yeShyb290W1wiQG1vbm9ncmlkL2pzLXV0aWxzXCJdKTtcbn0pKHdpbmRvdywgZnVuY3Rpb24oX19XRUJQQUNLX0VYVEVSTkFMX01PRFVMRV9fbW9ub2dyaWRfanNfdXRpbHNfXykge1xucmV0dXJuICIsIiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwgeyBlbnVtZXJhYmxlOiB0cnVlLCBnZXQ6IGdldHRlciB9KTtcbiBcdFx0fVxuIFx0fTtcblxuIFx0Ly8gZGVmaW5lIF9fZXNNb2R1bGUgb24gZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yID0gZnVuY3Rpb24oZXhwb3J0cykge1xuIFx0XHRpZih0eXBlb2YgU3ltYm9sICE9PSAndW5kZWZpbmVkJyAmJiBTeW1ib2wudG9TdHJpbmdUYWcpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgU3ltYm9sLnRvU3RyaW5nVGFnLCB7IHZhbHVlOiAnTW9kdWxlJyB9KTtcbiBcdFx0fVxuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ19fZXNNb2R1bGUnLCB7IHZhbHVlOiB0cnVlIH0pO1xuIFx0fTtcblxuIFx0Ly8gY3JlYXRlIGEgZmFrZSBuYW1lc3BhY2Ugb2JqZWN0XG4gXHQvLyBtb2RlICYgMTogdmFsdWUgaXMgYSBtb2R1bGUgaWQsIHJlcXVpcmUgaXRcbiBcdC8vIG1vZGUgJiAyOiBtZXJnZSBhbGwgcHJvcGVydGllcyBvZiB2YWx1ZSBpbnRvIHRoZSBuc1xuIFx0Ly8gbW9kZSAmIDQ6IHJldHVybiB2YWx1ZSB3aGVuIGFscmVhZHkgbnMgb2JqZWN0XG4gXHQvLyBtb2RlICYgOHwxOiBiZWhhdmUgbGlrZSByZXF1aXJlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnQgPSBmdW5jdGlvbih2YWx1ZSwgbW9kZSkge1xuIFx0XHRpZihtb2RlICYgMSkgdmFsdWUgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKHZhbHVlKTtcbiBcdFx0aWYobW9kZSAmIDgpIHJldHVybiB2YWx1ZTtcbiBcdFx0aWYoKG1vZGUgJiA0KSAmJiB0eXBlb2YgdmFsdWUgPT09ICdvYmplY3QnICYmIHZhbHVlICYmIHZhbHVlLl9fZXNNb2R1bGUpIHJldHVybiB2YWx1ZTtcbiBcdFx0dmFyIG5zID0gT2JqZWN0LmNyZWF0ZShudWxsKTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yKG5zKTtcbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KG5zLCAnZGVmYXVsdCcsIHsgZW51bWVyYWJsZTogdHJ1ZSwgdmFsdWU6IHZhbHVlIH0pO1xuIFx0XHRpZihtb2RlICYgMiAmJiB0eXBlb2YgdmFsdWUgIT0gJ3N0cmluZycpIGZvcih2YXIga2V5IGluIHZhbHVlKSBfX3dlYnBhY2tfcmVxdWlyZV9fLmQobnMsIGtleSwgZnVuY3Rpb24oa2V5KSB7IHJldHVybiB2YWx1ZVtrZXldOyB9LmJpbmQobnVsbCwga2V5KSk7XG4gXHRcdHJldHVybiBucztcbiBcdH07XG5cbiBcdC8vIGdldERlZmF1bHRFeHBvcnQgZnVuY3Rpb24gZm9yIGNvbXBhdGliaWxpdHkgd2l0aCBub24taGFybW9ueSBtb2R1bGVzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSBmdW5jdGlvbihtb2R1bGUpIHtcbiBcdFx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0RGVmYXVsdCgpIHsgcmV0dXJuIG1vZHVsZVsnZGVmYXVsdCddOyB9IDpcbiBcdFx0XHRmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCAnYScsIGdldHRlcik7XG4gXHRcdHJldHVybiBnZXR0ZXI7XG4gXHR9O1xuXG4gXHQvLyBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGxcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydHkpIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KTsgfTtcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiXCI7XG5cblxuIFx0Ly8gTG9hZCBlbnRyeSBtb2R1bGUgYW5kIHJldHVybiBleHBvcnRzXG4gXHRyZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXyhfX3dlYnBhY2tfcmVxdWlyZV9fLnMgPSBcIi4vc3JjL2NvbXBvbmVudHMvQ29udHJvbEtpdC52dWVcIik7XG4iLCJmdW5jdGlvbiBfdHlwZW9mKG9iaikge1xuICBcIkBiYWJlbC9oZWxwZXJzIC0gdHlwZW9mXCI7XG5cbiAgaWYgKHR5cGVvZiBTeW1ib2wgPT09IFwiZnVuY3Rpb25cIiAmJiB0eXBlb2YgU3ltYm9sLml0ZXJhdG9yID09PSBcInN5bWJvbFwiKSB7XG4gICAgbW9kdWxlLmV4cG9ydHMgPSBfdHlwZW9mID0gZnVuY3Rpb24gX3R5cGVvZihvYmopIHtcbiAgICAgIHJldHVybiB0eXBlb2Ygb2JqO1xuICAgIH07XG4gIH0gZWxzZSB7XG4gICAgbW9kdWxlLmV4cG9ydHMgPSBfdHlwZW9mID0gZnVuY3Rpb24gX3R5cGVvZihvYmopIHtcbiAgICAgIHJldHVybiBvYmogJiYgdHlwZW9mIFN5bWJvbCA9PT0gXCJmdW5jdGlvblwiICYmIG9iai5jb25zdHJ1Y3RvciA9PT0gU3ltYm9sICYmIG9iaiAhPT0gU3ltYm9sLnByb3RvdHlwZSA/IFwic3ltYm9sXCIgOiB0eXBlb2Ygb2JqO1xuICAgIH07XG4gIH1cblxuICByZXR1cm4gX3R5cGVvZihvYmopO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IF90eXBlb2Y7IiwiPHNjcmlwdCB0eXBlPVwidGV4dC9qc3hcIj5cbmltcG9ydCB7IE9iamVjdFV0aWxzIH0gZnJvbSAnQG1vbm9ncmlkL2pzLXV0aWxzJ1xuaW1wb3J0IGZpbmQgZnJvbSAnbG9kYXNoL2ZpbmQnXG5cbmNvbnN0IGRlZmF1bHRTbGlkZXJTZXR0aW5ncyA9IHtcbiAgbWluOiAtMTAsXG4gIG1heDogMTAsXG4gIHN0ZXA6IDAuMDFcbn1cbi8qKlxuICogQSBzZWxmIGNvbnRhaW5lZCBpbnRlcmZhY2UgYWJsZSB0byBtb2RpZnkgYSB0YXJnZXQgb2JqZWN0XG4gKiBiYXNlZCBvbiBhIG1vZGVsIG9iamVjdC5cbiAqXG4gKiBJdCBhdXRvbWF0aWNhbGx5IGluZmVycyB0aGUgY29udHJvbCB0eXBlcyBhbmQgc3VwcG9ydHNcbiAqIGluZmluaXRlbHkgbmVzdGVkIHN1Yk9iamVjdHNcbiAqL1xuZXhwb3J0IGRlZmF1bHQge1xuICBuYW1lOiAnQ29udHJvbEtpdCcsXG5cbiAgZnVuY3Rpb25hbDogdHJ1ZSxcblxuICBwcm9wczoge1xuICAgIC8qKlxuICAgICAqIGEgc2V0dGluZ3MgbW9kZWwgb2JqZWN0IHdpdGggcHJvcGVydGllc1xuICAgICAqIHRvIGJlIG1vZGlmaWVkIGluIHRoZSB0YXJnZXQgb2JqZWN0LlxuICAgICAqXG4gICAgICogSXQgY2FuIGhhdmUgc3ViLW9iamVjdHMgYW5kIHN1Yi1wcm9wZXJ0aWVzLlxuICAgICAqL1xuICAgIG1vZGVsOiB7XG4gICAgICByZXF1aXJlZDogdHJ1ZVxuICAgIH0sXG4gICAgLyoqXG4gICAgICogVGhlIHRhcmdldCBvYmplY3QgdG8gbW9kaWZ5LlxuICAgICAqL1xuICAgIHRhcmdldDoge1xuICAgICAgcmVxdWlyZWQ6IGZhbHNlXG4gICAgfSxcbiAgICAvKipcbiAgICAgKiBBbiBhcnJheSBvZiBhZGRpdGlvbmFsIGNvbnRyb2xzIHRvIHVzZS5cbiAgICAgKiBUT0RPOiBEb2N1bWVudGF0aW9uXG4gICAgICovXG4gICAgYWRkaXRpb25hbENvbnRyb2xzOiB7XG4gICAgICB0eXBlOiBBcnJheSxcbiAgICAgIHJlcXVpcmVkOiBmYWxzZVxuICAgIH0sXG4gICAgLyoqXG4gICAgICogUm9vdCB0aXRsZSBkaXNwbGF5ZWRcbiAgICAgKi9cbiAgICB0aXRsZToge1xuICAgICAgdHlwZTogU3RyaW5nLFxuICAgICAgcmVxdWlyZWQ6IGZhbHNlLFxuICAgICAgZGVmYXVsdDogJ09wdGlvbnMnXG4gICAgfSxcbiAgICAvKipcbiAgICAgKiBPYmplY3QgY29udGFpbmluZyBhIGRlZmluaXRpb24gb2Ygc2V0dGluZ3NcbiAgICAgKiBUT0RPOiBEb2N1bWVudGF0aW9uXG4gICAgICovXG4gICAgc2V0dGluZ3M6IHtcbiAgICAgIHR5cGU6IE9iamVjdCxcbiAgICAgIHJlcXVpcmVkOiBmYWxzZSxcbiAgICAgIGRlZmF1bHQgKCkge1xuICAgICAgICByZXR1cm4ge31cbiAgICAgIH1cbiAgICB9XG4gIH0sXG5cbiAgcmVuZGVyIChoLCBjb250ZXh0KSB7XG4gICAgY29uc3Qgc2VudGVuY2VDYXNlID0gZnVuY3Rpb24gKHRleHQpIHtcbiAgICAgIHZhciByZXN1bHQgPSB0ZXh0LnJlcGxhY2UoLyhbQS1aXSg/IVtBLVpdKSkvZywgJyAkMScpXG4gICAgICByZXR1cm4gU3RyaW5nKHJlc3VsdC5jaGFyQXQoMCkudG9VcHBlckNhc2UoKSArIHJlc3VsdC5zbGljZSgxKSkudHJpbSgpXG4gICAgfVxuXG4gICAgY29uc3QgZ2V0TnVtYmVyU2xpZGVyID0gZnVuY3Rpb24gKGgsIHRhcmdldCwgcHJvcCwgcGF0aCkge1xuICAgICAgY29uc3QgdmFsID0gdGFyZ2V0W3Byb3BdXG5cbiAgICAgIGxldCBzZXR0aW5nXG4gICAgICBpZiAoY29udGV4dC5wcm9wcy5zZXR0aW5ncy5zbGlkZXJzKSB7XG4gICAgICAgIHNldHRpbmcgPSBmaW5kKGNvbnRleHQucHJvcHMuc2V0dGluZ3Muc2xpZGVycywgZnVuY3Rpb24gKG8pIHtcbiAgICAgICAgICByZXR1cm4gcGF0aC5tYXRjaChvLm1hdGNoKVxuICAgICAgICB9KVxuICAgICAgfVxuICAgICAgaWYgKCFzZXR0aW5nKSB7XG4gICAgICAgIHNldHRpbmcgPSBkZWZhdWx0U2xpZGVyU2V0dGluZ3NcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIChcbiAgICAgICAgPGRpdiBjbGFzcz1cIm51bWJlci1zbGlkZXItY29udGFpbmVyXCI+XG4gICAgICAgICAgPGlucHV0XG4gICAgICAgICAgICBpZD17cGF0aH1cbiAgICAgICAgICAgIGNsYXNzPVwibnVtYmVyLXNsaWRlclwiXG4gICAgICAgICAgICB0eXBlPVwicmFuZ2VcIlxuICAgICAgICAgICAgbWluPXtzZXR0aW5nLm1pbn1cbiAgICAgICAgICAgIG1heD17c2V0dGluZy5tYXh9XG4gICAgICAgICAgICBzdGVwPXtzZXR0aW5nLnN0ZXB9XG4gICAgICAgICAgICB2YWx1ZT17dmFsfVxuICAgICAgICAgICAgb24taW5wdXQ9e2Z1bmN0aW9uIChlKSB7IHRhcmdldFtwcm9wXSA9IHBhcnNlRmxvYXQoZS50YXJnZXQudmFsdWUpIH19XG4gICAgICAgICAgICBvbi1jaGFuZ2U9e2Z1bmN0aW9uIChlKSB7IHRhcmdldFtwcm9wXSA9IHBhcnNlRmxvYXQoZS50YXJnZXQudmFsdWUpIH19XG4gICAgICAgICAgLz5cbiAgICAgICAgICA8aW5wdXRcbiAgICAgICAgICAgIGNsYXNzPVwibnVtYmVyLWlucHV0XCJcbiAgICAgICAgICAgIHZhbHVlPXt2YWx9XG4gICAgICAgICAgICBzdGVwPXtzZXR0aW5nLnN0ZXB9XG4gICAgICAgICAgICB0eXBlPVwibnVtYmVyXCJcbiAgICAgICAgICAgIG9uLWNoYW5nZT17ZnVuY3Rpb24gKGUpIHsgdGFyZ2V0W3Byb3BdID0gcGFyc2VGbG9hdChlLnRhcmdldC52YWx1ZSkgfX1cbiAgICAgICAgICAgIG9uLWlucHV0PXtmdW5jdGlvbiAoZSkgeyB0YXJnZXRbcHJvcF0gPSBwYXJzZUZsb2F0KGUudGFyZ2V0LnZhbHVlKSB9fVxuICAgICAgICAgIC8+XG4gICAgICAgIDwvZGl2PlxuICAgICAgKSAvL1xuICAgIH1cbiAgICBjb25zdCBwYXJzZU1vZGVsID0gZnVuY3Rpb24gKGgsIG1vZGVsLCBvdXRwdXQsIHRpdGxlID0gJ09wdGlvbnMnLCB0YXJnZXQsIHBhdGggPSAnbW9kZWwnKSB7XG4gICAgICBjb25zdCBjaGlsZHJlbiA9IFtdXG4gICAgICBmb3IgKGNvbnN0IHByb3AgaW4gbW9kZWwpIHtcbiAgICAgICAgaWYgKHByb3AuaW5kZXhPZignXycpID09PSAwKSBjb250aW51ZSAvLyBza2lwIFwicHJpdmF0ZVwiIHByb3BzXG4gICAgICAgIHN3aXRjaCAodHlwZW9mIG1vZGVsW3Byb3BdKSB7XG4gICAgICAgICAgY2FzZSAnc3RyaW5nJzpcbiAgICAgICAgICAgIGNoaWxkcmVuLnB1c2goXG4gICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJpdGVtXCI+XG4gICAgICAgICAgICAgICAgPGxhYmVsIGZvcj17cGF0aCArICcuJyArIHByb3B9IGNsYXNzPVwiaXRlbS10aXRsZVwiPntzZW50ZW5jZUNhc2UocHJvcCl9PC9sYWJlbD5cbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiaXRlbS1jb250ZW50XCI+XG4gICAgICAgICAgICAgICAgICA8aW5wdXQgaWQ9e3BhdGggKyAnLicgKyBwcm9wfSB2YWx1ZT17dGFyZ2V0W3Byb3BdfSBvbi1jaGFuZ2U9eyhldmVudCkgPT4geyB0YXJnZXRbcHJvcF0gPSBldmVudC50YXJnZXQudmFsdWUgfX0gLz5cbiAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICApIC8vXG4gICAgICAgICAgICBicmVha1xuICAgICAgICAgIGNhc2UgJ2Jvb2xlYW4nOlxuICAgICAgICAgICAgY2hpbGRyZW4ucHVzaChcbiAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cIml0ZW1cIj5cbiAgICAgICAgICAgICAgICA8bGFiZWwgZm9yPXtwYXRoICsgJy4nICsgcHJvcH0gY2xhc3M9XCJpdGVtLXRpdGxlXCI+e3NlbnRlbmNlQ2FzZShwcm9wKX08L2xhYmVsPlxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJpdGVtLWNvbnRlbnRcIj5cbiAgICAgICAgICAgICAgICAgIDxpbnB1dCBpZD17cGF0aCArICcuJyArIHByb3B9IHR5cGU9XCJjaGVja2JveFwiIGNoZWNrZWQ9e3RhcmdldFtwcm9wXX0gb24tY2hhbmdlPXsoZXZlbnQpID0+IHsgdGFyZ2V0W3Byb3BdID0gZXZlbnQudGFyZ2V0LmNoZWNrZWQgfX0gLz5cbiAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICApIC8vXG4gICAgICAgICAgICBicmVha1xuICAgICAgICAgIGNhc2UgJ251bWJlcic6XG4gICAgICAgICAgICBjaGlsZHJlbi5wdXNoKFxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiaXRlbVwiPlxuICAgICAgICAgICAgICAgIDxsYWJlbCBmb3I9e3BhdGggKyAnLicgKyBwcm9wfSBjbGFzcz1cIml0ZW0tdGl0bGVcIj57c2VudGVuY2VDYXNlKHByb3ApfTwvbGFiZWw+XG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cIml0ZW0tY29udGVudFwiPntnZXROdW1iZXJTbGlkZXIoaCwgdGFyZ2V0LCBwcm9wLCBwYXRoICsgJy4nICsgcHJvcCl9PC9kaXY+XG4gICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgKSAvL1xuICAgICAgICAgICAgYnJlYWtcbiAgICAgICAgICBjYXNlICdmdW5jdGlvbic6XG4gICAgICAgICAgICBjaGlsZHJlbi5wdXNoKFxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiaXRlbVwiPlxuICAgICAgICAgICAgICAgIDxsYWJlbCBmb3I9e3BhdGggKyAnLicgKyBwcm9wfSBjbGFzcz1cIml0ZW0tdGl0bGVcIj5FeGVjdXRlPC9sYWJlbD5cbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiaXRlbS1jb250ZW50XCI+XG4gICAgICAgICAgICAgICAgICA8YnV0dG9uIGlkPXtwYXRoICsgJy4nICsgcHJvcH0gb24tY2xpY2s9e3RhcmdldFtwcm9wXSA/IHRhcmdldFtwcm9wXSA6IG1vZGVsW3Byb3BdfT57c2VudGVuY2VDYXNlKHByb3ApfTwvYnV0dG9uPlxuICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICkgLy9cbiAgICAgICAgICAgIGJyZWFrXG4gICAgICAgICAgY2FzZSAnb2JqZWN0JzpcbiAgICAgICAgICAgIGlmIChPYmplY3RVdGlscy5pc1BsYWluT2JqZWN0KG1vZGVsW3Byb3BdKSkge1xuICAgICAgICAgICAgICBwYXJzZU1vZGVsKGgsIG1vZGVsW3Byb3BdLCBjaGlsZHJlbiwgcHJvcCwgdGFyZ2V0W3Byb3BdLCBwYXRoICsgJy4nICsgcHJvcClcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgIGlmIChjb250ZXh0LnByb3BzLmFkZGl0aW9uYWxDb250cm9scyAmJiBjb250ZXh0LnByb3BzLmFkZGl0aW9uYWxDb250cm9scy5sZW5ndGggPiAwKSB7XG4gICAgICAgICAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCBjb250ZXh0LnByb3BzLmFkZGl0aW9uYWxDb250cm9scy5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgICAgICAgY29uc3QgY29udHJvbHMgPSBjb250ZXh0LnByb3BzLmFkZGl0aW9uYWxDb250cm9sc1tpXVxuICAgICAgICAgICAgICAgICAgZm9yIChsZXQgaiA9IDA7IGogPCBjb250cm9scy5sZW5ndGg7IGorKykge1xuICAgICAgICAgICAgICAgICAgICBpZiAoY29udHJvbHNbal0ubWF0Y2gobW9kZWxbcHJvcF0pKSB7XG4gICAgICAgICAgICAgICAgICAgICAgY2hpbGRyZW4ucHVzaChcbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJpdGVtXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgIDxsYWJlbCBmb3I9e3BhdGggKyAnLicgKyBwcm9wfSBjbGFzcz1cIml0ZW0tdGl0bGVcIj57c2VudGVuY2VDYXNlKHByb3ApfTwvbGFiZWw+XG4gICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJpdGVtLWNvbnRlbnRcIj57Y29udHJvbHNbal0uZ2V0Q29udHJvbChoLCB0YXJnZXQsIHByb3AsIHBhdGggKyAnLicgKyBwcm9wKX08L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICkgLy9cbiAgICAgICAgICAgICAgICAgICAgICBicmVha1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBicmVha1xuICAgICAgICB9XG4gICAgICB9XG4gICAgICBvdXRwdXQucHVzaChcbiAgICAgICAgPGRpdiBjbGFzcz1cIndyYXAtY29sbGFic2libGUgd3JhcC1jb2xsYWJzaWJsZS1wYW5lbFwiPlxuICAgICAgICAgIDxpbnB1dCBpZD17cGF0aH0gY2xhc3M9XCJ0b2dnbGVcIiB0eXBlPVwiY2hlY2tib3hcIiAvPlxuICAgICAgICAgIDxsYWJlbCBmb3I9e3BhdGh9IGNsYXNzPVwibGJsLXRvZ2dsZSBsYmwtdG9nZ2xlLXBhbmVsXCI+e3NlbnRlbmNlQ2FzZSh0aXRsZSl9PC9sYWJlbD5cbiAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sbGFwc2libGUtY29udGVudCBjb2xsYXBzaWJsZS1jb250ZW50LXBhbmVsXCI+XG4gICAgICAgICAgICB7Y2hpbGRyZW59XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgIDwvZGl2PlxuICAgICAgKSAvL1xuICAgIH1cbiAgICBjb25zdCBlbGVtZW50cyA9IFtdXG4gICAgcGFyc2VNb2RlbChoLCBjb250ZXh0LnByb3BzLm1vZGVsLCBlbGVtZW50cywgY29udGV4dC5wcm9wcy50aXRsZSwgY29udGV4dC5wcm9wcy50YXJnZXQgPyBjb250ZXh0LnByb3BzLnRhcmdldCA6IGNvbnRleHQucHJvcHMubW9kZWwsIGNvbnRleHQucHJvcHMudGFyZ2V0ID8gJ3RhcmdldCcgOiAnbW9kZWwnKVxuICAgIHJldHVybiA8ZGl2IGNsYXNzPVwiY29udHJvbC1wYW5lbFwiPntlbGVtZW50c308L2Rpdj5cbiAgfVxufVxuPC9zY3JpcHQ+XG5cbjxzdHlsZSBsYW5nPVwic2Nzc1wiPlxuJGMtcHJpbWFyeTogIzA1MWU0MjtcbiRjLXByaW1hcnktdmFyaWF0aW9uOiAjYzNjZWRmO1xuXG4kYy1zZWNvbmRhcnk6ICMwODEyMjA7XG4kYy1zZWNvbmRhcnktdmFyaWF0aW9uOiAjNjg4Y2MyO1xuXG4uY29udHJvbC1wYW5lbCB7XG4gIGJhY2tncm91bmQ6ICMwMDA7XG4gIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XG4gIGNvbG9yOiAkYy1wcmltYXJ5LXZhcmlhdGlvbjtcbiAgZm9udC1mYW1pbHk6IG1vbm9zcGFjZTtcblxuICBmb250LXNpemU6IDExcHg7XG4gIGxpbmUtaGVpZ2h0OiAxO1xuICBtYXgtaGVpZ2h0OiAxMDAlO1xuICBvdmVyZmxvdy14OiBoaWRkZW47XG4gIG92ZXJmbG93LXk6IGF1dG87XG5cbiAgcG9zaXRpb246IGFic29sdXRlO1xuICByaWdodDogMDtcbiAgdG9wOiAwO1xuICB3aWR0aDogMzcwcHg7XG5cbiAgLypcbiAgICBjb2xsYXBzaWJsZSBzdHlsZVxuICAqL1xuICAud3JhcC1jb2xsYWJzaWJsZSB7XG4gICAgYm94LXNpemluZzogYm9yZGVyLWJveDtcbiAgfVxuXG4gIC53cmFwLWNvbGxhYnNpYmxlID4gaW5wdXRbdHlwZT0nY2hlY2tib3gnXSB7XG4gICAgZGlzcGxheTogbm9uZTtcbiAgfVxuXG4gIGxhYmVsIHtcbiAgICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xuICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgfVxuXG4gIC5jb2xsYXBzaWJsZS1jb250ZW50IHtcbiAgICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xuICAgIGRpc3BsYXk6IG5vbmU7XG4gICAgaGVpZ2h0OiAwO1xuICAgIG92ZXJmbG93OiBoaWRkZW47XG4gIH1cbiAgLyogc3R5bGVsaW50LWRpc2FibGUgKi9cbiAgLnRvZ2dsZTpjaGVja2VkICsgLmxibC10b2dnbGUgKyAuY29sbGFwc2libGUtY29udGVudCB7XG4gICAgZGlzcGxheTogYmxvY2s7XG4gICAgaGVpZ2h0OiBhdXRvO1xuICB9XG4gIC8qIHN0eWxlbGludC1lbmFibGUgKi9cblxuICAvKlxuICAgIGNvbGxhcHNpYmxlIHBhbmVsIHN0eWxlXG4gICovXG4gIC53cmFwLWNvbGxhYnNpYmxlLXBhbmVsIHtcbiAgICBtYXJnaW4tYm90dG9tOiAycHg7XG4gICAgbWFyZ2luLXRvcDogMnB4O1xuICAgIHBhZGRpbmctbGVmdDogNXB4O1xuICAgIHBhZGRpbmctcmlnaHQ6IDVweDtcbiAgICB3aWR0aDogMTAwJTtcbiAgfVxuXG4gIC5sYmwtdG9nZ2xlLXBhbmVsIHtcbiAgICBiYWNrZ3JvdW5kOiAkYy1wcmltYXJ5O1xuICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCByZ2JhKCRjLXNlY29uZGFyeS12YXJpYXRpb24sIDAuMjUpO1xuICAgIGJvcmRlci1sZWZ0OiAxcHggc29saWQgcmdiYSgkYy1zZWNvbmRhcnktdmFyaWF0aW9uLCAwLjI1KTtcbiAgICBib3JkZXItcmFkaXVzOiAycHg7XG4gICAgYm9yZGVyLXJpZ2h0OiAxcHggc29saWQgcmdiYSgkYy1zZWNvbmRhcnktdmFyaWF0aW9uLCAwLjI1KTtcbiAgICBib3JkZXItdG9wOiAxcHggc29saWQgcmdiYSgkYy1zZWNvbmRhcnktdmFyaWF0aW9uLCAwLjI1KTtcbiAgICBjb2xvcjogJGMtcHJpbWFyeS12YXJpYXRpb247XG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgcGFkZGluZzogNHB4O1xuXG4gICAgJjo6YmVmb3JlIHtcbiAgICAgIGJvcmRlci1ib3R0b206IDVweCBzb2xpZCB0cmFuc3BhcmVudDtcbiAgICAgIGJvcmRlci1sZWZ0OiA1cHggc29saWQgY3VycmVudENvbG9yO1xuXG4gICAgICBib3JkZXItdG9wOiA1cHggc29saWQgdHJhbnNwYXJlbnQ7XG4gICAgICBjb250ZW50OiAnICc7XG4gICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgICBtYXJnaW4tcmlnaHQ6IDAuN3JlbTtcbiAgICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlWSgtMnB4KTtcblxuICAgICAgdHJhbnNpdGlvbjogdHJhbnNmb3JtIDAuMnMgZWFzZS1vdXQ7XG5cbiAgICAgIHZlcnRpY2FsLWFsaWduOiBtaWRkbGU7XG4gICAgfVxuICB9XG5cbiAgLmNvbGxhcHNpYmxlLWNvbnRlbnQtcGFuZWwge1xuICAgIGJhY2tncm91bmQ6IHJnYmEoJGMtc2Vjb25kYXJ5LCAwLjQ1KTtcbiAgICBib3JkZXItYm90dG9tLWxlZnQtcmFkaXVzOiAycHg7XG4gICAgYm9yZGVyLWJvdHRvbS1yaWdodC1yYWRpdXM6IDJweDtcbiAgICB3aWR0aDogMTAwJTtcbiAgICAvLyBwYWRkaW5nLWJvdHRvbTogMnB4O1xuICB9XG4gIC8qIHN0eWxlbGludC1kaXNhYmxlICovXG4gIC50b2dnbGU6Y2hlY2tlZCArIC5sYmwtdG9nZ2xlLXBhbmVsICsgLmNvbGxhcHNpYmxlLWNvbnRlbnQtcGFuZWwge1xuICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCByZ2JhKCRjLXNlY29uZGFyeS12YXJpYXRpb24sIDAuMjUpO1xuICAgIGJvcmRlci1sZWZ0OiAxcHggc29saWQgcmdiYSgkYy1zZWNvbmRhcnktdmFyaWF0aW9uLCAwLjI1KTtcbiAgICBib3JkZXItcmlnaHQ6IDFweCBzb2xpZCByZ2JhKCRjLXNlY29uZGFyeS12YXJpYXRpb24sIDAuMjUpO1xuICB9XG4gIC8qIHN0eWxlbGludC1lbmFibGUgKi9cblxuICAudG9nZ2xlOmNoZWNrZWQgKyAubGJsLXRvZ2dsZS1wYW5lbCB7XG4gICAgYm9yZGVyLWJvdHRvbTogMDtcbiAgICBib3JkZXItYm90dG9tLWxlZnQtcmFkaXVzOiAwO1xuICAgIGJvcmRlci1ib3R0b20tcmlnaHQtcmFkaXVzOiAwO1xuICB9XG5cbiAgLnRvZ2dsZTpjaGVja2VkICsgLmxibC10b2dnbGUtcGFuZWw6OmJlZm9yZSB7XG4gICAgdHJhbnNmb3JtOiByb3RhdGUoOTBkZWcpIHRyYW5zbGF0ZVgoLTNweCk7XG4gIH1cblxuICAudmMtY2hyb21lLWJvZHksXG4gIC52Yy1jaHJvbWUge1xuICAgIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xuICAgIGJveC1zaGFkb3c6IG5vbmU7XG4gIH1cblxuICAvKlxuICAgIGl0ZW1zIHN0eWxlXG4gICovXG4gIC5pdGVtIHtcbiAgICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xuICAgIHBhZGRpbmctYm90dG9tOiAxcHg7XG4gICAgcGFkZGluZy10b3A6IDFweDtcbiAgICB3aWR0aDogMTAwJTtcbiAgfVxuXG4gIC5zZXQtYm9yZGVyLWNvbG9yIHtcbiAgICBib3JkZXI6IDFweCBzb2xpZCAkYy1zZWNvbmRhcnktdmFyaWF0aW9uO1xuICB9XG5cbiAgLml0ZW0tdGl0bGUsXG4gIC5pdGVtLWNvbnRlbnQge1xuICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIHZlcnRpY2FsLWFsaWduOiBtaWRkbGU7XG4gIH1cblxuICAuaXRlbS1jb250ZW50IHtcbiAgICBwYWRkaW5nOiAwIDVweDtcbiAgICB3aWR0aDogNjUlO1xuICB9XG5cbiAgLml0ZW0tdGl0bGUge1xuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xuICAgIHdpZHRoOiAzNSU7XG4gIH1cblxuICAvKlxuICAgIE51bWJlciBzbGlkZXIgc3R5bGVcbiAgKi9cbiAgLm51bWJlci1zbGlkZXItY29udGFpbmVyIHtcbiAgICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICB3aGl0ZS1zcGFjZTogbm93cmFwO1xuICAgIHdpZHRoOiAxMDAlO1xuXG4gICAgLm51bWJlci1zbGlkZXIsXG4gICAgLm51bWJlci1pbnB1dCB7XG4gICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgfVxuXG4gICAgLm51bWJlci1zbGlkZXIge1xuICAgICAgbWFyZ2luOiAwO1xuICAgICAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xuICAgICAgcGFkZGluZzogMDtcbiAgICAgIHdpZHRoOiA3MCU7XG4gICAgfVxuXG4gICAgLm51bWJlci1pbnB1dCB7XG4gICAgICBmb250LXNpemU6IDExcHg7XG4gICAgICBoZWlnaHQ6IDExcHg7XG4gICAgICB0b3A6IC00cHg7XG4gICAgICB3aWR0aDogMjAlO1xuICAgIH1cbiAgfVxuXG4gIC8qXG4gICAgQnV0dG9uIGlucHV0IHN0eWxlXG4gICovXG4gIGJ1dHRvbiB7XG4gICAgYmFja2dyb3VuZDogJGMtcHJpbWFyeTtcbiAgICBib3JkZXI6IDFweCBzb2xpZCByZ2JhKCRjLXNlY29uZGFyeS12YXJpYXRpb24sIDAuMjUpO1xuICAgIGJvcmRlci1yYWRpdXM6IDJweDtcbiAgICBjb2xvcjogJGMtcHJpbWFyeS12YXJpYXRpb247XG4gICAgY3Vyc29yOiBwb2ludGVyO1xuICAgIGZvbnQtc2l6ZTogMTFweDtcbiAgICBwYWRkaW5nOiA1cHggMTBweDtcbiAgfVxuICAvKiBzdHlsZWxpbnQtZGlzYWJsZSAqL1xuICBpbnB1dCxcbiAgLnZjLWlucHV0X19pbnB1dCB7XG4gICAgYmFja2dyb3VuZDogJGMtc2Vjb25kYXJ5O1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICRjLXNlY29uZGFyeS12YXJpYXRpb247XG4gICAgY29sb3I6ICRjLXByaW1hcnktdmFyaWF0aW9uO1xuICAgIC8qIHN0eWxlbGludC1lbmFibGUgKi9cbiAgICAvLyBiYWNrZ3JvdW5kOiBub25lO1xuICAgIGZvbnQtZmFtaWx5OiBtb25vc3BhY2U7XG4gICAgcGFkZGluZzogMXB4O1xuICB9XG5cbiAgaW5wdXRbdHlwZT1jaGVja2JveF0ge1xuICAgIGFwcGVhcmFuY2U6IG5vbmU7XG4gICAgYmFja2dyb3VuZDogJGMtc2Vjb25kYXJ5O1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICRjLXNlY29uZGFyeS12YXJpYXRpb247XG4gICAgY3Vyc29yOiBwb2ludGVyO1xuICAgIGhlaWdodDogMTVweDtcbiAgICBtYXJnaW46IDA7XG4gICAgd2lkdGg6IDE1cHg7XG5cbiAgICAmOjphZnRlciB7XG4gICAgICBiYWNrZ3JvdW5kOiAkYy1wcmltYXJ5LXZhcmlhdGlvbjtcbiAgICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XG4gICAgICBjb250ZW50OiAnJztcbiAgICAgIGRpc3BsYXk6IG5vbmU7XG4gICAgICBoZWlnaHQ6IDEwMCU7XG4gICAgICB3aWR0aDogMTAwJTtcbiAgICB9XG5cbiAgICAmOmNoZWNrZWQge1xuICAgICAgJjo6YWZ0ZXIge1xuICAgICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICAvKlxuICAgIFJhbmdlIGlucHV0IHN0eWxlXG4gICovXG4gIGlucHV0W3R5cGU9cmFuZ2VdIHtcbiAgICBhcHBlYXJhbmNlOiBub25lO1xuICAgIGJhY2tncm91bmQ6IG5vbmU7XG4gICAgYm9yZGVyOiAwO1xuICAgIG1hcmdpbjogMThweCAwO1xuXG4gICAgJjo6LXdlYmtpdC1zbGlkZXItcnVubmFibGUtdHJhY2sge1xuICAgICAgYmFja2dyb3VuZDogJGMtc2Vjb25kYXJ5O1xuICAgICAgYm9yZGVyOiAxcHggc29saWQgJGMtc2Vjb25kYXJ5LXZhcmlhdGlvbjtcbiAgICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICAgIGhlaWdodDogMTVweDtcbiAgICB9XG5cbiAgICAmOjotbW96LXJhbmdlLXRyYWNrIHtcbiAgICAgIGJhY2tncm91bmQ6ICRjLXNlY29uZGFyeTtcbiAgICAgIGJvcmRlcjogMXB4IHNvbGlkICRjLXNlY29uZGFyeS12YXJpYXRpb247XG4gICAgICBjdXJzb3I6IHBvaW50ZXI7XG4gICAgICBoZWlnaHQ6IDE1cHg7XG4gICAgfVxuXG4gICAgJjo6LW1zLXRyYWNrIHtcbiAgICAgIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xuICAgICAgYm9yZGVyOiAwO1xuICAgICAgY29sb3I6IHRyYW5zcGFyZW50O1xuICAgICAgY3Vyc29yOiBwb2ludGVyO1xuICAgICAgaGVpZ2h0OiAxNXB4O1xuICAgIH1cblxuICAgICY6Oi13ZWJraXQtc2xpZGVyLXRodW1iIHtcbiAgICAgIGFwcGVhcmFuY2U6IG5vbmU7XG4gICAgICBiYWNrZ3JvdW5kOiAkYy1wcmltYXJ5O1xuICAgICAgLy8gYm9yZGVyOiAwO1xuICAgICAgYm9yZGVyOiAxcHggc29saWQgJGMtcHJpbWFyeS12YXJpYXRpb247XG4gICAgICBib3JkZXItcmFkaXVzOiAxcHg7XG4gICAgICBjdXJzb3I6IHBvaW50ZXI7XG4gICAgICBoZWlnaHQ6IDExcHg7XG4gICAgICBtYXJnaW4tdG9wOiAxcHg7XG4gICAgICB3aWR0aDogN3B4O1xuICAgIH1cblxuICAgICY6Oi1tb3otcmFuZ2UtdGh1bWIge1xuICAgICAgYmFja2dyb3VuZDogJGMtcHJpbWFyeTtcbiAgICAgIC8vIGJvcmRlcjogMDtcbiAgICAgIGJvcmRlcjogMXB4IHNvbGlkICRjLXByaW1hcnktdmFyaWF0aW9uO1xuICAgICAgYm9yZGVyLXJhZGl1czogMXB4O1xuICAgICAgY3Vyc29yOiBwb2ludGVyO1xuICAgICAgaGVpZ2h0OiAxMXB4O1xuICAgICAgd2lkdGg6IDdweDtcbiAgICB9XG5cbiAgICAmOjotbXMtdGh1bWIge1xuICAgICAgYmFja2dyb3VuZDogJGMtcHJpbWFyeTtcbiAgICAgIC8vIGJvcmRlcjogMDtcbiAgICAgIGJvcmRlcjogMXB4IHNvbGlkICRjLXByaW1hcnktdmFyaWF0aW9uO1xuICAgICAgYm9yZGVyLXJhZGl1czogMXB4O1xuICAgICAgY3Vyc29yOiBwb2ludGVyO1xuICAgICAgaGVpZ2h0OiAxMXB4O1xuICAgICAgd2lkdGg6IDdweDtcbiAgICB9XG5cbiAgICAmOjotbXMtZmlsbC1sb3dlciB7XG4gICAgICBiYWNrZ3JvdW5kOiAkYy1zZWNvbmRhcnk7XG4gICAgICBib3JkZXI6IDFweCBzb2xpZCAkYy1zZWNvbmRhcnktdmFyaWF0aW9uO1xuICAgIH1cblxuICAgICY6Oi1tcy1maWxsLXVwcGVyIHtcbiAgICAgIGJhY2tncm91bmQ6ICRjLXNlY29uZGFyeTtcbiAgICAgIGJvcmRlcjogMXB4IHNvbGlkICRjLXNlY29uZGFyeS12YXJpYXRpb247XG4gICAgfVxuXG4gICAgJjpmb2N1cyB7XG4gICAgICBvdXRsaW5lOiBub25lO1xuXG4gICAgICAmOjotd2Via2l0LXNsaWRlci1ydW5uYWJsZS10cmFjayB7XG4gICAgICAgIGJhY2tncm91bmQ6ICRjLXByaW1hcnk7XG4gICAgICB9XG5cbiAgICAgICY6Oi1tcy1maWxsLWxvd2VyIHtcbiAgICAgICAgYmFja2dyb3VuZDogJGMtcHJpbWFyeTtcbiAgICAgIH1cblxuICAgICAgJjo6LW1zLWZpbGwtdXBwZXIge1xuICAgICAgICBiYWNrZ3JvdW5kOiAkYy1wcmltYXJ5O1xuICAgICAgfVxuICAgIH1cbiAgfVxufVxuXG48L3N0eWxlPlxuIiwiLy8gSW1wb3J0c1xudmFyIF9fX0NTU19MT0FERVJfQVBJX0lNUE9SVF9fXyA9IHJlcXVpcmUoXCIuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9kaXN0L3J1bnRpbWUvYXBpLmpzXCIpO1xuZXhwb3J0cyA9IF9fX0NTU19MT0FERVJfQVBJX0lNUE9SVF9fXyhmYWxzZSk7XG4vLyBNb2R1bGVcbmV4cG9ydHMucHVzaChbbW9kdWxlLmlkLCBcIi5jb250cm9sLXBhbmVsIHtcXG4gIGJhY2tncm91bmQ6ICMwMDA7XFxuICAtd2Via2l0LWJveC1zaXppbmc6IGJvcmRlci1ib3g7XFxuICAgICAgICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XFxuICBjb2xvcjogI2MzY2VkZjtcXG4gIGZvbnQtZmFtaWx5OiBtb25vc3BhY2U7XFxuICBmb250LXNpemU6IDExcHg7XFxuICBsaW5lLWhlaWdodDogMTtcXG4gIG1heC1oZWlnaHQ6IDEwMCU7XFxuICBvdmVyZmxvdy14OiBoaWRkZW47XFxuICBvdmVyZmxvdy15OiBhdXRvO1xcbiAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgcmlnaHQ6IDA7XFxuICB0b3A6IDA7XFxuICB3aWR0aDogMzcwcHg7XFxuICAvKlxcbiAgICBjb2xsYXBzaWJsZSBzdHlsZVxcbiAgKi9cXG4gIC8qIHN0eWxlbGludC1kaXNhYmxlICovXFxuICAvKiBzdHlsZWxpbnQtZW5hYmxlICovXFxuICAvKlxcbiAgICBjb2xsYXBzaWJsZSBwYW5lbCBzdHlsZVxcbiAgKi9cXG4gIC8qIHN0eWxlbGludC1kaXNhYmxlICovXFxuICAvKiBzdHlsZWxpbnQtZW5hYmxlICovXFxuICAvKlxcbiAgICBpdGVtcyBzdHlsZVxcbiAgKi9cXG4gIC8qXFxuICAgIE51bWJlciBzbGlkZXIgc3R5bGVcXG4gICovXFxuICAvKlxcbiAgICBCdXR0b24gaW5wdXQgc3R5bGVcXG4gICovXFxuICAvKiBzdHlsZWxpbnQtZGlzYWJsZSAqL1xcbiAgLypcXG4gICAgUmFuZ2UgaW5wdXQgc3R5bGVcXG4gICovXFxufVxcbi5jb250cm9sLXBhbmVsIC53cmFwLWNvbGxhYnNpYmxlIHtcXG4gIC13ZWJraXQtYm94LXNpemluZzogYm9yZGVyLWJveDtcXG4gICAgICAgICAgYm94LXNpemluZzogYm9yZGVyLWJveDtcXG59XFxuLmNvbnRyb2wtcGFuZWwgLndyYXAtY29sbGFic2libGUgPiBpbnB1dFt0eXBlPWNoZWNrYm94XSB7XFxuICBkaXNwbGF5OiBub25lO1xcbn1cXG4uY29udHJvbC1wYW5lbCBsYWJlbCB7XFxuICAtd2Via2l0LWJveC1zaXppbmc6IGJvcmRlci1ib3g7XFxuICAgICAgICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XFxuICBjdXJzb3I6IHBvaW50ZXI7XFxuICBkaXNwbGF5OiBibG9jaztcXG59XFxuLmNvbnRyb2wtcGFuZWwgLmNvbGxhcHNpYmxlLWNvbnRlbnQge1xcbiAgLXdlYmtpdC1ib3gtc2l6aW5nOiBib3JkZXItYm94O1xcbiAgICAgICAgICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xcbiAgZGlzcGxheTogbm9uZTtcXG4gIGhlaWdodDogMDtcXG4gIG92ZXJmbG93OiBoaWRkZW47XFxufVxcbi5jb250cm9sLXBhbmVsIC50b2dnbGU6Y2hlY2tlZCArIC5sYmwtdG9nZ2xlICsgLmNvbGxhcHNpYmxlLWNvbnRlbnQge1xcbiAgZGlzcGxheTogYmxvY2s7XFxuICBoZWlnaHQ6IGF1dG87XFxufVxcbi5jb250cm9sLXBhbmVsIC53cmFwLWNvbGxhYnNpYmxlLXBhbmVsIHtcXG4gIG1hcmdpbi1ib3R0b206IDJweDtcXG4gIG1hcmdpbi10b3A6IDJweDtcXG4gIHBhZGRpbmctbGVmdDogNXB4O1xcbiAgcGFkZGluZy1yaWdodDogNXB4O1xcbiAgd2lkdGg6IDEwMCU7XFxufVxcbi5jb250cm9sLXBhbmVsIC5sYmwtdG9nZ2xlLXBhbmVsIHtcXG4gIGJhY2tncm91bmQ6ICMwNTFlNDI7XFxuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgcmdiYSgxMDQsIDE0MCwgMTk0LCAwLjI1KTtcXG4gIGJvcmRlci1sZWZ0OiAxcHggc29saWQgcmdiYSgxMDQsIDE0MCwgMTk0LCAwLjI1KTtcXG4gIGJvcmRlci1yYWRpdXM6IDJweDtcXG4gIGJvcmRlci1yaWdodDogMXB4IHNvbGlkIHJnYmEoMTA0LCAxNDAsIDE5NCwgMC4yNSk7XFxuICBib3JkZXItdG9wOiAxcHggc29saWQgcmdiYSgxMDQsIDE0MCwgMTk0LCAwLjI1KTtcXG4gIGNvbG9yOiAjYzNjZWRmO1xcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XFxuICBwYWRkaW5nOiA0cHg7XFxufVxcbi5jb250cm9sLXBhbmVsIC5sYmwtdG9nZ2xlLXBhbmVsOjpiZWZvcmUge1xcbiAgYm9yZGVyLWJvdHRvbTogNXB4IHNvbGlkIHRyYW5zcGFyZW50O1xcbiAgYm9yZGVyLWxlZnQ6IDVweCBzb2xpZCBjdXJyZW50Q29sb3I7XFxuICBib3JkZXItdG9wOiA1cHggc29saWQgdHJhbnNwYXJlbnQ7XFxuICBjb250ZW50OiBcXFwiIFxcXCI7XFxuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XFxuICBtYXJnaW4tcmlnaHQ6IDAuN3JlbTtcXG4gIC13ZWJraXQtdHJhbnNmb3JtOiB0cmFuc2xhdGVZKC0ycHgpO1xcbiAgICAgICAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVkoLTJweCk7XFxuICAtd2Via2l0LXRyYW5zaXRpb246IC13ZWJraXQtdHJhbnNmb3JtIDAuMnMgZWFzZS1vdXQ7XFxuICB0cmFuc2l0aW9uOiAtd2Via2l0LXRyYW5zZm9ybSAwLjJzIGVhc2Utb3V0O1xcbiAgdHJhbnNpdGlvbjogdHJhbnNmb3JtIDAuMnMgZWFzZS1vdXQ7XFxuICB0cmFuc2l0aW9uOiB0cmFuc2Zvcm0gMC4ycyBlYXNlLW91dCwgLXdlYmtpdC10cmFuc2Zvcm0gMC4ycyBlYXNlLW91dDtcXG4gIHZlcnRpY2FsLWFsaWduOiBtaWRkbGU7XFxufVxcbi5jb250cm9sLXBhbmVsIC5jb2xsYXBzaWJsZS1jb250ZW50LXBhbmVsIHtcXG4gIGJhY2tncm91bmQ6IHJnYmEoOCwgMTgsIDMyLCAwLjQ1KTtcXG4gIGJvcmRlci1ib3R0b20tbGVmdC1yYWRpdXM6IDJweDtcXG4gIGJvcmRlci1ib3R0b20tcmlnaHQtcmFkaXVzOiAycHg7XFxuICB3aWR0aDogMTAwJTtcXG59XFxuLmNvbnRyb2wtcGFuZWwgLnRvZ2dsZTpjaGVja2VkICsgLmxibC10b2dnbGUtcGFuZWwgKyAuY29sbGFwc2libGUtY29udGVudC1wYW5lbCB7XFxuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgcmdiYSgxMDQsIDE0MCwgMTk0LCAwLjI1KTtcXG4gIGJvcmRlci1sZWZ0OiAxcHggc29saWQgcmdiYSgxMDQsIDE0MCwgMTk0LCAwLjI1KTtcXG4gIGJvcmRlci1yaWdodDogMXB4IHNvbGlkIHJnYmEoMTA0LCAxNDAsIDE5NCwgMC4yNSk7XFxufVxcbi5jb250cm9sLXBhbmVsIC50b2dnbGU6Y2hlY2tlZCArIC5sYmwtdG9nZ2xlLXBhbmVsIHtcXG4gIGJvcmRlci1ib3R0b206IDA7XFxuICBib3JkZXItYm90dG9tLWxlZnQtcmFkaXVzOiAwO1xcbiAgYm9yZGVyLWJvdHRvbS1yaWdodC1yYWRpdXM6IDA7XFxufVxcbi5jb250cm9sLXBhbmVsIC50b2dnbGU6Y2hlY2tlZCArIC5sYmwtdG9nZ2xlLXBhbmVsOjpiZWZvcmUge1xcbiAgLXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSg5MGRlZykgdHJhbnNsYXRlWCgtM3B4KTtcXG4gICAgICAgICAgdHJhbnNmb3JtOiByb3RhdGUoOTBkZWcpIHRyYW5zbGF0ZVgoLTNweCk7XFxufVxcbi5jb250cm9sLXBhbmVsIC52Yy1jaHJvbWUtYm9keSxcXG4uY29udHJvbC1wYW5lbCAudmMtY2hyb21lIHtcXG4gIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiBub25lO1xcbiAgICAgICAgICBib3gtc2hhZG93OiBub25lO1xcbn1cXG4uY29udHJvbC1wYW5lbCAuaXRlbSB7XFxuICAtd2Via2l0LWJveC1zaXppbmc6IGJvcmRlci1ib3g7XFxuICAgICAgICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XFxuICBwYWRkaW5nLWJvdHRvbTogMXB4O1xcbiAgcGFkZGluZy10b3A6IDFweDtcXG4gIHdpZHRoOiAxMDAlO1xcbn1cXG4uY29udHJvbC1wYW5lbCAuc2V0LWJvcmRlci1jb2xvciB7XFxuICBib3JkZXI6IDFweCBzb2xpZCAjNjg4Y2MyO1xcbn1cXG4uY29udHJvbC1wYW5lbCAuaXRlbS10aXRsZSxcXG4uY29udHJvbC1wYW5lbCAuaXRlbS1jb250ZW50IHtcXG4gIC13ZWJraXQtYm94LXNpemluZzogYm9yZGVyLWJveDtcXG4gICAgICAgICAgYm94LXNpemluZzogYm9yZGVyLWJveDtcXG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcXG4gIHZlcnRpY2FsLWFsaWduOiBtaWRkbGU7XFxufVxcbi5jb250cm9sLXBhbmVsIC5pdGVtLWNvbnRlbnQge1xcbiAgcGFkZGluZzogMCA1cHg7XFxuICB3aWR0aDogNjUlO1xcbn1cXG4uY29udHJvbC1wYW5lbCAuaXRlbS10aXRsZSB7XFxuICB0ZXh0LWFsaWduOiByaWdodDtcXG4gIHdpZHRoOiAzNSU7XFxufVxcbi5jb250cm9sLXBhbmVsIC5udW1iZXItc2xpZGVyLWNvbnRhaW5lciB7XFxuICAtd2Via2l0LWJveC1zaXppbmc6IGJvcmRlci1ib3g7XFxuICAgICAgICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XFxuICBwb3NpdGlvbjogcmVsYXRpdmU7XFxuICB3aGl0ZS1zcGFjZTogbm93cmFwO1xcbiAgd2lkdGg6IDEwMCU7XFxufVxcbi5jb250cm9sLXBhbmVsIC5udW1iZXItc2xpZGVyLWNvbnRhaW5lciAubnVtYmVyLXNsaWRlcixcXG4uY29udHJvbC1wYW5lbCAubnVtYmVyLXNsaWRlci1jb250YWluZXIgLm51bWJlci1pbnB1dCB7XFxuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XFxuICBwb3NpdGlvbjogcmVsYXRpdmU7XFxufVxcbi5jb250cm9sLXBhbmVsIC5udW1iZXItc2xpZGVyLWNvbnRhaW5lciAubnVtYmVyLXNsaWRlciB7XFxuICBtYXJnaW46IDA7XFxuICBtYXJnaW4tcmlnaHQ6IDEwcHg7XFxuICBwYWRkaW5nOiAwO1xcbiAgd2lkdGg6IDcwJTtcXG59XFxuLmNvbnRyb2wtcGFuZWwgLm51bWJlci1zbGlkZXItY29udGFpbmVyIC5udW1iZXItaW5wdXQge1xcbiAgZm9udC1zaXplOiAxMXB4O1xcbiAgaGVpZ2h0OiAxMXB4O1xcbiAgdG9wOiAtNHB4O1xcbiAgd2lkdGg6IDIwJTtcXG59XFxuLmNvbnRyb2wtcGFuZWwgYnV0dG9uIHtcXG4gIGJhY2tncm91bmQ6ICMwNTFlNDI7XFxuICBib3JkZXI6IDFweCBzb2xpZCByZ2JhKDEwNCwgMTQwLCAxOTQsIDAuMjUpO1xcbiAgYm9yZGVyLXJhZGl1czogMnB4O1xcbiAgY29sb3I6ICNjM2NlZGY7XFxuICBjdXJzb3I6IHBvaW50ZXI7XFxuICBmb250LXNpemU6IDExcHg7XFxuICBwYWRkaW5nOiA1cHggMTBweDtcXG59XFxuLmNvbnRyb2wtcGFuZWwgaW5wdXQsXFxuLmNvbnRyb2wtcGFuZWwgLnZjLWlucHV0X19pbnB1dCB7XFxuICBiYWNrZ3JvdW5kOiAjMDgxMjIwO1xcbiAgYm9yZGVyOiAxcHggc29saWQgIzY4OGNjMjtcXG4gIGNvbG9yOiAjYzNjZWRmO1xcbiAgLyogc3R5bGVsaW50LWVuYWJsZSAqL1xcbiAgZm9udC1mYW1pbHk6IG1vbm9zcGFjZTtcXG4gIHBhZGRpbmc6IDFweDtcXG59XFxuLmNvbnRyb2wtcGFuZWwgaW5wdXRbdHlwZT1jaGVja2JveF0ge1xcbiAgLXdlYmtpdC1hcHBlYXJhbmNlOiBub25lO1xcbiAgICAgLW1vei1hcHBlYXJhbmNlOiBub25lO1xcbiAgICAgICAgICBhcHBlYXJhbmNlOiBub25lO1xcbiAgYmFja2dyb3VuZDogIzA4MTIyMDtcXG4gIGJvcmRlcjogMXB4IHNvbGlkICM2ODhjYzI7XFxuICBjdXJzb3I6IHBvaW50ZXI7XFxuICBoZWlnaHQ6IDE1cHg7XFxuICBtYXJnaW46IDA7XFxuICB3aWR0aDogMTVweDtcXG59XFxuLmNvbnRyb2wtcGFuZWwgaW5wdXRbdHlwZT1jaGVja2JveF06OmFmdGVyIHtcXG4gIGJhY2tncm91bmQ6ICNjM2NlZGY7XFxuICAtd2Via2l0LWJveC1zaXppbmc6IGJvcmRlci1ib3g7XFxuICAgICAgICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XFxuICBjb250ZW50OiBcXFwiXFxcIjtcXG4gIGRpc3BsYXk6IG5vbmU7XFxuICBoZWlnaHQ6IDEwMCU7XFxuICB3aWR0aDogMTAwJTtcXG59XFxuLmNvbnRyb2wtcGFuZWwgaW5wdXRbdHlwZT1jaGVja2JveF06Y2hlY2tlZDo6YWZ0ZXIge1xcbiAgZGlzcGxheTogYmxvY2s7XFxufVxcbi5jb250cm9sLXBhbmVsIGlucHV0W3R5cGU9cmFuZ2VdIHtcXG4gIC13ZWJraXQtYXBwZWFyYW5jZTogbm9uZTtcXG4gICAgIC1tb3otYXBwZWFyYW5jZTogbm9uZTtcXG4gICAgICAgICAgYXBwZWFyYW5jZTogbm9uZTtcXG4gIGJhY2tncm91bmQ6IG5vbmU7XFxuICBib3JkZXI6IDA7XFxuICBtYXJnaW46IDE4cHggMDtcXG59XFxuLmNvbnRyb2wtcGFuZWwgaW5wdXRbdHlwZT1yYW5nZV06Oi13ZWJraXQtc2xpZGVyLXJ1bm5hYmxlLXRyYWNrIHtcXG4gIGJhY2tncm91bmQ6ICMwODEyMjA7XFxuICBib3JkZXI6IDFweCBzb2xpZCAjNjg4Y2MyO1xcbiAgY3Vyc29yOiBwb2ludGVyO1xcbiAgaGVpZ2h0OiAxNXB4O1xcbn1cXG4uY29udHJvbC1wYW5lbCBpbnB1dFt0eXBlPXJhbmdlXTo6LW1vei1yYW5nZS10cmFjayB7XFxuICBiYWNrZ3JvdW5kOiAjMDgxMjIwO1xcbiAgYm9yZGVyOiAxcHggc29saWQgIzY4OGNjMjtcXG4gIGN1cnNvcjogcG9pbnRlcjtcXG4gIGhlaWdodDogMTVweDtcXG59XFxuLmNvbnRyb2wtcGFuZWwgaW5wdXRbdHlwZT1yYW5nZV06Oi1tcy10cmFjayB7XFxuICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcXG4gIGJvcmRlcjogMDtcXG4gIGNvbG9yOiB0cmFuc3BhcmVudDtcXG4gIGN1cnNvcjogcG9pbnRlcjtcXG4gIGhlaWdodDogMTVweDtcXG59XFxuLmNvbnRyb2wtcGFuZWwgaW5wdXRbdHlwZT1yYW5nZV06Oi13ZWJraXQtc2xpZGVyLXRodW1iIHtcXG4gIC13ZWJraXQtYXBwZWFyYW5jZTogbm9uZTtcXG4gICAgICAgICAgYXBwZWFyYW5jZTogbm9uZTtcXG4gIGJhY2tncm91bmQ6ICMwNTFlNDI7XFxuICBib3JkZXI6IDFweCBzb2xpZCAjYzNjZWRmO1xcbiAgYm9yZGVyLXJhZGl1czogMXB4O1xcbiAgY3Vyc29yOiBwb2ludGVyO1xcbiAgaGVpZ2h0OiAxMXB4O1xcbiAgbWFyZ2luLXRvcDogMXB4O1xcbiAgd2lkdGg6IDdweDtcXG59XFxuLmNvbnRyb2wtcGFuZWwgaW5wdXRbdHlwZT1yYW5nZV06Oi1tb3otcmFuZ2UtdGh1bWIge1xcbiAgYmFja2dyb3VuZDogIzA1MWU0MjtcXG4gIGJvcmRlcjogMXB4IHNvbGlkICNjM2NlZGY7XFxuICBib3JkZXItcmFkaXVzOiAxcHg7XFxuICBjdXJzb3I6IHBvaW50ZXI7XFxuICBoZWlnaHQ6IDExcHg7XFxuICB3aWR0aDogN3B4O1xcbn1cXG4uY29udHJvbC1wYW5lbCBpbnB1dFt0eXBlPXJhbmdlXTo6LW1zLXRodW1iIHtcXG4gIGJhY2tncm91bmQ6ICMwNTFlNDI7XFxuICBib3JkZXI6IDFweCBzb2xpZCAjYzNjZWRmO1xcbiAgYm9yZGVyLXJhZGl1czogMXB4O1xcbiAgY3Vyc29yOiBwb2ludGVyO1xcbiAgaGVpZ2h0OiAxMXB4O1xcbiAgd2lkdGg6IDdweDtcXG59XFxuLmNvbnRyb2wtcGFuZWwgaW5wdXRbdHlwZT1yYW5nZV06Oi1tcy1maWxsLWxvd2VyIHtcXG4gIGJhY2tncm91bmQ6ICMwODEyMjA7XFxuICBib3JkZXI6IDFweCBzb2xpZCAjNjg4Y2MyO1xcbn1cXG4uY29udHJvbC1wYW5lbCBpbnB1dFt0eXBlPXJhbmdlXTo6LW1zLWZpbGwtdXBwZXIge1xcbiAgYmFja2dyb3VuZDogIzA4MTIyMDtcXG4gIGJvcmRlcjogMXB4IHNvbGlkICM2ODhjYzI7XFxufVxcbi5jb250cm9sLXBhbmVsIGlucHV0W3R5cGU9cmFuZ2VdOmZvY3VzIHtcXG4gIG91dGxpbmU6IG5vbmU7XFxufVxcbi5jb250cm9sLXBhbmVsIGlucHV0W3R5cGU9cmFuZ2VdOmZvY3VzOjotd2Via2l0LXNsaWRlci1ydW5uYWJsZS10cmFjayB7XFxuICBiYWNrZ3JvdW5kOiAjMDUxZTQyO1xcbn1cXG4uY29udHJvbC1wYW5lbCBpbnB1dFt0eXBlPXJhbmdlXTpmb2N1czo6LW1zLWZpbGwtbG93ZXIge1xcbiAgYmFja2dyb3VuZDogIzA1MWU0MjtcXG59XFxuLmNvbnRyb2wtcGFuZWwgaW5wdXRbdHlwZT1yYW5nZV06Zm9jdXM6Oi1tcy1maWxsLXVwcGVyIHtcXG4gIGJhY2tncm91bmQ6ICMwNTFlNDI7XFxufVwiLCBcIlwiXSk7XG4vLyBFeHBvcnRzXG5tb2R1bGUuZXhwb3J0cyA9IGV4cG9ydHM7XG4iLCJcInVzZSBzdHJpY3RcIjtcblxuLypcbiAgTUlUIExpY2Vuc2UgaHR0cDovL3d3dy5vcGVuc291cmNlLm9yZy9saWNlbnNlcy9taXQtbGljZW5zZS5waHBcbiAgQXV0aG9yIFRvYmlhcyBLb3BwZXJzIEBzb2tyYVxuKi9cbi8vIGNzcyBiYXNlIGNvZGUsIGluamVjdGVkIGJ5IHRoZSBjc3MtbG9hZGVyXG4vLyBlc2xpbnQtZGlzYWJsZS1uZXh0LWxpbmUgZnVuYy1uYW1lc1xubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiAodXNlU291cmNlTWFwKSB7XG4gIHZhciBsaXN0ID0gW107IC8vIHJldHVybiB0aGUgbGlzdCBvZiBtb2R1bGVzIGFzIGNzcyBzdHJpbmdcblxuICBsaXN0LnRvU3RyaW5nID0gZnVuY3Rpb24gdG9TdHJpbmcoKSB7XG4gICAgcmV0dXJuIHRoaXMubWFwKGZ1bmN0aW9uIChpdGVtKSB7XG4gICAgICB2YXIgY29udGVudCA9IGNzc1dpdGhNYXBwaW5nVG9TdHJpbmcoaXRlbSwgdXNlU291cmNlTWFwKTtcblxuICAgICAgaWYgKGl0ZW1bMl0pIHtcbiAgICAgICAgcmV0dXJuIFwiQG1lZGlhIFwiLmNvbmNhdChpdGVtWzJdLCBcIiB7XCIpLmNvbmNhdChjb250ZW50LCBcIn1cIik7XG4gICAgICB9XG5cbiAgICAgIHJldHVybiBjb250ZW50O1xuICAgIH0pLmpvaW4oJycpO1xuICB9OyAvLyBpbXBvcnQgYSBsaXN0IG9mIG1vZHVsZXMgaW50byB0aGUgbGlzdFxuICAvLyBlc2xpbnQtZGlzYWJsZS1uZXh0LWxpbmUgZnVuYy1uYW1lc1xuXG5cbiAgbGlzdC5pID0gZnVuY3Rpb24gKG1vZHVsZXMsIG1lZGlhUXVlcnksIGRlZHVwZSkge1xuICAgIGlmICh0eXBlb2YgbW9kdWxlcyA9PT0gJ3N0cmluZycpIHtcbiAgICAgIC8vIGVzbGludC1kaXNhYmxlLW5leHQtbGluZSBuby1wYXJhbS1yZWFzc2lnblxuICAgICAgbW9kdWxlcyA9IFtbbnVsbCwgbW9kdWxlcywgJyddXTtcbiAgICB9XG5cbiAgICB2YXIgYWxyZWFkeUltcG9ydGVkTW9kdWxlcyA9IHt9O1xuXG4gICAgaWYgKGRlZHVwZSkge1xuICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCB0aGlzLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgIC8vIGVzbGludC1kaXNhYmxlLW5leHQtbGluZSBwcmVmZXItZGVzdHJ1Y3R1cmluZ1xuICAgICAgICB2YXIgaWQgPSB0aGlzW2ldWzBdO1xuXG4gICAgICAgIGlmIChpZCAhPSBudWxsKSB7XG4gICAgICAgICAgYWxyZWFkeUltcG9ydGVkTW9kdWxlc1tpZF0gPSB0cnVlO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuXG4gICAgZm9yICh2YXIgX2kgPSAwOyBfaSA8IG1vZHVsZXMubGVuZ3RoOyBfaSsrKSB7XG4gICAgICB2YXIgaXRlbSA9IFtdLmNvbmNhdChtb2R1bGVzW19pXSk7XG5cbiAgICAgIGlmIChkZWR1cGUgJiYgYWxyZWFkeUltcG9ydGVkTW9kdWxlc1tpdGVtWzBdXSkge1xuICAgICAgICAvLyBlc2xpbnQtZGlzYWJsZS1uZXh0LWxpbmUgbm8tY29udGludWVcbiAgICAgICAgY29udGludWU7XG4gICAgICB9XG5cbiAgICAgIGlmIChtZWRpYVF1ZXJ5KSB7XG4gICAgICAgIGlmICghaXRlbVsyXSkge1xuICAgICAgICAgIGl0ZW1bMl0gPSBtZWRpYVF1ZXJ5O1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIGl0ZW1bMl0gPSBcIlwiLmNvbmNhdChtZWRpYVF1ZXJ5LCBcIiBhbmQgXCIpLmNvbmNhdChpdGVtWzJdKTtcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICBsaXN0LnB1c2goaXRlbSk7XG4gICAgfVxuICB9O1xuXG4gIHJldHVybiBsaXN0O1xufTtcblxuZnVuY3Rpb24gY3NzV2l0aE1hcHBpbmdUb1N0cmluZyhpdGVtLCB1c2VTb3VyY2VNYXApIHtcbiAgdmFyIGNvbnRlbnQgPSBpdGVtWzFdIHx8ICcnOyAvLyBlc2xpbnQtZGlzYWJsZS1uZXh0LWxpbmUgcHJlZmVyLWRlc3RydWN0dXJpbmdcblxuICB2YXIgY3NzTWFwcGluZyA9IGl0ZW1bM107XG5cbiAgaWYgKCFjc3NNYXBwaW5nKSB7XG4gICAgcmV0dXJuIGNvbnRlbnQ7XG4gIH1cblxuICBpZiAodXNlU291cmNlTWFwICYmIHR5cGVvZiBidG9hID09PSAnZnVuY3Rpb24nKSB7XG4gICAgdmFyIHNvdXJjZU1hcHBpbmcgPSB0b0NvbW1lbnQoY3NzTWFwcGluZyk7XG4gICAgdmFyIHNvdXJjZVVSTHMgPSBjc3NNYXBwaW5nLnNvdXJjZXMubWFwKGZ1bmN0aW9uIChzb3VyY2UpIHtcbiAgICAgIHJldHVybiBcIi8qIyBzb3VyY2VVUkw9XCIuY29uY2F0KGNzc01hcHBpbmcuc291cmNlUm9vdCB8fCAnJykuY29uY2F0KHNvdXJjZSwgXCIgKi9cIik7XG4gICAgfSk7XG4gICAgcmV0dXJuIFtjb250ZW50XS5jb25jYXQoc291cmNlVVJMcykuY29uY2F0KFtzb3VyY2VNYXBwaW5nXSkuam9pbignXFxuJyk7XG4gIH1cblxuICByZXR1cm4gW2NvbnRlbnRdLmpvaW4oJ1xcbicpO1xufSAvLyBBZGFwdGVkIGZyb20gY29udmVydC1zb3VyY2UtbWFwIChNSVQpXG5cblxuZnVuY3Rpb24gdG9Db21tZW50KHNvdXJjZU1hcCkge1xuICAvLyBlc2xpbnQtZGlzYWJsZS1uZXh0LWxpbmUgbm8tdW5kZWZcbiAgdmFyIGJhc2U2NCA9IGJ0b2EodW5lc2NhcGUoZW5jb2RlVVJJQ29tcG9uZW50KEpTT04uc3RyaW5naWZ5KHNvdXJjZU1hcCkpKSk7XG4gIHZhciBkYXRhID0gXCJzb3VyY2VNYXBwaW5nVVJMPWRhdGE6YXBwbGljYXRpb24vanNvbjtjaGFyc2V0PXV0Zi04O2Jhc2U2NCxcIi5jb25jYXQoYmFzZTY0KTtcbiAgcmV0dXJuIFwiLyojIFwiLmNvbmNhdChkYXRhLCBcIiAqL1wiKTtcbn0iLCJ2YXIgZ2V0TmF0aXZlID0gcmVxdWlyZSgnLi9fZ2V0TmF0aXZlJyksXG4gICAgcm9vdCA9IHJlcXVpcmUoJy4vX3Jvb3QnKTtcblxuLyogQnVpbHQtaW4gbWV0aG9kIHJlZmVyZW5jZXMgdGhhdCBhcmUgdmVyaWZpZWQgdG8gYmUgbmF0aXZlLiAqL1xudmFyIERhdGFWaWV3ID0gZ2V0TmF0aXZlKHJvb3QsICdEYXRhVmlldycpO1xuXG5tb2R1bGUuZXhwb3J0cyA9IERhdGFWaWV3O1xuIiwidmFyIGhhc2hDbGVhciA9IHJlcXVpcmUoJy4vX2hhc2hDbGVhcicpLFxuICAgIGhhc2hEZWxldGUgPSByZXF1aXJlKCcuL19oYXNoRGVsZXRlJyksXG4gICAgaGFzaEdldCA9IHJlcXVpcmUoJy4vX2hhc2hHZXQnKSxcbiAgICBoYXNoSGFzID0gcmVxdWlyZSgnLi9faGFzaEhhcycpLFxuICAgIGhhc2hTZXQgPSByZXF1aXJlKCcuL19oYXNoU2V0Jyk7XG5cbi8qKlxuICogQ3JlYXRlcyBhIGhhc2ggb2JqZWN0LlxuICpcbiAqIEBwcml2YXRlXG4gKiBAY29uc3RydWN0b3JcbiAqIEBwYXJhbSB7QXJyYXl9IFtlbnRyaWVzXSBUaGUga2V5LXZhbHVlIHBhaXJzIHRvIGNhY2hlLlxuICovXG5mdW5jdGlvbiBIYXNoKGVudHJpZXMpIHtcbiAgdmFyIGluZGV4ID0gLTEsXG4gICAgICBsZW5ndGggPSBlbnRyaWVzID09IG51bGwgPyAwIDogZW50cmllcy5sZW5ndGg7XG5cbiAgdGhpcy5jbGVhcigpO1xuICB3aGlsZSAoKytpbmRleCA8IGxlbmd0aCkge1xuICAgIHZhciBlbnRyeSA9IGVudHJpZXNbaW5kZXhdO1xuICAgIHRoaXMuc2V0KGVudHJ5WzBdLCBlbnRyeVsxXSk7XG4gIH1cbn1cblxuLy8gQWRkIG1ldGhvZHMgdG8gYEhhc2hgLlxuSGFzaC5wcm90b3R5cGUuY2xlYXIgPSBoYXNoQ2xlYXI7XG5IYXNoLnByb3RvdHlwZVsnZGVsZXRlJ10gPSBoYXNoRGVsZXRlO1xuSGFzaC5wcm90b3R5cGUuZ2V0ID0gaGFzaEdldDtcbkhhc2gucHJvdG90eXBlLmhhcyA9IGhhc2hIYXM7XG5IYXNoLnByb3RvdHlwZS5zZXQgPSBoYXNoU2V0O1xuXG5tb2R1bGUuZXhwb3J0cyA9IEhhc2g7XG4iLCJ2YXIgbGlzdENhY2hlQ2xlYXIgPSByZXF1aXJlKCcuL19saXN0Q2FjaGVDbGVhcicpLFxuICAgIGxpc3RDYWNoZURlbGV0ZSA9IHJlcXVpcmUoJy4vX2xpc3RDYWNoZURlbGV0ZScpLFxuICAgIGxpc3RDYWNoZUdldCA9IHJlcXVpcmUoJy4vX2xpc3RDYWNoZUdldCcpLFxuICAgIGxpc3RDYWNoZUhhcyA9IHJlcXVpcmUoJy4vX2xpc3RDYWNoZUhhcycpLFxuICAgIGxpc3RDYWNoZVNldCA9IHJlcXVpcmUoJy4vX2xpc3RDYWNoZVNldCcpO1xuXG4vKipcbiAqIENyZWF0ZXMgYW4gbGlzdCBjYWNoZSBvYmplY3QuXG4gKlxuICogQHByaXZhdGVcbiAqIEBjb25zdHJ1Y3RvclxuICogQHBhcmFtIHtBcnJheX0gW2VudHJpZXNdIFRoZSBrZXktdmFsdWUgcGFpcnMgdG8gY2FjaGUuXG4gKi9cbmZ1bmN0aW9uIExpc3RDYWNoZShlbnRyaWVzKSB7XG4gIHZhciBpbmRleCA9IC0xLFxuICAgICAgbGVuZ3RoID0gZW50cmllcyA9PSBudWxsID8gMCA6IGVudHJpZXMubGVuZ3RoO1xuXG4gIHRoaXMuY2xlYXIoKTtcbiAgd2hpbGUgKCsraW5kZXggPCBsZW5ndGgpIHtcbiAgICB2YXIgZW50cnkgPSBlbnRyaWVzW2luZGV4XTtcbiAgICB0aGlzLnNldChlbnRyeVswXSwgZW50cnlbMV0pO1xuICB9XG59XG5cbi8vIEFkZCBtZXRob2RzIHRvIGBMaXN0Q2FjaGVgLlxuTGlzdENhY2hlLnByb3RvdHlwZS5jbGVhciA9IGxpc3RDYWNoZUNsZWFyO1xuTGlzdENhY2hlLnByb3RvdHlwZVsnZGVsZXRlJ10gPSBsaXN0Q2FjaGVEZWxldGU7XG5MaXN0Q2FjaGUucHJvdG90eXBlLmdldCA9IGxpc3RDYWNoZUdldDtcbkxpc3RDYWNoZS5wcm90b3R5cGUuaGFzID0gbGlzdENhY2hlSGFzO1xuTGlzdENhY2hlLnByb3RvdHlwZS5zZXQgPSBsaXN0Q2FjaGVTZXQ7XG5cbm1vZHVsZS5leHBvcnRzID0gTGlzdENhY2hlO1xuIiwidmFyIGdldE5hdGl2ZSA9IHJlcXVpcmUoJy4vX2dldE5hdGl2ZScpLFxuICAgIHJvb3QgPSByZXF1aXJlKCcuL19yb290Jyk7XG5cbi8qIEJ1aWx0LWluIG1ldGhvZCByZWZlcmVuY2VzIHRoYXQgYXJlIHZlcmlmaWVkIHRvIGJlIG5hdGl2ZS4gKi9cbnZhciBNYXAgPSBnZXROYXRpdmUocm9vdCwgJ01hcCcpO1xuXG5tb2R1bGUuZXhwb3J0cyA9IE1hcDtcbiIsInZhciBtYXBDYWNoZUNsZWFyID0gcmVxdWlyZSgnLi9fbWFwQ2FjaGVDbGVhcicpLFxuICAgIG1hcENhY2hlRGVsZXRlID0gcmVxdWlyZSgnLi9fbWFwQ2FjaGVEZWxldGUnKSxcbiAgICBtYXBDYWNoZUdldCA9IHJlcXVpcmUoJy4vX21hcENhY2hlR2V0JyksXG4gICAgbWFwQ2FjaGVIYXMgPSByZXF1aXJlKCcuL19tYXBDYWNoZUhhcycpLFxuICAgIG1hcENhY2hlU2V0ID0gcmVxdWlyZSgnLi9fbWFwQ2FjaGVTZXQnKTtcblxuLyoqXG4gKiBDcmVhdGVzIGEgbWFwIGNhY2hlIG9iamVjdCB0byBzdG9yZSBrZXktdmFsdWUgcGFpcnMuXG4gKlxuICogQHByaXZhdGVcbiAqIEBjb25zdHJ1Y3RvclxuICogQHBhcmFtIHtBcnJheX0gW2VudHJpZXNdIFRoZSBrZXktdmFsdWUgcGFpcnMgdG8gY2FjaGUuXG4gKi9cbmZ1bmN0aW9uIE1hcENhY2hlKGVudHJpZXMpIHtcbiAgdmFyIGluZGV4ID0gLTEsXG4gICAgICBsZW5ndGggPSBlbnRyaWVzID09IG51bGwgPyAwIDogZW50cmllcy5sZW5ndGg7XG5cbiAgdGhpcy5jbGVhcigpO1xuICB3aGlsZSAoKytpbmRleCA8IGxlbmd0aCkge1xuICAgIHZhciBlbnRyeSA9IGVudHJpZXNbaW5kZXhdO1xuICAgIHRoaXMuc2V0KGVudHJ5WzBdLCBlbnRyeVsxXSk7XG4gIH1cbn1cblxuLy8gQWRkIG1ldGhvZHMgdG8gYE1hcENhY2hlYC5cbk1hcENhY2hlLnByb3RvdHlwZS5jbGVhciA9IG1hcENhY2hlQ2xlYXI7XG5NYXBDYWNoZS5wcm90b3R5cGVbJ2RlbGV0ZSddID0gbWFwQ2FjaGVEZWxldGU7XG5NYXBDYWNoZS5wcm90b3R5cGUuZ2V0ID0gbWFwQ2FjaGVHZXQ7XG5NYXBDYWNoZS5wcm90b3R5cGUuaGFzID0gbWFwQ2FjaGVIYXM7XG5NYXBDYWNoZS5wcm90b3R5cGUuc2V0ID0gbWFwQ2FjaGVTZXQ7XG5cbm1vZHVsZS5leHBvcnRzID0gTWFwQ2FjaGU7XG4iLCJ2YXIgZ2V0TmF0aXZlID0gcmVxdWlyZSgnLi9fZ2V0TmF0aXZlJyksXG4gICAgcm9vdCA9IHJlcXVpcmUoJy4vX3Jvb3QnKTtcblxuLyogQnVpbHQtaW4gbWV0aG9kIHJlZmVyZW5jZXMgdGhhdCBhcmUgdmVyaWZpZWQgdG8gYmUgbmF0aXZlLiAqL1xudmFyIFByb21pc2UgPSBnZXROYXRpdmUocm9vdCwgJ1Byb21pc2UnKTtcblxubW9kdWxlLmV4cG9ydHMgPSBQcm9taXNlO1xuIiwidmFyIGdldE5hdGl2ZSA9IHJlcXVpcmUoJy4vX2dldE5hdGl2ZScpLFxuICAgIHJvb3QgPSByZXF1aXJlKCcuL19yb290Jyk7XG5cbi8qIEJ1aWx0LWluIG1ldGhvZCByZWZlcmVuY2VzIHRoYXQgYXJlIHZlcmlmaWVkIHRvIGJlIG5hdGl2ZS4gKi9cbnZhciBTZXQgPSBnZXROYXRpdmUocm9vdCwgJ1NldCcpO1xuXG5tb2R1bGUuZXhwb3J0cyA9IFNldDtcbiIsInZhciBNYXBDYWNoZSA9IHJlcXVpcmUoJy4vX01hcENhY2hlJyksXG4gICAgc2V0Q2FjaGVBZGQgPSByZXF1aXJlKCcuL19zZXRDYWNoZUFkZCcpLFxuICAgIHNldENhY2hlSGFzID0gcmVxdWlyZSgnLi9fc2V0Q2FjaGVIYXMnKTtcblxuLyoqXG4gKlxuICogQ3JlYXRlcyBhbiBhcnJheSBjYWNoZSBvYmplY3QgdG8gc3RvcmUgdW5pcXVlIHZhbHVlcy5cbiAqXG4gKiBAcHJpdmF0ZVxuICogQGNvbnN0cnVjdG9yXG4gKiBAcGFyYW0ge0FycmF5fSBbdmFsdWVzXSBUaGUgdmFsdWVzIHRvIGNhY2hlLlxuICovXG5mdW5jdGlvbiBTZXRDYWNoZSh2YWx1ZXMpIHtcbiAgdmFyIGluZGV4ID0gLTEsXG4gICAgICBsZW5ndGggPSB2YWx1ZXMgPT0gbnVsbCA/IDAgOiB2YWx1ZXMubGVuZ3RoO1xuXG4gIHRoaXMuX19kYXRhX18gPSBuZXcgTWFwQ2FjaGU7XG4gIHdoaWxlICgrK2luZGV4IDwgbGVuZ3RoKSB7XG4gICAgdGhpcy5hZGQodmFsdWVzW2luZGV4XSk7XG4gIH1cbn1cblxuLy8gQWRkIG1ldGhvZHMgdG8gYFNldENhY2hlYC5cblNldENhY2hlLnByb3RvdHlwZS5hZGQgPSBTZXRDYWNoZS5wcm90b3R5cGUucHVzaCA9IHNldENhY2hlQWRkO1xuU2V0Q2FjaGUucHJvdG90eXBlLmhhcyA9IHNldENhY2hlSGFzO1xuXG5tb2R1bGUuZXhwb3J0cyA9IFNldENhY2hlO1xuIiwidmFyIExpc3RDYWNoZSA9IHJlcXVpcmUoJy4vX0xpc3RDYWNoZScpLFxuICAgIHN0YWNrQ2xlYXIgPSByZXF1aXJlKCcuL19zdGFja0NsZWFyJyksXG4gICAgc3RhY2tEZWxldGUgPSByZXF1aXJlKCcuL19zdGFja0RlbGV0ZScpLFxuICAgIHN0YWNrR2V0ID0gcmVxdWlyZSgnLi9fc3RhY2tHZXQnKSxcbiAgICBzdGFja0hhcyA9IHJlcXVpcmUoJy4vX3N0YWNrSGFzJyksXG4gICAgc3RhY2tTZXQgPSByZXF1aXJlKCcuL19zdGFja1NldCcpO1xuXG4vKipcbiAqIENyZWF0ZXMgYSBzdGFjayBjYWNoZSBvYmplY3QgdG8gc3RvcmUga2V5LXZhbHVlIHBhaXJzLlxuICpcbiAqIEBwcml2YXRlXG4gKiBAY29uc3RydWN0b3JcbiAqIEBwYXJhbSB7QXJyYXl9IFtlbnRyaWVzXSBUaGUga2V5LXZhbHVlIHBhaXJzIHRvIGNhY2hlLlxuICovXG5mdW5jdGlvbiBTdGFjayhlbnRyaWVzKSB7XG4gIHZhciBkYXRhID0gdGhpcy5fX2RhdGFfXyA9IG5ldyBMaXN0Q2FjaGUoZW50cmllcyk7XG4gIHRoaXMuc2l6ZSA9IGRhdGEuc2l6ZTtcbn1cblxuLy8gQWRkIG1ldGhvZHMgdG8gYFN0YWNrYC5cblN0YWNrLnByb3RvdHlwZS5jbGVhciA9IHN0YWNrQ2xlYXI7XG5TdGFjay5wcm90b3R5cGVbJ2RlbGV0ZSddID0gc3RhY2tEZWxldGU7XG5TdGFjay5wcm90b3R5cGUuZ2V0ID0gc3RhY2tHZXQ7XG5TdGFjay5wcm90b3R5cGUuaGFzID0gc3RhY2tIYXM7XG5TdGFjay5wcm90b3R5cGUuc2V0ID0gc3RhY2tTZXQ7XG5cbm1vZHVsZS5leHBvcnRzID0gU3RhY2s7XG4iLCJ2YXIgcm9vdCA9IHJlcXVpcmUoJy4vX3Jvb3QnKTtcblxuLyoqIEJ1aWx0LWluIHZhbHVlIHJlZmVyZW5jZXMuICovXG52YXIgU3ltYm9sID0gcm9vdC5TeW1ib2w7XG5cbm1vZHVsZS5leHBvcnRzID0gU3ltYm9sO1xuIiwidmFyIHJvb3QgPSByZXF1aXJlKCcuL19yb290Jyk7XG5cbi8qKiBCdWlsdC1pbiB2YWx1ZSByZWZlcmVuY2VzLiAqL1xudmFyIFVpbnQ4QXJyYXkgPSByb290LlVpbnQ4QXJyYXk7XG5cbm1vZHVsZS5leHBvcnRzID0gVWludDhBcnJheTtcbiIsInZhciBnZXROYXRpdmUgPSByZXF1aXJlKCcuL19nZXROYXRpdmUnKSxcbiAgICByb290ID0gcmVxdWlyZSgnLi9fcm9vdCcpO1xuXG4vKiBCdWlsdC1pbiBtZXRob2QgcmVmZXJlbmNlcyB0aGF0IGFyZSB2ZXJpZmllZCB0byBiZSBuYXRpdmUuICovXG52YXIgV2Vha01hcCA9IGdldE5hdGl2ZShyb290LCAnV2Vha01hcCcpO1xuXG5tb2R1bGUuZXhwb3J0cyA9IFdlYWtNYXA7XG4iLCIvKipcbiAqIEEgc3BlY2lhbGl6ZWQgdmVyc2lvbiBvZiBgXy5maWx0ZXJgIGZvciBhcnJheXMgd2l0aG91dCBzdXBwb3J0IGZvclxuICogaXRlcmF0ZWUgc2hvcnRoYW5kcy5cbiAqXG4gKiBAcHJpdmF0ZVxuICogQHBhcmFtIHtBcnJheX0gW2FycmF5XSBUaGUgYXJyYXkgdG8gaXRlcmF0ZSBvdmVyLlxuICogQHBhcmFtIHtGdW5jdGlvbn0gcHJlZGljYXRlIFRoZSBmdW5jdGlvbiBpbnZva2VkIHBlciBpdGVyYXRpb24uXG4gKiBAcmV0dXJucyB7QXJyYXl9IFJldHVybnMgdGhlIG5ldyBmaWx0ZXJlZCBhcnJheS5cbiAqL1xuZnVuY3Rpb24gYXJyYXlGaWx0ZXIoYXJyYXksIHByZWRpY2F0ZSkge1xuICB2YXIgaW5kZXggPSAtMSxcbiAgICAgIGxlbmd0aCA9IGFycmF5ID09IG51bGwgPyAwIDogYXJyYXkubGVuZ3RoLFxuICAgICAgcmVzSW5kZXggPSAwLFxuICAgICAgcmVzdWx0ID0gW107XG5cbiAgd2hpbGUgKCsraW5kZXggPCBsZW5ndGgpIHtcbiAgICB2YXIgdmFsdWUgPSBhcnJheVtpbmRleF07XG4gICAgaWYgKHByZWRpY2F0ZSh2YWx1ZSwgaW5kZXgsIGFycmF5KSkge1xuICAgICAgcmVzdWx0W3Jlc0luZGV4KytdID0gdmFsdWU7XG4gICAgfVxuICB9XG4gIHJldHVybiByZXN1bHQ7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gYXJyYXlGaWx0ZXI7XG4iLCJ2YXIgYmFzZVRpbWVzID0gcmVxdWlyZSgnLi9fYmFzZVRpbWVzJyksXG4gICAgaXNBcmd1bWVudHMgPSByZXF1aXJlKCcuL2lzQXJndW1lbnRzJyksXG4gICAgaXNBcnJheSA9IHJlcXVpcmUoJy4vaXNBcnJheScpLFxuICAgIGlzQnVmZmVyID0gcmVxdWlyZSgnLi9pc0J1ZmZlcicpLFxuICAgIGlzSW5kZXggPSByZXF1aXJlKCcuL19pc0luZGV4JyksXG4gICAgaXNUeXBlZEFycmF5ID0gcmVxdWlyZSgnLi9pc1R5cGVkQXJyYXknKTtcblxuLyoqIFVzZWQgZm9yIGJ1aWx0LWluIG1ldGhvZCByZWZlcmVuY2VzLiAqL1xudmFyIG9iamVjdFByb3RvID0gT2JqZWN0LnByb3RvdHlwZTtcblxuLyoqIFVzZWQgdG8gY2hlY2sgb2JqZWN0cyBmb3Igb3duIHByb3BlcnRpZXMuICovXG52YXIgaGFzT3duUHJvcGVydHkgPSBvYmplY3RQcm90by5oYXNPd25Qcm9wZXJ0eTtcblxuLyoqXG4gKiBDcmVhdGVzIGFuIGFycmF5IG9mIHRoZSBlbnVtZXJhYmxlIHByb3BlcnR5IG5hbWVzIG9mIHRoZSBhcnJheS1saWtlIGB2YWx1ZWAuXG4gKlxuICogQHByaXZhdGVcbiAqIEBwYXJhbSB7Kn0gdmFsdWUgVGhlIHZhbHVlIHRvIHF1ZXJ5LlxuICogQHBhcmFtIHtib29sZWFufSBpbmhlcml0ZWQgU3BlY2lmeSByZXR1cm5pbmcgaW5oZXJpdGVkIHByb3BlcnR5IG5hbWVzLlxuICogQHJldHVybnMge0FycmF5fSBSZXR1cm5zIHRoZSBhcnJheSBvZiBwcm9wZXJ0eSBuYW1lcy5cbiAqL1xuZnVuY3Rpb24gYXJyYXlMaWtlS2V5cyh2YWx1ZSwgaW5oZXJpdGVkKSB7XG4gIHZhciBpc0FyciA9IGlzQXJyYXkodmFsdWUpLFxuICAgICAgaXNBcmcgPSAhaXNBcnIgJiYgaXNBcmd1bWVudHModmFsdWUpLFxuICAgICAgaXNCdWZmID0gIWlzQXJyICYmICFpc0FyZyAmJiBpc0J1ZmZlcih2YWx1ZSksXG4gICAgICBpc1R5cGUgPSAhaXNBcnIgJiYgIWlzQXJnICYmICFpc0J1ZmYgJiYgaXNUeXBlZEFycmF5KHZhbHVlKSxcbiAgICAgIHNraXBJbmRleGVzID0gaXNBcnIgfHwgaXNBcmcgfHwgaXNCdWZmIHx8IGlzVHlwZSxcbiAgICAgIHJlc3VsdCA9IHNraXBJbmRleGVzID8gYmFzZVRpbWVzKHZhbHVlLmxlbmd0aCwgU3RyaW5nKSA6IFtdLFxuICAgICAgbGVuZ3RoID0gcmVzdWx0Lmxlbmd0aDtcblxuICBmb3IgKHZhciBrZXkgaW4gdmFsdWUpIHtcbiAgICBpZiAoKGluaGVyaXRlZCB8fCBoYXNPd25Qcm9wZXJ0eS5jYWxsKHZhbHVlLCBrZXkpKSAmJlxuICAgICAgICAhKHNraXBJbmRleGVzICYmIChcbiAgICAgICAgICAgLy8gU2FmYXJpIDkgaGFzIGVudW1lcmFibGUgYGFyZ3VtZW50cy5sZW5ndGhgIGluIHN0cmljdCBtb2RlLlxuICAgICAgICAgICBrZXkgPT0gJ2xlbmd0aCcgfHxcbiAgICAgICAgICAgLy8gTm9kZS5qcyAwLjEwIGhhcyBlbnVtZXJhYmxlIG5vbi1pbmRleCBwcm9wZXJ0aWVzIG9uIGJ1ZmZlcnMuXG4gICAgICAgICAgIChpc0J1ZmYgJiYgKGtleSA9PSAnb2Zmc2V0JyB8fCBrZXkgPT0gJ3BhcmVudCcpKSB8fFxuICAgICAgICAgICAvLyBQaGFudG9tSlMgMiBoYXMgZW51bWVyYWJsZSBub24taW5kZXggcHJvcGVydGllcyBvbiB0eXBlZCBhcnJheXMuXG4gICAgICAgICAgIChpc1R5cGUgJiYgKGtleSA9PSAnYnVmZmVyJyB8fCBrZXkgPT0gJ2J5dGVMZW5ndGgnIHx8IGtleSA9PSAnYnl0ZU9mZnNldCcpKSB8fFxuICAgICAgICAgICAvLyBTa2lwIGluZGV4IHByb3BlcnRpZXMuXG4gICAgICAgICAgIGlzSW5kZXgoa2V5LCBsZW5ndGgpXG4gICAgICAgICkpKSB7XG4gICAgICByZXN1bHQucHVzaChrZXkpO1xuICAgIH1cbiAgfVxuICByZXR1cm4gcmVzdWx0O1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IGFycmF5TGlrZUtleXM7XG4iLCIvKipcbiAqIEEgc3BlY2lhbGl6ZWQgdmVyc2lvbiBvZiBgXy5tYXBgIGZvciBhcnJheXMgd2l0aG91dCBzdXBwb3J0IGZvciBpdGVyYXRlZVxuICogc2hvcnRoYW5kcy5cbiAqXG4gKiBAcHJpdmF0ZVxuICogQHBhcmFtIHtBcnJheX0gW2FycmF5XSBUaGUgYXJyYXkgdG8gaXRlcmF0ZSBvdmVyLlxuICogQHBhcmFtIHtGdW5jdGlvbn0gaXRlcmF0ZWUgVGhlIGZ1bmN0aW9uIGludm9rZWQgcGVyIGl0ZXJhdGlvbi5cbiAqIEByZXR1cm5zIHtBcnJheX0gUmV0dXJucyB0aGUgbmV3IG1hcHBlZCBhcnJheS5cbiAqL1xuZnVuY3Rpb24gYXJyYXlNYXAoYXJyYXksIGl0ZXJhdGVlKSB7XG4gIHZhciBpbmRleCA9IC0xLFxuICAgICAgbGVuZ3RoID0gYXJyYXkgPT0gbnVsbCA/IDAgOiBhcnJheS5sZW5ndGgsXG4gICAgICByZXN1bHQgPSBBcnJheShsZW5ndGgpO1xuXG4gIHdoaWxlICgrK2luZGV4IDwgbGVuZ3RoKSB7XG4gICAgcmVzdWx0W2luZGV4XSA9IGl0ZXJhdGVlKGFycmF5W2luZGV4XSwgaW5kZXgsIGFycmF5KTtcbiAgfVxuICByZXR1cm4gcmVzdWx0O1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IGFycmF5TWFwO1xuIiwiLyoqXG4gKiBBcHBlbmRzIHRoZSBlbGVtZW50cyBvZiBgdmFsdWVzYCB0byBgYXJyYXlgLlxuICpcbiAqIEBwcml2YXRlXG4gKiBAcGFyYW0ge0FycmF5fSBhcnJheSBUaGUgYXJyYXkgdG8gbW9kaWZ5LlxuICogQHBhcmFtIHtBcnJheX0gdmFsdWVzIFRoZSB2YWx1ZXMgdG8gYXBwZW5kLlxuICogQHJldHVybnMge0FycmF5fSBSZXR1cm5zIGBhcnJheWAuXG4gKi9cbmZ1bmN0aW9uIGFycmF5UHVzaChhcnJheSwgdmFsdWVzKSB7XG4gIHZhciBpbmRleCA9IC0xLFxuICAgICAgbGVuZ3RoID0gdmFsdWVzLmxlbmd0aCxcbiAgICAgIG9mZnNldCA9IGFycmF5Lmxlbmd0aDtcblxuICB3aGlsZSAoKytpbmRleCA8IGxlbmd0aCkge1xuICAgIGFycmF5W29mZnNldCArIGluZGV4XSA9IHZhbHVlc1tpbmRleF07XG4gIH1cbiAgcmV0dXJuIGFycmF5O1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IGFycmF5UHVzaDtcbiIsIi8qKlxuICogQSBzcGVjaWFsaXplZCB2ZXJzaW9uIG9mIGBfLnNvbWVgIGZvciBhcnJheXMgd2l0aG91dCBzdXBwb3J0IGZvciBpdGVyYXRlZVxuICogc2hvcnRoYW5kcy5cbiAqXG4gKiBAcHJpdmF0ZVxuICogQHBhcmFtIHtBcnJheX0gW2FycmF5XSBUaGUgYXJyYXkgdG8gaXRlcmF0ZSBvdmVyLlxuICogQHBhcmFtIHtGdW5jdGlvbn0gcHJlZGljYXRlIFRoZSBmdW5jdGlvbiBpbnZva2VkIHBlciBpdGVyYXRpb24uXG4gKiBAcmV0dXJucyB7Ym9vbGVhbn0gUmV0dXJucyBgdHJ1ZWAgaWYgYW55IGVsZW1lbnQgcGFzc2VzIHRoZSBwcmVkaWNhdGUgY2hlY2ssXG4gKiAgZWxzZSBgZmFsc2VgLlxuICovXG5mdW5jdGlvbiBhcnJheVNvbWUoYXJyYXksIHByZWRpY2F0ZSkge1xuICB2YXIgaW5kZXggPSAtMSxcbiAgICAgIGxlbmd0aCA9IGFycmF5ID09IG51bGwgPyAwIDogYXJyYXkubGVuZ3RoO1xuXG4gIHdoaWxlICgrK2luZGV4IDwgbGVuZ3RoKSB7XG4gICAgaWYgKHByZWRpY2F0ZShhcnJheVtpbmRleF0sIGluZGV4LCBhcnJheSkpIHtcbiAgICAgIHJldHVybiB0cnVlO1xuICAgIH1cbiAgfVxuICByZXR1cm4gZmFsc2U7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gYXJyYXlTb21lO1xuIiwidmFyIGVxID0gcmVxdWlyZSgnLi9lcScpO1xuXG4vKipcbiAqIEdldHMgdGhlIGluZGV4IGF0IHdoaWNoIHRoZSBga2V5YCBpcyBmb3VuZCBpbiBgYXJyYXlgIG9mIGtleS12YWx1ZSBwYWlycy5cbiAqXG4gKiBAcHJpdmF0ZVxuICogQHBhcmFtIHtBcnJheX0gYXJyYXkgVGhlIGFycmF5IHRvIGluc3BlY3QuXG4gKiBAcGFyYW0geyp9IGtleSBUaGUga2V5IHRvIHNlYXJjaCBmb3IuXG4gKiBAcmV0dXJucyB7bnVtYmVyfSBSZXR1cm5zIHRoZSBpbmRleCBvZiB0aGUgbWF0Y2hlZCB2YWx1ZSwgZWxzZSBgLTFgLlxuICovXG5mdW5jdGlvbiBhc3NvY0luZGV4T2YoYXJyYXksIGtleSkge1xuICB2YXIgbGVuZ3RoID0gYXJyYXkubGVuZ3RoO1xuICB3aGlsZSAobGVuZ3RoLS0pIHtcbiAgICBpZiAoZXEoYXJyYXlbbGVuZ3RoXVswXSwga2V5KSkge1xuICAgICAgcmV0dXJuIGxlbmd0aDtcbiAgICB9XG4gIH1cbiAgcmV0dXJuIC0xO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IGFzc29jSW5kZXhPZjtcbiIsIi8qKlxuICogVGhlIGJhc2UgaW1wbGVtZW50YXRpb24gb2YgYF8uZmluZEluZGV4YCBhbmQgYF8uZmluZExhc3RJbmRleGAgd2l0aG91dFxuICogc3VwcG9ydCBmb3IgaXRlcmF0ZWUgc2hvcnRoYW5kcy5cbiAqXG4gKiBAcHJpdmF0ZVxuICogQHBhcmFtIHtBcnJheX0gYXJyYXkgVGhlIGFycmF5IHRvIGluc3BlY3QuXG4gKiBAcGFyYW0ge0Z1bmN0aW9ufSBwcmVkaWNhdGUgVGhlIGZ1bmN0aW9uIGludm9rZWQgcGVyIGl0ZXJhdGlvbi5cbiAqIEBwYXJhbSB7bnVtYmVyfSBmcm9tSW5kZXggVGhlIGluZGV4IHRvIHNlYXJjaCBmcm9tLlxuICogQHBhcmFtIHtib29sZWFufSBbZnJvbVJpZ2h0XSBTcGVjaWZ5IGl0ZXJhdGluZyBmcm9tIHJpZ2h0IHRvIGxlZnQuXG4gKiBAcmV0dXJucyB7bnVtYmVyfSBSZXR1cm5zIHRoZSBpbmRleCBvZiB0aGUgbWF0Y2hlZCB2YWx1ZSwgZWxzZSBgLTFgLlxuICovXG5mdW5jdGlvbiBiYXNlRmluZEluZGV4KGFycmF5LCBwcmVkaWNhdGUsIGZyb21JbmRleCwgZnJvbVJpZ2h0KSB7XG4gIHZhciBsZW5ndGggPSBhcnJheS5sZW5ndGgsXG4gICAgICBpbmRleCA9IGZyb21JbmRleCArIChmcm9tUmlnaHQgPyAxIDogLTEpO1xuXG4gIHdoaWxlICgoZnJvbVJpZ2h0ID8gaW5kZXgtLSA6ICsraW5kZXggPCBsZW5ndGgpKSB7XG4gICAgaWYgKHByZWRpY2F0ZShhcnJheVtpbmRleF0sIGluZGV4LCBhcnJheSkpIHtcbiAgICAgIHJldHVybiBpbmRleDtcbiAgICB9XG4gIH1cbiAgcmV0dXJuIC0xO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IGJhc2VGaW5kSW5kZXg7XG4iLCJ2YXIgY2FzdFBhdGggPSByZXF1aXJlKCcuL19jYXN0UGF0aCcpLFxuICAgIHRvS2V5ID0gcmVxdWlyZSgnLi9fdG9LZXknKTtcblxuLyoqXG4gKiBUaGUgYmFzZSBpbXBsZW1lbnRhdGlvbiBvZiBgXy5nZXRgIHdpdGhvdXQgc3VwcG9ydCBmb3IgZGVmYXVsdCB2YWx1ZXMuXG4gKlxuICogQHByaXZhdGVcbiAqIEBwYXJhbSB7T2JqZWN0fSBvYmplY3QgVGhlIG9iamVjdCB0byBxdWVyeS5cbiAqIEBwYXJhbSB7QXJyYXl8c3RyaW5nfSBwYXRoIFRoZSBwYXRoIG9mIHRoZSBwcm9wZXJ0eSB0byBnZXQuXG4gKiBAcmV0dXJucyB7Kn0gUmV0dXJucyB0aGUgcmVzb2x2ZWQgdmFsdWUuXG4gKi9cbmZ1bmN0aW9uIGJhc2VHZXQob2JqZWN0LCBwYXRoKSB7XG4gIHBhdGggPSBjYXN0UGF0aChwYXRoLCBvYmplY3QpO1xuXG4gIHZhciBpbmRleCA9IDAsXG4gICAgICBsZW5ndGggPSBwYXRoLmxlbmd0aDtcblxuICB3aGlsZSAob2JqZWN0ICE9IG51bGwgJiYgaW5kZXggPCBsZW5ndGgpIHtcbiAgICBvYmplY3QgPSBvYmplY3RbdG9LZXkocGF0aFtpbmRleCsrXSldO1xuICB9XG4gIHJldHVybiAoaW5kZXggJiYgaW5kZXggPT0gbGVuZ3RoKSA/IG9iamVjdCA6IHVuZGVmaW5lZDtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBiYXNlR2V0O1xuIiwidmFyIGFycmF5UHVzaCA9IHJlcXVpcmUoJy4vX2FycmF5UHVzaCcpLFxuICAgIGlzQXJyYXkgPSByZXF1aXJlKCcuL2lzQXJyYXknKTtcblxuLyoqXG4gKiBUaGUgYmFzZSBpbXBsZW1lbnRhdGlvbiBvZiBgZ2V0QWxsS2V5c2AgYW5kIGBnZXRBbGxLZXlzSW5gIHdoaWNoIHVzZXNcbiAqIGBrZXlzRnVuY2AgYW5kIGBzeW1ib2xzRnVuY2AgdG8gZ2V0IHRoZSBlbnVtZXJhYmxlIHByb3BlcnR5IG5hbWVzIGFuZFxuICogc3ltYm9scyBvZiBgb2JqZWN0YC5cbiAqXG4gKiBAcHJpdmF0ZVxuICogQHBhcmFtIHtPYmplY3R9IG9iamVjdCBUaGUgb2JqZWN0IHRvIHF1ZXJ5LlxuICogQHBhcmFtIHtGdW5jdGlvbn0ga2V5c0Z1bmMgVGhlIGZ1bmN0aW9uIHRvIGdldCB0aGUga2V5cyBvZiBgb2JqZWN0YC5cbiAqIEBwYXJhbSB7RnVuY3Rpb259IHN5bWJvbHNGdW5jIFRoZSBmdW5jdGlvbiB0byBnZXQgdGhlIHN5bWJvbHMgb2YgYG9iamVjdGAuXG4gKiBAcmV0dXJucyB7QXJyYXl9IFJldHVybnMgdGhlIGFycmF5IG9mIHByb3BlcnR5IG5hbWVzIGFuZCBzeW1ib2xzLlxuICovXG5mdW5jdGlvbiBiYXNlR2V0QWxsS2V5cyhvYmplY3QsIGtleXNGdW5jLCBzeW1ib2xzRnVuYykge1xuICB2YXIgcmVzdWx0ID0ga2V5c0Z1bmMob2JqZWN0KTtcbiAgcmV0dXJuIGlzQXJyYXkob2JqZWN0KSA/IHJlc3VsdCA6IGFycmF5UHVzaChyZXN1bHQsIHN5bWJvbHNGdW5jKG9iamVjdCkpO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IGJhc2VHZXRBbGxLZXlzO1xuIiwidmFyIFN5bWJvbCA9IHJlcXVpcmUoJy4vX1N5bWJvbCcpLFxuICAgIGdldFJhd1RhZyA9IHJlcXVpcmUoJy4vX2dldFJhd1RhZycpLFxuICAgIG9iamVjdFRvU3RyaW5nID0gcmVxdWlyZSgnLi9fb2JqZWN0VG9TdHJpbmcnKTtcblxuLyoqIGBPYmplY3QjdG9TdHJpbmdgIHJlc3VsdCByZWZlcmVuY2VzLiAqL1xudmFyIG51bGxUYWcgPSAnW29iamVjdCBOdWxsXScsXG4gICAgdW5kZWZpbmVkVGFnID0gJ1tvYmplY3QgVW5kZWZpbmVkXSc7XG5cbi8qKiBCdWlsdC1pbiB2YWx1ZSByZWZlcmVuY2VzLiAqL1xudmFyIHN5bVRvU3RyaW5nVGFnID0gU3ltYm9sID8gU3ltYm9sLnRvU3RyaW5nVGFnIDogdW5kZWZpbmVkO1xuXG4vKipcbiAqIFRoZSBiYXNlIGltcGxlbWVudGF0aW9uIG9mIGBnZXRUYWdgIHdpdGhvdXQgZmFsbGJhY2tzIGZvciBidWdneSBlbnZpcm9ubWVudHMuXG4gKlxuICogQHByaXZhdGVcbiAqIEBwYXJhbSB7Kn0gdmFsdWUgVGhlIHZhbHVlIHRvIHF1ZXJ5LlxuICogQHJldHVybnMge3N0cmluZ30gUmV0dXJucyB0aGUgYHRvU3RyaW5nVGFnYC5cbiAqL1xuZnVuY3Rpb24gYmFzZUdldFRhZyh2YWx1ZSkge1xuICBpZiAodmFsdWUgPT0gbnVsbCkge1xuICAgIHJldHVybiB2YWx1ZSA9PT0gdW5kZWZpbmVkID8gdW5kZWZpbmVkVGFnIDogbnVsbFRhZztcbiAgfVxuICByZXR1cm4gKHN5bVRvU3RyaW5nVGFnICYmIHN5bVRvU3RyaW5nVGFnIGluIE9iamVjdCh2YWx1ZSkpXG4gICAgPyBnZXRSYXdUYWcodmFsdWUpXG4gICAgOiBvYmplY3RUb1N0cmluZyh2YWx1ZSk7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gYmFzZUdldFRhZztcbiIsIi8qKlxuICogVGhlIGJhc2UgaW1wbGVtZW50YXRpb24gb2YgYF8uaGFzSW5gIHdpdGhvdXQgc3VwcG9ydCBmb3IgZGVlcCBwYXRocy5cbiAqXG4gKiBAcHJpdmF0ZVxuICogQHBhcmFtIHtPYmplY3R9IFtvYmplY3RdIFRoZSBvYmplY3QgdG8gcXVlcnkuXG4gKiBAcGFyYW0ge0FycmF5fHN0cmluZ30ga2V5IFRoZSBrZXkgdG8gY2hlY2suXG4gKiBAcmV0dXJucyB7Ym9vbGVhbn0gUmV0dXJucyBgdHJ1ZWAgaWYgYGtleWAgZXhpc3RzLCBlbHNlIGBmYWxzZWAuXG4gKi9cbmZ1bmN0aW9uIGJhc2VIYXNJbihvYmplY3QsIGtleSkge1xuICByZXR1cm4gb2JqZWN0ICE9IG51bGwgJiYga2V5IGluIE9iamVjdChvYmplY3QpO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IGJhc2VIYXNJbjtcbiIsInZhciBiYXNlR2V0VGFnID0gcmVxdWlyZSgnLi9fYmFzZUdldFRhZycpLFxuICAgIGlzT2JqZWN0TGlrZSA9IHJlcXVpcmUoJy4vaXNPYmplY3RMaWtlJyk7XG5cbi8qKiBgT2JqZWN0I3RvU3RyaW5nYCByZXN1bHQgcmVmZXJlbmNlcy4gKi9cbnZhciBhcmdzVGFnID0gJ1tvYmplY3QgQXJndW1lbnRzXSc7XG5cbi8qKlxuICogVGhlIGJhc2UgaW1wbGVtZW50YXRpb24gb2YgYF8uaXNBcmd1bWVudHNgLlxuICpcbiAqIEBwcml2YXRlXG4gKiBAcGFyYW0geyp9IHZhbHVlIFRoZSB2YWx1ZSB0byBjaGVjay5cbiAqIEByZXR1cm5zIHtib29sZWFufSBSZXR1cm5zIGB0cnVlYCBpZiBgdmFsdWVgIGlzIGFuIGBhcmd1bWVudHNgIG9iamVjdCxcbiAqL1xuZnVuY3Rpb24gYmFzZUlzQXJndW1lbnRzKHZhbHVlKSB7XG4gIHJldHVybiBpc09iamVjdExpa2UodmFsdWUpICYmIGJhc2VHZXRUYWcodmFsdWUpID09IGFyZ3NUYWc7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gYmFzZUlzQXJndW1lbnRzO1xuIiwidmFyIGJhc2VJc0VxdWFsRGVlcCA9IHJlcXVpcmUoJy4vX2Jhc2VJc0VxdWFsRGVlcCcpLFxuICAgIGlzT2JqZWN0TGlrZSA9IHJlcXVpcmUoJy4vaXNPYmplY3RMaWtlJyk7XG5cbi8qKlxuICogVGhlIGJhc2UgaW1wbGVtZW50YXRpb24gb2YgYF8uaXNFcXVhbGAgd2hpY2ggc3VwcG9ydHMgcGFydGlhbCBjb21wYXJpc29uc1xuICogYW5kIHRyYWNrcyB0cmF2ZXJzZWQgb2JqZWN0cy5cbiAqXG4gKiBAcHJpdmF0ZVxuICogQHBhcmFtIHsqfSB2YWx1ZSBUaGUgdmFsdWUgdG8gY29tcGFyZS5cbiAqIEBwYXJhbSB7Kn0gb3RoZXIgVGhlIG90aGVyIHZhbHVlIHRvIGNvbXBhcmUuXG4gKiBAcGFyYW0ge2Jvb2xlYW59IGJpdG1hc2sgVGhlIGJpdG1hc2sgZmxhZ3MuXG4gKiAgMSAtIFVub3JkZXJlZCBjb21wYXJpc29uXG4gKiAgMiAtIFBhcnRpYWwgY29tcGFyaXNvblxuICogQHBhcmFtIHtGdW5jdGlvbn0gW2N1c3RvbWl6ZXJdIFRoZSBmdW5jdGlvbiB0byBjdXN0b21pemUgY29tcGFyaXNvbnMuXG4gKiBAcGFyYW0ge09iamVjdH0gW3N0YWNrXSBUcmFja3MgdHJhdmVyc2VkIGB2YWx1ZWAgYW5kIGBvdGhlcmAgb2JqZWN0cy5cbiAqIEByZXR1cm5zIHtib29sZWFufSBSZXR1cm5zIGB0cnVlYCBpZiB0aGUgdmFsdWVzIGFyZSBlcXVpdmFsZW50LCBlbHNlIGBmYWxzZWAuXG4gKi9cbmZ1bmN0aW9uIGJhc2VJc0VxdWFsKHZhbHVlLCBvdGhlciwgYml0bWFzaywgY3VzdG9taXplciwgc3RhY2spIHtcbiAgaWYgKHZhbHVlID09PSBvdGhlcikge1xuICAgIHJldHVybiB0cnVlO1xuICB9XG4gIGlmICh2YWx1ZSA9PSBudWxsIHx8IG90aGVyID09IG51bGwgfHwgKCFpc09iamVjdExpa2UodmFsdWUpICYmICFpc09iamVjdExpa2Uob3RoZXIpKSkge1xuICAgIHJldHVybiB2YWx1ZSAhPT0gdmFsdWUgJiYgb3RoZXIgIT09IG90aGVyO1xuICB9XG4gIHJldHVybiBiYXNlSXNFcXVhbERlZXAodmFsdWUsIG90aGVyLCBiaXRtYXNrLCBjdXN0b21pemVyLCBiYXNlSXNFcXVhbCwgc3RhY2spO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IGJhc2VJc0VxdWFsO1xuIiwidmFyIFN0YWNrID0gcmVxdWlyZSgnLi9fU3RhY2snKSxcbiAgICBlcXVhbEFycmF5cyA9IHJlcXVpcmUoJy4vX2VxdWFsQXJyYXlzJyksXG4gICAgZXF1YWxCeVRhZyA9IHJlcXVpcmUoJy4vX2VxdWFsQnlUYWcnKSxcbiAgICBlcXVhbE9iamVjdHMgPSByZXF1aXJlKCcuL19lcXVhbE9iamVjdHMnKSxcbiAgICBnZXRUYWcgPSByZXF1aXJlKCcuL19nZXRUYWcnKSxcbiAgICBpc0FycmF5ID0gcmVxdWlyZSgnLi9pc0FycmF5JyksXG4gICAgaXNCdWZmZXIgPSByZXF1aXJlKCcuL2lzQnVmZmVyJyksXG4gICAgaXNUeXBlZEFycmF5ID0gcmVxdWlyZSgnLi9pc1R5cGVkQXJyYXknKTtcblxuLyoqIFVzZWQgdG8gY29tcG9zZSBiaXRtYXNrcyBmb3IgdmFsdWUgY29tcGFyaXNvbnMuICovXG52YXIgQ09NUEFSRV9QQVJUSUFMX0ZMQUcgPSAxO1xuXG4vKiogYE9iamVjdCN0b1N0cmluZ2AgcmVzdWx0IHJlZmVyZW5jZXMuICovXG52YXIgYXJnc1RhZyA9ICdbb2JqZWN0IEFyZ3VtZW50c10nLFxuICAgIGFycmF5VGFnID0gJ1tvYmplY3QgQXJyYXldJyxcbiAgICBvYmplY3RUYWcgPSAnW29iamVjdCBPYmplY3RdJztcblxuLyoqIFVzZWQgZm9yIGJ1aWx0LWluIG1ldGhvZCByZWZlcmVuY2VzLiAqL1xudmFyIG9iamVjdFByb3RvID0gT2JqZWN0LnByb3RvdHlwZTtcblxuLyoqIFVzZWQgdG8gY2hlY2sgb2JqZWN0cyBmb3Igb3duIHByb3BlcnRpZXMuICovXG52YXIgaGFzT3duUHJvcGVydHkgPSBvYmplY3RQcm90by5oYXNPd25Qcm9wZXJ0eTtcblxuLyoqXG4gKiBBIHNwZWNpYWxpemVkIHZlcnNpb24gb2YgYGJhc2VJc0VxdWFsYCBmb3IgYXJyYXlzIGFuZCBvYmplY3RzIHdoaWNoIHBlcmZvcm1zXG4gKiBkZWVwIGNvbXBhcmlzb25zIGFuZCB0cmFja3MgdHJhdmVyc2VkIG9iamVjdHMgZW5hYmxpbmcgb2JqZWN0cyB3aXRoIGNpcmN1bGFyXG4gKiByZWZlcmVuY2VzIHRvIGJlIGNvbXBhcmVkLlxuICpcbiAqIEBwcml2YXRlXG4gKiBAcGFyYW0ge09iamVjdH0gb2JqZWN0IFRoZSBvYmplY3QgdG8gY29tcGFyZS5cbiAqIEBwYXJhbSB7T2JqZWN0fSBvdGhlciBUaGUgb3RoZXIgb2JqZWN0IHRvIGNvbXBhcmUuXG4gKiBAcGFyYW0ge251bWJlcn0gYml0bWFzayBUaGUgYml0bWFzayBmbGFncy4gU2VlIGBiYXNlSXNFcXVhbGAgZm9yIG1vcmUgZGV0YWlscy5cbiAqIEBwYXJhbSB7RnVuY3Rpb259IGN1c3RvbWl6ZXIgVGhlIGZ1bmN0aW9uIHRvIGN1c3RvbWl6ZSBjb21wYXJpc29ucy5cbiAqIEBwYXJhbSB7RnVuY3Rpb259IGVxdWFsRnVuYyBUaGUgZnVuY3Rpb24gdG8gZGV0ZXJtaW5lIGVxdWl2YWxlbnRzIG9mIHZhbHVlcy5cbiAqIEBwYXJhbSB7T2JqZWN0fSBbc3RhY2tdIFRyYWNrcyB0cmF2ZXJzZWQgYG9iamVjdGAgYW5kIGBvdGhlcmAgb2JqZWN0cy5cbiAqIEByZXR1cm5zIHtib29sZWFufSBSZXR1cm5zIGB0cnVlYCBpZiB0aGUgb2JqZWN0cyBhcmUgZXF1aXZhbGVudCwgZWxzZSBgZmFsc2VgLlxuICovXG5mdW5jdGlvbiBiYXNlSXNFcXVhbERlZXAob2JqZWN0LCBvdGhlciwgYml0bWFzaywgY3VzdG9taXplciwgZXF1YWxGdW5jLCBzdGFjaykge1xuICB2YXIgb2JqSXNBcnIgPSBpc0FycmF5KG9iamVjdCksXG4gICAgICBvdGhJc0FyciA9IGlzQXJyYXkob3RoZXIpLFxuICAgICAgb2JqVGFnID0gb2JqSXNBcnIgPyBhcnJheVRhZyA6IGdldFRhZyhvYmplY3QpLFxuICAgICAgb3RoVGFnID0gb3RoSXNBcnIgPyBhcnJheVRhZyA6IGdldFRhZyhvdGhlcik7XG5cbiAgb2JqVGFnID0gb2JqVGFnID09IGFyZ3NUYWcgPyBvYmplY3RUYWcgOiBvYmpUYWc7XG4gIG90aFRhZyA9IG90aFRhZyA9PSBhcmdzVGFnID8gb2JqZWN0VGFnIDogb3RoVGFnO1xuXG4gIHZhciBvYmpJc09iaiA9IG9ialRhZyA9PSBvYmplY3RUYWcsXG4gICAgICBvdGhJc09iaiA9IG90aFRhZyA9PSBvYmplY3RUYWcsXG4gICAgICBpc1NhbWVUYWcgPSBvYmpUYWcgPT0gb3RoVGFnO1xuXG4gIGlmIChpc1NhbWVUYWcgJiYgaXNCdWZmZXIob2JqZWN0KSkge1xuICAgIGlmICghaXNCdWZmZXIob3RoZXIpKSB7XG4gICAgICByZXR1cm4gZmFsc2U7XG4gICAgfVxuICAgIG9iaklzQXJyID0gdHJ1ZTtcbiAgICBvYmpJc09iaiA9IGZhbHNlO1xuICB9XG4gIGlmIChpc1NhbWVUYWcgJiYgIW9iaklzT2JqKSB7XG4gICAgc3RhY2sgfHwgKHN0YWNrID0gbmV3IFN0YWNrKTtcbiAgICByZXR1cm4gKG9iaklzQXJyIHx8IGlzVHlwZWRBcnJheShvYmplY3QpKVxuICAgICAgPyBlcXVhbEFycmF5cyhvYmplY3QsIG90aGVyLCBiaXRtYXNrLCBjdXN0b21pemVyLCBlcXVhbEZ1bmMsIHN0YWNrKVxuICAgICAgOiBlcXVhbEJ5VGFnKG9iamVjdCwgb3RoZXIsIG9ialRhZywgYml0bWFzaywgY3VzdG9taXplciwgZXF1YWxGdW5jLCBzdGFjayk7XG4gIH1cbiAgaWYgKCEoYml0bWFzayAmIENPTVBBUkVfUEFSVElBTF9GTEFHKSkge1xuICAgIHZhciBvYmpJc1dyYXBwZWQgPSBvYmpJc09iaiAmJiBoYXNPd25Qcm9wZXJ0eS5jYWxsKG9iamVjdCwgJ19fd3JhcHBlZF9fJyksXG4gICAgICAgIG90aElzV3JhcHBlZCA9IG90aElzT2JqICYmIGhhc093blByb3BlcnR5LmNhbGwob3RoZXIsICdfX3dyYXBwZWRfXycpO1xuXG4gICAgaWYgKG9iaklzV3JhcHBlZCB8fCBvdGhJc1dyYXBwZWQpIHtcbiAgICAgIHZhciBvYmpVbndyYXBwZWQgPSBvYmpJc1dyYXBwZWQgPyBvYmplY3QudmFsdWUoKSA6IG9iamVjdCxcbiAgICAgICAgICBvdGhVbndyYXBwZWQgPSBvdGhJc1dyYXBwZWQgPyBvdGhlci52YWx1ZSgpIDogb3RoZXI7XG5cbiAgICAgIHN0YWNrIHx8IChzdGFjayA9IG5ldyBTdGFjayk7XG4gICAgICByZXR1cm4gZXF1YWxGdW5jKG9ialVud3JhcHBlZCwgb3RoVW53cmFwcGVkLCBiaXRtYXNrLCBjdXN0b21pemVyLCBzdGFjayk7XG4gICAgfVxuICB9XG4gIGlmICghaXNTYW1lVGFnKSB7XG4gICAgcmV0dXJuIGZhbHNlO1xuICB9XG4gIHN0YWNrIHx8IChzdGFjayA9IG5ldyBTdGFjayk7XG4gIHJldHVybiBlcXVhbE9iamVjdHMob2JqZWN0LCBvdGhlciwgYml0bWFzaywgY3VzdG9taXplciwgZXF1YWxGdW5jLCBzdGFjayk7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gYmFzZUlzRXF1YWxEZWVwO1xuIiwidmFyIFN0YWNrID0gcmVxdWlyZSgnLi9fU3RhY2snKSxcbiAgICBiYXNlSXNFcXVhbCA9IHJlcXVpcmUoJy4vX2Jhc2VJc0VxdWFsJyk7XG5cbi8qKiBVc2VkIHRvIGNvbXBvc2UgYml0bWFza3MgZm9yIHZhbHVlIGNvbXBhcmlzb25zLiAqL1xudmFyIENPTVBBUkVfUEFSVElBTF9GTEFHID0gMSxcbiAgICBDT01QQVJFX1VOT1JERVJFRF9GTEFHID0gMjtcblxuLyoqXG4gKiBUaGUgYmFzZSBpbXBsZW1lbnRhdGlvbiBvZiBgXy5pc01hdGNoYCB3aXRob3V0IHN1cHBvcnQgZm9yIGl0ZXJhdGVlIHNob3J0aGFuZHMuXG4gKlxuICogQHByaXZhdGVcbiAqIEBwYXJhbSB7T2JqZWN0fSBvYmplY3QgVGhlIG9iamVjdCB0byBpbnNwZWN0LlxuICogQHBhcmFtIHtPYmplY3R9IHNvdXJjZSBUaGUgb2JqZWN0IG9mIHByb3BlcnR5IHZhbHVlcyB0byBtYXRjaC5cbiAqIEBwYXJhbSB7QXJyYXl9IG1hdGNoRGF0YSBUaGUgcHJvcGVydHkgbmFtZXMsIHZhbHVlcywgYW5kIGNvbXBhcmUgZmxhZ3MgdG8gbWF0Y2guXG4gKiBAcGFyYW0ge0Z1bmN0aW9ufSBbY3VzdG9taXplcl0gVGhlIGZ1bmN0aW9uIHRvIGN1c3RvbWl6ZSBjb21wYXJpc29ucy5cbiAqIEByZXR1cm5zIHtib29sZWFufSBSZXR1cm5zIGB0cnVlYCBpZiBgb2JqZWN0YCBpcyBhIG1hdGNoLCBlbHNlIGBmYWxzZWAuXG4gKi9cbmZ1bmN0aW9uIGJhc2VJc01hdGNoKG9iamVjdCwgc291cmNlLCBtYXRjaERhdGEsIGN1c3RvbWl6ZXIpIHtcbiAgdmFyIGluZGV4ID0gbWF0Y2hEYXRhLmxlbmd0aCxcbiAgICAgIGxlbmd0aCA9IGluZGV4LFxuICAgICAgbm9DdXN0b21pemVyID0gIWN1c3RvbWl6ZXI7XG5cbiAgaWYgKG9iamVjdCA9PSBudWxsKSB7XG4gICAgcmV0dXJuICFsZW5ndGg7XG4gIH1cbiAgb2JqZWN0ID0gT2JqZWN0KG9iamVjdCk7XG4gIHdoaWxlIChpbmRleC0tKSB7XG4gICAgdmFyIGRhdGEgPSBtYXRjaERhdGFbaW5kZXhdO1xuICAgIGlmICgobm9DdXN0b21pemVyICYmIGRhdGFbMl0pXG4gICAgICAgICAgPyBkYXRhWzFdICE9PSBvYmplY3RbZGF0YVswXV1cbiAgICAgICAgICA6ICEoZGF0YVswXSBpbiBvYmplY3QpXG4gICAgICAgICkge1xuICAgICAgcmV0dXJuIGZhbHNlO1xuICAgIH1cbiAgfVxuICB3aGlsZSAoKytpbmRleCA8IGxlbmd0aCkge1xuICAgIGRhdGEgPSBtYXRjaERhdGFbaW5kZXhdO1xuICAgIHZhciBrZXkgPSBkYXRhWzBdLFxuICAgICAgICBvYmpWYWx1ZSA9IG9iamVjdFtrZXldLFxuICAgICAgICBzcmNWYWx1ZSA9IGRhdGFbMV07XG5cbiAgICBpZiAobm9DdXN0b21pemVyICYmIGRhdGFbMl0pIHtcbiAgICAgIGlmIChvYmpWYWx1ZSA9PT0gdW5kZWZpbmVkICYmICEoa2V5IGluIG9iamVjdCkpIHtcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgfVxuICAgIH0gZWxzZSB7XG4gICAgICB2YXIgc3RhY2sgPSBuZXcgU3RhY2s7XG4gICAgICBpZiAoY3VzdG9taXplcikge1xuICAgICAgICB2YXIgcmVzdWx0ID0gY3VzdG9taXplcihvYmpWYWx1ZSwgc3JjVmFsdWUsIGtleSwgb2JqZWN0LCBzb3VyY2UsIHN0YWNrKTtcbiAgICAgIH1cbiAgICAgIGlmICghKHJlc3VsdCA9PT0gdW5kZWZpbmVkXG4gICAgICAgICAgICA/IGJhc2VJc0VxdWFsKHNyY1ZhbHVlLCBvYmpWYWx1ZSwgQ09NUEFSRV9QQVJUSUFMX0ZMQUcgfCBDT01QQVJFX1VOT1JERVJFRF9GTEFHLCBjdXN0b21pemVyLCBzdGFjaylcbiAgICAgICAgICAgIDogcmVzdWx0XG4gICAgICAgICAgKSkge1xuICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICB9XG4gICAgfVxuICB9XG4gIHJldHVybiB0cnVlO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IGJhc2VJc01hdGNoO1xuIiwidmFyIGlzRnVuY3Rpb24gPSByZXF1aXJlKCcuL2lzRnVuY3Rpb24nKSxcbiAgICBpc01hc2tlZCA9IHJlcXVpcmUoJy4vX2lzTWFza2VkJyksXG4gICAgaXNPYmplY3QgPSByZXF1aXJlKCcuL2lzT2JqZWN0JyksXG4gICAgdG9Tb3VyY2UgPSByZXF1aXJlKCcuL190b1NvdXJjZScpO1xuXG4vKipcbiAqIFVzZWQgdG8gbWF0Y2ggYFJlZ0V4cGBcbiAqIFtzeW50YXggY2hhcmFjdGVyc10oaHR0cDovL2VjbWEtaW50ZXJuYXRpb25hbC5vcmcvZWNtYS0yNjIvNy4wLyNzZWMtcGF0dGVybnMpLlxuICovXG52YXIgcmVSZWdFeHBDaGFyID0gL1tcXFxcXiQuKis/KClbXFxde318XS9nO1xuXG4vKiogVXNlZCB0byBkZXRlY3QgaG9zdCBjb25zdHJ1Y3RvcnMgKFNhZmFyaSkuICovXG52YXIgcmVJc0hvc3RDdG9yID0gL15cXFtvYmplY3QgLis/Q29uc3RydWN0b3JcXF0kLztcblxuLyoqIFVzZWQgZm9yIGJ1aWx0LWluIG1ldGhvZCByZWZlcmVuY2VzLiAqL1xudmFyIGZ1bmNQcm90byA9IEZ1bmN0aW9uLnByb3RvdHlwZSxcbiAgICBvYmplY3RQcm90byA9IE9iamVjdC5wcm90b3R5cGU7XG5cbi8qKiBVc2VkIHRvIHJlc29sdmUgdGhlIGRlY29tcGlsZWQgc291cmNlIG9mIGZ1bmN0aW9ucy4gKi9cbnZhciBmdW5jVG9TdHJpbmcgPSBmdW5jUHJvdG8udG9TdHJpbmc7XG5cbi8qKiBVc2VkIHRvIGNoZWNrIG9iamVjdHMgZm9yIG93biBwcm9wZXJ0aWVzLiAqL1xudmFyIGhhc093blByb3BlcnR5ID0gb2JqZWN0UHJvdG8uaGFzT3duUHJvcGVydHk7XG5cbi8qKiBVc2VkIHRvIGRldGVjdCBpZiBhIG1ldGhvZCBpcyBuYXRpdmUuICovXG52YXIgcmVJc05hdGl2ZSA9IFJlZ0V4cCgnXicgK1xuICBmdW5jVG9TdHJpbmcuY2FsbChoYXNPd25Qcm9wZXJ0eSkucmVwbGFjZShyZVJlZ0V4cENoYXIsICdcXFxcJCYnKVxuICAucmVwbGFjZSgvaGFzT3duUHJvcGVydHl8KGZ1bmN0aW9uKS4qPyg/PVxcXFxcXCgpfCBmb3IgLis/KD89XFxcXFxcXSkvZywgJyQxLio/JykgKyAnJCdcbik7XG5cbi8qKlxuICogVGhlIGJhc2UgaW1wbGVtZW50YXRpb24gb2YgYF8uaXNOYXRpdmVgIHdpdGhvdXQgYmFkIHNoaW0gY2hlY2tzLlxuICpcbiAqIEBwcml2YXRlXG4gKiBAcGFyYW0geyp9IHZhbHVlIFRoZSB2YWx1ZSB0byBjaGVjay5cbiAqIEByZXR1cm5zIHtib29sZWFufSBSZXR1cm5zIGB0cnVlYCBpZiBgdmFsdWVgIGlzIGEgbmF0aXZlIGZ1bmN0aW9uLFxuICogIGVsc2UgYGZhbHNlYC5cbiAqL1xuZnVuY3Rpb24gYmFzZUlzTmF0aXZlKHZhbHVlKSB7XG4gIGlmICghaXNPYmplY3QodmFsdWUpIHx8IGlzTWFza2VkKHZhbHVlKSkge1xuICAgIHJldHVybiBmYWxzZTtcbiAgfVxuICB2YXIgcGF0dGVybiA9IGlzRnVuY3Rpb24odmFsdWUpID8gcmVJc05hdGl2ZSA6IHJlSXNIb3N0Q3RvcjtcbiAgcmV0dXJuIHBhdHRlcm4udGVzdCh0b1NvdXJjZSh2YWx1ZSkpO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IGJhc2VJc05hdGl2ZTtcbiIsInZhciBiYXNlR2V0VGFnID0gcmVxdWlyZSgnLi9fYmFzZUdldFRhZycpLFxuICAgIGlzTGVuZ3RoID0gcmVxdWlyZSgnLi9pc0xlbmd0aCcpLFxuICAgIGlzT2JqZWN0TGlrZSA9IHJlcXVpcmUoJy4vaXNPYmplY3RMaWtlJyk7XG5cbi8qKiBgT2JqZWN0I3RvU3RyaW5nYCByZXN1bHQgcmVmZXJlbmNlcy4gKi9cbnZhciBhcmdzVGFnID0gJ1tvYmplY3QgQXJndW1lbnRzXScsXG4gICAgYXJyYXlUYWcgPSAnW29iamVjdCBBcnJheV0nLFxuICAgIGJvb2xUYWcgPSAnW29iamVjdCBCb29sZWFuXScsXG4gICAgZGF0ZVRhZyA9ICdbb2JqZWN0IERhdGVdJyxcbiAgICBlcnJvclRhZyA9ICdbb2JqZWN0IEVycm9yXScsXG4gICAgZnVuY1RhZyA9ICdbb2JqZWN0IEZ1bmN0aW9uXScsXG4gICAgbWFwVGFnID0gJ1tvYmplY3QgTWFwXScsXG4gICAgbnVtYmVyVGFnID0gJ1tvYmplY3QgTnVtYmVyXScsXG4gICAgb2JqZWN0VGFnID0gJ1tvYmplY3QgT2JqZWN0XScsXG4gICAgcmVnZXhwVGFnID0gJ1tvYmplY3QgUmVnRXhwXScsXG4gICAgc2V0VGFnID0gJ1tvYmplY3QgU2V0XScsXG4gICAgc3RyaW5nVGFnID0gJ1tvYmplY3QgU3RyaW5nXScsXG4gICAgd2Vha01hcFRhZyA9ICdbb2JqZWN0IFdlYWtNYXBdJztcblxudmFyIGFycmF5QnVmZmVyVGFnID0gJ1tvYmplY3QgQXJyYXlCdWZmZXJdJyxcbiAgICBkYXRhVmlld1RhZyA9ICdbb2JqZWN0IERhdGFWaWV3XScsXG4gICAgZmxvYXQzMlRhZyA9ICdbb2JqZWN0IEZsb2F0MzJBcnJheV0nLFxuICAgIGZsb2F0NjRUYWcgPSAnW29iamVjdCBGbG9hdDY0QXJyYXldJyxcbiAgICBpbnQ4VGFnID0gJ1tvYmplY3QgSW50OEFycmF5XScsXG4gICAgaW50MTZUYWcgPSAnW29iamVjdCBJbnQxNkFycmF5XScsXG4gICAgaW50MzJUYWcgPSAnW29iamVjdCBJbnQzMkFycmF5XScsXG4gICAgdWludDhUYWcgPSAnW29iamVjdCBVaW50OEFycmF5XScsXG4gICAgdWludDhDbGFtcGVkVGFnID0gJ1tvYmplY3QgVWludDhDbGFtcGVkQXJyYXldJyxcbiAgICB1aW50MTZUYWcgPSAnW29iamVjdCBVaW50MTZBcnJheV0nLFxuICAgIHVpbnQzMlRhZyA9ICdbb2JqZWN0IFVpbnQzMkFycmF5XSc7XG5cbi8qKiBVc2VkIHRvIGlkZW50aWZ5IGB0b1N0cmluZ1RhZ2AgdmFsdWVzIG9mIHR5cGVkIGFycmF5cy4gKi9cbnZhciB0eXBlZEFycmF5VGFncyA9IHt9O1xudHlwZWRBcnJheVRhZ3NbZmxvYXQzMlRhZ10gPSB0eXBlZEFycmF5VGFnc1tmbG9hdDY0VGFnXSA9XG50eXBlZEFycmF5VGFnc1tpbnQ4VGFnXSA9IHR5cGVkQXJyYXlUYWdzW2ludDE2VGFnXSA9XG50eXBlZEFycmF5VGFnc1tpbnQzMlRhZ10gPSB0eXBlZEFycmF5VGFnc1t1aW50OFRhZ10gPVxudHlwZWRBcnJheVRhZ3NbdWludDhDbGFtcGVkVGFnXSA9IHR5cGVkQXJyYXlUYWdzW3VpbnQxNlRhZ10gPVxudHlwZWRBcnJheVRhZ3NbdWludDMyVGFnXSA9IHRydWU7XG50eXBlZEFycmF5VGFnc1thcmdzVGFnXSA9IHR5cGVkQXJyYXlUYWdzW2FycmF5VGFnXSA9XG50eXBlZEFycmF5VGFnc1thcnJheUJ1ZmZlclRhZ10gPSB0eXBlZEFycmF5VGFnc1tib29sVGFnXSA9XG50eXBlZEFycmF5VGFnc1tkYXRhVmlld1RhZ10gPSB0eXBlZEFycmF5VGFnc1tkYXRlVGFnXSA9XG50eXBlZEFycmF5VGFnc1tlcnJvclRhZ10gPSB0eXBlZEFycmF5VGFnc1tmdW5jVGFnXSA9XG50eXBlZEFycmF5VGFnc1ttYXBUYWddID0gdHlwZWRBcnJheVRhZ3NbbnVtYmVyVGFnXSA9XG50eXBlZEFycmF5VGFnc1tvYmplY3RUYWddID0gdHlwZWRBcnJheVRhZ3NbcmVnZXhwVGFnXSA9XG50eXBlZEFycmF5VGFnc1tzZXRUYWddID0gdHlwZWRBcnJheVRhZ3Nbc3RyaW5nVGFnXSA9XG50eXBlZEFycmF5VGFnc1t3ZWFrTWFwVGFnXSA9IGZhbHNlO1xuXG4vKipcbiAqIFRoZSBiYXNlIGltcGxlbWVudGF0aW9uIG9mIGBfLmlzVHlwZWRBcnJheWAgd2l0aG91dCBOb2RlLmpzIG9wdGltaXphdGlvbnMuXG4gKlxuICogQHByaXZhdGVcbiAqIEBwYXJhbSB7Kn0gdmFsdWUgVGhlIHZhbHVlIHRvIGNoZWNrLlxuICogQHJldHVybnMge2Jvb2xlYW59IFJldHVybnMgYHRydWVgIGlmIGB2YWx1ZWAgaXMgYSB0eXBlZCBhcnJheSwgZWxzZSBgZmFsc2VgLlxuICovXG5mdW5jdGlvbiBiYXNlSXNUeXBlZEFycmF5KHZhbHVlKSB7XG4gIHJldHVybiBpc09iamVjdExpa2UodmFsdWUpICYmXG4gICAgaXNMZW5ndGgodmFsdWUubGVuZ3RoKSAmJiAhIXR5cGVkQXJyYXlUYWdzW2Jhc2VHZXRUYWcodmFsdWUpXTtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBiYXNlSXNUeXBlZEFycmF5O1xuIiwidmFyIGJhc2VNYXRjaGVzID0gcmVxdWlyZSgnLi9fYmFzZU1hdGNoZXMnKSxcbiAgICBiYXNlTWF0Y2hlc1Byb3BlcnR5ID0gcmVxdWlyZSgnLi9fYmFzZU1hdGNoZXNQcm9wZXJ0eScpLFxuICAgIGlkZW50aXR5ID0gcmVxdWlyZSgnLi9pZGVudGl0eScpLFxuICAgIGlzQXJyYXkgPSByZXF1aXJlKCcuL2lzQXJyYXknKSxcbiAgICBwcm9wZXJ0eSA9IHJlcXVpcmUoJy4vcHJvcGVydHknKTtcblxuLyoqXG4gKiBUaGUgYmFzZSBpbXBsZW1lbnRhdGlvbiBvZiBgXy5pdGVyYXRlZWAuXG4gKlxuICogQHByaXZhdGVcbiAqIEBwYXJhbSB7Kn0gW3ZhbHVlPV8uaWRlbnRpdHldIFRoZSB2YWx1ZSB0byBjb252ZXJ0IHRvIGFuIGl0ZXJhdGVlLlxuICogQHJldHVybnMge0Z1bmN0aW9ufSBSZXR1cm5zIHRoZSBpdGVyYXRlZS5cbiAqL1xuZnVuY3Rpb24gYmFzZUl0ZXJhdGVlKHZhbHVlKSB7XG4gIC8vIERvbid0IHN0b3JlIHRoZSBgdHlwZW9mYCByZXN1bHQgaW4gYSB2YXJpYWJsZSB0byBhdm9pZCBhIEpJVCBidWcgaW4gU2FmYXJpIDkuXG4gIC8vIFNlZSBodHRwczovL2J1Z3Mud2Via2l0Lm9yZy9zaG93X2J1Zy5jZ2k/aWQ9MTU2MDM0IGZvciBtb3JlIGRldGFpbHMuXG4gIGlmICh0eXBlb2YgdmFsdWUgPT0gJ2Z1bmN0aW9uJykge1xuICAgIHJldHVybiB2YWx1ZTtcbiAgfVxuICBpZiAodmFsdWUgPT0gbnVsbCkge1xuICAgIHJldHVybiBpZGVudGl0eTtcbiAgfVxuICBpZiAodHlwZW9mIHZhbHVlID09ICdvYmplY3QnKSB7XG4gICAgcmV0dXJuIGlzQXJyYXkodmFsdWUpXG4gICAgICA/IGJhc2VNYXRjaGVzUHJvcGVydHkodmFsdWVbMF0sIHZhbHVlWzFdKVxuICAgICAgOiBiYXNlTWF0Y2hlcyh2YWx1ZSk7XG4gIH1cbiAgcmV0dXJuIHByb3BlcnR5KHZhbHVlKTtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBiYXNlSXRlcmF0ZWU7XG4iLCJ2YXIgaXNQcm90b3R5cGUgPSByZXF1aXJlKCcuL19pc1Byb3RvdHlwZScpLFxuICAgIG5hdGl2ZUtleXMgPSByZXF1aXJlKCcuL19uYXRpdmVLZXlzJyk7XG5cbi8qKiBVc2VkIGZvciBidWlsdC1pbiBtZXRob2QgcmVmZXJlbmNlcy4gKi9cbnZhciBvYmplY3RQcm90byA9IE9iamVjdC5wcm90b3R5cGU7XG5cbi8qKiBVc2VkIHRvIGNoZWNrIG9iamVjdHMgZm9yIG93biBwcm9wZXJ0aWVzLiAqL1xudmFyIGhhc093blByb3BlcnR5ID0gb2JqZWN0UHJvdG8uaGFzT3duUHJvcGVydHk7XG5cbi8qKlxuICogVGhlIGJhc2UgaW1wbGVtZW50YXRpb24gb2YgYF8ua2V5c2Agd2hpY2ggZG9lc24ndCB0cmVhdCBzcGFyc2UgYXJyYXlzIGFzIGRlbnNlLlxuICpcbiAqIEBwcml2YXRlXG4gKiBAcGFyYW0ge09iamVjdH0gb2JqZWN0IFRoZSBvYmplY3QgdG8gcXVlcnkuXG4gKiBAcmV0dXJucyB7QXJyYXl9IFJldHVybnMgdGhlIGFycmF5IG9mIHByb3BlcnR5IG5hbWVzLlxuICovXG5mdW5jdGlvbiBiYXNlS2V5cyhvYmplY3QpIHtcbiAgaWYgKCFpc1Byb3RvdHlwZShvYmplY3QpKSB7XG4gICAgcmV0dXJuIG5hdGl2ZUtleXMob2JqZWN0KTtcbiAgfVxuICB2YXIgcmVzdWx0ID0gW107XG4gIGZvciAodmFyIGtleSBpbiBPYmplY3Qob2JqZWN0KSkge1xuICAgIGlmIChoYXNPd25Qcm9wZXJ0eS5jYWxsKG9iamVjdCwga2V5KSAmJiBrZXkgIT0gJ2NvbnN0cnVjdG9yJykge1xuICAgICAgcmVzdWx0LnB1c2goa2V5KTtcbiAgICB9XG4gIH1cbiAgcmV0dXJuIHJlc3VsdDtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBiYXNlS2V5cztcbiIsInZhciBiYXNlSXNNYXRjaCA9IHJlcXVpcmUoJy4vX2Jhc2VJc01hdGNoJyksXG4gICAgZ2V0TWF0Y2hEYXRhID0gcmVxdWlyZSgnLi9fZ2V0TWF0Y2hEYXRhJyksXG4gICAgbWF0Y2hlc1N0cmljdENvbXBhcmFibGUgPSByZXF1aXJlKCcuL19tYXRjaGVzU3RyaWN0Q29tcGFyYWJsZScpO1xuXG4vKipcbiAqIFRoZSBiYXNlIGltcGxlbWVudGF0aW9uIG9mIGBfLm1hdGNoZXNgIHdoaWNoIGRvZXNuJ3QgY2xvbmUgYHNvdXJjZWAuXG4gKlxuICogQHByaXZhdGVcbiAqIEBwYXJhbSB7T2JqZWN0fSBzb3VyY2UgVGhlIG9iamVjdCBvZiBwcm9wZXJ0eSB2YWx1ZXMgdG8gbWF0Y2guXG4gKiBAcmV0dXJucyB7RnVuY3Rpb259IFJldHVybnMgdGhlIG5ldyBzcGVjIGZ1bmN0aW9uLlxuICovXG5mdW5jdGlvbiBiYXNlTWF0Y2hlcyhzb3VyY2UpIHtcbiAgdmFyIG1hdGNoRGF0YSA9IGdldE1hdGNoRGF0YShzb3VyY2UpO1xuICBpZiAobWF0Y2hEYXRhLmxlbmd0aCA9PSAxICYmIG1hdGNoRGF0YVswXVsyXSkge1xuICAgIHJldHVybiBtYXRjaGVzU3RyaWN0Q29tcGFyYWJsZShtYXRjaERhdGFbMF1bMF0sIG1hdGNoRGF0YVswXVsxXSk7XG4gIH1cbiAgcmV0dXJuIGZ1bmN0aW9uKG9iamVjdCkge1xuICAgIHJldHVybiBvYmplY3QgPT09IHNvdXJjZSB8fCBiYXNlSXNNYXRjaChvYmplY3QsIHNvdXJjZSwgbWF0Y2hEYXRhKTtcbiAgfTtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBiYXNlTWF0Y2hlcztcbiIsInZhciBiYXNlSXNFcXVhbCA9IHJlcXVpcmUoJy4vX2Jhc2VJc0VxdWFsJyksXG4gICAgZ2V0ID0gcmVxdWlyZSgnLi9nZXQnKSxcbiAgICBoYXNJbiA9IHJlcXVpcmUoJy4vaGFzSW4nKSxcbiAgICBpc0tleSA9IHJlcXVpcmUoJy4vX2lzS2V5JyksXG4gICAgaXNTdHJpY3RDb21wYXJhYmxlID0gcmVxdWlyZSgnLi9faXNTdHJpY3RDb21wYXJhYmxlJyksXG4gICAgbWF0Y2hlc1N0cmljdENvbXBhcmFibGUgPSByZXF1aXJlKCcuL19tYXRjaGVzU3RyaWN0Q29tcGFyYWJsZScpLFxuICAgIHRvS2V5ID0gcmVxdWlyZSgnLi9fdG9LZXknKTtcblxuLyoqIFVzZWQgdG8gY29tcG9zZSBiaXRtYXNrcyBmb3IgdmFsdWUgY29tcGFyaXNvbnMuICovXG52YXIgQ09NUEFSRV9QQVJUSUFMX0ZMQUcgPSAxLFxuICAgIENPTVBBUkVfVU5PUkRFUkVEX0ZMQUcgPSAyO1xuXG4vKipcbiAqIFRoZSBiYXNlIGltcGxlbWVudGF0aW9uIG9mIGBfLm1hdGNoZXNQcm9wZXJ0eWAgd2hpY2ggZG9lc24ndCBjbG9uZSBgc3JjVmFsdWVgLlxuICpcbiAqIEBwcml2YXRlXG4gKiBAcGFyYW0ge3N0cmluZ30gcGF0aCBUaGUgcGF0aCBvZiB0aGUgcHJvcGVydHkgdG8gZ2V0LlxuICogQHBhcmFtIHsqfSBzcmNWYWx1ZSBUaGUgdmFsdWUgdG8gbWF0Y2guXG4gKiBAcmV0dXJucyB7RnVuY3Rpb259IFJldHVybnMgdGhlIG5ldyBzcGVjIGZ1bmN0aW9uLlxuICovXG5mdW5jdGlvbiBiYXNlTWF0Y2hlc1Byb3BlcnR5KHBhdGgsIHNyY1ZhbHVlKSB7XG4gIGlmIChpc0tleShwYXRoKSAmJiBpc1N0cmljdENvbXBhcmFibGUoc3JjVmFsdWUpKSB7XG4gICAgcmV0dXJuIG1hdGNoZXNTdHJpY3RDb21wYXJhYmxlKHRvS2V5KHBhdGgpLCBzcmNWYWx1ZSk7XG4gIH1cbiAgcmV0dXJuIGZ1bmN0aW9uKG9iamVjdCkge1xuICAgIHZhciBvYmpWYWx1ZSA9IGdldChvYmplY3QsIHBhdGgpO1xuICAgIHJldHVybiAob2JqVmFsdWUgPT09IHVuZGVmaW5lZCAmJiBvYmpWYWx1ZSA9PT0gc3JjVmFsdWUpXG4gICAgICA/IGhhc0luKG9iamVjdCwgcGF0aClcbiAgICAgIDogYmFzZUlzRXF1YWwoc3JjVmFsdWUsIG9ialZhbHVlLCBDT01QQVJFX1BBUlRJQUxfRkxBRyB8IENPTVBBUkVfVU5PUkRFUkVEX0ZMQUcpO1xuICB9O1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IGJhc2VNYXRjaGVzUHJvcGVydHk7XG4iLCIvKipcbiAqIFRoZSBiYXNlIGltcGxlbWVudGF0aW9uIG9mIGBfLnByb3BlcnR5YCB3aXRob3V0IHN1cHBvcnQgZm9yIGRlZXAgcGF0aHMuXG4gKlxuICogQHByaXZhdGVcbiAqIEBwYXJhbSB7c3RyaW5nfSBrZXkgVGhlIGtleSBvZiB0aGUgcHJvcGVydHkgdG8gZ2V0LlxuICogQHJldHVybnMge0Z1bmN0aW9ufSBSZXR1cm5zIHRoZSBuZXcgYWNjZXNzb3IgZnVuY3Rpb24uXG4gKi9cbmZ1bmN0aW9uIGJhc2VQcm9wZXJ0eShrZXkpIHtcbiAgcmV0dXJuIGZ1bmN0aW9uKG9iamVjdCkge1xuICAgIHJldHVybiBvYmplY3QgPT0gbnVsbCA/IHVuZGVmaW5lZCA6IG9iamVjdFtrZXldO1xuICB9O1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IGJhc2VQcm9wZXJ0eTtcbiIsInZhciBiYXNlR2V0ID0gcmVxdWlyZSgnLi9fYmFzZUdldCcpO1xuXG4vKipcbiAqIEEgc3BlY2lhbGl6ZWQgdmVyc2lvbiBvZiBgYmFzZVByb3BlcnR5YCB3aGljaCBzdXBwb3J0cyBkZWVwIHBhdGhzLlxuICpcbiAqIEBwcml2YXRlXG4gKiBAcGFyYW0ge0FycmF5fHN0cmluZ30gcGF0aCBUaGUgcGF0aCBvZiB0aGUgcHJvcGVydHkgdG8gZ2V0LlxuICogQHJldHVybnMge0Z1bmN0aW9ufSBSZXR1cm5zIHRoZSBuZXcgYWNjZXNzb3IgZnVuY3Rpb24uXG4gKi9cbmZ1bmN0aW9uIGJhc2VQcm9wZXJ0eURlZXAocGF0aCkge1xuICByZXR1cm4gZnVuY3Rpb24ob2JqZWN0KSB7XG4gICAgcmV0dXJuIGJhc2VHZXQob2JqZWN0LCBwYXRoKTtcbiAgfTtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBiYXNlUHJvcGVydHlEZWVwO1xuIiwiLyoqXG4gKiBUaGUgYmFzZSBpbXBsZW1lbnRhdGlvbiBvZiBgXy50aW1lc2Agd2l0aG91dCBzdXBwb3J0IGZvciBpdGVyYXRlZSBzaG9ydGhhbmRzXG4gKiBvciBtYXggYXJyYXkgbGVuZ3RoIGNoZWNrcy5cbiAqXG4gKiBAcHJpdmF0ZVxuICogQHBhcmFtIHtudW1iZXJ9IG4gVGhlIG51bWJlciBvZiB0aW1lcyB0byBpbnZva2UgYGl0ZXJhdGVlYC5cbiAqIEBwYXJhbSB7RnVuY3Rpb259IGl0ZXJhdGVlIFRoZSBmdW5jdGlvbiBpbnZva2VkIHBlciBpdGVyYXRpb24uXG4gKiBAcmV0dXJucyB7QXJyYXl9IFJldHVybnMgdGhlIGFycmF5IG9mIHJlc3VsdHMuXG4gKi9cbmZ1bmN0aW9uIGJhc2VUaW1lcyhuLCBpdGVyYXRlZSkge1xuICB2YXIgaW5kZXggPSAtMSxcbiAgICAgIHJlc3VsdCA9IEFycmF5KG4pO1xuXG4gIHdoaWxlICgrK2luZGV4IDwgbikge1xuICAgIHJlc3VsdFtpbmRleF0gPSBpdGVyYXRlZShpbmRleCk7XG4gIH1cbiAgcmV0dXJuIHJlc3VsdDtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBiYXNlVGltZXM7XG4iLCJ2YXIgU3ltYm9sID0gcmVxdWlyZSgnLi9fU3ltYm9sJyksXG4gICAgYXJyYXlNYXAgPSByZXF1aXJlKCcuL19hcnJheU1hcCcpLFxuICAgIGlzQXJyYXkgPSByZXF1aXJlKCcuL2lzQXJyYXknKSxcbiAgICBpc1N5bWJvbCA9IHJlcXVpcmUoJy4vaXNTeW1ib2wnKTtcblxuLyoqIFVzZWQgYXMgcmVmZXJlbmNlcyBmb3IgdmFyaW91cyBgTnVtYmVyYCBjb25zdGFudHMuICovXG52YXIgSU5GSU5JVFkgPSAxIC8gMDtcblxuLyoqIFVzZWQgdG8gY29udmVydCBzeW1ib2xzIHRvIHByaW1pdGl2ZXMgYW5kIHN0cmluZ3MuICovXG52YXIgc3ltYm9sUHJvdG8gPSBTeW1ib2wgPyBTeW1ib2wucHJvdG90eXBlIDogdW5kZWZpbmVkLFxuICAgIHN5bWJvbFRvU3RyaW5nID0gc3ltYm9sUHJvdG8gPyBzeW1ib2xQcm90by50b1N0cmluZyA6IHVuZGVmaW5lZDtcblxuLyoqXG4gKiBUaGUgYmFzZSBpbXBsZW1lbnRhdGlvbiBvZiBgXy50b1N0cmluZ2Agd2hpY2ggZG9lc24ndCBjb252ZXJ0IG51bGxpc2hcbiAqIHZhbHVlcyB0byBlbXB0eSBzdHJpbmdzLlxuICpcbiAqIEBwcml2YXRlXG4gKiBAcGFyYW0geyp9IHZhbHVlIFRoZSB2YWx1ZSB0byBwcm9jZXNzLlxuICogQHJldHVybnMge3N0cmluZ30gUmV0dXJucyB0aGUgc3RyaW5nLlxuICovXG5mdW5jdGlvbiBiYXNlVG9TdHJpbmcodmFsdWUpIHtcbiAgLy8gRXhpdCBlYXJseSBmb3Igc3RyaW5ncyB0byBhdm9pZCBhIHBlcmZvcm1hbmNlIGhpdCBpbiBzb21lIGVudmlyb25tZW50cy5cbiAgaWYgKHR5cGVvZiB2YWx1ZSA9PSAnc3RyaW5nJykge1xuICAgIHJldHVybiB2YWx1ZTtcbiAgfVxuICBpZiAoaXNBcnJheSh2YWx1ZSkpIHtcbiAgICAvLyBSZWN1cnNpdmVseSBjb252ZXJ0IHZhbHVlcyAoc3VzY2VwdGlibGUgdG8gY2FsbCBzdGFjayBsaW1pdHMpLlxuICAgIHJldHVybiBhcnJheU1hcCh2YWx1ZSwgYmFzZVRvU3RyaW5nKSArICcnO1xuICB9XG4gIGlmIChpc1N5bWJvbCh2YWx1ZSkpIHtcbiAgICByZXR1cm4gc3ltYm9sVG9TdHJpbmcgPyBzeW1ib2xUb1N0cmluZy5jYWxsKHZhbHVlKSA6ICcnO1xuICB9XG4gIHZhciByZXN1bHQgPSAodmFsdWUgKyAnJyk7XG4gIHJldHVybiAocmVzdWx0ID09ICcwJyAmJiAoMSAvIHZhbHVlKSA9PSAtSU5GSU5JVFkpID8gJy0wJyA6IHJlc3VsdDtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBiYXNlVG9TdHJpbmc7XG4iLCIvKipcbiAqIFRoZSBiYXNlIGltcGxlbWVudGF0aW9uIG9mIGBfLnVuYXJ5YCB3aXRob3V0IHN1cHBvcnQgZm9yIHN0b3JpbmcgbWV0YWRhdGEuXG4gKlxuICogQHByaXZhdGVcbiAqIEBwYXJhbSB7RnVuY3Rpb259IGZ1bmMgVGhlIGZ1bmN0aW9uIHRvIGNhcCBhcmd1bWVudHMgZm9yLlxuICogQHJldHVybnMge0Z1bmN0aW9ufSBSZXR1cm5zIHRoZSBuZXcgY2FwcGVkIGZ1bmN0aW9uLlxuICovXG5mdW5jdGlvbiBiYXNlVW5hcnkoZnVuYykge1xuICByZXR1cm4gZnVuY3Rpb24odmFsdWUpIHtcbiAgICByZXR1cm4gZnVuYyh2YWx1ZSk7XG4gIH07XG59XG5cbm1vZHVsZS5leHBvcnRzID0gYmFzZVVuYXJ5O1xuIiwiLyoqXG4gKiBDaGVja3MgaWYgYSBgY2FjaGVgIHZhbHVlIGZvciBga2V5YCBleGlzdHMuXG4gKlxuICogQHByaXZhdGVcbiAqIEBwYXJhbSB7T2JqZWN0fSBjYWNoZSBUaGUgY2FjaGUgdG8gcXVlcnkuXG4gKiBAcGFyYW0ge3N0cmluZ30ga2V5IFRoZSBrZXkgb2YgdGhlIGVudHJ5IHRvIGNoZWNrLlxuICogQHJldHVybnMge2Jvb2xlYW59IFJldHVybnMgYHRydWVgIGlmIGFuIGVudHJ5IGZvciBga2V5YCBleGlzdHMsIGVsc2UgYGZhbHNlYC5cbiAqL1xuZnVuY3Rpb24gY2FjaGVIYXMoY2FjaGUsIGtleSkge1xuICByZXR1cm4gY2FjaGUuaGFzKGtleSk7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gY2FjaGVIYXM7XG4iLCJ2YXIgaXNBcnJheSA9IHJlcXVpcmUoJy4vaXNBcnJheScpLFxuICAgIGlzS2V5ID0gcmVxdWlyZSgnLi9faXNLZXknKSxcbiAgICBzdHJpbmdUb1BhdGggPSByZXF1aXJlKCcuL19zdHJpbmdUb1BhdGgnKSxcbiAgICB0b1N0cmluZyA9IHJlcXVpcmUoJy4vdG9TdHJpbmcnKTtcblxuLyoqXG4gKiBDYXN0cyBgdmFsdWVgIHRvIGEgcGF0aCBhcnJheSBpZiBpdCdzIG5vdCBvbmUuXG4gKlxuICogQHByaXZhdGVcbiAqIEBwYXJhbSB7Kn0gdmFsdWUgVGhlIHZhbHVlIHRvIGluc3BlY3QuXG4gKiBAcGFyYW0ge09iamVjdH0gW29iamVjdF0gVGhlIG9iamVjdCB0byBxdWVyeSBrZXlzIG9uLlxuICogQHJldHVybnMge0FycmF5fSBSZXR1cm5zIHRoZSBjYXN0IHByb3BlcnR5IHBhdGggYXJyYXkuXG4gKi9cbmZ1bmN0aW9uIGNhc3RQYXRoKHZhbHVlLCBvYmplY3QpIHtcbiAgaWYgKGlzQXJyYXkodmFsdWUpKSB7XG4gICAgcmV0dXJuIHZhbHVlO1xuICB9XG4gIHJldHVybiBpc0tleSh2YWx1ZSwgb2JqZWN0KSA/IFt2YWx1ZV0gOiBzdHJpbmdUb1BhdGgodG9TdHJpbmcodmFsdWUpKTtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBjYXN0UGF0aDtcbiIsInZhciByb290ID0gcmVxdWlyZSgnLi9fcm9vdCcpO1xuXG4vKiogVXNlZCB0byBkZXRlY3Qgb3ZlcnJlYWNoaW5nIGNvcmUtanMgc2hpbXMuICovXG52YXIgY29yZUpzRGF0YSA9IHJvb3RbJ19fY29yZS1qc19zaGFyZWRfXyddO1xuXG5tb2R1bGUuZXhwb3J0cyA9IGNvcmVKc0RhdGE7XG4iLCJ2YXIgYmFzZUl0ZXJhdGVlID0gcmVxdWlyZSgnLi9fYmFzZUl0ZXJhdGVlJyksXG4gICAgaXNBcnJheUxpa2UgPSByZXF1aXJlKCcuL2lzQXJyYXlMaWtlJyksXG4gICAga2V5cyA9IHJlcXVpcmUoJy4va2V5cycpO1xuXG4vKipcbiAqIENyZWF0ZXMgYSBgXy5maW5kYCBvciBgXy5maW5kTGFzdGAgZnVuY3Rpb24uXG4gKlxuICogQHByaXZhdGVcbiAqIEBwYXJhbSB7RnVuY3Rpb259IGZpbmRJbmRleEZ1bmMgVGhlIGZ1bmN0aW9uIHRvIGZpbmQgdGhlIGNvbGxlY3Rpb24gaW5kZXguXG4gKiBAcmV0dXJucyB7RnVuY3Rpb259IFJldHVybnMgdGhlIG5ldyBmaW5kIGZ1bmN0aW9uLlxuICovXG5mdW5jdGlvbiBjcmVhdGVGaW5kKGZpbmRJbmRleEZ1bmMpIHtcbiAgcmV0dXJuIGZ1bmN0aW9uKGNvbGxlY3Rpb24sIHByZWRpY2F0ZSwgZnJvbUluZGV4KSB7XG4gICAgdmFyIGl0ZXJhYmxlID0gT2JqZWN0KGNvbGxlY3Rpb24pO1xuICAgIGlmICghaXNBcnJheUxpa2UoY29sbGVjdGlvbikpIHtcbiAgICAgIHZhciBpdGVyYXRlZSA9IGJhc2VJdGVyYXRlZShwcmVkaWNhdGUsIDMpO1xuICAgICAgY29sbGVjdGlvbiA9IGtleXMoY29sbGVjdGlvbik7XG4gICAgICBwcmVkaWNhdGUgPSBmdW5jdGlvbihrZXkpIHsgcmV0dXJuIGl0ZXJhdGVlKGl0ZXJhYmxlW2tleV0sIGtleSwgaXRlcmFibGUpOyB9O1xuICAgIH1cbiAgICB2YXIgaW5kZXggPSBmaW5kSW5kZXhGdW5jKGNvbGxlY3Rpb24sIHByZWRpY2F0ZSwgZnJvbUluZGV4KTtcbiAgICByZXR1cm4gaW5kZXggPiAtMSA/IGl0ZXJhYmxlW2l0ZXJhdGVlID8gY29sbGVjdGlvbltpbmRleF0gOiBpbmRleF0gOiB1bmRlZmluZWQ7XG4gIH07XG59XG5cbm1vZHVsZS5leHBvcnRzID0gY3JlYXRlRmluZDtcbiIsInZhciBTZXRDYWNoZSA9IHJlcXVpcmUoJy4vX1NldENhY2hlJyksXG4gICAgYXJyYXlTb21lID0gcmVxdWlyZSgnLi9fYXJyYXlTb21lJyksXG4gICAgY2FjaGVIYXMgPSByZXF1aXJlKCcuL19jYWNoZUhhcycpO1xuXG4vKiogVXNlZCB0byBjb21wb3NlIGJpdG1hc2tzIGZvciB2YWx1ZSBjb21wYXJpc29ucy4gKi9cbnZhciBDT01QQVJFX1BBUlRJQUxfRkxBRyA9IDEsXG4gICAgQ09NUEFSRV9VTk9SREVSRURfRkxBRyA9IDI7XG5cbi8qKlxuICogQSBzcGVjaWFsaXplZCB2ZXJzaW9uIG9mIGBiYXNlSXNFcXVhbERlZXBgIGZvciBhcnJheXMgd2l0aCBzdXBwb3J0IGZvclxuICogcGFydGlhbCBkZWVwIGNvbXBhcmlzb25zLlxuICpcbiAqIEBwcml2YXRlXG4gKiBAcGFyYW0ge0FycmF5fSBhcnJheSBUaGUgYXJyYXkgdG8gY29tcGFyZS5cbiAqIEBwYXJhbSB7QXJyYXl9IG90aGVyIFRoZSBvdGhlciBhcnJheSB0byBjb21wYXJlLlxuICogQHBhcmFtIHtudW1iZXJ9IGJpdG1hc2sgVGhlIGJpdG1hc2sgZmxhZ3MuIFNlZSBgYmFzZUlzRXF1YWxgIGZvciBtb3JlIGRldGFpbHMuXG4gKiBAcGFyYW0ge0Z1bmN0aW9ufSBjdXN0b21pemVyIFRoZSBmdW5jdGlvbiB0byBjdXN0b21pemUgY29tcGFyaXNvbnMuXG4gKiBAcGFyYW0ge0Z1bmN0aW9ufSBlcXVhbEZ1bmMgVGhlIGZ1bmN0aW9uIHRvIGRldGVybWluZSBlcXVpdmFsZW50cyBvZiB2YWx1ZXMuXG4gKiBAcGFyYW0ge09iamVjdH0gc3RhY2sgVHJhY2tzIHRyYXZlcnNlZCBgYXJyYXlgIGFuZCBgb3RoZXJgIG9iamVjdHMuXG4gKiBAcmV0dXJucyB7Ym9vbGVhbn0gUmV0dXJucyBgdHJ1ZWAgaWYgdGhlIGFycmF5cyBhcmUgZXF1aXZhbGVudCwgZWxzZSBgZmFsc2VgLlxuICovXG5mdW5jdGlvbiBlcXVhbEFycmF5cyhhcnJheSwgb3RoZXIsIGJpdG1hc2ssIGN1c3RvbWl6ZXIsIGVxdWFsRnVuYywgc3RhY2spIHtcbiAgdmFyIGlzUGFydGlhbCA9IGJpdG1hc2sgJiBDT01QQVJFX1BBUlRJQUxfRkxBRyxcbiAgICAgIGFyckxlbmd0aCA9IGFycmF5Lmxlbmd0aCxcbiAgICAgIG90aExlbmd0aCA9IG90aGVyLmxlbmd0aDtcblxuICBpZiAoYXJyTGVuZ3RoICE9IG90aExlbmd0aCAmJiAhKGlzUGFydGlhbCAmJiBvdGhMZW5ndGggPiBhcnJMZW5ndGgpKSB7XG4gICAgcmV0dXJuIGZhbHNlO1xuICB9XG4gIC8vIEFzc3VtZSBjeWNsaWMgdmFsdWVzIGFyZSBlcXVhbC5cbiAgdmFyIHN0YWNrZWQgPSBzdGFjay5nZXQoYXJyYXkpO1xuICBpZiAoc3RhY2tlZCAmJiBzdGFjay5nZXQob3RoZXIpKSB7XG4gICAgcmV0dXJuIHN0YWNrZWQgPT0gb3RoZXI7XG4gIH1cbiAgdmFyIGluZGV4ID0gLTEsXG4gICAgICByZXN1bHQgPSB0cnVlLFxuICAgICAgc2VlbiA9IChiaXRtYXNrICYgQ09NUEFSRV9VTk9SREVSRURfRkxBRykgPyBuZXcgU2V0Q2FjaGUgOiB1bmRlZmluZWQ7XG5cbiAgc3RhY2suc2V0KGFycmF5LCBvdGhlcik7XG4gIHN0YWNrLnNldChvdGhlciwgYXJyYXkpO1xuXG4gIC8vIElnbm9yZSBub24taW5kZXggcHJvcGVydGllcy5cbiAgd2hpbGUgKCsraW5kZXggPCBhcnJMZW5ndGgpIHtcbiAgICB2YXIgYXJyVmFsdWUgPSBhcnJheVtpbmRleF0sXG4gICAgICAgIG90aFZhbHVlID0gb3RoZXJbaW5kZXhdO1xuXG4gICAgaWYgKGN1c3RvbWl6ZXIpIHtcbiAgICAgIHZhciBjb21wYXJlZCA9IGlzUGFydGlhbFxuICAgICAgICA/IGN1c3RvbWl6ZXIob3RoVmFsdWUsIGFyclZhbHVlLCBpbmRleCwgb3RoZXIsIGFycmF5LCBzdGFjaylcbiAgICAgICAgOiBjdXN0b21pemVyKGFyclZhbHVlLCBvdGhWYWx1ZSwgaW5kZXgsIGFycmF5LCBvdGhlciwgc3RhY2spO1xuICAgIH1cbiAgICBpZiAoY29tcGFyZWQgIT09IHVuZGVmaW5lZCkge1xuICAgICAgaWYgKGNvbXBhcmVkKSB7XG4gICAgICAgIGNvbnRpbnVlO1xuICAgICAgfVxuICAgICAgcmVzdWx0ID0gZmFsc2U7XG4gICAgICBicmVhaztcbiAgICB9XG4gICAgLy8gUmVjdXJzaXZlbHkgY29tcGFyZSBhcnJheXMgKHN1c2NlcHRpYmxlIHRvIGNhbGwgc3RhY2sgbGltaXRzKS5cbiAgICBpZiAoc2Vlbikge1xuICAgICAgaWYgKCFhcnJheVNvbWUob3RoZXIsIGZ1bmN0aW9uKG90aFZhbHVlLCBvdGhJbmRleCkge1xuICAgICAgICAgICAgaWYgKCFjYWNoZUhhcyhzZWVuLCBvdGhJbmRleCkgJiZcbiAgICAgICAgICAgICAgICAoYXJyVmFsdWUgPT09IG90aFZhbHVlIHx8IGVxdWFsRnVuYyhhcnJWYWx1ZSwgb3RoVmFsdWUsIGJpdG1hc2ssIGN1c3RvbWl6ZXIsIHN0YWNrKSkpIHtcbiAgICAgICAgICAgICAgcmV0dXJuIHNlZW4ucHVzaChvdGhJbmRleCk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfSkpIHtcbiAgICAgICAgcmVzdWx0ID0gZmFsc2U7XG4gICAgICAgIGJyZWFrO1xuICAgICAgfVxuICAgIH0gZWxzZSBpZiAoIShcbiAgICAgICAgICBhcnJWYWx1ZSA9PT0gb3RoVmFsdWUgfHxcbiAgICAgICAgICAgIGVxdWFsRnVuYyhhcnJWYWx1ZSwgb3RoVmFsdWUsIGJpdG1hc2ssIGN1c3RvbWl6ZXIsIHN0YWNrKVxuICAgICAgICApKSB7XG4gICAgICByZXN1bHQgPSBmYWxzZTtcbiAgICAgIGJyZWFrO1xuICAgIH1cbiAgfVxuICBzdGFja1snZGVsZXRlJ10oYXJyYXkpO1xuICBzdGFja1snZGVsZXRlJ10ob3RoZXIpO1xuICByZXR1cm4gcmVzdWx0O1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IGVxdWFsQXJyYXlzO1xuIiwidmFyIFN5bWJvbCA9IHJlcXVpcmUoJy4vX1N5bWJvbCcpLFxuICAgIFVpbnQ4QXJyYXkgPSByZXF1aXJlKCcuL19VaW50OEFycmF5JyksXG4gICAgZXEgPSByZXF1aXJlKCcuL2VxJyksXG4gICAgZXF1YWxBcnJheXMgPSByZXF1aXJlKCcuL19lcXVhbEFycmF5cycpLFxuICAgIG1hcFRvQXJyYXkgPSByZXF1aXJlKCcuL19tYXBUb0FycmF5JyksXG4gICAgc2V0VG9BcnJheSA9IHJlcXVpcmUoJy4vX3NldFRvQXJyYXknKTtcblxuLyoqIFVzZWQgdG8gY29tcG9zZSBiaXRtYXNrcyBmb3IgdmFsdWUgY29tcGFyaXNvbnMuICovXG52YXIgQ09NUEFSRV9QQVJUSUFMX0ZMQUcgPSAxLFxuICAgIENPTVBBUkVfVU5PUkRFUkVEX0ZMQUcgPSAyO1xuXG4vKiogYE9iamVjdCN0b1N0cmluZ2AgcmVzdWx0IHJlZmVyZW5jZXMuICovXG52YXIgYm9vbFRhZyA9ICdbb2JqZWN0IEJvb2xlYW5dJyxcbiAgICBkYXRlVGFnID0gJ1tvYmplY3QgRGF0ZV0nLFxuICAgIGVycm9yVGFnID0gJ1tvYmplY3QgRXJyb3JdJyxcbiAgICBtYXBUYWcgPSAnW29iamVjdCBNYXBdJyxcbiAgICBudW1iZXJUYWcgPSAnW29iamVjdCBOdW1iZXJdJyxcbiAgICByZWdleHBUYWcgPSAnW29iamVjdCBSZWdFeHBdJyxcbiAgICBzZXRUYWcgPSAnW29iamVjdCBTZXRdJyxcbiAgICBzdHJpbmdUYWcgPSAnW29iamVjdCBTdHJpbmddJyxcbiAgICBzeW1ib2xUYWcgPSAnW29iamVjdCBTeW1ib2xdJztcblxudmFyIGFycmF5QnVmZmVyVGFnID0gJ1tvYmplY3QgQXJyYXlCdWZmZXJdJyxcbiAgICBkYXRhVmlld1RhZyA9ICdbb2JqZWN0IERhdGFWaWV3XSc7XG5cbi8qKiBVc2VkIHRvIGNvbnZlcnQgc3ltYm9scyB0byBwcmltaXRpdmVzIGFuZCBzdHJpbmdzLiAqL1xudmFyIHN5bWJvbFByb3RvID0gU3ltYm9sID8gU3ltYm9sLnByb3RvdHlwZSA6IHVuZGVmaW5lZCxcbiAgICBzeW1ib2xWYWx1ZU9mID0gc3ltYm9sUHJvdG8gPyBzeW1ib2xQcm90by52YWx1ZU9mIDogdW5kZWZpbmVkO1xuXG4vKipcbiAqIEEgc3BlY2lhbGl6ZWQgdmVyc2lvbiBvZiBgYmFzZUlzRXF1YWxEZWVwYCBmb3IgY29tcGFyaW5nIG9iamVjdHMgb2ZcbiAqIHRoZSBzYW1lIGB0b1N0cmluZ1RhZ2AuXG4gKlxuICogKipOb3RlOioqIFRoaXMgZnVuY3Rpb24gb25seSBzdXBwb3J0cyBjb21wYXJpbmcgdmFsdWVzIHdpdGggdGFncyBvZlxuICogYEJvb2xlYW5gLCBgRGF0ZWAsIGBFcnJvcmAsIGBOdW1iZXJgLCBgUmVnRXhwYCwgb3IgYFN0cmluZ2AuXG4gKlxuICogQHByaXZhdGVcbiAqIEBwYXJhbSB7T2JqZWN0fSBvYmplY3QgVGhlIG9iamVjdCB0byBjb21wYXJlLlxuICogQHBhcmFtIHtPYmplY3R9IG90aGVyIFRoZSBvdGhlciBvYmplY3QgdG8gY29tcGFyZS5cbiAqIEBwYXJhbSB7c3RyaW5nfSB0YWcgVGhlIGB0b1N0cmluZ1RhZ2Agb2YgdGhlIG9iamVjdHMgdG8gY29tcGFyZS5cbiAqIEBwYXJhbSB7bnVtYmVyfSBiaXRtYXNrIFRoZSBiaXRtYXNrIGZsYWdzLiBTZWUgYGJhc2VJc0VxdWFsYCBmb3IgbW9yZSBkZXRhaWxzLlxuICogQHBhcmFtIHtGdW5jdGlvbn0gY3VzdG9taXplciBUaGUgZnVuY3Rpb24gdG8gY3VzdG9taXplIGNvbXBhcmlzb25zLlxuICogQHBhcmFtIHtGdW5jdGlvbn0gZXF1YWxGdW5jIFRoZSBmdW5jdGlvbiB0byBkZXRlcm1pbmUgZXF1aXZhbGVudHMgb2YgdmFsdWVzLlxuICogQHBhcmFtIHtPYmplY3R9IHN0YWNrIFRyYWNrcyB0cmF2ZXJzZWQgYG9iamVjdGAgYW5kIGBvdGhlcmAgb2JqZWN0cy5cbiAqIEByZXR1cm5zIHtib29sZWFufSBSZXR1cm5zIGB0cnVlYCBpZiB0aGUgb2JqZWN0cyBhcmUgZXF1aXZhbGVudCwgZWxzZSBgZmFsc2VgLlxuICovXG5mdW5jdGlvbiBlcXVhbEJ5VGFnKG9iamVjdCwgb3RoZXIsIHRhZywgYml0bWFzaywgY3VzdG9taXplciwgZXF1YWxGdW5jLCBzdGFjaykge1xuICBzd2l0Y2ggKHRhZykge1xuICAgIGNhc2UgZGF0YVZpZXdUYWc6XG4gICAgICBpZiAoKG9iamVjdC5ieXRlTGVuZ3RoICE9IG90aGVyLmJ5dGVMZW5ndGgpIHx8XG4gICAgICAgICAgKG9iamVjdC5ieXRlT2Zmc2V0ICE9IG90aGVyLmJ5dGVPZmZzZXQpKSB7XG4gICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgIH1cbiAgICAgIG9iamVjdCA9IG9iamVjdC5idWZmZXI7XG4gICAgICBvdGhlciA9IG90aGVyLmJ1ZmZlcjtcblxuICAgIGNhc2UgYXJyYXlCdWZmZXJUYWc6XG4gICAgICBpZiAoKG9iamVjdC5ieXRlTGVuZ3RoICE9IG90aGVyLmJ5dGVMZW5ndGgpIHx8XG4gICAgICAgICAgIWVxdWFsRnVuYyhuZXcgVWludDhBcnJheShvYmplY3QpLCBuZXcgVWludDhBcnJheShvdGhlcikpKSB7XG4gICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgIH1cbiAgICAgIHJldHVybiB0cnVlO1xuXG4gICAgY2FzZSBib29sVGFnOlxuICAgIGNhc2UgZGF0ZVRhZzpcbiAgICBjYXNlIG51bWJlclRhZzpcbiAgICAgIC8vIENvZXJjZSBib29sZWFucyB0byBgMWAgb3IgYDBgIGFuZCBkYXRlcyB0byBtaWxsaXNlY29uZHMuXG4gICAgICAvLyBJbnZhbGlkIGRhdGVzIGFyZSBjb2VyY2VkIHRvIGBOYU5gLlxuICAgICAgcmV0dXJuIGVxKCtvYmplY3QsICtvdGhlcik7XG5cbiAgICBjYXNlIGVycm9yVGFnOlxuICAgICAgcmV0dXJuIG9iamVjdC5uYW1lID09IG90aGVyLm5hbWUgJiYgb2JqZWN0Lm1lc3NhZ2UgPT0gb3RoZXIubWVzc2FnZTtcblxuICAgIGNhc2UgcmVnZXhwVGFnOlxuICAgIGNhc2Ugc3RyaW5nVGFnOlxuICAgICAgLy8gQ29lcmNlIHJlZ2V4ZXMgdG8gc3RyaW5ncyBhbmQgdHJlYXQgc3RyaW5ncywgcHJpbWl0aXZlcyBhbmQgb2JqZWN0cyxcbiAgICAgIC8vIGFzIGVxdWFsLiBTZWUgaHR0cDovL3d3dy5lY21hLWludGVybmF0aW9uYWwub3JnL2VjbWEtMjYyLzcuMC8jc2VjLXJlZ2V4cC5wcm90b3R5cGUudG9zdHJpbmdcbiAgICAgIC8vIGZvciBtb3JlIGRldGFpbHMuXG4gICAgICByZXR1cm4gb2JqZWN0ID09IChvdGhlciArICcnKTtcblxuICAgIGNhc2UgbWFwVGFnOlxuICAgICAgdmFyIGNvbnZlcnQgPSBtYXBUb0FycmF5O1xuXG4gICAgY2FzZSBzZXRUYWc6XG4gICAgICB2YXIgaXNQYXJ0aWFsID0gYml0bWFzayAmIENPTVBBUkVfUEFSVElBTF9GTEFHO1xuICAgICAgY29udmVydCB8fCAoY29udmVydCA9IHNldFRvQXJyYXkpO1xuXG4gICAgICBpZiAob2JqZWN0LnNpemUgIT0gb3RoZXIuc2l6ZSAmJiAhaXNQYXJ0aWFsKSB7XG4gICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgIH1cbiAgICAgIC8vIEFzc3VtZSBjeWNsaWMgdmFsdWVzIGFyZSBlcXVhbC5cbiAgICAgIHZhciBzdGFja2VkID0gc3RhY2suZ2V0KG9iamVjdCk7XG4gICAgICBpZiAoc3RhY2tlZCkge1xuICAgICAgICByZXR1cm4gc3RhY2tlZCA9PSBvdGhlcjtcbiAgICAgIH1cbiAgICAgIGJpdG1hc2sgfD0gQ09NUEFSRV9VTk9SREVSRURfRkxBRztcblxuICAgICAgLy8gUmVjdXJzaXZlbHkgY29tcGFyZSBvYmplY3RzIChzdXNjZXB0aWJsZSB0byBjYWxsIHN0YWNrIGxpbWl0cykuXG4gICAgICBzdGFjay5zZXQob2JqZWN0LCBvdGhlcik7XG4gICAgICB2YXIgcmVzdWx0ID0gZXF1YWxBcnJheXMoY29udmVydChvYmplY3QpLCBjb252ZXJ0KG90aGVyKSwgYml0bWFzaywgY3VzdG9taXplciwgZXF1YWxGdW5jLCBzdGFjayk7XG4gICAgICBzdGFja1snZGVsZXRlJ10ob2JqZWN0KTtcbiAgICAgIHJldHVybiByZXN1bHQ7XG5cbiAgICBjYXNlIHN5bWJvbFRhZzpcbiAgICAgIGlmIChzeW1ib2xWYWx1ZU9mKSB7XG4gICAgICAgIHJldHVybiBzeW1ib2xWYWx1ZU9mLmNhbGwob2JqZWN0KSA9PSBzeW1ib2xWYWx1ZU9mLmNhbGwob3RoZXIpO1xuICAgICAgfVxuICB9XG4gIHJldHVybiBmYWxzZTtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBlcXVhbEJ5VGFnO1xuIiwidmFyIGdldEFsbEtleXMgPSByZXF1aXJlKCcuL19nZXRBbGxLZXlzJyk7XG5cbi8qKiBVc2VkIHRvIGNvbXBvc2UgYml0bWFza3MgZm9yIHZhbHVlIGNvbXBhcmlzb25zLiAqL1xudmFyIENPTVBBUkVfUEFSVElBTF9GTEFHID0gMTtcblxuLyoqIFVzZWQgZm9yIGJ1aWx0LWluIG1ldGhvZCByZWZlcmVuY2VzLiAqL1xudmFyIG9iamVjdFByb3RvID0gT2JqZWN0LnByb3RvdHlwZTtcblxuLyoqIFVzZWQgdG8gY2hlY2sgb2JqZWN0cyBmb3Igb3duIHByb3BlcnRpZXMuICovXG52YXIgaGFzT3duUHJvcGVydHkgPSBvYmplY3RQcm90by5oYXNPd25Qcm9wZXJ0eTtcblxuLyoqXG4gKiBBIHNwZWNpYWxpemVkIHZlcnNpb24gb2YgYGJhc2VJc0VxdWFsRGVlcGAgZm9yIG9iamVjdHMgd2l0aCBzdXBwb3J0IGZvclxuICogcGFydGlhbCBkZWVwIGNvbXBhcmlzb25zLlxuICpcbiAqIEBwcml2YXRlXG4gKiBAcGFyYW0ge09iamVjdH0gb2JqZWN0IFRoZSBvYmplY3QgdG8gY29tcGFyZS5cbiAqIEBwYXJhbSB7T2JqZWN0fSBvdGhlciBUaGUgb3RoZXIgb2JqZWN0IHRvIGNvbXBhcmUuXG4gKiBAcGFyYW0ge251bWJlcn0gYml0bWFzayBUaGUgYml0bWFzayBmbGFncy4gU2VlIGBiYXNlSXNFcXVhbGAgZm9yIG1vcmUgZGV0YWlscy5cbiAqIEBwYXJhbSB7RnVuY3Rpb259IGN1c3RvbWl6ZXIgVGhlIGZ1bmN0aW9uIHRvIGN1c3RvbWl6ZSBjb21wYXJpc29ucy5cbiAqIEBwYXJhbSB7RnVuY3Rpb259IGVxdWFsRnVuYyBUaGUgZnVuY3Rpb24gdG8gZGV0ZXJtaW5lIGVxdWl2YWxlbnRzIG9mIHZhbHVlcy5cbiAqIEBwYXJhbSB7T2JqZWN0fSBzdGFjayBUcmFja3MgdHJhdmVyc2VkIGBvYmplY3RgIGFuZCBgb3RoZXJgIG9iamVjdHMuXG4gKiBAcmV0dXJucyB7Ym9vbGVhbn0gUmV0dXJucyBgdHJ1ZWAgaWYgdGhlIG9iamVjdHMgYXJlIGVxdWl2YWxlbnQsIGVsc2UgYGZhbHNlYC5cbiAqL1xuZnVuY3Rpb24gZXF1YWxPYmplY3RzKG9iamVjdCwgb3RoZXIsIGJpdG1hc2ssIGN1c3RvbWl6ZXIsIGVxdWFsRnVuYywgc3RhY2spIHtcbiAgdmFyIGlzUGFydGlhbCA9IGJpdG1hc2sgJiBDT01QQVJFX1BBUlRJQUxfRkxBRyxcbiAgICAgIG9ialByb3BzID0gZ2V0QWxsS2V5cyhvYmplY3QpLFxuICAgICAgb2JqTGVuZ3RoID0gb2JqUHJvcHMubGVuZ3RoLFxuICAgICAgb3RoUHJvcHMgPSBnZXRBbGxLZXlzKG90aGVyKSxcbiAgICAgIG90aExlbmd0aCA9IG90aFByb3BzLmxlbmd0aDtcblxuICBpZiAob2JqTGVuZ3RoICE9IG90aExlbmd0aCAmJiAhaXNQYXJ0aWFsKSB7XG4gICAgcmV0dXJuIGZhbHNlO1xuICB9XG4gIHZhciBpbmRleCA9IG9iakxlbmd0aDtcbiAgd2hpbGUgKGluZGV4LS0pIHtcbiAgICB2YXIga2V5ID0gb2JqUHJvcHNbaW5kZXhdO1xuICAgIGlmICghKGlzUGFydGlhbCA/IGtleSBpbiBvdGhlciA6IGhhc093blByb3BlcnR5LmNhbGwob3RoZXIsIGtleSkpKSB7XG4gICAgICByZXR1cm4gZmFsc2U7XG4gICAgfVxuICB9XG4gIC8vIEFzc3VtZSBjeWNsaWMgdmFsdWVzIGFyZSBlcXVhbC5cbiAgdmFyIHN0YWNrZWQgPSBzdGFjay5nZXQob2JqZWN0KTtcbiAgaWYgKHN0YWNrZWQgJiYgc3RhY2suZ2V0KG90aGVyKSkge1xuICAgIHJldHVybiBzdGFja2VkID09IG90aGVyO1xuICB9XG4gIHZhciByZXN1bHQgPSB0cnVlO1xuICBzdGFjay5zZXQob2JqZWN0LCBvdGhlcik7XG4gIHN0YWNrLnNldChvdGhlciwgb2JqZWN0KTtcblxuICB2YXIgc2tpcEN0b3IgPSBpc1BhcnRpYWw7XG4gIHdoaWxlICgrK2luZGV4IDwgb2JqTGVuZ3RoKSB7XG4gICAga2V5ID0gb2JqUHJvcHNbaW5kZXhdO1xuICAgIHZhciBvYmpWYWx1ZSA9IG9iamVjdFtrZXldLFxuICAgICAgICBvdGhWYWx1ZSA9IG90aGVyW2tleV07XG5cbiAgICBpZiAoY3VzdG9taXplcikge1xuICAgICAgdmFyIGNvbXBhcmVkID0gaXNQYXJ0aWFsXG4gICAgICAgID8gY3VzdG9taXplcihvdGhWYWx1ZSwgb2JqVmFsdWUsIGtleSwgb3RoZXIsIG9iamVjdCwgc3RhY2spXG4gICAgICAgIDogY3VzdG9taXplcihvYmpWYWx1ZSwgb3RoVmFsdWUsIGtleSwgb2JqZWN0LCBvdGhlciwgc3RhY2spO1xuICAgIH1cbiAgICAvLyBSZWN1cnNpdmVseSBjb21wYXJlIG9iamVjdHMgKHN1c2NlcHRpYmxlIHRvIGNhbGwgc3RhY2sgbGltaXRzKS5cbiAgICBpZiAoIShjb21wYXJlZCA9PT0gdW5kZWZpbmVkXG4gICAgICAgICAgPyAob2JqVmFsdWUgPT09IG90aFZhbHVlIHx8IGVxdWFsRnVuYyhvYmpWYWx1ZSwgb3RoVmFsdWUsIGJpdG1hc2ssIGN1c3RvbWl6ZXIsIHN0YWNrKSlcbiAgICAgICAgICA6IGNvbXBhcmVkXG4gICAgICAgICkpIHtcbiAgICAgIHJlc3VsdCA9IGZhbHNlO1xuICAgICAgYnJlYWs7XG4gICAgfVxuICAgIHNraXBDdG9yIHx8IChza2lwQ3RvciA9IGtleSA9PSAnY29uc3RydWN0b3InKTtcbiAgfVxuICBpZiAocmVzdWx0ICYmICFza2lwQ3Rvcikge1xuICAgIHZhciBvYmpDdG9yID0gb2JqZWN0LmNvbnN0cnVjdG9yLFxuICAgICAgICBvdGhDdG9yID0gb3RoZXIuY29uc3RydWN0b3I7XG5cbiAgICAvLyBOb24gYE9iamVjdGAgb2JqZWN0IGluc3RhbmNlcyB3aXRoIGRpZmZlcmVudCBjb25zdHJ1Y3RvcnMgYXJlIG5vdCBlcXVhbC5cbiAgICBpZiAob2JqQ3RvciAhPSBvdGhDdG9yICYmXG4gICAgICAgICgnY29uc3RydWN0b3InIGluIG9iamVjdCAmJiAnY29uc3RydWN0b3InIGluIG90aGVyKSAmJlxuICAgICAgICAhKHR5cGVvZiBvYmpDdG9yID09ICdmdW5jdGlvbicgJiYgb2JqQ3RvciBpbnN0YW5jZW9mIG9iakN0b3IgJiZcbiAgICAgICAgICB0eXBlb2Ygb3RoQ3RvciA9PSAnZnVuY3Rpb24nICYmIG90aEN0b3IgaW5zdGFuY2VvZiBvdGhDdG9yKSkge1xuICAgICAgcmVzdWx0ID0gZmFsc2U7XG4gICAgfVxuICB9XG4gIHN0YWNrWydkZWxldGUnXShvYmplY3QpO1xuICBzdGFja1snZGVsZXRlJ10ob3RoZXIpO1xuICByZXR1cm4gcmVzdWx0O1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IGVxdWFsT2JqZWN0cztcbiIsIi8qKiBEZXRlY3QgZnJlZSB2YXJpYWJsZSBgZ2xvYmFsYCBmcm9tIE5vZGUuanMuICovXG52YXIgZnJlZUdsb2JhbCA9IHR5cGVvZiBnbG9iYWwgPT0gJ29iamVjdCcgJiYgZ2xvYmFsICYmIGdsb2JhbC5PYmplY3QgPT09IE9iamVjdCAmJiBnbG9iYWw7XG5cbm1vZHVsZS5leHBvcnRzID0gZnJlZUdsb2JhbDtcbiIsInZhciBiYXNlR2V0QWxsS2V5cyA9IHJlcXVpcmUoJy4vX2Jhc2VHZXRBbGxLZXlzJyksXG4gICAgZ2V0U3ltYm9scyA9IHJlcXVpcmUoJy4vX2dldFN5bWJvbHMnKSxcbiAgICBrZXlzID0gcmVxdWlyZSgnLi9rZXlzJyk7XG5cbi8qKlxuICogQ3JlYXRlcyBhbiBhcnJheSBvZiBvd24gZW51bWVyYWJsZSBwcm9wZXJ0eSBuYW1lcyBhbmQgc3ltYm9scyBvZiBgb2JqZWN0YC5cbiAqXG4gKiBAcHJpdmF0ZVxuICogQHBhcmFtIHtPYmplY3R9IG9iamVjdCBUaGUgb2JqZWN0IHRvIHF1ZXJ5LlxuICogQHJldHVybnMge0FycmF5fSBSZXR1cm5zIHRoZSBhcnJheSBvZiBwcm9wZXJ0eSBuYW1lcyBhbmQgc3ltYm9scy5cbiAqL1xuZnVuY3Rpb24gZ2V0QWxsS2V5cyhvYmplY3QpIHtcbiAgcmV0dXJuIGJhc2VHZXRBbGxLZXlzKG9iamVjdCwga2V5cywgZ2V0U3ltYm9scyk7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gZ2V0QWxsS2V5cztcbiIsInZhciBpc0tleWFibGUgPSByZXF1aXJlKCcuL19pc0tleWFibGUnKTtcblxuLyoqXG4gKiBHZXRzIHRoZSBkYXRhIGZvciBgbWFwYC5cbiAqXG4gKiBAcHJpdmF0ZVxuICogQHBhcmFtIHtPYmplY3R9IG1hcCBUaGUgbWFwIHRvIHF1ZXJ5LlxuICogQHBhcmFtIHtzdHJpbmd9IGtleSBUaGUgcmVmZXJlbmNlIGtleS5cbiAqIEByZXR1cm5zIHsqfSBSZXR1cm5zIHRoZSBtYXAgZGF0YS5cbiAqL1xuZnVuY3Rpb24gZ2V0TWFwRGF0YShtYXAsIGtleSkge1xuICB2YXIgZGF0YSA9IG1hcC5fX2RhdGFfXztcbiAgcmV0dXJuIGlzS2V5YWJsZShrZXkpXG4gICAgPyBkYXRhW3R5cGVvZiBrZXkgPT0gJ3N0cmluZycgPyAnc3RyaW5nJyA6ICdoYXNoJ11cbiAgICA6IGRhdGEubWFwO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IGdldE1hcERhdGE7XG4iLCJ2YXIgaXNTdHJpY3RDb21wYXJhYmxlID0gcmVxdWlyZSgnLi9faXNTdHJpY3RDb21wYXJhYmxlJyksXG4gICAga2V5cyA9IHJlcXVpcmUoJy4va2V5cycpO1xuXG4vKipcbiAqIEdldHMgdGhlIHByb3BlcnR5IG5hbWVzLCB2YWx1ZXMsIGFuZCBjb21wYXJlIGZsYWdzIG9mIGBvYmplY3RgLlxuICpcbiAqIEBwcml2YXRlXG4gKiBAcGFyYW0ge09iamVjdH0gb2JqZWN0IFRoZSBvYmplY3QgdG8gcXVlcnkuXG4gKiBAcmV0dXJucyB7QXJyYXl9IFJldHVybnMgdGhlIG1hdGNoIGRhdGEgb2YgYG9iamVjdGAuXG4gKi9cbmZ1bmN0aW9uIGdldE1hdGNoRGF0YShvYmplY3QpIHtcbiAgdmFyIHJlc3VsdCA9IGtleXMob2JqZWN0KSxcbiAgICAgIGxlbmd0aCA9IHJlc3VsdC5sZW5ndGg7XG5cbiAgd2hpbGUgKGxlbmd0aC0tKSB7XG4gICAgdmFyIGtleSA9IHJlc3VsdFtsZW5ndGhdLFxuICAgICAgICB2YWx1ZSA9IG9iamVjdFtrZXldO1xuXG4gICAgcmVzdWx0W2xlbmd0aF0gPSBba2V5LCB2YWx1ZSwgaXNTdHJpY3RDb21wYXJhYmxlKHZhbHVlKV07XG4gIH1cbiAgcmV0dXJuIHJlc3VsdDtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBnZXRNYXRjaERhdGE7XG4iLCJ2YXIgYmFzZUlzTmF0aXZlID0gcmVxdWlyZSgnLi9fYmFzZUlzTmF0aXZlJyksXG4gICAgZ2V0VmFsdWUgPSByZXF1aXJlKCcuL19nZXRWYWx1ZScpO1xuXG4vKipcbiAqIEdldHMgdGhlIG5hdGl2ZSBmdW5jdGlvbiBhdCBga2V5YCBvZiBgb2JqZWN0YC5cbiAqXG4gKiBAcHJpdmF0ZVxuICogQHBhcmFtIHtPYmplY3R9IG9iamVjdCBUaGUgb2JqZWN0IHRvIHF1ZXJ5LlxuICogQHBhcmFtIHtzdHJpbmd9IGtleSBUaGUga2V5IG9mIHRoZSBtZXRob2QgdG8gZ2V0LlxuICogQHJldHVybnMgeyp9IFJldHVybnMgdGhlIGZ1bmN0aW9uIGlmIGl0J3MgbmF0aXZlLCBlbHNlIGB1bmRlZmluZWRgLlxuICovXG5mdW5jdGlvbiBnZXROYXRpdmUob2JqZWN0LCBrZXkpIHtcbiAgdmFyIHZhbHVlID0gZ2V0VmFsdWUob2JqZWN0LCBrZXkpO1xuICByZXR1cm4gYmFzZUlzTmF0aXZlKHZhbHVlKSA/IHZhbHVlIDogdW5kZWZpbmVkO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IGdldE5hdGl2ZTtcbiIsInZhciBTeW1ib2wgPSByZXF1aXJlKCcuL19TeW1ib2wnKTtcblxuLyoqIFVzZWQgZm9yIGJ1aWx0LWluIG1ldGhvZCByZWZlcmVuY2VzLiAqL1xudmFyIG9iamVjdFByb3RvID0gT2JqZWN0LnByb3RvdHlwZTtcblxuLyoqIFVzZWQgdG8gY2hlY2sgb2JqZWN0cyBmb3Igb3duIHByb3BlcnRpZXMuICovXG52YXIgaGFzT3duUHJvcGVydHkgPSBvYmplY3RQcm90by5oYXNPd25Qcm9wZXJ0eTtcblxuLyoqXG4gKiBVc2VkIHRvIHJlc29sdmUgdGhlXG4gKiBbYHRvU3RyaW5nVGFnYF0oaHR0cDovL2VjbWEtaW50ZXJuYXRpb25hbC5vcmcvZWNtYS0yNjIvNy4wLyNzZWMtb2JqZWN0LnByb3RvdHlwZS50b3N0cmluZylcbiAqIG9mIHZhbHVlcy5cbiAqL1xudmFyIG5hdGl2ZU9iamVjdFRvU3RyaW5nID0gb2JqZWN0UHJvdG8udG9TdHJpbmc7XG5cbi8qKiBCdWlsdC1pbiB2YWx1ZSByZWZlcmVuY2VzLiAqL1xudmFyIHN5bVRvU3RyaW5nVGFnID0gU3ltYm9sID8gU3ltYm9sLnRvU3RyaW5nVGFnIDogdW5kZWZpbmVkO1xuXG4vKipcbiAqIEEgc3BlY2lhbGl6ZWQgdmVyc2lvbiBvZiBgYmFzZUdldFRhZ2Agd2hpY2ggaWdub3JlcyBgU3ltYm9sLnRvU3RyaW5nVGFnYCB2YWx1ZXMuXG4gKlxuICogQHByaXZhdGVcbiAqIEBwYXJhbSB7Kn0gdmFsdWUgVGhlIHZhbHVlIHRvIHF1ZXJ5LlxuICogQHJldHVybnMge3N0cmluZ30gUmV0dXJucyB0aGUgcmF3IGB0b1N0cmluZ1RhZ2AuXG4gKi9cbmZ1bmN0aW9uIGdldFJhd1RhZyh2YWx1ZSkge1xuICB2YXIgaXNPd24gPSBoYXNPd25Qcm9wZXJ0eS5jYWxsKHZhbHVlLCBzeW1Ub1N0cmluZ1RhZyksXG4gICAgICB0YWcgPSB2YWx1ZVtzeW1Ub1N0cmluZ1RhZ107XG5cbiAgdHJ5IHtcbiAgICB2YWx1ZVtzeW1Ub1N0cmluZ1RhZ10gPSB1bmRlZmluZWQ7XG4gICAgdmFyIHVubWFza2VkID0gdHJ1ZTtcbiAgfSBjYXRjaCAoZSkge31cblxuICB2YXIgcmVzdWx0ID0gbmF0aXZlT2JqZWN0VG9TdHJpbmcuY2FsbCh2YWx1ZSk7XG4gIGlmICh1bm1hc2tlZCkge1xuICAgIGlmIChpc093bikge1xuICAgICAgdmFsdWVbc3ltVG9TdHJpbmdUYWddID0gdGFnO1xuICAgIH0gZWxzZSB7XG4gICAgICBkZWxldGUgdmFsdWVbc3ltVG9TdHJpbmdUYWddO1xuICAgIH1cbiAgfVxuICByZXR1cm4gcmVzdWx0O1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IGdldFJhd1RhZztcbiIsInZhciBhcnJheUZpbHRlciA9IHJlcXVpcmUoJy4vX2FycmF5RmlsdGVyJyksXG4gICAgc3R1YkFycmF5ID0gcmVxdWlyZSgnLi9zdHViQXJyYXknKTtcblxuLyoqIFVzZWQgZm9yIGJ1aWx0LWluIG1ldGhvZCByZWZlcmVuY2VzLiAqL1xudmFyIG9iamVjdFByb3RvID0gT2JqZWN0LnByb3RvdHlwZTtcblxuLyoqIEJ1aWx0LWluIHZhbHVlIHJlZmVyZW5jZXMuICovXG52YXIgcHJvcGVydHlJc0VudW1lcmFibGUgPSBvYmplY3RQcm90by5wcm9wZXJ0eUlzRW51bWVyYWJsZTtcblxuLyogQnVpbHQtaW4gbWV0aG9kIHJlZmVyZW5jZXMgZm9yIHRob3NlIHdpdGggdGhlIHNhbWUgbmFtZSBhcyBvdGhlciBgbG9kYXNoYCBtZXRob2RzLiAqL1xudmFyIG5hdGl2ZUdldFN5bWJvbHMgPSBPYmplY3QuZ2V0T3duUHJvcGVydHlTeW1ib2xzO1xuXG4vKipcbiAqIENyZWF0ZXMgYW4gYXJyYXkgb2YgdGhlIG93biBlbnVtZXJhYmxlIHN5bWJvbHMgb2YgYG9iamVjdGAuXG4gKlxuICogQHByaXZhdGVcbiAqIEBwYXJhbSB7T2JqZWN0fSBvYmplY3QgVGhlIG9iamVjdCB0byBxdWVyeS5cbiAqIEByZXR1cm5zIHtBcnJheX0gUmV0dXJucyB0aGUgYXJyYXkgb2Ygc3ltYm9scy5cbiAqL1xudmFyIGdldFN5bWJvbHMgPSAhbmF0aXZlR2V0U3ltYm9scyA/IHN0dWJBcnJheSA6IGZ1bmN0aW9uKG9iamVjdCkge1xuICBpZiAob2JqZWN0ID09IG51bGwpIHtcbiAgICByZXR1cm4gW107XG4gIH1cbiAgb2JqZWN0ID0gT2JqZWN0KG9iamVjdCk7XG4gIHJldHVybiBhcnJheUZpbHRlcihuYXRpdmVHZXRTeW1ib2xzKG9iamVjdCksIGZ1bmN0aW9uKHN5bWJvbCkge1xuICAgIHJldHVybiBwcm9wZXJ0eUlzRW51bWVyYWJsZS5jYWxsKG9iamVjdCwgc3ltYm9sKTtcbiAgfSk7XG59O1xuXG5tb2R1bGUuZXhwb3J0cyA9IGdldFN5bWJvbHM7XG4iLCJ2YXIgRGF0YVZpZXcgPSByZXF1aXJlKCcuL19EYXRhVmlldycpLFxuICAgIE1hcCA9IHJlcXVpcmUoJy4vX01hcCcpLFxuICAgIFByb21pc2UgPSByZXF1aXJlKCcuL19Qcm9taXNlJyksXG4gICAgU2V0ID0gcmVxdWlyZSgnLi9fU2V0JyksXG4gICAgV2Vha01hcCA9IHJlcXVpcmUoJy4vX1dlYWtNYXAnKSxcbiAgICBiYXNlR2V0VGFnID0gcmVxdWlyZSgnLi9fYmFzZUdldFRhZycpLFxuICAgIHRvU291cmNlID0gcmVxdWlyZSgnLi9fdG9Tb3VyY2UnKTtcblxuLyoqIGBPYmplY3QjdG9TdHJpbmdgIHJlc3VsdCByZWZlcmVuY2VzLiAqL1xudmFyIG1hcFRhZyA9ICdbb2JqZWN0IE1hcF0nLFxuICAgIG9iamVjdFRhZyA9ICdbb2JqZWN0IE9iamVjdF0nLFxuICAgIHByb21pc2VUYWcgPSAnW29iamVjdCBQcm9taXNlXScsXG4gICAgc2V0VGFnID0gJ1tvYmplY3QgU2V0XScsXG4gICAgd2Vha01hcFRhZyA9ICdbb2JqZWN0IFdlYWtNYXBdJztcblxudmFyIGRhdGFWaWV3VGFnID0gJ1tvYmplY3QgRGF0YVZpZXddJztcblxuLyoqIFVzZWQgdG8gZGV0ZWN0IG1hcHMsIHNldHMsIGFuZCB3ZWFrbWFwcy4gKi9cbnZhciBkYXRhVmlld0N0b3JTdHJpbmcgPSB0b1NvdXJjZShEYXRhVmlldyksXG4gICAgbWFwQ3RvclN0cmluZyA9IHRvU291cmNlKE1hcCksXG4gICAgcHJvbWlzZUN0b3JTdHJpbmcgPSB0b1NvdXJjZShQcm9taXNlKSxcbiAgICBzZXRDdG9yU3RyaW5nID0gdG9Tb3VyY2UoU2V0KSxcbiAgICB3ZWFrTWFwQ3RvclN0cmluZyA9IHRvU291cmNlKFdlYWtNYXApO1xuXG4vKipcbiAqIEdldHMgdGhlIGB0b1N0cmluZ1RhZ2Agb2YgYHZhbHVlYC5cbiAqXG4gKiBAcHJpdmF0ZVxuICogQHBhcmFtIHsqfSB2YWx1ZSBUaGUgdmFsdWUgdG8gcXVlcnkuXG4gKiBAcmV0dXJucyB7c3RyaW5nfSBSZXR1cm5zIHRoZSBgdG9TdHJpbmdUYWdgLlxuICovXG52YXIgZ2V0VGFnID0gYmFzZUdldFRhZztcblxuLy8gRmFsbGJhY2sgZm9yIGRhdGEgdmlld3MsIG1hcHMsIHNldHMsIGFuZCB3ZWFrIG1hcHMgaW4gSUUgMTEgYW5kIHByb21pc2VzIGluIE5vZGUuanMgPCA2LlxuaWYgKChEYXRhVmlldyAmJiBnZXRUYWcobmV3IERhdGFWaWV3KG5ldyBBcnJheUJ1ZmZlcigxKSkpICE9IGRhdGFWaWV3VGFnKSB8fFxuICAgIChNYXAgJiYgZ2V0VGFnKG5ldyBNYXApICE9IG1hcFRhZykgfHxcbiAgICAoUHJvbWlzZSAmJiBnZXRUYWcoUHJvbWlzZS5yZXNvbHZlKCkpICE9IHByb21pc2VUYWcpIHx8XG4gICAgKFNldCAmJiBnZXRUYWcobmV3IFNldCkgIT0gc2V0VGFnKSB8fFxuICAgIChXZWFrTWFwICYmIGdldFRhZyhuZXcgV2Vha01hcCkgIT0gd2Vha01hcFRhZykpIHtcbiAgZ2V0VGFnID0gZnVuY3Rpb24odmFsdWUpIHtcbiAgICB2YXIgcmVzdWx0ID0gYmFzZUdldFRhZyh2YWx1ZSksXG4gICAgICAgIEN0b3IgPSByZXN1bHQgPT0gb2JqZWN0VGFnID8gdmFsdWUuY29uc3RydWN0b3IgOiB1bmRlZmluZWQsXG4gICAgICAgIGN0b3JTdHJpbmcgPSBDdG9yID8gdG9Tb3VyY2UoQ3RvcikgOiAnJztcblxuICAgIGlmIChjdG9yU3RyaW5nKSB7XG4gICAgICBzd2l0Y2ggKGN0b3JTdHJpbmcpIHtcbiAgICAgICAgY2FzZSBkYXRhVmlld0N0b3JTdHJpbmc6IHJldHVybiBkYXRhVmlld1RhZztcbiAgICAgICAgY2FzZSBtYXBDdG9yU3RyaW5nOiByZXR1cm4gbWFwVGFnO1xuICAgICAgICBjYXNlIHByb21pc2VDdG9yU3RyaW5nOiByZXR1cm4gcHJvbWlzZVRhZztcbiAgICAgICAgY2FzZSBzZXRDdG9yU3RyaW5nOiByZXR1cm4gc2V0VGFnO1xuICAgICAgICBjYXNlIHdlYWtNYXBDdG9yU3RyaW5nOiByZXR1cm4gd2Vha01hcFRhZztcbiAgICAgIH1cbiAgICB9XG4gICAgcmV0dXJuIHJlc3VsdDtcbiAgfTtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBnZXRUYWc7XG4iLCIvKipcbiAqIEdldHMgdGhlIHZhbHVlIGF0IGBrZXlgIG9mIGBvYmplY3RgLlxuICpcbiAqIEBwcml2YXRlXG4gKiBAcGFyYW0ge09iamVjdH0gW29iamVjdF0gVGhlIG9iamVjdCB0byBxdWVyeS5cbiAqIEBwYXJhbSB7c3RyaW5nfSBrZXkgVGhlIGtleSBvZiB0aGUgcHJvcGVydHkgdG8gZ2V0LlxuICogQHJldHVybnMgeyp9IFJldHVybnMgdGhlIHByb3BlcnR5IHZhbHVlLlxuICovXG5mdW5jdGlvbiBnZXRWYWx1ZShvYmplY3QsIGtleSkge1xuICByZXR1cm4gb2JqZWN0ID09IG51bGwgPyB1bmRlZmluZWQgOiBvYmplY3Rba2V5XTtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBnZXRWYWx1ZTtcbiIsInZhciBjYXN0UGF0aCA9IHJlcXVpcmUoJy4vX2Nhc3RQYXRoJyksXG4gICAgaXNBcmd1bWVudHMgPSByZXF1aXJlKCcuL2lzQXJndW1lbnRzJyksXG4gICAgaXNBcnJheSA9IHJlcXVpcmUoJy4vaXNBcnJheScpLFxuICAgIGlzSW5kZXggPSByZXF1aXJlKCcuL19pc0luZGV4JyksXG4gICAgaXNMZW5ndGggPSByZXF1aXJlKCcuL2lzTGVuZ3RoJyksXG4gICAgdG9LZXkgPSByZXF1aXJlKCcuL190b0tleScpO1xuXG4vKipcbiAqIENoZWNrcyBpZiBgcGF0aGAgZXhpc3RzIG9uIGBvYmplY3RgLlxuICpcbiAqIEBwcml2YXRlXG4gKiBAcGFyYW0ge09iamVjdH0gb2JqZWN0IFRoZSBvYmplY3QgdG8gcXVlcnkuXG4gKiBAcGFyYW0ge0FycmF5fHN0cmluZ30gcGF0aCBUaGUgcGF0aCB0byBjaGVjay5cbiAqIEBwYXJhbSB7RnVuY3Rpb259IGhhc0Z1bmMgVGhlIGZ1bmN0aW9uIHRvIGNoZWNrIHByb3BlcnRpZXMuXG4gKiBAcmV0dXJucyB7Ym9vbGVhbn0gUmV0dXJucyBgdHJ1ZWAgaWYgYHBhdGhgIGV4aXN0cywgZWxzZSBgZmFsc2VgLlxuICovXG5mdW5jdGlvbiBoYXNQYXRoKG9iamVjdCwgcGF0aCwgaGFzRnVuYykge1xuICBwYXRoID0gY2FzdFBhdGgocGF0aCwgb2JqZWN0KTtcblxuICB2YXIgaW5kZXggPSAtMSxcbiAgICAgIGxlbmd0aCA9IHBhdGgubGVuZ3RoLFxuICAgICAgcmVzdWx0ID0gZmFsc2U7XG5cbiAgd2hpbGUgKCsraW5kZXggPCBsZW5ndGgpIHtcbiAgICB2YXIga2V5ID0gdG9LZXkocGF0aFtpbmRleF0pO1xuICAgIGlmICghKHJlc3VsdCA9IG9iamVjdCAhPSBudWxsICYmIGhhc0Z1bmMob2JqZWN0LCBrZXkpKSkge1xuICAgICAgYnJlYWs7XG4gICAgfVxuICAgIG9iamVjdCA9IG9iamVjdFtrZXldO1xuICB9XG4gIGlmIChyZXN1bHQgfHwgKytpbmRleCAhPSBsZW5ndGgpIHtcbiAgICByZXR1cm4gcmVzdWx0O1xuICB9XG4gIGxlbmd0aCA9IG9iamVjdCA9PSBudWxsID8gMCA6IG9iamVjdC5sZW5ndGg7XG4gIHJldHVybiAhIWxlbmd0aCAmJiBpc0xlbmd0aChsZW5ndGgpICYmIGlzSW5kZXgoa2V5LCBsZW5ndGgpICYmXG4gICAgKGlzQXJyYXkob2JqZWN0KSB8fCBpc0FyZ3VtZW50cyhvYmplY3QpKTtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBoYXNQYXRoO1xuIiwidmFyIG5hdGl2ZUNyZWF0ZSA9IHJlcXVpcmUoJy4vX25hdGl2ZUNyZWF0ZScpO1xuXG4vKipcbiAqIFJlbW92ZXMgYWxsIGtleS12YWx1ZSBlbnRyaWVzIGZyb20gdGhlIGhhc2guXG4gKlxuICogQHByaXZhdGVcbiAqIEBuYW1lIGNsZWFyXG4gKiBAbWVtYmVyT2YgSGFzaFxuICovXG5mdW5jdGlvbiBoYXNoQ2xlYXIoKSB7XG4gIHRoaXMuX19kYXRhX18gPSBuYXRpdmVDcmVhdGUgPyBuYXRpdmVDcmVhdGUobnVsbCkgOiB7fTtcbiAgdGhpcy5zaXplID0gMDtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBoYXNoQ2xlYXI7XG4iLCIvKipcbiAqIFJlbW92ZXMgYGtleWAgYW5kIGl0cyB2YWx1ZSBmcm9tIHRoZSBoYXNoLlxuICpcbiAqIEBwcml2YXRlXG4gKiBAbmFtZSBkZWxldGVcbiAqIEBtZW1iZXJPZiBIYXNoXG4gKiBAcGFyYW0ge09iamVjdH0gaGFzaCBUaGUgaGFzaCB0byBtb2RpZnkuXG4gKiBAcGFyYW0ge3N0cmluZ30ga2V5IFRoZSBrZXkgb2YgdGhlIHZhbHVlIHRvIHJlbW92ZS5cbiAqIEByZXR1cm5zIHtib29sZWFufSBSZXR1cm5zIGB0cnVlYCBpZiB0aGUgZW50cnkgd2FzIHJlbW92ZWQsIGVsc2UgYGZhbHNlYC5cbiAqL1xuZnVuY3Rpb24gaGFzaERlbGV0ZShrZXkpIHtcbiAgdmFyIHJlc3VsdCA9IHRoaXMuaGFzKGtleSkgJiYgZGVsZXRlIHRoaXMuX19kYXRhX19ba2V5XTtcbiAgdGhpcy5zaXplIC09IHJlc3VsdCA/IDEgOiAwO1xuICByZXR1cm4gcmVzdWx0O1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IGhhc2hEZWxldGU7XG4iLCJ2YXIgbmF0aXZlQ3JlYXRlID0gcmVxdWlyZSgnLi9fbmF0aXZlQ3JlYXRlJyk7XG5cbi8qKiBVc2VkIHRvIHN0YW5kLWluIGZvciBgdW5kZWZpbmVkYCBoYXNoIHZhbHVlcy4gKi9cbnZhciBIQVNIX1VOREVGSU5FRCA9ICdfX2xvZGFzaF9oYXNoX3VuZGVmaW5lZF9fJztcblxuLyoqIFVzZWQgZm9yIGJ1aWx0LWluIG1ldGhvZCByZWZlcmVuY2VzLiAqL1xudmFyIG9iamVjdFByb3RvID0gT2JqZWN0LnByb3RvdHlwZTtcblxuLyoqIFVzZWQgdG8gY2hlY2sgb2JqZWN0cyBmb3Igb3duIHByb3BlcnRpZXMuICovXG52YXIgaGFzT3duUHJvcGVydHkgPSBvYmplY3RQcm90by5oYXNPd25Qcm9wZXJ0eTtcblxuLyoqXG4gKiBHZXRzIHRoZSBoYXNoIHZhbHVlIGZvciBga2V5YC5cbiAqXG4gKiBAcHJpdmF0ZVxuICogQG5hbWUgZ2V0XG4gKiBAbWVtYmVyT2YgSGFzaFxuICogQHBhcmFtIHtzdHJpbmd9IGtleSBUaGUga2V5IG9mIHRoZSB2YWx1ZSB0byBnZXQuXG4gKiBAcmV0dXJucyB7Kn0gUmV0dXJucyB0aGUgZW50cnkgdmFsdWUuXG4gKi9cbmZ1bmN0aW9uIGhhc2hHZXQoa2V5KSB7XG4gIHZhciBkYXRhID0gdGhpcy5fX2RhdGFfXztcbiAgaWYgKG5hdGl2ZUNyZWF0ZSkge1xuICAgIHZhciByZXN1bHQgPSBkYXRhW2tleV07XG4gICAgcmV0dXJuIHJlc3VsdCA9PT0gSEFTSF9VTkRFRklORUQgPyB1bmRlZmluZWQgOiByZXN1bHQ7XG4gIH1cbiAgcmV0dXJuIGhhc093blByb3BlcnR5LmNhbGwoZGF0YSwga2V5KSA/IGRhdGFba2V5XSA6IHVuZGVmaW5lZDtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBoYXNoR2V0O1xuIiwidmFyIG5hdGl2ZUNyZWF0ZSA9IHJlcXVpcmUoJy4vX25hdGl2ZUNyZWF0ZScpO1xuXG4vKiogVXNlZCBmb3IgYnVpbHQtaW4gbWV0aG9kIHJlZmVyZW5jZXMuICovXG52YXIgb2JqZWN0UHJvdG8gPSBPYmplY3QucHJvdG90eXBlO1xuXG4vKiogVXNlZCB0byBjaGVjayBvYmplY3RzIGZvciBvd24gcHJvcGVydGllcy4gKi9cbnZhciBoYXNPd25Qcm9wZXJ0eSA9IG9iamVjdFByb3RvLmhhc093blByb3BlcnR5O1xuXG4vKipcbiAqIENoZWNrcyBpZiBhIGhhc2ggdmFsdWUgZm9yIGBrZXlgIGV4aXN0cy5cbiAqXG4gKiBAcHJpdmF0ZVxuICogQG5hbWUgaGFzXG4gKiBAbWVtYmVyT2YgSGFzaFxuICogQHBhcmFtIHtzdHJpbmd9IGtleSBUaGUga2V5IG9mIHRoZSBlbnRyeSB0byBjaGVjay5cbiAqIEByZXR1cm5zIHtib29sZWFufSBSZXR1cm5zIGB0cnVlYCBpZiBhbiBlbnRyeSBmb3IgYGtleWAgZXhpc3RzLCBlbHNlIGBmYWxzZWAuXG4gKi9cbmZ1bmN0aW9uIGhhc2hIYXMoa2V5KSB7XG4gIHZhciBkYXRhID0gdGhpcy5fX2RhdGFfXztcbiAgcmV0dXJuIG5hdGl2ZUNyZWF0ZSA/IChkYXRhW2tleV0gIT09IHVuZGVmaW5lZCkgOiBoYXNPd25Qcm9wZXJ0eS5jYWxsKGRhdGEsIGtleSk7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gaGFzaEhhcztcbiIsInZhciBuYXRpdmVDcmVhdGUgPSByZXF1aXJlKCcuL19uYXRpdmVDcmVhdGUnKTtcblxuLyoqIFVzZWQgdG8gc3RhbmQtaW4gZm9yIGB1bmRlZmluZWRgIGhhc2ggdmFsdWVzLiAqL1xudmFyIEhBU0hfVU5ERUZJTkVEID0gJ19fbG9kYXNoX2hhc2hfdW5kZWZpbmVkX18nO1xuXG4vKipcbiAqIFNldHMgdGhlIGhhc2ggYGtleWAgdG8gYHZhbHVlYC5cbiAqXG4gKiBAcHJpdmF0ZVxuICogQG5hbWUgc2V0XG4gKiBAbWVtYmVyT2YgSGFzaFxuICogQHBhcmFtIHtzdHJpbmd9IGtleSBUaGUga2V5IG9mIHRoZSB2YWx1ZSB0byBzZXQuXG4gKiBAcGFyYW0geyp9IHZhbHVlIFRoZSB2YWx1ZSB0byBzZXQuXG4gKiBAcmV0dXJucyB7T2JqZWN0fSBSZXR1cm5zIHRoZSBoYXNoIGluc3RhbmNlLlxuICovXG5mdW5jdGlvbiBoYXNoU2V0KGtleSwgdmFsdWUpIHtcbiAgdmFyIGRhdGEgPSB0aGlzLl9fZGF0YV9fO1xuICB0aGlzLnNpemUgKz0gdGhpcy5oYXMoa2V5KSA/IDAgOiAxO1xuICBkYXRhW2tleV0gPSAobmF0aXZlQ3JlYXRlICYmIHZhbHVlID09PSB1bmRlZmluZWQpID8gSEFTSF9VTkRFRklORUQgOiB2YWx1ZTtcbiAgcmV0dXJuIHRoaXM7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gaGFzaFNldDtcbiIsIi8qKiBVc2VkIGFzIHJlZmVyZW5jZXMgZm9yIHZhcmlvdXMgYE51bWJlcmAgY29uc3RhbnRzLiAqL1xudmFyIE1BWF9TQUZFX0lOVEVHRVIgPSA5MDA3MTk5MjU0NzQwOTkxO1xuXG4vKiogVXNlZCB0byBkZXRlY3QgdW5zaWduZWQgaW50ZWdlciB2YWx1ZXMuICovXG52YXIgcmVJc1VpbnQgPSAvXig/OjB8WzEtOV1cXGQqKSQvO1xuXG4vKipcbiAqIENoZWNrcyBpZiBgdmFsdWVgIGlzIGEgdmFsaWQgYXJyYXktbGlrZSBpbmRleC5cbiAqXG4gKiBAcHJpdmF0ZVxuICogQHBhcmFtIHsqfSB2YWx1ZSBUaGUgdmFsdWUgdG8gY2hlY2suXG4gKiBAcGFyYW0ge251bWJlcn0gW2xlbmd0aD1NQVhfU0FGRV9JTlRFR0VSXSBUaGUgdXBwZXIgYm91bmRzIG9mIGEgdmFsaWQgaW5kZXguXG4gKiBAcmV0dXJucyB7Ym9vbGVhbn0gUmV0dXJucyBgdHJ1ZWAgaWYgYHZhbHVlYCBpcyBhIHZhbGlkIGluZGV4LCBlbHNlIGBmYWxzZWAuXG4gKi9cbmZ1bmN0aW9uIGlzSW5kZXgodmFsdWUsIGxlbmd0aCkge1xuICB2YXIgdHlwZSA9IHR5cGVvZiB2YWx1ZTtcbiAgbGVuZ3RoID0gbGVuZ3RoID09IG51bGwgPyBNQVhfU0FGRV9JTlRFR0VSIDogbGVuZ3RoO1xuXG4gIHJldHVybiAhIWxlbmd0aCAmJlxuICAgICh0eXBlID09ICdudW1iZXInIHx8XG4gICAgICAodHlwZSAhPSAnc3ltYm9sJyAmJiByZUlzVWludC50ZXN0KHZhbHVlKSkpICYmXG4gICAgICAgICh2YWx1ZSA+IC0xICYmIHZhbHVlICUgMSA9PSAwICYmIHZhbHVlIDwgbGVuZ3RoKTtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBpc0luZGV4O1xuIiwidmFyIGlzQXJyYXkgPSByZXF1aXJlKCcuL2lzQXJyYXknKSxcbiAgICBpc1N5bWJvbCA9IHJlcXVpcmUoJy4vaXNTeW1ib2wnKTtcblxuLyoqIFVzZWQgdG8gbWF0Y2ggcHJvcGVydHkgbmFtZXMgd2l0aGluIHByb3BlcnR5IHBhdGhzLiAqL1xudmFyIHJlSXNEZWVwUHJvcCA9IC9cXC58XFxbKD86W15bXFxdXSp8KFtcIiddKSg/Oig/IVxcMSlbXlxcXFxdfFxcXFwuKSo/XFwxKVxcXS8sXG4gICAgcmVJc1BsYWluUHJvcCA9IC9eXFx3KiQvO1xuXG4vKipcbiAqIENoZWNrcyBpZiBgdmFsdWVgIGlzIGEgcHJvcGVydHkgbmFtZSBhbmQgbm90IGEgcHJvcGVydHkgcGF0aC5cbiAqXG4gKiBAcHJpdmF0ZVxuICogQHBhcmFtIHsqfSB2YWx1ZSBUaGUgdmFsdWUgdG8gY2hlY2suXG4gKiBAcGFyYW0ge09iamVjdH0gW29iamVjdF0gVGhlIG9iamVjdCB0byBxdWVyeSBrZXlzIG9uLlxuICogQHJldHVybnMge2Jvb2xlYW59IFJldHVybnMgYHRydWVgIGlmIGB2YWx1ZWAgaXMgYSBwcm9wZXJ0eSBuYW1lLCBlbHNlIGBmYWxzZWAuXG4gKi9cbmZ1bmN0aW9uIGlzS2V5KHZhbHVlLCBvYmplY3QpIHtcbiAgaWYgKGlzQXJyYXkodmFsdWUpKSB7XG4gICAgcmV0dXJuIGZhbHNlO1xuICB9XG4gIHZhciB0eXBlID0gdHlwZW9mIHZhbHVlO1xuICBpZiAodHlwZSA9PSAnbnVtYmVyJyB8fCB0eXBlID09ICdzeW1ib2wnIHx8IHR5cGUgPT0gJ2Jvb2xlYW4nIHx8XG4gICAgICB2YWx1ZSA9PSBudWxsIHx8IGlzU3ltYm9sKHZhbHVlKSkge1xuICAgIHJldHVybiB0cnVlO1xuICB9XG4gIHJldHVybiByZUlzUGxhaW5Qcm9wLnRlc3QodmFsdWUpIHx8ICFyZUlzRGVlcFByb3AudGVzdCh2YWx1ZSkgfHxcbiAgICAob2JqZWN0ICE9IG51bGwgJiYgdmFsdWUgaW4gT2JqZWN0KG9iamVjdCkpO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IGlzS2V5O1xuIiwiLyoqXG4gKiBDaGVja3MgaWYgYHZhbHVlYCBpcyBzdWl0YWJsZSBmb3IgdXNlIGFzIHVuaXF1ZSBvYmplY3Qga2V5LlxuICpcbiAqIEBwcml2YXRlXG4gKiBAcGFyYW0geyp9IHZhbHVlIFRoZSB2YWx1ZSB0byBjaGVjay5cbiAqIEByZXR1cm5zIHtib29sZWFufSBSZXR1cm5zIGB0cnVlYCBpZiBgdmFsdWVgIGlzIHN1aXRhYmxlLCBlbHNlIGBmYWxzZWAuXG4gKi9cbmZ1bmN0aW9uIGlzS2V5YWJsZSh2YWx1ZSkge1xuICB2YXIgdHlwZSA9IHR5cGVvZiB2YWx1ZTtcbiAgcmV0dXJuICh0eXBlID09ICdzdHJpbmcnIHx8IHR5cGUgPT0gJ251bWJlcicgfHwgdHlwZSA9PSAnc3ltYm9sJyB8fCB0eXBlID09ICdib29sZWFuJylcbiAgICA/ICh2YWx1ZSAhPT0gJ19fcHJvdG9fXycpXG4gICAgOiAodmFsdWUgPT09IG51bGwpO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IGlzS2V5YWJsZTtcbiIsInZhciBjb3JlSnNEYXRhID0gcmVxdWlyZSgnLi9fY29yZUpzRGF0YScpO1xuXG4vKiogVXNlZCB0byBkZXRlY3QgbWV0aG9kcyBtYXNxdWVyYWRpbmcgYXMgbmF0aXZlLiAqL1xudmFyIG1hc2tTcmNLZXkgPSAoZnVuY3Rpb24oKSB7XG4gIHZhciB1aWQgPSAvW14uXSskLy5leGVjKGNvcmVKc0RhdGEgJiYgY29yZUpzRGF0YS5rZXlzICYmIGNvcmVKc0RhdGEua2V5cy5JRV9QUk9UTyB8fCAnJyk7XG4gIHJldHVybiB1aWQgPyAoJ1N5bWJvbChzcmMpXzEuJyArIHVpZCkgOiAnJztcbn0oKSk7XG5cbi8qKlxuICogQ2hlY2tzIGlmIGBmdW5jYCBoYXMgaXRzIHNvdXJjZSBtYXNrZWQuXG4gKlxuICogQHByaXZhdGVcbiAqIEBwYXJhbSB7RnVuY3Rpb259IGZ1bmMgVGhlIGZ1bmN0aW9uIHRvIGNoZWNrLlxuICogQHJldHVybnMge2Jvb2xlYW59IFJldHVybnMgYHRydWVgIGlmIGBmdW5jYCBpcyBtYXNrZWQsIGVsc2UgYGZhbHNlYC5cbiAqL1xuZnVuY3Rpb24gaXNNYXNrZWQoZnVuYykge1xuICByZXR1cm4gISFtYXNrU3JjS2V5ICYmIChtYXNrU3JjS2V5IGluIGZ1bmMpO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IGlzTWFza2VkO1xuIiwiLyoqIFVzZWQgZm9yIGJ1aWx0LWluIG1ldGhvZCByZWZlcmVuY2VzLiAqL1xudmFyIG9iamVjdFByb3RvID0gT2JqZWN0LnByb3RvdHlwZTtcblxuLyoqXG4gKiBDaGVja3MgaWYgYHZhbHVlYCBpcyBsaWtlbHkgYSBwcm90b3R5cGUgb2JqZWN0LlxuICpcbiAqIEBwcml2YXRlXG4gKiBAcGFyYW0geyp9IHZhbHVlIFRoZSB2YWx1ZSB0byBjaGVjay5cbiAqIEByZXR1cm5zIHtib29sZWFufSBSZXR1cm5zIGB0cnVlYCBpZiBgdmFsdWVgIGlzIGEgcHJvdG90eXBlLCBlbHNlIGBmYWxzZWAuXG4gKi9cbmZ1bmN0aW9uIGlzUHJvdG90eXBlKHZhbHVlKSB7XG4gIHZhciBDdG9yID0gdmFsdWUgJiYgdmFsdWUuY29uc3RydWN0b3IsXG4gICAgICBwcm90byA9ICh0eXBlb2YgQ3RvciA9PSAnZnVuY3Rpb24nICYmIEN0b3IucHJvdG90eXBlKSB8fCBvYmplY3RQcm90bztcblxuICByZXR1cm4gdmFsdWUgPT09IHByb3RvO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IGlzUHJvdG90eXBlO1xuIiwidmFyIGlzT2JqZWN0ID0gcmVxdWlyZSgnLi9pc09iamVjdCcpO1xuXG4vKipcbiAqIENoZWNrcyBpZiBgdmFsdWVgIGlzIHN1aXRhYmxlIGZvciBzdHJpY3QgZXF1YWxpdHkgY29tcGFyaXNvbnMsIGkuZS4gYD09PWAuXG4gKlxuICogQHByaXZhdGVcbiAqIEBwYXJhbSB7Kn0gdmFsdWUgVGhlIHZhbHVlIHRvIGNoZWNrLlxuICogQHJldHVybnMge2Jvb2xlYW59IFJldHVybnMgYHRydWVgIGlmIGB2YWx1ZWAgaWYgc3VpdGFibGUgZm9yIHN0cmljdFxuICogIGVxdWFsaXR5IGNvbXBhcmlzb25zLCBlbHNlIGBmYWxzZWAuXG4gKi9cbmZ1bmN0aW9uIGlzU3RyaWN0Q29tcGFyYWJsZSh2YWx1ZSkge1xuICByZXR1cm4gdmFsdWUgPT09IHZhbHVlICYmICFpc09iamVjdCh2YWx1ZSk7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gaXNTdHJpY3RDb21wYXJhYmxlO1xuIiwiLyoqXG4gKiBSZW1vdmVzIGFsbCBrZXktdmFsdWUgZW50cmllcyBmcm9tIHRoZSBsaXN0IGNhY2hlLlxuICpcbiAqIEBwcml2YXRlXG4gKiBAbmFtZSBjbGVhclxuICogQG1lbWJlck9mIExpc3RDYWNoZVxuICovXG5mdW5jdGlvbiBsaXN0Q2FjaGVDbGVhcigpIHtcbiAgdGhpcy5fX2RhdGFfXyA9IFtdO1xuICB0aGlzLnNpemUgPSAwO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IGxpc3RDYWNoZUNsZWFyO1xuIiwidmFyIGFzc29jSW5kZXhPZiA9IHJlcXVpcmUoJy4vX2Fzc29jSW5kZXhPZicpO1xuXG4vKiogVXNlZCBmb3IgYnVpbHQtaW4gbWV0aG9kIHJlZmVyZW5jZXMuICovXG52YXIgYXJyYXlQcm90byA9IEFycmF5LnByb3RvdHlwZTtcblxuLyoqIEJ1aWx0LWluIHZhbHVlIHJlZmVyZW5jZXMuICovXG52YXIgc3BsaWNlID0gYXJyYXlQcm90by5zcGxpY2U7XG5cbi8qKlxuICogUmVtb3ZlcyBga2V5YCBhbmQgaXRzIHZhbHVlIGZyb20gdGhlIGxpc3QgY2FjaGUuXG4gKlxuICogQHByaXZhdGVcbiAqIEBuYW1lIGRlbGV0ZVxuICogQG1lbWJlck9mIExpc3RDYWNoZVxuICogQHBhcmFtIHtzdHJpbmd9IGtleSBUaGUga2V5IG9mIHRoZSB2YWx1ZSB0byByZW1vdmUuXG4gKiBAcmV0dXJucyB7Ym9vbGVhbn0gUmV0dXJucyBgdHJ1ZWAgaWYgdGhlIGVudHJ5IHdhcyByZW1vdmVkLCBlbHNlIGBmYWxzZWAuXG4gKi9cbmZ1bmN0aW9uIGxpc3RDYWNoZURlbGV0ZShrZXkpIHtcbiAgdmFyIGRhdGEgPSB0aGlzLl9fZGF0YV9fLFxuICAgICAgaW5kZXggPSBhc3NvY0luZGV4T2YoZGF0YSwga2V5KTtcblxuICBpZiAoaW5kZXggPCAwKSB7XG4gICAgcmV0dXJuIGZhbHNlO1xuICB9XG4gIHZhciBsYXN0SW5kZXggPSBkYXRhLmxlbmd0aCAtIDE7XG4gIGlmIChpbmRleCA9PSBsYXN0SW5kZXgpIHtcbiAgICBkYXRhLnBvcCgpO1xuICB9IGVsc2Uge1xuICAgIHNwbGljZS5jYWxsKGRhdGEsIGluZGV4LCAxKTtcbiAgfVxuICAtLXRoaXMuc2l6ZTtcbiAgcmV0dXJuIHRydWU7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gbGlzdENhY2hlRGVsZXRlO1xuIiwidmFyIGFzc29jSW5kZXhPZiA9IHJlcXVpcmUoJy4vX2Fzc29jSW5kZXhPZicpO1xuXG4vKipcbiAqIEdldHMgdGhlIGxpc3QgY2FjaGUgdmFsdWUgZm9yIGBrZXlgLlxuICpcbiAqIEBwcml2YXRlXG4gKiBAbmFtZSBnZXRcbiAqIEBtZW1iZXJPZiBMaXN0Q2FjaGVcbiAqIEBwYXJhbSB7c3RyaW5nfSBrZXkgVGhlIGtleSBvZiB0aGUgdmFsdWUgdG8gZ2V0LlxuICogQHJldHVybnMgeyp9IFJldHVybnMgdGhlIGVudHJ5IHZhbHVlLlxuICovXG5mdW5jdGlvbiBsaXN0Q2FjaGVHZXQoa2V5KSB7XG4gIHZhciBkYXRhID0gdGhpcy5fX2RhdGFfXyxcbiAgICAgIGluZGV4ID0gYXNzb2NJbmRleE9mKGRhdGEsIGtleSk7XG5cbiAgcmV0dXJuIGluZGV4IDwgMCA/IHVuZGVmaW5lZCA6IGRhdGFbaW5kZXhdWzFdO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IGxpc3RDYWNoZUdldDtcbiIsInZhciBhc3NvY0luZGV4T2YgPSByZXF1aXJlKCcuL19hc3NvY0luZGV4T2YnKTtcblxuLyoqXG4gKiBDaGVja3MgaWYgYSBsaXN0IGNhY2hlIHZhbHVlIGZvciBga2V5YCBleGlzdHMuXG4gKlxuICogQHByaXZhdGVcbiAqIEBuYW1lIGhhc1xuICogQG1lbWJlck9mIExpc3RDYWNoZVxuICogQHBhcmFtIHtzdHJpbmd9IGtleSBUaGUga2V5IG9mIHRoZSBlbnRyeSB0byBjaGVjay5cbiAqIEByZXR1cm5zIHtib29sZWFufSBSZXR1cm5zIGB0cnVlYCBpZiBhbiBlbnRyeSBmb3IgYGtleWAgZXhpc3RzLCBlbHNlIGBmYWxzZWAuXG4gKi9cbmZ1bmN0aW9uIGxpc3RDYWNoZUhhcyhrZXkpIHtcbiAgcmV0dXJuIGFzc29jSW5kZXhPZih0aGlzLl9fZGF0YV9fLCBrZXkpID4gLTE7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gbGlzdENhY2hlSGFzO1xuIiwidmFyIGFzc29jSW5kZXhPZiA9IHJlcXVpcmUoJy4vX2Fzc29jSW5kZXhPZicpO1xuXG4vKipcbiAqIFNldHMgdGhlIGxpc3QgY2FjaGUgYGtleWAgdG8gYHZhbHVlYC5cbiAqXG4gKiBAcHJpdmF0ZVxuICogQG5hbWUgc2V0XG4gKiBAbWVtYmVyT2YgTGlzdENhY2hlXG4gKiBAcGFyYW0ge3N0cmluZ30ga2V5IFRoZSBrZXkgb2YgdGhlIHZhbHVlIHRvIHNldC5cbiAqIEBwYXJhbSB7Kn0gdmFsdWUgVGhlIHZhbHVlIHRvIHNldC5cbiAqIEByZXR1cm5zIHtPYmplY3R9IFJldHVybnMgdGhlIGxpc3QgY2FjaGUgaW5zdGFuY2UuXG4gKi9cbmZ1bmN0aW9uIGxpc3RDYWNoZVNldChrZXksIHZhbHVlKSB7XG4gIHZhciBkYXRhID0gdGhpcy5fX2RhdGFfXyxcbiAgICAgIGluZGV4ID0gYXNzb2NJbmRleE9mKGRhdGEsIGtleSk7XG5cbiAgaWYgKGluZGV4IDwgMCkge1xuICAgICsrdGhpcy5zaXplO1xuICAgIGRhdGEucHVzaChba2V5LCB2YWx1ZV0pO1xuICB9IGVsc2Uge1xuICAgIGRhdGFbaW5kZXhdWzFdID0gdmFsdWU7XG4gIH1cbiAgcmV0dXJuIHRoaXM7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gbGlzdENhY2hlU2V0O1xuIiwidmFyIEhhc2ggPSByZXF1aXJlKCcuL19IYXNoJyksXG4gICAgTGlzdENhY2hlID0gcmVxdWlyZSgnLi9fTGlzdENhY2hlJyksXG4gICAgTWFwID0gcmVxdWlyZSgnLi9fTWFwJyk7XG5cbi8qKlxuICogUmVtb3ZlcyBhbGwga2V5LXZhbHVlIGVudHJpZXMgZnJvbSB0aGUgbWFwLlxuICpcbiAqIEBwcml2YXRlXG4gKiBAbmFtZSBjbGVhclxuICogQG1lbWJlck9mIE1hcENhY2hlXG4gKi9cbmZ1bmN0aW9uIG1hcENhY2hlQ2xlYXIoKSB7XG4gIHRoaXMuc2l6ZSA9IDA7XG4gIHRoaXMuX19kYXRhX18gPSB7XG4gICAgJ2hhc2gnOiBuZXcgSGFzaCxcbiAgICAnbWFwJzogbmV3IChNYXAgfHwgTGlzdENhY2hlKSxcbiAgICAnc3RyaW5nJzogbmV3IEhhc2hcbiAgfTtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBtYXBDYWNoZUNsZWFyO1xuIiwidmFyIGdldE1hcERhdGEgPSByZXF1aXJlKCcuL19nZXRNYXBEYXRhJyk7XG5cbi8qKlxuICogUmVtb3ZlcyBga2V5YCBhbmQgaXRzIHZhbHVlIGZyb20gdGhlIG1hcC5cbiAqXG4gKiBAcHJpdmF0ZVxuICogQG5hbWUgZGVsZXRlXG4gKiBAbWVtYmVyT2YgTWFwQ2FjaGVcbiAqIEBwYXJhbSB7c3RyaW5nfSBrZXkgVGhlIGtleSBvZiB0aGUgdmFsdWUgdG8gcmVtb3ZlLlxuICogQHJldHVybnMge2Jvb2xlYW59IFJldHVybnMgYHRydWVgIGlmIHRoZSBlbnRyeSB3YXMgcmVtb3ZlZCwgZWxzZSBgZmFsc2VgLlxuICovXG5mdW5jdGlvbiBtYXBDYWNoZURlbGV0ZShrZXkpIHtcbiAgdmFyIHJlc3VsdCA9IGdldE1hcERhdGEodGhpcywga2V5KVsnZGVsZXRlJ10oa2V5KTtcbiAgdGhpcy5zaXplIC09IHJlc3VsdCA/IDEgOiAwO1xuICByZXR1cm4gcmVzdWx0O1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IG1hcENhY2hlRGVsZXRlO1xuIiwidmFyIGdldE1hcERhdGEgPSByZXF1aXJlKCcuL19nZXRNYXBEYXRhJyk7XG5cbi8qKlxuICogR2V0cyB0aGUgbWFwIHZhbHVlIGZvciBga2V5YC5cbiAqXG4gKiBAcHJpdmF0ZVxuICogQG5hbWUgZ2V0XG4gKiBAbWVtYmVyT2YgTWFwQ2FjaGVcbiAqIEBwYXJhbSB7c3RyaW5nfSBrZXkgVGhlIGtleSBvZiB0aGUgdmFsdWUgdG8gZ2V0LlxuICogQHJldHVybnMgeyp9IFJldHVybnMgdGhlIGVudHJ5IHZhbHVlLlxuICovXG5mdW5jdGlvbiBtYXBDYWNoZUdldChrZXkpIHtcbiAgcmV0dXJuIGdldE1hcERhdGEodGhpcywga2V5KS5nZXQoa2V5KTtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBtYXBDYWNoZUdldDtcbiIsInZhciBnZXRNYXBEYXRhID0gcmVxdWlyZSgnLi9fZ2V0TWFwRGF0YScpO1xuXG4vKipcbiAqIENoZWNrcyBpZiBhIG1hcCB2YWx1ZSBmb3IgYGtleWAgZXhpc3RzLlxuICpcbiAqIEBwcml2YXRlXG4gKiBAbmFtZSBoYXNcbiAqIEBtZW1iZXJPZiBNYXBDYWNoZVxuICogQHBhcmFtIHtzdHJpbmd9IGtleSBUaGUga2V5IG9mIHRoZSBlbnRyeSB0byBjaGVjay5cbiAqIEByZXR1cm5zIHtib29sZWFufSBSZXR1cm5zIGB0cnVlYCBpZiBhbiBlbnRyeSBmb3IgYGtleWAgZXhpc3RzLCBlbHNlIGBmYWxzZWAuXG4gKi9cbmZ1bmN0aW9uIG1hcENhY2hlSGFzKGtleSkge1xuICByZXR1cm4gZ2V0TWFwRGF0YSh0aGlzLCBrZXkpLmhhcyhrZXkpO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IG1hcENhY2hlSGFzO1xuIiwidmFyIGdldE1hcERhdGEgPSByZXF1aXJlKCcuL19nZXRNYXBEYXRhJyk7XG5cbi8qKlxuICogU2V0cyB0aGUgbWFwIGBrZXlgIHRvIGB2YWx1ZWAuXG4gKlxuICogQHByaXZhdGVcbiAqIEBuYW1lIHNldFxuICogQG1lbWJlck9mIE1hcENhY2hlXG4gKiBAcGFyYW0ge3N0cmluZ30ga2V5IFRoZSBrZXkgb2YgdGhlIHZhbHVlIHRvIHNldC5cbiAqIEBwYXJhbSB7Kn0gdmFsdWUgVGhlIHZhbHVlIHRvIHNldC5cbiAqIEByZXR1cm5zIHtPYmplY3R9IFJldHVybnMgdGhlIG1hcCBjYWNoZSBpbnN0YW5jZS5cbiAqL1xuZnVuY3Rpb24gbWFwQ2FjaGVTZXQoa2V5LCB2YWx1ZSkge1xuICB2YXIgZGF0YSA9IGdldE1hcERhdGEodGhpcywga2V5KSxcbiAgICAgIHNpemUgPSBkYXRhLnNpemU7XG5cbiAgZGF0YS5zZXQoa2V5LCB2YWx1ZSk7XG4gIHRoaXMuc2l6ZSArPSBkYXRhLnNpemUgPT0gc2l6ZSA/IDAgOiAxO1xuICByZXR1cm4gdGhpcztcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBtYXBDYWNoZVNldDtcbiIsIi8qKlxuICogQ29udmVydHMgYG1hcGAgdG8gaXRzIGtleS12YWx1ZSBwYWlycy5cbiAqXG4gKiBAcHJpdmF0ZVxuICogQHBhcmFtIHtPYmplY3R9IG1hcCBUaGUgbWFwIHRvIGNvbnZlcnQuXG4gKiBAcmV0dXJucyB7QXJyYXl9IFJldHVybnMgdGhlIGtleS12YWx1ZSBwYWlycy5cbiAqL1xuZnVuY3Rpb24gbWFwVG9BcnJheShtYXApIHtcbiAgdmFyIGluZGV4ID0gLTEsXG4gICAgICByZXN1bHQgPSBBcnJheShtYXAuc2l6ZSk7XG5cbiAgbWFwLmZvckVhY2goZnVuY3Rpb24odmFsdWUsIGtleSkge1xuICAgIHJlc3VsdFsrK2luZGV4XSA9IFtrZXksIHZhbHVlXTtcbiAgfSk7XG4gIHJldHVybiByZXN1bHQ7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gbWFwVG9BcnJheTtcbiIsIi8qKlxuICogQSBzcGVjaWFsaXplZCB2ZXJzaW9uIG9mIGBtYXRjaGVzUHJvcGVydHlgIGZvciBzb3VyY2UgdmFsdWVzIHN1aXRhYmxlXG4gKiBmb3Igc3RyaWN0IGVxdWFsaXR5IGNvbXBhcmlzb25zLCBpLmUuIGA9PT1gLlxuICpcbiAqIEBwcml2YXRlXG4gKiBAcGFyYW0ge3N0cmluZ30ga2V5IFRoZSBrZXkgb2YgdGhlIHByb3BlcnR5IHRvIGdldC5cbiAqIEBwYXJhbSB7Kn0gc3JjVmFsdWUgVGhlIHZhbHVlIHRvIG1hdGNoLlxuICogQHJldHVybnMge0Z1bmN0aW9ufSBSZXR1cm5zIHRoZSBuZXcgc3BlYyBmdW5jdGlvbi5cbiAqL1xuZnVuY3Rpb24gbWF0Y2hlc1N0cmljdENvbXBhcmFibGUoa2V5LCBzcmNWYWx1ZSkge1xuICByZXR1cm4gZnVuY3Rpb24ob2JqZWN0KSB7XG4gICAgaWYgKG9iamVjdCA9PSBudWxsKSB7XG4gICAgICByZXR1cm4gZmFsc2U7XG4gICAgfVxuICAgIHJldHVybiBvYmplY3Rba2V5XSA9PT0gc3JjVmFsdWUgJiZcbiAgICAgIChzcmNWYWx1ZSAhPT0gdW5kZWZpbmVkIHx8IChrZXkgaW4gT2JqZWN0KG9iamVjdCkpKTtcbiAgfTtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBtYXRjaGVzU3RyaWN0Q29tcGFyYWJsZTtcbiIsInZhciBtZW1vaXplID0gcmVxdWlyZSgnLi9tZW1vaXplJyk7XG5cbi8qKiBVc2VkIGFzIHRoZSBtYXhpbXVtIG1lbW9pemUgY2FjaGUgc2l6ZS4gKi9cbnZhciBNQVhfTUVNT0laRV9TSVpFID0gNTAwO1xuXG4vKipcbiAqIEEgc3BlY2lhbGl6ZWQgdmVyc2lvbiBvZiBgXy5tZW1vaXplYCB3aGljaCBjbGVhcnMgdGhlIG1lbW9pemVkIGZ1bmN0aW9uJ3NcbiAqIGNhY2hlIHdoZW4gaXQgZXhjZWVkcyBgTUFYX01FTU9JWkVfU0laRWAuXG4gKlxuICogQHByaXZhdGVcbiAqIEBwYXJhbSB7RnVuY3Rpb259IGZ1bmMgVGhlIGZ1bmN0aW9uIHRvIGhhdmUgaXRzIG91dHB1dCBtZW1vaXplZC5cbiAqIEByZXR1cm5zIHtGdW5jdGlvbn0gUmV0dXJucyB0aGUgbmV3IG1lbW9pemVkIGZ1bmN0aW9uLlxuICovXG5mdW5jdGlvbiBtZW1vaXplQ2FwcGVkKGZ1bmMpIHtcbiAgdmFyIHJlc3VsdCA9IG1lbW9pemUoZnVuYywgZnVuY3Rpb24oa2V5KSB7XG4gICAgaWYgKGNhY2hlLnNpemUgPT09IE1BWF9NRU1PSVpFX1NJWkUpIHtcbiAgICAgIGNhY2hlLmNsZWFyKCk7XG4gICAgfVxuICAgIHJldHVybiBrZXk7XG4gIH0pO1xuXG4gIHZhciBjYWNoZSA9IHJlc3VsdC5jYWNoZTtcbiAgcmV0dXJuIHJlc3VsdDtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBtZW1vaXplQ2FwcGVkO1xuIiwidmFyIGdldE5hdGl2ZSA9IHJlcXVpcmUoJy4vX2dldE5hdGl2ZScpO1xuXG4vKiBCdWlsdC1pbiBtZXRob2QgcmVmZXJlbmNlcyB0aGF0IGFyZSB2ZXJpZmllZCB0byBiZSBuYXRpdmUuICovXG52YXIgbmF0aXZlQ3JlYXRlID0gZ2V0TmF0aXZlKE9iamVjdCwgJ2NyZWF0ZScpO1xuXG5tb2R1bGUuZXhwb3J0cyA9IG5hdGl2ZUNyZWF0ZTtcbiIsInZhciBvdmVyQXJnID0gcmVxdWlyZSgnLi9fb3ZlckFyZycpO1xuXG4vKiBCdWlsdC1pbiBtZXRob2QgcmVmZXJlbmNlcyBmb3IgdGhvc2Ugd2l0aCB0aGUgc2FtZSBuYW1lIGFzIG90aGVyIGBsb2Rhc2hgIG1ldGhvZHMuICovXG52YXIgbmF0aXZlS2V5cyA9IG92ZXJBcmcoT2JqZWN0LmtleXMsIE9iamVjdCk7XG5cbm1vZHVsZS5leHBvcnRzID0gbmF0aXZlS2V5cztcbiIsInZhciBmcmVlR2xvYmFsID0gcmVxdWlyZSgnLi9fZnJlZUdsb2JhbCcpO1xuXG4vKiogRGV0ZWN0IGZyZWUgdmFyaWFibGUgYGV4cG9ydHNgLiAqL1xudmFyIGZyZWVFeHBvcnRzID0gdHlwZW9mIGV4cG9ydHMgPT0gJ29iamVjdCcgJiYgZXhwb3J0cyAmJiAhZXhwb3J0cy5ub2RlVHlwZSAmJiBleHBvcnRzO1xuXG4vKiogRGV0ZWN0IGZyZWUgdmFyaWFibGUgYG1vZHVsZWAuICovXG52YXIgZnJlZU1vZHVsZSA9IGZyZWVFeHBvcnRzICYmIHR5cGVvZiBtb2R1bGUgPT0gJ29iamVjdCcgJiYgbW9kdWxlICYmICFtb2R1bGUubm9kZVR5cGUgJiYgbW9kdWxlO1xuXG4vKiogRGV0ZWN0IHRoZSBwb3B1bGFyIENvbW1vbkpTIGV4dGVuc2lvbiBgbW9kdWxlLmV4cG9ydHNgLiAqL1xudmFyIG1vZHVsZUV4cG9ydHMgPSBmcmVlTW9kdWxlICYmIGZyZWVNb2R1bGUuZXhwb3J0cyA9PT0gZnJlZUV4cG9ydHM7XG5cbi8qKiBEZXRlY3QgZnJlZSB2YXJpYWJsZSBgcHJvY2Vzc2AgZnJvbSBOb2RlLmpzLiAqL1xudmFyIGZyZWVQcm9jZXNzID0gbW9kdWxlRXhwb3J0cyAmJiBmcmVlR2xvYmFsLnByb2Nlc3M7XG5cbi8qKiBVc2VkIHRvIGFjY2VzcyBmYXN0ZXIgTm9kZS5qcyBoZWxwZXJzLiAqL1xudmFyIG5vZGVVdGlsID0gKGZ1bmN0aW9uKCkge1xuICB0cnkge1xuICAgIC8vIFVzZSBgdXRpbC50eXBlc2AgZm9yIE5vZGUuanMgMTArLlxuICAgIHZhciB0eXBlcyA9IGZyZWVNb2R1bGUgJiYgZnJlZU1vZHVsZS5yZXF1aXJlICYmIGZyZWVNb2R1bGUucmVxdWlyZSgndXRpbCcpLnR5cGVzO1xuXG4gICAgaWYgKHR5cGVzKSB7XG4gICAgICByZXR1cm4gdHlwZXM7XG4gICAgfVxuXG4gICAgLy8gTGVnYWN5IGBwcm9jZXNzLmJpbmRpbmcoJ3V0aWwnKWAgZm9yIE5vZGUuanMgPCAxMC5cbiAgICByZXR1cm4gZnJlZVByb2Nlc3MgJiYgZnJlZVByb2Nlc3MuYmluZGluZyAmJiBmcmVlUHJvY2Vzcy5iaW5kaW5nKCd1dGlsJyk7XG4gIH0gY2F0Y2ggKGUpIHt9XG59KCkpO1xuXG5tb2R1bGUuZXhwb3J0cyA9IG5vZGVVdGlsO1xuIiwiLyoqIFVzZWQgZm9yIGJ1aWx0LWluIG1ldGhvZCByZWZlcmVuY2VzLiAqL1xudmFyIG9iamVjdFByb3RvID0gT2JqZWN0LnByb3RvdHlwZTtcblxuLyoqXG4gKiBVc2VkIHRvIHJlc29sdmUgdGhlXG4gKiBbYHRvU3RyaW5nVGFnYF0oaHR0cDovL2VjbWEtaW50ZXJuYXRpb25hbC5vcmcvZWNtYS0yNjIvNy4wLyNzZWMtb2JqZWN0LnByb3RvdHlwZS50b3N0cmluZylcbiAqIG9mIHZhbHVlcy5cbiAqL1xudmFyIG5hdGl2ZU9iamVjdFRvU3RyaW5nID0gb2JqZWN0UHJvdG8udG9TdHJpbmc7XG5cbi8qKlxuICogQ29udmVydHMgYHZhbHVlYCB0byBhIHN0cmluZyB1c2luZyBgT2JqZWN0LnByb3RvdHlwZS50b1N0cmluZ2AuXG4gKlxuICogQHByaXZhdGVcbiAqIEBwYXJhbSB7Kn0gdmFsdWUgVGhlIHZhbHVlIHRvIGNvbnZlcnQuXG4gKiBAcmV0dXJucyB7c3RyaW5nfSBSZXR1cm5zIHRoZSBjb252ZXJ0ZWQgc3RyaW5nLlxuICovXG5mdW5jdGlvbiBvYmplY3RUb1N0cmluZyh2YWx1ZSkge1xuICByZXR1cm4gbmF0aXZlT2JqZWN0VG9TdHJpbmcuY2FsbCh2YWx1ZSk7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gb2JqZWN0VG9TdHJpbmc7XG4iLCIvKipcbiAqIENyZWF0ZXMgYSB1bmFyeSBmdW5jdGlvbiB0aGF0IGludm9rZXMgYGZ1bmNgIHdpdGggaXRzIGFyZ3VtZW50IHRyYW5zZm9ybWVkLlxuICpcbiAqIEBwcml2YXRlXG4gKiBAcGFyYW0ge0Z1bmN0aW9ufSBmdW5jIFRoZSBmdW5jdGlvbiB0byB3cmFwLlxuICogQHBhcmFtIHtGdW5jdGlvbn0gdHJhbnNmb3JtIFRoZSBhcmd1bWVudCB0cmFuc2Zvcm0uXG4gKiBAcmV0dXJucyB7RnVuY3Rpb259IFJldHVybnMgdGhlIG5ldyBmdW5jdGlvbi5cbiAqL1xuZnVuY3Rpb24gb3ZlckFyZyhmdW5jLCB0cmFuc2Zvcm0pIHtcbiAgcmV0dXJuIGZ1bmN0aW9uKGFyZykge1xuICAgIHJldHVybiBmdW5jKHRyYW5zZm9ybShhcmcpKTtcbiAgfTtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBvdmVyQXJnO1xuIiwidmFyIGZyZWVHbG9iYWwgPSByZXF1aXJlKCcuL19mcmVlR2xvYmFsJyk7XG5cbi8qKiBEZXRlY3QgZnJlZSB2YXJpYWJsZSBgc2VsZmAuICovXG52YXIgZnJlZVNlbGYgPSB0eXBlb2Ygc2VsZiA9PSAnb2JqZWN0JyAmJiBzZWxmICYmIHNlbGYuT2JqZWN0ID09PSBPYmplY3QgJiYgc2VsZjtcblxuLyoqIFVzZWQgYXMgYSByZWZlcmVuY2UgdG8gdGhlIGdsb2JhbCBvYmplY3QuICovXG52YXIgcm9vdCA9IGZyZWVHbG9iYWwgfHwgZnJlZVNlbGYgfHwgRnVuY3Rpb24oJ3JldHVybiB0aGlzJykoKTtcblxubW9kdWxlLmV4cG9ydHMgPSByb290O1xuIiwiLyoqIFVzZWQgdG8gc3RhbmQtaW4gZm9yIGB1bmRlZmluZWRgIGhhc2ggdmFsdWVzLiAqL1xudmFyIEhBU0hfVU5ERUZJTkVEID0gJ19fbG9kYXNoX2hhc2hfdW5kZWZpbmVkX18nO1xuXG4vKipcbiAqIEFkZHMgYHZhbHVlYCB0byB0aGUgYXJyYXkgY2FjaGUuXG4gKlxuICogQHByaXZhdGVcbiAqIEBuYW1lIGFkZFxuICogQG1lbWJlck9mIFNldENhY2hlXG4gKiBAYWxpYXMgcHVzaFxuICogQHBhcmFtIHsqfSB2YWx1ZSBUaGUgdmFsdWUgdG8gY2FjaGUuXG4gKiBAcmV0dXJucyB7T2JqZWN0fSBSZXR1cm5zIHRoZSBjYWNoZSBpbnN0YW5jZS5cbiAqL1xuZnVuY3Rpb24gc2V0Q2FjaGVBZGQodmFsdWUpIHtcbiAgdGhpcy5fX2RhdGFfXy5zZXQodmFsdWUsIEhBU0hfVU5ERUZJTkVEKTtcbiAgcmV0dXJuIHRoaXM7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gc2V0Q2FjaGVBZGQ7XG4iLCIvKipcbiAqIENoZWNrcyBpZiBgdmFsdWVgIGlzIGluIHRoZSBhcnJheSBjYWNoZS5cbiAqXG4gKiBAcHJpdmF0ZVxuICogQG5hbWUgaGFzXG4gKiBAbWVtYmVyT2YgU2V0Q2FjaGVcbiAqIEBwYXJhbSB7Kn0gdmFsdWUgVGhlIHZhbHVlIHRvIHNlYXJjaCBmb3IuXG4gKiBAcmV0dXJucyB7bnVtYmVyfSBSZXR1cm5zIGB0cnVlYCBpZiBgdmFsdWVgIGlzIGZvdW5kLCBlbHNlIGBmYWxzZWAuXG4gKi9cbmZ1bmN0aW9uIHNldENhY2hlSGFzKHZhbHVlKSB7XG4gIHJldHVybiB0aGlzLl9fZGF0YV9fLmhhcyh2YWx1ZSk7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gc2V0Q2FjaGVIYXM7XG4iLCIvKipcbiAqIENvbnZlcnRzIGBzZXRgIHRvIGFuIGFycmF5IG9mIGl0cyB2YWx1ZXMuXG4gKlxuICogQHByaXZhdGVcbiAqIEBwYXJhbSB7T2JqZWN0fSBzZXQgVGhlIHNldCB0byBjb252ZXJ0LlxuICogQHJldHVybnMge0FycmF5fSBSZXR1cm5zIHRoZSB2YWx1ZXMuXG4gKi9cbmZ1bmN0aW9uIHNldFRvQXJyYXkoc2V0KSB7XG4gIHZhciBpbmRleCA9IC0xLFxuICAgICAgcmVzdWx0ID0gQXJyYXkoc2V0LnNpemUpO1xuXG4gIHNldC5mb3JFYWNoKGZ1bmN0aW9uKHZhbHVlKSB7XG4gICAgcmVzdWx0WysraW5kZXhdID0gdmFsdWU7XG4gIH0pO1xuICByZXR1cm4gcmVzdWx0O1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IHNldFRvQXJyYXk7XG4iLCJ2YXIgTGlzdENhY2hlID0gcmVxdWlyZSgnLi9fTGlzdENhY2hlJyk7XG5cbi8qKlxuICogUmVtb3ZlcyBhbGwga2V5LXZhbHVlIGVudHJpZXMgZnJvbSB0aGUgc3RhY2suXG4gKlxuICogQHByaXZhdGVcbiAqIEBuYW1lIGNsZWFyXG4gKiBAbWVtYmVyT2YgU3RhY2tcbiAqL1xuZnVuY3Rpb24gc3RhY2tDbGVhcigpIHtcbiAgdGhpcy5fX2RhdGFfXyA9IG5ldyBMaXN0Q2FjaGU7XG4gIHRoaXMuc2l6ZSA9IDA7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gc3RhY2tDbGVhcjtcbiIsIi8qKlxuICogUmVtb3ZlcyBga2V5YCBhbmQgaXRzIHZhbHVlIGZyb20gdGhlIHN0YWNrLlxuICpcbiAqIEBwcml2YXRlXG4gKiBAbmFtZSBkZWxldGVcbiAqIEBtZW1iZXJPZiBTdGFja1xuICogQHBhcmFtIHtzdHJpbmd9IGtleSBUaGUga2V5IG9mIHRoZSB2YWx1ZSB0byByZW1vdmUuXG4gKiBAcmV0dXJucyB7Ym9vbGVhbn0gUmV0dXJucyBgdHJ1ZWAgaWYgdGhlIGVudHJ5IHdhcyByZW1vdmVkLCBlbHNlIGBmYWxzZWAuXG4gKi9cbmZ1bmN0aW9uIHN0YWNrRGVsZXRlKGtleSkge1xuICB2YXIgZGF0YSA9IHRoaXMuX19kYXRhX18sXG4gICAgICByZXN1bHQgPSBkYXRhWydkZWxldGUnXShrZXkpO1xuXG4gIHRoaXMuc2l6ZSA9IGRhdGEuc2l6ZTtcbiAgcmV0dXJuIHJlc3VsdDtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBzdGFja0RlbGV0ZTtcbiIsIi8qKlxuICogR2V0cyB0aGUgc3RhY2sgdmFsdWUgZm9yIGBrZXlgLlxuICpcbiAqIEBwcml2YXRlXG4gKiBAbmFtZSBnZXRcbiAqIEBtZW1iZXJPZiBTdGFja1xuICogQHBhcmFtIHtzdHJpbmd9IGtleSBUaGUga2V5IG9mIHRoZSB2YWx1ZSB0byBnZXQuXG4gKiBAcmV0dXJucyB7Kn0gUmV0dXJucyB0aGUgZW50cnkgdmFsdWUuXG4gKi9cbmZ1bmN0aW9uIHN0YWNrR2V0KGtleSkge1xuICByZXR1cm4gdGhpcy5fX2RhdGFfXy5nZXQoa2V5KTtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBzdGFja0dldDtcbiIsIi8qKlxuICogQ2hlY2tzIGlmIGEgc3RhY2sgdmFsdWUgZm9yIGBrZXlgIGV4aXN0cy5cbiAqXG4gKiBAcHJpdmF0ZVxuICogQG5hbWUgaGFzXG4gKiBAbWVtYmVyT2YgU3RhY2tcbiAqIEBwYXJhbSB7c3RyaW5nfSBrZXkgVGhlIGtleSBvZiB0aGUgZW50cnkgdG8gY2hlY2suXG4gKiBAcmV0dXJucyB7Ym9vbGVhbn0gUmV0dXJucyBgdHJ1ZWAgaWYgYW4gZW50cnkgZm9yIGBrZXlgIGV4aXN0cywgZWxzZSBgZmFsc2VgLlxuICovXG5mdW5jdGlvbiBzdGFja0hhcyhrZXkpIHtcbiAgcmV0dXJuIHRoaXMuX19kYXRhX18uaGFzKGtleSk7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gc3RhY2tIYXM7XG4iLCJ2YXIgTGlzdENhY2hlID0gcmVxdWlyZSgnLi9fTGlzdENhY2hlJyksXG4gICAgTWFwID0gcmVxdWlyZSgnLi9fTWFwJyksXG4gICAgTWFwQ2FjaGUgPSByZXF1aXJlKCcuL19NYXBDYWNoZScpO1xuXG4vKiogVXNlZCBhcyB0aGUgc2l6ZSB0byBlbmFibGUgbGFyZ2UgYXJyYXkgb3B0aW1pemF0aW9ucy4gKi9cbnZhciBMQVJHRV9BUlJBWV9TSVpFID0gMjAwO1xuXG4vKipcbiAqIFNldHMgdGhlIHN0YWNrIGBrZXlgIHRvIGB2YWx1ZWAuXG4gKlxuICogQHByaXZhdGVcbiAqIEBuYW1lIHNldFxuICogQG1lbWJlck9mIFN0YWNrXG4gKiBAcGFyYW0ge3N0cmluZ30ga2V5IFRoZSBrZXkgb2YgdGhlIHZhbHVlIHRvIHNldC5cbiAqIEBwYXJhbSB7Kn0gdmFsdWUgVGhlIHZhbHVlIHRvIHNldC5cbiAqIEByZXR1cm5zIHtPYmplY3R9IFJldHVybnMgdGhlIHN0YWNrIGNhY2hlIGluc3RhbmNlLlxuICovXG5mdW5jdGlvbiBzdGFja1NldChrZXksIHZhbHVlKSB7XG4gIHZhciBkYXRhID0gdGhpcy5fX2RhdGFfXztcbiAgaWYgKGRhdGEgaW5zdGFuY2VvZiBMaXN0Q2FjaGUpIHtcbiAgICB2YXIgcGFpcnMgPSBkYXRhLl9fZGF0YV9fO1xuICAgIGlmICghTWFwIHx8IChwYWlycy5sZW5ndGggPCBMQVJHRV9BUlJBWV9TSVpFIC0gMSkpIHtcbiAgICAgIHBhaXJzLnB1c2goW2tleSwgdmFsdWVdKTtcbiAgICAgIHRoaXMuc2l6ZSA9ICsrZGF0YS5zaXplO1xuICAgICAgcmV0dXJuIHRoaXM7XG4gICAgfVxuICAgIGRhdGEgPSB0aGlzLl9fZGF0YV9fID0gbmV3IE1hcENhY2hlKHBhaXJzKTtcbiAgfVxuICBkYXRhLnNldChrZXksIHZhbHVlKTtcbiAgdGhpcy5zaXplID0gZGF0YS5zaXplO1xuICByZXR1cm4gdGhpcztcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBzdGFja1NldDtcbiIsInZhciBtZW1vaXplQ2FwcGVkID0gcmVxdWlyZSgnLi9fbWVtb2l6ZUNhcHBlZCcpO1xuXG4vKiogVXNlZCB0byBtYXRjaCBwcm9wZXJ0eSBuYW1lcyB3aXRoaW4gcHJvcGVydHkgcGF0aHMuICovXG52YXIgcmVQcm9wTmFtZSA9IC9bXi5bXFxdXSt8XFxbKD86KC0/XFxkKyg/OlxcLlxcZCspPyl8KFtcIiddKSgoPzooPyFcXDIpW15cXFxcXXxcXFxcLikqPylcXDIpXFxdfCg/PSg/OlxcLnxcXFtcXF0pKD86XFwufFxcW1xcXXwkKSkvZztcblxuLyoqIFVzZWQgdG8gbWF0Y2ggYmFja3NsYXNoZXMgaW4gcHJvcGVydHkgcGF0aHMuICovXG52YXIgcmVFc2NhcGVDaGFyID0gL1xcXFwoXFxcXCk/L2c7XG5cbi8qKlxuICogQ29udmVydHMgYHN0cmluZ2AgdG8gYSBwcm9wZXJ0eSBwYXRoIGFycmF5LlxuICpcbiAqIEBwcml2YXRlXG4gKiBAcGFyYW0ge3N0cmluZ30gc3RyaW5nIFRoZSBzdHJpbmcgdG8gY29udmVydC5cbiAqIEByZXR1cm5zIHtBcnJheX0gUmV0dXJucyB0aGUgcHJvcGVydHkgcGF0aCBhcnJheS5cbiAqL1xudmFyIHN0cmluZ1RvUGF0aCA9IG1lbW9pemVDYXBwZWQoZnVuY3Rpb24oc3RyaW5nKSB7XG4gIHZhciByZXN1bHQgPSBbXTtcbiAgaWYgKHN0cmluZy5jaGFyQ29kZUF0KDApID09PSA0NiAvKiAuICovKSB7XG4gICAgcmVzdWx0LnB1c2goJycpO1xuICB9XG4gIHN0cmluZy5yZXBsYWNlKHJlUHJvcE5hbWUsIGZ1bmN0aW9uKG1hdGNoLCBudW1iZXIsIHF1b3RlLCBzdWJTdHJpbmcpIHtcbiAgICByZXN1bHQucHVzaChxdW90ZSA/IHN1YlN0cmluZy5yZXBsYWNlKHJlRXNjYXBlQ2hhciwgJyQxJykgOiAobnVtYmVyIHx8IG1hdGNoKSk7XG4gIH0pO1xuICByZXR1cm4gcmVzdWx0O1xufSk7XG5cbm1vZHVsZS5leHBvcnRzID0gc3RyaW5nVG9QYXRoO1xuIiwidmFyIGlzU3ltYm9sID0gcmVxdWlyZSgnLi9pc1N5bWJvbCcpO1xuXG4vKiogVXNlZCBhcyByZWZlcmVuY2VzIGZvciB2YXJpb3VzIGBOdW1iZXJgIGNvbnN0YW50cy4gKi9cbnZhciBJTkZJTklUWSA9IDEgLyAwO1xuXG4vKipcbiAqIENvbnZlcnRzIGB2YWx1ZWAgdG8gYSBzdHJpbmcga2V5IGlmIGl0J3Mgbm90IGEgc3RyaW5nIG9yIHN5bWJvbC5cbiAqXG4gKiBAcHJpdmF0ZVxuICogQHBhcmFtIHsqfSB2YWx1ZSBUaGUgdmFsdWUgdG8gaW5zcGVjdC5cbiAqIEByZXR1cm5zIHtzdHJpbmd8c3ltYm9sfSBSZXR1cm5zIHRoZSBrZXkuXG4gKi9cbmZ1bmN0aW9uIHRvS2V5KHZhbHVlKSB7XG4gIGlmICh0eXBlb2YgdmFsdWUgPT0gJ3N0cmluZycgfHwgaXNTeW1ib2wodmFsdWUpKSB7XG4gICAgcmV0dXJuIHZhbHVlO1xuICB9XG4gIHZhciByZXN1bHQgPSAodmFsdWUgKyAnJyk7XG4gIHJldHVybiAocmVzdWx0ID09ICcwJyAmJiAoMSAvIHZhbHVlKSA9PSAtSU5GSU5JVFkpID8gJy0wJyA6IHJlc3VsdDtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSB0b0tleTtcbiIsIi8qKiBVc2VkIGZvciBidWlsdC1pbiBtZXRob2QgcmVmZXJlbmNlcy4gKi9cbnZhciBmdW5jUHJvdG8gPSBGdW5jdGlvbi5wcm90b3R5cGU7XG5cbi8qKiBVc2VkIHRvIHJlc29sdmUgdGhlIGRlY29tcGlsZWQgc291cmNlIG9mIGZ1bmN0aW9ucy4gKi9cbnZhciBmdW5jVG9TdHJpbmcgPSBmdW5jUHJvdG8udG9TdHJpbmc7XG5cbi8qKlxuICogQ29udmVydHMgYGZ1bmNgIHRvIGl0cyBzb3VyY2UgY29kZS5cbiAqXG4gKiBAcHJpdmF0ZVxuICogQHBhcmFtIHtGdW5jdGlvbn0gZnVuYyBUaGUgZnVuY3Rpb24gdG8gY29udmVydC5cbiAqIEByZXR1cm5zIHtzdHJpbmd9IFJldHVybnMgdGhlIHNvdXJjZSBjb2RlLlxuICovXG5mdW5jdGlvbiB0b1NvdXJjZShmdW5jKSB7XG4gIGlmIChmdW5jICE9IG51bGwpIHtcbiAgICB0cnkge1xuICAgICAgcmV0dXJuIGZ1bmNUb1N0cmluZy5jYWxsKGZ1bmMpO1xuICAgIH0gY2F0Y2ggKGUpIHt9XG4gICAgdHJ5IHtcbiAgICAgIHJldHVybiAoZnVuYyArICcnKTtcbiAgICB9IGNhdGNoIChlKSB7fVxuICB9XG4gIHJldHVybiAnJztcbn1cblxubW9kdWxlLmV4cG9ydHMgPSB0b1NvdXJjZTtcbiIsIi8qKlxuICogUGVyZm9ybXMgYVxuICogW2BTYW1lVmFsdWVaZXJvYF0oaHR0cDovL2VjbWEtaW50ZXJuYXRpb25hbC5vcmcvZWNtYS0yNjIvNy4wLyNzZWMtc2FtZXZhbHVlemVybylcbiAqIGNvbXBhcmlzb24gYmV0d2VlbiB0d28gdmFsdWVzIHRvIGRldGVybWluZSBpZiB0aGV5IGFyZSBlcXVpdmFsZW50LlxuICpcbiAqIEBzdGF0aWNcbiAqIEBtZW1iZXJPZiBfXG4gKiBAc2luY2UgNC4wLjBcbiAqIEBjYXRlZ29yeSBMYW5nXG4gKiBAcGFyYW0geyp9IHZhbHVlIFRoZSB2YWx1ZSB0byBjb21wYXJlLlxuICogQHBhcmFtIHsqfSBvdGhlciBUaGUgb3RoZXIgdmFsdWUgdG8gY29tcGFyZS5cbiAqIEByZXR1cm5zIHtib29sZWFufSBSZXR1cm5zIGB0cnVlYCBpZiB0aGUgdmFsdWVzIGFyZSBlcXVpdmFsZW50LCBlbHNlIGBmYWxzZWAuXG4gKiBAZXhhbXBsZVxuICpcbiAqIHZhciBvYmplY3QgPSB7ICdhJzogMSB9O1xuICogdmFyIG90aGVyID0geyAnYSc6IDEgfTtcbiAqXG4gKiBfLmVxKG9iamVjdCwgb2JqZWN0KTtcbiAqIC8vID0+IHRydWVcbiAqXG4gKiBfLmVxKG9iamVjdCwgb3RoZXIpO1xuICogLy8gPT4gZmFsc2VcbiAqXG4gKiBfLmVxKCdhJywgJ2EnKTtcbiAqIC8vID0+IHRydWVcbiAqXG4gKiBfLmVxKCdhJywgT2JqZWN0KCdhJykpO1xuICogLy8gPT4gZmFsc2VcbiAqXG4gKiBfLmVxKE5hTiwgTmFOKTtcbiAqIC8vID0+IHRydWVcbiAqL1xuZnVuY3Rpb24gZXEodmFsdWUsIG90aGVyKSB7XG4gIHJldHVybiB2YWx1ZSA9PT0gb3RoZXIgfHwgKHZhbHVlICE9PSB2YWx1ZSAmJiBvdGhlciAhPT0gb3RoZXIpO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IGVxO1xuIiwidmFyIGNyZWF0ZUZpbmQgPSByZXF1aXJlKCcuL19jcmVhdGVGaW5kJyksXG4gICAgZmluZEluZGV4ID0gcmVxdWlyZSgnLi9maW5kSW5kZXgnKTtcblxuLyoqXG4gKiBJdGVyYXRlcyBvdmVyIGVsZW1lbnRzIG9mIGBjb2xsZWN0aW9uYCwgcmV0dXJuaW5nIHRoZSBmaXJzdCBlbGVtZW50XG4gKiBgcHJlZGljYXRlYCByZXR1cm5zIHRydXRoeSBmb3IuIFRoZSBwcmVkaWNhdGUgaXMgaW52b2tlZCB3aXRoIHRocmVlXG4gKiBhcmd1bWVudHM6ICh2YWx1ZSwgaW5kZXh8a2V5LCBjb2xsZWN0aW9uKS5cbiAqXG4gKiBAc3RhdGljXG4gKiBAbWVtYmVyT2YgX1xuICogQHNpbmNlIDAuMS4wXG4gKiBAY2F0ZWdvcnkgQ29sbGVjdGlvblxuICogQHBhcmFtIHtBcnJheXxPYmplY3R9IGNvbGxlY3Rpb24gVGhlIGNvbGxlY3Rpb24gdG8gaW5zcGVjdC5cbiAqIEBwYXJhbSB7RnVuY3Rpb259IFtwcmVkaWNhdGU9Xy5pZGVudGl0eV0gVGhlIGZ1bmN0aW9uIGludm9rZWQgcGVyIGl0ZXJhdGlvbi5cbiAqIEBwYXJhbSB7bnVtYmVyfSBbZnJvbUluZGV4PTBdIFRoZSBpbmRleCB0byBzZWFyY2ggZnJvbS5cbiAqIEByZXR1cm5zIHsqfSBSZXR1cm5zIHRoZSBtYXRjaGVkIGVsZW1lbnQsIGVsc2UgYHVuZGVmaW5lZGAuXG4gKiBAZXhhbXBsZVxuICpcbiAqIHZhciB1c2VycyA9IFtcbiAqICAgeyAndXNlcic6ICdiYXJuZXknLCAgJ2FnZSc6IDM2LCAnYWN0aXZlJzogdHJ1ZSB9LFxuICogICB7ICd1c2VyJzogJ2ZyZWQnLCAgICAnYWdlJzogNDAsICdhY3RpdmUnOiBmYWxzZSB9LFxuICogICB7ICd1c2VyJzogJ3BlYmJsZXMnLCAnYWdlJzogMSwgICdhY3RpdmUnOiB0cnVlIH1cbiAqIF07XG4gKlxuICogXy5maW5kKHVzZXJzLCBmdW5jdGlvbihvKSB7IHJldHVybiBvLmFnZSA8IDQwOyB9KTtcbiAqIC8vID0+IG9iamVjdCBmb3IgJ2Jhcm5leSdcbiAqXG4gKiAvLyBUaGUgYF8ubWF0Y2hlc2AgaXRlcmF0ZWUgc2hvcnRoYW5kLlxuICogXy5maW5kKHVzZXJzLCB7ICdhZ2UnOiAxLCAnYWN0aXZlJzogdHJ1ZSB9KTtcbiAqIC8vID0+IG9iamVjdCBmb3IgJ3BlYmJsZXMnXG4gKlxuICogLy8gVGhlIGBfLm1hdGNoZXNQcm9wZXJ0eWAgaXRlcmF0ZWUgc2hvcnRoYW5kLlxuICogXy5maW5kKHVzZXJzLCBbJ2FjdGl2ZScsIGZhbHNlXSk7XG4gKiAvLyA9PiBvYmplY3QgZm9yICdmcmVkJ1xuICpcbiAqIC8vIFRoZSBgXy5wcm9wZXJ0eWAgaXRlcmF0ZWUgc2hvcnRoYW5kLlxuICogXy5maW5kKHVzZXJzLCAnYWN0aXZlJyk7XG4gKiAvLyA9PiBvYmplY3QgZm9yICdiYXJuZXknXG4gKi9cbnZhciBmaW5kID0gY3JlYXRlRmluZChmaW5kSW5kZXgpO1xuXG5tb2R1bGUuZXhwb3J0cyA9IGZpbmQ7XG4iLCJ2YXIgYmFzZUZpbmRJbmRleCA9IHJlcXVpcmUoJy4vX2Jhc2VGaW5kSW5kZXgnKSxcbiAgICBiYXNlSXRlcmF0ZWUgPSByZXF1aXJlKCcuL19iYXNlSXRlcmF0ZWUnKSxcbiAgICB0b0ludGVnZXIgPSByZXF1aXJlKCcuL3RvSW50ZWdlcicpO1xuXG4vKiBCdWlsdC1pbiBtZXRob2QgcmVmZXJlbmNlcyBmb3IgdGhvc2Ugd2l0aCB0aGUgc2FtZSBuYW1lIGFzIG90aGVyIGBsb2Rhc2hgIG1ldGhvZHMuICovXG52YXIgbmF0aXZlTWF4ID0gTWF0aC5tYXg7XG5cbi8qKlxuICogVGhpcyBtZXRob2QgaXMgbGlrZSBgXy5maW5kYCBleGNlcHQgdGhhdCBpdCByZXR1cm5zIHRoZSBpbmRleCBvZiB0aGUgZmlyc3RcbiAqIGVsZW1lbnQgYHByZWRpY2F0ZWAgcmV0dXJucyB0cnV0aHkgZm9yIGluc3RlYWQgb2YgdGhlIGVsZW1lbnQgaXRzZWxmLlxuICpcbiAqIEBzdGF0aWNcbiAqIEBtZW1iZXJPZiBfXG4gKiBAc2luY2UgMS4xLjBcbiAqIEBjYXRlZ29yeSBBcnJheVxuICogQHBhcmFtIHtBcnJheX0gYXJyYXkgVGhlIGFycmF5IHRvIGluc3BlY3QuXG4gKiBAcGFyYW0ge0Z1bmN0aW9ufSBbcHJlZGljYXRlPV8uaWRlbnRpdHldIFRoZSBmdW5jdGlvbiBpbnZva2VkIHBlciBpdGVyYXRpb24uXG4gKiBAcGFyYW0ge251bWJlcn0gW2Zyb21JbmRleD0wXSBUaGUgaW5kZXggdG8gc2VhcmNoIGZyb20uXG4gKiBAcmV0dXJucyB7bnVtYmVyfSBSZXR1cm5zIHRoZSBpbmRleCBvZiB0aGUgZm91bmQgZWxlbWVudCwgZWxzZSBgLTFgLlxuICogQGV4YW1wbGVcbiAqXG4gKiB2YXIgdXNlcnMgPSBbXG4gKiAgIHsgJ3VzZXInOiAnYmFybmV5JywgICdhY3RpdmUnOiBmYWxzZSB9LFxuICogICB7ICd1c2VyJzogJ2ZyZWQnLCAgICAnYWN0aXZlJzogZmFsc2UgfSxcbiAqICAgeyAndXNlcic6ICdwZWJibGVzJywgJ2FjdGl2ZSc6IHRydWUgfVxuICogXTtcbiAqXG4gKiBfLmZpbmRJbmRleCh1c2VycywgZnVuY3Rpb24obykgeyByZXR1cm4gby51c2VyID09ICdiYXJuZXknOyB9KTtcbiAqIC8vID0+IDBcbiAqXG4gKiAvLyBUaGUgYF8ubWF0Y2hlc2AgaXRlcmF0ZWUgc2hvcnRoYW5kLlxuICogXy5maW5kSW5kZXgodXNlcnMsIHsgJ3VzZXInOiAnZnJlZCcsICdhY3RpdmUnOiBmYWxzZSB9KTtcbiAqIC8vID0+IDFcbiAqXG4gKiAvLyBUaGUgYF8ubWF0Y2hlc1Byb3BlcnR5YCBpdGVyYXRlZSBzaG9ydGhhbmQuXG4gKiBfLmZpbmRJbmRleCh1c2VycywgWydhY3RpdmUnLCBmYWxzZV0pO1xuICogLy8gPT4gMFxuICpcbiAqIC8vIFRoZSBgXy5wcm9wZXJ0eWAgaXRlcmF0ZWUgc2hvcnRoYW5kLlxuICogXy5maW5kSW5kZXgodXNlcnMsICdhY3RpdmUnKTtcbiAqIC8vID0+IDJcbiAqL1xuZnVuY3Rpb24gZmluZEluZGV4KGFycmF5LCBwcmVkaWNhdGUsIGZyb21JbmRleCkge1xuICB2YXIgbGVuZ3RoID0gYXJyYXkgPT0gbnVsbCA/IDAgOiBhcnJheS5sZW5ndGg7XG4gIGlmICghbGVuZ3RoKSB7XG4gICAgcmV0dXJuIC0xO1xuICB9XG4gIHZhciBpbmRleCA9IGZyb21JbmRleCA9PSBudWxsID8gMCA6IHRvSW50ZWdlcihmcm9tSW5kZXgpO1xuICBpZiAoaW5kZXggPCAwKSB7XG4gICAgaW5kZXggPSBuYXRpdmVNYXgobGVuZ3RoICsgaW5kZXgsIDApO1xuICB9XG4gIHJldHVybiBiYXNlRmluZEluZGV4KGFycmF5LCBiYXNlSXRlcmF0ZWUocHJlZGljYXRlLCAzKSwgaW5kZXgpO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IGZpbmRJbmRleDtcbiIsInZhciBiYXNlR2V0ID0gcmVxdWlyZSgnLi9fYmFzZUdldCcpO1xuXG4vKipcbiAqIEdldHMgdGhlIHZhbHVlIGF0IGBwYXRoYCBvZiBgb2JqZWN0YC4gSWYgdGhlIHJlc29sdmVkIHZhbHVlIGlzXG4gKiBgdW5kZWZpbmVkYCwgdGhlIGBkZWZhdWx0VmFsdWVgIGlzIHJldHVybmVkIGluIGl0cyBwbGFjZS5cbiAqXG4gKiBAc3RhdGljXG4gKiBAbWVtYmVyT2YgX1xuICogQHNpbmNlIDMuNy4wXG4gKiBAY2F0ZWdvcnkgT2JqZWN0XG4gKiBAcGFyYW0ge09iamVjdH0gb2JqZWN0IFRoZSBvYmplY3QgdG8gcXVlcnkuXG4gKiBAcGFyYW0ge0FycmF5fHN0cmluZ30gcGF0aCBUaGUgcGF0aCBvZiB0aGUgcHJvcGVydHkgdG8gZ2V0LlxuICogQHBhcmFtIHsqfSBbZGVmYXVsdFZhbHVlXSBUaGUgdmFsdWUgcmV0dXJuZWQgZm9yIGB1bmRlZmluZWRgIHJlc29sdmVkIHZhbHVlcy5cbiAqIEByZXR1cm5zIHsqfSBSZXR1cm5zIHRoZSByZXNvbHZlZCB2YWx1ZS5cbiAqIEBleGFtcGxlXG4gKlxuICogdmFyIG9iamVjdCA9IHsgJ2EnOiBbeyAnYic6IHsgJ2MnOiAzIH0gfV0gfTtcbiAqXG4gKiBfLmdldChvYmplY3QsICdhWzBdLmIuYycpO1xuICogLy8gPT4gM1xuICpcbiAqIF8uZ2V0KG9iamVjdCwgWydhJywgJzAnLCAnYicsICdjJ10pO1xuICogLy8gPT4gM1xuICpcbiAqIF8uZ2V0KG9iamVjdCwgJ2EuYi5jJywgJ2RlZmF1bHQnKTtcbiAqIC8vID0+ICdkZWZhdWx0J1xuICovXG5mdW5jdGlvbiBnZXQob2JqZWN0LCBwYXRoLCBkZWZhdWx0VmFsdWUpIHtcbiAgdmFyIHJlc3VsdCA9IG9iamVjdCA9PSBudWxsID8gdW5kZWZpbmVkIDogYmFzZUdldChvYmplY3QsIHBhdGgpO1xuICByZXR1cm4gcmVzdWx0ID09PSB1bmRlZmluZWQgPyBkZWZhdWx0VmFsdWUgOiByZXN1bHQ7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gZ2V0O1xuIiwidmFyIGJhc2VIYXNJbiA9IHJlcXVpcmUoJy4vX2Jhc2VIYXNJbicpLFxuICAgIGhhc1BhdGggPSByZXF1aXJlKCcuL19oYXNQYXRoJyk7XG5cbi8qKlxuICogQ2hlY2tzIGlmIGBwYXRoYCBpcyBhIGRpcmVjdCBvciBpbmhlcml0ZWQgcHJvcGVydHkgb2YgYG9iamVjdGAuXG4gKlxuICogQHN0YXRpY1xuICogQG1lbWJlck9mIF9cbiAqIEBzaW5jZSA0LjAuMFxuICogQGNhdGVnb3J5IE9iamVjdFxuICogQHBhcmFtIHtPYmplY3R9IG9iamVjdCBUaGUgb2JqZWN0IHRvIHF1ZXJ5LlxuICogQHBhcmFtIHtBcnJheXxzdHJpbmd9IHBhdGggVGhlIHBhdGggdG8gY2hlY2suXG4gKiBAcmV0dXJucyB7Ym9vbGVhbn0gUmV0dXJucyBgdHJ1ZWAgaWYgYHBhdGhgIGV4aXN0cywgZWxzZSBgZmFsc2VgLlxuICogQGV4YW1wbGVcbiAqXG4gKiB2YXIgb2JqZWN0ID0gXy5jcmVhdGUoeyAnYSc6IF8uY3JlYXRlKHsgJ2InOiAyIH0pIH0pO1xuICpcbiAqIF8uaGFzSW4ob2JqZWN0LCAnYScpO1xuICogLy8gPT4gdHJ1ZVxuICpcbiAqIF8uaGFzSW4ob2JqZWN0LCAnYS5iJyk7XG4gKiAvLyA9PiB0cnVlXG4gKlxuICogXy5oYXNJbihvYmplY3QsIFsnYScsICdiJ10pO1xuICogLy8gPT4gdHJ1ZVxuICpcbiAqIF8uaGFzSW4ob2JqZWN0LCAnYicpO1xuICogLy8gPT4gZmFsc2VcbiAqL1xuZnVuY3Rpb24gaGFzSW4ob2JqZWN0LCBwYXRoKSB7XG4gIHJldHVybiBvYmplY3QgIT0gbnVsbCAmJiBoYXNQYXRoKG9iamVjdCwgcGF0aCwgYmFzZUhhc0luKTtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBoYXNJbjtcbiIsIi8qKlxuICogVGhpcyBtZXRob2QgcmV0dXJucyB0aGUgZmlyc3QgYXJndW1lbnQgaXQgcmVjZWl2ZXMuXG4gKlxuICogQHN0YXRpY1xuICogQHNpbmNlIDAuMS4wXG4gKiBAbWVtYmVyT2YgX1xuICogQGNhdGVnb3J5IFV0aWxcbiAqIEBwYXJhbSB7Kn0gdmFsdWUgQW55IHZhbHVlLlxuICogQHJldHVybnMgeyp9IFJldHVybnMgYHZhbHVlYC5cbiAqIEBleGFtcGxlXG4gKlxuICogdmFyIG9iamVjdCA9IHsgJ2EnOiAxIH07XG4gKlxuICogY29uc29sZS5sb2coXy5pZGVudGl0eShvYmplY3QpID09PSBvYmplY3QpO1xuICogLy8gPT4gdHJ1ZVxuICovXG5mdW5jdGlvbiBpZGVudGl0eSh2YWx1ZSkge1xuICByZXR1cm4gdmFsdWU7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gaWRlbnRpdHk7XG4iLCJ2YXIgYmFzZUlzQXJndW1lbnRzID0gcmVxdWlyZSgnLi9fYmFzZUlzQXJndW1lbnRzJyksXG4gICAgaXNPYmplY3RMaWtlID0gcmVxdWlyZSgnLi9pc09iamVjdExpa2UnKTtcblxuLyoqIFVzZWQgZm9yIGJ1aWx0LWluIG1ldGhvZCByZWZlcmVuY2VzLiAqL1xudmFyIG9iamVjdFByb3RvID0gT2JqZWN0LnByb3RvdHlwZTtcblxuLyoqIFVzZWQgdG8gY2hlY2sgb2JqZWN0cyBmb3Igb3duIHByb3BlcnRpZXMuICovXG52YXIgaGFzT3duUHJvcGVydHkgPSBvYmplY3RQcm90by5oYXNPd25Qcm9wZXJ0eTtcblxuLyoqIEJ1aWx0LWluIHZhbHVlIHJlZmVyZW5jZXMuICovXG52YXIgcHJvcGVydHlJc0VudW1lcmFibGUgPSBvYmplY3RQcm90by5wcm9wZXJ0eUlzRW51bWVyYWJsZTtcblxuLyoqXG4gKiBDaGVja3MgaWYgYHZhbHVlYCBpcyBsaWtlbHkgYW4gYGFyZ3VtZW50c2Agb2JqZWN0LlxuICpcbiAqIEBzdGF0aWNcbiAqIEBtZW1iZXJPZiBfXG4gKiBAc2luY2UgMC4xLjBcbiAqIEBjYXRlZ29yeSBMYW5nXG4gKiBAcGFyYW0geyp9IHZhbHVlIFRoZSB2YWx1ZSB0byBjaGVjay5cbiAqIEByZXR1cm5zIHtib29sZWFufSBSZXR1cm5zIGB0cnVlYCBpZiBgdmFsdWVgIGlzIGFuIGBhcmd1bWVudHNgIG9iamVjdCxcbiAqICBlbHNlIGBmYWxzZWAuXG4gKiBAZXhhbXBsZVxuICpcbiAqIF8uaXNBcmd1bWVudHMoZnVuY3Rpb24oKSB7IHJldHVybiBhcmd1bWVudHM7IH0oKSk7XG4gKiAvLyA9PiB0cnVlXG4gKlxuICogXy5pc0FyZ3VtZW50cyhbMSwgMiwgM10pO1xuICogLy8gPT4gZmFsc2VcbiAqL1xudmFyIGlzQXJndW1lbnRzID0gYmFzZUlzQXJndW1lbnRzKGZ1bmN0aW9uKCkgeyByZXR1cm4gYXJndW1lbnRzOyB9KCkpID8gYmFzZUlzQXJndW1lbnRzIDogZnVuY3Rpb24odmFsdWUpIHtcbiAgcmV0dXJuIGlzT2JqZWN0TGlrZSh2YWx1ZSkgJiYgaGFzT3duUHJvcGVydHkuY2FsbCh2YWx1ZSwgJ2NhbGxlZScpICYmXG4gICAgIXByb3BlcnR5SXNFbnVtZXJhYmxlLmNhbGwodmFsdWUsICdjYWxsZWUnKTtcbn07XG5cbm1vZHVsZS5leHBvcnRzID0gaXNBcmd1bWVudHM7XG4iLCIvKipcbiAqIENoZWNrcyBpZiBgdmFsdWVgIGlzIGNsYXNzaWZpZWQgYXMgYW4gYEFycmF5YCBvYmplY3QuXG4gKlxuICogQHN0YXRpY1xuICogQG1lbWJlck9mIF9cbiAqIEBzaW5jZSAwLjEuMFxuICogQGNhdGVnb3J5IExhbmdcbiAqIEBwYXJhbSB7Kn0gdmFsdWUgVGhlIHZhbHVlIHRvIGNoZWNrLlxuICogQHJldHVybnMge2Jvb2xlYW59IFJldHVybnMgYHRydWVgIGlmIGB2YWx1ZWAgaXMgYW4gYXJyYXksIGVsc2UgYGZhbHNlYC5cbiAqIEBleGFtcGxlXG4gKlxuICogXy5pc0FycmF5KFsxLCAyLCAzXSk7XG4gKiAvLyA9PiB0cnVlXG4gKlxuICogXy5pc0FycmF5KGRvY3VtZW50LmJvZHkuY2hpbGRyZW4pO1xuICogLy8gPT4gZmFsc2VcbiAqXG4gKiBfLmlzQXJyYXkoJ2FiYycpO1xuICogLy8gPT4gZmFsc2VcbiAqXG4gKiBfLmlzQXJyYXkoXy5ub29wKTtcbiAqIC8vID0+IGZhbHNlXG4gKi9cbnZhciBpc0FycmF5ID0gQXJyYXkuaXNBcnJheTtcblxubW9kdWxlLmV4cG9ydHMgPSBpc0FycmF5O1xuIiwidmFyIGlzRnVuY3Rpb24gPSByZXF1aXJlKCcuL2lzRnVuY3Rpb24nKSxcbiAgICBpc0xlbmd0aCA9IHJlcXVpcmUoJy4vaXNMZW5ndGgnKTtcblxuLyoqXG4gKiBDaGVja3MgaWYgYHZhbHVlYCBpcyBhcnJheS1saWtlLiBBIHZhbHVlIGlzIGNvbnNpZGVyZWQgYXJyYXktbGlrZSBpZiBpdCdzXG4gKiBub3QgYSBmdW5jdGlvbiBhbmQgaGFzIGEgYHZhbHVlLmxlbmd0aGAgdGhhdCdzIGFuIGludGVnZXIgZ3JlYXRlciB0aGFuIG9yXG4gKiBlcXVhbCB0byBgMGAgYW5kIGxlc3MgdGhhbiBvciBlcXVhbCB0byBgTnVtYmVyLk1BWF9TQUZFX0lOVEVHRVJgLlxuICpcbiAqIEBzdGF0aWNcbiAqIEBtZW1iZXJPZiBfXG4gKiBAc2luY2UgNC4wLjBcbiAqIEBjYXRlZ29yeSBMYW5nXG4gKiBAcGFyYW0geyp9IHZhbHVlIFRoZSB2YWx1ZSB0byBjaGVjay5cbiAqIEByZXR1cm5zIHtib29sZWFufSBSZXR1cm5zIGB0cnVlYCBpZiBgdmFsdWVgIGlzIGFycmF5LWxpa2UsIGVsc2UgYGZhbHNlYC5cbiAqIEBleGFtcGxlXG4gKlxuICogXy5pc0FycmF5TGlrZShbMSwgMiwgM10pO1xuICogLy8gPT4gdHJ1ZVxuICpcbiAqIF8uaXNBcnJheUxpa2UoZG9jdW1lbnQuYm9keS5jaGlsZHJlbik7XG4gKiAvLyA9PiB0cnVlXG4gKlxuICogXy5pc0FycmF5TGlrZSgnYWJjJyk7XG4gKiAvLyA9PiB0cnVlXG4gKlxuICogXy5pc0FycmF5TGlrZShfLm5vb3ApO1xuICogLy8gPT4gZmFsc2VcbiAqL1xuZnVuY3Rpb24gaXNBcnJheUxpa2UodmFsdWUpIHtcbiAgcmV0dXJuIHZhbHVlICE9IG51bGwgJiYgaXNMZW5ndGgodmFsdWUubGVuZ3RoKSAmJiAhaXNGdW5jdGlvbih2YWx1ZSk7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gaXNBcnJheUxpa2U7XG4iLCJ2YXIgcm9vdCA9IHJlcXVpcmUoJy4vX3Jvb3QnKSxcbiAgICBzdHViRmFsc2UgPSByZXF1aXJlKCcuL3N0dWJGYWxzZScpO1xuXG4vKiogRGV0ZWN0IGZyZWUgdmFyaWFibGUgYGV4cG9ydHNgLiAqL1xudmFyIGZyZWVFeHBvcnRzID0gdHlwZW9mIGV4cG9ydHMgPT0gJ29iamVjdCcgJiYgZXhwb3J0cyAmJiAhZXhwb3J0cy5ub2RlVHlwZSAmJiBleHBvcnRzO1xuXG4vKiogRGV0ZWN0IGZyZWUgdmFyaWFibGUgYG1vZHVsZWAuICovXG52YXIgZnJlZU1vZHVsZSA9IGZyZWVFeHBvcnRzICYmIHR5cGVvZiBtb2R1bGUgPT0gJ29iamVjdCcgJiYgbW9kdWxlICYmICFtb2R1bGUubm9kZVR5cGUgJiYgbW9kdWxlO1xuXG4vKiogRGV0ZWN0IHRoZSBwb3B1bGFyIENvbW1vbkpTIGV4dGVuc2lvbiBgbW9kdWxlLmV4cG9ydHNgLiAqL1xudmFyIG1vZHVsZUV4cG9ydHMgPSBmcmVlTW9kdWxlICYmIGZyZWVNb2R1bGUuZXhwb3J0cyA9PT0gZnJlZUV4cG9ydHM7XG5cbi8qKiBCdWlsdC1pbiB2YWx1ZSByZWZlcmVuY2VzLiAqL1xudmFyIEJ1ZmZlciA9IG1vZHVsZUV4cG9ydHMgPyByb290LkJ1ZmZlciA6IHVuZGVmaW5lZDtcblxuLyogQnVpbHQtaW4gbWV0aG9kIHJlZmVyZW5jZXMgZm9yIHRob3NlIHdpdGggdGhlIHNhbWUgbmFtZSBhcyBvdGhlciBgbG9kYXNoYCBtZXRob2RzLiAqL1xudmFyIG5hdGl2ZUlzQnVmZmVyID0gQnVmZmVyID8gQnVmZmVyLmlzQnVmZmVyIDogdW5kZWZpbmVkO1xuXG4vKipcbiAqIENoZWNrcyBpZiBgdmFsdWVgIGlzIGEgYnVmZmVyLlxuICpcbiAqIEBzdGF0aWNcbiAqIEBtZW1iZXJPZiBfXG4gKiBAc2luY2UgNC4zLjBcbiAqIEBjYXRlZ29yeSBMYW5nXG4gKiBAcGFyYW0geyp9IHZhbHVlIFRoZSB2YWx1ZSB0byBjaGVjay5cbiAqIEByZXR1cm5zIHtib29sZWFufSBSZXR1cm5zIGB0cnVlYCBpZiBgdmFsdWVgIGlzIGEgYnVmZmVyLCBlbHNlIGBmYWxzZWAuXG4gKiBAZXhhbXBsZVxuICpcbiAqIF8uaXNCdWZmZXIobmV3IEJ1ZmZlcigyKSk7XG4gKiAvLyA9PiB0cnVlXG4gKlxuICogXy5pc0J1ZmZlcihuZXcgVWludDhBcnJheSgyKSk7XG4gKiAvLyA9PiBmYWxzZVxuICovXG52YXIgaXNCdWZmZXIgPSBuYXRpdmVJc0J1ZmZlciB8fCBzdHViRmFsc2U7XG5cbm1vZHVsZS5leHBvcnRzID0gaXNCdWZmZXI7XG4iLCJ2YXIgYmFzZUdldFRhZyA9IHJlcXVpcmUoJy4vX2Jhc2VHZXRUYWcnKSxcbiAgICBpc09iamVjdCA9IHJlcXVpcmUoJy4vaXNPYmplY3QnKTtcblxuLyoqIGBPYmplY3QjdG9TdHJpbmdgIHJlc3VsdCByZWZlcmVuY2VzLiAqL1xudmFyIGFzeW5jVGFnID0gJ1tvYmplY3QgQXN5bmNGdW5jdGlvbl0nLFxuICAgIGZ1bmNUYWcgPSAnW29iamVjdCBGdW5jdGlvbl0nLFxuICAgIGdlblRhZyA9ICdbb2JqZWN0IEdlbmVyYXRvckZ1bmN0aW9uXScsXG4gICAgcHJveHlUYWcgPSAnW29iamVjdCBQcm94eV0nO1xuXG4vKipcbiAqIENoZWNrcyBpZiBgdmFsdWVgIGlzIGNsYXNzaWZpZWQgYXMgYSBgRnVuY3Rpb25gIG9iamVjdC5cbiAqXG4gKiBAc3RhdGljXG4gKiBAbWVtYmVyT2YgX1xuICogQHNpbmNlIDAuMS4wXG4gKiBAY2F0ZWdvcnkgTGFuZ1xuICogQHBhcmFtIHsqfSB2YWx1ZSBUaGUgdmFsdWUgdG8gY2hlY2suXG4gKiBAcmV0dXJucyB7Ym9vbGVhbn0gUmV0dXJucyBgdHJ1ZWAgaWYgYHZhbHVlYCBpcyBhIGZ1bmN0aW9uLCBlbHNlIGBmYWxzZWAuXG4gKiBAZXhhbXBsZVxuICpcbiAqIF8uaXNGdW5jdGlvbihfKTtcbiAqIC8vID0+IHRydWVcbiAqXG4gKiBfLmlzRnVuY3Rpb24oL2FiYy8pO1xuICogLy8gPT4gZmFsc2VcbiAqL1xuZnVuY3Rpb24gaXNGdW5jdGlvbih2YWx1ZSkge1xuICBpZiAoIWlzT2JqZWN0KHZhbHVlKSkge1xuICAgIHJldHVybiBmYWxzZTtcbiAgfVxuICAvLyBUaGUgdXNlIG9mIGBPYmplY3QjdG9TdHJpbmdgIGF2b2lkcyBpc3N1ZXMgd2l0aCB0aGUgYHR5cGVvZmAgb3BlcmF0b3JcbiAgLy8gaW4gU2FmYXJpIDkgd2hpY2ggcmV0dXJucyAnb2JqZWN0JyBmb3IgdHlwZWQgYXJyYXlzIGFuZCBvdGhlciBjb25zdHJ1Y3RvcnMuXG4gIHZhciB0YWcgPSBiYXNlR2V0VGFnKHZhbHVlKTtcbiAgcmV0dXJuIHRhZyA9PSBmdW5jVGFnIHx8IHRhZyA9PSBnZW5UYWcgfHwgdGFnID09IGFzeW5jVGFnIHx8IHRhZyA9PSBwcm94eVRhZztcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBpc0Z1bmN0aW9uO1xuIiwiLyoqIFVzZWQgYXMgcmVmZXJlbmNlcyBmb3IgdmFyaW91cyBgTnVtYmVyYCBjb25zdGFudHMuICovXG52YXIgTUFYX1NBRkVfSU5URUdFUiA9IDkwMDcxOTkyNTQ3NDA5OTE7XG5cbi8qKlxuICogQ2hlY2tzIGlmIGB2YWx1ZWAgaXMgYSB2YWxpZCBhcnJheS1saWtlIGxlbmd0aC5cbiAqXG4gKiAqKk5vdGU6KiogVGhpcyBtZXRob2QgaXMgbG9vc2VseSBiYXNlZCBvblxuICogW2BUb0xlbmd0aGBdKGh0dHA6Ly9lY21hLWludGVybmF0aW9uYWwub3JnL2VjbWEtMjYyLzcuMC8jc2VjLXRvbGVuZ3RoKS5cbiAqXG4gKiBAc3RhdGljXG4gKiBAbWVtYmVyT2YgX1xuICogQHNpbmNlIDQuMC4wXG4gKiBAY2F0ZWdvcnkgTGFuZ1xuICogQHBhcmFtIHsqfSB2YWx1ZSBUaGUgdmFsdWUgdG8gY2hlY2suXG4gKiBAcmV0dXJucyB7Ym9vbGVhbn0gUmV0dXJucyBgdHJ1ZWAgaWYgYHZhbHVlYCBpcyBhIHZhbGlkIGxlbmd0aCwgZWxzZSBgZmFsc2VgLlxuICogQGV4YW1wbGVcbiAqXG4gKiBfLmlzTGVuZ3RoKDMpO1xuICogLy8gPT4gdHJ1ZVxuICpcbiAqIF8uaXNMZW5ndGgoTnVtYmVyLk1JTl9WQUxVRSk7XG4gKiAvLyA9PiBmYWxzZVxuICpcbiAqIF8uaXNMZW5ndGgoSW5maW5pdHkpO1xuICogLy8gPT4gZmFsc2VcbiAqXG4gKiBfLmlzTGVuZ3RoKCczJyk7XG4gKiAvLyA9PiBmYWxzZVxuICovXG5mdW5jdGlvbiBpc0xlbmd0aCh2YWx1ZSkge1xuICByZXR1cm4gdHlwZW9mIHZhbHVlID09ICdudW1iZXInICYmXG4gICAgdmFsdWUgPiAtMSAmJiB2YWx1ZSAlIDEgPT0gMCAmJiB2YWx1ZSA8PSBNQVhfU0FGRV9JTlRFR0VSO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IGlzTGVuZ3RoO1xuIiwiLyoqXG4gKiBDaGVja3MgaWYgYHZhbHVlYCBpcyB0aGVcbiAqIFtsYW5ndWFnZSB0eXBlXShodHRwOi8vd3d3LmVjbWEtaW50ZXJuYXRpb25hbC5vcmcvZWNtYS0yNjIvNy4wLyNzZWMtZWNtYXNjcmlwdC1sYW5ndWFnZS10eXBlcylcbiAqIG9mIGBPYmplY3RgLiAoZS5nLiBhcnJheXMsIGZ1bmN0aW9ucywgb2JqZWN0cywgcmVnZXhlcywgYG5ldyBOdW1iZXIoMClgLCBhbmQgYG5ldyBTdHJpbmcoJycpYClcbiAqXG4gKiBAc3RhdGljXG4gKiBAbWVtYmVyT2YgX1xuICogQHNpbmNlIDAuMS4wXG4gKiBAY2F0ZWdvcnkgTGFuZ1xuICogQHBhcmFtIHsqfSB2YWx1ZSBUaGUgdmFsdWUgdG8gY2hlY2suXG4gKiBAcmV0dXJucyB7Ym9vbGVhbn0gUmV0dXJucyBgdHJ1ZWAgaWYgYHZhbHVlYCBpcyBhbiBvYmplY3QsIGVsc2UgYGZhbHNlYC5cbiAqIEBleGFtcGxlXG4gKlxuICogXy5pc09iamVjdCh7fSk7XG4gKiAvLyA9PiB0cnVlXG4gKlxuICogXy5pc09iamVjdChbMSwgMiwgM10pO1xuICogLy8gPT4gdHJ1ZVxuICpcbiAqIF8uaXNPYmplY3QoXy5ub29wKTtcbiAqIC8vID0+IHRydWVcbiAqXG4gKiBfLmlzT2JqZWN0KG51bGwpO1xuICogLy8gPT4gZmFsc2VcbiAqL1xuZnVuY3Rpb24gaXNPYmplY3QodmFsdWUpIHtcbiAgdmFyIHR5cGUgPSB0eXBlb2YgdmFsdWU7XG4gIHJldHVybiB2YWx1ZSAhPSBudWxsICYmICh0eXBlID09ICdvYmplY3QnIHx8IHR5cGUgPT0gJ2Z1bmN0aW9uJyk7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gaXNPYmplY3Q7XG4iLCIvKipcbiAqIENoZWNrcyBpZiBgdmFsdWVgIGlzIG9iamVjdC1saWtlLiBBIHZhbHVlIGlzIG9iamVjdC1saWtlIGlmIGl0J3Mgbm90IGBudWxsYFxuICogYW5kIGhhcyBhIGB0eXBlb2ZgIHJlc3VsdCBvZiBcIm9iamVjdFwiLlxuICpcbiAqIEBzdGF0aWNcbiAqIEBtZW1iZXJPZiBfXG4gKiBAc2luY2UgNC4wLjBcbiAqIEBjYXRlZ29yeSBMYW5nXG4gKiBAcGFyYW0geyp9IHZhbHVlIFRoZSB2YWx1ZSB0byBjaGVjay5cbiAqIEByZXR1cm5zIHtib29sZWFufSBSZXR1cm5zIGB0cnVlYCBpZiBgdmFsdWVgIGlzIG9iamVjdC1saWtlLCBlbHNlIGBmYWxzZWAuXG4gKiBAZXhhbXBsZVxuICpcbiAqIF8uaXNPYmplY3RMaWtlKHt9KTtcbiAqIC8vID0+IHRydWVcbiAqXG4gKiBfLmlzT2JqZWN0TGlrZShbMSwgMiwgM10pO1xuICogLy8gPT4gdHJ1ZVxuICpcbiAqIF8uaXNPYmplY3RMaWtlKF8ubm9vcCk7XG4gKiAvLyA9PiBmYWxzZVxuICpcbiAqIF8uaXNPYmplY3RMaWtlKG51bGwpO1xuICogLy8gPT4gZmFsc2VcbiAqL1xuZnVuY3Rpb24gaXNPYmplY3RMaWtlKHZhbHVlKSB7XG4gIHJldHVybiB2YWx1ZSAhPSBudWxsICYmIHR5cGVvZiB2YWx1ZSA9PSAnb2JqZWN0Jztcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBpc09iamVjdExpa2U7XG4iLCJ2YXIgYmFzZUdldFRhZyA9IHJlcXVpcmUoJy4vX2Jhc2VHZXRUYWcnKSxcbiAgICBpc09iamVjdExpa2UgPSByZXF1aXJlKCcuL2lzT2JqZWN0TGlrZScpO1xuXG4vKiogYE9iamVjdCN0b1N0cmluZ2AgcmVzdWx0IHJlZmVyZW5jZXMuICovXG52YXIgc3ltYm9sVGFnID0gJ1tvYmplY3QgU3ltYm9sXSc7XG5cbi8qKlxuICogQ2hlY2tzIGlmIGB2YWx1ZWAgaXMgY2xhc3NpZmllZCBhcyBhIGBTeW1ib2xgIHByaW1pdGl2ZSBvciBvYmplY3QuXG4gKlxuICogQHN0YXRpY1xuICogQG1lbWJlck9mIF9cbiAqIEBzaW5jZSA0LjAuMFxuICogQGNhdGVnb3J5IExhbmdcbiAqIEBwYXJhbSB7Kn0gdmFsdWUgVGhlIHZhbHVlIHRvIGNoZWNrLlxuICogQHJldHVybnMge2Jvb2xlYW59IFJldHVybnMgYHRydWVgIGlmIGB2YWx1ZWAgaXMgYSBzeW1ib2wsIGVsc2UgYGZhbHNlYC5cbiAqIEBleGFtcGxlXG4gKlxuICogXy5pc1N5bWJvbChTeW1ib2wuaXRlcmF0b3IpO1xuICogLy8gPT4gdHJ1ZVxuICpcbiAqIF8uaXNTeW1ib2woJ2FiYycpO1xuICogLy8gPT4gZmFsc2VcbiAqL1xuZnVuY3Rpb24gaXNTeW1ib2wodmFsdWUpIHtcbiAgcmV0dXJuIHR5cGVvZiB2YWx1ZSA9PSAnc3ltYm9sJyB8fFxuICAgIChpc09iamVjdExpa2UodmFsdWUpICYmIGJhc2VHZXRUYWcodmFsdWUpID09IHN5bWJvbFRhZyk7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gaXNTeW1ib2w7XG4iLCJ2YXIgYmFzZUlzVHlwZWRBcnJheSA9IHJlcXVpcmUoJy4vX2Jhc2VJc1R5cGVkQXJyYXknKSxcbiAgICBiYXNlVW5hcnkgPSByZXF1aXJlKCcuL19iYXNlVW5hcnknKSxcbiAgICBub2RlVXRpbCA9IHJlcXVpcmUoJy4vX25vZGVVdGlsJyk7XG5cbi8qIE5vZGUuanMgaGVscGVyIHJlZmVyZW5jZXMuICovXG52YXIgbm9kZUlzVHlwZWRBcnJheSA9IG5vZGVVdGlsICYmIG5vZGVVdGlsLmlzVHlwZWRBcnJheTtcblxuLyoqXG4gKiBDaGVja3MgaWYgYHZhbHVlYCBpcyBjbGFzc2lmaWVkIGFzIGEgdHlwZWQgYXJyYXkuXG4gKlxuICogQHN0YXRpY1xuICogQG1lbWJlck9mIF9cbiAqIEBzaW5jZSAzLjAuMFxuICogQGNhdGVnb3J5IExhbmdcbiAqIEBwYXJhbSB7Kn0gdmFsdWUgVGhlIHZhbHVlIHRvIGNoZWNrLlxuICogQHJldHVybnMge2Jvb2xlYW59IFJldHVybnMgYHRydWVgIGlmIGB2YWx1ZWAgaXMgYSB0eXBlZCBhcnJheSwgZWxzZSBgZmFsc2VgLlxuICogQGV4YW1wbGVcbiAqXG4gKiBfLmlzVHlwZWRBcnJheShuZXcgVWludDhBcnJheSk7XG4gKiAvLyA9PiB0cnVlXG4gKlxuICogXy5pc1R5cGVkQXJyYXkoW10pO1xuICogLy8gPT4gZmFsc2VcbiAqL1xudmFyIGlzVHlwZWRBcnJheSA9IG5vZGVJc1R5cGVkQXJyYXkgPyBiYXNlVW5hcnkobm9kZUlzVHlwZWRBcnJheSkgOiBiYXNlSXNUeXBlZEFycmF5O1xuXG5tb2R1bGUuZXhwb3J0cyA9IGlzVHlwZWRBcnJheTtcbiIsInZhciBhcnJheUxpa2VLZXlzID0gcmVxdWlyZSgnLi9fYXJyYXlMaWtlS2V5cycpLFxuICAgIGJhc2VLZXlzID0gcmVxdWlyZSgnLi9fYmFzZUtleXMnKSxcbiAgICBpc0FycmF5TGlrZSA9IHJlcXVpcmUoJy4vaXNBcnJheUxpa2UnKTtcblxuLyoqXG4gKiBDcmVhdGVzIGFuIGFycmF5IG9mIHRoZSBvd24gZW51bWVyYWJsZSBwcm9wZXJ0eSBuYW1lcyBvZiBgb2JqZWN0YC5cbiAqXG4gKiAqKk5vdGU6KiogTm9uLW9iamVjdCB2YWx1ZXMgYXJlIGNvZXJjZWQgdG8gb2JqZWN0cy4gU2VlIHRoZVxuICogW0VTIHNwZWNdKGh0dHA6Ly9lY21hLWludGVybmF0aW9uYWwub3JnL2VjbWEtMjYyLzcuMC8jc2VjLW9iamVjdC5rZXlzKVxuICogZm9yIG1vcmUgZGV0YWlscy5cbiAqXG4gKiBAc3RhdGljXG4gKiBAc2luY2UgMC4xLjBcbiAqIEBtZW1iZXJPZiBfXG4gKiBAY2F0ZWdvcnkgT2JqZWN0XG4gKiBAcGFyYW0ge09iamVjdH0gb2JqZWN0IFRoZSBvYmplY3QgdG8gcXVlcnkuXG4gKiBAcmV0dXJucyB7QXJyYXl9IFJldHVybnMgdGhlIGFycmF5IG9mIHByb3BlcnR5IG5hbWVzLlxuICogQGV4YW1wbGVcbiAqXG4gKiBmdW5jdGlvbiBGb28oKSB7XG4gKiAgIHRoaXMuYSA9IDE7XG4gKiAgIHRoaXMuYiA9IDI7XG4gKiB9XG4gKlxuICogRm9vLnByb3RvdHlwZS5jID0gMztcbiAqXG4gKiBfLmtleXMobmV3IEZvbyk7XG4gKiAvLyA9PiBbJ2EnLCAnYiddIChpdGVyYXRpb24gb3JkZXIgaXMgbm90IGd1YXJhbnRlZWQpXG4gKlxuICogXy5rZXlzKCdoaScpO1xuICogLy8gPT4gWycwJywgJzEnXVxuICovXG5mdW5jdGlvbiBrZXlzKG9iamVjdCkge1xuICByZXR1cm4gaXNBcnJheUxpa2Uob2JqZWN0KSA/IGFycmF5TGlrZUtleXMob2JqZWN0KSA6IGJhc2VLZXlzKG9iamVjdCk7XG59XG5cbm1vZHVsZS5leHBvcnRzID0ga2V5cztcbiIsInZhciBNYXBDYWNoZSA9IHJlcXVpcmUoJy4vX01hcENhY2hlJyk7XG5cbi8qKiBFcnJvciBtZXNzYWdlIGNvbnN0YW50cy4gKi9cbnZhciBGVU5DX0VSUk9SX1RFWFQgPSAnRXhwZWN0ZWQgYSBmdW5jdGlvbic7XG5cbi8qKlxuICogQ3JlYXRlcyBhIGZ1bmN0aW9uIHRoYXQgbWVtb2l6ZXMgdGhlIHJlc3VsdCBvZiBgZnVuY2AuIElmIGByZXNvbHZlcmAgaXNcbiAqIHByb3ZpZGVkLCBpdCBkZXRlcm1pbmVzIHRoZSBjYWNoZSBrZXkgZm9yIHN0b3JpbmcgdGhlIHJlc3VsdCBiYXNlZCBvbiB0aGVcbiAqIGFyZ3VtZW50cyBwcm92aWRlZCB0byB0aGUgbWVtb2l6ZWQgZnVuY3Rpb24uIEJ5IGRlZmF1bHQsIHRoZSBmaXJzdCBhcmd1bWVudFxuICogcHJvdmlkZWQgdG8gdGhlIG1lbW9pemVkIGZ1bmN0aW9uIGlzIHVzZWQgYXMgdGhlIG1hcCBjYWNoZSBrZXkuIFRoZSBgZnVuY2BcbiAqIGlzIGludm9rZWQgd2l0aCB0aGUgYHRoaXNgIGJpbmRpbmcgb2YgdGhlIG1lbW9pemVkIGZ1bmN0aW9uLlxuICpcbiAqICoqTm90ZToqKiBUaGUgY2FjaGUgaXMgZXhwb3NlZCBhcyB0aGUgYGNhY2hlYCBwcm9wZXJ0eSBvbiB0aGUgbWVtb2l6ZWRcbiAqIGZ1bmN0aW9uLiBJdHMgY3JlYXRpb24gbWF5IGJlIGN1c3RvbWl6ZWQgYnkgcmVwbGFjaW5nIHRoZSBgXy5tZW1vaXplLkNhY2hlYFxuICogY29uc3RydWN0b3Igd2l0aCBvbmUgd2hvc2UgaW5zdGFuY2VzIGltcGxlbWVudCB0aGVcbiAqIFtgTWFwYF0oaHR0cDovL2VjbWEtaW50ZXJuYXRpb25hbC5vcmcvZWNtYS0yNjIvNy4wLyNzZWMtcHJvcGVydGllcy1vZi10aGUtbWFwLXByb3RvdHlwZS1vYmplY3QpXG4gKiBtZXRob2QgaW50ZXJmYWNlIG9mIGBjbGVhcmAsIGBkZWxldGVgLCBgZ2V0YCwgYGhhc2AsIGFuZCBgc2V0YC5cbiAqXG4gKiBAc3RhdGljXG4gKiBAbWVtYmVyT2YgX1xuICogQHNpbmNlIDAuMS4wXG4gKiBAY2F0ZWdvcnkgRnVuY3Rpb25cbiAqIEBwYXJhbSB7RnVuY3Rpb259IGZ1bmMgVGhlIGZ1bmN0aW9uIHRvIGhhdmUgaXRzIG91dHB1dCBtZW1vaXplZC5cbiAqIEBwYXJhbSB7RnVuY3Rpb259IFtyZXNvbHZlcl0gVGhlIGZ1bmN0aW9uIHRvIHJlc29sdmUgdGhlIGNhY2hlIGtleS5cbiAqIEByZXR1cm5zIHtGdW5jdGlvbn0gUmV0dXJucyB0aGUgbmV3IG1lbW9pemVkIGZ1bmN0aW9uLlxuICogQGV4YW1wbGVcbiAqXG4gKiB2YXIgb2JqZWN0ID0geyAnYSc6IDEsICdiJzogMiB9O1xuICogdmFyIG90aGVyID0geyAnYyc6IDMsICdkJzogNCB9O1xuICpcbiAqIHZhciB2YWx1ZXMgPSBfLm1lbW9pemUoXy52YWx1ZXMpO1xuICogdmFsdWVzKG9iamVjdCk7XG4gKiAvLyA9PiBbMSwgMl1cbiAqXG4gKiB2YWx1ZXMob3RoZXIpO1xuICogLy8gPT4gWzMsIDRdXG4gKlxuICogb2JqZWN0LmEgPSAyO1xuICogdmFsdWVzKG9iamVjdCk7XG4gKiAvLyA9PiBbMSwgMl1cbiAqXG4gKiAvLyBNb2RpZnkgdGhlIHJlc3VsdCBjYWNoZS5cbiAqIHZhbHVlcy5jYWNoZS5zZXQob2JqZWN0LCBbJ2EnLCAnYiddKTtcbiAqIHZhbHVlcyhvYmplY3QpO1xuICogLy8gPT4gWydhJywgJ2InXVxuICpcbiAqIC8vIFJlcGxhY2UgYF8ubWVtb2l6ZS5DYWNoZWAuXG4gKiBfLm1lbW9pemUuQ2FjaGUgPSBXZWFrTWFwO1xuICovXG5mdW5jdGlvbiBtZW1vaXplKGZ1bmMsIHJlc29sdmVyKSB7XG4gIGlmICh0eXBlb2YgZnVuYyAhPSAnZnVuY3Rpb24nIHx8IChyZXNvbHZlciAhPSBudWxsICYmIHR5cGVvZiByZXNvbHZlciAhPSAnZnVuY3Rpb24nKSkge1xuICAgIHRocm93IG5ldyBUeXBlRXJyb3IoRlVOQ19FUlJPUl9URVhUKTtcbiAgfVxuICB2YXIgbWVtb2l6ZWQgPSBmdW5jdGlvbigpIHtcbiAgICB2YXIgYXJncyA9IGFyZ3VtZW50cyxcbiAgICAgICAga2V5ID0gcmVzb2x2ZXIgPyByZXNvbHZlci5hcHBseSh0aGlzLCBhcmdzKSA6IGFyZ3NbMF0sXG4gICAgICAgIGNhY2hlID0gbWVtb2l6ZWQuY2FjaGU7XG5cbiAgICBpZiAoY2FjaGUuaGFzKGtleSkpIHtcbiAgICAgIHJldHVybiBjYWNoZS5nZXQoa2V5KTtcbiAgICB9XG4gICAgdmFyIHJlc3VsdCA9IGZ1bmMuYXBwbHkodGhpcywgYXJncyk7XG4gICAgbWVtb2l6ZWQuY2FjaGUgPSBjYWNoZS5zZXQoa2V5LCByZXN1bHQpIHx8IGNhY2hlO1xuICAgIHJldHVybiByZXN1bHQ7XG4gIH07XG4gIG1lbW9pemVkLmNhY2hlID0gbmV3IChtZW1vaXplLkNhY2hlIHx8IE1hcENhY2hlKTtcbiAgcmV0dXJuIG1lbW9pemVkO1xufVxuXG4vLyBFeHBvc2UgYE1hcENhY2hlYC5cbm1lbW9pemUuQ2FjaGUgPSBNYXBDYWNoZTtcblxubW9kdWxlLmV4cG9ydHMgPSBtZW1vaXplO1xuIiwidmFyIGJhc2VQcm9wZXJ0eSA9IHJlcXVpcmUoJy4vX2Jhc2VQcm9wZXJ0eScpLFxuICAgIGJhc2VQcm9wZXJ0eURlZXAgPSByZXF1aXJlKCcuL19iYXNlUHJvcGVydHlEZWVwJyksXG4gICAgaXNLZXkgPSByZXF1aXJlKCcuL19pc0tleScpLFxuICAgIHRvS2V5ID0gcmVxdWlyZSgnLi9fdG9LZXknKTtcblxuLyoqXG4gKiBDcmVhdGVzIGEgZnVuY3Rpb24gdGhhdCByZXR1cm5zIHRoZSB2YWx1ZSBhdCBgcGF0aGAgb2YgYSBnaXZlbiBvYmplY3QuXG4gKlxuICogQHN0YXRpY1xuICogQG1lbWJlck9mIF9cbiAqIEBzaW5jZSAyLjQuMFxuICogQGNhdGVnb3J5IFV0aWxcbiAqIEBwYXJhbSB7QXJyYXl8c3RyaW5nfSBwYXRoIFRoZSBwYXRoIG9mIHRoZSBwcm9wZXJ0eSB0byBnZXQuXG4gKiBAcmV0dXJucyB7RnVuY3Rpb259IFJldHVybnMgdGhlIG5ldyBhY2Nlc3NvciBmdW5jdGlvbi5cbiAqIEBleGFtcGxlXG4gKlxuICogdmFyIG9iamVjdHMgPSBbXG4gKiAgIHsgJ2EnOiB7ICdiJzogMiB9IH0sXG4gKiAgIHsgJ2EnOiB7ICdiJzogMSB9IH1cbiAqIF07XG4gKlxuICogXy5tYXAob2JqZWN0cywgXy5wcm9wZXJ0eSgnYS5iJykpO1xuICogLy8gPT4gWzIsIDFdXG4gKlxuICogXy5tYXAoXy5zb3J0Qnkob2JqZWN0cywgXy5wcm9wZXJ0eShbJ2EnLCAnYiddKSksICdhLmInKTtcbiAqIC8vID0+IFsxLCAyXVxuICovXG5mdW5jdGlvbiBwcm9wZXJ0eShwYXRoKSB7XG4gIHJldHVybiBpc0tleShwYXRoKSA/IGJhc2VQcm9wZXJ0eSh0b0tleShwYXRoKSkgOiBiYXNlUHJvcGVydHlEZWVwKHBhdGgpO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IHByb3BlcnR5O1xuIiwiLyoqXG4gKiBUaGlzIG1ldGhvZCByZXR1cm5zIGEgbmV3IGVtcHR5IGFycmF5LlxuICpcbiAqIEBzdGF0aWNcbiAqIEBtZW1iZXJPZiBfXG4gKiBAc2luY2UgNC4xMy4wXG4gKiBAY2F0ZWdvcnkgVXRpbFxuICogQHJldHVybnMge0FycmF5fSBSZXR1cm5zIHRoZSBuZXcgZW1wdHkgYXJyYXkuXG4gKiBAZXhhbXBsZVxuICpcbiAqIHZhciBhcnJheXMgPSBfLnRpbWVzKDIsIF8uc3R1YkFycmF5KTtcbiAqXG4gKiBjb25zb2xlLmxvZyhhcnJheXMpO1xuICogLy8gPT4gW1tdLCBbXV1cbiAqXG4gKiBjb25zb2xlLmxvZyhhcnJheXNbMF0gPT09IGFycmF5c1sxXSk7XG4gKiAvLyA9PiBmYWxzZVxuICovXG5mdW5jdGlvbiBzdHViQXJyYXkoKSB7XG4gIHJldHVybiBbXTtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBzdHViQXJyYXk7XG4iLCIvKipcbiAqIFRoaXMgbWV0aG9kIHJldHVybnMgYGZhbHNlYC5cbiAqXG4gKiBAc3RhdGljXG4gKiBAbWVtYmVyT2YgX1xuICogQHNpbmNlIDQuMTMuMFxuICogQGNhdGVnb3J5IFV0aWxcbiAqIEByZXR1cm5zIHtib29sZWFufSBSZXR1cm5zIGBmYWxzZWAuXG4gKiBAZXhhbXBsZVxuICpcbiAqIF8udGltZXMoMiwgXy5zdHViRmFsc2UpO1xuICogLy8gPT4gW2ZhbHNlLCBmYWxzZV1cbiAqL1xuZnVuY3Rpb24gc3R1YkZhbHNlKCkge1xuICByZXR1cm4gZmFsc2U7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gc3R1YkZhbHNlO1xuIiwidmFyIHRvTnVtYmVyID0gcmVxdWlyZSgnLi90b051bWJlcicpO1xuXG4vKiogVXNlZCBhcyByZWZlcmVuY2VzIGZvciB2YXJpb3VzIGBOdW1iZXJgIGNvbnN0YW50cy4gKi9cbnZhciBJTkZJTklUWSA9IDEgLyAwLFxuICAgIE1BWF9JTlRFR0VSID0gMS43OTc2OTMxMzQ4NjIzMTU3ZSszMDg7XG5cbi8qKlxuICogQ29udmVydHMgYHZhbHVlYCB0byBhIGZpbml0ZSBudW1iZXIuXG4gKlxuICogQHN0YXRpY1xuICogQG1lbWJlck9mIF9cbiAqIEBzaW5jZSA0LjEyLjBcbiAqIEBjYXRlZ29yeSBMYW5nXG4gKiBAcGFyYW0geyp9IHZhbHVlIFRoZSB2YWx1ZSB0byBjb252ZXJ0LlxuICogQHJldHVybnMge251bWJlcn0gUmV0dXJucyB0aGUgY29udmVydGVkIG51bWJlci5cbiAqIEBleGFtcGxlXG4gKlxuICogXy50b0Zpbml0ZSgzLjIpO1xuICogLy8gPT4gMy4yXG4gKlxuICogXy50b0Zpbml0ZShOdW1iZXIuTUlOX1ZBTFVFKTtcbiAqIC8vID0+IDVlLTMyNFxuICpcbiAqIF8udG9GaW5pdGUoSW5maW5pdHkpO1xuICogLy8gPT4gMS43OTc2OTMxMzQ4NjIzMTU3ZSszMDhcbiAqXG4gKiBfLnRvRmluaXRlKCczLjInKTtcbiAqIC8vID0+IDMuMlxuICovXG5mdW5jdGlvbiB0b0Zpbml0ZSh2YWx1ZSkge1xuICBpZiAoIXZhbHVlKSB7XG4gICAgcmV0dXJuIHZhbHVlID09PSAwID8gdmFsdWUgOiAwO1xuICB9XG4gIHZhbHVlID0gdG9OdW1iZXIodmFsdWUpO1xuICBpZiAodmFsdWUgPT09IElORklOSVRZIHx8IHZhbHVlID09PSAtSU5GSU5JVFkpIHtcbiAgICB2YXIgc2lnbiA9ICh2YWx1ZSA8IDAgPyAtMSA6IDEpO1xuICAgIHJldHVybiBzaWduICogTUFYX0lOVEVHRVI7XG4gIH1cbiAgcmV0dXJuIHZhbHVlID09PSB2YWx1ZSA/IHZhbHVlIDogMDtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSB0b0Zpbml0ZTtcbiIsInZhciB0b0Zpbml0ZSA9IHJlcXVpcmUoJy4vdG9GaW5pdGUnKTtcblxuLyoqXG4gKiBDb252ZXJ0cyBgdmFsdWVgIHRvIGFuIGludGVnZXIuXG4gKlxuICogKipOb3RlOioqIFRoaXMgbWV0aG9kIGlzIGxvb3NlbHkgYmFzZWQgb25cbiAqIFtgVG9JbnRlZ2VyYF0oaHR0cDovL3d3dy5lY21hLWludGVybmF0aW9uYWwub3JnL2VjbWEtMjYyLzcuMC8jc2VjLXRvaW50ZWdlcikuXG4gKlxuICogQHN0YXRpY1xuICogQG1lbWJlck9mIF9cbiAqIEBzaW5jZSA0LjAuMFxuICogQGNhdGVnb3J5IExhbmdcbiAqIEBwYXJhbSB7Kn0gdmFsdWUgVGhlIHZhbHVlIHRvIGNvbnZlcnQuXG4gKiBAcmV0dXJucyB7bnVtYmVyfSBSZXR1cm5zIHRoZSBjb252ZXJ0ZWQgaW50ZWdlci5cbiAqIEBleGFtcGxlXG4gKlxuICogXy50b0ludGVnZXIoMy4yKTtcbiAqIC8vID0+IDNcbiAqXG4gKiBfLnRvSW50ZWdlcihOdW1iZXIuTUlOX1ZBTFVFKTtcbiAqIC8vID0+IDBcbiAqXG4gKiBfLnRvSW50ZWdlcihJbmZpbml0eSk7XG4gKiAvLyA9PiAxLjc5NzY5MzEzNDg2MjMxNTdlKzMwOFxuICpcbiAqIF8udG9JbnRlZ2VyKCczLjInKTtcbiAqIC8vID0+IDNcbiAqL1xuZnVuY3Rpb24gdG9JbnRlZ2VyKHZhbHVlKSB7XG4gIHZhciByZXN1bHQgPSB0b0Zpbml0ZSh2YWx1ZSksXG4gICAgICByZW1haW5kZXIgPSByZXN1bHQgJSAxO1xuXG4gIHJldHVybiByZXN1bHQgPT09IHJlc3VsdCA/IChyZW1haW5kZXIgPyByZXN1bHQgLSByZW1haW5kZXIgOiByZXN1bHQpIDogMDtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSB0b0ludGVnZXI7XG4iLCJ2YXIgaXNPYmplY3QgPSByZXF1aXJlKCcuL2lzT2JqZWN0JyksXG4gICAgaXNTeW1ib2wgPSByZXF1aXJlKCcuL2lzU3ltYm9sJyk7XG5cbi8qKiBVc2VkIGFzIHJlZmVyZW5jZXMgZm9yIHZhcmlvdXMgYE51bWJlcmAgY29uc3RhbnRzLiAqL1xudmFyIE5BTiA9IDAgLyAwO1xuXG4vKiogVXNlZCB0byBtYXRjaCBsZWFkaW5nIGFuZCB0cmFpbGluZyB3aGl0ZXNwYWNlLiAqL1xudmFyIHJlVHJpbSA9IC9eXFxzK3xcXHMrJC9nO1xuXG4vKiogVXNlZCB0byBkZXRlY3QgYmFkIHNpZ25lZCBoZXhhZGVjaW1hbCBzdHJpbmcgdmFsdWVzLiAqL1xudmFyIHJlSXNCYWRIZXggPSAvXlstK10weFswLTlhLWZdKyQvaTtcblxuLyoqIFVzZWQgdG8gZGV0ZWN0IGJpbmFyeSBzdHJpbmcgdmFsdWVzLiAqL1xudmFyIHJlSXNCaW5hcnkgPSAvXjBiWzAxXSskL2k7XG5cbi8qKiBVc2VkIHRvIGRldGVjdCBvY3RhbCBzdHJpbmcgdmFsdWVzLiAqL1xudmFyIHJlSXNPY3RhbCA9IC9eMG9bMC03XSskL2k7XG5cbi8qKiBCdWlsdC1pbiBtZXRob2QgcmVmZXJlbmNlcyB3aXRob3V0IGEgZGVwZW5kZW5jeSBvbiBgcm9vdGAuICovXG52YXIgZnJlZVBhcnNlSW50ID0gcGFyc2VJbnQ7XG5cbi8qKlxuICogQ29udmVydHMgYHZhbHVlYCB0byBhIG51bWJlci5cbiAqXG4gKiBAc3RhdGljXG4gKiBAbWVtYmVyT2YgX1xuICogQHNpbmNlIDQuMC4wXG4gKiBAY2F0ZWdvcnkgTGFuZ1xuICogQHBhcmFtIHsqfSB2YWx1ZSBUaGUgdmFsdWUgdG8gcHJvY2Vzcy5cbiAqIEByZXR1cm5zIHtudW1iZXJ9IFJldHVybnMgdGhlIG51bWJlci5cbiAqIEBleGFtcGxlXG4gKlxuICogXy50b051bWJlcigzLjIpO1xuICogLy8gPT4gMy4yXG4gKlxuICogXy50b051bWJlcihOdW1iZXIuTUlOX1ZBTFVFKTtcbiAqIC8vID0+IDVlLTMyNFxuICpcbiAqIF8udG9OdW1iZXIoSW5maW5pdHkpO1xuICogLy8gPT4gSW5maW5pdHlcbiAqXG4gKiBfLnRvTnVtYmVyKCczLjInKTtcbiAqIC8vID0+IDMuMlxuICovXG5mdW5jdGlvbiB0b051bWJlcih2YWx1ZSkge1xuICBpZiAodHlwZW9mIHZhbHVlID09ICdudW1iZXInKSB7XG4gICAgcmV0dXJuIHZhbHVlO1xuICB9XG4gIGlmIChpc1N5bWJvbCh2YWx1ZSkpIHtcbiAgICByZXR1cm4gTkFOO1xuICB9XG4gIGlmIChpc09iamVjdCh2YWx1ZSkpIHtcbiAgICB2YXIgb3RoZXIgPSB0eXBlb2YgdmFsdWUudmFsdWVPZiA9PSAnZnVuY3Rpb24nID8gdmFsdWUudmFsdWVPZigpIDogdmFsdWU7XG4gICAgdmFsdWUgPSBpc09iamVjdChvdGhlcikgPyAob3RoZXIgKyAnJykgOiBvdGhlcjtcbiAgfVxuICBpZiAodHlwZW9mIHZhbHVlICE9ICdzdHJpbmcnKSB7XG4gICAgcmV0dXJuIHZhbHVlID09PSAwID8gdmFsdWUgOiArdmFsdWU7XG4gIH1cbiAgdmFsdWUgPSB2YWx1ZS5yZXBsYWNlKHJlVHJpbSwgJycpO1xuICB2YXIgaXNCaW5hcnkgPSByZUlzQmluYXJ5LnRlc3QodmFsdWUpO1xuICByZXR1cm4gKGlzQmluYXJ5IHx8IHJlSXNPY3RhbC50ZXN0KHZhbHVlKSlcbiAgICA/IGZyZWVQYXJzZUludCh2YWx1ZS5zbGljZSgyKSwgaXNCaW5hcnkgPyAyIDogOClcbiAgICA6IChyZUlzQmFkSGV4LnRlc3QodmFsdWUpID8gTkFOIDogK3ZhbHVlKTtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSB0b051bWJlcjtcbiIsInZhciBiYXNlVG9TdHJpbmcgPSByZXF1aXJlKCcuL19iYXNlVG9TdHJpbmcnKTtcblxuLyoqXG4gKiBDb252ZXJ0cyBgdmFsdWVgIHRvIGEgc3RyaW5nLiBBbiBlbXB0eSBzdHJpbmcgaXMgcmV0dXJuZWQgZm9yIGBudWxsYFxuICogYW5kIGB1bmRlZmluZWRgIHZhbHVlcy4gVGhlIHNpZ24gb2YgYC0wYCBpcyBwcmVzZXJ2ZWQuXG4gKlxuICogQHN0YXRpY1xuICogQG1lbWJlck9mIF9cbiAqIEBzaW5jZSA0LjAuMFxuICogQGNhdGVnb3J5IExhbmdcbiAqIEBwYXJhbSB7Kn0gdmFsdWUgVGhlIHZhbHVlIHRvIGNvbnZlcnQuXG4gKiBAcmV0dXJucyB7c3RyaW5nfSBSZXR1cm5zIHRoZSBjb252ZXJ0ZWQgc3RyaW5nLlxuICogQGV4YW1wbGVcbiAqXG4gKiBfLnRvU3RyaW5nKG51bGwpO1xuICogLy8gPT4gJydcbiAqXG4gKiBfLnRvU3RyaW5nKC0wKTtcbiAqIC8vID0+ICctMCdcbiAqXG4gKiBfLnRvU3RyaW5nKFsxLCAyLCAzXSk7XG4gKiAvLyA9PiAnMSwyLDMnXG4gKi9cbmZ1bmN0aW9uIHRvU3RyaW5nKHZhbHVlKSB7XG4gIHJldHVybiB2YWx1ZSA9PSBudWxsID8gJycgOiBiYXNlVG9TdHJpbmcodmFsdWUpO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IHRvU3RyaW5nO1xuIiwiLyogZ2xvYmFscyBfX1ZVRV9TU1JfQ09OVEVYVF9fICovXG5cbi8vIElNUE9SVEFOVDogRG8gTk9UIHVzZSBFUzIwMTUgZmVhdHVyZXMgaW4gdGhpcyBmaWxlIChleGNlcHQgZm9yIG1vZHVsZXMpLlxuLy8gVGhpcyBtb2R1bGUgaXMgYSBydW50aW1lIHV0aWxpdHkgZm9yIGNsZWFuZXIgY29tcG9uZW50IG1vZHVsZSBvdXRwdXQgYW5kIHdpbGxcbi8vIGJlIGluY2x1ZGVkIGluIHRoZSBmaW5hbCB3ZWJwYWNrIHVzZXIgYnVuZGxlLlxuXG5leHBvcnQgZGVmYXVsdCBmdW5jdGlvbiBub3JtYWxpemVDb21wb25lbnQgKFxuICBzY3JpcHRFeHBvcnRzLFxuICByZW5kZXIsXG4gIHN0YXRpY1JlbmRlckZucyxcbiAgZnVuY3Rpb25hbFRlbXBsYXRlLFxuICBpbmplY3RTdHlsZXMsXG4gIHNjb3BlSWQsXG4gIG1vZHVsZUlkZW50aWZpZXIsIC8qIHNlcnZlciBvbmx5ICovXG4gIHNoYWRvd01vZGUgLyogdnVlLWNsaSBvbmx5ICovXG4pIHtcbiAgLy8gVnVlLmV4dGVuZCBjb25zdHJ1Y3RvciBleHBvcnQgaW50ZXJvcFxuICB2YXIgb3B0aW9ucyA9IHR5cGVvZiBzY3JpcHRFeHBvcnRzID09PSAnZnVuY3Rpb24nXG4gICAgPyBzY3JpcHRFeHBvcnRzLm9wdGlvbnNcbiAgICA6IHNjcmlwdEV4cG9ydHNcblxuICAvLyByZW5kZXIgZnVuY3Rpb25zXG4gIGlmIChyZW5kZXIpIHtcbiAgICBvcHRpb25zLnJlbmRlciA9IHJlbmRlclxuICAgIG9wdGlvbnMuc3RhdGljUmVuZGVyRm5zID0gc3RhdGljUmVuZGVyRm5zXG4gICAgb3B0aW9ucy5fY29tcGlsZWQgPSB0cnVlXG4gIH1cblxuICAvLyBmdW5jdGlvbmFsIHRlbXBsYXRlXG4gIGlmIChmdW5jdGlvbmFsVGVtcGxhdGUpIHtcbiAgICBvcHRpb25zLmZ1bmN0aW9uYWwgPSB0cnVlXG4gIH1cblxuICAvLyBzY29wZWRJZFxuICBpZiAoc2NvcGVJZCkge1xuICAgIG9wdGlvbnMuX3Njb3BlSWQgPSAnZGF0YS12LScgKyBzY29wZUlkXG4gIH1cblxuICB2YXIgaG9va1xuICBpZiAobW9kdWxlSWRlbnRpZmllcikgeyAvLyBzZXJ2ZXIgYnVpbGRcbiAgICBob29rID0gZnVuY3Rpb24gKGNvbnRleHQpIHtcbiAgICAgIC8vIDIuMyBpbmplY3Rpb25cbiAgICAgIGNvbnRleHQgPVxuICAgICAgICBjb250ZXh0IHx8IC8vIGNhY2hlZCBjYWxsXG4gICAgICAgICh0aGlzLiR2bm9kZSAmJiB0aGlzLiR2bm9kZS5zc3JDb250ZXh0KSB8fCAvLyBzdGF0ZWZ1bFxuICAgICAgICAodGhpcy5wYXJlbnQgJiYgdGhpcy5wYXJlbnQuJHZub2RlICYmIHRoaXMucGFyZW50LiR2bm9kZS5zc3JDb250ZXh0KSAvLyBmdW5jdGlvbmFsXG4gICAgICAvLyAyLjIgd2l0aCBydW5Jbk5ld0NvbnRleHQ6IHRydWVcbiAgICAgIGlmICghY29udGV4dCAmJiB0eXBlb2YgX19WVUVfU1NSX0NPTlRFWFRfXyAhPT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgICAgY29udGV4dCA9IF9fVlVFX1NTUl9DT05URVhUX19cbiAgICAgIH1cbiAgICAgIC8vIGluamVjdCBjb21wb25lbnQgc3R5bGVzXG4gICAgICBpZiAoaW5qZWN0U3R5bGVzKSB7XG4gICAgICAgIGluamVjdFN0eWxlcy5jYWxsKHRoaXMsIGNvbnRleHQpXG4gICAgICB9XG4gICAgICAvLyByZWdpc3RlciBjb21wb25lbnQgbW9kdWxlIGlkZW50aWZpZXIgZm9yIGFzeW5jIGNodW5rIGluZmVycmVuY2VcbiAgICAgIGlmIChjb250ZXh0ICYmIGNvbnRleHQuX3JlZ2lzdGVyZWRDb21wb25lbnRzKSB7XG4gICAgICAgIGNvbnRleHQuX3JlZ2lzdGVyZWRDb21wb25lbnRzLmFkZChtb2R1bGVJZGVudGlmaWVyKVxuICAgICAgfVxuICAgIH1cbiAgICAvLyB1c2VkIGJ5IHNzciBpbiBjYXNlIGNvbXBvbmVudCBpcyBjYWNoZWQgYW5kIGJlZm9yZUNyZWF0ZVxuICAgIC8vIG5ldmVyIGdldHMgY2FsbGVkXG4gICAgb3B0aW9ucy5fc3NyUmVnaXN0ZXIgPSBob29rXG4gIH0gZWxzZSBpZiAoaW5qZWN0U3R5bGVzKSB7XG4gICAgaG9vayA9IHNoYWRvd01vZGVcbiAgICAgID8gZnVuY3Rpb24gKCkge1xuICAgICAgICBpbmplY3RTdHlsZXMuY2FsbChcbiAgICAgICAgICB0aGlzLFxuICAgICAgICAgIChvcHRpb25zLmZ1bmN0aW9uYWwgPyB0aGlzLnBhcmVudCA6IHRoaXMpLiRyb290LiRvcHRpb25zLnNoYWRvd1Jvb3RcbiAgICAgICAgKVxuICAgICAgfVxuICAgICAgOiBpbmplY3RTdHlsZXNcbiAgfVxuXG4gIGlmIChob29rKSB7XG4gICAgaWYgKG9wdGlvbnMuZnVuY3Rpb25hbCkge1xuICAgICAgLy8gZm9yIHRlbXBsYXRlLW9ubHkgaG90LXJlbG9hZCBiZWNhdXNlIGluIHRoYXQgY2FzZSB0aGUgcmVuZGVyIGZuIGRvZXNuJ3RcbiAgICAgIC8vIGdvIHRocm91Z2ggdGhlIG5vcm1hbGl6ZXJcbiAgICAgIG9wdGlvbnMuX2luamVjdFN0eWxlcyA9IGhvb2tcbiAgICAgIC8vIHJlZ2lzdGVyIGZvciBmdW5jdGlvbmFsIGNvbXBvbmVudCBpbiB2dWUgZmlsZVxuICAgICAgdmFyIG9yaWdpbmFsUmVuZGVyID0gb3B0aW9ucy5yZW5kZXJcbiAgICAgIG9wdGlvbnMucmVuZGVyID0gZnVuY3Rpb24gcmVuZGVyV2l0aFN0eWxlSW5qZWN0aW9uIChoLCBjb250ZXh0KSB7XG4gICAgICAgIGhvb2suY2FsbChjb250ZXh0KVxuICAgICAgICByZXR1cm4gb3JpZ2luYWxSZW5kZXIoaCwgY29udGV4dClcbiAgICAgIH1cbiAgICB9IGVsc2Uge1xuICAgICAgLy8gaW5qZWN0IGNvbXBvbmVudCByZWdpc3RyYXRpb24gYXMgYmVmb3JlQ3JlYXRlIGhvb2tcbiAgICAgIHZhciBleGlzdGluZyA9IG9wdGlvbnMuYmVmb3JlQ3JlYXRlXG4gICAgICBvcHRpb25zLmJlZm9yZUNyZWF0ZSA9IGV4aXN0aW5nXG4gICAgICAgID8gW10uY29uY2F0KGV4aXN0aW5nLCBob29rKVxuICAgICAgICA6IFtob29rXVxuICAgIH1cbiAgfVxuXG4gIHJldHVybiB7XG4gICAgZXhwb3J0czogc2NyaXB0RXhwb3J0cyxcbiAgICBvcHRpb25zOiBvcHRpb25zXG4gIH1cbn1cbiIsIi8vIHN0eWxlLWxvYWRlcjogQWRkcyBzb21lIGNzcyB0byB0aGUgRE9NIGJ5IGFkZGluZyBhIDxzdHlsZT4gdGFnXG5cbi8vIGxvYWQgdGhlIHN0eWxlc1xudmFyIGNvbnRlbnQgPSByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9kaXN0L2Nqcy5qcz8/cmVmLS03LTEhLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2xvYWRlcnMvc3R5bGVQb3N0TG9hZGVyLmpzIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9zcmMvaW5kZXguanM/P3JlZi0tNy0yIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9zYXNzLWxvYWRlci9kaXN0L2Nqcy5qcz8/cmVmLS03LTMhLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2luZGV4LmpzPz92dWUtbG9hZGVyLW9wdGlvbnMhLi9Db250cm9sS2l0LnZ1ZT92dWUmdHlwZT1zdHlsZSZpbmRleD0wJmxhbmc9c2NzcyZcIik7XG5pZih0eXBlb2YgY29udGVudCA9PT0gJ3N0cmluZycpIGNvbnRlbnQgPSBbW21vZHVsZS5pZCwgY29udGVudCwgJyddXTtcbmlmKGNvbnRlbnQubG9jYWxzKSBtb2R1bGUuZXhwb3J0cyA9IGNvbnRlbnQubG9jYWxzO1xuLy8gYWRkIHRoZSBzdHlsZXMgdG8gdGhlIERPTVxudmFyIGFkZCA9IHJlcXVpcmUoXCIhLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1zdHlsZS1sb2FkZXIvbGliL2FkZFN0eWxlc0NsaWVudC5qc1wiKS5kZWZhdWx0XG52YXIgdXBkYXRlID0gYWRkKFwiNmMwYmEzOTZcIiwgY29udGVudCwgZmFsc2UsIHt9KTtcbi8vIEhvdCBNb2R1bGUgUmVwbGFjZW1lbnRcbmlmKG1vZHVsZS5ob3QpIHtcbiAvLyBXaGVuIHRoZSBzdHlsZXMgY2hhbmdlLCB1cGRhdGUgdGhlIDxzdHlsZT4gdGFnc1xuIGlmKCFjb250ZW50LmxvY2Fscykge1xuICAgbW9kdWxlLmhvdC5hY2NlcHQoXCIhIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2Rpc3QvY2pzLmpzPz9yZWYtLTctMSEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvbG9hZGVycy9zdHlsZVBvc3RMb2FkZXIuanMhLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL3NyYy9pbmRleC5qcz8/cmVmLS03LTIhLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Nhc3MtbG9hZGVyL2Rpc3QvY2pzLmpzPz9yZWYtLTctMyEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvaW5kZXguanM/P3Z1ZS1sb2FkZXItb3B0aW9ucyEuL0NvbnRyb2xLaXQudnVlP3Z1ZSZ0eXBlPXN0eWxlJmluZGV4PTAmbGFuZz1zY3NzJlwiLCBmdW5jdGlvbigpIHtcbiAgICAgdmFyIG5ld0NvbnRlbnQgPSByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9kaXN0L2Nqcy5qcz8/cmVmLS03LTEhLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2xvYWRlcnMvc3R5bGVQb3N0TG9hZGVyLmpzIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9zcmMvaW5kZXguanM/P3JlZi0tNy0yIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9zYXNzLWxvYWRlci9kaXN0L2Nqcy5qcz8/cmVmLS03LTMhLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2luZGV4LmpzPz92dWUtbG9hZGVyLW9wdGlvbnMhLi9Db250cm9sS2l0LnZ1ZT92dWUmdHlwZT1zdHlsZSZpbmRleD0wJmxhbmc9c2NzcyZcIik7XG4gICAgIGlmKHR5cGVvZiBuZXdDb250ZW50ID09PSAnc3RyaW5nJykgbmV3Q29udGVudCA9IFtbbW9kdWxlLmlkLCBuZXdDb250ZW50LCAnJ11dO1xuICAgICB1cGRhdGUobmV3Q29udGVudCk7XG4gICB9KTtcbiB9XG4gLy8gV2hlbiB0aGUgbW9kdWxlIGlzIGRpc3Bvc2VkLCByZW1vdmUgdGhlIDxzdHlsZT4gdGFnc1xuIG1vZHVsZS5ob3QuZGlzcG9zZShmdW5jdGlvbigpIHsgdXBkYXRlKCk7IH0pO1xufSIsIi8qXG4gIE1JVCBMaWNlbnNlIGh0dHA6Ly93d3cub3BlbnNvdXJjZS5vcmcvbGljZW5zZXMvbWl0LWxpY2Vuc2UucGhwXG4gIEF1dGhvciBUb2JpYXMgS29wcGVycyBAc29rcmFcbiAgTW9kaWZpZWQgYnkgRXZhbiBZb3UgQHl5eDk5MDgwM1xuKi9cblxuaW1wb3J0IGxpc3RUb1N0eWxlcyBmcm9tICcuL2xpc3RUb1N0eWxlcydcblxudmFyIGhhc0RvY3VtZW50ID0gdHlwZW9mIGRvY3VtZW50ICE9PSAndW5kZWZpbmVkJ1xuXG5pZiAodHlwZW9mIERFQlVHICE9PSAndW5kZWZpbmVkJyAmJiBERUJVRykge1xuICBpZiAoIWhhc0RvY3VtZW50KSB7XG4gICAgdGhyb3cgbmV3IEVycm9yKFxuICAgICd2dWUtc3R5bGUtbG9hZGVyIGNhbm5vdCBiZSB1c2VkIGluIGEgbm9uLWJyb3dzZXIgZW52aXJvbm1lbnQuICcgK1xuICAgIFwiVXNlIHsgdGFyZ2V0OiAnbm9kZScgfSBpbiB5b3VyIFdlYnBhY2sgY29uZmlnIHRvIGluZGljYXRlIGEgc2VydmVyLXJlbmRlcmluZyBlbnZpcm9ubWVudC5cIlxuICApIH1cbn1cblxuLypcbnR5cGUgU3R5bGVPYmplY3QgPSB7XG4gIGlkOiBudW1iZXI7XG4gIHBhcnRzOiBBcnJheTxTdHlsZU9iamVjdFBhcnQ+XG59XG5cbnR5cGUgU3R5bGVPYmplY3RQYXJ0ID0ge1xuICBjc3M6IHN0cmluZztcbiAgbWVkaWE6IHN0cmluZztcbiAgc291cmNlTWFwOiA/c3RyaW5nXG59XG4qL1xuXG52YXIgc3R5bGVzSW5Eb20gPSB7LypcbiAgW2lkOiBudW1iZXJdOiB7XG4gICAgaWQ6IG51bWJlcixcbiAgICByZWZzOiBudW1iZXIsXG4gICAgcGFydHM6IEFycmF5PChvYmo/OiBTdHlsZU9iamVjdFBhcnQpID0+IHZvaWQ+XG4gIH1cbiovfVxuXG52YXIgaGVhZCA9IGhhc0RvY3VtZW50ICYmIChkb2N1bWVudC5oZWFkIHx8IGRvY3VtZW50LmdldEVsZW1lbnRzQnlUYWdOYW1lKCdoZWFkJylbMF0pXG52YXIgc2luZ2xldG9uRWxlbWVudCA9IG51bGxcbnZhciBzaW5nbGV0b25Db3VudGVyID0gMFxudmFyIGlzUHJvZHVjdGlvbiA9IGZhbHNlXG52YXIgbm9vcCA9IGZ1bmN0aW9uICgpIHt9XG52YXIgb3B0aW9ucyA9IG51bGxcbnZhciBzc3JJZEtleSA9ICdkYXRhLXZ1ZS1zc3ItaWQnXG5cbi8vIEZvcmNlIHNpbmdsZS10YWcgc29sdXRpb24gb24gSUU2LTksIHdoaWNoIGhhcyBhIGhhcmQgbGltaXQgb24gdGhlICMgb2YgPHN0eWxlPlxuLy8gdGFncyBpdCB3aWxsIGFsbG93IG9uIGEgcGFnZVxudmFyIGlzT2xkSUUgPSB0eXBlb2YgbmF2aWdhdG9yICE9PSAndW5kZWZpbmVkJyAmJiAvbXNpZSBbNi05XVxcYi8udGVzdChuYXZpZ2F0b3IudXNlckFnZW50LnRvTG93ZXJDYXNlKCkpXG5cbmV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIGFkZFN0eWxlc0NsaWVudCAocGFyZW50SWQsIGxpc3QsIF9pc1Byb2R1Y3Rpb24sIF9vcHRpb25zKSB7XG4gIGlzUHJvZHVjdGlvbiA9IF9pc1Byb2R1Y3Rpb25cblxuICBvcHRpb25zID0gX29wdGlvbnMgfHwge31cblxuICB2YXIgc3R5bGVzID0gbGlzdFRvU3R5bGVzKHBhcmVudElkLCBsaXN0KVxuICBhZGRTdHlsZXNUb0RvbShzdHlsZXMpXG5cbiAgcmV0dXJuIGZ1bmN0aW9uIHVwZGF0ZSAobmV3TGlzdCkge1xuICAgIHZhciBtYXlSZW1vdmUgPSBbXVxuICAgIGZvciAodmFyIGkgPSAwOyBpIDwgc3R5bGVzLmxlbmd0aDsgaSsrKSB7XG4gICAgICB2YXIgaXRlbSA9IHN0eWxlc1tpXVxuICAgICAgdmFyIGRvbVN0eWxlID0gc3R5bGVzSW5Eb21baXRlbS5pZF1cbiAgICAgIGRvbVN0eWxlLnJlZnMtLVxuICAgICAgbWF5UmVtb3ZlLnB1c2goZG9tU3R5bGUpXG4gICAgfVxuICAgIGlmIChuZXdMaXN0KSB7XG4gICAgICBzdHlsZXMgPSBsaXN0VG9TdHlsZXMocGFyZW50SWQsIG5ld0xpc3QpXG4gICAgICBhZGRTdHlsZXNUb0RvbShzdHlsZXMpXG4gICAgfSBlbHNlIHtcbiAgICAgIHN0eWxlcyA9IFtdXG4gICAgfVxuICAgIGZvciAodmFyIGkgPSAwOyBpIDwgbWF5UmVtb3ZlLmxlbmd0aDsgaSsrKSB7XG4gICAgICB2YXIgZG9tU3R5bGUgPSBtYXlSZW1vdmVbaV1cbiAgICAgIGlmIChkb21TdHlsZS5yZWZzID09PSAwKSB7XG4gICAgICAgIGZvciAodmFyIGogPSAwOyBqIDwgZG9tU3R5bGUucGFydHMubGVuZ3RoOyBqKyspIHtcbiAgICAgICAgICBkb21TdHlsZS5wYXJ0c1tqXSgpXG4gICAgICAgIH1cbiAgICAgICAgZGVsZXRlIHN0eWxlc0luRG9tW2RvbVN0eWxlLmlkXVxuICAgICAgfVxuICAgIH1cbiAgfVxufVxuXG5mdW5jdGlvbiBhZGRTdHlsZXNUb0RvbSAoc3R5bGVzIC8qIEFycmF5PFN0eWxlT2JqZWN0PiAqLykge1xuICBmb3IgKHZhciBpID0gMDsgaSA8IHN0eWxlcy5sZW5ndGg7IGkrKykge1xuICAgIHZhciBpdGVtID0gc3R5bGVzW2ldXG4gICAgdmFyIGRvbVN0eWxlID0gc3R5bGVzSW5Eb21baXRlbS5pZF1cbiAgICBpZiAoZG9tU3R5bGUpIHtcbiAgICAgIGRvbVN0eWxlLnJlZnMrK1xuICAgICAgZm9yICh2YXIgaiA9IDA7IGogPCBkb21TdHlsZS5wYXJ0cy5sZW5ndGg7IGorKykge1xuICAgICAgICBkb21TdHlsZS5wYXJ0c1tqXShpdGVtLnBhcnRzW2pdKVxuICAgICAgfVxuICAgICAgZm9yICg7IGogPCBpdGVtLnBhcnRzLmxlbmd0aDsgaisrKSB7XG4gICAgICAgIGRvbVN0eWxlLnBhcnRzLnB1c2goYWRkU3R5bGUoaXRlbS5wYXJ0c1tqXSkpXG4gICAgICB9XG4gICAgICBpZiAoZG9tU3R5bGUucGFydHMubGVuZ3RoID4gaXRlbS5wYXJ0cy5sZW5ndGgpIHtcbiAgICAgICAgZG9tU3R5bGUucGFydHMubGVuZ3RoID0gaXRlbS5wYXJ0cy5sZW5ndGhcbiAgICAgIH1cbiAgICB9IGVsc2Uge1xuICAgICAgdmFyIHBhcnRzID0gW11cbiAgICAgIGZvciAodmFyIGogPSAwOyBqIDwgaXRlbS5wYXJ0cy5sZW5ndGg7IGorKykge1xuICAgICAgICBwYXJ0cy5wdXNoKGFkZFN0eWxlKGl0ZW0ucGFydHNbal0pKVxuICAgICAgfVxuICAgICAgc3R5bGVzSW5Eb21baXRlbS5pZF0gPSB7IGlkOiBpdGVtLmlkLCByZWZzOiAxLCBwYXJ0czogcGFydHMgfVxuICAgIH1cbiAgfVxufVxuXG5mdW5jdGlvbiBjcmVhdGVTdHlsZUVsZW1lbnQgKCkge1xuICB2YXIgc3R5bGVFbGVtZW50ID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnc3R5bGUnKVxuICBzdHlsZUVsZW1lbnQudHlwZSA9ICd0ZXh0L2NzcydcbiAgaGVhZC5hcHBlbmRDaGlsZChzdHlsZUVsZW1lbnQpXG4gIHJldHVybiBzdHlsZUVsZW1lbnRcbn1cblxuZnVuY3Rpb24gYWRkU3R5bGUgKG9iaiAvKiBTdHlsZU9iamVjdFBhcnQgKi8pIHtcbiAgdmFyIHVwZGF0ZSwgcmVtb3ZlXG4gIHZhciBzdHlsZUVsZW1lbnQgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCdzdHlsZVsnICsgc3NySWRLZXkgKyAnfj1cIicgKyBvYmouaWQgKyAnXCJdJylcblxuICBpZiAoc3R5bGVFbGVtZW50KSB7XG4gICAgaWYgKGlzUHJvZHVjdGlvbikge1xuICAgICAgLy8gaGFzIFNTUiBzdHlsZXMgYW5kIGluIHByb2R1Y3Rpb24gbW9kZS5cbiAgICAgIC8vIHNpbXBseSBkbyBub3RoaW5nLlxuICAgICAgcmV0dXJuIG5vb3BcbiAgICB9IGVsc2Uge1xuICAgICAgLy8gaGFzIFNTUiBzdHlsZXMgYnV0IGluIGRldiBtb2RlLlxuICAgICAgLy8gZm9yIHNvbWUgcmVhc29uIENocm9tZSBjYW4ndCBoYW5kbGUgc291cmNlIG1hcCBpbiBzZXJ2ZXItcmVuZGVyZWRcbiAgICAgIC8vIHN0eWxlIHRhZ3MgLSBzb3VyY2UgbWFwcyBpbiA8c3R5bGU+IG9ubHkgd29ya3MgaWYgdGhlIHN0eWxlIHRhZyBpc1xuICAgICAgLy8gY3JlYXRlZCBhbmQgaW5zZXJ0ZWQgZHluYW1pY2FsbHkuIFNvIHdlIHJlbW92ZSB0aGUgc2VydmVyIHJlbmRlcmVkXG4gICAgICAvLyBzdHlsZXMgYW5kIGluamVjdCBuZXcgb25lcy5cbiAgICAgIHN0eWxlRWxlbWVudC5wYXJlbnROb2RlLnJlbW92ZUNoaWxkKHN0eWxlRWxlbWVudClcbiAgICB9XG4gIH1cblxuICBpZiAoaXNPbGRJRSkge1xuICAgIC8vIHVzZSBzaW5nbGV0b24gbW9kZSBmb3IgSUU5LlxuICAgIHZhciBzdHlsZUluZGV4ID0gc2luZ2xldG9uQ291bnRlcisrXG4gICAgc3R5bGVFbGVtZW50ID0gc2luZ2xldG9uRWxlbWVudCB8fCAoc2luZ2xldG9uRWxlbWVudCA9IGNyZWF0ZVN0eWxlRWxlbWVudCgpKVxuICAgIHVwZGF0ZSA9IGFwcGx5VG9TaW5nbGV0b25UYWcuYmluZChudWxsLCBzdHlsZUVsZW1lbnQsIHN0eWxlSW5kZXgsIGZhbHNlKVxuICAgIHJlbW92ZSA9IGFwcGx5VG9TaW5nbGV0b25UYWcuYmluZChudWxsLCBzdHlsZUVsZW1lbnQsIHN0eWxlSW5kZXgsIHRydWUpXG4gIH0gZWxzZSB7XG4gICAgLy8gdXNlIG11bHRpLXN0eWxlLXRhZyBtb2RlIGluIGFsbCBvdGhlciBjYXNlc1xuICAgIHN0eWxlRWxlbWVudCA9IGNyZWF0ZVN0eWxlRWxlbWVudCgpXG4gICAgdXBkYXRlID0gYXBwbHlUb1RhZy5iaW5kKG51bGwsIHN0eWxlRWxlbWVudClcbiAgICByZW1vdmUgPSBmdW5jdGlvbiAoKSB7XG4gICAgICBzdHlsZUVsZW1lbnQucGFyZW50Tm9kZS5yZW1vdmVDaGlsZChzdHlsZUVsZW1lbnQpXG4gICAgfVxuICB9XG5cbiAgdXBkYXRlKG9iailcblxuICByZXR1cm4gZnVuY3Rpb24gdXBkYXRlU3R5bGUgKG5ld09iaiAvKiBTdHlsZU9iamVjdFBhcnQgKi8pIHtcbiAgICBpZiAobmV3T2JqKSB7XG4gICAgICBpZiAobmV3T2JqLmNzcyA9PT0gb2JqLmNzcyAmJlxuICAgICAgICAgIG5ld09iai5tZWRpYSA9PT0gb2JqLm1lZGlhICYmXG4gICAgICAgICAgbmV3T2JqLnNvdXJjZU1hcCA9PT0gb2JqLnNvdXJjZU1hcCkge1xuICAgICAgICByZXR1cm5cbiAgICAgIH1cbiAgICAgIHVwZGF0ZShvYmogPSBuZXdPYmopXG4gICAgfSBlbHNlIHtcbiAgICAgIHJlbW92ZSgpXG4gICAgfVxuICB9XG59XG5cbnZhciByZXBsYWNlVGV4dCA9IChmdW5jdGlvbiAoKSB7XG4gIHZhciB0ZXh0U3RvcmUgPSBbXVxuXG4gIHJldHVybiBmdW5jdGlvbiAoaW5kZXgsIHJlcGxhY2VtZW50KSB7XG4gICAgdGV4dFN0b3JlW2luZGV4XSA9IHJlcGxhY2VtZW50XG4gICAgcmV0dXJuIHRleHRTdG9yZS5maWx0ZXIoQm9vbGVhbikuam9pbignXFxuJylcbiAgfVxufSkoKVxuXG5mdW5jdGlvbiBhcHBseVRvU2luZ2xldG9uVGFnIChzdHlsZUVsZW1lbnQsIGluZGV4LCByZW1vdmUsIG9iaikge1xuICB2YXIgY3NzID0gcmVtb3ZlID8gJycgOiBvYmouY3NzXG5cbiAgaWYgKHN0eWxlRWxlbWVudC5zdHlsZVNoZWV0KSB7XG4gICAgc3R5bGVFbGVtZW50LnN0eWxlU2hlZXQuY3NzVGV4dCA9IHJlcGxhY2VUZXh0KGluZGV4LCBjc3MpXG4gIH0gZWxzZSB7XG4gICAgdmFyIGNzc05vZGUgPSBkb2N1bWVudC5jcmVhdGVUZXh0Tm9kZShjc3MpXG4gICAgdmFyIGNoaWxkTm9kZXMgPSBzdHlsZUVsZW1lbnQuY2hpbGROb2Rlc1xuICAgIGlmIChjaGlsZE5vZGVzW2luZGV4XSkgc3R5bGVFbGVtZW50LnJlbW92ZUNoaWxkKGNoaWxkTm9kZXNbaW5kZXhdKVxuICAgIGlmIChjaGlsZE5vZGVzLmxlbmd0aCkge1xuICAgICAgc3R5bGVFbGVtZW50Lmluc2VydEJlZm9yZShjc3NOb2RlLCBjaGlsZE5vZGVzW2luZGV4XSlcbiAgICB9IGVsc2Uge1xuICAgICAgc3R5bGVFbGVtZW50LmFwcGVuZENoaWxkKGNzc05vZGUpXG4gICAgfVxuICB9XG59XG5cbmZ1bmN0aW9uIGFwcGx5VG9UYWcgKHN0eWxlRWxlbWVudCwgb2JqKSB7XG4gIHZhciBjc3MgPSBvYmouY3NzXG4gIHZhciBtZWRpYSA9IG9iai5tZWRpYVxuICB2YXIgc291cmNlTWFwID0gb2JqLnNvdXJjZU1hcFxuXG4gIGlmIChtZWRpYSkge1xuICAgIHN0eWxlRWxlbWVudC5zZXRBdHRyaWJ1dGUoJ21lZGlhJywgbWVkaWEpXG4gIH1cbiAgaWYgKG9wdGlvbnMuc3NySWQpIHtcbiAgICBzdHlsZUVsZW1lbnQuc2V0QXR0cmlidXRlKHNzcklkS2V5LCBvYmouaWQpXG4gIH1cblxuICBpZiAoc291cmNlTWFwKSB7XG4gICAgLy8gaHR0cHM6Ly9kZXZlbG9wZXIuY2hyb21lLmNvbS9kZXZ0b29scy9kb2NzL2phdmFzY3JpcHQtZGVidWdnaW5nXG4gICAgLy8gdGhpcyBtYWtlcyBzb3VyY2UgbWFwcyBpbnNpZGUgc3R5bGUgdGFncyB3b3JrIHByb3Blcmx5IGluIENocm9tZVxuICAgIGNzcyArPSAnXFxuLyojIHNvdXJjZVVSTD0nICsgc291cmNlTWFwLnNvdXJjZXNbMF0gKyAnICovJ1xuICAgIC8vIGh0dHA6Ly9zdGFja292ZXJmbG93LmNvbS9hLzI2NjAzODc1XG4gICAgY3NzICs9ICdcXG4vKiMgc291cmNlTWFwcGluZ1VSTD1kYXRhOmFwcGxpY2F0aW9uL2pzb247YmFzZTY0LCcgKyBidG9hKHVuZXNjYXBlKGVuY29kZVVSSUNvbXBvbmVudChKU09OLnN0cmluZ2lmeShzb3VyY2VNYXApKSkpICsgJyAqLydcbiAgfVxuXG4gIGlmIChzdHlsZUVsZW1lbnQuc3R5bGVTaGVldCkge1xuICAgIHN0eWxlRWxlbWVudC5zdHlsZVNoZWV0LmNzc1RleHQgPSBjc3NcbiAgfSBlbHNlIHtcbiAgICB3aGlsZSAoc3R5bGVFbGVtZW50LmZpcnN0Q2hpbGQpIHtcbiAgICAgIHN0eWxlRWxlbWVudC5yZW1vdmVDaGlsZChzdHlsZUVsZW1lbnQuZmlyc3RDaGlsZClcbiAgICB9XG4gICAgc3R5bGVFbGVtZW50LmFwcGVuZENoaWxkKGRvY3VtZW50LmNyZWF0ZVRleHROb2RlKGNzcykpXG4gIH1cbn1cbiIsIi8qKlxuICogVHJhbnNsYXRlcyB0aGUgbGlzdCBmb3JtYXQgcHJvZHVjZWQgYnkgY3NzLWxvYWRlciBpbnRvIHNvbWV0aGluZ1xuICogZWFzaWVyIHRvIG1hbmlwdWxhdGUuXG4gKi9cbmV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIGxpc3RUb1N0eWxlcyAocGFyZW50SWQsIGxpc3QpIHtcbiAgdmFyIHN0eWxlcyA9IFtdXG4gIHZhciBuZXdTdHlsZXMgPSB7fVxuICBmb3IgKHZhciBpID0gMDsgaSA8IGxpc3QubGVuZ3RoOyBpKyspIHtcbiAgICB2YXIgaXRlbSA9IGxpc3RbaV1cbiAgICB2YXIgaWQgPSBpdGVtWzBdXG4gICAgdmFyIGNzcyA9IGl0ZW1bMV1cbiAgICB2YXIgbWVkaWEgPSBpdGVtWzJdXG4gICAgdmFyIHNvdXJjZU1hcCA9IGl0ZW1bM11cbiAgICB2YXIgcGFydCA9IHtcbiAgICAgIGlkOiBwYXJlbnRJZCArICc6JyArIGksXG4gICAgICBjc3M6IGNzcyxcbiAgICAgIG1lZGlhOiBtZWRpYSxcbiAgICAgIHNvdXJjZU1hcDogc291cmNlTWFwXG4gICAgfVxuICAgIGlmICghbmV3U3R5bGVzW2lkXSkge1xuICAgICAgc3R5bGVzLnB1c2gobmV3U3R5bGVzW2lkXSA9IHsgaWQ6IGlkLCBwYXJ0czogW3BhcnRdIH0pXG4gICAgfSBlbHNlIHtcbiAgICAgIG5ld1N0eWxlc1tpZF0ucGFydHMucHVzaChwYXJ0KVxuICAgIH1cbiAgfVxuICByZXR1cm4gc3R5bGVzXG59XG4iLCJ2YXIgZztcblxuLy8gVGhpcyB3b3JrcyBpbiBub24tc3RyaWN0IG1vZGVcbmcgPSAoZnVuY3Rpb24oKSB7XG5cdHJldHVybiB0aGlzO1xufSkoKTtcblxudHJ5IHtcblx0Ly8gVGhpcyB3b3JrcyBpZiBldmFsIGlzIGFsbG93ZWQgKHNlZSBDU1ApXG5cdGcgPSBnIHx8IG5ldyBGdW5jdGlvbihcInJldHVybiB0aGlzXCIpKCk7XG59IGNhdGNoIChlKSB7XG5cdC8vIFRoaXMgd29ya3MgaWYgdGhlIHdpbmRvdyByZWZlcmVuY2UgaXMgYXZhaWxhYmxlXG5cdGlmICh0eXBlb2Ygd2luZG93ID09PSBcIm9iamVjdFwiKSBnID0gd2luZG93O1xufVxuXG4vLyBnIGNhbiBzdGlsbCBiZSB1bmRlZmluZWQsIGJ1dCBub3RoaW5nIHRvIGRvIGFib3V0IGl0Li4uXG4vLyBXZSByZXR1cm4gdW5kZWZpbmVkLCBpbnN0ZWFkIG9mIG5vdGhpbmcgaGVyZSwgc28gaXQnc1xuLy8gZWFzaWVyIHRvIGhhbmRsZSB0aGlzIGNhc2UuIGlmKCFnbG9iYWwpIHsgLi4ufVxuXG5tb2R1bGUuZXhwb3J0cyA9IGc7XG4iLCJtb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uKG1vZHVsZSkge1xuXHRpZiAoIW1vZHVsZS53ZWJwYWNrUG9seWZpbGwpIHtcblx0XHRtb2R1bGUuZGVwcmVjYXRlID0gZnVuY3Rpb24oKSB7fTtcblx0XHRtb2R1bGUucGF0aHMgPSBbXTtcblx0XHQvLyBtb2R1bGUucGFyZW50ID0gdW5kZWZpbmVkIGJ5IGRlZmF1bHRcblx0XHRpZiAoIW1vZHVsZS5jaGlsZHJlbikgbW9kdWxlLmNoaWxkcmVuID0gW107XG5cdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KG1vZHVsZSwgXCJsb2FkZWRcIiwge1xuXHRcdFx0ZW51bWVyYWJsZTogdHJ1ZSxcblx0XHRcdGdldDogZnVuY3Rpb24oKSB7XG5cdFx0XHRcdHJldHVybiBtb2R1bGUubDtcblx0XHRcdH1cblx0XHR9KTtcblx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkobW9kdWxlLCBcImlkXCIsIHtcblx0XHRcdGVudW1lcmFibGU6IHRydWUsXG5cdFx0XHRnZXQ6IGZ1bmN0aW9uKCkge1xuXHRcdFx0XHRyZXR1cm4gbW9kdWxlLmk7XG5cdFx0XHR9XG5cdFx0fSk7XG5cdFx0bW9kdWxlLndlYnBhY2tQb2x5ZmlsbCA9IDE7XG5cdH1cblx0cmV0dXJuIG1vZHVsZTtcbn07XG4iLCJ2YXIgcmVuZGVyLCBzdGF0aWNSZW5kZXJGbnNcbmltcG9ydCBzY3JpcHQgZnJvbSBcIi4vQ29udHJvbEtpdC52dWU/dnVlJnR5cGU9c2NyaXB0Jmxhbmc9anMmXCJcbmV4cG9ydCAqIGZyb20gXCIuL0NvbnRyb2xLaXQudnVlP3Z1ZSZ0eXBlPXNjcmlwdCZsYW5nPWpzJlwiXG5pbXBvcnQgc3R5bGUwIGZyb20gXCIuL0NvbnRyb2xLaXQudnVlP3Z1ZSZ0eXBlPXN0eWxlJmluZGV4PTAmbGFuZz1zY3NzJlwiXG5cblxuLyogbm9ybWFsaXplIGNvbXBvbmVudCAqL1xuaW1wb3J0IG5vcm1hbGl6ZXIgZnJvbSBcIiEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvcnVudGltZS9jb21wb25lbnROb3JtYWxpemVyLmpzXCJcbnZhciBjb21wb25lbnQgPSBub3JtYWxpemVyKFxuICBzY3JpcHQsXG4gIHJlbmRlcixcbiAgc3RhdGljUmVuZGVyRm5zLFxuICBmYWxzZSxcbiAgbnVsbCxcbiAgbnVsbCxcbiAgbnVsbFxuICBcbilcblxuLyogaG90IHJlbG9hZCAqL1xuaWYgKG1vZHVsZS5ob3QpIHtcbiAgdmFyIGFwaSA9IHJlcXVpcmUoXCJEOlxcXFxVc2Vyc1xcXFxkYW5pXFxcXERlc2t0b3BcXFxcQG1vbm9ncmlkXFxcXG5vZGVfbW9kdWxlc1xcXFx2dWUtaG90LXJlbG9hZC1hcGlcXFxcZGlzdFxcXFxpbmRleC5qc1wiKVxuICBhcGkuaW5zdGFsbChyZXF1aXJlKCd2dWUnKSlcbiAgaWYgKGFwaS5jb21wYXRpYmxlKSB7XG4gICAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICAgIGlmICghYXBpLmlzUmVjb3JkZWQoJzVkNDAzZGU5JykpIHtcbiAgICAgIGFwaS5jcmVhdGVSZWNvcmQoJzVkNDAzZGU5JywgY29tcG9uZW50Lm9wdGlvbnMpXG4gICAgfSBlbHNlIHtcbiAgICAgIGFwaS5yZWxvYWQoJzVkNDAzZGU5JywgY29tcG9uZW50Lm9wdGlvbnMpXG4gICAgfVxuICAgIFxuICB9XG59XG5jb21wb25lbnQub3B0aW9ucy5fX2ZpbGUgPSBcInNyYy9jb21wb25lbnRzL0NvbnRyb2xLaXQudnVlXCJcbmV4cG9ydCBkZWZhdWx0IGNvbXBvbmVudC5leHBvcnRzIiwiaW1wb3J0IG1vZCBmcm9tIFwiLSEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvYmFiZWwtbG9hZGVyL2xpYi9pbmRleC5qcyEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvaW5kZXguanM/P3Z1ZS1sb2FkZXItb3B0aW9ucyEuL0NvbnRyb2xLaXQudnVlP3Z1ZSZ0eXBlPXNjcmlwdCZsYW5nPWpzJlwiOyBleHBvcnQgZGVmYXVsdCBtb2Q7IGV4cG9ydCAqIGZyb20gXCItIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9iYWJlbC1sb2FkZXIvbGliL2luZGV4LmpzIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9pbmRleC5qcz8/dnVlLWxvYWRlci1vcHRpb25zIS4vQ29udHJvbEtpdC52dWU/dnVlJnR5cGU9c2NyaXB0Jmxhbmc9anMmXCIiLCJpbXBvcnQgbW9kIGZyb20gXCItIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtc3R5bGUtbG9hZGVyL2luZGV4LmpzIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2Rpc3QvY2pzLmpzPz9yZWYtLTctMSEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvbG9hZGVycy9zdHlsZVBvc3RMb2FkZXIuanMhLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL3NyYy9pbmRleC5qcz8/cmVmLS03LTIhLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Nhc3MtbG9hZGVyL2Rpc3QvY2pzLmpzPz9yZWYtLTctMyEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvaW5kZXguanM/P3Z1ZS1sb2FkZXItb3B0aW9ucyEuL0NvbnRyb2xLaXQudnVlP3Z1ZSZ0eXBlPXN0eWxlJmluZGV4PTAmbGFuZz1zY3NzJlwiOyBleHBvcnQgZGVmYXVsdCBtb2Q7IGV4cG9ydCAqIGZyb20gXCItIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtc3R5bGUtbG9hZGVyL2luZGV4LmpzIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2Rpc3QvY2pzLmpzPz9yZWYtLTctMSEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvbG9hZGVycy9zdHlsZVBvc3RMb2FkZXIuanMhLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL3NyYy9pbmRleC5qcz8/cmVmLS03LTIhLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Nhc3MtbG9hZGVyL2Rpc3QvY2pzLmpzPz9yZWYtLTctMyEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvaW5kZXguanM/P3Z1ZS1sb2FkZXItb3B0aW9ucyEuL0NvbnRyb2xLaXQudnVlP3Z1ZSZ0eXBlPXN0eWxlJmluZGV4PTAmbGFuZz1zY3NzJlwiIiwibW9kdWxlLmV4cG9ydHMgPSBfX1dFQlBBQ0tfRVhURVJOQUxfTU9EVUxFX19tb25vZ3JpZF9qc191dGlsc19fOyJdLCJzb3VyY2VSb290IjoiIn0=