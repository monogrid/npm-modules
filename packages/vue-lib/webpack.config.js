/* global __dirname require, module */
const path = require('path')
const env = require('yargs').argv.env // use --env with webpack 2
const pkg = require('./package.json')
const glob = require('glob')
const vueLoaderConfig = require('./vue-loader.conf')
const { VueLoaderPlugin } = require('vue-loader')
const TerserPlugin = require('terser-webpack-plugin')
// const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const utils = require('./utils')
const merge = require('webpack-merge')

const libraryName = pkg.name.split('/')[1]
// let outputFile
let mode

if (env === 'build') {
  mode = 'production'
  // outputFile = libraryName + '.min.js'
} else {
  mode = 'development'
  // outputFile = libraryName + '.js'
}

const baseConfig = {
  mode: mode,
  entry: path.join(__dirname, 'src/index.js'),
  devtool: mode === 'development' ? 'inline-source-map' : false,
  output: {
    path: path.join(__dirname, 'lib'),
    filename: 'outputFile',
    library: libraryName,
    libraryTarget: 'umd',
    umdNamedDefine: true
  },
  resolve: {
    extensions: ['.js', '.vue', '.json']
  },
  optimization: {
    minimizer: mode === 'production' ? [new TerserPlugin({
      cache: true,
      parallel: true,
      sourceMap: false
    })] : []
  },
  plugins: [
    new VueLoaderPlugin()
  ],
  module: {
    rules: [
      {
        test: /\.vue$/,
        loader: 'vue-loader',
        options: vueLoaderConfig
      },
      {
        test: /(\.jsx|\.js)$/,
        loader: 'babel-loader'
        // options: {
        //   comments: false,
        //   cacheDirectory: true
        // }
        // exclude: /node_modules/
      },
      {
        test: /\.(js|vue|jsx)$/,
        loader: 'eslint-loader',
        options: {
          formatter: require('eslint-friendly-formatter'),
          emitWarning: true
        },
        exclude: /node_modules/
      }
    ].concat(utils.styleLoaders({
      sourceMap: false,
      extract: false,
      usePostCSS: true
    }))
  },
  externals: ['vue', /@monogrid\/js-utils.*/]
}

const files = glob.sync('src/**/*.js').concat(glob.sync('src/**/*.vue')).concat(glob.sync('src/**/*.jsx'))
const config = []
files.forEach(file => {
  const extname = path.extname(file)
  const baseName = path.basename(file).split(extname)[0]
  const dirname = path.dirname(file)
  const filename = baseName + (mode === 'production' ? '.min.js' : '.js')
  config.push(merge(baseConfig, {
    entry: path.join(__dirname, file),
    output: {
      path: path.join(__dirname, 'lib' + (dirname.split('src')[1] ? dirname.split('src')[1] : '')),
      filename
    }
  }))
})

module.exports = config
