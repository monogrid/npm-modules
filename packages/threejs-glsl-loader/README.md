# threejs-glsl-loader
>A Webpack Loader that allows to load threejs inlined GLSL chunks.

**ALPHA VERSION, Tested with Threejs r92** 

## Installation

```bash
npm install --save-dev @monogrid/threejs-glsl-loader 
```

or

```bash
yarn add --dev @monogrid/threejs-glsl-loader
```

## Webpack Configuration

``` js
module: {
  rules: [{
    test: /\.(glsl|vert|frag)$/,
    loader: '@monogrid/threejs-glsl-loader',
    // Default values (can be omitted)
    options: {
      chunksPath: '../ShaderChunk', // if chunk fails to load with provided path (relative), the loader will retry with this one before giving up
      chunksExt: 'glsl', // Chunks extension, used when #import statement omits extension
    }
  }]
}
```

## How it behaves

``` glsl
// The content of chunks/a-chunk.glsl file will be inlined here as string
// the loader will first try to load from the specified path (relative to the current glsl file)
// Please, do not specify `./` if the additional file you're going to load resides in the same directory,
// this will give an error. However, adding `../` will let you move to upper level folder to search required file in there.
#include chunks/a-chunk.glsl;

// This is how Three.js specifies its chunks in its .glsl files
// the loader will ignore <> characters and append chunksExt
// the loader will then try to load the chunk from the same folder as the current glsl file
// if this fails it will try to load the chunk from the specified chunksPath config argument
// the default '../ShaderChunk' value for chunksPath currently (three.js r92) maps to where chunks are located in three.js
#include <a-chunk>;

```

## LICENSE
Copyright (c) 2020, MONOGRID S.R.L.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
