# lut-loader

> A webpack loader that enable to bundle LUT files (.cube) as PNG

## Installation

```bash
npm install --save-dev @monogrid/lut-loader
```

or

```bash
yarn add --dev @monogrid/lut-loader
```

## Webpack Configuration

You can use lut-loader in combination with other loaders.

For example, if you want to parse a .cube file and import it as PNG directly in your source code you have to do:

``` js
module: {
  rules: [{
    test: /\.cube$/,
    use: [
      {
        loader: 'file-loader',
        options: {
          name: utils.assetsPath('assets/lut/[name].[hash:7].png')
        }
      },
      {
        loader: '@monogrid/lut-loader'
      }
    ]
  }]
}
```

or if you want to obtain only the buffer data:

``` js
module: {
  rules: [{
    test: /\.cube$/,
    use: [
      {
        loader: '@monogrid/lut-loader'
      }
    ]
  }]
}
```

example of usage:

``` js
import LUTNight from './assets/luts-source/night.cube'

// ...

applyPngLut(LUTNight)
```

## LICENSE
Copyright (c) 2020, MONOGRID S.R.L.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
