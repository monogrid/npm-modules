(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define("js-utils", [], factory);
	else if(typeof exports === 'object')
		exports["js-utils"] = factory();
	else
		root["js-utils"] = factory();
})(typeof self !== 'undefined' ? self : this, function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/FileUtils.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/FileUtils.js":
/*!**************************!*\
  !*** ./src/FileUtils.js ***!
  \**************************/
/*! exports provided: downloadFile, loadFile, loadJSON, loadImage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "downloadFile", function() { return downloadFile; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "loadFile", function() { return loadFile; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "loadJSON", function() { return loadJSON; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "loadImage", function() { return loadImage; });
/**
 * A collection of File related utilities.
 *
 * @module FileUtils
 */

/**
 * Downloads a file to the user's machine.
 *
 * @param {String} content - The content to be downloaded.
 * @param {String} fileName - The name of the file downloaded into the user's PC.
 * @param {String} contentType - The file type.
 */
var downloadFile = function downloadFile(content, fileName, contentType) {
  var a = document.createElement('a');
  var file = new Blob([content], {
    type: contentType
  });
  a.href = URL.createObjectURL(file);
  a.download = fileName;
  a.click();
};
/**
 * Prompts the user to select a single file from his harddrive.
 *
 * @param {String} [accept] - Accept string to restrict file selection to certain file types.
 * @returns {Promise<Array>} Array of files selected by the user.
 */


var loadFile = function loadFile(accept) {
  return new Promise(function (resolve, reject) {
    var input = document.createElement('input');
    input.type = 'file';
    input.accept = accept;

    input.onchange = function (event) {
      resolve(event.target.files);
    };

    input.click();
  });
};
/**
 * Prompts the user to select a single JSON file from his harddrive
 * and returns the result of his selection.
 *
 * @returns {Promise<String>} The parsed JSON file.
 */


var loadJSON = function loadJSON() {
  return new Promise(function (resolve, reject) {
    loadFile('.json').then(function (files) {
      var fr = new FileReader();

      fr.onload = function (event) {
        resolve(JSON.parse(event.target.result));
      };

      fr.readAsText(files[0]);
      return true;
    }).catch(function (e) {
      throw e;
    });
  });
};
/**
 * Prompts the user to select a single JSON file from his harddrive
 * and returns the result of his selection
 *
 * @returns {Promise<Image>} The selected image object.
 */


var loadImage = function loadImage() {
  return new Promise(function (resolve, reject) {
    var img = new Image();

    img.onload = function (event) {
      resolve(img);
    };

    loadFile('image/*').then(function (files) {
      var fr = new FileReader();

      fr.onload = function (event) {
        img.src = event.target.result;
      };

      fr.readAsDataURL(files[0]);
      return true;
    }).catch(function (e) {
      throw e;
    });
  });
};



/***/ })

/******/ });
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9qcy11dGlscy93ZWJwYWNrL3VuaXZlcnNhbE1vZHVsZURlZmluaXRpb24iLCJ3ZWJwYWNrOi8vanMtdXRpbHMvd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vanMtdXRpbHMvLi9zcmMvRmlsZVV0aWxzLmpzIl0sIm5hbWVzIjpbImRvd25sb2FkRmlsZSIsImNvbnRlbnQiLCJmaWxlTmFtZSIsImNvbnRlbnRUeXBlIiwiYSIsImRvY3VtZW50IiwiY3JlYXRlRWxlbWVudCIsImZpbGUiLCJCbG9iIiwidHlwZSIsImhyZWYiLCJVUkwiLCJjcmVhdGVPYmplY3RVUkwiLCJkb3dubG9hZCIsImNsaWNrIiwibG9hZEZpbGUiLCJhY2NlcHQiLCJQcm9taXNlIiwicmVzb2x2ZSIsInJlamVjdCIsImlucHV0Iiwib25jaGFuZ2UiLCJldmVudCIsInRhcmdldCIsImZpbGVzIiwibG9hZEpTT04iLCJ0aGVuIiwiZnIiLCJGaWxlUmVhZGVyIiwib25sb2FkIiwiSlNPTiIsInBhcnNlIiwicmVzdWx0IiwicmVhZEFzVGV4dCIsImNhdGNoIiwiZSIsImxvYWRJbWFnZSIsImltZyIsIkltYWdlIiwic3JjIiwicmVhZEFzRGF0YVVSTCJdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQztBQUNELE87UUNWQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTs7O1FBR0E7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLDBDQUEwQyxnQ0FBZ0M7UUFDMUU7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSx3REFBd0Qsa0JBQWtCO1FBQzFFO1FBQ0EsaURBQWlELGNBQWM7UUFDL0Q7O1FBRUE7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBLHlDQUF5QyxpQ0FBaUM7UUFDMUUsZ0hBQWdILG1CQUFtQixFQUFFO1FBQ3JJO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0EsMkJBQTJCLDBCQUEwQixFQUFFO1FBQ3ZELGlDQUFpQyxlQUFlO1FBQ2hEO1FBQ0E7UUFDQTs7UUFFQTtRQUNBLHNEQUFzRCwrREFBK0Q7O1FBRXJIO1FBQ0E7OztRQUdBO1FBQ0E7Ozs7Ozs7Ozs7Ozs7QUNsRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBOzs7Ozs7QUFNQTs7Ozs7OztBQU9BLElBQU1BLFlBQVksR0FBRyxTQUFmQSxZQUFlLENBQVVDLE9BQVYsRUFBbUJDLFFBQW5CLEVBQTZCQyxXQUE3QixFQUEwQztBQUM3RCxNQUFNQyxDQUFDLEdBQUdDLFFBQVEsQ0FBQ0MsYUFBVCxDQUF1QixHQUF2QixDQUFWO0FBQ0EsTUFBTUMsSUFBSSxHQUFHLElBQUlDLElBQUosQ0FBUyxDQUFDUCxPQUFELENBQVQsRUFBb0I7QUFBRVEsUUFBSSxFQUFFTjtBQUFSLEdBQXBCLENBQWI7QUFDQUMsR0FBQyxDQUFDTSxJQUFGLEdBQVNDLEdBQUcsQ0FBQ0MsZUFBSixDQUFvQkwsSUFBcEIsQ0FBVDtBQUNBSCxHQUFDLENBQUNTLFFBQUYsR0FBYVgsUUFBYjtBQUNBRSxHQUFDLENBQUNVLEtBQUY7QUFDRCxDQU5EO0FBUUE7Ozs7Ozs7O0FBTUEsSUFBTUMsUUFBUSxHQUFHLFNBQVhBLFFBQVcsQ0FBVUMsTUFBVixFQUFrQjtBQUNqQyxTQUFPLElBQUlDLE9BQUosQ0FBWSxVQUFDQyxPQUFELEVBQVVDLE1BQVYsRUFBcUI7QUFDdEMsUUFBTUMsS0FBSyxHQUFHZixRQUFRLENBQUNDLGFBQVQsQ0FBdUIsT0FBdkIsQ0FBZDtBQUNBYyxTQUFLLENBQUNYLElBQU4sR0FBYSxNQUFiO0FBQ0FXLFNBQUssQ0FBQ0osTUFBTixHQUFlQSxNQUFmOztBQUNBSSxTQUFLLENBQUNDLFFBQU4sR0FBaUIsVUFBQ0MsS0FBRCxFQUFXO0FBQzFCSixhQUFPLENBQUNJLEtBQUssQ0FBQ0MsTUFBTixDQUFhQyxLQUFkLENBQVA7QUFDRCxLQUZEOztBQUdBSixTQUFLLENBQUNOLEtBQU47QUFDRCxHQVJNLENBQVA7QUFTRCxDQVZEO0FBWUE7Ozs7Ozs7O0FBTUEsSUFBTVcsUUFBUSxHQUFHLFNBQVhBLFFBQVcsR0FBWTtBQUMzQixTQUFPLElBQUlSLE9BQUosQ0FBWSxVQUFDQyxPQUFELEVBQVVDLE1BQVYsRUFBcUI7QUFDdENKLFlBQVEsQ0FBQyxPQUFELENBQVIsQ0FBa0JXLElBQWxCLENBQXVCLFVBQUFGLEtBQUssRUFBSTtBQUM5QixVQUFNRyxFQUFFLEdBQUcsSUFBSUMsVUFBSixFQUFYOztBQUNBRCxRQUFFLENBQUNFLE1BQUgsR0FBWSxVQUFDUCxLQUFELEVBQVc7QUFDckJKLGVBQU8sQ0FBQ1ksSUFBSSxDQUFDQyxLQUFMLENBQVdULEtBQUssQ0FBQ0MsTUFBTixDQUFhUyxNQUF4QixDQUFELENBQVA7QUFDRCxPQUZEOztBQUdBTCxRQUFFLENBQUNNLFVBQUgsQ0FBY1QsS0FBSyxDQUFDLENBQUQsQ0FBbkI7QUFDQSxhQUFPLElBQVA7QUFDRCxLQVBELEVBT0dVLEtBUEgsQ0FPUyxVQUFBQyxDQUFDLEVBQUk7QUFBRSxZQUFNQSxDQUFOO0FBQVMsS0FQekI7QUFRRCxHQVRNLENBQVA7QUFVRCxDQVhEO0FBWUE7Ozs7Ozs7O0FBTUEsSUFBTUMsU0FBUyxHQUFHLFNBQVpBLFNBQVksR0FBWTtBQUM1QixTQUFPLElBQUluQixPQUFKLENBQVksVUFBQ0MsT0FBRCxFQUFVQyxNQUFWLEVBQXFCO0FBQ3RDLFFBQU1rQixHQUFHLEdBQUcsSUFBSUMsS0FBSixFQUFaOztBQUNBRCxPQUFHLENBQUNSLE1BQUosR0FBYSxVQUFDUCxLQUFELEVBQVc7QUFDdEJKLGFBQU8sQ0FBQ21CLEdBQUQsQ0FBUDtBQUNELEtBRkQ7O0FBSUF0QixZQUFRLENBQUMsU0FBRCxDQUFSLENBQW9CVyxJQUFwQixDQUF5QixVQUFBRixLQUFLLEVBQUk7QUFDaEMsVUFBTUcsRUFBRSxHQUFHLElBQUlDLFVBQUosRUFBWDs7QUFDQUQsUUFBRSxDQUFDRSxNQUFILEdBQVksVUFBQ1AsS0FBRCxFQUFXO0FBQ3JCZSxXQUFHLENBQUNFLEdBQUosR0FBVWpCLEtBQUssQ0FBQ0MsTUFBTixDQUFhUyxNQUF2QjtBQUNELE9BRkQ7O0FBR0FMLFFBQUUsQ0FBQ2EsYUFBSCxDQUFpQmhCLEtBQUssQ0FBQyxDQUFELENBQXRCO0FBQ0EsYUFBTyxJQUFQO0FBQ0QsS0FQRCxFQU9HVSxLQVBILENBT1MsVUFBQUMsQ0FBQyxFQUFJO0FBQUUsWUFBTUEsQ0FBTjtBQUFTLEtBUHpCO0FBUUQsR0FkTSxDQUFQO0FBZUQsQ0FoQkQiLCJmaWxlIjoiRmlsZVV0aWxzLmpzIiwic291cmNlc0NvbnRlbnQiOlsiKGZ1bmN0aW9uIHdlYnBhY2tVbml2ZXJzYWxNb2R1bGVEZWZpbml0aW9uKHJvb3QsIGZhY3RvcnkpIHtcblx0aWYodHlwZW9mIGV4cG9ydHMgPT09ICdvYmplY3QnICYmIHR5cGVvZiBtb2R1bGUgPT09ICdvYmplY3QnKVxuXHRcdG1vZHVsZS5leHBvcnRzID0gZmFjdG9yeSgpO1xuXHRlbHNlIGlmKHR5cGVvZiBkZWZpbmUgPT09ICdmdW5jdGlvbicgJiYgZGVmaW5lLmFtZClcblx0XHRkZWZpbmUoXCJqcy11dGlsc1wiLCBbXSwgZmFjdG9yeSk7XG5cdGVsc2UgaWYodHlwZW9mIGV4cG9ydHMgPT09ICdvYmplY3QnKVxuXHRcdGV4cG9ydHNbXCJqcy11dGlsc1wiXSA9IGZhY3RvcnkoKTtcblx0ZWxzZVxuXHRcdHJvb3RbXCJqcy11dGlsc1wiXSA9IGZhY3RvcnkoKTtcbn0pKHR5cGVvZiBzZWxmICE9PSAndW5kZWZpbmVkJyA/IHNlbGYgOiB0aGlzLCBmdW5jdGlvbigpIHtcbnJldHVybiAiLCIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9uIGZvciBoYXJtb255IGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuIFx0XHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIG5hbWUpKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIG5hbWUsIHsgZW51bWVyYWJsZTogdHJ1ZSwgZ2V0OiBnZXR0ZXIgfSk7XG4gXHRcdH1cbiBcdH07XG5cbiBcdC8vIGRlZmluZSBfX2VzTW9kdWxlIG9uIGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uciA9IGZ1bmN0aW9uKGV4cG9ydHMpIHtcbiBcdFx0aWYodHlwZW9mIFN5bWJvbCAhPT0gJ3VuZGVmaW5lZCcgJiYgU3ltYm9sLnRvU3RyaW5nVGFnKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFN5bWJvbC50b1N0cmluZ1RhZywgeyB2YWx1ZTogJ01vZHVsZScgfSk7XG4gXHRcdH1cbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdfX2VzTW9kdWxlJywgeyB2YWx1ZTogdHJ1ZSB9KTtcbiBcdH07XG5cbiBcdC8vIGNyZWF0ZSBhIGZha2UgbmFtZXNwYWNlIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDE6IHZhbHVlIGlzIGEgbW9kdWxlIGlkLCByZXF1aXJlIGl0XG4gXHQvLyBtb2RlICYgMjogbWVyZ2UgYWxsIHByb3BlcnRpZXMgb2YgdmFsdWUgaW50byB0aGUgbnNcbiBcdC8vIG1vZGUgJiA0OiByZXR1cm4gdmFsdWUgd2hlbiBhbHJlYWR5IG5zIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDh8MTogYmVoYXZlIGxpa2UgcmVxdWlyZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy50ID0gZnVuY3Rpb24odmFsdWUsIG1vZGUpIHtcbiBcdFx0aWYobW9kZSAmIDEpIHZhbHVlID0gX193ZWJwYWNrX3JlcXVpcmVfXyh2YWx1ZSk7XG4gXHRcdGlmKG1vZGUgJiA4KSByZXR1cm4gdmFsdWU7XG4gXHRcdGlmKChtb2RlICYgNCkgJiYgdHlwZW9mIHZhbHVlID09PSAnb2JqZWN0JyAmJiB2YWx1ZSAmJiB2YWx1ZS5fX2VzTW9kdWxlKSByZXR1cm4gdmFsdWU7XG4gXHRcdHZhciBucyA9IE9iamVjdC5jcmVhdGUobnVsbCk7XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18ucihucyk7XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShucywgJ2RlZmF1bHQnLCB7IGVudW1lcmFibGU6IHRydWUsIHZhbHVlOiB2YWx1ZSB9KTtcbiBcdFx0aWYobW9kZSAmIDIgJiYgdHlwZW9mIHZhbHVlICE9ICdzdHJpbmcnKSBmb3IodmFyIGtleSBpbiB2YWx1ZSkgX193ZWJwYWNrX3JlcXVpcmVfXy5kKG5zLCBrZXksIGZ1bmN0aW9uKGtleSkgeyByZXR1cm4gdmFsdWVba2V5XTsgfS5iaW5kKG51bGwsIGtleSkpO1xuIFx0XHRyZXR1cm4gbnM7XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gXCIuL3NyYy9GaWxlVXRpbHMuanNcIik7XG4iLCIvKipcbiAqIEEgY29sbGVjdGlvbiBvZiBGaWxlIHJlbGF0ZWQgdXRpbGl0aWVzLlxuICpcbiAqIEBtb2R1bGUgRmlsZVV0aWxzXG4gKi9cblxuLyoqXG4gKiBEb3dubG9hZHMgYSBmaWxlIHRvIHRoZSB1c2VyJ3MgbWFjaGluZS5cbiAqXG4gKiBAcGFyYW0ge1N0cmluZ30gY29udGVudCAtIFRoZSBjb250ZW50IHRvIGJlIGRvd25sb2FkZWQuXG4gKiBAcGFyYW0ge1N0cmluZ30gZmlsZU5hbWUgLSBUaGUgbmFtZSBvZiB0aGUgZmlsZSBkb3dubG9hZGVkIGludG8gdGhlIHVzZXIncyBQQy5cbiAqIEBwYXJhbSB7U3RyaW5nfSBjb250ZW50VHlwZSAtIFRoZSBmaWxlIHR5cGUuXG4gKi9cbmNvbnN0IGRvd25sb2FkRmlsZSA9IGZ1bmN0aW9uIChjb250ZW50LCBmaWxlTmFtZSwgY29udGVudFR5cGUpIHtcbiAgY29uc3QgYSA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2EnKVxuICBjb25zdCBmaWxlID0gbmV3IEJsb2IoW2NvbnRlbnRdLCB7IHR5cGU6IGNvbnRlbnRUeXBlIH0pXG4gIGEuaHJlZiA9IFVSTC5jcmVhdGVPYmplY3RVUkwoZmlsZSlcbiAgYS5kb3dubG9hZCA9IGZpbGVOYW1lXG4gIGEuY2xpY2soKVxufVxuXG4vKipcbiAqIFByb21wdHMgdGhlIHVzZXIgdG8gc2VsZWN0IGEgc2luZ2xlIGZpbGUgZnJvbSBoaXMgaGFyZGRyaXZlLlxuICpcbiAqIEBwYXJhbSB7U3RyaW5nfSBbYWNjZXB0XSAtIEFjY2VwdCBzdHJpbmcgdG8gcmVzdHJpY3QgZmlsZSBzZWxlY3Rpb24gdG8gY2VydGFpbiBmaWxlIHR5cGVzLlxuICogQHJldHVybnMge1Byb21pc2U8QXJyYXk+fSBBcnJheSBvZiBmaWxlcyBzZWxlY3RlZCBieSB0aGUgdXNlci5cbiAqL1xuY29uc3QgbG9hZEZpbGUgPSBmdW5jdGlvbiAoYWNjZXB0KSB7XG4gIHJldHVybiBuZXcgUHJvbWlzZSgocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XG4gICAgY29uc3QgaW5wdXQgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdpbnB1dCcpXG4gICAgaW5wdXQudHlwZSA9ICdmaWxlJ1xuICAgIGlucHV0LmFjY2VwdCA9IGFjY2VwdFxuICAgIGlucHV0Lm9uY2hhbmdlID0gKGV2ZW50KSA9PiB7XG4gICAgICByZXNvbHZlKGV2ZW50LnRhcmdldC5maWxlcylcbiAgICB9XG4gICAgaW5wdXQuY2xpY2soKVxuICB9KVxufVxuXG4vKipcbiAqIFByb21wdHMgdGhlIHVzZXIgdG8gc2VsZWN0IGEgc2luZ2xlIEpTT04gZmlsZSBmcm9tIGhpcyBoYXJkZHJpdmVcbiAqIGFuZCByZXR1cm5zIHRoZSByZXN1bHQgb2YgaGlzIHNlbGVjdGlvbi5cbiAqXG4gKiBAcmV0dXJucyB7UHJvbWlzZTxTdHJpbmc+fSBUaGUgcGFyc2VkIEpTT04gZmlsZS5cbiAqL1xuY29uc3QgbG9hZEpTT04gPSBmdW5jdGlvbiAoKSB7XG4gIHJldHVybiBuZXcgUHJvbWlzZSgocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XG4gICAgbG9hZEZpbGUoJy5qc29uJykudGhlbihmaWxlcyA9PiB7XG4gICAgICBjb25zdCBmciA9IG5ldyBGaWxlUmVhZGVyKClcbiAgICAgIGZyLm9ubG9hZCA9IChldmVudCkgPT4ge1xuICAgICAgICByZXNvbHZlKEpTT04ucGFyc2UoZXZlbnQudGFyZ2V0LnJlc3VsdCkpXG4gICAgICB9XG4gICAgICBmci5yZWFkQXNUZXh0KGZpbGVzWzBdKVxuICAgICAgcmV0dXJuIHRydWVcbiAgICB9KS5jYXRjaChlID0+IHsgdGhyb3cgZSB9KVxuICB9KVxufVxuLyoqXG4gKiBQcm9tcHRzIHRoZSB1c2VyIHRvIHNlbGVjdCBhIHNpbmdsZSBKU09OIGZpbGUgZnJvbSBoaXMgaGFyZGRyaXZlXG4gKiBhbmQgcmV0dXJucyB0aGUgcmVzdWx0IG9mIGhpcyBzZWxlY3Rpb25cbiAqXG4gKiBAcmV0dXJucyB7UHJvbWlzZTxJbWFnZT59IFRoZSBzZWxlY3RlZCBpbWFnZSBvYmplY3QuXG4gKi9cbmNvbnN0IGxvYWRJbWFnZSA9IGZ1bmN0aW9uICgpIHtcbiAgcmV0dXJuIG5ldyBQcm9taXNlKChyZXNvbHZlLCByZWplY3QpID0+IHtcbiAgICBjb25zdCBpbWcgPSBuZXcgSW1hZ2UoKVxuICAgIGltZy5vbmxvYWQgPSAoZXZlbnQpID0+IHtcbiAgICAgIHJlc29sdmUoaW1nKVxuICAgIH1cblxuICAgIGxvYWRGaWxlKCdpbWFnZS8qJykudGhlbihmaWxlcyA9PiB7XG4gICAgICBjb25zdCBmciA9IG5ldyBGaWxlUmVhZGVyKClcbiAgICAgIGZyLm9ubG9hZCA9IChldmVudCkgPT4ge1xuICAgICAgICBpbWcuc3JjID0gZXZlbnQudGFyZ2V0LnJlc3VsdFxuICAgICAgfVxuICAgICAgZnIucmVhZEFzRGF0YVVSTChmaWxlc1swXSlcbiAgICAgIHJldHVybiB0cnVlXG4gICAgfSkuY2F0Y2goZSA9PiB7IHRocm93IGUgfSlcbiAgfSlcbn1cblxuZXhwb3J0IHtcbiAgZG93bmxvYWRGaWxlLFxuICBsb2FkRmlsZSxcbiAgbG9hZEpTT04sXG4gIGxvYWRJbWFnZVxufVxuIl0sInNvdXJjZVJvb3QiOiIifQ==