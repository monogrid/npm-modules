(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define("js-utils", [], factory);
	else if(typeof exports === 'object')
		exports["js-utils"] = factory();
	else
		root["js-utils"] = factory();
})(typeof self !== 'undefined' ? self : this, function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/Gyroscope.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "../../node_modules/@babel/runtime/helpers/classCallCheck.js":
/*!*********************************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/@babel/runtime/helpers/classCallCheck.js ***!
  \*********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

module.exports = _classCallCheck;

/***/ }),

/***/ "../../node_modules/@babel/runtime/helpers/createClass.js":
/*!******************************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/@babel/runtime/helpers/createClass.js ***!
  \******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

module.exports = _createClass;

/***/ }),

/***/ "../../node_modules/@hughsk/fulltilt/dist/fulltilt.js":
/*!**************************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/@hughsk/fulltilt/dist/fulltilt.js ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/**
 *
 * FULL TILT
 * http://github.com/richtr/Full-Tilt
 *
 * A standalone DeviceOrientation + DeviceMotion JavaScript library that
 * normalises orientation sensor input, applies relevant screen orientation
 * transforms, returns Euler Angle, Quaternion and Rotation
 * Matrix representations back to web developers and provides conversion
 * between all supported orientation representation types.
 *
 * Copyright: 2014 Rich Tibbett
 * License:   MIT
 *
 */

(function ( window ) {

// Only initialize the FULLTILT API if it is not already attached to the DOM
if ( window.FULLTILT !== undefined && window.FULLTILT !== null ) {
	return;
}

var M_PI   = Math.PI;
var M_PI_2 = M_PI / 2;
var M_2_PI = 2 * M_PI;

// Degree to Radian conversion
var degToRad = M_PI / 180;
var radToDeg = 180 / M_PI;

// Internal device orientation + motion variables
var sensors = {
	"orientation": {
		active:    false,
		callbacks: [],
		data:      undefined
	},
	"motion": {
		active:    false,
		callbacks: [],
		data:      undefined
	}
};
var screenActive = false;

// Internal screen orientation variables
var hasScreenOrientationAPI = window.screen && window.screen.orientation && window.screen.orientation.angle !== undefined && window.screen.orientation.angle !== null ? true : false;
var screenOrientationAngle = ( hasScreenOrientationAPI ? window.screen.orientation.angle : ( window.orientation || 0 ) ) * degToRad;

var SCREEN_ROTATION_0        = 0,
    SCREEN_ROTATION_90       = M_PI_2,
    SCREEN_ROTATION_180      = M_PI,
    SCREEN_ROTATION_270      = M_2_PI / 3,
    SCREEN_ROTATION_MINUS_90 = - M_PI_2;

// Math.sign polyfill
function sign(x) {
	x = +x; // convert to a number
	if (x === 0 || isNaN(x))
		return x;
	return x > 0 ? 1 : -1;
}

///// Promise-based Sensor Data checker //////

function SensorCheck(sensorRootObj) {

	var promise = new Promise(function(resolve, reject) {

		var runCheck = function (tries) {

			setTimeout(function() {

				if (sensorRootObj && sensorRootObj.data) {

					resolve();

				} else if (tries >= 20) {

					reject();

				} else {

					runCheck(++tries);

				}

			}, 50);

		};

		runCheck(0);

	});

	return promise;

}

////// Internal Event Handlers //////

function handleScreenOrientationChange () {

	if ( hasScreenOrientationAPI ) {

		screenOrientationAngle = ( window.screen.orientation.angle || 0 ) * degToRad;

	} else {

		screenOrientationAngle = ( window.orientation || 0 ) * degToRad;

	}

}

function handleDeviceOrientationChange ( event ) {

	sensors.orientation.data = event;

	// Fire every callback function each time deviceorientation is updated
	for ( var i in sensors.orientation.callbacks ) {

		sensors.orientation.callbacks[ i ].call( this );

	}

}

function handleDeviceMotionChange ( event ) {

	sensors.motion.data = event;

	// Fire every callback function each time devicemotion is updated
	for ( var i in sensors.motion.callbacks ) {

		sensors.motion.callbacks[ i ].call( this );

	}

}

///// FULLTILT API Root Object /////

var FULLTILT = {};

FULLTILT.version = "0.5.3";

///// FULLTILT API Root Methods /////

FULLTILT.getDeviceOrientation = function(options) {

	var promise = new Promise(function(resolve, reject) {

		var control = new FULLTILT.DeviceOrientation(options);

		control.start();

		var orientationSensorCheck = new SensorCheck(sensors.orientation);

		orientationSensorCheck.then(function() {

			resolve(control);

		}).catch(function() {

			control.stop();
			reject('DeviceOrientation is not supported');

		});

	});

	return promise;

};

FULLTILT.getDeviceMotion = function(options) {

	var promise = new Promise(function(resolve, reject) {

		var control = new FULLTILT.DeviceMotion(options);

		control.start();

		var motionSensorCheck = new SensorCheck(sensors.motion);

		motionSensorCheck.then(function() {

			resolve(control);

		}).catch(function() {

			control.stop();
			reject('DeviceMotion is not supported');

		});

	});

	return promise;

};


////// FULLTILT.Quaternion //////

FULLTILT.Quaternion = function ( x, y, z, w ) {

	var quat, outQuat;

	this.set = function ( x, y, z, w ) {

		this.x = x || 0;
		this.y = y || 0;
		this.z = z || 0;
		this.w = w || 1;

	};

	this.copy = function ( quaternion ) {

		this.x = quaternion.x;
		this.y = quaternion.y;
		this.z = quaternion.z;
		this.w = quaternion.w;

	};

	this.setFromEuler = (function () {

		var _x, _y, _z;
		var _x_2, _y_2, _z_2;
		var cX, cY, cZ, sX, sY, sZ;

		return function ( euler ) {

			euler = euler || {};

			_z = ( euler.alpha || 0 ) * degToRad;
			_x = ( euler.beta || 0 ) * degToRad;
			_y = ( euler.gamma || 0 ) * degToRad;

			_z_2 = _z / 2;
			_x_2 = _x / 2;
			_y_2 = _y / 2;

			cX = Math.cos( _x_2 );
			cY = Math.cos( _y_2 );
			cZ = Math.cos( _z_2 );
			sX = Math.sin( _x_2 );
			sY = Math.sin( _y_2 );
			sZ = Math.sin( _z_2 );

			this.set(
				sX * cY * cZ - cX * sY * sZ, // x
				cX * sY * cZ + sX * cY * sZ, // y
				cX * cY * sZ + sX * sY * cZ, // z
				cX * cY * cZ - sX * sY * sZ  // w
			);

			this.normalize();

			return this;

		};

	})();

	this.setFromRotationMatrix = (function () {

		var R;

		return function( matrix ) {

			R = matrix.elements;

			this.set(
				0.5 * Math.sqrt( 1 + R[0] - R[4] - R[8] ) * sign( R[7] - R[5] ), // x
				0.5 * Math.sqrt( 1 - R[0] + R[4] - R[8] ) * sign( R[2] - R[6] ), // y
				0.5 * Math.sqrt( 1 - R[0] - R[4] + R[8] ) * sign( R[3] - R[1] ), // z
				0.5 * Math.sqrt( 1 + R[0] + R[4] + R[8] )                        // w
			);

			return this;

		};

	})();

	this.multiply = function ( quaternion ) {

		outQuat = FULLTILT.Quaternion.prototype.multiplyQuaternions( this, quaternion );
		this.copy( outQuat );

		return this;

	};

	this.rotateX = function ( angle ) {

		outQuat = FULLTILT.Quaternion.prototype.rotateByAxisAngle( this, [ 1, 0, 0 ], angle );
		this.copy( outQuat );

		return this;

	};

	this.rotateY = function ( angle ) {

		outQuat = FULLTILT.Quaternion.prototype.rotateByAxisAngle( this, [ 0, 1, 0 ], angle );
		this.copy( outQuat );

		return this;

	};

	this.rotateZ = function ( angle ) {

		outQuat = FULLTILT.Quaternion.prototype.rotateByAxisAngle( this, [ 0, 0, 1 ], angle );
		this.copy( outQuat );

		return this;

	};

	this.normalize = function () {

		return FULLTILT.Quaternion.prototype.normalize( this );

	};

	// Initialize object values
	this.set( x, y, z, w );

};

FULLTILT.Quaternion.prototype = {

	constructor: FULLTILT.Quaternion,

	multiplyQuaternions: function () {

		var multipliedQuat = new FULLTILT.Quaternion();

		return function ( a, b ) {

			var qax = a.x, qay = a.y, qaz = a.z, qaw = a.w;
			var qbx = b.x, qby = b.y, qbz = b.z, qbw = b.w;

			multipliedQuat.set(
				qax * qbw + qaw * qbx + qay * qbz - qaz * qby, // x
				qay * qbw + qaw * qby + qaz * qbx - qax * qbz, // y
				qaz * qbw + qaw * qbz + qax * qby - qay * qbx, // z
				qaw * qbw - qax * qbx - qay * qby - qaz * qbz  // w
			);

			return multipliedQuat;

		};

	}(),

	normalize: function( q ) {

		var len = Math.sqrt( q.x * q.x + q.y * q.y + q.z * q.z + q.w * q.w );

		if ( len === 0 ) {

			q.x = 0;
			q.y = 0;
			q.z = 0;
			q.w = 1;

		} else {

			len = 1 / len;

			q.x *= len;
			q.y *= len;
			q.z *= len;
			q.w *= len;

		}

		return q;

	},

	rotateByAxisAngle: function () {

		var outputQuaternion = new FULLTILT.Quaternion();
		var transformQuaternion = new FULLTILT.Quaternion();

		var halfAngle, sA;

		return function ( targetQuaternion, axis, angle ) {

			halfAngle = ( angle || 0 ) / 2;
			sA = Math.sin( halfAngle );

			transformQuaternion.set(
				( axis[ 0 ] || 0 ) * sA, // x
				( axis[ 1 ] || 0 ) * sA, // y
				( axis[ 2 ] || 0 ) * sA, // z
				Math.cos( halfAngle )    // w
			);

			// Multiply quaternion by q
			outputQuaternion = FULLTILT.Quaternion.prototype.multiplyQuaternions( targetQuaternion, transformQuaternion );

			return FULLTILT.Quaternion.prototype.normalize( outputQuaternion );

		};

	}()

};

////// FULLTILT.RotationMatrix //////

FULLTILT.RotationMatrix = function ( m11, m12, m13, m21, m22, m23, m31, m32, m33 ) {

	var outMatrix;

	this.elements = new Float32Array( 9 );

	this.identity = function () {

		this.set(
			1, 0, 0,
			0, 1, 0,
			0, 0, 1
		);

		return this;

	};

	this.set = function ( m11, m12, m13, m21, m22, m23, m31, m32, m33 ) {

		this.elements[ 0 ] = m11 || 1;
		this.elements[ 1 ] = m12 || 0;
		this.elements[ 2 ] = m13 || 0;
		this.elements[ 3 ] = m21 || 0;
		this.elements[ 4 ] = m22 || 1;
		this.elements[ 5 ] = m23 || 0;
		this.elements[ 6 ] = m31 || 0;
		this.elements[ 7 ] = m32 || 0;
		this.elements[ 8 ] = m33 || 1;

	};

	this.copy = function ( matrix ) {

		this.elements[ 0 ] = matrix.elements[ 0 ];
		this.elements[ 1 ] = matrix.elements[ 1 ];
		this.elements[ 2 ] = matrix.elements[ 2 ];
		this.elements[ 3 ] = matrix.elements[ 3 ];
		this.elements[ 4 ] = matrix.elements[ 4 ];
		this.elements[ 5 ] = matrix.elements[ 5 ];
		this.elements[ 6 ] = matrix.elements[ 6 ];
		this.elements[ 7 ] = matrix.elements[ 7 ];
		this.elements[ 8 ] = matrix.elements[ 8 ];

	};

	this.setFromEuler = (function() {

		var _x, _y, _z;
		var cX, cY, cZ, sX, sY, sZ;

		return function ( euler ) {

			euler = euler || {};

			_z = ( euler.alpha || 0 ) * degToRad;
			_x = ( euler.beta || 0 ) * degToRad;
			_y = ( euler.gamma || 0 ) * degToRad;

			cX = Math.cos( _x );
			cY = Math.cos( _y );
			cZ = Math.cos( _z );
			sX = Math.sin( _x );
			sY = Math.sin( _y );
			sZ = Math.sin( _z );

			//
			// ZXY-ordered rotation matrix construction.
			//

			this.set(
				cZ * cY - sZ * sX * sY, // 1,1
				- cX * sZ,              // 1,2
				cY * sZ * sX + cZ * sY, // 1,3

				cY * sZ + cZ * sX * sY, // 2,1
				cZ * cX,                // 2,2
				sZ * sY - cZ * cY * sX, // 2,3

				- cX * sY,              // 3,1
				sX,                     // 3,2
				cX * cY                 // 3,3
			);

			this.normalize();

			return this;

		};

	})();

	this.setFromQuaternion = (function() {

		var sqw, sqx, sqy, sqz;

		return function( q ) {

			sqw = q.w * q.w;
			sqx = q.x * q.x;
			sqy = q.y * q.y;
			sqz = q.z * q.z;

			this.set(
				sqw + sqx - sqy - sqz,       // 1,1
				2 * (q.x * q.y - q.w * q.z), // 1,2
				2 * (q.x * q.z + q.w * q.y), // 1,3

				2 * (q.x * q.y + q.w * q.z), // 2,1
				sqw - sqx + sqy - sqz,       // 2,2
				2 * (q.y * q.z - q.w * q.x), // 2,3

				2 * (q.x * q.z - q.w * q.y), // 3,1
				2 * (q.y * q.z + q.w * q.x), // 3,2
				sqw - sqx - sqy + sqz        // 3,3
			);

			return this;

		};

	})();

	this.multiply = function ( m ) {

		outMatrix = FULLTILT.RotationMatrix.prototype.multiplyMatrices( this, m );
		this.copy( outMatrix );

		return this;

	};

	this.rotateX = function ( angle ) {

		outMatrix = FULLTILT.RotationMatrix.prototype.rotateByAxisAngle( this, [ 1, 0, 0 ], angle );
		this.copy( outMatrix );

		return this;

	};

	this.rotateY = function ( angle ) {

		outMatrix = FULLTILT.RotationMatrix.prototype.rotateByAxisAngle( this, [ 0, 1, 0 ], angle );
		this.copy( outMatrix );

		return this;

	};

	this.rotateZ = function ( angle ) {

		outMatrix = FULLTILT.RotationMatrix.prototype.rotateByAxisAngle( this, [ 0, 0, 1 ], angle );
		this.copy( outMatrix );

		return this;

	};

	this.normalize = function () {

		return FULLTILT.RotationMatrix.prototype.normalize( this );

	};

	// Initialize object values
	this.set( m11, m12, m13, m21, m22, m23, m31, m32, m33 );

};

FULLTILT.RotationMatrix.prototype = {

	constructor: FULLTILT.RotationMatrix,

	multiplyMatrices: function () {

		var matrix = new FULLTILT.RotationMatrix();

		var aE, bE;

		return function ( a, b ) {

			aE = a.elements;
			bE = b.elements;

			matrix.set(
				aE[0] * bE[0] + aE[1] * bE[3] + aE[2] * bE[6],
				aE[0] * bE[1] + aE[1] * bE[4] + aE[2] * bE[7],
				aE[0] * bE[2] + aE[1] * bE[5] + aE[2] * bE[8],

				aE[3] * bE[0] + aE[4] * bE[3] + aE[5] * bE[6],
				aE[3] * bE[1] + aE[4] * bE[4] + aE[5] * bE[7],
				aE[3] * bE[2] + aE[4] * bE[5] + aE[5] * bE[8],

				aE[6] * bE[0] + aE[7] * bE[3] + aE[8] * bE[6],
				aE[6] * bE[1] + aE[7] * bE[4] + aE[8] * bE[7],
				aE[6] * bE[2] + aE[7] * bE[5] + aE[8] * bE[8]
			);

			return matrix;

		};

	}(),

	normalize: function( matrix ) {

		var R = matrix.elements;

		// Calculate matrix determinant
		var determinant = R[0] * R[4] * R[8] - R[0] * R[5] * R[7] - R[1] * R[3] * R[8] + R[1] * R[5] * R[6] + R[2] * R[3] * R[7] - R[2] * R[4] * R[6];

		// Normalize matrix values
		R[0] /= determinant;
		R[1] /= determinant;
		R[2] /= determinant;
		R[3] /= determinant;
		R[4] /= determinant;
		R[5] /= determinant;
		R[6] /= determinant;
		R[7] /= determinant;
		R[8] /= determinant;

		matrix.elements = R;

		return matrix;

	},

	rotateByAxisAngle: function () {

		var outputMatrix = new FULLTILT.RotationMatrix();
		var transformMatrix = new FULLTILT.RotationMatrix();

		var sA, cA;
		var validAxis = false;

		return function ( targetRotationMatrix, axis, angle ) {

			transformMatrix.identity(); // reset transform matrix

			validAxis = false;

			sA = Math.sin( angle );
			cA = Math.cos( angle );

			if ( axis[ 0 ] === 1 && axis[ 1 ] === 0 && axis[ 2 ] === 0 ) { // x

				validAxis = true;

				transformMatrix.elements[4] = cA;
				transformMatrix.elements[5] = -sA;
				transformMatrix.elements[7] = sA;
				transformMatrix.elements[8] = cA;

	 		} else if ( axis[ 1 ] === 1 && axis[ 0 ] === 0 && axis[ 2 ] === 0 ) { // y

				validAxis = true;

				transformMatrix.elements[0] = cA;
				transformMatrix.elements[2] = sA;
				transformMatrix.elements[6] = -sA;
				transformMatrix.elements[8] = cA;

	 		} else if ( axis[ 2 ] === 1 && axis[ 0 ] === 0 && axis[ 1 ] === 0 ) { // z

				validAxis = true;

				transformMatrix.elements[0] = cA;
				transformMatrix.elements[1] = -sA;
				transformMatrix.elements[3] = sA;
				transformMatrix.elements[4] = cA;

	 		}

			if ( validAxis ) {

				outputMatrix = FULLTILT.RotationMatrix.prototype.multiplyMatrices( targetRotationMatrix, transformMatrix );

				outputMatrix = FULLTILT.RotationMatrix.prototype.normalize( outputMatrix );

			} else {

				outputMatrix = targetRotationMatrix;

			}

			return outputMatrix;

		};

	}()

};

////// FULLTILT.Euler //////

FULLTILT.Euler = function ( alpha, beta, gamma ) {

	this.set = function ( alpha, beta, gamma ) {

		this.alpha = alpha || 0;
		this.beta  = beta  || 0;
		this.gamma = gamma || 0;

	};

	this.copy = function ( inEuler ) {

		this.alpha = inEuler.alpha;
		this.beta  = inEuler.beta;
		this.gamma = inEuler.gamma;

	};

	this.setFromRotationMatrix = (function () {

		var R, _alpha, _beta, _gamma;

		return function ( matrix ) {

			R = matrix.elements;

			if (R[8] > 0) { // cos(beta) > 0

				_alpha = Math.atan2(-R[1], R[4]);
				_beta  = Math.asin(R[7]); // beta (-pi/2, pi/2)
				_gamma = Math.atan2(-R[6], R[8]); // gamma (-pi/2, pi/2)

			} else if (R[8] < 0) {  // cos(beta) < 0

				_alpha = Math.atan2(R[1], -R[4]);
				_beta  = -Math.asin(R[7]);
				_beta  += (_beta >= 0) ? - M_PI : M_PI; // beta [-pi,-pi/2) U (pi/2,pi)
				_gamma = Math.atan2(R[6], -R[8]); // gamma (-pi/2, pi/2)

			} else { // R[8] == 0

				if (R[6] > 0) {  // cos(gamma) == 0, cos(beta) > 0

					_alpha = Math.atan2(-R[1], R[4]);
					_beta  = Math.asin(R[7]); // beta [-pi/2, pi/2]
					_gamma = - M_PI_2; // gamma = -pi/2

				} else if (R[6] < 0) { // cos(gamma) == 0, cos(beta) < 0

					_alpha = Math.atan2(R[1], -R[4]);
					_beta  = -Math.asin(R[7]);
					_beta  += (_beta >= 0) ? - M_PI : M_PI; // beta [-pi,-pi/2) U (pi/2,pi)
					_gamma = - M_PI_2; // gamma = -pi/2

				} else { // R[6] == 0, cos(beta) == 0

					// gimbal lock discontinuity
					_alpha = Math.atan2(R[3], R[0]);
					_beta  = (R[7] > 0) ? M_PI_2 : - M_PI_2; // beta = +-pi/2
					_gamma = 0; // gamma = 0

				}

			}

			// alpha is in [-pi, pi], make sure it is in [0, 2*pi).
			if (_alpha < 0) {
				_alpha += M_2_PI; // alpha [0, 2*pi)
			}

			// Convert to degrees
			_alpha *= radToDeg;
			_beta  *= radToDeg;
			_gamma *= radToDeg;

			// apply derived euler angles to current object
			this.set( _alpha, _beta, _gamma );

		};

	})();

	this.setFromQuaternion = (function () {

		var _alpha, _beta, _gamma;

		return function ( q ) {

			var sqw = q.w * q.w;
			var sqx = q.x * q.x;
			var sqy = q.y * q.y;
			var sqz = q.z * q.z;

			var unitLength = sqw + sqx + sqy + sqz; // Normalised == 1, otherwise correction divisor.
			var wxyz = q.w * q.x + q.y * q.z;
			var epsilon = 1e-6; // rounding factor

			if (wxyz > (0.5 - epsilon) * unitLength) {

				_alpha = 2 * Math.atan2(q.y, q.w);
				_beta = M_PI_2;
				_gamma = 0;

			} else if (wxyz < (-0.5 + epsilon) * unitLength) {

				_alpha = -2 * Math.atan2(q.y, q.w);
				_beta = -M_PI_2;
				_gamma = 0;

			} else {

				var aX = sqw - sqx + sqy - sqz;
				var aY = 2 * (q.w * q.z - q.x * q.y);

				var gX = sqw - sqx - sqy + sqz;
				var gY = 2 * (q.w * q.y - q.x * q.z);

				if (gX > 0) {

					_alpha = Math.atan2(aY, aX);
					_beta  = Math.asin(2 * wxyz / unitLength);
					_gamma = Math.atan2(gY, gX);

				} else {

					_alpha = Math.atan2(-aY, -aX);
					_beta  = -Math.asin(2 * wxyz / unitLength);
					_beta  += _beta < 0 ? M_PI : - M_PI;
					_gamma = Math.atan2(-gY, -gX);

				}

			}

			// alpha is in [-pi, pi], make sure it is in [0, 2*pi).
			if (_alpha < 0) {
				_alpha += M_2_PI; // alpha [0, 2*pi)
			}

			// Convert to degrees
			_alpha *= radToDeg;
			_beta  *= radToDeg;
			_gamma *= radToDeg;

			// apply derived euler angles to current object
			this.set( _alpha, _beta, _gamma );

		};

	})();

	this.rotateX = function ( angle ) {

		FULLTILT.Euler.prototype.rotateByAxisAngle( this, [ 1, 0, 0 ], angle );

		return this;

	};

	this.rotateY = function ( angle ) {

		FULLTILT.Euler.prototype.rotateByAxisAngle( this, [ 0, 1, 0 ], angle );

		return this;

	};

	this.rotateZ = function ( angle ) {

		FULLTILT.Euler.prototype.rotateByAxisAngle( this, [ 0, 0, 1 ], angle );

		return this;

	};

	// Initialize object values
	this.set( alpha, beta, gamma );

};

FULLTILT.Euler.prototype = {

	constructor: FULLTILT.Euler,

	rotateByAxisAngle: function () {

		var _matrix = new FULLTILT.RotationMatrix();
		var outEuler;

		return function ( targetEuler, axis, angle ) {

			_matrix.setFromEuler( targetEuler );

			_matrix = FULLTILT.RotationMatrix.prototype.rotateByAxisAngle( _matrix, axis, angle );

			targetEuler.setFromRotationMatrix( _matrix );

			return targetEuler;

		};

	}()

};

///// FULLTILT.DeviceOrientation //////

FULLTILT.DeviceOrientation = function (options) {

	this.options = options || {}; // by default use UA deviceorientation 'type' ("game" on iOS, "world" on Android)

	var tries = 0;
	var maxTries = 200;
	var successCount = 0;
	var successThreshold = 10;

	this.alphaOffsetScreen = 0;
	this.alphaOffsetDevice = undefined;

	// Create a game-based deviceorientation object (initial alpha === 0 degrees)
	if (this.options.type === "game") {

		var setGameAlphaOffset = function(evt) {

			if (evt.alpha !== null) { // do regardless of whether 'evt.absolute' is also true
				this.alphaOffsetDevice = new FULLTILT.Euler(evt.alpha, 0, 0);
				this.alphaOffsetDevice.rotateZ( -screenOrientationAngle );

				// Discard first {successThreshold} responses while a better compass lock is found by UA
				if(++successCount >= successThreshold) {
					window.removeEventListener( 'deviceorientation', setGameAlphaOffset, false );
					return;
				}
			}

			if(++tries >= maxTries) {
				window.removeEventListener( 'deviceorientation', setGameAlphaOffset, false );
			}

		}.bind(this);

		window.addEventListener( 'deviceorientation', setGameAlphaOffset, false );

	// Create a compass-based deviceorientation object (initial alpha === compass degrees)
	} else if (this.options.type === "world") {

		var setCompassAlphaOffset = function(evt) {

			if (evt.absolute !== true && evt.webkitCompassAccuracy !== undefined && evt.webkitCompassAccuracy !== null && +evt.webkitCompassAccuracy >= 0 && +evt.webkitCompassAccuracy < 50) {
				this.alphaOffsetDevice = new FULLTILT.Euler(evt.webkitCompassHeading, 0, 0);
				this.alphaOffsetDevice.rotateZ( screenOrientationAngle );
				this.alphaOffsetScreen = screenOrientationAngle;

				// Discard first {successThreshold} responses while a better compass lock is found by UA
				if(++successCount >= successThreshold) {
					window.removeEventListener( 'deviceorientation', setCompassAlphaOffset, false );
					return;
				}
			}

			if(++tries >= maxTries) {
				window.removeEventListener( 'deviceorientation', setCompassAlphaOffset, false );
			}

		}.bind(this);

		window.addEventListener( 'deviceorientation', setCompassAlphaOffset, false );

	} // else... use whatever orientation system the UA provides ("game" on iOS, "world" on Android)

};

FULLTILT.DeviceOrientation.prototype = {

	constructor: FULLTILT.DeviceOrientation,

	start: function ( callback ) {

		if ( callback && Object.prototype.toString.call( callback ) == '[object Function]' ) {

			sensors.orientation.callbacks.push( callback );

		}

		if( !screenActive ) {

			if ( hasScreenOrientationAPI ) {

			window.screen.orientation.addEventListener( 'change', handleScreenOrientationChange, false );

			} else {

				window.addEventListener( 'orientationchange', handleScreenOrientationChange, false );

			}

		}

		if ( !sensors.orientation.active ) {

			window.addEventListener( 'deviceorientation', handleDeviceOrientationChange, false );

			sensors.orientation.active = true;

		}

	},

	stop: function () {

		if ( sensors.orientation.active ) {

			window.removeEventListener( 'deviceorientation', handleDeviceOrientationChange, false );

			sensors.orientation.active = false;

		}

	},

	listen: function( callback ) {

		this.start( callback );

	},

	getFixedFrameQuaternion: (function () {

		var euler = new FULLTILT.Euler();
		var matrix = new FULLTILT.RotationMatrix();
		var quaternion = new FULLTILT.Quaternion();

		return function() {

			var orientationData = sensors.orientation.data || { alpha: 0, beta: 0, gamma: 0 };

			var adjustedAlpha = orientationData.alpha;

			if (this.alphaOffsetDevice) {
				matrix.setFromEuler( this.alphaOffsetDevice );
				matrix.rotateZ( - this.alphaOffsetScreen );
				euler.setFromRotationMatrix( matrix );

				if (euler.alpha < 0) {
					euler.alpha += 360;
				}

				euler.alpha %= 360;

				adjustedAlpha -= euler.alpha;
			}

			euler.set(
				adjustedAlpha,
				orientationData.beta,
				orientationData.gamma
			);

			quaternion.setFromEuler( euler );

			return quaternion;

		};

	})(),

	getScreenAdjustedQuaternion: (function () {

		var quaternion;

		return function() {

			quaternion = this.getFixedFrameQuaternion();

			// Automatically apply screen orientation transform
			quaternion.rotateZ( - screenOrientationAngle );

			return quaternion;

		};

	})(),

	getFixedFrameMatrix: (function () {

		var euler = new FULLTILT.Euler();
		var matrix = new FULLTILT.RotationMatrix();

		return function () {

			var orientationData = sensors.orientation.data || { alpha: 0, beta: 0, gamma: 0 };

			var adjustedAlpha = orientationData.alpha;

			if (this.alphaOffsetDevice) {
				matrix.setFromEuler( this.alphaOffsetDevice );
				matrix.rotateZ( - this.alphaOffsetScreen );
				euler.setFromRotationMatrix( matrix );

				if (euler.alpha < 0) {
					euler.alpha += 360;
				}

				euler.alpha %= 360;

				adjustedAlpha -= euler.alpha;
			}

			euler.set(
				adjustedAlpha,
				orientationData.beta,
				orientationData.gamma
			);

			matrix.setFromEuler( euler );

			return matrix;

		};

	})(),

	getScreenAdjustedMatrix: (function () {

		var matrix;

		return function () {

			matrix = this.getFixedFrameMatrix();

			// Automatically apply screen orientation transform
			matrix.rotateZ( - screenOrientationAngle );

			return matrix;

		};

	})(),

	getFixedFrameEuler: (function () {

		var euler = new FULLTILT.Euler();
		var matrix;

		return function () {

			matrix = this.getFixedFrameMatrix();

			euler.setFromRotationMatrix( matrix );

			return euler;

		};

	})(),

	getScreenAdjustedEuler: (function () {

		var euler = new FULLTILT.Euler();
		var matrix;

		return function () {

			matrix = this.getScreenAdjustedMatrix();

			euler.setFromRotationMatrix( matrix );

			return euler;

		};

	})(),

	isAbsolute: function () {

		if ( sensors.orientation.data && sensors.orientation.data.absolute === true ) {
			return true;
		}

		return false;

	},

	getLastRawEventData: function () {

		return sensors.orientation.data || {};

	},

	ALPHA: 'alpha',
	BETA: 'beta',
	GAMMA: 'gamma'

};


///// FULLTILT.DeviceMotion //////

FULLTILT.DeviceMotion = function (options) {

	this.options = options || {}; // placeholder object since no options are currently supported

};

FULLTILT.DeviceMotion.prototype = {

	constructor: FULLTILT.DeviceMotion,

	start: function ( callback ) {

		if ( callback && Object.prototype.toString.call( callback ) == '[object Function]' ) {

			sensors.motion.callbacks.push( callback );

		}

		if( !screenActive ) {

			if ( hasScreenOrientationAPI ) {

				window.screen.orientation.addEventListener( 'change', handleScreenOrientationChange, false );

			} else {

				window.addEventListener( 'orientationchange', handleScreenOrientationChange, false );

			}

		}

		if ( !sensors.motion.active ) {

			window.addEventListener( 'devicemotion', handleDeviceMotionChange, false );

			sensors.motion.active = true;

		}

	},

	stop: function () {

		if ( sensors.motion.active ) {

			window.removeEventListener( 'devicemotion', handleDeviceMotionChange, false );

			sensors.motion.active = false;

		}

	},

	listen: function( callback ) {

		this.start( callback );

	},

	getScreenAdjustedAcceleration: function () {

		var accData = sensors.motion.data && sensors.motion.data.acceleration ? sensors.motion.data.acceleration : { x: 0, y: 0, z: 0 };
		var screenAccData = {};

		switch ( screenOrientationAngle ) {
			case SCREEN_ROTATION_90:
				screenAccData.x = - accData.y;
				screenAccData.y =   accData.x;
				break;
			case SCREEN_ROTATION_180:
				screenAccData.x = - accData.x;
				screenAccData.y = - accData.y;
				break;
			case SCREEN_ROTATION_270:
			case SCREEN_ROTATION_MINUS_90:
				screenAccData.x =   accData.y;
				screenAccData.y = - accData.x;
				break;
			default: // SCREEN_ROTATION_0
				screenAccData.x =   accData.x;
				screenAccData.y =   accData.y;
				break;
		}

		screenAccData.z = accData.z;

		return screenAccData;

	},

	getScreenAdjustedAccelerationIncludingGravity: function () {

		var accGData = sensors.motion.data && sensors.motion.data.accelerationIncludingGravity ? sensors.motion.data.accelerationIncludingGravity : { x: 0, y: 0, z: 0 };
		var screenAccGData = {};

		switch ( screenOrientationAngle ) {
			case SCREEN_ROTATION_90:
				screenAccGData.x = - accGData.y;
				screenAccGData.y =   accGData.x;
				break;
			case SCREEN_ROTATION_180:
				screenAccGData.x = - accGData.x;
				screenAccGData.y = - accGData.y;
				break;
			case SCREEN_ROTATION_270:
			case SCREEN_ROTATION_MINUS_90:
				screenAccGData.x =   accGData.y;
				screenAccGData.y = - accGData.x;
				break;
			default: // SCREEN_ROTATION_0
				screenAccGData.x =   accGData.x;
				screenAccGData.y =   accGData.y;
				break;
		}

		screenAccGData.z = accGData.z;

		return screenAccGData;

	},

	getScreenAdjustedRotationRate: function () {

		var rotRateData = sensors.motion.data && sensors.motion.data.rotationRate ? sensors.motion.data.rotationRate : { alpha: 0, beta: 0, gamma: 0 };
		var screenRotRateData = {};

		switch ( screenOrientationAngle ) {
			case SCREEN_ROTATION_90:
				screenRotRateData.beta  = - rotRateData.gamma;
				screenRotRateData.gamma =   rotRateData.beta;
				break;
			case SCREEN_ROTATION_180:
				screenRotRateData.beta  = - rotRateData.beta;
				screenRotRateData.gamma = - rotRateData.gamma;
				break;
			case SCREEN_ROTATION_270:
			case SCREEN_ROTATION_MINUS_90:
				screenRotRateData.beta  =   rotRateData.gamma;
				screenRotRateData.gamma = - rotRateData.beta;
				break;
			default: // SCREEN_ROTATION_0
				screenRotRateData.beta  =   rotRateData.beta;
				screenRotRateData.gamma =   rotRateData.gamma;
				break;
		}

		screenRotRateData.alpha = rotRateData.alpha;

		return screenRotRateData;

	},

	getLastRawEventData: function () {

		return sensors.motion.data || {};

	}

};


////// Attach FULLTILT to root DOM element //////

window.FULLTILT = FULLTILT;

})( window );

/***/ }),

/***/ "./src/Gyroscope.js":
/*!**************************!*\
  !*** ./src/Gyroscope.js ***!
  \**************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/classCallCheck */ "../../node_modules/@babel/runtime/helpers/classCallCheck.js");
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime/helpers/createClass */ "../../node_modules/@babel/runtime/helpers/createClass.js");
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _RAF__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./RAF */ "./src/RAF.js");




__webpack_require__(/*! @hughsk/fulltilt/dist/fulltilt */ "../../node_modules/@hughsk/fulltilt/dist/fulltilt.js");
/**
 * An utility singleton that returns a device's Gyroscope
 * data in the form of orientationX and orientationY.
 *
 * The values are updated every frame and the system stops updating
 * if it determines that the device does not have gyroscope capabilities.
 *
 * @class Gyroscope
 * @property {Number} orientationX
 * @property {Number} orientationY
 * @property {Boolean} enabled
 */


var Gyroscope = /*#__PURE__*/function () {
  /**
   * @ignore
   */
  function Gyroscope() {
    _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default()(this, Gyroscope);

    this.orientationX = 0;
    this.orientationY = 0;
    this.enabled = true;
    this._update = this._update.bind(this);
    _RAF__WEBPACK_IMPORTED_MODULE_2__["default"].add(this._update);
  }
  /**
   * @ignore
   */


  _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1___default()(Gyroscope, [{
    key: "_update",
    value: function _update() {
      var _this = this;

      var fulltiltPromise = new window.FULLTILT.getDeviceOrientation({
        type: 'game'
      }); // eslint-disable-line new-cap

      fulltiltPromise.then(function (deviceOrientation) {
        var euler = deviceOrientation.getScreenAdjustedEuler();
        _this.orientationX = euler.beta;
        _this.orientationY = euler.gamma;

        if (_this.orientationX > 90) {
          _this.orientationY = -_this.orientationY;
        }

        return null; // console.log(this.orientationX, this.orientationY)
      }).catch(function (message) {
        console.warn('Disabling Gyroscope, reason:', message);
        _this.enabled = false;
        _RAF__WEBPACK_IMPORTED_MODULE_2__["default"].remove(_this._update);
      });
    }
  }]);

  return Gyroscope;
}();
/**
 * @ignore
 */


/* harmony default export */ __webpack_exports__["default"] = (Gyroscope);

/***/ }),

/***/ "./src/RAF.js":
/*!********************!*\
  !*** ./src/RAF.js ***!
  \********************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/classCallCheck */ "../../node_modules/@babel/runtime/helpers/classCallCheck.js");
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime/helpers/createClass */ "../../node_modules/@babel/runtime/helpers/createClass.js");
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1__);



/**
 * An utility singleton that holds all requestAnimationFrame subscribers
 * and calls them in sequence each frame.
 *
 * @class RAF
 * @example
 *
 * import RAF from 'js-utils'
 *
 * constructor () {
 *  RAF.add(this.onUpdate)
 * }
 *
 * onUpdate () {
 *  // do stuff
 * }
 *
 * onDestroy () {
 *  RAF.remove(this.onUpdate)
 * }
 */
var RAF = /*#__PURE__*/function () {
  function RAF() {
    _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default()(this, RAF);

    this.listeners = [];
    this._update = this._update.bind(this);

    this._update();
  }
  /**
   * Adds a subscriber to be called at each requestAnimationFrame
   *
   * @param {Function} listener A subscriber function
   */


  _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1___default()(RAF, [{
    key: "add",
    value: function add(listener) {
      var index = this.listeners.indexOf(listener);

      if (index === -1) {
        this.listeners.push(listener);
      }
    }
    /**
     * Removes a subscriber from requestAnimationFrame
     *
     * @param {Function} listener A subscriber function
     */

  }, {
    key: "remove",
    value: function remove(listener) {
      var index = this.listeners.indexOf(listener);

      if (index !== -1) {
        this.listeners.splice(index, 1);
      }
    }
    /**
     * @ignore
     */

  }, {
    key: "_update",
    value: function _update() {
      for (var i = 0; i < this.listeners.length; i++) {
        this.listeners[i]();
      }

      window.requestAnimationFrame(this._update);
    }
  }]);

  return RAF;
}();
/**
 * @ignore
 */


/* harmony default export */ __webpack_exports__["default"] = (new RAF());

/***/ })

/******/ });
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9qcy11dGlscy93ZWJwYWNrL3VuaXZlcnNhbE1vZHVsZURlZmluaXRpb24iLCJ3ZWJwYWNrOi8vanMtdXRpbHMvd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vanMtdXRpbHMvRDovVXNlcnMvZGFuaS9EZXNrdG9wL0Btb25vZ3JpZC9ub2RlX21vZHVsZXMvQGJhYmVsL3J1bnRpbWUvaGVscGVycy9jbGFzc0NhbGxDaGVjay5qcyIsIndlYnBhY2s6Ly9qcy11dGlscy9EOi9Vc2Vycy9kYW5pL0Rlc2t0b3AvQG1vbm9ncmlkL25vZGVfbW9kdWxlcy9AYmFiZWwvcnVudGltZS9oZWxwZXJzL2NyZWF0ZUNsYXNzLmpzIiwid2VicGFjazovL2pzLXV0aWxzL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL0BodWdoc2svZnVsbHRpbHQvZGlzdC9mdWxsdGlsdC5qcyIsIndlYnBhY2s6Ly9qcy11dGlscy8uL3NyYy9HeXJvc2NvcGUuanMiLCJ3ZWJwYWNrOi8vanMtdXRpbHMvLi9zcmMvUkFGLmpzIl0sIm5hbWVzIjpbInJlcXVpcmUiLCJHeXJvc2NvcGUiLCJvcmllbnRhdGlvblgiLCJvcmllbnRhdGlvblkiLCJlbmFibGVkIiwiX3VwZGF0ZSIsImJpbmQiLCJSQUYiLCJhZGQiLCJmdWxsdGlsdFByb21pc2UiLCJ3aW5kb3ciLCJGVUxMVElMVCIsImdldERldmljZU9yaWVudGF0aW9uIiwidHlwZSIsInRoZW4iLCJkZXZpY2VPcmllbnRhdGlvbiIsImV1bGVyIiwiZ2V0U2NyZWVuQWRqdXN0ZWRFdWxlciIsImJldGEiLCJnYW1tYSIsImNhdGNoIiwibWVzc2FnZSIsImNvbnNvbGUiLCJ3YXJuIiwicmVtb3ZlIiwibGlzdGVuZXJzIiwibGlzdGVuZXIiLCJpbmRleCIsImluZGV4T2YiLCJwdXNoIiwic3BsaWNlIiwiaSIsImxlbmd0aCIsInJlcXVlc3RBbmltYXRpb25GcmFtZSJdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQztBQUNELE87UUNWQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTs7O1FBR0E7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLDBDQUEwQyxnQ0FBZ0M7UUFDMUU7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSx3REFBd0Qsa0JBQWtCO1FBQzFFO1FBQ0EsaURBQWlELGNBQWM7UUFDL0Q7O1FBRUE7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBLHlDQUF5QyxpQ0FBaUM7UUFDMUUsZ0hBQWdILG1CQUFtQixFQUFFO1FBQ3JJO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0EsMkJBQTJCLDBCQUEwQixFQUFFO1FBQ3ZELGlDQUFpQyxlQUFlO1FBQ2hEO1FBQ0E7UUFDQTs7UUFFQTtRQUNBLHNEQUFzRCwrREFBK0Q7O1FBRXJIO1FBQ0E7OztRQUdBO1FBQ0E7Ozs7Ozs7Ozs7OztBQ2xGQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLGlDOzs7Ozs7Ozs7OztBQ05BO0FBQ0EsaUJBQWlCLGtCQUFrQjtBQUNuQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLDhCOzs7Ozs7Ozs7OztBQ2hCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxFQUFFO0FBQ0Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLFFBQVE7QUFDUjtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxLQUFLOztBQUVMOztBQUVBLEtBQUs7O0FBRUw7O0FBRUE7O0FBRUEsSUFBSTs7QUFFSjs7QUFFQTs7QUFFQSxFQUFFOztBQUVGOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLEVBQUU7O0FBRUY7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLEdBQUc7O0FBRUg7QUFDQTs7QUFFQSxHQUFHOztBQUVILEVBQUU7O0FBRUY7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsR0FBRzs7QUFFSDtBQUNBOztBQUVBLEdBQUc7O0FBRUgsRUFBRTs7QUFFRjs7QUFFQTs7O0FBR0E7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUE7QUFDQTtBQUNBOztBQUVBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxFQUFFOztBQUVGOztBQUVBOztBQUVBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQSxFQUFFOztBQUVGOztBQUVBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBOztBQUVBOztBQUVBOztBQUVBOztBQUVBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQSxFQUFFOztBQUVGOztBQUVBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLEdBQUc7O0FBRUg7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUEsRUFBRTs7QUFFRjs7QUFFQTtBQUNBOztBQUVBOztBQUVBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUEsRUFBRTs7QUFFRjs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUE7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsRUFBRTs7QUFFRjs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUEsRUFBRTs7QUFFRjs7QUFFQTtBQUNBOztBQUVBOztBQUVBOztBQUVBOztBQUVBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUEsRUFBRTs7QUFFRjs7QUFFQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBOztBQUVBLEVBQUU7O0FBRUY7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBOztBQUVBLDhCQUE4Qjs7QUFFOUI7O0FBRUE7QUFDQTs7QUFFQSxpRUFBaUU7O0FBRWpFOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLEtBQUssb0VBQW9FOztBQUV6RTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxLQUFLLG9FQUFvRTs7QUFFekU7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsSUFBSTs7QUFFSjs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxFQUFFOztBQUVGOztBQUVBOztBQUVBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsa0JBQWtCOztBQUVsQjtBQUNBLDZCQUE2QjtBQUM3QixxQ0FBcUM7O0FBRXJDLElBQUkscUJBQXFCOztBQUV6QjtBQUNBO0FBQ0EsMkNBQTJDO0FBQzNDLHFDQUFxQzs7QUFFckMsSUFBSSxPQUFPOztBQUVYLG1CQUFtQjs7QUFFbkI7QUFDQSw4QkFBOEI7QUFDOUIsdUJBQXVCOztBQUV2QixLQUFLLHFCQUFxQjs7QUFFMUI7QUFDQTtBQUNBLDRDQUE0QztBQUM1Qyx1QkFBdUI7O0FBRXZCLEtBQUssT0FBTzs7QUFFWjtBQUNBO0FBQ0EsNkNBQTZDO0FBQzdDLGdCQUFnQjs7QUFFaEI7O0FBRUE7O0FBRUE7QUFDQTtBQUNBLHFCQUFxQjtBQUNyQjs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBOztBQUVBLEVBQUU7O0FBRUY7O0FBRUE7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsMENBQTBDO0FBQzFDO0FBQ0Esc0JBQXNCOztBQUV0Qjs7QUFFQTtBQUNBO0FBQ0E7O0FBRUEsSUFBSTs7QUFFSjtBQUNBO0FBQ0E7O0FBRUEsSUFBSTs7QUFFSjtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBOztBQUVBLEtBQUs7O0FBRUw7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUE7QUFDQTtBQUNBLHFCQUFxQjtBQUNyQjs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBOztBQUVBLEVBQUU7O0FBRUY7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLEVBQUU7O0FBRUY7O0FBRUE7O0FBRUE7O0FBRUEsOEJBQThCOztBQUU5QjtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7O0FBRUEsNEJBQTRCO0FBQzVCO0FBQ0E7O0FBRUEsc0JBQXNCLGlCQUFpQjtBQUN2QztBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQSxHQUFHOztBQUVIOztBQUVBO0FBQ0EsRUFBRTs7QUFFRjs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxzQkFBc0IsaUJBQWlCO0FBQ3ZDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBLEdBQUc7O0FBRUg7O0FBRUEsRUFBRTs7QUFFRjs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxJQUFJOztBQUVKOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLEVBQUU7O0FBRUY7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsRUFBRTs7QUFFRjs7QUFFQTs7QUFFQSxFQUFFOztBQUVGOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQSxzREFBc0Q7O0FBRXREOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsRUFBRTs7QUFFRjs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBOztBQUVBOztBQUVBOztBQUVBLEVBQUU7O0FBRUY7O0FBRUE7QUFDQTs7QUFFQTs7QUFFQSxzREFBc0Q7O0FBRXREOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsRUFBRTs7QUFFRjs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBOztBQUVBOztBQUVBOztBQUVBLEVBQUU7O0FBRUY7O0FBRUE7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxFQUFFOztBQUVGOztBQUVBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsRUFBRTs7QUFFRjs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUEsRUFBRTs7QUFFRjs7QUFFQTs7QUFFQSxFQUFFOztBQUVGO0FBQ0E7QUFDQTs7QUFFQTs7O0FBR0E7O0FBRUE7O0FBRUEsOEJBQThCOztBQUU5Qjs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxJQUFJOztBQUVKOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLEVBQUU7O0FBRUY7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsRUFBRTs7QUFFRjs7QUFFQTs7QUFFQSxFQUFFOztBQUVGOztBQUVBLDhHQUE4RztBQUM5Rzs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQSxFQUFFOztBQUVGOztBQUVBLCtJQUErSTtBQUMvSTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQSxFQUFFOztBQUVGOztBQUVBLGtIQUFrSDtBQUNsSDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQSxFQUFFOztBQUVGOztBQUVBOztBQUVBOztBQUVBOzs7QUFHQTs7QUFFQTs7QUFFQSxDQUFDLFk7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDdDJDRDs7QUFDQUEsbUJBQU8sQ0FBQyw0RkFBRCxDQUFQO0FBQ0E7Ozs7Ozs7Ozs7Ozs7O0lBWU1DLFM7QUFDSjs7O0FBR0EsdUJBQWU7QUFBQTs7QUFDYixTQUFLQyxZQUFMLEdBQW9CLENBQXBCO0FBQ0EsU0FBS0MsWUFBTCxHQUFvQixDQUFwQjtBQUNBLFNBQUtDLE9BQUwsR0FBZSxJQUFmO0FBQ0EsU0FBS0MsT0FBTCxHQUFlLEtBQUtBLE9BQUwsQ0FBYUMsSUFBYixDQUFrQixJQUFsQixDQUFmO0FBQ0FDLGdEQUFHLENBQUNDLEdBQUosQ0FBUSxLQUFLSCxPQUFiO0FBQ0Q7QUFFRDs7Ozs7Ozs4QkFHVztBQUFBOztBQUNULFVBQU1JLGVBQWUsR0FBRyxJQUFJQyxNQUFNLENBQUNDLFFBQVAsQ0FBZ0JDLG9CQUFwQixDQUF5QztBQUFFQyxZQUFJLEVBQUU7QUFBUixPQUF6QyxDQUF4QixDQURTLENBQzBFOztBQUVuRkoscUJBQWUsQ0FBQ0ssSUFBaEIsQ0FBcUIsVUFBQ0MsaUJBQUQsRUFBdUI7QUFDMUMsWUFBTUMsS0FBSyxHQUFHRCxpQkFBaUIsQ0FBQ0Usc0JBQWxCLEVBQWQ7QUFFQSxhQUFJLENBQUNmLFlBQUwsR0FBb0JjLEtBQUssQ0FBQ0UsSUFBMUI7QUFDQSxhQUFJLENBQUNmLFlBQUwsR0FBb0JhLEtBQUssQ0FBQ0csS0FBMUI7O0FBRUEsWUFBSSxLQUFJLENBQUNqQixZQUFMLEdBQW9CLEVBQXhCLEVBQTRCO0FBQzFCLGVBQUksQ0FBQ0MsWUFBTCxHQUFvQixDQUFDLEtBQUksQ0FBQ0EsWUFBMUI7QUFDRDs7QUFDRCxlQUFPLElBQVAsQ0FUMEMsQ0FVMUM7QUFDRCxPQVhELEVBV0dpQixLQVhILENBV1MsVUFBQ0MsT0FBRCxFQUFhO0FBQ3BCQyxlQUFPLENBQUNDLElBQVIsQ0FBYSw4QkFBYixFQUE2Q0YsT0FBN0M7QUFDQSxhQUFJLENBQUNqQixPQUFMLEdBQWUsS0FBZjtBQUNBRyxvREFBRyxDQUFDaUIsTUFBSixDQUFXLEtBQUksQ0FBQ25CLE9BQWhCO0FBQ0QsT0FmRDtBQWdCRDs7Ozs7QUFFSDs7Ozs7QUFHZUosd0VBQWYsRTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNyREE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztJQXFCTU0sRztBQUNKLGlCQUFlO0FBQUE7O0FBQ2IsU0FBS2tCLFNBQUwsR0FBaUIsRUFBakI7QUFDQSxTQUFLcEIsT0FBTCxHQUFlLEtBQUtBLE9BQUwsQ0FBYUMsSUFBYixDQUFrQixJQUFsQixDQUFmOztBQUNBLFNBQUtELE9BQUw7QUFDRDtBQUVEOzs7Ozs7Ozs7d0JBS0txQixRLEVBQVU7QUFDYixVQUFNQyxLQUFLLEdBQUcsS0FBS0YsU0FBTCxDQUFlRyxPQUFmLENBQXVCRixRQUF2QixDQUFkOztBQUNBLFVBQUlDLEtBQUssS0FBSyxDQUFDLENBQWYsRUFBa0I7QUFDaEIsYUFBS0YsU0FBTCxDQUFlSSxJQUFmLENBQW9CSCxRQUFwQjtBQUNEO0FBQ0Y7QUFFRDs7Ozs7Ozs7MkJBS1FBLFEsRUFBVTtBQUNoQixVQUFNQyxLQUFLLEdBQUcsS0FBS0YsU0FBTCxDQUFlRyxPQUFmLENBQXVCRixRQUF2QixDQUFkOztBQUNBLFVBQUlDLEtBQUssS0FBSyxDQUFDLENBQWYsRUFBa0I7QUFDaEIsYUFBS0YsU0FBTCxDQUFlSyxNQUFmLENBQXNCSCxLQUF0QixFQUE2QixDQUE3QjtBQUNEO0FBQ0Y7QUFFRDs7Ozs7OzhCQUdXO0FBQ1QsV0FBSyxJQUFJSSxDQUFDLEdBQUcsQ0FBYixFQUFnQkEsQ0FBQyxHQUFHLEtBQUtOLFNBQUwsQ0FBZU8sTUFBbkMsRUFBMkNELENBQUMsRUFBNUMsRUFBZ0Q7QUFDOUMsYUFBS04sU0FBTCxDQUFlTSxDQUFmO0FBQ0Q7O0FBQ0RyQixZQUFNLENBQUN1QixxQkFBUCxDQUE2QixLQUFLNUIsT0FBbEM7QUFDRDs7Ozs7QUFFSDs7Ozs7QUFHZSxtRUFBSUUsR0FBSixFQUFmLEUiLCJmaWxlIjoiR3lyb3Njb3BlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiKGZ1bmN0aW9uIHdlYnBhY2tVbml2ZXJzYWxNb2R1bGVEZWZpbml0aW9uKHJvb3QsIGZhY3RvcnkpIHtcblx0aWYodHlwZW9mIGV4cG9ydHMgPT09ICdvYmplY3QnICYmIHR5cGVvZiBtb2R1bGUgPT09ICdvYmplY3QnKVxuXHRcdG1vZHVsZS5leHBvcnRzID0gZmFjdG9yeSgpO1xuXHRlbHNlIGlmKHR5cGVvZiBkZWZpbmUgPT09ICdmdW5jdGlvbicgJiYgZGVmaW5lLmFtZClcblx0XHRkZWZpbmUoXCJqcy11dGlsc1wiLCBbXSwgZmFjdG9yeSk7XG5cdGVsc2UgaWYodHlwZW9mIGV4cG9ydHMgPT09ICdvYmplY3QnKVxuXHRcdGV4cG9ydHNbXCJqcy11dGlsc1wiXSA9IGZhY3RvcnkoKTtcblx0ZWxzZVxuXHRcdHJvb3RbXCJqcy11dGlsc1wiXSA9IGZhY3RvcnkoKTtcbn0pKHR5cGVvZiBzZWxmICE9PSAndW5kZWZpbmVkJyA/IHNlbGYgOiB0aGlzLCBmdW5jdGlvbigpIHtcbnJldHVybiAiLCIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9uIGZvciBoYXJtb255IGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuIFx0XHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIG5hbWUpKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIG5hbWUsIHsgZW51bWVyYWJsZTogdHJ1ZSwgZ2V0OiBnZXR0ZXIgfSk7XG4gXHRcdH1cbiBcdH07XG5cbiBcdC8vIGRlZmluZSBfX2VzTW9kdWxlIG9uIGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uciA9IGZ1bmN0aW9uKGV4cG9ydHMpIHtcbiBcdFx0aWYodHlwZW9mIFN5bWJvbCAhPT0gJ3VuZGVmaW5lZCcgJiYgU3ltYm9sLnRvU3RyaW5nVGFnKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFN5bWJvbC50b1N0cmluZ1RhZywgeyB2YWx1ZTogJ01vZHVsZScgfSk7XG4gXHRcdH1cbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdfX2VzTW9kdWxlJywgeyB2YWx1ZTogdHJ1ZSB9KTtcbiBcdH07XG5cbiBcdC8vIGNyZWF0ZSBhIGZha2UgbmFtZXNwYWNlIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDE6IHZhbHVlIGlzIGEgbW9kdWxlIGlkLCByZXF1aXJlIGl0XG4gXHQvLyBtb2RlICYgMjogbWVyZ2UgYWxsIHByb3BlcnRpZXMgb2YgdmFsdWUgaW50byB0aGUgbnNcbiBcdC8vIG1vZGUgJiA0OiByZXR1cm4gdmFsdWUgd2hlbiBhbHJlYWR5IG5zIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDh8MTogYmVoYXZlIGxpa2UgcmVxdWlyZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy50ID0gZnVuY3Rpb24odmFsdWUsIG1vZGUpIHtcbiBcdFx0aWYobW9kZSAmIDEpIHZhbHVlID0gX193ZWJwYWNrX3JlcXVpcmVfXyh2YWx1ZSk7XG4gXHRcdGlmKG1vZGUgJiA4KSByZXR1cm4gdmFsdWU7XG4gXHRcdGlmKChtb2RlICYgNCkgJiYgdHlwZW9mIHZhbHVlID09PSAnb2JqZWN0JyAmJiB2YWx1ZSAmJiB2YWx1ZS5fX2VzTW9kdWxlKSByZXR1cm4gdmFsdWU7XG4gXHRcdHZhciBucyA9IE9iamVjdC5jcmVhdGUobnVsbCk7XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18ucihucyk7XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShucywgJ2RlZmF1bHQnLCB7IGVudW1lcmFibGU6IHRydWUsIHZhbHVlOiB2YWx1ZSB9KTtcbiBcdFx0aWYobW9kZSAmIDIgJiYgdHlwZW9mIHZhbHVlICE9ICdzdHJpbmcnKSBmb3IodmFyIGtleSBpbiB2YWx1ZSkgX193ZWJwYWNrX3JlcXVpcmVfXy5kKG5zLCBrZXksIGZ1bmN0aW9uKGtleSkgeyByZXR1cm4gdmFsdWVba2V5XTsgfS5iaW5kKG51bGwsIGtleSkpO1xuIFx0XHRyZXR1cm4gbnM7XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gXCIuL3NyYy9HeXJvc2NvcGUuanNcIik7XG4iLCJmdW5jdGlvbiBfY2xhc3NDYWxsQ2hlY2soaW5zdGFuY2UsIENvbnN0cnVjdG9yKSB7XG4gIGlmICghKGluc3RhbmNlIGluc3RhbmNlb2YgQ29uc3RydWN0b3IpKSB7XG4gICAgdGhyb3cgbmV3IFR5cGVFcnJvcihcIkNhbm5vdCBjYWxsIGEgY2xhc3MgYXMgYSBmdW5jdGlvblwiKTtcbiAgfVxufVxuXG5tb2R1bGUuZXhwb3J0cyA9IF9jbGFzc0NhbGxDaGVjazsiLCJmdW5jdGlvbiBfZGVmaW5lUHJvcGVydGllcyh0YXJnZXQsIHByb3BzKSB7XG4gIGZvciAodmFyIGkgPSAwOyBpIDwgcHJvcHMubGVuZ3RoOyBpKyspIHtcbiAgICB2YXIgZGVzY3JpcHRvciA9IHByb3BzW2ldO1xuICAgIGRlc2NyaXB0b3IuZW51bWVyYWJsZSA9IGRlc2NyaXB0b3IuZW51bWVyYWJsZSB8fCBmYWxzZTtcbiAgICBkZXNjcmlwdG9yLmNvbmZpZ3VyYWJsZSA9IHRydWU7XG4gICAgaWYgKFwidmFsdWVcIiBpbiBkZXNjcmlwdG9yKSBkZXNjcmlwdG9yLndyaXRhYmxlID0gdHJ1ZTtcbiAgICBPYmplY3QuZGVmaW5lUHJvcGVydHkodGFyZ2V0LCBkZXNjcmlwdG9yLmtleSwgZGVzY3JpcHRvcik7XG4gIH1cbn1cblxuZnVuY3Rpb24gX2NyZWF0ZUNsYXNzKENvbnN0cnVjdG9yLCBwcm90b1Byb3BzLCBzdGF0aWNQcm9wcykge1xuICBpZiAocHJvdG9Qcm9wcykgX2RlZmluZVByb3BlcnRpZXMoQ29uc3RydWN0b3IucHJvdG90eXBlLCBwcm90b1Byb3BzKTtcbiAgaWYgKHN0YXRpY1Byb3BzKSBfZGVmaW5lUHJvcGVydGllcyhDb25zdHJ1Y3Rvciwgc3RhdGljUHJvcHMpO1xuICByZXR1cm4gQ29uc3RydWN0b3I7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gX2NyZWF0ZUNsYXNzOyIsIi8qKlxuICpcbiAqIEZVTEwgVElMVFxuICogaHR0cDovL2dpdGh1Yi5jb20vcmljaHRyL0Z1bGwtVGlsdFxuICpcbiAqIEEgc3RhbmRhbG9uZSBEZXZpY2VPcmllbnRhdGlvbiArIERldmljZU1vdGlvbiBKYXZhU2NyaXB0IGxpYnJhcnkgdGhhdFxuICogbm9ybWFsaXNlcyBvcmllbnRhdGlvbiBzZW5zb3IgaW5wdXQsIGFwcGxpZXMgcmVsZXZhbnQgc2NyZWVuIG9yaWVudGF0aW9uXG4gKiB0cmFuc2Zvcm1zLCByZXR1cm5zIEV1bGVyIEFuZ2xlLCBRdWF0ZXJuaW9uIGFuZCBSb3RhdGlvblxuICogTWF0cml4IHJlcHJlc2VudGF0aW9ucyBiYWNrIHRvIHdlYiBkZXZlbG9wZXJzIGFuZCBwcm92aWRlcyBjb252ZXJzaW9uXG4gKiBiZXR3ZWVuIGFsbCBzdXBwb3J0ZWQgb3JpZW50YXRpb24gcmVwcmVzZW50YXRpb24gdHlwZXMuXG4gKlxuICogQ29weXJpZ2h0OiAyMDE0IFJpY2ggVGliYmV0dFxuICogTGljZW5zZTogICBNSVRcbiAqXG4gKi9cblxuKGZ1bmN0aW9uICggd2luZG93ICkge1xuXG4vLyBPbmx5IGluaXRpYWxpemUgdGhlIEZVTExUSUxUIEFQSSBpZiBpdCBpcyBub3QgYWxyZWFkeSBhdHRhY2hlZCB0byB0aGUgRE9NXG5pZiAoIHdpbmRvdy5GVUxMVElMVCAhPT0gdW5kZWZpbmVkICYmIHdpbmRvdy5GVUxMVElMVCAhPT0gbnVsbCApIHtcblx0cmV0dXJuO1xufVxuXG52YXIgTV9QSSAgID0gTWF0aC5QSTtcbnZhciBNX1BJXzIgPSBNX1BJIC8gMjtcbnZhciBNXzJfUEkgPSAyICogTV9QSTtcblxuLy8gRGVncmVlIHRvIFJhZGlhbiBjb252ZXJzaW9uXG52YXIgZGVnVG9SYWQgPSBNX1BJIC8gMTgwO1xudmFyIHJhZFRvRGVnID0gMTgwIC8gTV9QSTtcblxuLy8gSW50ZXJuYWwgZGV2aWNlIG9yaWVudGF0aW9uICsgbW90aW9uIHZhcmlhYmxlc1xudmFyIHNlbnNvcnMgPSB7XG5cdFwib3JpZW50YXRpb25cIjoge1xuXHRcdGFjdGl2ZTogICAgZmFsc2UsXG5cdFx0Y2FsbGJhY2tzOiBbXSxcblx0XHRkYXRhOiAgICAgIHVuZGVmaW5lZFxuXHR9LFxuXHRcIm1vdGlvblwiOiB7XG5cdFx0YWN0aXZlOiAgICBmYWxzZSxcblx0XHRjYWxsYmFja3M6IFtdLFxuXHRcdGRhdGE6ICAgICAgdW5kZWZpbmVkXG5cdH1cbn07XG52YXIgc2NyZWVuQWN0aXZlID0gZmFsc2U7XG5cbi8vIEludGVybmFsIHNjcmVlbiBvcmllbnRhdGlvbiB2YXJpYWJsZXNcbnZhciBoYXNTY3JlZW5PcmllbnRhdGlvbkFQSSA9IHdpbmRvdy5zY3JlZW4gJiYgd2luZG93LnNjcmVlbi5vcmllbnRhdGlvbiAmJiB3aW5kb3cuc2NyZWVuLm9yaWVudGF0aW9uLmFuZ2xlICE9PSB1bmRlZmluZWQgJiYgd2luZG93LnNjcmVlbi5vcmllbnRhdGlvbi5hbmdsZSAhPT0gbnVsbCA/IHRydWUgOiBmYWxzZTtcbnZhciBzY3JlZW5PcmllbnRhdGlvbkFuZ2xlID0gKCBoYXNTY3JlZW5PcmllbnRhdGlvbkFQSSA/IHdpbmRvdy5zY3JlZW4ub3JpZW50YXRpb24uYW5nbGUgOiAoIHdpbmRvdy5vcmllbnRhdGlvbiB8fCAwICkgKSAqIGRlZ1RvUmFkO1xuXG52YXIgU0NSRUVOX1JPVEFUSU9OXzAgICAgICAgID0gMCxcbiAgICBTQ1JFRU5fUk9UQVRJT05fOTAgICAgICAgPSBNX1BJXzIsXG4gICAgU0NSRUVOX1JPVEFUSU9OXzE4MCAgICAgID0gTV9QSSxcbiAgICBTQ1JFRU5fUk9UQVRJT05fMjcwICAgICAgPSBNXzJfUEkgLyAzLFxuICAgIFNDUkVFTl9ST1RBVElPTl9NSU5VU185MCA9IC0gTV9QSV8yO1xuXG4vLyBNYXRoLnNpZ24gcG9seWZpbGxcbmZ1bmN0aW9uIHNpZ24oeCkge1xuXHR4ID0gK3g7IC8vIGNvbnZlcnQgdG8gYSBudW1iZXJcblx0aWYgKHggPT09IDAgfHwgaXNOYU4oeCkpXG5cdFx0cmV0dXJuIHg7XG5cdHJldHVybiB4ID4gMCA/IDEgOiAtMTtcbn1cblxuLy8vLy8gUHJvbWlzZS1iYXNlZCBTZW5zb3IgRGF0YSBjaGVja2VyIC8vLy8vL1xuXG5mdW5jdGlvbiBTZW5zb3JDaGVjayhzZW5zb3JSb290T2JqKSB7XG5cblx0dmFyIHByb21pc2UgPSBuZXcgUHJvbWlzZShmdW5jdGlvbihyZXNvbHZlLCByZWplY3QpIHtcblxuXHRcdHZhciBydW5DaGVjayA9IGZ1bmN0aW9uICh0cmllcykge1xuXG5cdFx0XHRzZXRUaW1lb3V0KGZ1bmN0aW9uKCkge1xuXG5cdFx0XHRcdGlmIChzZW5zb3JSb290T2JqICYmIHNlbnNvclJvb3RPYmouZGF0YSkge1xuXG5cdFx0XHRcdFx0cmVzb2x2ZSgpO1xuXG5cdFx0XHRcdH0gZWxzZSBpZiAodHJpZXMgPj0gMjApIHtcblxuXHRcdFx0XHRcdHJlamVjdCgpO1xuXG5cdFx0XHRcdH0gZWxzZSB7XG5cblx0XHRcdFx0XHRydW5DaGVjaygrK3RyaWVzKTtcblxuXHRcdFx0XHR9XG5cblx0XHRcdH0sIDUwKTtcblxuXHRcdH07XG5cblx0XHRydW5DaGVjaygwKTtcblxuXHR9KTtcblxuXHRyZXR1cm4gcHJvbWlzZTtcblxufVxuXG4vLy8vLy8gSW50ZXJuYWwgRXZlbnQgSGFuZGxlcnMgLy8vLy8vXG5cbmZ1bmN0aW9uIGhhbmRsZVNjcmVlbk9yaWVudGF0aW9uQ2hhbmdlICgpIHtcblxuXHRpZiAoIGhhc1NjcmVlbk9yaWVudGF0aW9uQVBJICkge1xuXG5cdFx0c2NyZWVuT3JpZW50YXRpb25BbmdsZSA9ICggd2luZG93LnNjcmVlbi5vcmllbnRhdGlvbi5hbmdsZSB8fCAwICkgKiBkZWdUb1JhZDtcblxuXHR9IGVsc2Uge1xuXG5cdFx0c2NyZWVuT3JpZW50YXRpb25BbmdsZSA9ICggd2luZG93Lm9yaWVudGF0aW9uIHx8IDAgKSAqIGRlZ1RvUmFkO1xuXG5cdH1cblxufVxuXG5mdW5jdGlvbiBoYW5kbGVEZXZpY2VPcmllbnRhdGlvbkNoYW5nZSAoIGV2ZW50ICkge1xuXG5cdHNlbnNvcnMub3JpZW50YXRpb24uZGF0YSA9IGV2ZW50O1xuXG5cdC8vIEZpcmUgZXZlcnkgY2FsbGJhY2sgZnVuY3Rpb24gZWFjaCB0aW1lIGRldmljZW9yaWVudGF0aW9uIGlzIHVwZGF0ZWRcblx0Zm9yICggdmFyIGkgaW4gc2Vuc29ycy5vcmllbnRhdGlvbi5jYWxsYmFja3MgKSB7XG5cblx0XHRzZW5zb3JzLm9yaWVudGF0aW9uLmNhbGxiYWNrc1sgaSBdLmNhbGwoIHRoaXMgKTtcblxuXHR9XG5cbn1cblxuZnVuY3Rpb24gaGFuZGxlRGV2aWNlTW90aW9uQ2hhbmdlICggZXZlbnQgKSB7XG5cblx0c2Vuc29ycy5tb3Rpb24uZGF0YSA9IGV2ZW50O1xuXG5cdC8vIEZpcmUgZXZlcnkgY2FsbGJhY2sgZnVuY3Rpb24gZWFjaCB0aW1lIGRldmljZW1vdGlvbiBpcyB1cGRhdGVkXG5cdGZvciAoIHZhciBpIGluIHNlbnNvcnMubW90aW9uLmNhbGxiYWNrcyApIHtcblxuXHRcdHNlbnNvcnMubW90aW9uLmNhbGxiYWNrc1sgaSBdLmNhbGwoIHRoaXMgKTtcblxuXHR9XG5cbn1cblxuLy8vLy8gRlVMTFRJTFQgQVBJIFJvb3QgT2JqZWN0IC8vLy8vXG5cbnZhciBGVUxMVElMVCA9IHt9O1xuXG5GVUxMVElMVC52ZXJzaW9uID0gXCIwLjUuM1wiO1xuXG4vLy8vLyBGVUxMVElMVCBBUEkgUm9vdCBNZXRob2RzIC8vLy8vXG5cbkZVTExUSUxULmdldERldmljZU9yaWVudGF0aW9uID0gZnVuY3Rpb24ob3B0aW9ucykge1xuXG5cdHZhciBwcm9taXNlID0gbmV3IFByb21pc2UoZnVuY3Rpb24ocmVzb2x2ZSwgcmVqZWN0KSB7XG5cblx0XHR2YXIgY29udHJvbCA9IG5ldyBGVUxMVElMVC5EZXZpY2VPcmllbnRhdGlvbihvcHRpb25zKTtcblxuXHRcdGNvbnRyb2wuc3RhcnQoKTtcblxuXHRcdHZhciBvcmllbnRhdGlvblNlbnNvckNoZWNrID0gbmV3IFNlbnNvckNoZWNrKHNlbnNvcnMub3JpZW50YXRpb24pO1xuXG5cdFx0b3JpZW50YXRpb25TZW5zb3JDaGVjay50aGVuKGZ1bmN0aW9uKCkge1xuXG5cdFx0XHRyZXNvbHZlKGNvbnRyb2wpO1xuXG5cdFx0fSkuY2F0Y2goZnVuY3Rpb24oKSB7XG5cblx0XHRcdGNvbnRyb2wuc3RvcCgpO1xuXHRcdFx0cmVqZWN0KCdEZXZpY2VPcmllbnRhdGlvbiBpcyBub3Qgc3VwcG9ydGVkJyk7XG5cblx0XHR9KTtcblxuXHR9KTtcblxuXHRyZXR1cm4gcHJvbWlzZTtcblxufTtcblxuRlVMTFRJTFQuZ2V0RGV2aWNlTW90aW9uID0gZnVuY3Rpb24ob3B0aW9ucykge1xuXG5cdHZhciBwcm9taXNlID0gbmV3IFByb21pc2UoZnVuY3Rpb24ocmVzb2x2ZSwgcmVqZWN0KSB7XG5cblx0XHR2YXIgY29udHJvbCA9IG5ldyBGVUxMVElMVC5EZXZpY2VNb3Rpb24ob3B0aW9ucyk7XG5cblx0XHRjb250cm9sLnN0YXJ0KCk7XG5cblx0XHR2YXIgbW90aW9uU2Vuc29yQ2hlY2sgPSBuZXcgU2Vuc29yQ2hlY2soc2Vuc29ycy5tb3Rpb24pO1xuXG5cdFx0bW90aW9uU2Vuc29yQ2hlY2sudGhlbihmdW5jdGlvbigpIHtcblxuXHRcdFx0cmVzb2x2ZShjb250cm9sKTtcblxuXHRcdH0pLmNhdGNoKGZ1bmN0aW9uKCkge1xuXG5cdFx0XHRjb250cm9sLnN0b3AoKTtcblx0XHRcdHJlamVjdCgnRGV2aWNlTW90aW9uIGlzIG5vdCBzdXBwb3J0ZWQnKTtcblxuXHRcdH0pO1xuXG5cdH0pO1xuXG5cdHJldHVybiBwcm9taXNlO1xuXG59O1xuXG5cbi8vLy8vLyBGVUxMVElMVC5RdWF0ZXJuaW9uIC8vLy8vL1xuXG5GVUxMVElMVC5RdWF0ZXJuaW9uID0gZnVuY3Rpb24gKCB4LCB5LCB6LCB3ICkge1xuXG5cdHZhciBxdWF0LCBvdXRRdWF0O1xuXG5cdHRoaXMuc2V0ID0gZnVuY3Rpb24gKCB4LCB5LCB6LCB3ICkge1xuXG5cdFx0dGhpcy54ID0geCB8fCAwO1xuXHRcdHRoaXMueSA9IHkgfHwgMDtcblx0XHR0aGlzLnogPSB6IHx8IDA7XG5cdFx0dGhpcy53ID0gdyB8fCAxO1xuXG5cdH07XG5cblx0dGhpcy5jb3B5ID0gZnVuY3Rpb24gKCBxdWF0ZXJuaW9uICkge1xuXG5cdFx0dGhpcy54ID0gcXVhdGVybmlvbi54O1xuXHRcdHRoaXMueSA9IHF1YXRlcm5pb24ueTtcblx0XHR0aGlzLnogPSBxdWF0ZXJuaW9uLno7XG5cdFx0dGhpcy53ID0gcXVhdGVybmlvbi53O1xuXG5cdH07XG5cblx0dGhpcy5zZXRGcm9tRXVsZXIgPSAoZnVuY3Rpb24gKCkge1xuXG5cdFx0dmFyIF94LCBfeSwgX3o7XG5cdFx0dmFyIF94XzIsIF95XzIsIF96XzI7XG5cdFx0dmFyIGNYLCBjWSwgY1osIHNYLCBzWSwgc1o7XG5cblx0XHRyZXR1cm4gZnVuY3Rpb24gKCBldWxlciApIHtcblxuXHRcdFx0ZXVsZXIgPSBldWxlciB8fCB7fTtcblxuXHRcdFx0X3ogPSAoIGV1bGVyLmFscGhhIHx8IDAgKSAqIGRlZ1RvUmFkO1xuXHRcdFx0X3ggPSAoIGV1bGVyLmJldGEgfHwgMCApICogZGVnVG9SYWQ7XG5cdFx0XHRfeSA9ICggZXVsZXIuZ2FtbWEgfHwgMCApICogZGVnVG9SYWQ7XG5cblx0XHRcdF96XzIgPSBfeiAvIDI7XG5cdFx0XHRfeF8yID0gX3ggLyAyO1xuXHRcdFx0X3lfMiA9IF95IC8gMjtcblxuXHRcdFx0Y1ggPSBNYXRoLmNvcyggX3hfMiApO1xuXHRcdFx0Y1kgPSBNYXRoLmNvcyggX3lfMiApO1xuXHRcdFx0Y1ogPSBNYXRoLmNvcyggX3pfMiApO1xuXHRcdFx0c1ggPSBNYXRoLnNpbiggX3hfMiApO1xuXHRcdFx0c1kgPSBNYXRoLnNpbiggX3lfMiApO1xuXHRcdFx0c1ogPSBNYXRoLnNpbiggX3pfMiApO1xuXG5cdFx0XHR0aGlzLnNldChcblx0XHRcdFx0c1ggKiBjWSAqIGNaIC0gY1ggKiBzWSAqIHNaLCAvLyB4XG5cdFx0XHRcdGNYICogc1kgKiBjWiArIHNYICogY1kgKiBzWiwgLy8geVxuXHRcdFx0XHRjWCAqIGNZICogc1ogKyBzWCAqIHNZICogY1osIC8vIHpcblx0XHRcdFx0Y1ggKiBjWSAqIGNaIC0gc1ggKiBzWSAqIHNaICAvLyB3XG5cdFx0XHQpO1xuXG5cdFx0XHR0aGlzLm5vcm1hbGl6ZSgpO1xuXG5cdFx0XHRyZXR1cm4gdGhpcztcblxuXHRcdH07XG5cblx0fSkoKTtcblxuXHR0aGlzLnNldEZyb21Sb3RhdGlvbk1hdHJpeCA9IChmdW5jdGlvbiAoKSB7XG5cblx0XHR2YXIgUjtcblxuXHRcdHJldHVybiBmdW5jdGlvbiggbWF0cml4ICkge1xuXG5cdFx0XHRSID0gbWF0cml4LmVsZW1lbnRzO1xuXG5cdFx0XHR0aGlzLnNldChcblx0XHRcdFx0MC41ICogTWF0aC5zcXJ0KCAxICsgUlswXSAtIFJbNF0gLSBSWzhdICkgKiBzaWduKCBSWzddIC0gUls1XSApLCAvLyB4XG5cdFx0XHRcdDAuNSAqIE1hdGguc3FydCggMSAtIFJbMF0gKyBSWzRdIC0gUls4XSApICogc2lnbiggUlsyXSAtIFJbNl0gKSwgLy8geVxuXHRcdFx0XHQwLjUgKiBNYXRoLnNxcnQoIDEgLSBSWzBdIC0gUls0XSArIFJbOF0gKSAqIHNpZ24oIFJbM10gLSBSWzFdICksIC8vIHpcblx0XHRcdFx0MC41ICogTWF0aC5zcXJ0KCAxICsgUlswXSArIFJbNF0gKyBSWzhdICkgICAgICAgICAgICAgICAgICAgICAgICAvLyB3XG5cdFx0XHQpO1xuXG5cdFx0XHRyZXR1cm4gdGhpcztcblxuXHRcdH07XG5cblx0fSkoKTtcblxuXHR0aGlzLm11bHRpcGx5ID0gZnVuY3Rpb24gKCBxdWF0ZXJuaW9uICkge1xuXG5cdFx0b3V0UXVhdCA9IEZVTExUSUxULlF1YXRlcm5pb24ucHJvdG90eXBlLm11bHRpcGx5UXVhdGVybmlvbnMoIHRoaXMsIHF1YXRlcm5pb24gKTtcblx0XHR0aGlzLmNvcHkoIG91dFF1YXQgKTtcblxuXHRcdHJldHVybiB0aGlzO1xuXG5cdH07XG5cblx0dGhpcy5yb3RhdGVYID0gZnVuY3Rpb24gKCBhbmdsZSApIHtcblxuXHRcdG91dFF1YXQgPSBGVUxMVElMVC5RdWF0ZXJuaW9uLnByb3RvdHlwZS5yb3RhdGVCeUF4aXNBbmdsZSggdGhpcywgWyAxLCAwLCAwIF0sIGFuZ2xlICk7XG5cdFx0dGhpcy5jb3B5KCBvdXRRdWF0ICk7XG5cblx0XHRyZXR1cm4gdGhpcztcblxuXHR9O1xuXG5cdHRoaXMucm90YXRlWSA9IGZ1bmN0aW9uICggYW5nbGUgKSB7XG5cblx0XHRvdXRRdWF0ID0gRlVMTFRJTFQuUXVhdGVybmlvbi5wcm90b3R5cGUucm90YXRlQnlBeGlzQW5nbGUoIHRoaXMsIFsgMCwgMSwgMCBdLCBhbmdsZSApO1xuXHRcdHRoaXMuY29weSggb3V0UXVhdCApO1xuXG5cdFx0cmV0dXJuIHRoaXM7XG5cblx0fTtcblxuXHR0aGlzLnJvdGF0ZVogPSBmdW5jdGlvbiAoIGFuZ2xlICkge1xuXG5cdFx0b3V0UXVhdCA9IEZVTExUSUxULlF1YXRlcm5pb24ucHJvdG90eXBlLnJvdGF0ZUJ5QXhpc0FuZ2xlKCB0aGlzLCBbIDAsIDAsIDEgXSwgYW5nbGUgKTtcblx0XHR0aGlzLmNvcHkoIG91dFF1YXQgKTtcblxuXHRcdHJldHVybiB0aGlzO1xuXG5cdH07XG5cblx0dGhpcy5ub3JtYWxpemUgPSBmdW5jdGlvbiAoKSB7XG5cblx0XHRyZXR1cm4gRlVMTFRJTFQuUXVhdGVybmlvbi5wcm90b3R5cGUubm9ybWFsaXplKCB0aGlzICk7XG5cblx0fTtcblxuXHQvLyBJbml0aWFsaXplIG9iamVjdCB2YWx1ZXNcblx0dGhpcy5zZXQoIHgsIHksIHosIHcgKTtcblxufTtcblxuRlVMTFRJTFQuUXVhdGVybmlvbi5wcm90b3R5cGUgPSB7XG5cblx0Y29uc3RydWN0b3I6IEZVTExUSUxULlF1YXRlcm5pb24sXG5cblx0bXVsdGlwbHlRdWF0ZXJuaW9uczogZnVuY3Rpb24gKCkge1xuXG5cdFx0dmFyIG11bHRpcGxpZWRRdWF0ID0gbmV3IEZVTExUSUxULlF1YXRlcm5pb24oKTtcblxuXHRcdHJldHVybiBmdW5jdGlvbiAoIGEsIGIgKSB7XG5cblx0XHRcdHZhciBxYXggPSBhLngsIHFheSA9IGEueSwgcWF6ID0gYS56LCBxYXcgPSBhLnc7XG5cdFx0XHR2YXIgcWJ4ID0gYi54LCBxYnkgPSBiLnksIHFieiA9IGIueiwgcWJ3ID0gYi53O1xuXG5cdFx0XHRtdWx0aXBsaWVkUXVhdC5zZXQoXG5cdFx0XHRcdHFheCAqIHFidyArIHFhdyAqIHFieCArIHFheSAqIHFieiAtIHFheiAqIHFieSwgLy8geFxuXHRcdFx0XHRxYXkgKiBxYncgKyBxYXcgKiBxYnkgKyBxYXogKiBxYnggLSBxYXggKiBxYnosIC8vIHlcblx0XHRcdFx0cWF6ICogcWJ3ICsgcWF3ICogcWJ6ICsgcWF4ICogcWJ5IC0gcWF5ICogcWJ4LCAvLyB6XG5cdFx0XHRcdHFhdyAqIHFidyAtIHFheCAqIHFieCAtIHFheSAqIHFieSAtIHFheiAqIHFieiAgLy8gd1xuXHRcdFx0KTtcblxuXHRcdFx0cmV0dXJuIG11bHRpcGxpZWRRdWF0O1xuXG5cdFx0fTtcblxuXHR9KCksXG5cblx0bm9ybWFsaXplOiBmdW5jdGlvbiggcSApIHtcblxuXHRcdHZhciBsZW4gPSBNYXRoLnNxcnQoIHEueCAqIHEueCArIHEueSAqIHEueSArIHEueiAqIHEueiArIHEudyAqIHEudyApO1xuXG5cdFx0aWYgKCBsZW4gPT09IDAgKSB7XG5cblx0XHRcdHEueCA9IDA7XG5cdFx0XHRxLnkgPSAwO1xuXHRcdFx0cS56ID0gMDtcblx0XHRcdHEudyA9IDE7XG5cblx0XHR9IGVsc2Uge1xuXG5cdFx0XHRsZW4gPSAxIC8gbGVuO1xuXG5cdFx0XHRxLnggKj0gbGVuO1xuXHRcdFx0cS55ICo9IGxlbjtcblx0XHRcdHEueiAqPSBsZW47XG5cdFx0XHRxLncgKj0gbGVuO1xuXG5cdFx0fVxuXG5cdFx0cmV0dXJuIHE7XG5cblx0fSxcblxuXHRyb3RhdGVCeUF4aXNBbmdsZTogZnVuY3Rpb24gKCkge1xuXG5cdFx0dmFyIG91dHB1dFF1YXRlcm5pb24gPSBuZXcgRlVMTFRJTFQuUXVhdGVybmlvbigpO1xuXHRcdHZhciB0cmFuc2Zvcm1RdWF0ZXJuaW9uID0gbmV3IEZVTExUSUxULlF1YXRlcm5pb24oKTtcblxuXHRcdHZhciBoYWxmQW5nbGUsIHNBO1xuXG5cdFx0cmV0dXJuIGZ1bmN0aW9uICggdGFyZ2V0UXVhdGVybmlvbiwgYXhpcywgYW5nbGUgKSB7XG5cblx0XHRcdGhhbGZBbmdsZSA9ICggYW5nbGUgfHwgMCApIC8gMjtcblx0XHRcdHNBID0gTWF0aC5zaW4oIGhhbGZBbmdsZSApO1xuXG5cdFx0XHR0cmFuc2Zvcm1RdWF0ZXJuaW9uLnNldChcblx0XHRcdFx0KCBheGlzWyAwIF0gfHwgMCApICogc0EsIC8vIHhcblx0XHRcdFx0KCBheGlzWyAxIF0gfHwgMCApICogc0EsIC8vIHlcblx0XHRcdFx0KCBheGlzWyAyIF0gfHwgMCApICogc0EsIC8vIHpcblx0XHRcdFx0TWF0aC5jb3MoIGhhbGZBbmdsZSApICAgIC8vIHdcblx0XHRcdCk7XG5cblx0XHRcdC8vIE11bHRpcGx5IHF1YXRlcm5pb24gYnkgcVxuXHRcdFx0b3V0cHV0UXVhdGVybmlvbiA9IEZVTExUSUxULlF1YXRlcm5pb24ucHJvdG90eXBlLm11bHRpcGx5UXVhdGVybmlvbnMoIHRhcmdldFF1YXRlcm5pb24sIHRyYW5zZm9ybVF1YXRlcm5pb24gKTtcblxuXHRcdFx0cmV0dXJuIEZVTExUSUxULlF1YXRlcm5pb24ucHJvdG90eXBlLm5vcm1hbGl6ZSggb3V0cHV0UXVhdGVybmlvbiApO1xuXG5cdFx0fTtcblxuXHR9KClcblxufTtcblxuLy8vLy8vIEZVTExUSUxULlJvdGF0aW9uTWF0cml4IC8vLy8vL1xuXG5GVUxMVElMVC5Sb3RhdGlvbk1hdHJpeCA9IGZ1bmN0aW9uICggbTExLCBtMTIsIG0xMywgbTIxLCBtMjIsIG0yMywgbTMxLCBtMzIsIG0zMyApIHtcblxuXHR2YXIgb3V0TWF0cml4O1xuXG5cdHRoaXMuZWxlbWVudHMgPSBuZXcgRmxvYXQzMkFycmF5KCA5ICk7XG5cblx0dGhpcy5pZGVudGl0eSA9IGZ1bmN0aW9uICgpIHtcblxuXHRcdHRoaXMuc2V0KFxuXHRcdFx0MSwgMCwgMCxcblx0XHRcdDAsIDEsIDAsXG5cdFx0XHQwLCAwLCAxXG5cdFx0KTtcblxuXHRcdHJldHVybiB0aGlzO1xuXG5cdH07XG5cblx0dGhpcy5zZXQgPSBmdW5jdGlvbiAoIG0xMSwgbTEyLCBtMTMsIG0yMSwgbTIyLCBtMjMsIG0zMSwgbTMyLCBtMzMgKSB7XG5cblx0XHR0aGlzLmVsZW1lbnRzWyAwIF0gPSBtMTEgfHwgMTtcblx0XHR0aGlzLmVsZW1lbnRzWyAxIF0gPSBtMTIgfHwgMDtcblx0XHR0aGlzLmVsZW1lbnRzWyAyIF0gPSBtMTMgfHwgMDtcblx0XHR0aGlzLmVsZW1lbnRzWyAzIF0gPSBtMjEgfHwgMDtcblx0XHR0aGlzLmVsZW1lbnRzWyA0IF0gPSBtMjIgfHwgMTtcblx0XHR0aGlzLmVsZW1lbnRzWyA1IF0gPSBtMjMgfHwgMDtcblx0XHR0aGlzLmVsZW1lbnRzWyA2IF0gPSBtMzEgfHwgMDtcblx0XHR0aGlzLmVsZW1lbnRzWyA3IF0gPSBtMzIgfHwgMDtcblx0XHR0aGlzLmVsZW1lbnRzWyA4IF0gPSBtMzMgfHwgMTtcblxuXHR9O1xuXG5cdHRoaXMuY29weSA9IGZ1bmN0aW9uICggbWF0cml4ICkge1xuXG5cdFx0dGhpcy5lbGVtZW50c1sgMCBdID0gbWF0cml4LmVsZW1lbnRzWyAwIF07XG5cdFx0dGhpcy5lbGVtZW50c1sgMSBdID0gbWF0cml4LmVsZW1lbnRzWyAxIF07XG5cdFx0dGhpcy5lbGVtZW50c1sgMiBdID0gbWF0cml4LmVsZW1lbnRzWyAyIF07XG5cdFx0dGhpcy5lbGVtZW50c1sgMyBdID0gbWF0cml4LmVsZW1lbnRzWyAzIF07XG5cdFx0dGhpcy5lbGVtZW50c1sgNCBdID0gbWF0cml4LmVsZW1lbnRzWyA0IF07XG5cdFx0dGhpcy5lbGVtZW50c1sgNSBdID0gbWF0cml4LmVsZW1lbnRzWyA1IF07XG5cdFx0dGhpcy5lbGVtZW50c1sgNiBdID0gbWF0cml4LmVsZW1lbnRzWyA2IF07XG5cdFx0dGhpcy5lbGVtZW50c1sgNyBdID0gbWF0cml4LmVsZW1lbnRzWyA3IF07XG5cdFx0dGhpcy5lbGVtZW50c1sgOCBdID0gbWF0cml4LmVsZW1lbnRzWyA4IF07XG5cblx0fTtcblxuXHR0aGlzLnNldEZyb21FdWxlciA9IChmdW5jdGlvbigpIHtcblxuXHRcdHZhciBfeCwgX3ksIF96O1xuXHRcdHZhciBjWCwgY1ksIGNaLCBzWCwgc1ksIHNaO1xuXG5cdFx0cmV0dXJuIGZ1bmN0aW9uICggZXVsZXIgKSB7XG5cblx0XHRcdGV1bGVyID0gZXVsZXIgfHwge307XG5cblx0XHRcdF96ID0gKCBldWxlci5hbHBoYSB8fCAwICkgKiBkZWdUb1JhZDtcblx0XHRcdF94ID0gKCBldWxlci5iZXRhIHx8IDAgKSAqIGRlZ1RvUmFkO1xuXHRcdFx0X3kgPSAoIGV1bGVyLmdhbW1hIHx8IDAgKSAqIGRlZ1RvUmFkO1xuXG5cdFx0XHRjWCA9IE1hdGguY29zKCBfeCApO1xuXHRcdFx0Y1kgPSBNYXRoLmNvcyggX3kgKTtcblx0XHRcdGNaID0gTWF0aC5jb3MoIF96ICk7XG5cdFx0XHRzWCA9IE1hdGguc2luKCBfeCApO1xuXHRcdFx0c1kgPSBNYXRoLnNpbiggX3kgKTtcblx0XHRcdHNaID0gTWF0aC5zaW4oIF96ICk7XG5cblx0XHRcdC8vXG5cdFx0XHQvLyBaWFktb3JkZXJlZCByb3RhdGlvbiBtYXRyaXggY29uc3RydWN0aW9uLlxuXHRcdFx0Ly9cblxuXHRcdFx0dGhpcy5zZXQoXG5cdFx0XHRcdGNaICogY1kgLSBzWiAqIHNYICogc1ksIC8vIDEsMVxuXHRcdFx0XHQtIGNYICogc1osICAgICAgICAgICAgICAvLyAxLDJcblx0XHRcdFx0Y1kgKiBzWiAqIHNYICsgY1ogKiBzWSwgLy8gMSwzXG5cblx0XHRcdFx0Y1kgKiBzWiArIGNaICogc1ggKiBzWSwgLy8gMiwxXG5cdFx0XHRcdGNaICogY1gsICAgICAgICAgICAgICAgIC8vIDIsMlxuXHRcdFx0XHRzWiAqIHNZIC0gY1ogKiBjWSAqIHNYLCAvLyAyLDNcblxuXHRcdFx0XHQtIGNYICogc1ksICAgICAgICAgICAgICAvLyAzLDFcblx0XHRcdFx0c1gsICAgICAgICAgICAgICAgICAgICAgLy8gMywyXG5cdFx0XHRcdGNYICogY1kgICAgICAgICAgICAgICAgIC8vIDMsM1xuXHRcdFx0KTtcblxuXHRcdFx0dGhpcy5ub3JtYWxpemUoKTtcblxuXHRcdFx0cmV0dXJuIHRoaXM7XG5cblx0XHR9O1xuXG5cdH0pKCk7XG5cblx0dGhpcy5zZXRGcm9tUXVhdGVybmlvbiA9IChmdW5jdGlvbigpIHtcblxuXHRcdHZhciBzcXcsIHNxeCwgc3F5LCBzcXo7XG5cblx0XHRyZXR1cm4gZnVuY3Rpb24oIHEgKSB7XG5cblx0XHRcdHNxdyA9IHEudyAqIHEudztcblx0XHRcdHNxeCA9IHEueCAqIHEueDtcblx0XHRcdHNxeSA9IHEueSAqIHEueTtcblx0XHRcdHNxeiA9IHEueiAqIHEuejtcblxuXHRcdFx0dGhpcy5zZXQoXG5cdFx0XHRcdHNxdyArIHNxeCAtIHNxeSAtIHNxeiwgICAgICAgLy8gMSwxXG5cdFx0XHRcdDIgKiAocS54ICogcS55IC0gcS53ICogcS56KSwgLy8gMSwyXG5cdFx0XHRcdDIgKiAocS54ICogcS56ICsgcS53ICogcS55KSwgLy8gMSwzXG5cblx0XHRcdFx0MiAqIChxLnggKiBxLnkgKyBxLncgKiBxLnopLCAvLyAyLDFcblx0XHRcdFx0c3F3IC0gc3F4ICsgc3F5IC0gc3F6LCAgICAgICAvLyAyLDJcblx0XHRcdFx0MiAqIChxLnkgKiBxLnogLSBxLncgKiBxLngpLCAvLyAyLDNcblxuXHRcdFx0XHQyICogKHEueCAqIHEueiAtIHEudyAqIHEueSksIC8vIDMsMVxuXHRcdFx0XHQyICogKHEueSAqIHEueiArIHEudyAqIHEueCksIC8vIDMsMlxuXHRcdFx0XHRzcXcgLSBzcXggLSBzcXkgKyBzcXogICAgICAgIC8vIDMsM1xuXHRcdFx0KTtcblxuXHRcdFx0cmV0dXJuIHRoaXM7XG5cblx0XHR9O1xuXG5cdH0pKCk7XG5cblx0dGhpcy5tdWx0aXBseSA9IGZ1bmN0aW9uICggbSApIHtcblxuXHRcdG91dE1hdHJpeCA9IEZVTExUSUxULlJvdGF0aW9uTWF0cml4LnByb3RvdHlwZS5tdWx0aXBseU1hdHJpY2VzKCB0aGlzLCBtICk7XG5cdFx0dGhpcy5jb3B5KCBvdXRNYXRyaXggKTtcblxuXHRcdHJldHVybiB0aGlzO1xuXG5cdH07XG5cblx0dGhpcy5yb3RhdGVYID0gZnVuY3Rpb24gKCBhbmdsZSApIHtcblxuXHRcdG91dE1hdHJpeCA9IEZVTExUSUxULlJvdGF0aW9uTWF0cml4LnByb3RvdHlwZS5yb3RhdGVCeUF4aXNBbmdsZSggdGhpcywgWyAxLCAwLCAwIF0sIGFuZ2xlICk7XG5cdFx0dGhpcy5jb3B5KCBvdXRNYXRyaXggKTtcblxuXHRcdHJldHVybiB0aGlzO1xuXG5cdH07XG5cblx0dGhpcy5yb3RhdGVZID0gZnVuY3Rpb24gKCBhbmdsZSApIHtcblxuXHRcdG91dE1hdHJpeCA9IEZVTExUSUxULlJvdGF0aW9uTWF0cml4LnByb3RvdHlwZS5yb3RhdGVCeUF4aXNBbmdsZSggdGhpcywgWyAwLCAxLCAwIF0sIGFuZ2xlICk7XG5cdFx0dGhpcy5jb3B5KCBvdXRNYXRyaXggKTtcblxuXHRcdHJldHVybiB0aGlzO1xuXG5cdH07XG5cblx0dGhpcy5yb3RhdGVaID0gZnVuY3Rpb24gKCBhbmdsZSApIHtcblxuXHRcdG91dE1hdHJpeCA9IEZVTExUSUxULlJvdGF0aW9uTWF0cml4LnByb3RvdHlwZS5yb3RhdGVCeUF4aXNBbmdsZSggdGhpcywgWyAwLCAwLCAxIF0sIGFuZ2xlICk7XG5cdFx0dGhpcy5jb3B5KCBvdXRNYXRyaXggKTtcblxuXHRcdHJldHVybiB0aGlzO1xuXG5cdH07XG5cblx0dGhpcy5ub3JtYWxpemUgPSBmdW5jdGlvbiAoKSB7XG5cblx0XHRyZXR1cm4gRlVMTFRJTFQuUm90YXRpb25NYXRyaXgucHJvdG90eXBlLm5vcm1hbGl6ZSggdGhpcyApO1xuXG5cdH07XG5cblx0Ly8gSW5pdGlhbGl6ZSBvYmplY3QgdmFsdWVzXG5cdHRoaXMuc2V0KCBtMTEsIG0xMiwgbTEzLCBtMjEsIG0yMiwgbTIzLCBtMzEsIG0zMiwgbTMzICk7XG5cbn07XG5cbkZVTExUSUxULlJvdGF0aW9uTWF0cml4LnByb3RvdHlwZSA9IHtcblxuXHRjb25zdHJ1Y3RvcjogRlVMTFRJTFQuUm90YXRpb25NYXRyaXgsXG5cblx0bXVsdGlwbHlNYXRyaWNlczogZnVuY3Rpb24gKCkge1xuXG5cdFx0dmFyIG1hdHJpeCA9IG5ldyBGVUxMVElMVC5Sb3RhdGlvbk1hdHJpeCgpO1xuXG5cdFx0dmFyIGFFLCBiRTtcblxuXHRcdHJldHVybiBmdW5jdGlvbiAoIGEsIGIgKSB7XG5cblx0XHRcdGFFID0gYS5lbGVtZW50cztcblx0XHRcdGJFID0gYi5lbGVtZW50cztcblxuXHRcdFx0bWF0cml4LnNldChcblx0XHRcdFx0YUVbMF0gKiBiRVswXSArIGFFWzFdICogYkVbM10gKyBhRVsyXSAqIGJFWzZdLFxuXHRcdFx0XHRhRVswXSAqIGJFWzFdICsgYUVbMV0gKiBiRVs0XSArIGFFWzJdICogYkVbN10sXG5cdFx0XHRcdGFFWzBdICogYkVbMl0gKyBhRVsxXSAqIGJFWzVdICsgYUVbMl0gKiBiRVs4XSxcblxuXHRcdFx0XHRhRVszXSAqIGJFWzBdICsgYUVbNF0gKiBiRVszXSArIGFFWzVdICogYkVbNl0sXG5cdFx0XHRcdGFFWzNdICogYkVbMV0gKyBhRVs0XSAqIGJFWzRdICsgYUVbNV0gKiBiRVs3XSxcblx0XHRcdFx0YUVbM10gKiBiRVsyXSArIGFFWzRdICogYkVbNV0gKyBhRVs1XSAqIGJFWzhdLFxuXG5cdFx0XHRcdGFFWzZdICogYkVbMF0gKyBhRVs3XSAqIGJFWzNdICsgYUVbOF0gKiBiRVs2XSxcblx0XHRcdFx0YUVbNl0gKiBiRVsxXSArIGFFWzddICogYkVbNF0gKyBhRVs4XSAqIGJFWzddLFxuXHRcdFx0XHRhRVs2XSAqIGJFWzJdICsgYUVbN10gKiBiRVs1XSArIGFFWzhdICogYkVbOF1cblx0XHRcdCk7XG5cblx0XHRcdHJldHVybiBtYXRyaXg7XG5cblx0XHR9O1xuXG5cdH0oKSxcblxuXHRub3JtYWxpemU6IGZ1bmN0aW9uKCBtYXRyaXggKSB7XG5cblx0XHR2YXIgUiA9IG1hdHJpeC5lbGVtZW50cztcblxuXHRcdC8vIENhbGN1bGF0ZSBtYXRyaXggZGV0ZXJtaW5hbnRcblx0XHR2YXIgZGV0ZXJtaW5hbnQgPSBSWzBdICogUls0XSAqIFJbOF0gLSBSWzBdICogUls1XSAqIFJbN10gLSBSWzFdICogUlszXSAqIFJbOF0gKyBSWzFdICogUls1XSAqIFJbNl0gKyBSWzJdICogUlszXSAqIFJbN10gLSBSWzJdICogUls0XSAqIFJbNl07XG5cblx0XHQvLyBOb3JtYWxpemUgbWF0cml4IHZhbHVlc1xuXHRcdFJbMF0gLz0gZGV0ZXJtaW5hbnQ7XG5cdFx0UlsxXSAvPSBkZXRlcm1pbmFudDtcblx0XHRSWzJdIC89IGRldGVybWluYW50O1xuXHRcdFJbM10gLz0gZGV0ZXJtaW5hbnQ7XG5cdFx0Uls0XSAvPSBkZXRlcm1pbmFudDtcblx0XHRSWzVdIC89IGRldGVybWluYW50O1xuXHRcdFJbNl0gLz0gZGV0ZXJtaW5hbnQ7XG5cdFx0Uls3XSAvPSBkZXRlcm1pbmFudDtcblx0XHRSWzhdIC89IGRldGVybWluYW50O1xuXG5cdFx0bWF0cml4LmVsZW1lbnRzID0gUjtcblxuXHRcdHJldHVybiBtYXRyaXg7XG5cblx0fSxcblxuXHRyb3RhdGVCeUF4aXNBbmdsZTogZnVuY3Rpb24gKCkge1xuXG5cdFx0dmFyIG91dHB1dE1hdHJpeCA9IG5ldyBGVUxMVElMVC5Sb3RhdGlvbk1hdHJpeCgpO1xuXHRcdHZhciB0cmFuc2Zvcm1NYXRyaXggPSBuZXcgRlVMTFRJTFQuUm90YXRpb25NYXRyaXgoKTtcblxuXHRcdHZhciBzQSwgY0E7XG5cdFx0dmFyIHZhbGlkQXhpcyA9IGZhbHNlO1xuXG5cdFx0cmV0dXJuIGZ1bmN0aW9uICggdGFyZ2V0Um90YXRpb25NYXRyaXgsIGF4aXMsIGFuZ2xlICkge1xuXG5cdFx0XHR0cmFuc2Zvcm1NYXRyaXguaWRlbnRpdHkoKTsgLy8gcmVzZXQgdHJhbnNmb3JtIG1hdHJpeFxuXG5cdFx0XHR2YWxpZEF4aXMgPSBmYWxzZTtcblxuXHRcdFx0c0EgPSBNYXRoLnNpbiggYW5nbGUgKTtcblx0XHRcdGNBID0gTWF0aC5jb3MoIGFuZ2xlICk7XG5cblx0XHRcdGlmICggYXhpc1sgMCBdID09PSAxICYmIGF4aXNbIDEgXSA9PT0gMCAmJiBheGlzWyAyIF0gPT09IDAgKSB7IC8vIHhcblxuXHRcdFx0XHR2YWxpZEF4aXMgPSB0cnVlO1xuXG5cdFx0XHRcdHRyYW5zZm9ybU1hdHJpeC5lbGVtZW50c1s0XSA9IGNBO1xuXHRcdFx0XHR0cmFuc2Zvcm1NYXRyaXguZWxlbWVudHNbNV0gPSAtc0E7XG5cdFx0XHRcdHRyYW5zZm9ybU1hdHJpeC5lbGVtZW50c1s3XSA9IHNBO1xuXHRcdFx0XHR0cmFuc2Zvcm1NYXRyaXguZWxlbWVudHNbOF0gPSBjQTtcblxuXHQgXHRcdH0gZWxzZSBpZiAoIGF4aXNbIDEgXSA9PT0gMSAmJiBheGlzWyAwIF0gPT09IDAgJiYgYXhpc1sgMiBdID09PSAwICkgeyAvLyB5XG5cblx0XHRcdFx0dmFsaWRBeGlzID0gdHJ1ZTtcblxuXHRcdFx0XHR0cmFuc2Zvcm1NYXRyaXguZWxlbWVudHNbMF0gPSBjQTtcblx0XHRcdFx0dHJhbnNmb3JtTWF0cml4LmVsZW1lbnRzWzJdID0gc0E7XG5cdFx0XHRcdHRyYW5zZm9ybU1hdHJpeC5lbGVtZW50c1s2XSA9IC1zQTtcblx0XHRcdFx0dHJhbnNmb3JtTWF0cml4LmVsZW1lbnRzWzhdID0gY0E7XG5cblx0IFx0XHR9IGVsc2UgaWYgKCBheGlzWyAyIF0gPT09IDEgJiYgYXhpc1sgMCBdID09PSAwICYmIGF4aXNbIDEgXSA9PT0gMCApIHsgLy8gelxuXG5cdFx0XHRcdHZhbGlkQXhpcyA9IHRydWU7XG5cblx0XHRcdFx0dHJhbnNmb3JtTWF0cml4LmVsZW1lbnRzWzBdID0gY0E7XG5cdFx0XHRcdHRyYW5zZm9ybU1hdHJpeC5lbGVtZW50c1sxXSA9IC1zQTtcblx0XHRcdFx0dHJhbnNmb3JtTWF0cml4LmVsZW1lbnRzWzNdID0gc0E7XG5cdFx0XHRcdHRyYW5zZm9ybU1hdHJpeC5lbGVtZW50c1s0XSA9IGNBO1xuXG5cdCBcdFx0fVxuXG5cdFx0XHRpZiAoIHZhbGlkQXhpcyApIHtcblxuXHRcdFx0XHRvdXRwdXRNYXRyaXggPSBGVUxMVElMVC5Sb3RhdGlvbk1hdHJpeC5wcm90b3R5cGUubXVsdGlwbHlNYXRyaWNlcyggdGFyZ2V0Um90YXRpb25NYXRyaXgsIHRyYW5zZm9ybU1hdHJpeCApO1xuXG5cdFx0XHRcdG91dHB1dE1hdHJpeCA9IEZVTExUSUxULlJvdGF0aW9uTWF0cml4LnByb3RvdHlwZS5ub3JtYWxpemUoIG91dHB1dE1hdHJpeCApO1xuXG5cdFx0XHR9IGVsc2Uge1xuXG5cdFx0XHRcdG91dHB1dE1hdHJpeCA9IHRhcmdldFJvdGF0aW9uTWF0cml4O1xuXG5cdFx0XHR9XG5cblx0XHRcdHJldHVybiBvdXRwdXRNYXRyaXg7XG5cblx0XHR9O1xuXG5cdH0oKVxuXG59O1xuXG4vLy8vLy8gRlVMTFRJTFQuRXVsZXIgLy8vLy8vXG5cbkZVTExUSUxULkV1bGVyID0gZnVuY3Rpb24gKCBhbHBoYSwgYmV0YSwgZ2FtbWEgKSB7XG5cblx0dGhpcy5zZXQgPSBmdW5jdGlvbiAoIGFscGhhLCBiZXRhLCBnYW1tYSApIHtcblxuXHRcdHRoaXMuYWxwaGEgPSBhbHBoYSB8fCAwO1xuXHRcdHRoaXMuYmV0YSAgPSBiZXRhICB8fCAwO1xuXHRcdHRoaXMuZ2FtbWEgPSBnYW1tYSB8fCAwO1xuXG5cdH07XG5cblx0dGhpcy5jb3B5ID0gZnVuY3Rpb24gKCBpbkV1bGVyICkge1xuXG5cdFx0dGhpcy5hbHBoYSA9IGluRXVsZXIuYWxwaGE7XG5cdFx0dGhpcy5iZXRhICA9IGluRXVsZXIuYmV0YTtcblx0XHR0aGlzLmdhbW1hID0gaW5FdWxlci5nYW1tYTtcblxuXHR9O1xuXG5cdHRoaXMuc2V0RnJvbVJvdGF0aW9uTWF0cml4ID0gKGZ1bmN0aW9uICgpIHtcblxuXHRcdHZhciBSLCBfYWxwaGEsIF9iZXRhLCBfZ2FtbWE7XG5cblx0XHRyZXR1cm4gZnVuY3Rpb24gKCBtYXRyaXggKSB7XG5cblx0XHRcdFIgPSBtYXRyaXguZWxlbWVudHM7XG5cblx0XHRcdGlmIChSWzhdID4gMCkgeyAvLyBjb3MoYmV0YSkgPiAwXG5cblx0XHRcdFx0X2FscGhhID0gTWF0aC5hdGFuMigtUlsxXSwgUls0XSk7XG5cdFx0XHRcdF9iZXRhICA9IE1hdGguYXNpbihSWzddKTsgLy8gYmV0YSAoLXBpLzIsIHBpLzIpXG5cdFx0XHRcdF9nYW1tYSA9IE1hdGguYXRhbjIoLVJbNl0sIFJbOF0pOyAvLyBnYW1tYSAoLXBpLzIsIHBpLzIpXG5cblx0XHRcdH0gZWxzZSBpZiAoUls4XSA8IDApIHsgIC8vIGNvcyhiZXRhKSA8IDBcblxuXHRcdFx0XHRfYWxwaGEgPSBNYXRoLmF0YW4yKFJbMV0sIC1SWzRdKTtcblx0XHRcdFx0X2JldGEgID0gLU1hdGguYXNpbihSWzddKTtcblx0XHRcdFx0X2JldGEgICs9IChfYmV0YSA+PSAwKSA/IC0gTV9QSSA6IE1fUEk7IC8vIGJldGEgWy1waSwtcGkvMikgVSAocGkvMixwaSlcblx0XHRcdFx0X2dhbW1hID0gTWF0aC5hdGFuMihSWzZdLCAtUls4XSk7IC8vIGdhbW1hICgtcGkvMiwgcGkvMilcblxuXHRcdFx0fSBlbHNlIHsgLy8gUls4XSA9PSAwXG5cblx0XHRcdFx0aWYgKFJbNl0gPiAwKSB7ICAvLyBjb3MoZ2FtbWEpID09IDAsIGNvcyhiZXRhKSA+IDBcblxuXHRcdFx0XHRcdF9hbHBoYSA9IE1hdGguYXRhbjIoLVJbMV0sIFJbNF0pO1xuXHRcdFx0XHRcdF9iZXRhICA9IE1hdGguYXNpbihSWzddKTsgLy8gYmV0YSBbLXBpLzIsIHBpLzJdXG5cdFx0XHRcdFx0X2dhbW1hID0gLSBNX1BJXzI7IC8vIGdhbW1hID0gLXBpLzJcblxuXHRcdFx0XHR9IGVsc2UgaWYgKFJbNl0gPCAwKSB7IC8vIGNvcyhnYW1tYSkgPT0gMCwgY29zKGJldGEpIDwgMFxuXG5cdFx0XHRcdFx0X2FscGhhID0gTWF0aC5hdGFuMihSWzFdLCAtUls0XSk7XG5cdFx0XHRcdFx0X2JldGEgID0gLU1hdGguYXNpbihSWzddKTtcblx0XHRcdFx0XHRfYmV0YSAgKz0gKF9iZXRhID49IDApID8gLSBNX1BJIDogTV9QSTsgLy8gYmV0YSBbLXBpLC1waS8yKSBVIChwaS8yLHBpKVxuXHRcdFx0XHRcdF9nYW1tYSA9IC0gTV9QSV8yOyAvLyBnYW1tYSA9IC1waS8yXG5cblx0XHRcdFx0fSBlbHNlIHsgLy8gUls2XSA9PSAwLCBjb3MoYmV0YSkgPT0gMFxuXG5cdFx0XHRcdFx0Ly8gZ2ltYmFsIGxvY2sgZGlzY29udGludWl0eVxuXHRcdFx0XHRcdF9hbHBoYSA9IE1hdGguYXRhbjIoUlszXSwgUlswXSk7XG5cdFx0XHRcdFx0X2JldGEgID0gKFJbN10gPiAwKSA/IE1fUElfMiA6IC0gTV9QSV8yOyAvLyBiZXRhID0gKy1waS8yXG5cdFx0XHRcdFx0X2dhbW1hID0gMDsgLy8gZ2FtbWEgPSAwXG5cblx0XHRcdFx0fVxuXG5cdFx0XHR9XG5cblx0XHRcdC8vIGFscGhhIGlzIGluIFstcGksIHBpXSwgbWFrZSBzdXJlIGl0IGlzIGluIFswLCAyKnBpKS5cblx0XHRcdGlmIChfYWxwaGEgPCAwKSB7XG5cdFx0XHRcdF9hbHBoYSArPSBNXzJfUEk7IC8vIGFscGhhIFswLCAyKnBpKVxuXHRcdFx0fVxuXG5cdFx0XHQvLyBDb252ZXJ0IHRvIGRlZ3JlZXNcblx0XHRcdF9hbHBoYSAqPSByYWRUb0RlZztcblx0XHRcdF9iZXRhICAqPSByYWRUb0RlZztcblx0XHRcdF9nYW1tYSAqPSByYWRUb0RlZztcblxuXHRcdFx0Ly8gYXBwbHkgZGVyaXZlZCBldWxlciBhbmdsZXMgdG8gY3VycmVudCBvYmplY3Rcblx0XHRcdHRoaXMuc2V0KCBfYWxwaGEsIF9iZXRhLCBfZ2FtbWEgKTtcblxuXHRcdH07XG5cblx0fSkoKTtcblxuXHR0aGlzLnNldEZyb21RdWF0ZXJuaW9uID0gKGZ1bmN0aW9uICgpIHtcblxuXHRcdHZhciBfYWxwaGEsIF9iZXRhLCBfZ2FtbWE7XG5cblx0XHRyZXR1cm4gZnVuY3Rpb24gKCBxICkge1xuXG5cdFx0XHR2YXIgc3F3ID0gcS53ICogcS53O1xuXHRcdFx0dmFyIHNxeCA9IHEueCAqIHEueDtcblx0XHRcdHZhciBzcXkgPSBxLnkgKiBxLnk7XG5cdFx0XHR2YXIgc3F6ID0gcS56ICogcS56O1xuXG5cdFx0XHR2YXIgdW5pdExlbmd0aCA9IHNxdyArIHNxeCArIHNxeSArIHNxejsgLy8gTm9ybWFsaXNlZCA9PSAxLCBvdGhlcndpc2UgY29ycmVjdGlvbiBkaXZpc29yLlxuXHRcdFx0dmFyIHd4eXogPSBxLncgKiBxLnggKyBxLnkgKiBxLno7XG5cdFx0XHR2YXIgZXBzaWxvbiA9IDFlLTY7IC8vIHJvdW5kaW5nIGZhY3RvclxuXG5cdFx0XHRpZiAod3h5eiA+ICgwLjUgLSBlcHNpbG9uKSAqIHVuaXRMZW5ndGgpIHtcblxuXHRcdFx0XHRfYWxwaGEgPSAyICogTWF0aC5hdGFuMihxLnksIHEudyk7XG5cdFx0XHRcdF9iZXRhID0gTV9QSV8yO1xuXHRcdFx0XHRfZ2FtbWEgPSAwO1xuXG5cdFx0XHR9IGVsc2UgaWYgKHd4eXogPCAoLTAuNSArIGVwc2lsb24pICogdW5pdExlbmd0aCkge1xuXG5cdFx0XHRcdF9hbHBoYSA9IC0yICogTWF0aC5hdGFuMihxLnksIHEudyk7XG5cdFx0XHRcdF9iZXRhID0gLU1fUElfMjtcblx0XHRcdFx0X2dhbW1hID0gMDtcblxuXHRcdFx0fSBlbHNlIHtcblxuXHRcdFx0XHR2YXIgYVggPSBzcXcgLSBzcXggKyBzcXkgLSBzcXo7XG5cdFx0XHRcdHZhciBhWSA9IDIgKiAocS53ICogcS56IC0gcS54ICogcS55KTtcblxuXHRcdFx0XHR2YXIgZ1ggPSBzcXcgLSBzcXggLSBzcXkgKyBzcXo7XG5cdFx0XHRcdHZhciBnWSA9IDIgKiAocS53ICogcS55IC0gcS54ICogcS56KTtcblxuXHRcdFx0XHRpZiAoZ1ggPiAwKSB7XG5cblx0XHRcdFx0XHRfYWxwaGEgPSBNYXRoLmF0YW4yKGFZLCBhWCk7XG5cdFx0XHRcdFx0X2JldGEgID0gTWF0aC5hc2luKDIgKiB3eHl6IC8gdW5pdExlbmd0aCk7XG5cdFx0XHRcdFx0X2dhbW1hID0gTWF0aC5hdGFuMihnWSwgZ1gpO1xuXG5cdFx0XHRcdH0gZWxzZSB7XG5cblx0XHRcdFx0XHRfYWxwaGEgPSBNYXRoLmF0YW4yKC1hWSwgLWFYKTtcblx0XHRcdFx0XHRfYmV0YSAgPSAtTWF0aC5hc2luKDIgKiB3eHl6IC8gdW5pdExlbmd0aCk7XG5cdFx0XHRcdFx0X2JldGEgICs9IF9iZXRhIDwgMCA/IE1fUEkgOiAtIE1fUEk7XG5cdFx0XHRcdFx0X2dhbW1hID0gTWF0aC5hdGFuMigtZ1ksIC1nWCk7XG5cblx0XHRcdFx0fVxuXG5cdFx0XHR9XG5cblx0XHRcdC8vIGFscGhhIGlzIGluIFstcGksIHBpXSwgbWFrZSBzdXJlIGl0IGlzIGluIFswLCAyKnBpKS5cblx0XHRcdGlmIChfYWxwaGEgPCAwKSB7XG5cdFx0XHRcdF9hbHBoYSArPSBNXzJfUEk7IC8vIGFscGhhIFswLCAyKnBpKVxuXHRcdFx0fVxuXG5cdFx0XHQvLyBDb252ZXJ0IHRvIGRlZ3JlZXNcblx0XHRcdF9hbHBoYSAqPSByYWRUb0RlZztcblx0XHRcdF9iZXRhICAqPSByYWRUb0RlZztcblx0XHRcdF9nYW1tYSAqPSByYWRUb0RlZztcblxuXHRcdFx0Ly8gYXBwbHkgZGVyaXZlZCBldWxlciBhbmdsZXMgdG8gY3VycmVudCBvYmplY3Rcblx0XHRcdHRoaXMuc2V0KCBfYWxwaGEsIF9iZXRhLCBfZ2FtbWEgKTtcblxuXHRcdH07XG5cblx0fSkoKTtcblxuXHR0aGlzLnJvdGF0ZVggPSBmdW5jdGlvbiAoIGFuZ2xlICkge1xuXG5cdFx0RlVMTFRJTFQuRXVsZXIucHJvdG90eXBlLnJvdGF0ZUJ5QXhpc0FuZ2xlKCB0aGlzLCBbIDEsIDAsIDAgXSwgYW5nbGUgKTtcblxuXHRcdHJldHVybiB0aGlzO1xuXG5cdH07XG5cblx0dGhpcy5yb3RhdGVZID0gZnVuY3Rpb24gKCBhbmdsZSApIHtcblxuXHRcdEZVTExUSUxULkV1bGVyLnByb3RvdHlwZS5yb3RhdGVCeUF4aXNBbmdsZSggdGhpcywgWyAwLCAxLCAwIF0sIGFuZ2xlICk7XG5cblx0XHRyZXR1cm4gdGhpcztcblxuXHR9O1xuXG5cdHRoaXMucm90YXRlWiA9IGZ1bmN0aW9uICggYW5nbGUgKSB7XG5cblx0XHRGVUxMVElMVC5FdWxlci5wcm90b3R5cGUucm90YXRlQnlBeGlzQW5nbGUoIHRoaXMsIFsgMCwgMCwgMSBdLCBhbmdsZSApO1xuXG5cdFx0cmV0dXJuIHRoaXM7XG5cblx0fTtcblxuXHQvLyBJbml0aWFsaXplIG9iamVjdCB2YWx1ZXNcblx0dGhpcy5zZXQoIGFscGhhLCBiZXRhLCBnYW1tYSApO1xuXG59O1xuXG5GVUxMVElMVC5FdWxlci5wcm90b3R5cGUgPSB7XG5cblx0Y29uc3RydWN0b3I6IEZVTExUSUxULkV1bGVyLFxuXG5cdHJvdGF0ZUJ5QXhpc0FuZ2xlOiBmdW5jdGlvbiAoKSB7XG5cblx0XHR2YXIgX21hdHJpeCA9IG5ldyBGVUxMVElMVC5Sb3RhdGlvbk1hdHJpeCgpO1xuXHRcdHZhciBvdXRFdWxlcjtcblxuXHRcdHJldHVybiBmdW5jdGlvbiAoIHRhcmdldEV1bGVyLCBheGlzLCBhbmdsZSApIHtcblxuXHRcdFx0X21hdHJpeC5zZXRGcm9tRXVsZXIoIHRhcmdldEV1bGVyICk7XG5cblx0XHRcdF9tYXRyaXggPSBGVUxMVElMVC5Sb3RhdGlvbk1hdHJpeC5wcm90b3R5cGUucm90YXRlQnlBeGlzQW5nbGUoIF9tYXRyaXgsIGF4aXMsIGFuZ2xlICk7XG5cblx0XHRcdHRhcmdldEV1bGVyLnNldEZyb21Sb3RhdGlvbk1hdHJpeCggX21hdHJpeCApO1xuXG5cdFx0XHRyZXR1cm4gdGFyZ2V0RXVsZXI7XG5cblx0XHR9O1xuXG5cdH0oKVxuXG59O1xuXG4vLy8vLyBGVUxMVElMVC5EZXZpY2VPcmllbnRhdGlvbiAvLy8vLy9cblxuRlVMTFRJTFQuRGV2aWNlT3JpZW50YXRpb24gPSBmdW5jdGlvbiAob3B0aW9ucykge1xuXG5cdHRoaXMub3B0aW9ucyA9IG9wdGlvbnMgfHwge307IC8vIGJ5IGRlZmF1bHQgdXNlIFVBIGRldmljZW9yaWVudGF0aW9uICd0eXBlJyAoXCJnYW1lXCIgb24gaU9TLCBcIndvcmxkXCIgb24gQW5kcm9pZClcblxuXHR2YXIgdHJpZXMgPSAwO1xuXHR2YXIgbWF4VHJpZXMgPSAyMDA7XG5cdHZhciBzdWNjZXNzQ291bnQgPSAwO1xuXHR2YXIgc3VjY2Vzc1RocmVzaG9sZCA9IDEwO1xuXG5cdHRoaXMuYWxwaGFPZmZzZXRTY3JlZW4gPSAwO1xuXHR0aGlzLmFscGhhT2Zmc2V0RGV2aWNlID0gdW5kZWZpbmVkO1xuXG5cdC8vIENyZWF0ZSBhIGdhbWUtYmFzZWQgZGV2aWNlb3JpZW50YXRpb24gb2JqZWN0IChpbml0aWFsIGFscGhhID09PSAwIGRlZ3JlZXMpXG5cdGlmICh0aGlzLm9wdGlvbnMudHlwZSA9PT0gXCJnYW1lXCIpIHtcblxuXHRcdHZhciBzZXRHYW1lQWxwaGFPZmZzZXQgPSBmdW5jdGlvbihldnQpIHtcblxuXHRcdFx0aWYgKGV2dC5hbHBoYSAhPT0gbnVsbCkgeyAvLyBkbyByZWdhcmRsZXNzIG9mIHdoZXRoZXIgJ2V2dC5hYnNvbHV0ZScgaXMgYWxzbyB0cnVlXG5cdFx0XHRcdHRoaXMuYWxwaGFPZmZzZXREZXZpY2UgPSBuZXcgRlVMTFRJTFQuRXVsZXIoZXZ0LmFscGhhLCAwLCAwKTtcblx0XHRcdFx0dGhpcy5hbHBoYU9mZnNldERldmljZS5yb3RhdGVaKCAtc2NyZWVuT3JpZW50YXRpb25BbmdsZSApO1xuXG5cdFx0XHRcdC8vIERpc2NhcmQgZmlyc3Qge3N1Y2Nlc3NUaHJlc2hvbGR9IHJlc3BvbnNlcyB3aGlsZSBhIGJldHRlciBjb21wYXNzIGxvY2sgaXMgZm91bmQgYnkgVUFcblx0XHRcdFx0aWYoKytzdWNjZXNzQ291bnQgPj0gc3VjY2Vzc1RocmVzaG9sZCkge1xuXHRcdFx0XHRcdHdpbmRvdy5yZW1vdmVFdmVudExpc3RlbmVyKCAnZGV2aWNlb3JpZW50YXRpb24nLCBzZXRHYW1lQWxwaGFPZmZzZXQsIGZhbHNlICk7XG5cdFx0XHRcdFx0cmV0dXJuO1xuXHRcdFx0XHR9XG5cdFx0XHR9XG5cblx0XHRcdGlmKCsrdHJpZXMgPj0gbWF4VHJpZXMpIHtcblx0XHRcdFx0d2luZG93LnJlbW92ZUV2ZW50TGlzdGVuZXIoICdkZXZpY2VvcmllbnRhdGlvbicsIHNldEdhbWVBbHBoYU9mZnNldCwgZmFsc2UgKTtcblx0XHRcdH1cblxuXHRcdH0uYmluZCh0aGlzKTtcblxuXHRcdHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKCAnZGV2aWNlb3JpZW50YXRpb24nLCBzZXRHYW1lQWxwaGFPZmZzZXQsIGZhbHNlICk7XG5cblx0Ly8gQ3JlYXRlIGEgY29tcGFzcy1iYXNlZCBkZXZpY2VvcmllbnRhdGlvbiBvYmplY3QgKGluaXRpYWwgYWxwaGEgPT09IGNvbXBhc3MgZGVncmVlcylcblx0fSBlbHNlIGlmICh0aGlzLm9wdGlvbnMudHlwZSA9PT0gXCJ3b3JsZFwiKSB7XG5cblx0XHR2YXIgc2V0Q29tcGFzc0FscGhhT2Zmc2V0ID0gZnVuY3Rpb24oZXZ0KSB7XG5cblx0XHRcdGlmIChldnQuYWJzb2x1dGUgIT09IHRydWUgJiYgZXZ0LndlYmtpdENvbXBhc3NBY2N1cmFjeSAhPT0gdW5kZWZpbmVkICYmIGV2dC53ZWJraXRDb21wYXNzQWNjdXJhY3kgIT09IG51bGwgJiYgK2V2dC53ZWJraXRDb21wYXNzQWNjdXJhY3kgPj0gMCAmJiArZXZ0LndlYmtpdENvbXBhc3NBY2N1cmFjeSA8IDUwKSB7XG5cdFx0XHRcdHRoaXMuYWxwaGFPZmZzZXREZXZpY2UgPSBuZXcgRlVMTFRJTFQuRXVsZXIoZXZ0LndlYmtpdENvbXBhc3NIZWFkaW5nLCAwLCAwKTtcblx0XHRcdFx0dGhpcy5hbHBoYU9mZnNldERldmljZS5yb3RhdGVaKCBzY3JlZW5PcmllbnRhdGlvbkFuZ2xlICk7XG5cdFx0XHRcdHRoaXMuYWxwaGFPZmZzZXRTY3JlZW4gPSBzY3JlZW5PcmllbnRhdGlvbkFuZ2xlO1xuXG5cdFx0XHRcdC8vIERpc2NhcmQgZmlyc3Qge3N1Y2Nlc3NUaHJlc2hvbGR9IHJlc3BvbnNlcyB3aGlsZSBhIGJldHRlciBjb21wYXNzIGxvY2sgaXMgZm91bmQgYnkgVUFcblx0XHRcdFx0aWYoKytzdWNjZXNzQ291bnQgPj0gc3VjY2Vzc1RocmVzaG9sZCkge1xuXHRcdFx0XHRcdHdpbmRvdy5yZW1vdmVFdmVudExpc3RlbmVyKCAnZGV2aWNlb3JpZW50YXRpb24nLCBzZXRDb21wYXNzQWxwaGFPZmZzZXQsIGZhbHNlICk7XG5cdFx0XHRcdFx0cmV0dXJuO1xuXHRcdFx0XHR9XG5cdFx0XHR9XG5cblx0XHRcdGlmKCsrdHJpZXMgPj0gbWF4VHJpZXMpIHtcblx0XHRcdFx0d2luZG93LnJlbW92ZUV2ZW50TGlzdGVuZXIoICdkZXZpY2VvcmllbnRhdGlvbicsIHNldENvbXBhc3NBbHBoYU9mZnNldCwgZmFsc2UgKTtcblx0XHRcdH1cblxuXHRcdH0uYmluZCh0aGlzKTtcblxuXHRcdHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKCAnZGV2aWNlb3JpZW50YXRpb24nLCBzZXRDb21wYXNzQWxwaGFPZmZzZXQsIGZhbHNlICk7XG5cblx0fSAvLyBlbHNlLi4uIHVzZSB3aGF0ZXZlciBvcmllbnRhdGlvbiBzeXN0ZW0gdGhlIFVBIHByb3ZpZGVzIChcImdhbWVcIiBvbiBpT1MsIFwid29ybGRcIiBvbiBBbmRyb2lkKVxuXG59O1xuXG5GVUxMVElMVC5EZXZpY2VPcmllbnRhdGlvbi5wcm90b3R5cGUgPSB7XG5cblx0Y29uc3RydWN0b3I6IEZVTExUSUxULkRldmljZU9yaWVudGF0aW9uLFxuXG5cdHN0YXJ0OiBmdW5jdGlvbiAoIGNhbGxiYWNrICkge1xuXG5cdFx0aWYgKCBjYWxsYmFjayAmJiBPYmplY3QucHJvdG90eXBlLnRvU3RyaW5nLmNhbGwoIGNhbGxiYWNrICkgPT0gJ1tvYmplY3QgRnVuY3Rpb25dJyApIHtcblxuXHRcdFx0c2Vuc29ycy5vcmllbnRhdGlvbi5jYWxsYmFja3MucHVzaCggY2FsbGJhY2sgKTtcblxuXHRcdH1cblxuXHRcdGlmKCAhc2NyZWVuQWN0aXZlICkge1xuXG5cdFx0XHRpZiAoIGhhc1NjcmVlbk9yaWVudGF0aW9uQVBJICkge1xuXG5cdFx0XHR3aW5kb3cuc2NyZWVuLm9yaWVudGF0aW9uLmFkZEV2ZW50TGlzdGVuZXIoICdjaGFuZ2UnLCBoYW5kbGVTY3JlZW5PcmllbnRhdGlvbkNoYW5nZSwgZmFsc2UgKTtcblxuXHRcdFx0fSBlbHNlIHtcblxuXHRcdFx0XHR3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lciggJ29yaWVudGF0aW9uY2hhbmdlJywgaGFuZGxlU2NyZWVuT3JpZW50YXRpb25DaGFuZ2UsIGZhbHNlICk7XG5cblx0XHRcdH1cblxuXHRcdH1cblxuXHRcdGlmICggIXNlbnNvcnMub3JpZW50YXRpb24uYWN0aXZlICkge1xuXG5cdFx0XHR3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lciggJ2RldmljZW9yaWVudGF0aW9uJywgaGFuZGxlRGV2aWNlT3JpZW50YXRpb25DaGFuZ2UsIGZhbHNlICk7XG5cblx0XHRcdHNlbnNvcnMub3JpZW50YXRpb24uYWN0aXZlID0gdHJ1ZTtcblxuXHRcdH1cblxuXHR9LFxuXG5cdHN0b3A6IGZ1bmN0aW9uICgpIHtcblxuXHRcdGlmICggc2Vuc29ycy5vcmllbnRhdGlvbi5hY3RpdmUgKSB7XG5cblx0XHRcdHdpbmRvdy5yZW1vdmVFdmVudExpc3RlbmVyKCAnZGV2aWNlb3JpZW50YXRpb24nLCBoYW5kbGVEZXZpY2VPcmllbnRhdGlvbkNoYW5nZSwgZmFsc2UgKTtcblxuXHRcdFx0c2Vuc29ycy5vcmllbnRhdGlvbi5hY3RpdmUgPSBmYWxzZTtcblxuXHRcdH1cblxuXHR9LFxuXG5cdGxpc3RlbjogZnVuY3Rpb24oIGNhbGxiYWNrICkge1xuXG5cdFx0dGhpcy5zdGFydCggY2FsbGJhY2sgKTtcblxuXHR9LFxuXG5cdGdldEZpeGVkRnJhbWVRdWF0ZXJuaW9uOiAoZnVuY3Rpb24gKCkge1xuXG5cdFx0dmFyIGV1bGVyID0gbmV3IEZVTExUSUxULkV1bGVyKCk7XG5cdFx0dmFyIG1hdHJpeCA9IG5ldyBGVUxMVElMVC5Sb3RhdGlvbk1hdHJpeCgpO1xuXHRcdHZhciBxdWF0ZXJuaW9uID0gbmV3IEZVTExUSUxULlF1YXRlcm5pb24oKTtcblxuXHRcdHJldHVybiBmdW5jdGlvbigpIHtcblxuXHRcdFx0dmFyIG9yaWVudGF0aW9uRGF0YSA9IHNlbnNvcnMub3JpZW50YXRpb24uZGF0YSB8fCB7IGFscGhhOiAwLCBiZXRhOiAwLCBnYW1tYTogMCB9O1xuXG5cdFx0XHR2YXIgYWRqdXN0ZWRBbHBoYSA9IG9yaWVudGF0aW9uRGF0YS5hbHBoYTtcblxuXHRcdFx0aWYgKHRoaXMuYWxwaGFPZmZzZXREZXZpY2UpIHtcblx0XHRcdFx0bWF0cml4LnNldEZyb21FdWxlciggdGhpcy5hbHBoYU9mZnNldERldmljZSApO1xuXHRcdFx0XHRtYXRyaXgucm90YXRlWiggLSB0aGlzLmFscGhhT2Zmc2V0U2NyZWVuICk7XG5cdFx0XHRcdGV1bGVyLnNldEZyb21Sb3RhdGlvbk1hdHJpeCggbWF0cml4ICk7XG5cblx0XHRcdFx0aWYgKGV1bGVyLmFscGhhIDwgMCkge1xuXHRcdFx0XHRcdGV1bGVyLmFscGhhICs9IDM2MDtcblx0XHRcdFx0fVxuXG5cdFx0XHRcdGV1bGVyLmFscGhhICU9IDM2MDtcblxuXHRcdFx0XHRhZGp1c3RlZEFscGhhIC09IGV1bGVyLmFscGhhO1xuXHRcdFx0fVxuXG5cdFx0XHRldWxlci5zZXQoXG5cdFx0XHRcdGFkanVzdGVkQWxwaGEsXG5cdFx0XHRcdG9yaWVudGF0aW9uRGF0YS5iZXRhLFxuXHRcdFx0XHRvcmllbnRhdGlvbkRhdGEuZ2FtbWFcblx0XHRcdCk7XG5cblx0XHRcdHF1YXRlcm5pb24uc2V0RnJvbUV1bGVyKCBldWxlciApO1xuXG5cdFx0XHRyZXR1cm4gcXVhdGVybmlvbjtcblxuXHRcdH07XG5cblx0fSkoKSxcblxuXHRnZXRTY3JlZW5BZGp1c3RlZFF1YXRlcm5pb246IChmdW5jdGlvbiAoKSB7XG5cblx0XHR2YXIgcXVhdGVybmlvbjtcblxuXHRcdHJldHVybiBmdW5jdGlvbigpIHtcblxuXHRcdFx0cXVhdGVybmlvbiA9IHRoaXMuZ2V0Rml4ZWRGcmFtZVF1YXRlcm5pb24oKTtcblxuXHRcdFx0Ly8gQXV0b21hdGljYWxseSBhcHBseSBzY3JlZW4gb3JpZW50YXRpb24gdHJhbnNmb3JtXG5cdFx0XHRxdWF0ZXJuaW9uLnJvdGF0ZVooIC0gc2NyZWVuT3JpZW50YXRpb25BbmdsZSApO1xuXG5cdFx0XHRyZXR1cm4gcXVhdGVybmlvbjtcblxuXHRcdH07XG5cblx0fSkoKSxcblxuXHRnZXRGaXhlZEZyYW1lTWF0cml4OiAoZnVuY3Rpb24gKCkge1xuXG5cdFx0dmFyIGV1bGVyID0gbmV3IEZVTExUSUxULkV1bGVyKCk7XG5cdFx0dmFyIG1hdHJpeCA9IG5ldyBGVUxMVElMVC5Sb3RhdGlvbk1hdHJpeCgpO1xuXG5cdFx0cmV0dXJuIGZ1bmN0aW9uICgpIHtcblxuXHRcdFx0dmFyIG9yaWVudGF0aW9uRGF0YSA9IHNlbnNvcnMub3JpZW50YXRpb24uZGF0YSB8fCB7IGFscGhhOiAwLCBiZXRhOiAwLCBnYW1tYTogMCB9O1xuXG5cdFx0XHR2YXIgYWRqdXN0ZWRBbHBoYSA9IG9yaWVudGF0aW9uRGF0YS5hbHBoYTtcblxuXHRcdFx0aWYgKHRoaXMuYWxwaGFPZmZzZXREZXZpY2UpIHtcblx0XHRcdFx0bWF0cml4LnNldEZyb21FdWxlciggdGhpcy5hbHBoYU9mZnNldERldmljZSApO1xuXHRcdFx0XHRtYXRyaXgucm90YXRlWiggLSB0aGlzLmFscGhhT2Zmc2V0U2NyZWVuICk7XG5cdFx0XHRcdGV1bGVyLnNldEZyb21Sb3RhdGlvbk1hdHJpeCggbWF0cml4ICk7XG5cblx0XHRcdFx0aWYgKGV1bGVyLmFscGhhIDwgMCkge1xuXHRcdFx0XHRcdGV1bGVyLmFscGhhICs9IDM2MDtcblx0XHRcdFx0fVxuXG5cdFx0XHRcdGV1bGVyLmFscGhhICU9IDM2MDtcblxuXHRcdFx0XHRhZGp1c3RlZEFscGhhIC09IGV1bGVyLmFscGhhO1xuXHRcdFx0fVxuXG5cdFx0XHRldWxlci5zZXQoXG5cdFx0XHRcdGFkanVzdGVkQWxwaGEsXG5cdFx0XHRcdG9yaWVudGF0aW9uRGF0YS5iZXRhLFxuXHRcdFx0XHRvcmllbnRhdGlvbkRhdGEuZ2FtbWFcblx0XHRcdCk7XG5cblx0XHRcdG1hdHJpeC5zZXRGcm9tRXVsZXIoIGV1bGVyICk7XG5cblx0XHRcdHJldHVybiBtYXRyaXg7XG5cblx0XHR9O1xuXG5cdH0pKCksXG5cblx0Z2V0U2NyZWVuQWRqdXN0ZWRNYXRyaXg6IChmdW5jdGlvbiAoKSB7XG5cblx0XHR2YXIgbWF0cml4O1xuXG5cdFx0cmV0dXJuIGZ1bmN0aW9uICgpIHtcblxuXHRcdFx0bWF0cml4ID0gdGhpcy5nZXRGaXhlZEZyYW1lTWF0cml4KCk7XG5cblx0XHRcdC8vIEF1dG9tYXRpY2FsbHkgYXBwbHkgc2NyZWVuIG9yaWVudGF0aW9uIHRyYW5zZm9ybVxuXHRcdFx0bWF0cml4LnJvdGF0ZVooIC0gc2NyZWVuT3JpZW50YXRpb25BbmdsZSApO1xuXG5cdFx0XHRyZXR1cm4gbWF0cml4O1xuXG5cdFx0fTtcblxuXHR9KSgpLFxuXG5cdGdldEZpeGVkRnJhbWVFdWxlcjogKGZ1bmN0aW9uICgpIHtcblxuXHRcdHZhciBldWxlciA9IG5ldyBGVUxMVElMVC5FdWxlcigpO1xuXHRcdHZhciBtYXRyaXg7XG5cblx0XHRyZXR1cm4gZnVuY3Rpb24gKCkge1xuXG5cdFx0XHRtYXRyaXggPSB0aGlzLmdldEZpeGVkRnJhbWVNYXRyaXgoKTtcblxuXHRcdFx0ZXVsZXIuc2V0RnJvbVJvdGF0aW9uTWF0cml4KCBtYXRyaXggKTtcblxuXHRcdFx0cmV0dXJuIGV1bGVyO1xuXG5cdFx0fTtcblxuXHR9KSgpLFxuXG5cdGdldFNjcmVlbkFkanVzdGVkRXVsZXI6IChmdW5jdGlvbiAoKSB7XG5cblx0XHR2YXIgZXVsZXIgPSBuZXcgRlVMTFRJTFQuRXVsZXIoKTtcblx0XHR2YXIgbWF0cml4O1xuXG5cdFx0cmV0dXJuIGZ1bmN0aW9uICgpIHtcblxuXHRcdFx0bWF0cml4ID0gdGhpcy5nZXRTY3JlZW5BZGp1c3RlZE1hdHJpeCgpO1xuXG5cdFx0XHRldWxlci5zZXRGcm9tUm90YXRpb25NYXRyaXgoIG1hdHJpeCApO1xuXG5cdFx0XHRyZXR1cm4gZXVsZXI7XG5cblx0XHR9O1xuXG5cdH0pKCksXG5cblx0aXNBYnNvbHV0ZTogZnVuY3Rpb24gKCkge1xuXG5cdFx0aWYgKCBzZW5zb3JzLm9yaWVudGF0aW9uLmRhdGEgJiYgc2Vuc29ycy5vcmllbnRhdGlvbi5kYXRhLmFic29sdXRlID09PSB0cnVlICkge1xuXHRcdFx0cmV0dXJuIHRydWU7XG5cdFx0fVxuXG5cdFx0cmV0dXJuIGZhbHNlO1xuXG5cdH0sXG5cblx0Z2V0TGFzdFJhd0V2ZW50RGF0YTogZnVuY3Rpb24gKCkge1xuXG5cdFx0cmV0dXJuIHNlbnNvcnMub3JpZW50YXRpb24uZGF0YSB8fCB7fTtcblxuXHR9LFxuXG5cdEFMUEhBOiAnYWxwaGEnLFxuXHRCRVRBOiAnYmV0YScsXG5cdEdBTU1BOiAnZ2FtbWEnXG5cbn07XG5cblxuLy8vLy8gRlVMTFRJTFQuRGV2aWNlTW90aW9uIC8vLy8vL1xuXG5GVUxMVElMVC5EZXZpY2VNb3Rpb24gPSBmdW5jdGlvbiAob3B0aW9ucykge1xuXG5cdHRoaXMub3B0aW9ucyA9IG9wdGlvbnMgfHwge307IC8vIHBsYWNlaG9sZGVyIG9iamVjdCBzaW5jZSBubyBvcHRpb25zIGFyZSBjdXJyZW50bHkgc3VwcG9ydGVkXG5cbn07XG5cbkZVTExUSUxULkRldmljZU1vdGlvbi5wcm90b3R5cGUgPSB7XG5cblx0Y29uc3RydWN0b3I6IEZVTExUSUxULkRldmljZU1vdGlvbixcblxuXHRzdGFydDogZnVuY3Rpb24gKCBjYWxsYmFjayApIHtcblxuXHRcdGlmICggY2FsbGJhY2sgJiYgT2JqZWN0LnByb3RvdHlwZS50b1N0cmluZy5jYWxsKCBjYWxsYmFjayApID09ICdbb2JqZWN0IEZ1bmN0aW9uXScgKSB7XG5cblx0XHRcdHNlbnNvcnMubW90aW9uLmNhbGxiYWNrcy5wdXNoKCBjYWxsYmFjayApO1xuXG5cdFx0fVxuXG5cdFx0aWYoICFzY3JlZW5BY3RpdmUgKSB7XG5cblx0XHRcdGlmICggaGFzU2NyZWVuT3JpZW50YXRpb25BUEkgKSB7XG5cblx0XHRcdFx0d2luZG93LnNjcmVlbi5vcmllbnRhdGlvbi5hZGRFdmVudExpc3RlbmVyKCAnY2hhbmdlJywgaGFuZGxlU2NyZWVuT3JpZW50YXRpb25DaGFuZ2UsIGZhbHNlICk7XG5cblx0XHRcdH0gZWxzZSB7XG5cblx0XHRcdFx0d2luZG93LmFkZEV2ZW50TGlzdGVuZXIoICdvcmllbnRhdGlvbmNoYW5nZScsIGhhbmRsZVNjcmVlbk9yaWVudGF0aW9uQ2hhbmdlLCBmYWxzZSApO1xuXG5cdFx0XHR9XG5cblx0XHR9XG5cblx0XHRpZiAoICFzZW5zb3JzLm1vdGlvbi5hY3RpdmUgKSB7XG5cblx0XHRcdHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKCAnZGV2aWNlbW90aW9uJywgaGFuZGxlRGV2aWNlTW90aW9uQ2hhbmdlLCBmYWxzZSApO1xuXG5cdFx0XHRzZW5zb3JzLm1vdGlvbi5hY3RpdmUgPSB0cnVlO1xuXG5cdFx0fVxuXG5cdH0sXG5cblx0c3RvcDogZnVuY3Rpb24gKCkge1xuXG5cdFx0aWYgKCBzZW5zb3JzLm1vdGlvbi5hY3RpdmUgKSB7XG5cblx0XHRcdHdpbmRvdy5yZW1vdmVFdmVudExpc3RlbmVyKCAnZGV2aWNlbW90aW9uJywgaGFuZGxlRGV2aWNlTW90aW9uQ2hhbmdlLCBmYWxzZSApO1xuXG5cdFx0XHRzZW5zb3JzLm1vdGlvbi5hY3RpdmUgPSBmYWxzZTtcblxuXHRcdH1cblxuXHR9LFxuXG5cdGxpc3RlbjogZnVuY3Rpb24oIGNhbGxiYWNrICkge1xuXG5cdFx0dGhpcy5zdGFydCggY2FsbGJhY2sgKTtcblxuXHR9LFxuXG5cdGdldFNjcmVlbkFkanVzdGVkQWNjZWxlcmF0aW9uOiBmdW5jdGlvbiAoKSB7XG5cblx0XHR2YXIgYWNjRGF0YSA9IHNlbnNvcnMubW90aW9uLmRhdGEgJiYgc2Vuc29ycy5tb3Rpb24uZGF0YS5hY2NlbGVyYXRpb24gPyBzZW5zb3JzLm1vdGlvbi5kYXRhLmFjY2VsZXJhdGlvbiA6IHsgeDogMCwgeTogMCwgejogMCB9O1xuXHRcdHZhciBzY3JlZW5BY2NEYXRhID0ge307XG5cblx0XHRzd2l0Y2ggKCBzY3JlZW5PcmllbnRhdGlvbkFuZ2xlICkge1xuXHRcdFx0Y2FzZSBTQ1JFRU5fUk9UQVRJT05fOTA6XG5cdFx0XHRcdHNjcmVlbkFjY0RhdGEueCA9IC0gYWNjRGF0YS55O1xuXHRcdFx0XHRzY3JlZW5BY2NEYXRhLnkgPSAgIGFjY0RhdGEueDtcblx0XHRcdFx0YnJlYWs7XG5cdFx0XHRjYXNlIFNDUkVFTl9ST1RBVElPTl8xODA6XG5cdFx0XHRcdHNjcmVlbkFjY0RhdGEueCA9IC0gYWNjRGF0YS54O1xuXHRcdFx0XHRzY3JlZW5BY2NEYXRhLnkgPSAtIGFjY0RhdGEueTtcblx0XHRcdFx0YnJlYWs7XG5cdFx0XHRjYXNlIFNDUkVFTl9ST1RBVElPTl8yNzA6XG5cdFx0XHRjYXNlIFNDUkVFTl9ST1RBVElPTl9NSU5VU185MDpcblx0XHRcdFx0c2NyZWVuQWNjRGF0YS54ID0gICBhY2NEYXRhLnk7XG5cdFx0XHRcdHNjcmVlbkFjY0RhdGEueSA9IC0gYWNjRGF0YS54O1xuXHRcdFx0XHRicmVhaztcblx0XHRcdGRlZmF1bHQ6IC8vIFNDUkVFTl9ST1RBVElPTl8wXG5cdFx0XHRcdHNjcmVlbkFjY0RhdGEueCA9ICAgYWNjRGF0YS54O1xuXHRcdFx0XHRzY3JlZW5BY2NEYXRhLnkgPSAgIGFjY0RhdGEueTtcblx0XHRcdFx0YnJlYWs7XG5cdFx0fVxuXG5cdFx0c2NyZWVuQWNjRGF0YS56ID0gYWNjRGF0YS56O1xuXG5cdFx0cmV0dXJuIHNjcmVlbkFjY0RhdGE7XG5cblx0fSxcblxuXHRnZXRTY3JlZW5BZGp1c3RlZEFjY2VsZXJhdGlvbkluY2x1ZGluZ0dyYXZpdHk6IGZ1bmN0aW9uICgpIHtcblxuXHRcdHZhciBhY2NHRGF0YSA9IHNlbnNvcnMubW90aW9uLmRhdGEgJiYgc2Vuc29ycy5tb3Rpb24uZGF0YS5hY2NlbGVyYXRpb25JbmNsdWRpbmdHcmF2aXR5ID8gc2Vuc29ycy5tb3Rpb24uZGF0YS5hY2NlbGVyYXRpb25JbmNsdWRpbmdHcmF2aXR5IDogeyB4OiAwLCB5OiAwLCB6OiAwIH07XG5cdFx0dmFyIHNjcmVlbkFjY0dEYXRhID0ge307XG5cblx0XHRzd2l0Y2ggKCBzY3JlZW5PcmllbnRhdGlvbkFuZ2xlICkge1xuXHRcdFx0Y2FzZSBTQ1JFRU5fUk9UQVRJT05fOTA6XG5cdFx0XHRcdHNjcmVlbkFjY0dEYXRhLnggPSAtIGFjY0dEYXRhLnk7XG5cdFx0XHRcdHNjcmVlbkFjY0dEYXRhLnkgPSAgIGFjY0dEYXRhLng7XG5cdFx0XHRcdGJyZWFrO1xuXHRcdFx0Y2FzZSBTQ1JFRU5fUk9UQVRJT05fMTgwOlxuXHRcdFx0XHRzY3JlZW5BY2NHRGF0YS54ID0gLSBhY2NHRGF0YS54O1xuXHRcdFx0XHRzY3JlZW5BY2NHRGF0YS55ID0gLSBhY2NHRGF0YS55O1xuXHRcdFx0XHRicmVhaztcblx0XHRcdGNhc2UgU0NSRUVOX1JPVEFUSU9OXzI3MDpcblx0XHRcdGNhc2UgU0NSRUVOX1JPVEFUSU9OX01JTlVTXzkwOlxuXHRcdFx0XHRzY3JlZW5BY2NHRGF0YS54ID0gICBhY2NHRGF0YS55O1xuXHRcdFx0XHRzY3JlZW5BY2NHRGF0YS55ID0gLSBhY2NHRGF0YS54O1xuXHRcdFx0XHRicmVhaztcblx0XHRcdGRlZmF1bHQ6IC8vIFNDUkVFTl9ST1RBVElPTl8wXG5cdFx0XHRcdHNjcmVlbkFjY0dEYXRhLnggPSAgIGFjY0dEYXRhLng7XG5cdFx0XHRcdHNjcmVlbkFjY0dEYXRhLnkgPSAgIGFjY0dEYXRhLnk7XG5cdFx0XHRcdGJyZWFrO1xuXHRcdH1cblxuXHRcdHNjcmVlbkFjY0dEYXRhLnogPSBhY2NHRGF0YS56O1xuXG5cdFx0cmV0dXJuIHNjcmVlbkFjY0dEYXRhO1xuXG5cdH0sXG5cblx0Z2V0U2NyZWVuQWRqdXN0ZWRSb3RhdGlvblJhdGU6IGZ1bmN0aW9uICgpIHtcblxuXHRcdHZhciByb3RSYXRlRGF0YSA9IHNlbnNvcnMubW90aW9uLmRhdGEgJiYgc2Vuc29ycy5tb3Rpb24uZGF0YS5yb3RhdGlvblJhdGUgPyBzZW5zb3JzLm1vdGlvbi5kYXRhLnJvdGF0aW9uUmF0ZSA6IHsgYWxwaGE6IDAsIGJldGE6IDAsIGdhbW1hOiAwIH07XG5cdFx0dmFyIHNjcmVlblJvdFJhdGVEYXRhID0ge307XG5cblx0XHRzd2l0Y2ggKCBzY3JlZW5PcmllbnRhdGlvbkFuZ2xlICkge1xuXHRcdFx0Y2FzZSBTQ1JFRU5fUk9UQVRJT05fOTA6XG5cdFx0XHRcdHNjcmVlblJvdFJhdGVEYXRhLmJldGEgID0gLSByb3RSYXRlRGF0YS5nYW1tYTtcblx0XHRcdFx0c2NyZWVuUm90UmF0ZURhdGEuZ2FtbWEgPSAgIHJvdFJhdGVEYXRhLmJldGE7XG5cdFx0XHRcdGJyZWFrO1xuXHRcdFx0Y2FzZSBTQ1JFRU5fUk9UQVRJT05fMTgwOlxuXHRcdFx0XHRzY3JlZW5Sb3RSYXRlRGF0YS5iZXRhICA9IC0gcm90UmF0ZURhdGEuYmV0YTtcblx0XHRcdFx0c2NyZWVuUm90UmF0ZURhdGEuZ2FtbWEgPSAtIHJvdFJhdGVEYXRhLmdhbW1hO1xuXHRcdFx0XHRicmVhaztcblx0XHRcdGNhc2UgU0NSRUVOX1JPVEFUSU9OXzI3MDpcblx0XHRcdGNhc2UgU0NSRUVOX1JPVEFUSU9OX01JTlVTXzkwOlxuXHRcdFx0XHRzY3JlZW5Sb3RSYXRlRGF0YS5iZXRhICA9ICAgcm90UmF0ZURhdGEuZ2FtbWE7XG5cdFx0XHRcdHNjcmVlblJvdFJhdGVEYXRhLmdhbW1hID0gLSByb3RSYXRlRGF0YS5iZXRhO1xuXHRcdFx0XHRicmVhaztcblx0XHRcdGRlZmF1bHQ6IC8vIFNDUkVFTl9ST1RBVElPTl8wXG5cdFx0XHRcdHNjcmVlblJvdFJhdGVEYXRhLmJldGEgID0gICByb3RSYXRlRGF0YS5iZXRhO1xuXHRcdFx0XHRzY3JlZW5Sb3RSYXRlRGF0YS5nYW1tYSA9ICAgcm90UmF0ZURhdGEuZ2FtbWE7XG5cdFx0XHRcdGJyZWFrO1xuXHRcdH1cblxuXHRcdHNjcmVlblJvdFJhdGVEYXRhLmFscGhhID0gcm90UmF0ZURhdGEuYWxwaGE7XG5cblx0XHRyZXR1cm4gc2NyZWVuUm90UmF0ZURhdGE7XG5cblx0fSxcblxuXHRnZXRMYXN0UmF3RXZlbnREYXRhOiBmdW5jdGlvbiAoKSB7XG5cblx0XHRyZXR1cm4gc2Vuc29ycy5tb3Rpb24uZGF0YSB8fCB7fTtcblxuXHR9XG5cbn07XG5cblxuLy8vLy8vIEF0dGFjaCBGVUxMVElMVCB0byByb290IERPTSBlbGVtZW50IC8vLy8vL1xuXG53aW5kb3cuRlVMTFRJTFQgPSBGVUxMVElMVDtcblxufSkoIHdpbmRvdyApOyIsImltcG9ydCBSQUYgZnJvbSAnLi9SQUYnXG5yZXF1aXJlKCdAaHVnaHNrL2Z1bGx0aWx0L2Rpc3QvZnVsbHRpbHQnKVxuLyoqXG4gKiBBbiB1dGlsaXR5IHNpbmdsZXRvbiB0aGF0IHJldHVybnMgYSBkZXZpY2UncyBHeXJvc2NvcGVcbiAqIGRhdGEgaW4gdGhlIGZvcm0gb2Ygb3JpZW50YXRpb25YIGFuZCBvcmllbnRhdGlvblkuXG4gKlxuICogVGhlIHZhbHVlcyBhcmUgdXBkYXRlZCBldmVyeSBmcmFtZSBhbmQgdGhlIHN5c3RlbSBzdG9wcyB1cGRhdGluZ1xuICogaWYgaXQgZGV0ZXJtaW5lcyB0aGF0IHRoZSBkZXZpY2UgZG9lcyBub3QgaGF2ZSBneXJvc2NvcGUgY2FwYWJpbGl0aWVzLlxuICpcbiAqIEBjbGFzcyBHeXJvc2NvcGVcbiAqIEBwcm9wZXJ0eSB7TnVtYmVyfSBvcmllbnRhdGlvblhcbiAqIEBwcm9wZXJ0eSB7TnVtYmVyfSBvcmllbnRhdGlvbllcbiAqIEBwcm9wZXJ0eSB7Qm9vbGVhbn0gZW5hYmxlZFxuICovXG5jbGFzcyBHeXJvc2NvcGUge1xuICAvKipcbiAgICogQGlnbm9yZVxuICAgKi9cbiAgY29uc3RydWN0b3IgKCkge1xuICAgIHRoaXMub3JpZW50YXRpb25YID0gMFxuICAgIHRoaXMub3JpZW50YXRpb25ZID0gMFxuICAgIHRoaXMuZW5hYmxlZCA9IHRydWVcbiAgICB0aGlzLl91cGRhdGUgPSB0aGlzLl91cGRhdGUuYmluZCh0aGlzKVxuICAgIFJBRi5hZGQodGhpcy5fdXBkYXRlKVxuICB9XG5cbiAgLyoqXG4gICAqIEBpZ25vcmVcbiAgICovXG4gIF91cGRhdGUgKCkge1xuICAgIGNvbnN0IGZ1bGx0aWx0UHJvbWlzZSA9IG5ldyB3aW5kb3cuRlVMTFRJTFQuZ2V0RGV2aWNlT3JpZW50YXRpb24oeyB0eXBlOiAnZ2FtZScgfSkgLy8gZXNsaW50LWRpc2FibGUtbGluZSBuZXctY2FwXG5cbiAgICBmdWxsdGlsdFByb21pc2UudGhlbigoZGV2aWNlT3JpZW50YXRpb24pID0+IHtcbiAgICAgIGNvbnN0IGV1bGVyID0gZGV2aWNlT3JpZW50YXRpb24uZ2V0U2NyZWVuQWRqdXN0ZWRFdWxlcigpXG5cbiAgICAgIHRoaXMub3JpZW50YXRpb25YID0gZXVsZXIuYmV0YVxuICAgICAgdGhpcy5vcmllbnRhdGlvblkgPSBldWxlci5nYW1tYVxuXG4gICAgICBpZiAodGhpcy5vcmllbnRhdGlvblggPiA5MCkge1xuICAgICAgICB0aGlzLm9yaWVudGF0aW9uWSA9IC10aGlzLm9yaWVudGF0aW9uWVxuICAgICAgfVxuICAgICAgcmV0dXJuIG51bGxcbiAgICAgIC8vIGNvbnNvbGUubG9nKHRoaXMub3JpZW50YXRpb25YLCB0aGlzLm9yaWVudGF0aW9uWSlcbiAgICB9KS5jYXRjaCgobWVzc2FnZSkgPT4ge1xuICAgICAgY29uc29sZS53YXJuKCdEaXNhYmxpbmcgR3lyb3Njb3BlLCByZWFzb246JywgbWVzc2FnZSlcbiAgICAgIHRoaXMuZW5hYmxlZCA9IGZhbHNlXG4gICAgICBSQUYucmVtb3ZlKHRoaXMuX3VwZGF0ZSlcbiAgICB9KVxuICB9XG59XG4vKipcbiAqIEBpZ25vcmVcbiAqL1xuZXhwb3J0IGRlZmF1bHQgR3lyb3Njb3BlXG4iLCIvKipcbiAqIEFuIHV0aWxpdHkgc2luZ2xldG9uIHRoYXQgaG9sZHMgYWxsIHJlcXVlc3RBbmltYXRpb25GcmFtZSBzdWJzY3JpYmVyc1xuICogYW5kIGNhbGxzIHRoZW0gaW4gc2VxdWVuY2UgZWFjaCBmcmFtZS5cbiAqXG4gKiBAY2xhc3MgUkFGXG4gKiBAZXhhbXBsZVxuICpcbiAqIGltcG9ydCBSQUYgZnJvbSAnanMtdXRpbHMnXG4gKlxuICogY29uc3RydWN0b3IgKCkge1xuICogIFJBRi5hZGQodGhpcy5vblVwZGF0ZSlcbiAqIH1cbiAqXG4gKiBvblVwZGF0ZSAoKSB7XG4gKiAgLy8gZG8gc3R1ZmZcbiAqIH1cbiAqXG4gKiBvbkRlc3Ryb3kgKCkge1xuICogIFJBRi5yZW1vdmUodGhpcy5vblVwZGF0ZSlcbiAqIH1cbiAqL1xuY2xhc3MgUkFGIHtcbiAgY29uc3RydWN0b3IgKCkge1xuICAgIHRoaXMubGlzdGVuZXJzID0gW11cbiAgICB0aGlzLl91cGRhdGUgPSB0aGlzLl91cGRhdGUuYmluZCh0aGlzKVxuICAgIHRoaXMuX3VwZGF0ZSgpXG4gIH1cblxuICAvKipcbiAgICogQWRkcyBhIHN1YnNjcmliZXIgdG8gYmUgY2FsbGVkIGF0IGVhY2ggcmVxdWVzdEFuaW1hdGlvbkZyYW1lXG4gICAqXG4gICAqIEBwYXJhbSB7RnVuY3Rpb259IGxpc3RlbmVyIEEgc3Vic2NyaWJlciBmdW5jdGlvblxuICAgKi9cbiAgYWRkIChsaXN0ZW5lcikge1xuICAgIGNvbnN0IGluZGV4ID0gdGhpcy5saXN0ZW5lcnMuaW5kZXhPZihsaXN0ZW5lcilcbiAgICBpZiAoaW5kZXggPT09IC0xKSB7XG4gICAgICB0aGlzLmxpc3RlbmVycy5wdXNoKGxpc3RlbmVyKVxuICAgIH1cbiAgfVxuXG4gIC8qKlxuICAgKiBSZW1vdmVzIGEgc3Vic2NyaWJlciBmcm9tIHJlcXVlc3RBbmltYXRpb25GcmFtZVxuICAgKlxuICAgKiBAcGFyYW0ge0Z1bmN0aW9ufSBsaXN0ZW5lciBBIHN1YnNjcmliZXIgZnVuY3Rpb25cbiAgICovXG4gIHJlbW92ZSAobGlzdGVuZXIpIHtcbiAgICBjb25zdCBpbmRleCA9IHRoaXMubGlzdGVuZXJzLmluZGV4T2YobGlzdGVuZXIpXG4gICAgaWYgKGluZGV4ICE9PSAtMSkge1xuICAgICAgdGhpcy5saXN0ZW5lcnMuc3BsaWNlKGluZGV4LCAxKVxuICAgIH1cbiAgfVxuXG4gIC8qKlxuICAgKiBAaWdub3JlXG4gICAqL1xuICBfdXBkYXRlICgpIHtcbiAgICBmb3IgKGxldCBpID0gMDsgaSA8IHRoaXMubGlzdGVuZXJzLmxlbmd0aDsgaSsrKSB7XG4gICAgICB0aGlzLmxpc3RlbmVyc1tpXSgpXG4gICAgfVxuICAgIHdpbmRvdy5yZXF1ZXN0QW5pbWF0aW9uRnJhbWUodGhpcy5fdXBkYXRlKVxuICB9XG59XG4vKipcbiAqIEBpZ25vcmVcbiAqL1xuZXhwb3J0IGRlZmF1bHQgbmV3IFJBRigpXG4iXSwic291cmNlUm9vdCI6IiJ9