(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define("js-utils", [], factory);
	else if(typeof exports === 'object')
		exports["js-utils"] = factory();
	else
		root["js-utils"] = factory();
})(typeof self !== 'undefined' ? self : this, function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/NumberUtils.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/NumberUtils.js":
/*!****************************!*\
  !*** ./src/NumberUtils.js ***!
  \****************************/
/*! exports provided: lerpNumber, distanceCompare, distance, mapToPower, randByPower, smoothstep, angleDistanceSign, angleDistance, map, clamp, range, mix */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "lerpNumber", function() { return lerpNumber; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "distanceCompare", function() { return distanceCompare; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "distance", function() { return distance; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "mapToPower", function() { return mapToPower; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "randByPower", function() { return randByPower; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "smoothstep", function() { return smoothstep; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "angleDistanceSign", function() { return angleDistanceSign; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "angleDistance", function() { return angleDistance; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "map", function() { return map; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "clamp", function() { return clamp; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "range", function() { return range; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "mix", function() { return mix; });
/**
 * A collection of Number related utilities
 *
 * @module NumberUtils
 */

/**
  * Linearly interpolates a Number
  *
  * @param {Number} v0 Initial value
  * @param {Number} v1 Final value
  * @param {Number} t zero to one
  * @returns {Number} Interpolation between v0 and v1 based on t
  */
var lerpNumber = function lerpNumber(v0, v1, t) {
  return v0 + t * (v1 - v0);
};
/**
 * TODO: Documentation
 *
 * @param {*} oldValue
 * @param {*} oldMin
 * @param {*} oldMax
 * @param {*} newMin
 * @param {*} newMax
 * @param {*} clamped
 */


var range = function range(oldValue, oldMin, oldMax, newMin, newMax, clamped) {
  var oldRange = oldMax - oldMin;
  var newRange = newMax - newMin;
  var newValue = (oldValue - oldMin) * newRange / oldRange + newMin;

  if (clamped) {
    return clamp(newValue, newMin, newMax);
  }

  return newValue;
};
/**
 * Clamps a value between min and max values
 *
 * @param {Number} value - The value to be clamped.
 * @param {Number} min - Minimum value.
 * @param {Number} max - Maximum value.
 * @returns {Number} A number clamped between min and max
 */


var clamp = function clamp(value) {
  var min = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
  var max = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 1;
  return Math.max(min, Math.min(value, max));
};
/**
 * TODO: Documentation
 *
 * @param {Number} value
 * @param {Number} min
 * @param {Number} max
 */


var map = function map(value, min, max) {
  return clamp((value - min) / (max - min), 0, 1);
};
/**
 * TODO: Documentation
 *
 * @param {Number} alpha
 * @param {Number} beta
 */


var angleDistance = function angleDistance(alpha, beta) {
  var phi = Math.abs(beta - alpha) % Math.PI * 2;
  return phi > Math.PI ? Math.PI * 2 - phi : phi;
};
/**
 * TODO: Documentation
 *
 * @param {Number} alpha
 * @param {Number} beta
 */


var angleDistanceSign = function angleDistanceSign(alpha, beta) {
  var sign = alpha - beta >= 0 && alpha - beta <= Math.PI || alpha - beta <= -Math.PI && alpha - beta >= -Math.PI * 2 ? 1 : -1;
  return angleDistance(alpha, beta) * sign;
};
/**
 * Smoothstep implementation.
 * https://en.wikipedia.org/wiki/Smoothstep
 *
 * @param {Number} min - Initial value
 * @param {Number} max - Final Value
 * @param {Number} value -
 * @returns {Number} Value of the interpolation
 */


var smoothstep = function smoothstep(min, max, value) {
  var x = Math.max(0, Math.min(1, (value - min) / (max - min)));
  return x * x * (3 - 2 * x);
};
/**
 * Similar to linear interpolation.
 *
 * @param {Number} value1 - Minimum value.
 * @param {Number} value2 - Maximum value.
 * @param {Number} percent - zero to one value percent
 */


var mix = function mix(value1, value2, percent) {
  return value1 * (1 - percent) + value2 * percent;
};
/**
 * Returns the sign of a number in -1,0,1 form.
 *
 * @param {Number} value - a number to be tested
 * @returns {Number} 0 if value is zero, -1 if value is less than 0, 1 otherwise
 */


var sign = function sign(value) {
  return value !== 0 ? value < 0 ? -1 : 1 : 0;
};
/**
 * TODO: Documentation
 *
 * @param {Number} min
 * @param {Number} max
 * @param {Number} power
 * @param {Number} [rd = Math.random] - Optional Random number generator
 */


var randByPower = function randByPower(min, max, power) {
  var rd = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : Math.random;
  var value = rd() * (max - min) + min;
  return mapToPower(value, min, max, power);
};
/**
 * TODO: Documentation
 *
 * @param {Number} value
 * @param {Number} min
 * @param {Number} max
 * @param {Number} power
 */


var mapToPower = function mapToPower(value, min, max, power) {
  var valueSign = sign(value);
  value = (value - min) / (max - min);
  value = valueSign > 0 ? value : value - 1;
  value = Math.pow(value * valueSign, Math.abs(power));
  value = valueSign > 0 ? value : 1 - value;
  return value * (max - min) + min;
};
/**
 * Computes distance between two points in 2D space.
 *
 * @param {Number} x1 - x coordinate of first point
 * @param {Number} y1 - y coordinate of first point
 * @param {Number} x2 - x coordinate of second point
 * @param {Number} y2 - y coordinate of second point
 * @returns {Number} Distance between x1,y1 and x2,y2
 */


var distance = function distance(x1, y1, x2, y2) {
  var dx = x1 - x2;
  var dy = y1 - y2;
  return Math.sqrt(dx * dx + dy * dy);
};
/**
 * Not a real distance calculation.
 * Useful to sort objects by distance ( much faster because no sqrt )
 *
 * @param {Number} x1 - x coordinate of first point
 * @param {Number} y1 - y coordinate of first point
 * @param {Number} x2 - x coordinate of second point
 * @param {Number} y2 - y coordinate of second point
 * @returns {Number} bogus distance between x1,y1 and x2,y2. DO NOT USE WHEN A REAL DISTANCE IS NEEDED
 */


var distanceCompare = function distanceCompare(x1, y1, x2, y2) {
  var dx = x1 - x2;
  var dy = y1 - y2;
  return dx * dx + dy * dy;
};



/***/ })

/******/ });
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9qcy11dGlscy93ZWJwYWNrL3VuaXZlcnNhbE1vZHVsZURlZmluaXRpb24iLCJ3ZWJwYWNrOi8vanMtdXRpbHMvd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vanMtdXRpbHMvLi9zcmMvTnVtYmVyVXRpbHMuanMiXSwibmFtZXMiOlsibGVycE51bWJlciIsInYwIiwidjEiLCJ0IiwicmFuZ2UiLCJvbGRWYWx1ZSIsIm9sZE1pbiIsIm9sZE1heCIsIm5ld01pbiIsIm5ld01heCIsImNsYW1wZWQiLCJvbGRSYW5nZSIsIm5ld1JhbmdlIiwibmV3VmFsdWUiLCJjbGFtcCIsInZhbHVlIiwibWluIiwibWF4IiwiTWF0aCIsIm1hcCIsImFuZ2xlRGlzdGFuY2UiLCJhbHBoYSIsImJldGEiLCJwaGkiLCJhYnMiLCJQSSIsImFuZ2xlRGlzdGFuY2VTaWduIiwic2lnbiIsInNtb290aHN0ZXAiLCJ4IiwibWl4IiwidmFsdWUxIiwidmFsdWUyIiwicGVyY2VudCIsInJhbmRCeVBvd2VyIiwicG93ZXIiLCJyZCIsInJhbmRvbSIsIm1hcFRvUG93ZXIiLCJ2YWx1ZVNpZ24iLCJwb3ciLCJkaXN0YW5jZSIsIngxIiwieTEiLCJ4MiIsInkyIiwiZHgiLCJkeSIsInNxcnQiLCJkaXN0YW5jZUNvbXBhcmUiXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUM7QUFDRCxPO1FDVkE7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7OztRQUdBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSwwQ0FBMEMsZ0NBQWdDO1FBQzFFO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0Esd0RBQXdELGtCQUFrQjtRQUMxRTtRQUNBLGlEQUFpRCxjQUFjO1FBQy9EOztRQUVBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQSx5Q0FBeUMsaUNBQWlDO1FBQzFFLGdIQUFnSCxtQkFBbUIsRUFBRTtRQUNySTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLDJCQUEyQiwwQkFBMEIsRUFBRTtRQUN2RCxpQ0FBaUMsZUFBZTtRQUNoRDtRQUNBO1FBQ0E7O1FBRUE7UUFDQSxzREFBc0QsK0RBQStEOztRQUVySDtRQUNBOzs7UUFHQTtRQUNBOzs7Ozs7Ozs7Ozs7O0FDbEZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7Ozs7OztBQU1BOzs7Ozs7OztBQVFBLElBQU1BLFVBQVUsR0FBRyxTQUFiQSxVQUFhLENBQVVDLEVBQVYsRUFBY0MsRUFBZCxFQUFrQkMsQ0FBbEIsRUFBcUI7QUFDdEMsU0FBT0YsRUFBRSxHQUFHRSxDQUFDLElBQUlELEVBQUUsR0FBR0QsRUFBVCxDQUFiO0FBQ0QsQ0FGRDtBQUlBOzs7Ozs7Ozs7Ozs7QUFVQSxJQUFNRyxLQUFLLEdBQUcsU0FBUkEsS0FBUSxDQUFVQyxRQUFWLEVBQW9CQyxNQUFwQixFQUE0QkMsTUFBNUIsRUFBb0NDLE1BQXBDLEVBQTRDQyxNQUE1QyxFQUFvREMsT0FBcEQsRUFBNkQ7QUFDekUsTUFBTUMsUUFBUSxHQUFHSixNQUFNLEdBQUdELE1BQTFCO0FBQ0EsTUFBTU0sUUFBUSxHQUFHSCxNQUFNLEdBQUdELE1BQTFCO0FBQ0EsTUFBTUssUUFBUSxHQUFJLENBQUNSLFFBQVEsR0FBR0MsTUFBWixJQUFzQk0sUUFBdkIsR0FBbUNELFFBQW5DLEdBQThDSCxNQUEvRDs7QUFFQSxNQUFJRSxPQUFKLEVBQWE7QUFDWCxXQUFPSSxLQUFLLENBQUNELFFBQUQsRUFBV0wsTUFBWCxFQUFtQkMsTUFBbkIsQ0FBWjtBQUNEOztBQUVELFNBQU9JLFFBQVA7QUFDRCxDQVZEO0FBV0E7Ozs7Ozs7Ozs7QUFRQSxJQUFNQyxLQUFLLEdBQUcsU0FBUkEsS0FBUSxDQUFDQyxLQUFEO0FBQUEsTUFBUUMsR0FBUix1RUFBYyxDQUFkO0FBQUEsTUFBaUJDLEdBQWpCLHVFQUF1QixDQUF2QjtBQUFBLFNBQTZCQyxJQUFJLENBQUNELEdBQUwsQ0FBU0QsR0FBVCxFQUFjRSxJQUFJLENBQUNGLEdBQUwsQ0FBU0QsS0FBVCxFQUFnQkUsR0FBaEIsQ0FBZCxDQUE3QjtBQUFBLENBQWQ7QUFDQTs7Ozs7Ozs7O0FBT0EsSUFBTUUsR0FBRyxHQUFHLFNBQU5BLEdBQU0sQ0FBQ0osS0FBRCxFQUFRQyxHQUFSLEVBQWFDLEdBQWI7QUFBQSxTQUFxQkgsS0FBSyxDQUFDLENBQUNDLEtBQUssR0FBR0MsR0FBVCxLQUFpQkMsR0FBRyxHQUFHRCxHQUF2QixDQUFELEVBQThCLENBQTlCLEVBQWlDLENBQWpDLENBQTFCO0FBQUEsQ0FBWjtBQUNBOzs7Ozs7OztBQU1BLElBQU1JLGFBQWEsR0FBRyxTQUFoQkEsYUFBZ0IsQ0FBVUMsS0FBVixFQUFpQkMsSUFBakIsRUFBdUI7QUFDM0MsTUFBTUMsR0FBRyxHQUFJTCxJQUFJLENBQUNNLEdBQUwsQ0FBU0YsSUFBSSxHQUFHRCxLQUFoQixJQUF5QkgsSUFBSSxDQUFDTyxFQUEvQixHQUFxQyxDQUFqRDtBQUNBLFNBQU9GLEdBQUcsR0FBR0wsSUFBSSxDQUFDTyxFQUFYLEdBQWdCUCxJQUFJLENBQUNPLEVBQUwsR0FBVSxDQUFWLEdBQWNGLEdBQTlCLEdBQW9DQSxHQUEzQztBQUNELENBSEQ7QUFJQTs7Ozs7Ozs7QUFNQSxJQUFNRyxpQkFBaUIsR0FBRyxTQUFwQkEsaUJBQW9CLENBQVVMLEtBQVYsRUFBaUJDLElBQWpCLEVBQXVCO0FBQy9DLE1BQU1LLElBQUksR0FDUE4sS0FBSyxHQUFHQyxJQUFSLElBQWdCLENBQWhCLElBQXFCRCxLQUFLLEdBQUdDLElBQVIsSUFBZ0JKLElBQUksQ0FBQ08sRUFBM0MsSUFDQ0osS0FBSyxHQUFHQyxJQUFSLElBQWdCLENBQUNKLElBQUksQ0FBQ08sRUFBdEIsSUFBNEJKLEtBQUssR0FBR0MsSUFBUixJQUFnQixDQUFDSixJQUFJLENBQUNPLEVBQU4sR0FBVyxDQUR4RCxHQUVJLENBRkosR0FHSSxDQUFDLENBSlA7QUFLQSxTQUFPTCxhQUFhLENBQUNDLEtBQUQsRUFBUUMsSUFBUixDQUFiLEdBQTZCSyxJQUFwQztBQUNELENBUEQ7QUFRQTs7Ozs7Ozs7Ozs7QUFTQSxJQUFNQyxVQUFVLEdBQUcsU0FBYkEsVUFBYSxDQUFDWixHQUFELEVBQU1DLEdBQU4sRUFBV0YsS0FBWCxFQUFxQjtBQUN0QyxNQUFNYyxDQUFDLEdBQUdYLElBQUksQ0FBQ0QsR0FBTCxDQUFTLENBQVQsRUFBWUMsSUFBSSxDQUFDRixHQUFMLENBQVMsQ0FBVCxFQUFZLENBQUNELEtBQUssR0FBR0MsR0FBVCxLQUFpQkMsR0FBRyxHQUFHRCxHQUF2QixDQUFaLENBQVosQ0FBVjtBQUNBLFNBQU9hLENBQUMsR0FBR0EsQ0FBSixJQUFTLElBQUksSUFBSUEsQ0FBakIsQ0FBUDtBQUNELENBSEQ7QUFJQTs7Ozs7Ozs7O0FBT0EsSUFBTUMsR0FBRyxHQUFHLFNBQU5BLEdBQU0sQ0FBQ0MsTUFBRCxFQUFTQyxNQUFULEVBQWlCQyxPQUFqQjtBQUFBLFNBQTZCRixNQUFNLElBQUksSUFBSUUsT0FBUixDQUFOLEdBQXlCRCxNQUFNLEdBQUdDLE9BQS9EO0FBQUEsQ0FBWjtBQUNBOzs7Ozs7OztBQU1BLElBQU1OLElBQUksR0FBRyxTQUFQQSxJQUFPLENBQUFaLEtBQUs7QUFBQSxTQUFLQSxLQUFLLEtBQUssQ0FBVixHQUFlQSxLQUFLLEdBQUcsQ0FBUixHQUFZLENBQUMsQ0FBYixHQUFpQixDQUFoQyxHQUFxQyxDQUExQztBQUFBLENBQWxCO0FBQ0E7Ozs7Ozs7Ozs7QUFRQSxJQUFNbUIsV0FBVyxHQUFHLFNBQWRBLFdBQWMsQ0FBVWxCLEdBQVYsRUFBZUMsR0FBZixFQUFvQmtCLEtBQXBCLEVBQTZDO0FBQUEsTUFBbEJDLEVBQWtCLHVFQUFibEIsSUFBSSxDQUFDbUIsTUFBUTtBQUMvRCxNQUFNdEIsS0FBSyxHQUFHcUIsRUFBRSxNQUFNbkIsR0FBRyxHQUFHRCxHQUFaLENBQUYsR0FBcUJBLEdBQW5DO0FBQ0EsU0FBT3NCLFVBQVUsQ0FBQ3ZCLEtBQUQsRUFBUUMsR0FBUixFQUFhQyxHQUFiLEVBQWtCa0IsS0FBbEIsQ0FBakI7QUFDRCxDQUhEO0FBSUE7Ozs7Ozs7Ozs7QUFRQSxJQUFNRyxVQUFVLEdBQUcsU0FBYkEsVUFBYSxDQUFVdkIsS0FBVixFQUFpQkMsR0FBakIsRUFBc0JDLEdBQXRCLEVBQTJCa0IsS0FBM0IsRUFBa0M7QUFDbkQsTUFBTUksU0FBUyxHQUFHWixJQUFJLENBQUNaLEtBQUQsQ0FBdEI7QUFFQUEsT0FBSyxHQUFHLENBQUNBLEtBQUssR0FBR0MsR0FBVCxLQUFpQkMsR0FBRyxHQUFHRCxHQUF2QixDQUFSO0FBQ0FELE9BQUssR0FBR3dCLFNBQVMsR0FBRyxDQUFaLEdBQWdCeEIsS0FBaEIsR0FBd0JBLEtBQUssR0FBRyxDQUF4QztBQUNBQSxPQUFLLEdBQUdHLElBQUksQ0FBQ3NCLEdBQUwsQ0FBU3pCLEtBQUssR0FBR3dCLFNBQWpCLEVBQTRCckIsSUFBSSxDQUFDTSxHQUFMLENBQVNXLEtBQVQsQ0FBNUIsQ0FBUjtBQUNBcEIsT0FBSyxHQUFHd0IsU0FBUyxHQUFHLENBQVosR0FBZ0J4QixLQUFoQixHQUF3QixJQUFJQSxLQUFwQztBQUVBLFNBQU9BLEtBQUssSUFBSUUsR0FBRyxHQUFHRCxHQUFWLENBQUwsR0FBc0JBLEdBQTdCO0FBQ0QsQ0FURDtBQVVBOzs7Ozs7Ozs7OztBQVNBLElBQU15QixRQUFRLEdBQUcsU0FBWEEsUUFBVyxDQUFVQyxFQUFWLEVBQWNDLEVBQWQsRUFBa0JDLEVBQWxCLEVBQXNCQyxFQUF0QixFQUEwQjtBQUN6QyxNQUFNQyxFQUFFLEdBQUdKLEVBQUUsR0FBR0UsRUFBaEI7QUFDQSxNQUFNRyxFQUFFLEdBQUdKLEVBQUUsR0FBR0UsRUFBaEI7QUFDQSxTQUFPM0IsSUFBSSxDQUFDOEIsSUFBTCxDQUFVRixFQUFFLEdBQUdBLEVBQUwsR0FBVUMsRUFBRSxHQUFHQSxFQUF6QixDQUFQO0FBQ0QsQ0FKRDtBQU1BOzs7Ozs7Ozs7Ozs7QUFVQSxJQUFNRSxlQUFlLEdBQUcsU0FBbEJBLGVBQWtCLENBQVVQLEVBQVYsRUFBY0MsRUFBZCxFQUFrQkMsRUFBbEIsRUFBc0JDLEVBQXRCLEVBQTBCO0FBQ2hELE1BQU1DLEVBQUUsR0FBR0osRUFBRSxHQUFHRSxFQUFoQjtBQUNBLE1BQU1HLEVBQUUsR0FBR0osRUFBRSxHQUFHRSxFQUFoQjtBQUNBLFNBQU9DLEVBQUUsR0FBR0EsRUFBTCxHQUFVQyxFQUFFLEdBQUdBLEVBQXRCO0FBQ0QsQ0FKRCIsImZpbGUiOiJOdW1iZXJVdGlscy5qcyIsInNvdXJjZXNDb250ZW50IjpbIihmdW5jdGlvbiB3ZWJwYWNrVW5pdmVyc2FsTW9kdWxlRGVmaW5pdGlvbihyb290LCBmYWN0b3J5KSB7XG5cdGlmKHR5cGVvZiBleHBvcnRzID09PSAnb2JqZWN0JyAmJiB0eXBlb2YgbW9kdWxlID09PSAnb2JqZWN0Jylcblx0XHRtb2R1bGUuZXhwb3J0cyA9IGZhY3RvcnkoKTtcblx0ZWxzZSBpZih0eXBlb2YgZGVmaW5lID09PSAnZnVuY3Rpb24nICYmIGRlZmluZS5hbWQpXG5cdFx0ZGVmaW5lKFwianMtdXRpbHNcIiwgW10sIGZhY3RvcnkpO1xuXHRlbHNlIGlmKHR5cGVvZiBleHBvcnRzID09PSAnb2JqZWN0Jylcblx0XHRleHBvcnRzW1wianMtdXRpbHNcIl0gPSBmYWN0b3J5KCk7XG5cdGVsc2Vcblx0XHRyb290W1wianMtdXRpbHNcIl0gPSBmYWN0b3J5KCk7XG59KSh0eXBlb2Ygc2VsZiAhPT0gJ3VuZGVmaW5lZCcgPyBzZWxmIDogdGhpcywgZnVuY3Rpb24oKSB7XG5yZXR1cm4gIiwiIFx0Ly8gVGhlIG1vZHVsZSBjYWNoZVxuIFx0dmFyIGluc3RhbGxlZE1vZHVsZXMgPSB7fTtcblxuIFx0Ly8gVGhlIHJlcXVpcmUgZnVuY3Rpb25cbiBcdGZ1bmN0aW9uIF9fd2VicGFja19yZXF1aXJlX18obW9kdWxlSWQpIHtcblxuIFx0XHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcbiBcdFx0aWYoaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0pIHtcbiBcdFx0XHRyZXR1cm4gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0uZXhwb3J0cztcbiBcdFx0fVxuIFx0XHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuIFx0XHR2YXIgbW9kdWxlID0gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0gPSB7XG4gXHRcdFx0aTogbW9kdWxlSWQsXG4gXHRcdFx0bDogZmFsc2UsXG4gXHRcdFx0ZXhwb3J0czoge31cbiBcdFx0fTtcblxuIFx0XHQvLyBFeGVjdXRlIHRoZSBtb2R1bGUgZnVuY3Rpb25cbiBcdFx0bW9kdWxlc1ttb2R1bGVJZF0uY2FsbChtb2R1bGUuZXhwb3J0cywgbW9kdWxlLCBtb2R1bGUuZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXyk7XG5cbiBcdFx0Ly8gRmxhZyB0aGUgbW9kdWxlIGFzIGxvYWRlZFxuIFx0XHRtb2R1bGUubCA9IHRydWU7XG5cbiBcdFx0Ly8gUmV0dXJuIHRoZSBleHBvcnRzIG9mIHRoZSBtb2R1bGVcbiBcdFx0cmV0dXJuIG1vZHVsZS5leHBvcnRzO1xuIFx0fVxuXG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlcyBvYmplY3QgKF9fd2VicGFja19tb2R1bGVzX18pXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm0gPSBtb2R1bGVzO1xuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZSBjYWNoZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5jID0gaW5zdGFsbGVkTW9kdWxlcztcblxuIFx0Ly8gZGVmaW5lIGdldHRlciBmdW5jdGlvbiBmb3IgaGFybW9ueSBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQgPSBmdW5jdGlvbihleHBvcnRzLCBuYW1lLCBnZXR0ZXIpIHtcbiBcdFx0aWYoIV9fd2VicGFja19yZXF1aXJlX18ubyhleHBvcnRzLCBuYW1lKSkge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBuYW1lLCB7IGVudW1lcmFibGU6IHRydWUsIGdldDogZ2V0dGVyIH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBkZWZpbmUgX19lc01vZHVsZSBvbiBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIgPSBmdW5jdGlvbihleHBvcnRzKSB7XG4gXHRcdGlmKHR5cGVvZiBTeW1ib2wgIT09ICd1bmRlZmluZWQnICYmIFN5bWJvbC50b1N0cmluZ1RhZykge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBTeW1ib2wudG9TdHJpbmdUYWcsIHsgdmFsdWU6ICdNb2R1bGUnIH0pO1xuIFx0XHR9XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnX19lc01vZHVsZScsIHsgdmFsdWU6IHRydWUgfSk7XG4gXHR9O1xuXG4gXHQvLyBjcmVhdGUgYSBmYWtlIG5hbWVzcGFjZSBvYmplY3RcbiBcdC8vIG1vZGUgJiAxOiB2YWx1ZSBpcyBhIG1vZHVsZSBpZCwgcmVxdWlyZSBpdFxuIFx0Ly8gbW9kZSAmIDI6IG1lcmdlIGFsbCBwcm9wZXJ0aWVzIG9mIHZhbHVlIGludG8gdGhlIG5zXG4gXHQvLyBtb2RlICYgNDogcmV0dXJuIHZhbHVlIHdoZW4gYWxyZWFkeSBucyBvYmplY3RcbiBcdC8vIG1vZGUgJiA4fDE6IGJlaGF2ZSBsaWtlIHJlcXVpcmVcbiBcdF9fd2VicGFja19yZXF1aXJlX18udCA9IGZ1bmN0aW9uKHZhbHVlLCBtb2RlKSB7XG4gXHRcdGlmKG1vZGUgJiAxKSB2YWx1ZSA9IF9fd2VicGFja19yZXF1aXJlX18odmFsdWUpO1xuIFx0XHRpZihtb2RlICYgOCkgcmV0dXJuIHZhbHVlO1xuIFx0XHRpZigobW9kZSAmIDQpICYmIHR5cGVvZiB2YWx1ZSA9PT0gJ29iamVjdCcgJiYgdmFsdWUgJiYgdmFsdWUuX19lc01vZHVsZSkgcmV0dXJuIHZhbHVlO1xuIFx0XHR2YXIgbnMgPSBPYmplY3QuY3JlYXRlKG51bGwpO1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIobnMpO1xuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkobnMsICdkZWZhdWx0JywgeyBlbnVtZXJhYmxlOiB0cnVlLCB2YWx1ZTogdmFsdWUgfSk7XG4gXHRcdGlmKG1vZGUgJiAyICYmIHR5cGVvZiB2YWx1ZSAhPSAnc3RyaW5nJykgZm9yKHZhciBrZXkgaW4gdmFsdWUpIF9fd2VicGFja19yZXF1aXJlX18uZChucywga2V5LCBmdW5jdGlvbihrZXkpIHsgcmV0dXJuIHZhbHVlW2tleV07IH0uYmluZChudWxsLCBrZXkpKTtcbiBcdFx0cmV0dXJuIG5zO1xuIFx0fTtcblxuIFx0Ly8gZ2V0RGVmYXVsdEV4cG9ydCBmdW5jdGlvbiBmb3IgY29tcGF0aWJpbGl0eSB3aXRoIG5vbi1oYXJtb255IG1vZHVsZXNcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubiA9IGZ1bmN0aW9uKG1vZHVsZSkge1xuIFx0XHR2YXIgZ2V0dGVyID0gbW9kdWxlICYmIG1vZHVsZS5fX2VzTW9kdWxlID9cbiBcdFx0XHRmdW5jdGlvbiBnZXREZWZhdWx0KCkgeyByZXR1cm4gbW9kdWxlWydkZWZhdWx0J107IH0gOlxuIFx0XHRcdGZ1bmN0aW9uIGdldE1vZHVsZUV4cG9ydHMoKSB7IHJldHVybiBtb2R1bGU7IH07XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18uZChnZXR0ZXIsICdhJywgZ2V0dGVyKTtcbiBcdFx0cmV0dXJuIGdldHRlcjtcbiBcdH07XG5cbiBcdC8vIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5vID0gZnVuY3Rpb24ob2JqZWN0LCBwcm9wZXJ0eSkgeyByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iamVjdCwgcHJvcGVydHkpOyB9O1xuXG4gXHQvLyBfX3dlYnBhY2tfcHVibGljX3BhdGhfX1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5wID0gXCJcIjtcblxuXG4gXHQvLyBMb2FkIGVudHJ5IG1vZHVsZSBhbmQgcmV0dXJuIGV4cG9ydHNcbiBcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKF9fd2VicGFja19yZXF1aXJlX18ucyA9IFwiLi9zcmMvTnVtYmVyVXRpbHMuanNcIik7XG4iLCIvKipcbiAqIEEgY29sbGVjdGlvbiBvZiBOdW1iZXIgcmVsYXRlZCB1dGlsaXRpZXNcbiAqXG4gKiBAbW9kdWxlIE51bWJlclV0aWxzXG4gKi9cblxuLyoqXG4gICogTGluZWFybHkgaW50ZXJwb2xhdGVzIGEgTnVtYmVyXG4gICpcbiAgKiBAcGFyYW0ge051bWJlcn0gdjAgSW5pdGlhbCB2YWx1ZVxuICAqIEBwYXJhbSB7TnVtYmVyfSB2MSBGaW5hbCB2YWx1ZVxuICAqIEBwYXJhbSB7TnVtYmVyfSB0IHplcm8gdG8gb25lXG4gICogQHJldHVybnMge051bWJlcn0gSW50ZXJwb2xhdGlvbiBiZXR3ZWVuIHYwIGFuZCB2MSBiYXNlZCBvbiB0XG4gICovXG5jb25zdCBsZXJwTnVtYmVyID0gZnVuY3Rpb24gKHYwLCB2MSwgdCkge1xuICByZXR1cm4gdjAgKyB0ICogKHYxIC0gdjApXG59XG5cbi8qKlxuICogVE9ETzogRG9jdW1lbnRhdGlvblxuICpcbiAqIEBwYXJhbSB7Kn0gb2xkVmFsdWVcbiAqIEBwYXJhbSB7Kn0gb2xkTWluXG4gKiBAcGFyYW0geyp9IG9sZE1heFxuICogQHBhcmFtIHsqfSBuZXdNaW5cbiAqIEBwYXJhbSB7Kn0gbmV3TWF4XG4gKiBAcGFyYW0geyp9IGNsYW1wZWRcbiAqL1xuY29uc3QgcmFuZ2UgPSBmdW5jdGlvbiAob2xkVmFsdWUsIG9sZE1pbiwgb2xkTWF4LCBuZXdNaW4sIG5ld01heCwgY2xhbXBlZCkge1xuICBjb25zdCBvbGRSYW5nZSA9IG9sZE1heCAtIG9sZE1pblxuICBjb25zdCBuZXdSYW5nZSA9IG5ld01heCAtIG5ld01pblxuICBjb25zdCBuZXdWYWx1ZSA9ICgob2xkVmFsdWUgLSBvbGRNaW4pICogbmV3UmFuZ2UpIC8gb2xkUmFuZ2UgKyBuZXdNaW5cblxuICBpZiAoY2xhbXBlZCkge1xuICAgIHJldHVybiBjbGFtcChuZXdWYWx1ZSwgbmV3TWluLCBuZXdNYXgpXG4gIH1cblxuICByZXR1cm4gbmV3VmFsdWVcbn1cbi8qKlxuICogQ2xhbXBzIGEgdmFsdWUgYmV0d2VlbiBtaW4gYW5kIG1heCB2YWx1ZXNcbiAqXG4gKiBAcGFyYW0ge051bWJlcn0gdmFsdWUgLSBUaGUgdmFsdWUgdG8gYmUgY2xhbXBlZC5cbiAqIEBwYXJhbSB7TnVtYmVyfSBtaW4gLSBNaW5pbXVtIHZhbHVlLlxuICogQHBhcmFtIHtOdW1iZXJ9IG1heCAtIE1heGltdW0gdmFsdWUuXG4gKiBAcmV0dXJucyB7TnVtYmVyfSBBIG51bWJlciBjbGFtcGVkIGJldHdlZW4gbWluIGFuZCBtYXhcbiAqL1xuY29uc3QgY2xhbXAgPSAodmFsdWUsIG1pbiA9IDAsIG1heCA9IDEpID0+IE1hdGgubWF4KG1pbiwgTWF0aC5taW4odmFsdWUsIG1heCkpXG4vKipcbiAqIFRPRE86IERvY3VtZW50YXRpb25cbiAqXG4gKiBAcGFyYW0ge051bWJlcn0gdmFsdWVcbiAqIEBwYXJhbSB7TnVtYmVyfSBtaW5cbiAqIEBwYXJhbSB7TnVtYmVyfSBtYXhcbiAqL1xuY29uc3QgbWFwID0gKHZhbHVlLCBtaW4sIG1heCkgPT4gY2xhbXAoKHZhbHVlIC0gbWluKSAvIChtYXggLSBtaW4pLCAwLCAxKVxuLyoqXG4gKiBUT0RPOiBEb2N1bWVudGF0aW9uXG4gKlxuICogQHBhcmFtIHtOdW1iZXJ9IGFscGhhXG4gKiBAcGFyYW0ge051bWJlcn0gYmV0YVxuICovXG5jb25zdCBhbmdsZURpc3RhbmNlID0gZnVuY3Rpb24gKGFscGhhLCBiZXRhKSB7XG4gIGNvbnN0IHBoaSA9IChNYXRoLmFicyhiZXRhIC0gYWxwaGEpICUgTWF0aC5QSSkgKiAyXG4gIHJldHVybiBwaGkgPiBNYXRoLlBJID8gTWF0aC5QSSAqIDIgLSBwaGkgOiBwaGlcbn1cbi8qKlxuICogVE9ETzogRG9jdW1lbnRhdGlvblxuICpcbiAqIEBwYXJhbSB7TnVtYmVyfSBhbHBoYVxuICogQHBhcmFtIHtOdW1iZXJ9IGJldGFcbiAqL1xuY29uc3QgYW5nbGVEaXN0YW5jZVNpZ24gPSBmdW5jdGlvbiAoYWxwaGEsIGJldGEpIHtcbiAgY29uc3Qgc2lnbiA9XG4gICAgKGFscGhhIC0gYmV0YSA+PSAwICYmIGFscGhhIC0gYmV0YSA8PSBNYXRoLlBJKSB8fFxuICAgIChhbHBoYSAtIGJldGEgPD0gLU1hdGguUEkgJiYgYWxwaGEgLSBiZXRhID49IC1NYXRoLlBJICogMilcbiAgICAgID8gMVxuICAgICAgOiAtMVxuICByZXR1cm4gYW5nbGVEaXN0YW5jZShhbHBoYSwgYmV0YSkgKiBzaWduXG59XG4vKipcbiAqIFNtb290aHN0ZXAgaW1wbGVtZW50YXRpb24uXG4gKiBodHRwczovL2VuLndpa2lwZWRpYS5vcmcvd2lraS9TbW9vdGhzdGVwXG4gKlxuICogQHBhcmFtIHtOdW1iZXJ9IG1pbiAtIEluaXRpYWwgdmFsdWVcbiAqIEBwYXJhbSB7TnVtYmVyfSBtYXggLSBGaW5hbCBWYWx1ZVxuICogQHBhcmFtIHtOdW1iZXJ9IHZhbHVlIC1cbiAqIEByZXR1cm5zIHtOdW1iZXJ9IFZhbHVlIG9mIHRoZSBpbnRlcnBvbGF0aW9uXG4gKi9cbmNvbnN0IHNtb290aHN0ZXAgPSAobWluLCBtYXgsIHZhbHVlKSA9PiB7XG4gIGNvbnN0IHggPSBNYXRoLm1heCgwLCBNYXRoLm1pbigxLCAodmFsdWUgLSBtaW4pIC8gKG1heCAtIG1pbikpKVxuICByZXR1cm4geCAqIHggKiAoMyAtIDIgKiB4KVxufVxuLyoqXG4gKiBTaW1pbGFyIHRvIGxpbmVhciBpbnRlcnBvbGF0aW9uLlxuICpcbiAqIEBwYXJhbSB7TnVtYmVyfSB2YWx1ZTEgLSBNaW5pbXVtIHZhbHVlLlxuICogQHBhcmFtIHtOdW1iZXJ9IHZhbHVlMiAtIE1heGltdW0gdmFsdWUuXG4gKiBAcGFyYW0ge051bWJlcn0gcGVyY2VudCAtIHplcm8gdG8gb25lIHZhbHVlIHBlcmNlbnRcbiAqL1xuY29uc3QgbWl4ID0gKHZhbHVlMSwgdmFsdWUyLCBwZXJjZW50KSA9PiB2YWx1ZTEgKiAoMSAtIHBlcmNlbnQpICsgdmFsdWUyICogcGVyY2VudFxuLyoqXG4gKiBSZXR1cm5zIHRoZSBzaWduIG9mIGEgbnVtYmVyIGluIC0xLDAsMSBmb3JtLlxuICpcbiAqIEBwYXJhbSB7TnVtYmVyfSB2YWx1ZSAtIGEgbnVtYmVyIHRvIGJlIHRlc3RlZFxuICogQHJldHVybnMge051bWJlcn0gMCBpZiB2YWx1ZSBpcyB6ZXJvLCAtMSBpZiB2YWx1ZSBpcyBsZXNzIHRoYW4gMCwgMSBvdGhlcndpc2VcbiAqL1xuY29uc3Qgc2lnbiA9IHZhbHVlID0+ICh2YWx1ZSAhPT0gMCA/ICh2YWx1ZSA8IDAgPyAtMSA6IDEpIDogMClcbi8qKlxuICogVE9ETzogRG9jdW1lbnRhdGlvblxuICpcbiAqIEBwYXJhbSB7TnVtYmVyfSBtaW5cbiAqIEBwYXJhbSB7TnVtYmVyfSBtYXhcbiAqIEBwYXJhbSB7TnVtYmVyfSBwb3dlclxuICogQHBhcmFtIHtOdW1iZXJ9IFtyZCA9IE1hdGgucmFuZG9tXSAtIE9wdGlvbmFsIFJhbmRvbSBudW1iZXIgZ2VuZXJhdG9yXG4gKi9cbmNvbnN0IHJhbmRCeVBvd2VyID0gZnVuY3Rpb24gKG1pbiwgbWF4LCBwb3dlciwgcmQgPSBNYXRoLnJhbmRvbSkge1xuICBjb25zdCB2YWx1ZSA9IHJkKCkgKiAobWF4IC0gbWluKSArIG1pblxuICByZXR1cm4gbWFwVG9Qb3dlcih2YWx1ZSwgbWluLCBtYXgsIHBvd2VyKVxufVxuLyoqXG4gKiBUT0RPOiBEb2N1bWVudGF0aW9uXG4gKlxuICogQHBhcmFtIHtOdW1iZXJ9IHZhbHVlXG4gKiBAcGFyYW0ge051bWJlcn0gbWluXG4gKiBAcGFyYW0ge051bWJlcn0gbWF4XG4gKiBAcGFyYW0ge051bWJlcn0gcG93ZXJcbiAqL1xuY29uc3QgbWFwVG9Qb3dlciA9IGZ1bmN0aW9uICh2YWx1ZSwgbWluLCBtYXgsIHBvd2VyKSB7XG4gIGNvbnN0IHZhbHVlU2lnbiA9IHNpZ24odmFsdWUpXG5cbiAgdmFsdWUgPSAodmFsdWUgLSBtaW4pIC8gKG1heCAtIG1pbilcbiAgdmFsdWUgPSB2YWx1ZVNpZ24gPiAwID8gdmFsdWUgOiB2YWx1ZSAtIDFcbiAgdmFsdWUgPSBNYXRoLnBvdyh2YWx1ZSAqIHZhbHVlU2lnbiwgTWF0aC5hYnMocG93ZXIpKVxuICB2YWx1ZSA9IHZhbHVlU2lnbiA+IDAgPyB2YWx1ZSA6IDEgLSB2YWx1ZVxuXG4gIHJldHVybiB2YWx1ZSAqIChtYXggLSBtaW4pICsgbWluXG59XG4vKipcbiAqIENvbXB1dGVzIGRpc3RhbmNlIGJldHdlZW4gdHdvIHBvaW50cyBpbiAyRCBzcGFjZS5cbiAqXG4gKiBAcGFyYW0ge051bWJlcn0geDEgLSB4IGNvb3JkaW5hdGUgb2YgZmlyc3QgcG9pbnRcbiAqIEBwYXJhbSB7TnVtYmVyfSB5MSAtIHkgY29vcmRpbmF0ZSBvZiBmaXJzdCBwb2ludFxuICogQHBhcmFtIHtOdW1iZXJ9IHgyIC0geCBjb29yZGluYXRlIG9mIHNlY29uZCBwb2ludFxuICogQHBhcmFtIHtOdW1iZXJ9IHkyIC0geSBjb29yZGluYXRlIG9mIHNlY29uZCBwb2ludFxuICogQHJldHVybnMge051bWJlcn0gRGlzdGFuY2UgYmV0d2VlbiB4MSx5MSBhbmQgeDIseTJcbiAqL1xuY29uc3QgZGlzdGFuY2UgPSBmdW5jdGlvbiAoeDEsIHkxLCB4MiwgeTIpIHtcbiAgY29uc3QgZHggPSB4MSAtIHgyXG4gIGNvbnN0IGR5ID0geTEgLSB5MlxuICByZXR1cm4gTWF0aC5zcXJ0KGR4ICogZHggKyBkeSAqIGR5KVxufVxuXG4vKipcbiAqIE5vdCBhIHJlYWwgZGlzdGFuY2UgY2FsY3VsYXRpb24uXG4gKiBVc2VmdWwgdG8gc29ydCBvYmplY3RzIGJ5IGRpc3RhbmNlICggbXVjaCBmYXN0ZXIgYmVjYXVzZSBubyBzcXJ0IClcbiAqXG4gKiBAcGFyYW0ge051bWJlcn0geDEgLSB4IGNvb3JkaW5hdGUgb2YgZmlyc3QgcG9pbnRcbiAqIEBwYXJhbSB7TnVtYmVyfSB5MSAtIHkgY29vcmRpbmF0ZSBvZiBmaXJzdCBwb2ludFxuICogQHBhcmFtIHtOdW1iZXJ9IHgyIC0geCBjb29yZGluYXRlIG9mIHNlY29uZCBwb2ludFxuICogQHBhcmFtIHtOdW1iZXJ9IHkyIC0geSBjb29yZGluYXRlIG9mIHNlY29uZCBwb2ludFxuICogQHJldHVybnMge051bWJlcn0gYm9ndXMgZGlzdGFuY2UgYmV0d2VlbiB4MSx5MSBhbmQgeDIseTIuIERPIE5PVCBVU0UgV0hFTiBBIFJFQUwgRElTVEFOQ0UgSVMgTkVFREVEXG4gKi9cbmNvbnN0IGRpc3RhbmNlQ29tcGFyZSA9IGZ1bmN0aW9uICh4MSwgeTEsIHgyLCB5Mikge1xuICBjb25zdCBkeCA9IHgxIC0geDJcbiAgY29uc3QgZHkgPSB5MSAtIHkyXG4gIHJldHVybiBkeCAqIGR4ICsgZHkgKiBkeVxufVxuXG5leHBvcnQge1xuICBsZXJwTnVtYmVyLFxuICBkaXN0YW5jZUNvbXBhcmUsXG4gIGRpc3RhbmNlLFxuICBtYXBUb1Bvd2VyLFxuICByYW5kQnlQb3dlcixcbiAgc21vb3Roc3RlcCxcbiAgYW5nbGVEaXN0YW5jZVNpZ24sXG4gIGFuZ2xlRGlzdGFuY2UsXG4gIG1hcCxcbiAgY2xhbXAsXG4gIHJhbmdlLFxuICBtaXhcbn1cbiJdLCJzb3VyY2VSb290IjoiIn0=