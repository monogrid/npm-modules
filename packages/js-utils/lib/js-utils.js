(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define("js-utils", [], factory);
	else if(typeof exports === 'object')
		exports["js-utils"] = factory();
	else
		root["js-utils"] = factory();
})(typeof self !== 'undefined' ? self : this, function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/js-utils.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "../../node_modules/@babel/runtime/helpers/classCallCheck.js":
/*!*********************************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/@babel/runtime/helpers/classCallCheck.js ***!
  \*********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

module.exports = _classCallCheck;

/***/ }),

/***/ "../../node_modules/@babel/runtime/helpers/createClass.js":
/*!******************************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/@babel/runtime/helpers/createClass.js ***!
  \******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

module.exports = _createClass;

/***/ }),

/***/ "../../node_modules/@babel/runtime/helpers/typeof.js":
/*!*************************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/@babel/runtime/helpers/typeof.js ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _typeof(obj) {
  "@babel/helpers - typeof";

  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    module.exports = _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    module.exports = _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}

module.exports = _typeof;

/***/ }),

/***/ "../../node_modules/@hughsk/fulltilt/dist/fulltilt.js":
/*!**************************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/@hughsk/fulltilt/dist/fulltilt.js ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/**
 *
 * FULL TILT
 * http://github.com/richtr/Full-Tilt
 *
 * A standalone DeviceOrientation + DeviceMotion JavaScript library that
 * normalises orientation sensor input, applies relevant screen orientation
 * transforms, returns Euler Angle, Quaternion and Rotation
 * Matrix representations back to web developers and provides conversion
 * between all supported orientation representation types.
 *
 * Copyright: 2014 Rich Tibbett
 * License:   MIT
 *
 */

(function ( window ) {

// Only initialize the FULLTILT API if it is not already attached to the DOM
if ( window.FULLTILT !== undefined && window.FULLTILT !== null ) {
	return;
}

var M_PI   = Math.PI;
var M_PI_2 = M_PI / 2;
var M_2_PI = 2 * M_PI;

// Degree to Radian conversion
var degToRad = M_PI / 180;
var radToDeg = 180 / M_PI;

// Internal device orientation + motion variables
var sensors = {
	"orientation": {
		active:    false,
		callbacks: [],
		data:      undefined
	},
	"motion": {
		active:    false,
		callbacks: [],
		data:      undefined
	}
};
var screenActive = false;

// Internal screen orientation variables
var hasScreenOrientationAPI = window.screen && window.screen.orientation && window.screen.orientation.angle !== undefined && window.screen.orientation.angle !== null ? true : false;
var screenOrientationAngle = ( hasScreenOrientationAPI ? window.screen.orientation.angle : ( window.orientation || 0 ) ) * degToRad;

var SCREEN_ROTATION_0        = 0,
    SCREEN_ROTATION_90       = M_PI_2,
    SCREEN_ROTATION_180      = M_PI,
    SCREEN_ROTATION_270      = M_2_PI / 3,
    SCREEN_ROTATION_MINUS_90 = - M_PI_2;

// Math.sign polyfill
function sign(x) {
	x = +x; // convert to a number
	if (x === 0 || isNaN(x))
		return x;
	return x > 0 ? 1 : -1;
}

///// Promise-based Sensor Data checker //////

function SensorCheck(sensorRootObj) {

	var promise = new Promise(function(resolve, reject) {

		var runCheck = function (tries) {

			setTimeout(function() {

				if (sensorRootObj && sensorRootObj.data) {

					resolve();

				} else if (tries >= 20) {

					reject();

				} else {

					runCheck(++tries);

				}

			}, 50);

		};

		runCheck(0);

	});

	return promise;

}

////// Internal Event Handlers //////

function handleScreenOrientationChange () {

	if ( hasScreenOrientationAPI ) {

		screenOrientationAngle = ( window.screen.orientation.angle || 0 ) * degToRad;

	} else {

		screenOrientationAngle = ( window.orientation || 0 ) * degToRad;

	}

}

function handleDeviceOrientationChange ( event ) {

	sensors.orientation.data = event;

	// Fire every callback function each time deviceorientation is updated
	for ( var i in sensors.orientation.callbacks ) {

		sensors.orientation.callbacks[ i ].call( this );

	}

}

function handleDeviceMotionChange ( event ) {

	sensors.motion.data = event;

	// Fire every callback function each time devicemotion is updated
	for ( var i in sensors.motion.callbacks ) {

		sensors.motion.callbacks[ i ].call( this );

	}

}

///// FULLTILT API Root Object /////

var FULLTILT = {};

FULLTILT.version = "0.5.3";

///// FULLTILT API Root Methods /////

FULLTILT.getDeviceOrientation = function(options) {

	var promise = new Promise(function(resolve, reject) {

		var control = new FULLTILT.DeviceOrientation(options);

		control.start();

		var orientationSensorCheck = new SensorCheck(sensors.orientation);

		orientationSensorCheck.then(function() {

			resolve(control);

		}).catch(function() {

			control.stop();
			reject('DeviceOrientation is not supported');

		});

	});

	return promise;

};

FULLTILT.getDeviceMotion = function(options) {

	var promise = new Promise(function(resolve, reject) {

		var control = new FULLTILT.DeviceMotion(options);

		control.start();

		var motionSensorCheck = new SensorCheck(sensors.motion);

		motionSensorCheck.then(function() {

			resolve(control);

		}).catch(function() {

			control.stop();
			reject('DeviceMotion is not supported');

		});

	});

	return promise;

};


////// FULLTILT.Quaternion //////

FULLTILT.Quaternion = function ( x, y, z, w ) {

	var quat, outQuat;

	this.set = function ( x, y, z, w ) {

		this.x = x || 0;
		this.y = y || 0;
		this.z = z || 0;
		this.w = w || 1;

	};

	this.copy = function ( quaternion ) {

		this.x = quaternion.x;
		this.y = quaternion.y;
		this.z = quaternion.z;
		this.w = quaternion.w;

	};

	this.setFromEuler = (function () {

		var _x, _y, _z;
		var _x_2, _y_2, _z_2;
		var cX, cY, cZ, sX, sY, sZ;

		return function ( euler ) {

			euler = euler || {};

			_z = ( euler.alpha || 0 ) * degToRad;
			_x = ( euler.beta || 0 ) * degToRad;
			_y = ( euler.gamma || 0 ) * degToRad;

			_z_2 = _z / 2;
			_x_2 = _x / 2;
			_y_2 = _y / 2;

			cX = Math.cos( _x_2 );
			cY = Math.cos( _y_2 );
			cZ = Math.cos( _z_2 );
			sX = Math.sin( _x_2 );
			sY = Math.sin( _y_2 );
			sZ = Math.sin( _z_2 );

			this.set(
				sX * cY * cZ - cX * sY * sZ, // x
				cX * sY * cZ + sX * cY * sZ, // y
				cX * cY * sZ + sX * sY * cZ, // z
				cX * cY * cZ - sX * sY * sZ  // w
			);

			this.normalize();

			return this;

		};

	})();

	this.setFromRotationMatrix = (function () {

		var R;

		return function( matrix ) {

			R = matrix.elements;

			this.set(
				0.5 * Math.sqrt( 1 + R[0] - R[4] - R[8] ) * sign( R[7] - R[5] ), // x
				0.5 * Math.sqrt( 1 - R[0] + R[4] - R[8] ) * sign( R[2] - R[6] ), // y
				0.5 * Math.sqrt( 1 - R[0] - R[4] + R[8] ) * sign( R[3] - R[1] ), // z
				0.5 * Math.sqrt( 1 + R[0] + R[4] + R[8] )                        // w
			);

			return this;

		};

	})();

	this.multiply = function ( quaternion ) {

		outQuat = FULLTILT.Quaternion.prototype.multiplyQuaternions( this, quaternion );
		this.copy( outQuat );

		return this;

	};

	this.rotateX = function ( angle ) {

		outQuat = FULLTILT.Quaternion.prototype.rotateByAxisAngle( this, [ 1, 0, 0 ], angle );
		this.copy( outQuat );

		return this;

	};

	this.rotateY = function ( angle ) {

		outQuat = FULLTILT.Quaternion.prototype.rotateByAxisAngle( this, [ 0, 1, 0 ], angle );
		this.copy( outQuat );

		return this;

	};

	this.rotateZ = function ( angle ) {

		outQuat = FULLTILT.Quaternion.prototype.rotateByAxisAngle( this, [ 0, 0, 1 ], angle );
		this.copy( outQuat );

		return this;

	};

	this.normalize = function () {

		return FULLTILT.Quaternion.prototype.normalize( this );

	};

	// Initialize object values
	this.set( x, y, z, w );

};

FULLTILT.Quaternion.prototype = {

	constructor: FULLTILT.Quaternion,

	multiplyQuaternions: function () {

		var multipliedQuat = new FULLTILT.Quaternion();

		return function ( a, b ) {

			var qax = a.x, qay = a.y, qaz = a.z, qaw = a.w;
			var qbx = b.x, qby = b.y, qbz = b.z, qbw = b.w;

			multipliedQuat.set(
				qax * qbw + qaw * qbx + qay * qbz - qaz * qby, // x
				qay * qbw + qaw * qby + qaz * qbx - qax * qbz, // y
				qaz * qbw + qaw * qbz + qax * qby - qay * qbx, // z
				qaw * qbw - qax * qbx - qay * qby - qaz * qbz  // w
			);

			return multipliedQuat;

		};

	}(),

	normalize: function( q ) {

		var len = Math.sqrt( q.x * q.x + q.y * q.y + q.z * q.z + q.w * q.w );

		if ( len === 0 ) {

			q.x = 0;
			q.y = 0;
			q.z = 0;
			q.w = 1;

		} else {

			len = 1 / len;

			q.x *= len;
			q.y *= len;
			q.z *= len;
			q.w *= len;

		}

		return q;

	},

	rotateByAxisAngle: function () {

		var outputQuaternion = new FULLTILT.Quaternion();
		var transformQuaternion = new FULLTILT.Quaternion();

		var halfAngle, sA;

		return function ( targetQuaternion, axis, angle ) {

			halfAngle = ( angle || 0 ) / 2;
			sA = Math.sin( halfAngle );

			transformQuaternion.set(
				( axis[ 0 ] || 0 ) * sA, // x
				( axis[ 1 ] || 0 ) * sA, // y
				( axis[ 2 ] || 0 ) * sA, // z
				Math.cos( halfAngle )    // w
			);

			// Multiply quaternion by q
			outputQuaternion = FULLTILT.Quaternion.prototype.multiplyQuaternions( targetQuaternion, transformQuaternion );

			return FULLTILT.Quaternion.prototype.normalize( outputQuaternion );

		};

	}()

};

////// FULLTILT.RotationMatrix //////

FULLTILT.RotationMatrix = function ( m11, m12, m13, m21, m22, m23, m31, m32, m33 ) {

	var outMatrix;

	this.elements = new Float32Array( 9 );

	this.identity = function () {

		this.set(
			1, 0, 0,
			0, 1, 0,
			0, 0, 1
		);

		return this;

	};

	this.set = function ( m11, m12, m13, m21, m22, m23, m31, m32, m33 ) {

		this.elements[ 0 ] = m11 || 1;
		this.elements[ 1 ] = m12 || 0;
		this.elements[ 2 ] = m13 || 0;
		this.elements[ 3 ] = m21 || 0;
		this.elements[ 4 ] = m22 || 1;
		this.elements[ 5 ] = m23 || 0;
		this.elements[ 6 ] = m31 || 0;
		this.elements[ 7 ] = m32 || 0;
		this.elements[ 8 ] = m33 || 1;

	};

	this.copy = function ( matrix ) {

		this.elements[ 0 ] = matrix.elements[ 0 ];
		this.elements[ 1 ] = matrix.elements[ 1 ];
		this.elements[ 2 ] = matrix.elements[ 2 ];
		this.elements[ 3 ] = matrix.elements[ 3 ];
		this.elements[ 4 ] = matrix.elements[ 4 ];
		this.elements[ 5 ] = matrix.elements[ 5 ];
		this.elements[ 6 ] = matrix.elements[ 6 ];
		this.elements[ 7 ] = matrix.elements[ 7 ];
		this.elements[ 8 ] = matrix.elements[ 8 ];

	};

	this.setFromEuler = (function() {

		var _x, _y, _z;
		var cX, cY, cZ, sX, sY, sZ;

		return function ( euler ) {

			euler = euler || {};

			_z = ( euler.alpha || 0 ) * degToRad;
			_x = ( euler.beta || 0 ) * degToRad;
			_y = ( euler.gamma || 0 ) * degToRad;

			cX = Math.cos( _x );
			cY = Math.cos( _y );
			cZ = Math.cos( _z );
			sX = Math.sin( _x );
			sY = Math.sin( _y );
			sZ = Math.sin( _z );

			//
			// ZXY-ordered rotation matrix construction.
			//

			this.set(
				cZ * cY - sZ * sX * sY, // 1,1
				- cX * sZ,              // 1,2
				cY * sZ * sX + cZ * sY, // 1,3

				cY * sZ + cZ * sX * sY, // 2,1
				cZ * cX,                // 2,2
				sZ * sY - cZ * cY * sX, // 2,3

				- cX * sY,              // 3,1
				sX,                     // 3,2
				cX * cY                 // 3,3
			);

			this.normalize();

			return this;

		};

	})();

	this.setFromQuaternion = (function() {

		var sqw, sqx, sqy, sqz;

		return function( q ) {

			sqw = q.w * q.w;
			sqx = q.x * q.x;
			sqy = q.y * q.y;
			sqz = q.z * q.z;

			this.set(
				sqw + sqx - sqy - sqz,       // 1,1
				2 * (q.x * q.y - q.w * q.z), // 1,2
				2 * (q.x * q.z + q.w * q.y), // 1,3

				2 * (q.x * q.y + q.w * q.z), // 2,1
				sqw - sqx + sqy - sqz,       // 2,2
				2 * (q.y * q.z - q.w * q.x), // 2,3

				2 * (q.x * q.z - q.w * q.y), // 3,1
				2 * (q.y * q.z + q.w * q.x), // 3,2
				sqw - sqx - sqy + sqz        // 3,3
			);

			return this;

		};

	})();

	this.multiply = function ( m ) {

		outMatrix = FULLTILT.RotationMatrix.prototype.multiplyMatrices( this, m );
		this.copy( outMatrix );

		return this;

	};

	this.rotateX = function ( angle ) {

		outMatrix = FULLTILT.RotationMatrix.prototype.rotateByAxisAngle( this, [ 1, 0, 0 ], angle );
		this.copy( outMatrix );

		return this;

	};

	this.rotateY = function ( angle ) {

		outMatrix = FULLTILT.RotationMatrix.prototype.rotateByAxisAngle( this, [ 0, 1, 0 ], angle );
		this.copy( outMatrix );

		return this;

	};

	this.rotateZ = function ( angle ) {

		outMatrix = FULLTILT.RotationMatrix.prototype.rotateByAxisAngle( this, [ 0, 0, 1 ], angle );
		this.copy( outMatrix );

		return this;

	};

	this.normalize = function () {

		return FULLTILT.RotationMatrix.prototype.normalize( this );

	};

	// Initialize object values
	this.set( m11, m12, m13, m21, m22, m23, m31, m32, m33 );

};

FULLTILT.RotationMatrix.prototype = {

	constructor: FULLTILT.RotationMatrix,

	multiplyMatrices: function () {

		var matrix = new FULLTILT.RotationMatrix();

		var aE, bE;

		return function ( a, b ) {

			aE = a.elements;
			bE = b.elements;

			matrix.set(
				aE[0] * bE[0] + aE[1] * bE[3] + aE[2] * bE[6],
				aE[0] * bE[1] + aE[1] * bE[4] + aE[2] * bE[7],
				aE[0] * bE[2] + aE[1] * bE[5] + aE[2] * bE[8],

				aE[3] * bE[0] + aE[4] * bE[3] + aE[5] * bE[6],
				aE[3] * bE[1] + aE[4] * bE[4] + aE[5] * bE[7],
				aE[3] * bE[2] + aE[4] * bE[5] + aE[5] * bE[8],

				aE[6] * bE[0] + aE[7] * bE[3] + aE[8] * bE[6],
				aE[6] * bE[1] + aE[7] * bE[4] + aE[8] * bE[7],
				aE[6] * bE[2] + aE[7] * bE[5] + aE[8] * bE[8]
			);

			return matrix;

		};

	}(),

	normalize: function( matrix ) {

		var R = matrix.elements;

		// Calculate matrix determinant
		var determinant = R[0] * R[4] * R[8] - R[0] * R[5] * R[7] - R[1] * R[3] * R[8] + R[1] * R[5] * R[6] + R[2] * R[3] * R[7] - R[2] * R[4] * R[6];

		// Normalize matrix values
		R[0] /= determinant;
		R[1] /= determinant;
		R[2] /= determinant;
		R[3] /= determinant;
		R[4] /= determinant;
		R[5] /= determinant;
		R[6] /= determinant;
		R[7] /= determinant;
		R[8] /= determinant;

		matrix.elements = R;

		return matrix;

	},

	rotateByAxisAngle: function () {

		var outputMatrix = new FULLTILT.RotationMatrix();
		var transformMatrix = new FULLTILT.RotationMatrix();

		var sA, cA;
		var validAxis = false;

		return function ( targetRotationMatrix, axis, angle ) {

			transformMatrix.identity(); // reset transform matrix

			validAxis = false;

			sA = Math.sin( angle );
			cA = Math.cos( angle );

			if ( axis[ 0 ] === 1 && axis[ 1 ] === 0 && axis[ 2 ] === 0 ) { // x

				validAxis = true;

				transformMatrix.elements[4] = cA;
				transformMatrix.elements[5] = -sA;
				transformMatrix.elements[7] = sA;
				transformMatrix.elements[8] = cA;

	 		} else if ( axis[ 1 ] === 1 && axis[ 0 ] === 0 && axis[ 2 ] === 0 ) { // y

				validAxis = true;

				transformMatrix.elements[0] = cA;
				transformMatrix.elements[2] = sA;
				transformMatrix.elements[6] = -sA;
				transformMatrix.elements[8] = cA;

	 		} else if ( axis[ 2 ] === 1 && axis[ 0 ] === 0 && axis[ 1 ] === 0 ) { // z

				validAxis = true;

				transformMatrix.elements[0] = cA;
				transformMatrix.elements[1] = -sA;
				transformMatrix.elements[3] = sA;
				transformMatrix.elements[4] = cA;

	 		}

			if ( validAxis ) {

				outputMatrix = FULLTILT.RotationMatrix.prototype.multiplyMatrices( targetRotationMatrix, transformMatrix );

				outputMatrix = FULLTILT.RotationMatrix.prototype.normalize( outputMatrix );

			} else {

				outputMatrix = targetRotationMatrix;

			}

			return outputMatrix;

		};

	}()

};

////// FULLTILT.Euler //////

FULLTILT.Euler = function ( alpha, beta, gamma ) {

	this.set = function ( alpha, beta, gamma ) {

		this.alpha = alpha || 0;
		this.beta  = beta  || 0;
		this.gamma = gamma || 0;

	};

	this.copy = function ( inEuler ) {

		this.alpha = inEuler.alpha;
		this.beta  = inEuler.beta;
		this.gamma = inEuler.gamma;

	};

	this.setFromRotationMatrix = (function () {

		var R, _alpha, _beta, _gamma;

		return function ( matrix ) {

			R = matrix.elements;

			if (R[8] > 0) { // cos(beta) > 0

				_alpha = Math.atan2(-R[1], R[4]);
				_beta  = Math.asin(R[7]); // beta (-pi/2, pi/2)
				_gamma = Math.atan2(-R[6], R[8]); // gamma (-pi/2, pi/2)

			} else if (R[8] < 0) {  // cos(beta) < 0

				_alpha = Math.atan2(R[1], -R[4]);
				_beta  = -Math.asin(R[7]);
				_beta  += (_beta >= 0) ? - M_PI : M_PI; // beta [-pi,-pi/2) U (pi/2,pi)
				_gamma = Math.atan2(R[6], -R[8]); // gamma (-pi/2, pi/2)

			} else { // R[8] == 0

				if (R[6] > 0) {  // cos(gamma) == 0, cos(beta) > 0

					_alpha = Math.atan2(-R[1], R[4]);
					_beta  = Math.asin(R[7]); // beta [-pi/2, pi/2]
					_gamma = - M_PI_2; // gamma = -pi/2

				} else if (R[6] < 0) { // cos(gamma) == 0, cos(beta) < 0

					_alpha = Math.atan2(R[1], -R[4]);
					_beta  = -Math.asin(R[7]);
					_beta  += (_beta >= 0) ? - M_PI : M_PI; // beta [-pi,-pi/2) U (pi/2,pi)
					_gamma = - M_PI_2; // gamma = -pi/2

				} else { // R[6] == 0, cos(beta) == 0

					// gimbal lock discontinuity
					_alpha = Math.atan2(R[3], R[0]);
					_beta  = (R[7] > 0) ? M_PI_2 : - M_PI_2; // beta = +-pi/2
					_gamma = 0; // gamma = 0

				}

			}

			// alpha is in [-pi, pi], make sure it is in [0, 2*pi).
			if (_alpha < 0) {
				_alpha += M_2_PI; // alpha [0, 2*pi)
			}

			// Convert to degrees
			_alpha *= radToDeg;
			_beta  *= radToDeg;
			_gamma *= radToDeg;

			// apply derived euler angles to current object
			this.set( _alpha, _beta, _gamma );

		};

	})();

	this.setFromQuaternion = (function () {

		var _alpha, _beta, _gamma;

		return function ( q ) {

			var sqw = q.w * q.w;
			var sqx = q.x * q.x;
			var sqy = q.y * q.y;
			var sqz = q.z * q.z;

			var unitLength = sqw + sqx + sqy + sqz; // Normalised == 1, otherwise correction divisor.
			var wxyz = q.w * q.x + q.y * q.z;
			var epsilon = 1e-6; // rounding factor

			if (wxyz > (0.5 - epsilon) * unitLength) {

				_alpha = 2 * Math.atan2(q.y, q.w);
				_beta = M_PI_2;
				_gamma = 0;

			} else if (wxyz < (-0.5 + epsilon) * unitLength) {

				_alpha = -2 * Math.atan2(q.y, q.w);
				_beta = -M_PI_2;
				_gamma = 0;

			} else {

				var aX = sqw - sqx + sqy - sqz;
				var aY = 2 * (q.w * q.z - q.x * q.y);

				var gX = sqw - sqx - sqy + sqz;
				var gY = 2 * (q.w * q.y - q.x * q.z);

				if (gX > 0) {

					_alpha = Math.atan2(aY, aX);
					_beta  = Math.asin(2 * wxyz / unitLength);
					_gamma = Math.atan2(gY, gX);

				} else {

					_alpha = Math.atan2(-aY, -aX);
					_beta  = -Math.asin(2 * wxyz / unitLength);
					_beta  += _beta < 0 ? M_PI : - M_PI;
					_gamma = Math.atan2(-gY, -gX);

				}

			}

			// alpha is in [-pi, pi], make sure it is in [0, 2*pi).
			if (_alpha < 0) {
				_alpha += M_2_PI; // alpha [0, 2*pi)
			}

			// Convert to degrees
			_alpha *= radToDeg;
			_beta  *= radToDeg;
			_gamma *= radToDeg;

			// apply derived euler angles to current object
			this.set( _alpha, _beta, _gamma );

		};

	})();

	this.rotateX = function ( angle ) {

		FULLTILT.Euler.prototype.rotateByAxisAngle( this, [ 1, 0, 0 ], angle );

		return this;

	};

	this.rotateY = function ( angle ) {

		FULLTILT.Euler.prototype.rotateByAxisAngle( this, [ 0, 1, 0 ], angle );

		return this;

	};

	this.rotateZ = function ( angle ) {

		FULLTILT.Euler.prototype.rotateByAxisAngle( this, [ 0, 0, 1 ], angle );

		return this;

	};

	// Initialize object values
	this.set( alpha, beta, gamma );

};

FULLTILT.Euler.prototype = {

	constructor: FULLTILT.Euler,

	rotateByAxisAngle: function () {

		var _matrix = new FULLTILT.RotationMatrix();
		var outEuler;

		return function ( targetEuler, axis, angle ) {

			_matrix.setFromEuler( targetEuler );

			_matrix = FULLTILT.RotationMatrix.prototype.rotateByAxisAngle( _matrix, axis, angle );

			targetEuler.setFromRotationMatrix( _matrix );

			return targetEuler;

		};

	}()

};

///// FULLTILT.DeviceOrientation //////

FULLTILT.DeviceOrientation = function (options) {

	this.options = options || {}; // by default use UA deviceorientation 'type' ("game" on iOS, "world" on Android)

	var tries = 0;
	var maxTries = 200;
	var successCount = 0;
	var successThreshold = 10;

	this.alphaOffsetScreen = 0;
	this.alphaOffsetDevice = undefined;

	// Create a game-based deviceorientation object (initial alpha === 0 degrees)
	if (this.options.type === "game") {

		var setGameAlphaOffset = function(evt) {

			if (evt.alpha !== null) { // do regardless of whether 'evt.absolute' is also true
				this.alphaOffsetDevice = new FULLTILT.Euler(evt.alpha, 0, 0);
				this.alphaOffsetDevice.rotateZ( -screenOrientationAngle );

				// Discard first {successThreshold} responses while a better compass lock is found by UA
				if(++successCount >= successThreshold) {
					window.removeEventListener( 'deviceorientation', setGameAlphaOffset, false );
					return;
				}
			}

			if(++tries >= maxTries) {
				window.removeEventListener( 'deviceorientation', setGameAlphaOffset, false );
			}

		}.bind(this);

		window.addEventListener( 'deviceorientation', setGameAlphaOffset, false );

	// Create a compass-based deviceorientation object (initial alpha === compass degrees)
	} else if (this.options.type === "world") {

		var setCompassAlphaOffset = function(evt) {

			if (evt.absolute !== true && evt.webkitCompassAccuracy !== undefined && evt.webkitCompassAccuracy !== null && +evt.webkitCompassAccuracy >= 0 && +evt.webkitCompassAccuracy < 50) {
				this.alphaOffsetDevice = new FULLTILT.Euler(evt.webkitCompassHeading, 0, 0);
				this.alphaOffsetDevice.rotateZ( screenOrientationAngle );
				this.alphaOffsetScreen = screenOrientationAngle;

				// Discard first {successThreshold} responses while a better compass lock is found by UA
				if(++successCount >= successThreshold) {
					window.removeEventListener( 'deviceorientation', setCompassAlphaOffset, false );
					return;
				}
			}

			if(++tries >= maxTries) {
				window.removeEventListener( 'deviceorientation', setCompassAlphaOffset, false );
			}

		}.bind(this);

		window.addEventListener( 'deviceorientation', setCompassAlphaOffset, false );

	} // else... use whatever orientation system the UA provides ("game" on iOS, "world" on Android)

};

FULLTILT.DeviceOrientation.prototype = {

	constructor: FULLTILT.DeviceOrientation,

	start: function ( callback ) {

		if ( callback && Object.prototype.toString.call( callback ) == '[object Function]' ) {

			sensors.orientation.callbacks.push( callback );

		}

		if( !screenActive ) {

			if ( hasScreenOrientationAPI ) {

			window.screen.orientation.addEventListener( 'change', handleScreenOrientationChange, false );

			} else {

				window.addEventListener( 'orientationchange', handleScreenOrientationChange, false );

			}

		}

		if ( !sensors.orientation.active ) {

			window.addEventListener( 'deviceorientation', handleDeviceOrientationChange, false );

			sensors.orientation.active = true;

		}

	},

	stop: function () {

		if ( sensors.orientation.active ) {

			window.removeEventListener( 'deviceorientation', handleDeviceOrientationChange, false );

			sensors.orientation.active = false;

		}

	},

	listen: function( callback ) {

		this.start( callback );

	},

	getFixedFrameQuaternion: (function () {

		var euler = new FULLTILT.Euler();
		var matrix = new FULLTILT.RotationMatrix();
		var quaternion = new FULLTILT.Quaternion();

		return function() {

			var orientationData = sensors.orientation.data || { alpha: 0, beta: 0, gamma: 0 };

			var adjustedAlpha = orientationData.alpha;

			if (this.alphaOffsetDevice) {
				matrix.setFromEuler( this.alphaOffsetDevice );
				matrix.rotateZ( - this.alphaOffsetScreen );
				euler.setFromRotationMatrix( matrix );

				if (euler.alpha < 0) {
					euler.alpha += 360;
				}

				euler.alpha %= 360;

				adjustedAlpha -= euler.alpha;
			}

			euler.set(
				adjustedAlpha,
				orientationData.beta,
				orientationData.gamma
			);

			quaternion.setFromEuler( euler );

			return quaternion;

		};

	})(),

	getScreenAdjustedQuaternion: (function () {

		var quaternion;

		return function() {

			quaternion = this.getFixedFrameQuaternion();

			// Automatically apply screen orientation transform
			quaternion.rotateZ( - screenOrientationAngle );

			return quaternion;

		};

	})(),

	getFixedFrameMatrix: (function () {

		var euler = new FULLTILT.Euler();
		var matrix = new FULLTILT.RotationMatrix();

		return function () {

			var orientationData = sensors.orientation.data || { alpha: 0, beta: 0, gamma: 0 };

			var adjustedAlpha = orientationData.alpha;

			if (this.alphaOffsetDevice) {
				matrix.setFromEuler( this.alphaOffsetDevice );
				matrix.rotateZ( - this.alphaOffsetScreen );
				euler.setFromRotationMatrix( matrix );

				if (euler.alpha < 0) {
					euler.alpha += 360;
				}

				euler.alpha %= 360;

				adjustedAlpha -= euler.alpha;
			}

			euler.set(
				adjustedAlpha,
				orientationData.beta,
				orientationData.gamma
			);

			matrix.setFromEuler( euler );

			return matrix;

		};

	})(),

	getScreenAdjustedMatrix: (function () {

		var matrix;

		return function () {

			matrix = this.getFixedFrameMatrix();

			// Automatically apply screen orientation transform
			matrix.rotateZ( - screenOrientationAngle );

			return matrix;

		};

	})(),

	getFixedFrameEuler: (function () {

		var euler = new FULLTILT.Euler();
		var matrix;

		return function () {

			matrix = this.getFixedFrameMatrix();

			euler.setFromRotationMatrix( matrix );

			return euler;

		};

	})(),

	getScreenAdjustedEuler: (function () {

		var euler = new FULLTILT.Euler();
		var matrix;

		return function () {

			matrix = this.getScreenAdjustedMatrix();

			euler.setFromRotationMatrix( matrix );

			return euler;

		};

	})(),

	isAbsolute: function () {

		if ( sensors.orientation.data && sensors.orientation.data.absolute === true ) {
			return true;
		}

		return false;

	},

	getLastRawEventData: function () {

		return sensors.orientation.data || {};

	},

	ALPHA: 'alpha',
	BETA: 'beta',
	GAMMA: 'gamma'

};


///// FULLTILT.DeviceMotion //////

FULLTILT.DeviceMotion = function (options) {

	this.options = options || {}; // placeholder object since no options are currently supported

};

FULLTILT.DeviceMotion.prototype = {

	constructor: FULLTILT.DeviceMotion,

	start: function ( callback ) {

		if ( callback && Object.prototype.toString.call( callback ) == '[object Function]' ) {

			sensors.motion.callbacks.push( callback );

		}

		if( !screenActive ) {

			if ( hasScreenOrientationAPI ) {

				window.screen.orientation.addEventListener( 'change', handleScreenOrientationChange, false );

			} else {

				window.addEventListener( 'orientationchange', handleScreenOrientationChange, false );

			}

		}

		if ( !sensors.motion.active ) {

			window.addEventListener( 'devicemotion', handleDeviceMotionChange, false );

			sensors.motion.active = true;

		}

	},

	stop: function () {

		if ( sensors.motion.active ) {

			window.removeEventListener( 'devicemotion', handleDeviceMotionChange, false );

			sensors.motion.active = false;

		}

	},

	listen: function( callback ) {

		this.start( callback );

	},

	getScreenAdjustedAcceleration: function () {

		var accData = sensors.motion.data && sensors.motion.data.acceleration ? sensors.motion.data.acceleration : { x: 0, y: 0, z: 0 };
		var screenAccData = {};

		switch ( screenOrientationAngle ) {
			case SCREEN_ROTATION_90:
				screenAccData.x = - accData.y;
				screenAccData.y =   accData.x;
				break;
			case SCREEN_ROTATION_180:
				screenAccData.x = - accData.x;
				screenAccData.y = - accData.y;
				break;
			case SCREEN_ROTATION_270:
			case SCREEN_ROTATION_MINUS_90:
				screenAccData.x =   accData.y;
				screenAccData.y = - accData.x;
				break;
			default: // SCREEN_ROTATION_0
				screenAccData.x =   accData.x;
				screenAccData.y =   accData.y;
				break;
		}

		screenAccData.z = accData.z;

		return screenAccData;

	},

	getScreenAdjustedAccelerationIncludingGravity: function () {

		var accGData = sensors.motion.data && sensors.motion.data.accelerationIncludingGravity ? sensors.motion.data.accelerationIncludingGravity : { x: 0, y: 0, z: 0 };
		var screenAccGData = {};

		switch ( screenOrientationAngle ) {
			case SCREEN_ROTATION_90:
				screenAccGData.x = - accGData.y;
				screenAccGData.y =   accGData.x;
				break;
			case SCREEN_ROTATION_180:
				screenAccGData.x = - accGData.x;
				screenAccGData.y = - accGData.y;
				break;
			case SCREEN_ROTATION_270:
			case SCREEN_ROTATION_MINUS_90:
				screenAccGData.x =   accGData.y;
				screenAccGData.y = - accGData.x;
				break;
			default: // SCREEN_ROTATION_0
				screenAccGData.x =   accGData.x;
				screenAccGData.y =   accGData.y;
				break;
		}

		screenAccGData.z = accGData.z;

		return screenAccGData;

	},

	getScreenAdjustedRotationRate: function () {

		var rotRateData = sensors.motion.data && sensors.motion.data.rotationRate ? sensors.motion.data.rotationRate : { alpha: 0, beta: 0, gamma: 0 };
		var screenRotRateData = {};

		switch ( screenOrientationAngle ) {
			case SCREEN_ROTATION_90:
				screenRotRateData.beta  = - rotRateData.gamma;
				screenRotRateData.gamma =   rotRateData.beta;
				break;
			case SCREEN_ROTATION_180:
				screenRotRateData.beta  = - rotRateData.beta;
				screenRotRateData.gamma = - rotRateData.gamma;
				break;
			case SCREEN_ROTATION_270:
			case SCREEN_ROTATION_MINUS_90:
				screenRotRateData.beta  =   rotRateData.gamma;
				screenRotRateData.gamma = - rotRateData.beta;
				break;
			default: // SCREEN_ROTATION_0
				screenRotRateData.beta  =   rotRateData.beta;
				screenRotRateData.gamma =   rotRateData.gamma;
				break;
		}

		screenRotRateData.alpha = rotRateData.alpha;

		return screenRotRateData;

	},

	getLastRawEventData: function () {

		return sensors.motion.data || {};

	}

};


////// Attach FULLTILT to root DOM element //////

window.FULLTILT = FULLTILT;

})( window );

/***/ }),

/***/ "../../node_modules/lodash/_Symbol.js":
/*!**********************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_Symbol.js ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var root = __webpack_require__(/*! ./_root */ "../../node_modules/lodash/_root.js");

/** Built-in value references. */
var Symbol = root.Symbol;

module.exports = Symbol;


/***/ }),

/***/ "../../node_modules/lodash/_arrayLikeKeys.js":
/*!*****************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_arrayLikeKeys.js ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var baseTimes = __webpack_require__(/*! ./_baseTimes */ "../../node_modules/lodash/_baseTimes.js"),
    isArguments = __webpack_require__(/*! ./isArguments */ "../../node_modules/lodash/isArguments.js"),
    isArray = __webpack_require__(/*! ./isArray */ "../../node_modules/lodash/isArray.js"),
    isBuffer = __webpack_require__(/*! ./isBuffer */ "../../node_modules/lodash/isBuffer.js"),
    isIndex = __webpack_require__(/*! ./_isIndex */ "../../node_modules/lodash/_isIndex.js"),
    isTypedArray = __webpack_require__(/*! ./isTypedArray */ "../../node_modules/lodash/isTypedArray.js");

/** Used for built-in method references. */
var objectProto = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty = objectProto.hasOwnProperty;

/**
 * Creates an array of the enumerable property names of the array-like `value`.
 *
 * @private
 * @param {*} value The value to query.
 * @param {boolean} inherited Specify returning inherited property names.
 * @returns {Array} Returns the array of property names.
 */
function arrayLikeKeys(value, inherited) {
  var isArr = isArray(value),
      isArg = !isArr && isArguments(value),
      isBuff = !isArr && !isArg && isBuffer(value),
      isType = !isArr && !isArg && !isBuff && isTypedArray(value),
      skipIndexes = isArr || isArg || isBuff || isType,
      result = skipIndexes ? baseTimes(value.length, String) : [],
      length = result.length;

  for (var key in value) {
    if ((inherited || hasOwnProperty.call(value, key)) &&
        !(skipIndexes && (
           // Safari 9 has enumerable `arguments.length` in strict mode.
           key == 'length' ||
           // Node.js 0.10 has enumerable non-index properties on buffers.
           (isBuff && (key == 'offset' || key == 'parent')) ||
           // PhantomJS 2 has enumerable non-index properties on typed arrays.
           (isType && (key == 'buffer' || key == 'byteLength' || key == 'byteOffset')) ||
           // Skip index properties.
           isIndex(key, length)
        ))) {
      result.push(key);
    }
  }
  return result;
}

module.exports = arrayLikeKeys;


/***/ }),

/***/ "../../node_modules/lodash/_baseGetTag.js":
/*!**************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_baseGetTag.js ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var Symbol = __webpack_require__(/*! ./_Symbol */ "../../node_modules/lodash/_Symbol.js"),
    getRawTag = __webpack_require__(/*! ./_getRawTag */ "../../node_modules/lodash/_getRawTag.js"),
    objectToString = __webpack_require__(/*! ./_objectToString */ "../../node_modules/lodash/_objectToString.js");

/** `Object#toString` result references. */
var nullTag = '[object Null]',
    undefinedTag = '[object Undefined]';

/** Built-in value references. */
var symToStringTag = Symbol ? Symbol.toStringTag : undefined;

/**
 * The base implementation of `getTag` without fallbacks for buggy environments.
 *
 * @private
 * @param {*} value The value to query.
 * @returns {string} Returns the `toStringTag`.
 */
function baseGetTag(value) {
  if (value == null) {
    return value === undefined ? undefinedTag : nullTag;
  }
  return (symToStringTag && symToStringTag in Object(value))
    ? getRawTag(value)
    : objectToString(value);
}

module.exports = baseGetTag;


/***/ }),

/***/ "../../node_modules/lodash/_baseIsArguments.js":
/*!*******************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_baseIsArguments.js ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var baseGetTag = __webpack_require__(/*! ./_baseGetTag */ "../../node_modules/lodash/_baseGetTag.js"),
    isObjectLike = __webpack_require__(/*! ./isObjectLike */ "../../node_modules/lodash/isObjectLike.js");

/** `Object#toString` result references. */
var argsTag = '[object Arguments]';

/**
 * The base implementation of `_.isArguments`.
 *
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is an `arguments` object,
 */
function baseIsArguments(value) {
  return isObjectLike(value) && baseGetTag(value) == argsTag;
}

module.exports = baseIsArguments;


/***/ }),

/***/ "../../node_modules/lodash/_baseIsTypedArray.js":
/*!********************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_baseIsTypedArray.js ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var baseGetTag = __webpack_require__(/*! ./_baseGetTag */ "../../node_modules/lodash/_baseGetTag.js"),
    isLength = __webpack_require__(/*! ./isLength */ "../../node_modules/lodash/isLength.js"),
    isObjectLike = __webpack_require__(/*! ./isObjectLike */ "../../node_modules/lodash/isObjectLike.js");

/** `Object#toString` result references. */
var argsTag = '[object Arguments]',
    arrayTag = '[object Array]',
    boolTag = '[object Boolean]',
    dateTag = '[object Date]',
    errorTag = '[object Error]',
    funcTag = '[object Function]',
    mapTag = '[object Map]',
    numberTag = '[object Number]',
    objectTag = '[object Object]',
    regexpTag = '[object RegExp]',
    setTag = '[object Set]',
    stringTag = '[object String]',
    weakMapTag = '[object WeakMap]';

var arrayBufferTag = '[object ArrayBuffer]',
    dataViewTag = '[object DataView]',
    float32Tag = '[object Float32Array]',
    float64Tag = '[object Float64Array]',
    int8Tag = '[object Int8Array]',
    int16Tag = '[object Int16Array]',
    int32Tag = '[object Int32Array]',
    uint8Tag = '[object Uint8Array]',
    uint8ClampedTag = '[object Uint8ClampedArray]',
    uint16Tag = '[object Uint16Array]',
    uint32Tag = '[object Uint32Array]';

/** Used to identify `toStringTag` values of typed arrays. */
var typedArrayTags = {};
typedArrayTags[float32Tag] = typedArrayTags[float64Tag] =
typedArrayTags[int8Tag] = typedArrayTags[int16Tag] =
typedArrayTags[int32Tag] = typedArrayTags[uint8Tag] =
typedArrayTags[uint8ClampedTag] = typedArrayTags[uint16Tag] =
typedArrayTags[uint32Tag] = true;
typedArrayTags[argsTag] = typedArrayTags[arrayTag] =
typedArrayTags[arrayBufferTag] = typedArrayTags[boolTag] =
typedArrayTags[dataViewTag] = typedArrayTags[dateTag] =
typedArrayTags[errorTag] = typedArrayTags[funcTag] =
typedArrayTags[mapTag] = typedArrayTags[numberTag] =
typedArrayTags[objectTag] = typedArrayTags[regexpTag] =
typedArrayTags[setTag] = typedArrayTags[stringTag] =
typedArrayTags[weakMapTag] = false;

/**
 * The base implementation of `_.isTypedArray` without Node.js optimizations.
 *
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a typed array, else `false`.
 */
function baseIsTypedArray(value) {
  return isObjectLike(value) &&
    isLength(value.length) && !!typedArrayTags[baseGetTag(value)];
}

module.exports = baseIsTypedArray;


/***/ }),

/***/ "../../node_modules/lodash/_baseKeys.js":
/*!************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_baseKeys.js ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var isPrototype = __webpack_require__(/*! ./_isPrototype */ "../../node_modules/lodash/_isPrototype.js"),
    nativeKeys = __webpack_require__(/*! ./_nativeKeys */ "../../node_modules/lodash/_nativeKeys.js");

/** Used for built-in method references. */
var objectProto = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty = objectProto.hasOwnProperty;

/**
 * The base implementation of `_.keys` which doesn't treat sparse arrays as dense.
 *
 * @private
 * @param {Object} object The object to query.
 * @returns {Array} Returns the array of property names.
 */
function baseKeys(object) {
  if (!isPrototype(object)) {
    return nativeKeys(object);
  }
  var result = [];
  for (var key in Object(object)) {
    if (hasOwnProperty.call(object, key) && key != 'constructor') {
      result.push(key);
    }
  }
  return result;
}

module.exports = baseKeys;


/***/ }),

/***/ "../../node_modules/lodash/_baseTimes.js":
/*!*************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_baseTimes.js ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/**
 * The base implementation of `_.times` without support for iteratee shorthands
 * or max array length checks.
 *
 * @private
 * @param {number} n The number of times to invoke `iteratee`.
 * @param {Function} iteratee The function invoked per iteration.
 * @returns {Array} Returns the array of results.
 */
function baseTimes(n, iteratee) {
  var index = -1,
      result = Array(n);

  while (++index < n) {
    result[index] = iteratee(index);
  }
  return result;
}

module.exports = baseTimes;


/***/ }),

/***/ "../../node_modules/lodash/_baseUnary.js":
/*!*************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_baseUnary.js ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/**
 * The base implementation of `_.unary` without support for storing metadata.
 *
 * @private
 * @param {Function} func The function to cap arguments for.
 * @returns {Function} Returns the new capped function.
 */
function baseUnary(func) {
  return function(value) {
    return func(value);
  };
}

module.exports = baseUnary;


/***/ }),

/***/ "../../node_modules/lodash/_freeGlobal.js":
/*!**************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_freeGlobal.js ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {/** Detect free variable `global` from Node.js. */
var freeGlobal = typeof global == 'object' && global && global.Object === Object && global;

module.exports = freeGlobal;

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../webpack/buildin/global.js */ "../../node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "../../node_modules/lodash/_getRawTag.js":
/*!*************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_getRawTag.js ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var Symbol = __webpack_require__(/*! ./_Symbol */ "../../node_modules/lodash/_Symbol.js");

/** Used for built-in method references. */
var objectProto = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty = objectProto.hasOwnProperty;

/**
 * Used to resolve the
 * [`toStringTag`](http://ecma-international.org/ecma-262/7.0/#sec-object.prototype.tostring)
 * of values.
 */
var nativeObjectToString = objectProto.toString;

/** Built-in value references. */
var symToStringTag = Symbol ? Symbol.toStringTag : undefined;

/**
 * A specialized version of `baseGetTag` which ignores `Symbol.toStringTag` values.
 *
 * @private
 * @param {*} value The value to query.
 * @returns {string} Returns the raw `toStringTag`.
 */
function getRawTag(value) {
  var isOwn = hasOwnProperty.call(value, symToStringTag),
      tag = value[symToStringTag];

  try {
    value[symToStringTag] = undefined;
    var unmasked = true;
  } catch (e) {}

  var result = nativeObjectToString.call(value);
  if (unmasked) {
    if (isOwn) {
      value[symToStringTag] = tag;
    } else {
      delete value[symToStringTag];
    }
  }
  return result;
}

module.exports = getRawTag;


/***/ }),

/***/ "../../node_modules/lodash/_isIndex.js":
/*!***********************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_isIndex.js ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/** Used as references for various `Number` constants. */
var MAX_SAFE_INTEGER = 9007199254740991;

/** Used to detect unsigned integer values. */
var reIsUint = /^(?:0|[1-9]\d*)$/;

/**
 * Checks if `value` is a valid array-like index.
 *
 * @private
 * @param {*} value The value to check.
 * @param {number} [length=MAX_SAFE_INTEGER] The upper bounds of a valid index.
 * @returns {boolean} Returns `true` if `value` is a valid index, else `false`.
 */
function isIndex(value, length) {
  var type = typeof value;
  length = length == null ? MAX_SAFE_INTEGER : length;

  return !!length &&
    (type == 'number' ||
      (type != 'symbol' && reIsUint.test(value))) &&
        (value > -1 && value % 1 == 0 && value < length);
}

module.exports = isIndex;


/***/ }),

/***/ "../../node_modules/lodash/_isPrototype.js":
/*!***************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_isPrototype.js ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/** Used for built-in method references. */
var objectProto = Object.prototype;

/**
 * Checks if `value` is likely a prototype object.
 *
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a prototype, else `false`.
 */
function isPrototype(value) {
  var Ctor = value && value.constructor,
      proto = (typeof Ctor == 'function' && Ctor.prototype) || objectProto;

  return value === proto;
}

module.exports = isPrototype;


/***/ }),

/***/ "../../node_modules/lodash/_nativeKeys.js":
/*!**************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_nativeKeys.js ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var overArg = __webpack_require__(/*! ./_overArg */ "../../node_modules/lodash/_overArg.js");

/* Built-in method references for those with the same name as other `lodash` methods. */
var nativeKeys = overArg(Object.keys, Object);

module.exports = nativeKeys;


/***/ }),

/***/ "../../node_modules/lodash/_nodeUtil.js":
/*!************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_nodeUtil.js ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(module) {var freeGlobal = __webpack_require__(/*! ./_freeGlobal */ "../../node_modules/lodash/_freeGlobal.js");

/** Detect free variable `exports`. */
var freeExports =  true && exports && !exports.nodeType && exports;

/** Detect free variable `module`. */
var freeModule = freeExports && typeof module == 'object' && module && !module.nodeType && module;

/** Detect the popular CommonJS extension `module.exports`. */
var moduleExports = freeModule && freeModule.exports === freeExports;

/** Detect free variable `process` from Node.js. */
var freeProcess = moduleExports && freeGlobal.process;

/** Used to access faster Node.js helpers. */
var nodeUtil = (function() {
  try {
    // Use `util.types` for Node.js 10+.
    var types = freeModule && freeModule.require && freeModule.require('util').types;

    if (types) {
      return types;
    }

    // Legacy `process.binding('util')` for Node.js < 10.
    return freeProcess && freeProcess.binding && freeProcess.binding('util');
  } catch (e) {}
}());

module.exports = nodeUtil;

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../webpack/buildin/module.js */ "../../node_modules/webpack/buildin/module.js")(module)))

/***/ }),

/***/ "../../node_modules/lodash/_objectToString.js":
/*!******************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_objectToString.js ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/** Used for built-in method references. */
var objectProto = Object.prototype;

/**
 * Used to resolve the
 * [`toStringTag`](http://ecma-international.org/ecma-262/7.0/#sec-object.prototype.tostring)
 * of values.
 */
var nativeObjectToString = objectProto.toString;

/**
 * Converts `value` to a string using `Object.prototype.toString`.
 *
 * @private
 * @param {*} value The value to convert.
 * @returns {string} Returns the converted string.
 */
function objectToString(value) {
  return nativeObjectToString.call(value);
}

module.exports = objectToString;


/***/ }),

/***/ "../../node_modules/lodash/_overArg.js":
/*!***********************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_overArg.js ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/**
 * Creates a unary function that invokes `func` with its argument transformed.
 *
 * @private
 * @param {Function} func The function to wrap.
 * @param {Function} transform The argument transform.
 * @returns {Function} Returns the new function.
 */
function overArg(func, transform) {
  return function(arg) {
    return func(transform(arg));
  };
}

module.exports = overArg;


/***/ }),

/***/ "../../node_modules/lodash/_root.js":
/*!********************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/_root.js ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var freeGlobal = __webpack_require__(/*! ./_freeGlobal */ "../../node_modules/lodash/_freeGlobal.js");

/** Detect free variable `self`. */
var freeSelf = typeof self == 'object' && self && self.Object === Object && self;

/** Used as a reference to the global object. */
var root = freeGlobal || freeSelf || Function('return this')();

module.exports = root;


/***/ }),

/***/ "../../node_modules/lodash/isArguments.js":
/*!**************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/isArguments.js ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var baseIsArguments = __webpack_require__(/*! ./_baseIsArguments */ "../../node_modules/lodash/_baseIsArguments.js"),
    isObjectLike = __webpack_require__(/*! ./isObjectLike */ "../../node_modules/lodash/isObjectLike.js");

/** Used for built-in method references. */
var objectProto = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty = objectProto.hasOwnProperty;

/** Built-in value references. */
var propertyIsEnumerable = objectProto.propertyIsEnumerable;

/**
 * Checks if `value` is likely an `arguments` object.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is an `arguments` object,
 *  else `false`.
 * @example
 *
 * _.isArguments(function() { return arguments; }());
 * // => true
 *
 * _.isArguments([1, 2, 3]);
 * // => false
 */
var isArguments = baseIsArguments(function() { return arguments; }()) ? baseIsArguments : function(value) {
  return isObjectLike(value) && hasOwnProperty.call(value, 'callee') &&
    !propertyIsEnumerable.call(value, 'callee');
};

module.exports = isArguments;


/***/ }),

/***/ "../../node_modules/lodash/isArray.js":
/*!**********************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/isArray.js ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/**
 * Checks if `value` is classified as an `Array` object.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is an array, else `false`.
 * @example
 *
 * _.isArray([1, 2, 3]);
 * // => true
 *
 * _.isArray(document.body.children);
 * // => false
 *
 * _.isArray('abc');
 * // => false
 *
 * _.isArray(_.noop);
 * // => false
 */
var isArray = Array.isArray;

module.exports = isArray;


/***/ }),

/***/ "../../node_modules/lodash/isArrayLike.js":
/*!**************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/isArrayLike.js ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var isFunction = __webpack_require__(/*! ./isFunction */ "../../node_modules/lodash/isFunction.js"),
    isLength = __webpack_require__(/*! ./isLength */ "../../node_modules/lodash/isLength.js");

/**
 * Checks if `value` is array-like. A value is considered array-like if it's
 * not a function and has a `value.length` that's an integer greater than or
 * equal to `0` and less than or equal to `Number.MAX_SAFE_INTEGER`.
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is array-like, else `false`.
 * @example
 *
 * _.isArrayLike([1, 2, 3]);
 * // => true
 *
 * _.isArrayLike(document.body.children);
 * // => true
 *
 * _.isArrayLike('abc');
 * // => true
 *
 * _.isArrayLike(_.noop);
 * // => false
 */
function isArrayLike(value) {
  return value != null && isLength(value.length) && !isFunction(value);
}

module.exports = isArrayLike;


/***/ }),

/***/ "../../node_modules/lodash/isBuffer.js":
/*!***********************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/isBuffer.js ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(module) {var root = __webpack_require__(/*! ./_root */ "../../node_modules/lodash/_root.js"),
    stubFalse = __webpack_require__(/*! ./stubFalse */ "../../node_modules/lodash/stubFalse.js");

/** Detect free variable `exports`. */
var freeExports =  true && exports && !exports.nodeType && exports;

/** Detect free variable `module`. */
var freeModule = freeExports && typeof module == 'object' && module && !module.nodeType && module;

/** Detect the popular CommonJS extension `module.exports`. */
var moduleExports = freeModule && freeModule.exports === freeExports;

/** Built-in value references. */
var Buffer = moduleExports ? root.Buffer : undefined;

/* Built-in method references for those with the same name as other `lodash` methods. */
var nativeIsBuffer = Buffer ? Buffer.isBuffer : undefined;

/**
 * Checks if `value` is a buffer.
 *
 * @static
 * @memberOf _
 * @since 4.3.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a buffer, else `false`.
 * @example
 *
 * _.isBuffer(new Buffer(2));
 * // => true
 *
 * _.isBuffer(new Uint8Array(2));
 * // => false
 */
var isBuffer = nativeIsBuffer || stubFalse;

module.exports = isBuffer;

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../webpack/buildin/module.js */ "../../node_modules/webpack/buildin/module.js")(module)))

/***/ }),

/***/ "../../node_modules/lodash/isFunction.js":
/*!*************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/isFunction.js ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var baseGetTag = __webpack_require__(/*! ./_baseGetTag */ "../../node_modules/lodash/_baseGetTag.js"),
    isObject = __webpack_require__(/*! ./isObject */ "../../node_modules/lodash/isObject.js");

/** `Object#toString` result references. */
var asyncTag = '[object AsyncFunction]',
    funcTag = '[object Function]',
    genTag = '[object GeneratorFunction]',
    proxyTag = '[object Proxy]';

/**
 * Checks if `value` is classified as a `Function` object.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a function, else `false`.
 * @example
 *
 * _.isFunction(_);
 * // => true
 *
 * _.isFunction(/abc/);
 * // => false
 */
function isFunction(value) {
  if (!isObject(value)) {
    return false;
  }
  // The use of `Object#toString` avoids issues with the `typeof` operator
  // in Safari 9 which returns 'object' for typed arrays and other constructors.
  var tag = baseGetTag(value);
  return tag == funcTag || tag == genTag || tag == asyncTag || tag == proxyTag;
}

module.exports = isFunction;


/***/ }),

/***/ "../../node_modules/lodash/isLength.js":
/*!***********************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/isLength.js ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/** Used as references for various `Number` constants. */
var MAX_SAFE_INTEGER = 9007199254740991;

/**
 * Checks if `value` is a valid array-like length.
 *
 * **Note:** This method is loosely based on
 * [`ToLength`](http://ecma-international.org/ecma-262/7.0/#sec-tolength).
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a valid length, else `false`.
 * @example
 *
 * _.isLength(3);
 * // => true
 *
 * _.isLength(Number.MIN_VALUE);
 * // => false
 *
 * _.isLength(Infinity);
 * // => false
 *
 * _.isLength('3');
 * // => false
 */
function isLength(value) {
  return typeof value == 'number' &&
    value > -1 && value % 1 == 0 && value <= MAX_SAFE_INTEGER;
}

module.exports = isLength;


/***/ }),

/***/ "../../node_modules/lodash/isObject.js":
/*!***********************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/isObject.js ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/**
 * Checks if `value` is the
 * [language type](http://www.ecma-international.org/ecma-262/7.0/#sec-ecmascript-language-types)
 * of `Object`. (e.g. arrays, functions, objects, regexes, `new Number(0)`, and `new String('')`)
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is an object, else `false`.
 * @example
 *
 * _.isObject({});
 * // => true
 *
 * _.isObject([1, 2, 3]);
 * // => true
 *
 * _.isObject(_.noop);
 * // => true
 *
 * _.isObject(null);
 * // => false
 */
function isObject(value) {
  var type = typeof value;
  return value != null && (type == 'object' || type == 'function');
}

module.exports = isObject;


/***/ }),

/***/ "../../node_modules/lodash/isObjectLike.js":
/*!***************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/isObjectLike.js ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/**
 * Checks if `value` is object-like. A value is object-like if it's not `null`
 * and has a `typeof` result of "object".
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is object-like, else `false`.
 * @example
 *
 * _.isObjectLike({});
 * // => true
 *
 * _.isObjectLike([1, 2, 3]);
 * // => true
 *
 * _.isObjectLike(_.noop);
 * // => false
 *
 * _.isObjectLike(null);
 * // => false
 */
function isObjectLike(value) {
  return value != null && typeof value == 'object';
}

module.exports = isObjectLike;


/***/ }),

/***/ "../../node_modules/lodash/isTypedArray.js":
/*!***************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/isTypedArray.js ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var baseIsTypedArray = __webpack_require__(/*! ./_baseIsTypedArray */ "../../node_modules/lodash/_baseIsTypedArray.js"),
    baseUnary = __webpack_require__(/*! ./_baseUnary */ "../../node_modules/lodash/_baseUnary.js"),
    nodeUtil = __webpack_require__(/*! ./_nodeUtil */ "../../node_modules/lodash/_nodeUtil.js");

/* Node.js helper references. */
var nodeIsTypedArray = nodeUtil && nodeUtil.isTypedArray;

/**
 * Checks if `value` is classified as a typed array.
 *
 * @static
 * @memberOf _
 * @since 3.0.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a typed array, else `false`.
 * @example
 *
 * _.isTypedArray(new Uint8Array);
 * // => true
 *
 * _.isTypedArray([]);
 * // => false
 */
var isTypedArray = nodeIsTypedArray ? baseUnary(nodeIsTypedArray) : baseIsTypedArray;

module.exports = isTypedArray;


/***/ }),

/***/ "../../node_modules/lodash/keys.js":
/*!*******************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/keys.js ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var arrayLikeKeys = __webpack_require__(/*! ./_arrayLikeKeys */ "../../node_modules/lodash/_arrayLikeKeys.js"),
    baseKeys = __webpack_require__(/*! ./_baseKeys */ "../../node_modules/lodash/_baseKeys.js"),
    isArrayLike = __webpack_require__(/*! ./isArrayLike */ "../../node_modules/lodash/isArrayLike.js");

/**
 * Creates an array of the own enumerable property names of `object`.
 *
 * **Note:** Non-object values are coerced to objects. See the
 * [ES spec](http://ecma-international.org/ecma-262/7.0/#sec-object.keys)
 * for more details.
 *
 * @static
 * @since 0.1.0
 * @memberOf _
 * @category Object
 * @param {Object} object The object to query.
 * @returns {Array} Returns the array of property names.
 * @example
 *
 * function Foo() {
 *   this.a = 1;
 *   this.b = 2;
 * }
 *
 * Foo.prototype.c = 3;
 *
 * _.keys(new Foo);
 * // => ['a', 'b'] (iteration order is not guaranteed)
 *
 * _.keys('hi');
 * // => ['0', '1']
 */
function keys(object) {
  return isArrayLike(object) ? arrayLikeKeys(object) : baseKeys(object);
}

module.exports = keys;


/***/ }),

/***/ "../../node_modules/lodash/stubFalse.js":
/*!************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/lodash/stubFalse.js ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/**
 * This method returns `false`.
 *
 * @static
 * @memberOf _
 * @since 4.13.0
 * @category Util
 * @returns {boolean} Returns `false`.
 * @example
 *
 * _.times(2, _.stubFalse);
 * // => [false, false]
 */
function stubFalse() {
  return false;
}

module.exports = stubFalse;


/***/ }),

/***/ "../../node_modules/mobile-detect/mobile-detect.js":
/*!***********************************************************************************!*\
  !*** D:/Users/dani/Desktop/@monogrid/node_modules/mobile-detect/mobile-detect.js ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// THIS FILE IS GENERATED - DO NOT EDIT!
/*!mobile-detect v1.4.4 2019-09-21*/
/*global module:false, define:false*/
/*jshint latedef:false*/
/*!@license Copyright 2013, Heinrich Goebl, License: MIT, see https://github.com/hgoebl/mobile-detect.js*/
(function (define, undefined) {
define(function () {
    'use strict';

    var impl = {};

    impl.mobileDetectRules = {
    "phones": {
        "iPhone": "\\biPhone\\b|\\biPod\\b",
        "BlackBerry": "BlackBerry|\\bBB10\\b|rim[0-9]+|\\b(BBA100|BBB100|BBD100|BBE100|BBF100|STH100)\\b-[0-9]+",
        "HTC": "HTC|HTC.*(Sensation|Evo|Vision|Explorer|6800|8100|8900|A7272|S510e|C110e|Legend|Desire|T8282)|APX515CKT|Qtek9090|APA9292KT|HD_mini|Sensation.*Z710e|PG86100|Z715e|Desire.*(A8181|HD)|ADR6200|ADR6400L|ADR6425|001HT|Inspire 4G|Android.*\\bEVO\\b|T-Mobile G1|Z520m|Android [0-9.]+; Pixel",
        "Nexus": "Nexus One|Nexus S|Galaxy.*Nexus|Android.*Nexus.*Mobile|Nexus 4|Nexus 5|Nexus 6",
        "Dell": "Dell[;]? (Streak|Aero|Venue|Venue Pro|Flash|Smoke|Mini 3iX)|XCD28|XCD35|\\b001DL\\b|\\b101DL\\b|\\bGS01\\b",
        "Motorola": "Motorola|DROIDX|DROID BIONIC|\\bDroid\\b.*Build|Android.*Xoom|HRI39|MOT-|A1260|A1680|A555|A853|A855|A953|A955|A956|Motorola.*ELECTRIFY|Motorola.*i1|i867|i940|MB200|MB300|MB501|MB502|MB508|MB511|MB520|MB525|MB526|MB611|MB612|MB632|MB810|MB855|MB860|MB861|MB865|MB870|ME501|ME502|ME511|ME525|ME600|ME632|ME722|ME811|ME860|ME863|ME865|MT620|MT710|MT716|MT720|MT810|MT870|MT917|Motorola.*TITANIUM|WX435|WX445|XT300|XT301|XT311|XT316|XT317|XT319|XT320|XT390|XT502|XT530|XT531|XT532|XT535|XT603|XT610|XT611|XT615|XT681|XT701|XT702|XT711|XT720|XT800|XT806|XT860|XT862|XT875|XT882|XT883|XT894|XT901|XT907|XT909|XT910|XT912|XT928|XT926|XT915|XT919|XT925|XT1021|\\bMoto E\\b|XT1068|XT1092|XT1052",
        "Samsung": "\\bSamsung\\b|SM-G950F|SM-G955F|SM-G9250|GT-19300|SGH-I337|BGT-S5230|GT-B2100|GT-B2700|GT-B2710|GT-B3210|GT-B3310|GT-B3410|GT-B3730|GT-B3740|GT-B5510|GT-B5512|GT-B5722|GT-B6520|GT-B7300|GT-B7320|GT-B7330|GT-B7350|GT-B7510|GT-B7722|GT-B7800|GT-C3010|GT-C3011|GT-C3060|GT-C3200|GT-C3212|GT-C3212I|GT-C3262|GT-C3222|GT-C3300|GT-C3300K|GT-C3303|GT-C3303K|GT-C3310|GT-C3322|GT-C3330|GT-C3350|GT-C3500|GT-C3510|GT-C3530|GT-C3630|GT-C3780|GT-C5010|GT-C5212|GT-C6620|GT-C6625|GT-C6712|GT-E1050|GT-E1070|GT-E1075|GT-E1080|GT-E1081|GT-E1085|GT-E1087|GT-E1100|GT-E1107|GT-E1110|GT-E1120|GT-E1125|GT-E1130|GT-E1160|GT-E1170|GT-E1175|GT-E1180|GT-E1182|GT-E1200|GT-E1210|GT-E1225|GT-E1230|GT-E1390|GT-E2100|GT-E2120|GT-E2121|GT-E2152|GT-E2220|GT-E2222|GT-E2230|GT-E2232|GT-E2250|GT-E2370|GT-E2550|GT-E2652|GT-E3210|GT-E3213|GT-I5500|GT-I5503|GT-I5700|GT-I5800|GT-I5801|GT-I6410|GT-I6420|GT-I7110|GT-I7410|GT-I7500|GT-I8000|GT-I8150|GT-I8160|GT-I8190|GT-I8320|GT-I8330|GT-I8350|GT-I8530|GT-I8700|GT-I8703|GT-I8910|GT-I9000|GT-I9001|GT-I9003|GT-I9010|GT-I9020|GT-I9023|GT-I9070|GT-I9082|GT-I9100|GT-I9103|GT-I9220|GT-I9250|GT-I9300|GT-I9305|GT-I9500|GT-I9505|GT-M3510|GT-M5650|GT-M7500|GT-M7600|GT-M7603|GT-M8800|GT-M8910|GT-N7000|GT-S3110|GT-S3310|GT-S3350|GT-S3353|GT-S3370|GT-S3650|GT-S3653|GT-S3770|GT-S3850|GT-S5210|GT-S5220|GT-S5229|GT-S5230|GT-S5233|GT-S5250|GT-S5253|GT-S5260|GT-S5263|GT-S5270|GT-S5300|GT-S5330|GT-S5350|GT-S5360|GT-S5363|GT-S5369|GT-S5380|GT-S5380D|GT-S5560|GT-S5570|GT-S5600|GT-S5603|GT-S5610|GT-S5620|GT-S5660|GT-S5670|GT-S5690|GT-S5750|GT-S5780|GT-S5830|GT-S5839|GT-S6102|GT-S6500|GT-S7070|GT-S7200|GT-S7220|GT-S7230|GT-S7233|GT-S7250|GT-S7500|GT-S7530|GT-S7550|GT-S7562|GT-S7710|GT-S8000|GT-S8003|GT-S8500|GT-S8530|GT-S8600|SCH-A310|SCH-A530|SCH-A570|SCH-A610|SCH-A630|SCH-A650|SCH-A790|SCH-A795|SCH-A850|SCH-A870|SCH-A890|SCH-A930|SCH-A950|SCH-A970|SCH-A990|SCH-I100|SCH-I110|SCH-I400|SCH-I405|SCH-I500|SCH-I510|SCH-I515|SCH-I600|SCH-I730|SCH-I760|SCH-I770|SCH-I830|SCH-I910|SCH-I920|SCH-I959|SCH-LC11|SCH-N150|SCH-N300|SCH-R100|SCH-R300|SCH-R351|SCH-R400|SCH-R410|SCH-T300|SCH-U310|SCH-U320|SCH-U350|SCH-U360|SCH-U365|SCH-U370|SCH-U380|SCH-U410|SCH-U430|SCH-U450|SCH-U460|SCH-U470|SCH-U490|SCH-U540|SCH-U550|SCH-U620|SCH-U640|SCH-U650|SCH-U660|SCH-U700|SCH-U740|SCH-U750|SCH-U810|SCH-U820|SCH-U900|SCH-U940|SCH-U960|SCS-26UC|SGH-A107|SGH-A117|SGH-A127|SGH-A137|SGH-A157|SGH-A167|SGH-A177|SGH-A187|SGH-A197|SGH-A227|SGH-A237|SGH-A257|SGH-A437|SGH-A517|SGH-A597|SGH-A637|SGH-A657|SGH-A667|SGH-A687|SGH-A697|SGH-A707|SGH-A717|SGH-A727|SGH-A737|SGH-A747|SGH-A767|SGH-A777|SGH-A797|SGH-A817|SGH-A827|SGH-A837|SGH-A847|SGH-A867|SGH-A877|SGH-A887|SGH-A897|SGH-A927|SGH-B100|SGH-B130|SGH-B200|SGH-B220|SGH-C100|SGH-C110|SGH-C120|SGH-C130|SGH-C140|SGH-C160|SGH-C170|SGH-C180|SGH-C200|SGH-C207|SGH-C210|SGH-C225|SGH-C230|SGH-C417|SGH-C450|SGH-D307|SGH-D347|SGH-D357|SGH-D407|SGH-D415|SGH-D780|SGH-D807|SGH-D980|SGH-E105|SGH-E200|SGH-E315|SGH-E316|SGH-E317|SGH-E335|SGH-E590|SGH-E635|SGH-E715|SGH-E890|SGH-F300|SGH-F480|SGH-I200|SGH-I300|SGH-I320|SGH-I550|SGH-I577|SGH-I600|SGH-I607|SGH-I617|SGH-I627|SGH-I637|SGH-I677|SGH-I700|SGH-I717|SGH-I727|SGH-i747M|SGH-I777|SGH-I780|SGH-I827|SGH-I847|SGH-I857|SGH-I896|SGH-I897|SGH-I900|SGH-I907|SGH-I917|SGH-I927|SGH-I937|SGH-I997|SGH-J150|SGH-J200|SGH-L170|SGH-L700|SGH-M110|SGH-M150|SGH-M200|SGH-N105|SGH-N500|SGH-N600|SGH-N620|SGH-N625|SGH-N700|SGH-N710|SGH-P107|SGH-P207|SGH-P300|SGH-P310|SGH-P520|SGH-P735|SGH-P777|SGH-Q105|SGH-R210|SGH-R220|SGH-R225|SGH-S105|SGH-S307|SGH-T109|SGH-T119|SGH-T139|SGH-T209|SGH-T219|SGH-T229|SGH-T239|SGH-T249|SGH-T259|SGH-T309|SGH-T319|SGH-T329|SGH-T339|SGH-T349|SGH-T359|SGH-T369|SGH-T379|SGH-T409|SGH-T429|SGH-T439|SGH-T459|SGH-T469|SGH-T479|SGH-T499|SGH-T509|SGH-T519|SGH-T539|SGH-T559|SGH-T589|SGH-T609|SGH-T619|SGH-T629|SGH-T639|SGH-T659|SGH-T669|SGH-T679|SGH-T709|SGH-T719|SGH-T729|SGH-T739|SGH-T746|SGH-T749|SGH-T759|SGH-T769|SGH-T809|SGH-T819|SGH-T839|SGH-T919|SGH-T929|SGH-T939|SGH-T959|SGH-T989|SGH-U100|SGH-U200|SGH-U800|SGH-V205|SGH-V206|SGH-X100|SGH-X105|SGH-X120|SGH-X140|SGH-X426|SGH-X427|SGH-X475|SGH-X495|SGH-X497|SGH-X507|SGH-X600|SGH-X610|SGH-X620|SGH-X630|SGH-X700|SGH-X820|SGH-X890|SGH-Z130|SGH-Z150|SGH-Z170|SGH-ZX10|SGH-ZX20|SHW-M110|SPH-A120|SPH-A400|SPH-A420|SPH-A460|SPH-A500|SPH-A560|SPH-A600|SPH-A620|SPH-A660|SPH-A700|SPH-A740|SPH-A760|SPH-A790|SPH-A800|SPH-A820|SPH-A840|SPH-A880|SPH-A900|SPH-A940|SPH-A960|SPH-D600|SPH-D700|SPH-D710|SPH-D720|SPH-I300|SPH-I325|SPH-I330|SPH-I350|SPH-I500|SPH-I600|SPH-I700|SPH-L700|SPH-M100|SPH-M220|SPH-M240|SPH-M300|SPH-M305|SPH-M320|SPH-M330|SPH-M350|SPH-M360|SPH-M370|SPH-M380|SPH-M510|SPH-M540|SPH-M550|SPH-M560|SPH-M570|SPH-M580|SPH-M610|SPH-M620|SPH-M630|SPH-M800|SPH-M810|SPH-M850|SPH-M900|SPH-M910|SPH-M920|SPH-M930|SPH-N100|SPH-N200|SPH-N240|SPH-N300|SPH-N400|SPH-Z400|SWC-E100|SCH-i909|GT-N7100|GT-N7105|SCH-I535|SM-N900A|SGH-I317|SGH-T999L|GT-S5360B|GT-I8262|GT-S6802|GT-S6312|GT-S6310|GT-S5312|GT-S5310|GT-I9105|GT-I8510|GT-S6790N|SM-G7105|SM-N9005|GT-S5301|GT-I9295|GT-I9195|SM-C101|GT-S7392|GT-S7560|GT-B7610|GT-I5510|GT-S7582|GT-S7530E|GT-I8750|SM-G9006V|SM-G9008V|SM-G9009D|SM-G900A|SM-G900D|SM-G900F|SM-G900H|SM-G900I|SM-G900J|SM-G900K|SM-G900L|SM-G900M|SM-G900P|SM-G900R4|SM-G900S|SM-G900T|SM-G900V|SM-G900W8|SHV-E160K|SCH-P709|SCH-P729|SM-T2558|GT-I9205|SM-G9350|SM-J120F|SM-G920F|SM-G920V|SM-G930F|SM-N910C|SM-A310F|GT-I9190|SM-J500FN|SM-G903F|SM-J330F",
        "LG": "\\bLG\\b;|LG[- ]?(C800|C900|E400|E610|E900|E-900|F160|F180K|F180L|F180S|730|855|L160|LS740|LS840|LS970|LU6200|MS690|MS695|MS770|MS840|MS870|MS910|P500|P700|P705|VM696|AS680|AS695|AX840|C729|E970|GS505|272|C395|E739BK|E960|L55C|L75C|LS696|LS860|P769BK|P350|P500|P509|P870|UN272|US730|VS840|VS950|LN272|LN510|LS670|LS855|LW690|MN270|MN510|P509|P769|P930|UN200|UN270|UN510|UN610|US670|US740|US760|UX265|UX840|VN271|VN530|VS660|VS700|VS740|VS750|VS910|VS920|VS930|VX9200|VX11000|AX840A|LW770|P506|P925|P999|E612|D955|D802|MS323|M257)|LM-G710",
        "Sony": "SonyST|SonyLT|SonyEricsson|SonyEricssonLT15iv|LT18i|E10i|LT28h|LT26w|SonyEricssonMT27i|C5303|C6902|C6903|C6906|C6943|D2533",
        "Asus": "Asus.*Galaxy|PadFone.*Mobile",
        "NokiaLumia": "Lumia [0-9]{3,4}",
        "Micromax": "Micromax.*\\b(A210|A92|A88|A72|A111|A110Q|A115|A116|A110|A90S|A26|A51|A35|A54|A25|A27|A89|A68|A65|A57|A90)\\b",
        "Palm": "PalmSource|Palm",
        "Vertu": "Vertu|Vertu.*Ltd|Vertu.*Ascent|Vertu.*Ayxta|Vertu.*Constellation(F|Quest)?|Vertu.*Monika|Vertu.*Signature",
        "Pantech": "PANTECH|IM-A850S|IM-A840S|IM-A830L|IM-A830K|IM-A830S|IM-A820L|IM-A810K|IM-A810S|IM-A800S|IM-T100K|IM-A725L|IM-A780L|IM-A775C|IM-A770K|IM-A760S|IM-A750K|IM-A740S|IM-A730S|IM-A720L|IM-A710K|IM-A690L|IM-A690S|IM-A650S|IM-A630K|IM-A600S|VEGA PTL21|PT003|P8010|ADR910L|P6030|P6020|P9070|P4100|P9060|P5000|CDM8992|TXT8045|ADR8995|IS11PT|P2030|P6010|P8000|PT002|IS06|CDM8999|P9050|PT001|TXT8040|P2020|P9020|P2000|P7040|P7000|C790",
        "Fly": "IQ230|IQ444|IQ450|IQ440|IQ442|IQ441|IQ245|IQ256|IQ236|IQ255|IQ235|IQ245|IQ275|IQ240|IQ285|IQ280|IQ270|IQ260|IQ250",
        "Wiko": "KITE 4G|HIGHWAY|GETAWAY|STAIRWAY|DARKSIDE|DARKFULL|DARKNIGHT|DARKMOON|SLIDE|WAX 4G|RAINBOW|BLOOM|SUNSET|GOA(?!nna)|LENNY|BARRY|IGGY|OZZY|CINK FIVE|CINK PEAX|CINK PEAX 2|CINK SLIM|CINK SLIM 2|CINK +|CINK KING|CINK PEAX|CINK SLIM|SUBLIM",
        "iMobile": "i-mobile (IQ|i-STYLE|idea|ZAA|Hitz)",
        "SimValley": "\\b(SP-80|XT-930|SX-340|XT-930|SX-310|SP-360|SP60|SPT-800|SP-120|SPT-800|SP-140|SPX-5|SPX-8|SP-100|SPX-8|SPX-12)\\b",
        "Wolfgang": "AT-B24D|AT-AS50HD|AT-AS40W|AT-AS55HD|AT-AS45q2|AT-B26D|AT-AS50Q",
        "Alcatel": "Alcatel",
        "Nintendo": "Nintendo (3DS|Switch)",
        "Amoi": "Amoi",
        "INQ": "INQ",
        "OnePlus": "ONEPLUS",
        "GenericPhone": "Tapatalk|PDA;|SAGEM|\\bmmp\\b|pocket|\\bpsp\\b|symbian|Smartphone|smartfon|treo|up.browser|up.link|vodafone|\\bwap\\b|nokia|Series40|Series60|S60|SonyEricsson|N900|MAUI.*WAP.*Browser"
    },
    "tablets": {
        "iPad": "iPad|iPad.*Mobile",
        "NexusTablet": "Android.*Nexus[\\s]+(7|9|10)",
        "GoogleTablet": "Android.*Pixel C",
        "SamsungTablet": "SAMSUNG.*Tablet|Galaxy.*Tab|SC-01C|GT-P1000|GT-P1003|GT-P1010|GT-P3105|GT-P6210|GT-P6800|GT-P6810|GT-P7100|GT-P7300|GT-P7310|GT-P7500|GT-P7510|SCH-I800|SCH-I815|SCH-I905|SGH-I957|SGH-I987|SGH-T849|SGH-T859|SGH-T869|SPH-P100|GT-P3100|GT-P3108|GT-P3110|GT-P5100|GT-P5110|GT-P6200|GT-P7320|GT-P7511|GT-N8000|GT-P8510|SGH-I497|SPH-P500|SGH-T779|SCH-I705|SCH-I915|GT-N8013|GT-P3113|GT-P5113|GT-P8110|GT-N8010|GT-N8005|GT-N8020|GT-P1013|GT-P6201|GT-P7501|GT-N5100|GT-N5105|GT-N5110|SHV-E140K|SHV-E140L|SHV-E140S|SHV-E150S|SHV-E230K|SHV-E230L|SHV-E230S|SHW-M180K|SHW-M180L|SHW-M180S|SHW-M180W|SHW-M300W|SHW-M305W|SHW-M380K|SHW-M380S|SHW-M380W|SHW-M430W|SHW-M480K|SHW-M480S|SHW-M480W|SHW-M485W|SHW-M486W|SHW-M500W|GT-I9228|SCH-P739|SCH-I925|GT-I9200|GT-P5200|GT-P5210|GT-P5210X|SM-T311|SM-T310|SM-T310X|SM-T210|SM-T210R|SM-T211|SM-P600|SM-P601|SM-P605|SM-P900|SM-P901|SM-T217|SM-T217A|SM-T217S|SM-P6000|SM-T3100|SGH-I467|XE500|SM-T110|GT-P5220|GT-I9200X|GT-N5110X|GT-N5120|SM-P905|SM-T111|SM-T2105|SM-T315|SM-T320|SM-T320X|SM-T321|SM-T520|SM-T525|SM-T530NU|SM-T230NU|SM-T330NU|SM-T900|XE500T1C|SM-P605V|SM-P905V|SM-T337V|SM-T537V|SM-T707V|SM-T807V|SM-P600X|SM-P900X|SM-T210X|SM-T230|SM-T230X|SM-T325|GT-P7503|SM-T531|SM-T330|SM-T530|SM-T705|SM-T705C|SM-T535|SM-T331|SM-T800|SM-T700|SM-T537|SM-T807|SM-P907A|SM-T337A|SM-T537A|SM-T707A|SM-T807A|SM-T237|SM-T807P|SM-P607T|SM-T217T|SM-T337T|SM-T807T|SM-T116NQ|SM-T116BU|SM-P550|SM-T350|SM-T550|SM-T9000|SM-P9000|SM-T705Y|SM-T805|GT-P3113|SM-T710|SM-T810|SM-T815|SM-T360|SM-T533|SM-T113|SM-T335|SM-T715|SM-T560|SM-T670|SM-T677|SM-T377|SM-T567|SM-T357T|SM-T555|SM-T561|SM-T713|SM-T719|SM-T813|SM-T819|SM-T580|SM-T355Y?|SM-T280|SM-T817A|SM-T820|SM-W700|SM-P580|SM-T587|SM-P350|SM-P555M|SM-P355M|SM-T113NU|SM-T815Y|SM-T585|SM-T285|SM-T825|SM-W708|SM-T835|SM-T830|SM-T837V|SM-T720|SM-T510|SM-T387V",
        "Kindle": "Kindle|Silk.*Accelerated|Android.*\\b(KFOT|KFTT|KFJWI|KFJWA|KFOTE|KFSOWI|KFTHWI|KFTHWA|KFAPWI|KFAPWA|WFJWAE|KFSAWA|KFSAWI|KFASWI|KFARWI|KFFOWI|KFGIWI|KFMEWI)\\b|Android.*Silk\/[0-9.]+ like Chrome\/[0-9.]+ (?!Mobile)",
        "SurfaceTablet": "Windows NT [0-9.]+; ARM;.*(Tablet|ARMBJS)",
        "HPTablet": "HP Slate (7|8|10)|HP ElitePad 900|hp-tablet|EliteBook.*Touch|HP 8|Slate 21|HP SlateBook 10",
        "AsusTablet": "^.*PadFone((?!Mobile).)*$|Transformer|TF101|TF101G|TF300T|TF300TG|TF300TL|TF700T|TF700KL|TF701T|TF810C|ME171|ME301T|ME302C|ME371MG|ME370T|ME372MG|ME172V|ME173X|ME400C|Slider SL101|\\bK00F\\b|\\bK00C\\b|\\bK00E\\b|\\bK00L\\b|TX201LA|ME176C|ME102A|\\bM80TA\\b|ME372CL|ME560CG|ME372CG|ME302KL| K010 | K011 | K017 | K01E |ME572C|ME103K|ME170C|ME171C|\\bME70C\\b|ME581C|ME581CL|ME8510C|ME181C|P01Y|PO1MA|P01Z|\\bP027\\b|\\bP024\\b|\\bP00C\\b",
        "BlackBerryTablet": "PlayBook|RIM Tablet",
        "HTCtablet": "HTC_Flyer_P512|HTC Flyer|HTC Jetstream|HTC-P715a|HTC EVO View 4G|PG41200|PG09410",
        "MotorolaTablet": "xoom|sholest|MZ615|MZ605|MZ505|MZ601|MZ602|MZ603|MZ604|MZ606|MZ607|MZ608|MZ609|MZ615|MZ616|MZ617",
        "NookTablet": "Android.*Nook|NookColor|nook browser|BNRV200|BNRV200A|BNTV250|BNTV250A|BNTV400|BNTV600|LogicPD Zoom2",
        "AcerTablet": "Android.*; \\b(A100|A101|A110|A200|A210|A211|A500|A501|A510|A511|A700|A701|W500|W500P|W501|W501P|W510|W511|W700|G100|G100W|B1-A71|B1-710|B1-711|A1-810|A1-811|A1-830)\\b|W3-810|\\bA3-A10\\b|\\bA3-A11\\b|\\bA3-A20\\b|\\bA3-A30",
        "ToshibaTablet": "Android.*(AT100|AT105|AT200|AT205|AT270|AT275|AT300|AT305|AT1S5|AT500|AT570|AT700|AT830)|TOSHIBA.*FOLIO",
        "LGTablet": "\\bL-06C|LG-V909|LG-V900|LG-V700|LG-V510|LG-V500|LG-V410|LG-V400|LG-VK810\\b",
        "FujitsuTablet": "Android.*\\b(F-01D|F-02F|F-05E|F-10D|M532|Q572)\\b",
        "PrestigioTablet": "PMP3170B|PMP3270B|PMP3470B|PMP7170B|PMP3370B|PMP3570C|PMP5870C|PMP3670B|PMP5570C|PMP5770D|PMP3970B|PMP3870C|PMP5580C|PMP5880D|PMP5780D|PMP5588C|PMP7280C|PMP7280C3G|PMP7280|PMP7880D|PMP5597D|PMP5597|PMP7100D|PER3464|PER3274|PER3574|PER3884|PER5274|PER5474|PMP5097CPRO|PMP5097|PMP7380D|PMP5297C|PMP5297C_QUAD|PMP812E|PMP812E3G|PMP812F|PMP810E|PMP880TD|PMT3017|PMT3037|PMT3047|PMT3057|PMT7008|PMT5887|PMT5001|PMT5002",
        "LenovoTablet": "Lenovo TAB|Idea(Tab|Pad)( A1|A10| K1|)|ThinkPad([ ]+)?Tablet|YT3-850M|YT3-X90L|YT3-X90F|YT3-X90X|Lenovo.*(S2109|S2110|S5000|S6000|K3011|A3000|A3500|A1000|A2107|A2109|A1107|A5500|A7600|B6000|B8000|B8080)(-|)(FL|F|HV|H|)|TB-X103F|TB-X304X|TB-X304F|TB-X304L|TB-X505F|TB-X505L|TB-X505X|TB-X605F|TB-X605L|TB-8703F|TB-8703X|TB-8703N|TB-8704N|TB-8704F|TB-8704X|TB-8704V|TB-7304F|TB-7304I|TB-7304X|Tab2A7-10F|Tab2A7-20F|TB2-X30L|YT3-X50L|YT3-X50F|YT3-X50M|YT-X705F|YT-X703F|YT-X703L|YT-X705L|YT-X705X|TB2-X30F|TB2-X30L|TB2-X30M|A2107A-F|A2107A-H|TB3-730F|TB3-730M|TB3-730X|TB-7504F|TB-7504X",
        "DellTablet": "Venue 11|Venue 8|Venue 7|Dell Streak 10|Dell Streak 7",
        "YarvikTablet": "Android.*\\b(TAB210|TAB211|TAB224|TAB250|TAB260|TAB264|TAB310|TAB360|TAB364|TAB410|TAB411|TAB420|TAB424|TAB450|TAB460|TAB461|TAB464|TAB465|TAB467|TAB468|TAB07-100|TAB07-101|TAB07-150|TAB07-151|TAB07-152|TAB07-200|TAB07-201-3G|TAB07-210|TAB07-211|TAB07-212|TAB07-214|TAB07-220|TAB07-400|TAB07-485|TAB08-150|TAB08-200|TAB08-201-3G|TAB08-201-30|TAB09-100|TAB09-211|TAB09-410|TAB10-150|TAB10-201|TAB10-211|TAB10-400|TAB10-410|TAB13-201|TAB274EUK|TAB275EUK|TAB374EUK|TAB462EUK|TAB474EUK|TAB9-200)\\b",
        "MedionTablet": "Android.*\\bOYO\\b|LIFE.*(P9212|P9514|P9516|S9512)|LIFETAB",
        "ArnovaTablet": "97G4|AN10G2|AN7bG3|AN7fG3|AN8G3|AN8cG3|AN7G3|AN9G3|AN7dG3|AN7dG3ST|AN7dG3ChildPad|AN10bG3|AN10bG3DT|AN9G2",
        "IntensoTablet": "INM8002KP|INM1010FP|INM805ND|Intenso Tab|TAB1004",
        "IRUTablet": "M702pro",
        "MegafonTablet": "MegaFon V9|\\bZTE V9\\b|Android.*\\bMT7A\\b",
        "EbodaTablet": "E-Boda (Supreme|Impresspeed|Izzycomm|Essential)",
        "AllViewTablet": "Allview.*(Viva|Alldro|City|Speed|All TV|Frenzy|Quasar|Shine|TX1|AX1|AX2)",
        "ArchosTablet": "\\b(101G9|80G9|A101IT)\\b|Qilive 97R|Archos5|\\bARCHOS (70|79|80|90|97|101|FAMILYPAD|)(b|c|)(G10| Cobalt| TITANIUM(HD|)| Xenon| Neon|XSK| 2| XS 2| PLATINUM| CARBON|GAMEPAD)\\b",
        "AinolTablet": "NOVO7|NOVO8|NOVO10|Novo7Aurora|Novo7Basic|NOVO7PALADIN|novo9-Spark",
        "NokiaLumiaTablet": "Lumia 2520",
        "SonyTablet": "Sony.*Tablet|Xperia Tablet|Sony Tablet S|SO-03E|SGPT12|SGPT13|SGPT114|SGPT121|SGPT122|SGPT123|SGPT111|SGPT112|SGPT113|SGPT131|SGPT132|SGPT133|SGPT211|SGPT212|SGPT213|SGP311|SGP312|SGP321|EBRD1101|EBRD1102|EBRD1201|SGP351|SGP341|SGP511|SGP512|SGP521|SGP541|SGP551|SGP621|SGP641|SGP612|SOT31|SGP771|SGP611|SGP612|SGP712",
        "PhilipsTablet": "\\b(PI2010|PI3000|PI3100|PI3105|PI3110|PI3205|PI3210|PI3900|PI4010|PI7000|PI7100)\\b",
        "CubeTablet": "Android.*(K8GT|U9GT|U10GT|U16GT|U17GT|U18GT|U19GT|U20GT|U23GT|U30GT)|CUBE U8GT",
        "CobyTablet": "MID1042|MID1045|MID1125|MID1126|MID7012|MID7014|MID7015|MID7034|MID7035|MID7036|MID7042|MID7048|MID7127|MID8042|MID8048|MID8127|MID9042|MID9740|MID9742|MID7022|MID7010",
        "MIDTablet": "M9701|M9000|M9100|M806|M1052|M806|T703|MID701|MID713|MID710|MID727|MID760|MID830|MID728|MID933|MID125|MID810|MID732|MID120|MID930|MID800|MID731|MID900|MID100|MID820|MID735|MID980|MID130|MID833|MID737|MID960|MID135|MID860|MID736|MID140|MID930|MID835|MID733|MID4X10",
        "MSITablet": "MSI \\b(Primo 73K|Primo 73L|Primo 81L|Primo 77|Primo 93|Primo 75|Primo 76|Primo 73|Primo 81|Primo 91|Primo 90|Enjoy 71|Enjoy 7|Enjoy 10)\\b",
        "SMiTTablet": "Android.*(\\bMID\\b|MID-560|MTV-T1200|MTV-PND531|MTV-P1101|MTV-PND530)",
        "RockChipTablet": "Android.*(RK2818|RK2808A|RK2918|RK3066)|RK2738|RK2808A",
        "FlyTablet": "IQ310|Fly Vision",
        "bqTablet": "Android.*(bq)?.*\\b(Elcano|Curie|Edison|Maxwell|Kepler|Pascal|Tesla|Hypatia|Platon|Newton|Livingstone|Cervantes|Avant|Aquaris ([E|M]10|M8))\\b|Maxwell.*Lite|Maxwell.*Plus",
        "HuaweiTablet": "MediaPad|MediaPad 7 Youth|IDEOS S7|S7-201c|S7-202u|S7-101|S7-103|S7-104|S7-105|S7-106|S7-201|S7-Slim|M2-A01L|BAH-L09|BAH-W09|AGS-L09|CMR-AL19",
        "NecTablet": "\\bN-06D|\\bN-08D",
        "PantechTablet": "Pantech.*P4100",
        "BronchoTablet": "Broncho.*(N701|N708|N802|a710)",
        "VersusTablet": "TOUCHPAD.*[78910]|\\bTOUCHTAB\\b",
        "ZyncTablet": "z1000|Z99 2G|z930|z990|z909|Z919|z900",
        "PositivoTablet": "TB07STA|TB10STA|TB07FTA|TB10FTA",
        "NabiTablet": "Android.*\\bNabi",
        "KoboTablet": "Kobo Touch|\\bK080\\b|\\bVox\\b Build|\\bArc\\b Build",
        "DanewTablet": "DSlide.*\\b(700|701R|702|703R|704|802|970|971|972|973|974|1010|1012)\\b",
        "TexetTablet": "NaviPad|TB-772A|TM-7045|TM-7055|TM-9750|TM-7016|TM-7024|TM-7026|TM-7041|TM-7043|TM-7047|TM-8041|TM-9741|TM-9747|TM-9748|TM-9751|TM-7022|TM-7021|TM-7020|TM-7011|TM-7010|TM-7023|TM-7025|TM-7037W|TM-7038W|TM-7027W|TM-9720|TM-9725|TM-9737W|TM-1020|TM-9738W|TM-9740|TM-9743W|TB-807A|TB-771A|TB-727A|TB-725A|TB-719A|TB-823A|TB-805A|TB-723A|TB-715A|TB-707A|TB-705A|TB-709A|TB-711A|TB-890HD|TB-880HD|TB-790HD|TB-780HD|TB-770HD|TB-721HD|TB-710HD|TB-434HD|TB-860HD|TB-840HD|TB-760HD|TB-750HD|TB-740HD|TB-730HD|TB-722HD|TB-720HD|TB-700HD|TB-500HD|TB-470HD|TB-431HD|TB-430HD|TB-506|TB-504|TB-446|TB-436|TB-416|TB-146SE|TB-126SE",
        "PlaystationTablet": "Playstation.*(Portable|Vita)",
        "TrekstorTablet": "ST10416-1|VT10416-1|ST70408-1|ST702xx-1|ST702xx-2|ST80208|ST97216|ST70104-2|VT10416-2|ST10216-2A|SurfTab",
        "PyleAudioTablet": "\\b(PTBL10CEU|PTBL10C|PTBL72BC|PTBL72BCEU|PTBL7CEU|PTBL7C|PTBL92BC|PTBL92BCEU|PTBL9CEU|PTBL9CUK|PTBL9C)\\b",
        "AdvanTablet": "Android.* \\b(E3A|T3X|T5C|T5B|T3E|T3C|T3B|T1J|T1F|T2A|T1H|T1i|E1C|T1-E|T5-A|T4|E1-B|T2Ci|T1-B|T1-D|O1-A|E1-A|T1-A|T3A|T4i)\\b ",
        "DanyTechTablet": "Genius Tab G3|Genius Tab S2|Genius Tab Q3|Genius Tab G4|Genius Tab Q4|Genius Tab G-II|Genius TAB GII|Genius TAB GIII|Genius Tab S1",
        "GalapadTablet": "Android.*\\bG1\\b(?!\\))",
        "MicromaxTablet": "Funbook|Micromax.*\\b(P250|P560|P360|P362|P600|P300|P350|P500|P275)\\b",
        "KarbonnTablet": "Android.*\\b(A39|A37|A34|ST8|ST10|ST7|Smart Tab3|Smart Tab2)\\b",
        "AllFineTablet": "Fine7 Genius|Fine7 Shine|Fine7 Air|Fine8 Style|Fine9 More|Fine10 Joy|Fine11 Wide",
        "PROSCANTablet": "\\b(PEM63|PLT1023G|PLT1041|PLT1044|PLT1044G|PLT1091|PLT4311|PLT4311PL|PLT4315|PLT7030|PLT7033|PLT7033D|PLT7035|PLT7035D|PLT7044K|PLT7045K|PLT7045KB|PLT7071KG|PLT7072|PLT7223G|PLT7225G|PLT7777G|PLT7810K|PLT7849G|PLT7851G|PLT7852G|PLT8015|PLT8031|PLT8034|PLT8036|PLT8080K|PLT8082|PLT8088|PLT8223G|PLT8234G|PLT8235G|PLT8816K|PLT9011|PLT9045K|PLT9233G|PLT9735|PLT9760G|PLT9770G)\\b",
        "YONESTablet": "BQ1078|BC1003|BC1077|RK9702|BC9730|BC9001|IT9001|BC7008|BC7010|BC708|BC728|BC7012|BC7030|BC7027|BC7026",
        "ChangJiaTablet": "TPC7102|TPC7103|TPC7105|TPC7106|TPC7107|TPC7201|TPC7203|TPC7205|TPC7210|TPC7708|TPC7709|TPC7712|TPC7110|TPC8101|TPC8103|TPC8105|TPC8106|TPC8203|TPC8205|TPC8503|TPC9106|TPC9701|TPC97101|TPC97103|TPC97105|TPC97106|TPC97111|TPC97113|TPC97203|TPC97603|TPC97809|TPC97205|TPC10101|TPC10103|TPC10106|TPC10111|TPC10203|TPC10205|TPC10503",
        "GUTablet": "TX-A1301|TX-M9002|Q702|kf026",
        "PointOfViewTablet": "TAB-P506|TAB-navi-7-3G-M|TAB-P517|TAB-P-527|TAB-P701|TAB-P703|TAB-P721|TAB-P731N|TAB-P741|TAB-P825|TAB-P905|TAB-P925|TAB-PR945|TAB-PL1015|TAB-P1025|TAB-PI1045|TAB-P1325|TAB-PROTAB[0-9]+|TAB-PROTAB25|TAB-PROTAB26|TAB-PROTAB27|TAB-PROTAB26XL|TAB-PROTAB2-IPS9|TAB-PROTAB30-IPS9|TAB-PROTAB25XXL|TAB-PROTAB26-IPS10|TAB-PROTAB30-IPS10",
        "OvermaxTablet": "OV-(SteelCore|NewBase|Basecore|Baseone|Exellen|Quattor|EduTab|Solution|ACTION|BasicTab|TeddyTab|MagicTab|Stream|TB-08|TB-09)|Qualcore 1027",
        "HCLTablet": "HCL.*Tablet|Connect-3G-2.0|Connect-2G-2.0|ME Tablet U1|ME Tablet U2|ME Tablet G1|ME Tablet X1|ME Tablet Y2|ME Tablet Sync",
        "DPSTablet": "DPS Dream 9|DPS Dual 7",
        "VistureTablet": "V97 HD|i75 3G|Visture V4( HD)?|Visture V5( HD)?|Visture V10",
        "CrestaTablet": "CTP(-)?810|CTP(-)?818|CTP(-)?828|CTP(-)?838|CTP(-)?888|CTP(-)?978|CTP(-)?980|CTP(-)?987|CTP(-)?988|CTP(-)?989",
        "MediatekTablet": "\\bMT8125|MT8389|MT8135|MT8377\\b",
        "ConcordeTablet": "Concorde([ ]+)?Tab|ConCorde ReadMan",
        "GoCleverTablet": "GOCLEVER TAB|A7GOCLEVER|M1042|M7841|M742|R1042BK|R1041|TAB A975|TAB A7842|TAB A741|TAB A741L|TAB M723G|TAB M721|TAB A1021|TAB I921|TAB R721|TAB I720|TAB T76|TAB R70|TAB R76.2|TAB R106|TAB R83.2|TAB M813G|TAB I721|GCTA722|TAB I70|TAB I71|TAB S73|TAB R73|TAB R74|TAB R93|TAB R75|TAB R76.1|TAB A73|TAB A93|TAB A93.2|TAB T72|TAB R83|TAB R974|TAB R973|TAB A101|TAB A103|TAB A104|TAB A104.2|R105BK|M713G|A972BK|TAB A971|TAB R974.2|TAB R104|TAB R83.3|TAB A1042",
        "ModecomTablet": "FreeTAB 9000|FreeTAB 7.4|FreeTAB 7004|FreeTAB 7800|FreeTAB 2096|FreeTAB 7.5|FreeTAB 1014|FreeTAB 1001 |FreeTAB 8001|FreeTAB 9706|FreeTAB 9702|FreeTAB 7003|FreeTAB 7002|FreeTAB 1002|FreeTAB 7801|FreeTAB 1331|FreeTAB 1004|FreeTAB 8002|FreeTAB 8014|FreeTAB 9704|FreeTAB 1003",
        "VoninoTablet": "\\b(Argus[ _]?S|Diamond[ _]?79HD|Emerald[ _]?78E|Luna[ _]?70C|Onyx[ _]?S|Onyx[ _]?Z|Orin[ _]?HD|Orin[ _]?S|Otis[ _]?S|SpeedStar[ _]?S|Magnet[ _]?M9|Primus[ _]?94[ _]?3G|Primus[ _]?94HD|Primus[ _]?QS|Android.*\\bQ8\\b|Sirius[ _]?EVO[ _]?QS|Sirius[ _]?QS|Spirit[ _]?S)\\b",
        "ECSTablet": "V07OT2|TM105A|S10OT1|TR10CS1",
        "StorexTablet": "eZee[_']?(Tab|Go)[0-9]+|TabLC7|Looney Tunes Tab",
        "VodafoneTablet": "SmartTab([ ]+)?[0-9]+|SmartTabII10|SmartTabII7|VF-1497|VFD 1400",
        "EssentielBTablet": "Smart[ ']?TAB[ ]+?[0-9]+|Family[ ']?TAB2",
        "RossMoorTablet": "RM-790|RM-997|RMD-878G|RMD-974R|RMT-705A|RMT-701|RME-601|RMT-501|RMT-711",
        "iMobileTablet": "i-mobile i-note",
        "TolinoTablet": "tolino tab [0-9.]+|tolino shine",
        "AudioSonicTablet": "\\bC-22Q|T7-QC|T-17B|T-17P\\b",
        "AMPETablet": "Android.* A78 ",
        "SkkTablet": "Android.* (SKYPAD|PHOENIX|CYCLOPS)",
        "TecnoTablet": "TECNO P9|TECNO DP8D",
        "JXDTablet": "Android.* \\b(F3000|A3300|JXD5000|JXD3000|JXD2000|JXD300B|JXD300|S5800|S7800|S602b|S5110b|S7300|S5300|S602|S603|S5100|S5110|S601|S7100a|P3000F|P3000s|P101|P200s|P1000m|P200m|P9100|P1000s|S6600b|S908|P1000|P300|S18|S6600|S9100)\\b",
        "iJoyTablet": "Tablet (Spirit 7|Essentia|Galatea|Fusion|Onix 7|Landa|Titan|Scooby|Deox|Stella|Themis|Argon|Unique 7|Sygnus|Hexen|Finity 7|Cream|Cream X2|Jade|Neon 7|Neron 7|Kandy|Scape|Saphyr 7|Rebel|Biox|Rebel|Rebel 8GB|Myst|Draco 7|Myst|Tab7-004|Myst|Tadeo Jones|Tablet Boing|Arrow|Draco Dual Cam|Aurix|Mint|Amity|Revolution|Finity 9|Neon 9|T9w|Amity 4GB Dual Cam|Stone 4GB|Stone 8GB|Andromeda|Silken|X2|Andromeda II|Halley|Flame|Saphyr 9,7|Touch 8|Planet|Triton|Unique 10|Hexen 10|Memphis 4GB|Memphis 8GB|Onix 10)",
        "FX2Tablet": "FX2 PAD7|FX2 PAD10",
        "XoroTablet": "KidsPAD 701|PAD[ ]?712|PAD[ ]?714|PAD[ ]?716|PAD[ ]?717|PAD[ ]?718|PAD[ ]?720|PAD[ ]?721|PAD[ ]?722|PAD[ ]?790|PAD[ ]?792|PAD[ ]?900|PAD[ ]?9715D|PAD[ ]?9716DR|PAD[ ]?9718DR|PAD[ ]?9719QR|PAD[ ]?9720QR|TelePAD1030|Telepad1032|TelePAD730|TelePAD731|TelePAD732|TelePAD735Q|TelePAD830|TelePAD9730|TelePAD795|MegaPAD 1331|MegaPAD 1851|MegaPAD 2151",
        "ViewsonicTablet": "ViewPad 10pi|ViewPad 10e|ViewPad 10s|ViewPad E72|ViewPad7|ViewPad E100|ViewPad 7e|ViewSonic VB733|VB100a",
        "VerizonTablet": "QTAQZ3|QTAIR7|QTAQTZ3|QTASUN1|QTASUN2|QTAXIA1",
        "OdysTablet": "LOOX|XENO10|ODYS[ -](Space|EVO|Xpress|NOON)|\\bXELIO\\b|Xelio10Pro|XELIO7PHONETAB|XELIO10EXTREME|XELIOPT2|NEO_QUAD10",
        "CaptivaTablet": "CAPTIVA PAD",
        "IconbitTablet": "NetTAB|NT-3702|NT-3702S|NT-3702S|NT-3603P|NT-3603P|NT-0704S|NT-0704S|NT-3805C|NT-3805C|NT-0806C|NT-0806C|NT-0909T|NT-0909T|NT-0907S|NT-0907S|NT-0902S|NT-0902S",
        "TeclastTablet": "T98 4G|\\bP80\\b|\\bX90HD\\b|X98 Air|X98 Air 3G|\\bX89\\b|P80 3G|\\bX80h\\b|P98 Air|\\bX89HD\\b|P98 3G|\\bP90HD\\b|P89 3G|X98 3G|\\bP70h\\b|P79HD 3G|G18d 3G|\\bP79HD\\b|\\bP89s\\b|\\bA88\\b|\\bP10HD\\b|\\bP19HD\\b|G18 3G|\\bP78HD\\b|\\bA78\\b|\\bP75\\b|G17s 3G|G17h 3G|\\bP85t\\b|\\bP90\\b|\\bP11\\b|\\bP98t\\b|\\bP98HD\\b|\\bG18d\\b|\\bP85s\\b|\\bP11HD\\b|\\bP88s\\b|\\bA80HD\\b|\\bA80se\\b|\\bA10h\\b|\\bP89\\b|\\bP78s\\b|\\bG18\\b|\\bP85\\b|\\bA70h\\b|\\bA70\\b|\\bG17\\b|\\bP18\\b|\\bA80s\\b|\\bA11s\\b|\\bP88HD\\b|\\bA80h\\b|\\bP76s\\b|\\bP76h\\b|\\bP98\\b|\\bA10HD\\b|\\bP78\\b|\\bP88\\b|\\bA11\\b|\\bA10t\\b|\\bP76a\\b|\\bP76t\\b|\\bP76e\\b|\\bP85HD\\b|\\bP85a\\b|\\bP86\\b|\\bP75HD\\b|\\bP76v\\b|\\bA12\\b|\\bP75a\\b|\\bA15\\b|\\bP76Ti\\b|\\bP81HD\\b|\\bA10\\b|\\bT760VE\\b|\\bT720HD\\b|\\bP76\\b|\\bP73\\b|\\bP71\\b|\\bP72\\b|\\bT720SE\\b|\\bC520Ti\\b|\\bT760\\b|\\bT720VE\\b|T720-3GE|T720-WiFi",
        "OndaTablet": "\\b(V975i|Vi30|VX530|V701|Vi60|V701s|Vi50|V801s|V719|Vx610w|VX610W|V819i|Vi10|VX580W|Vi10|V711s|V813|V811|V820w|V820|Vi20|V711|VI30W|V712|V891w|V972|V819w|V820w|Vi60|V820w|V711|V813s|V801|V819|V975s|V801|V819|V819|V818|V811|V712|V975m|V101w|V961w|V812|V818|V971|V971s|V919|V989|V116w|V102w|V973|Vi40)\\b[\\s]+|V10 \\b4G\\b",
        "JaytechTablet": "TPC-PA762",
        "BlaupunktTablet": "Endeavour 800NG|Endeavour 1010",
        "DigmaTablet": "\\b(iDx10|iDx9|iDx8|iDx7|iDxD7|iDxD8|iDsQ8|iDsQ7|iDsQ8|iDsD10|iDnD7|3TS804H|iDsQ11|iDj7|iDs10)\\b",
        "EvolioTablet": "ARIA_Mini_wifi|Aria[ _]Mini|Evolio X10|Evolio X7|Evolio X8|\\bEvotab\\b|\\bNeura\\b",
        "LavaTablet": "QPAD E704|\\bIvoryS\\b|E-TAB IVORY|\\bE-TAB\\b",
        "AocTablet": "MW0811|MW0812|MW0922|MTK8382|MW1031|MW0831|MW0821|MW0931|MW0712",
        "MpmanTablet": "MP11 OCTA|MP10 OCTA|MPQC1114|MPQC1004|MPQC994|MPQC974|MPQC973|MPQC804|MPQC784|MPQC780|\\bMPG7\\b|MPDCG75|MPDCG71|MPDC1006|MP101DC|MPDC9000|MPDC905|MPDC706HD|MPDC706|MPDC705|MPDC110|MPDC100|MPDC99|MPDC97|MPDC88|MPDC8|MPDC77|MP709|MID701|MID711|MID170|MPDC703|MPQC1010",
        "CelkonTablet": "CT695|CT888|CT[\\s]?910|CT7 Tab|CT9 Tab|CT3 Tab|CT2 Tab|CT1 Tab|C820|C720|\\bCT-1\\b",
        "WolderTablet": "miTab \\b(DIAMOND|SPACE|BROOKLYN|NEO|FLY|MANHATTAN|FUNK|EVOLUTION|SKY|GOCAR|IRON|GENIUS|POP|MINT|EPSILON|BROADWAY|JUMP|HOP|LEGEND|NEW AGE|LINE|ADVANCE|FEEL|FOLLOW|LIKE|LINK|LIVE|THINK|FREEDOM|CHICAGO|CLEVELAND|BALTIMORE-GH|IOWA|BOSTON|SEATTLE|PHOENIX|DALLAS|IN 101|MasterChef)\\b",
        "MediacomTablet": "M-MPI10C3G|M-SP10EG|M-SP10EGP|M-SP10HXAH|M-SP7HXAH|M-SP10HXBH|M-SP8HXAH|M-SP8MXA",
        "MiTablet": "\\bMI PAD\\b|\\bHM NOTE 1W\\b",
        "NibiruTablet": "Nibiru M1|Nibiru Jupiter One",
        "NexoTablet": "NEXO NOVA|NEXO 10|NEXO AVIO|NEXO FREE|NEXO GO|NEXO EVO|NEXO 3G|NEXO SMART|NEXO KIDDO|NEXO MOBI",
        "LeaderTablet": "TBLT10Q|TBLT10I|TBL-10WDKB|TBL-10WDKBO2013|TBL-W230V2|TBL-W450|TBL-W500|SV572|TBLT7I|TBA-AC7-8G|TBLT79|TBL-8W16|TBL-10W32|TBL-10WKB|TBL-W100",
        "UbislateTablet": "UbiSlate[\\s]?7C",
        "PocketBookTablet": "Pocketbook",
        "KocasoTablet": "\\b(TB-1207)\\b",
        "HisenseTablet": "\\b(F5281|E2371)\\b",
        "Hudl": "Hudl HT7S3|Hudl 2",
        "TelstraTablet": "T-Hub2",
        "GenericTablet": "Android.*\\b97D\\b|Tablet(?!.*PC)|BNTV250A|MID-WCDMA|LogicPD Zoom2|\\bA7EB\\b|CatNova8|A1_07|CT704|CT1002|\\bM721\\b|rk30sdk|\\bEVOTAB\\b|M758A|ET904|ALUMIUM10|Smartfren Tab|Endeavour 1010|Tablet-PC-4|Tagi Tab|\\bM6pro\\b|CT1020W|arc 10HD|\\bTP750\\b|\\bQTAQZ3\\b|WVT101|TM1088|KT107"
    },
    "oss": {
        "AndroidOS": "Android",
        "BlackBerryOS": "blackberry|\\bBB10\\b|rim tablet os",
        "PalmOS": "PalmOS|avantgo|blazer|elaine|hiptop|palm|plucker|xiino",
        "SymbianOS": "Symbian|SymbOS|Series60|Series40|SYB-[0-9]+|\\bS60\\b",
        "WindowsMobileOS": "Windows CE.*(PPC|Smartphone|Mobile|[0-9]{3}x[0-9]{3})|Windows Mobile|Windows Phone [0-9.]+|WCE;",
        "WindowsPhoneOS": "Windows Phone 10.0|Windows Phone 8.1|Windows Phone 8.0|Windows Phone OS|XBLWP7|ZuneWP7|Windows NT 6.[23]; ARM;",
        "iOS": "\\biPhone.*Mobile|\\biPod|\\biPad|AppleCoreMedia",
        "iPadOS": "CPU OS 13",
        "MeeGoOS": "MeeGo",
        "MaemoOS": "Maemo",
        "JavaOS": "J2ME\/|\\bMIDP\\b|\\bCLDC\\b",
        "webOS": "webOS|hpwOS",
        "badaOS": "\\bBada\\b",
        "BREWOS": "BREW"
    },
    "uas": {
        "Chrome": "\\bCrMo\\b|CriOS|Android.*Chrome\/[.0-9]* (Mobile)?",
        "Dolfin": "\\bDolfin\\b",
        "Opera": "Opera.*Mini|Opera.*Mobi|Android.*Opera|Mobile.*OPR\/[0-9.]+$|Coast\/[0-9.]+",
        "Skyfire": "Skyfire",
        "Edge": "Mobile Safari\/[.0-9]* Edge",
        "IE": "IEMobile|MSIEMobile",
        "Firefox": "fennec|firefox.*maemo|(Mobile|Tablet).*Firefox|Firefox.*Mobile|FxiOS",
        "Bolt": "bolt",
        "TeaShark": "teashark",
        "Blazer": "Blazer",
        "Safari": "Version.*Mobile.*Safari|Safari.*Mobile|MobileSafari",
        "WeChat": "\\bMicroMessenger\\b",
        "UCBrowser": "UC.*Browser|UCWEB",
        "baiduboxapp": "baiduboxapp",
        "baidubrowser": "baidubrowser",
        "DiigoBrowser": "DiigoBrowser",
        "Mercury": "\\bMercury\\b",
        "ObigoBrowser": "Obigo",
        "NetFront": "NF-Browser",
        "GenericBrowser": "NokiaBrowser|OviBrowser|OneBrowser|TwonkyBeamBrowser|SEMC.*Browser|FlyFlow|Minimo|NetFront|Novarra-Vision|MQQBrowser|MicroMessenger",
        "PaleMoon": "Android.*PaleMoon|Mobile.*PaleMoon"
    },
    "props": {
        "Mobile": "Mobile\/[VER]",
        "Build": "Build\/[VER]",
        "Version": "Version\/[VER]",
        "VendorID": "VendorID\/[VER]",
        "iPad": "iPad.*CPU[a-z ]+[VER]",
        "iPhone": "iPhone.*CPU[a-z ]+[VER]",
        "iPod": "iPod.*CPU[a-z ]+[VER]",
        "Kindle": "Kindle\/[VER]",
        "Chrome": [
            "Chrome\/[VER]",
            "CriOS\/[VER]",
            "CrMo\/[VER]"
        ],
        "Coast": [
            "Coast\/[VER]"
        ],
        "Dolfin": "Dolfin\/[VER]",
        "Firefox": [
            "Firefox\/[VER]",
            "FxiOS\/[VER]"
        ],
        "Fennec": "Fennec\/[VER]",
        "Edge": "Edge\/[VER]",
        "IE": [
            "IEMobile\/[VER];",
            "IEMobile [VER]",
            "MSIE [VER];",
            "Trident\/[0-9.]+;.*rv:[VER]"
        ],
        "NetFront": "NetFront\/[VER]",
        "NokiaBrowser": "NokiaBrowser\/[VER]",
        "Opera": [
            " OPR\/[VER]",
            "Opera Mini\/[VER]",
            "Version\/[VER]"
        ],
        "Opera Mini": "Opera Mini\/[VER]",
        "Opera Mobi": "Version\/[VER]",
        "UCBrowser": [
            "UCWEB[VER]",
            "UC.*Browser\/[VER]"
        ],
        "MQQBrowser": "MQQBrowser\/[VER]",
        "MicroMessenger": "MicroMessenger\/[VER]",
        "baiduboxapp": "baiduboxapp\/[VER]",
        "baidubrowser": "baidubrowser\/[VER]",
        "SamsungBrowser": "SamsungBrowser\/[VER]",
        "Iron": "Iron\/[VER]",
        "Safari": [
            "Version\/[VER]",
            "Safari\/[VER]"
        ],
        "Skyfire": "Skyfire\/[VER]",
        "Tizen": "Tizen\/[VER]",
        "Webkit": "webkit[ \/][VER]",
        "PaleMoon": "PaleMoon\/[VER]",
        "Gecko": "Gecko\/[VER]",
        "Trident": "Trident\/[VER]",
        "Presto": "Presto\/[VER]",
        "Goanna": "Goanna\/[VER]",
        "iOS": " \\bi?OS\\b [VER][ ;]{1}",
        "Android": "Android [VER]",
        "BlackBerry": [
            "BlackBerry[\\w]+\/[VER]",
            "BlackBerry.*Version\/[VER]",
            "Version\/[VER]"
        ],
        "BREW": "BREW [VER]",
        "Java": "Java\/[VER]",
        "Windows Phone OS": [
            "Windows Phone OS [VER]",
            "Windows Phone [VER]"
        ],
        "Windows Phone": "Windows Phone [VER]",
        "Windows CE": "Windows CE\/[VER]",
        "Windows NT": "Windows NT [VER]",
        "Symbian": [
            "SymbianOS\/[VER]",
            "Symbian\/[VER]"
        ],
        "webOS": [
            "webOS\/[VER]",
            "hpwOS\/[VER];"
        ]
    },
    "utils": {
        "Bot": "Googlebot|facebookexternalhit|Google-AMPHTML|s~amp-validator|AdsBot-Google|Google Keyword Suggestion|Facebot|YandexBot|YandexMobileBot|bingbot|ia_archiver|AhrefsBot|Ezooms|GSLFbot|WBSearchBot|Twitterbot|TweetmemeBot|Twikle|PaperLiBot|Wotbox|UnwindFetchor|Exabot|MJ12bot|YandexImages|TurnitinBot|Pingdom|contentkingapp",
        "MobileBot": "Googlebot-Mobile|AdsBot-Google-Mobile|YahooSeeker\/M1A1-R2D2",
        "DesktopMode": "WPDesktop",
        "TV": "SonyDTV|HbbTV",
        "WebKit": "(webkit)[ \/]([\\w.]+)",
        "Console": "\\b(Nintendo|Nintendo WiiU|Nintendo 3DS|Nintendo Switch|PLAYSTATION|Xbox)\\b",
        "Watch": "SM-V700"
    }
};

    // following patterns come from http://detectmobilebrowsers.com/
    impl.detectMobileBrowsers = {
        fullPattern: /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i,
        shortPattern: /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i,
        tabletPattern: /android|ipad|playbook|silk/i
    };

    var hasOwnProp = Object.prototype.hasOwnProperty,
        isArray;

    impl.FALLBACK_PHONE = 'UnknownPhone';
    impl.FALLBACK_TABLET = 'UnknownTablet';
    impl.FALLBACK_MOBILE = 'UnknownMobile';

    isArray = ('isArray' in Array) ?
        Array.isArray : function (value) { return Object.prototype.toString.call(value) === '[object Array]'; };

    function equalIC(a, b) {
        return a != null && b != null && a.toLowerCase() === b.toLowerCase();
    }

    function containsIC(array, value) {
        var valueLC, i, len = array.length;
        if (!len || !value) {
            return false;
        }
        valueLC = value.toLowerCase();
        for (i = 0; i < len; ++i) {
            if (valueLC === array[i].toLowerCase()) {
                return true;
            }
        }
        return false;
    }

    function convertPropsToRegExp(object) {
        for (var key in object) {
            if (hasOwnProp.call(object, key)) {
                object[key] = new RegExp(object[key], 'i');
            }
        }
    }

    function prepareUserAgent(userAgent) {
        return (userAgent || '').substr(0, 500); // mitigate vulnerable to ReDoS
    }

    (function init() {
        var key, values, value, i, len, verPos, mobileDetectRules = impl.mobileDetectRules;
        for (key in mobileDetectRules.props) {
            if (hasOwnProp.call(mobileDetectRules.props, key)) {
                values = mobileDetectRules.props[key];
                if (!isArray(values)) {
                    values = [values];
                }
                len = values.length;
                for (i = 0; i < len; ++i) {
                    value = values[i];
                    verPos = value.indexOf('[VER]');
                    if (verPos >= 0) {
                        value = value.substring(0, verPos) + '([\\w._\\+]+)' + value.substring(verPos + 5);
                    }
                    values[i] = new RegExp(value, 'i');
                }
                mobileDetectRules.props[key] = values;
            }
        }
        convertPropsToRegExp(mobileDetectRules.oss);
        convertPropsToRegExp(mobileDetectRules.phones);
        convertPropsToRegExp(mobileDetectRules.tablets);
        convertPropsToRegExp(mobileDetectRules.uas);
        convertPropsToRegExp(mobileDetectRules.utils);

        // copy some patterns to oss0 which are tested first (see issue#15)
        mobileDetectRules.oss0 = {
            WindowsPhoneOS: mobileDetectRules.oss.WindowsPhoneOS,
            WindowsMobileOS: mobileDetectRules.oss.WindowsMobileOS
        };
    }());

    /**
     * Test userAgent string against a set of rules and find the first matched key.
     * @param {Object} rules (key is String, value is RegExp)
     * @param {String} userAgent the navigator.userAgent (or HTTP-Header 'User-Agent').
     * @returns {String|null} the matched key if found, otherwise <tt>null</tt>
     * @private
     */
    impl.findMatch = function(rules, userAgent) {
        for (var key in rules) {
            if (hasOwnProp.call(rules, key)) {
                if (rules[key].test(userAgent)) {
                    return key;
                }
            }
        }
        return null;
    };

    /**
     * Test userAgent string against a set of rules and return an array of matched keys.
     * @param {Object} rules (key is String, value is RegExp)
     * @param {String} userAgent the navigator.userAgent (or HTTP-Header 'User-Agent').
     * @returns {Array} an array of matched keys, may be empty when there is no match, but not <tt>null</tt>
     * @private
     */
    impl.findMatches = function(rules, userAgent) {
        var result = [];
        for (var key in rules) {
            if (hasOwnProp.call(rules, key)) {
                if (rules[key].test(userAgent)) {
                    result.push(key);
                }
            }
        }
        return result;
    };

    /**
     * Check the version of the given property in the User-Agent.
     *
     * @param {String} propertyName
     * @param {String} userAgent
     * @return {String} version or <tt>null</tt> if version not found
     * @private
     */
    impl.getVersionStr = function (propertyName, userAgent) {
        var props = impl.mobileDetectRules.props, patterns, i, len, match;
        if (hasOwnProp.call(props, propertyName)) {
            patterns = props[propertyName];
            len = patterns.length;
            for (i = 0; i < len; ++i) {
                match = patterns[i].exec(userAgent);
                if (match !== null) {
                    return match[1];
                }
            }
        }
        return null;
    };

    /**
     * Check the version of the given property in the User-Agent.
     * Will return a float number. (eg. 2_0 will return 2.0, 4.3.1 will return 4.31)
     *
     * @param {String} propertyName
     * @param {String} userAgent
     * @return {Number} version or <tt>NaN</tt> if version not found
     * @private
     */
    impl.getVersion = function (propertyName, userAgent) {
        var version = impl.getVersionStr(propertyName, userAgent);
        return version ? impl.prepareVersionNo(version) : NaN;
    };

    /**
     * Prepare the version number.
     *
     * @param {String} version
     * @return {Number} the version number as a floating number
     * @private
     */
    impl.prepareVersionNo = function (version) {
        var numbers;

        numbers = version.split(/[a-z._ \/\-]/i);
        if (numbers.length === 1) {
            version = numbers[0];
        }
        if (numbers.length > 1) {
            version = numbers[0] + '.';
            numbers.shift();
            version += numbers.join('');
        }
        return Number(version);
    };

    impl.isMobileFallback = function (userAgent) {
        return impl.detectMobileBrowsers.fullPattern.test(userAgent) ||
            impl.detectMobileBrowsers.shortPattern.test(userAgent.substr(0,4));
    };

    impl.isTabletFallback = function (userAgent) {
        return impl.detectMobileBrowsers.tabletPattern.test(userAgent);
    };

    impl.prepareDetectionCache = function (cache, userAgent, maxPhoneWidth) {
        if (cache.mobile !== undefined) {
            return;
        }
        var phone, tablet, phoneSized;

        // first check for stronger tablet rules, then phone (see issue#5)
        tablet = impl.findMatch(impl.mobileDetectRules.tablets, userAgent);
        if (tablet) {
            cache.mobile = cache.tablet = tablet;
            cache.phone = null;
            return; // unambiguously identified as tablet
        }

        phone = impl.findMatch(impl.mobileDetectRules.phones, userAgent);
        if (phone) {
            cache.mobile = cache.phone = phone;
            cache.tablet = null;
            return; // unambiguously identified as phone
        }

        // our rules haven't found a match -> try more general fallback rules
        if (impl.isMobileFallback(userAgent)) {
            phoneSized = MobileDetect.isPhoneSized(maxPhoneWidth);
            if (phoneSized === undefined) {
                cache.mobile = impl.FALLBACK_MOBILE;
                cache.tablet = cache.phone = null;
            } else if (phoneSized) {
                cache.mobile = cache.phone = impl.FALLBACK_PHONE;
                cache.tablet = null;
            } else {
                cache.mobile = cache.tablet = impl.FALLBACK_TABLET;
                cache.phone = null;
            }
        } else if (impl.isTabletFallback(userAgent)) {
            cache.mobile = cache.tablet = impl.FALLBACK_TABLET;
            cache.phone = null;
        } else {
            // not mobile at all!
            cache.mobile = cache.tablet = cache.phone = null;
        }
    };

    // t is a reference to a MobileDetect instance
    impl.mobileGrade = function (t) {
        // impl note:
        // To keep in sync w/ Mobile_Detect.php easily, the following code is tightly aligned to the PHP version.
        // When changes are made in Mobile_Detect.php, copy this method and replace:
        //     $this-> / t.
        //     self::MOBILE_GRADE_(.) / '$1'
        //     , self::VERSION_TYPE_FLOAT / (nothing)
        //     isIOS() / os('iOS')
        //     [reg] / (nothing)   <-- jsdelivr complaining about unescaped unicode character U+00AE
        var $isMobile = t.mobile() !== null;

        if (
            // Apple iOS 3.2-5.1 - Tested on the original iPad (4.3 / 5.0), iPad 2 (4.3), iPad 3 (5.1), original iPhone (3.1), iPhone 3 (3.2), 3GS (4.3), 4 (4.3 / 5.0), and 4S (5.1)
            t.os('iOS') && t.version('iPad')>=4.3 ||
            t.os('iOS') && t.version('iPhone')>=3.1 ||
            t.os('iOS') && t.version('iPod')>=3.1 ||

            // Android 2.1-2.3 - Tested on the HTC Incredible (2.2), original Droid (2.2), HTC Aria (2.1), Google Nexus S (2.3). Functional on 1.5 & 1.6 but performance may be sluggish, tested on Google G1 (1.5)
            // Android 3.1 (Honeycomb)  - Tested on the Samsung Galaxy Tab 10.1 and Motorola XOOM
            // Android 4.0 (ICS)  - Tested on a Galaxy Nexus. Note: transition performance can be poor on upgraded devices
            // Android 4.1 (Jelly Bean)  - Tested on a Galaxy Nexus and Galaxy 7
            ( t.version('Android')>2.1 && t.is('Webkit') ) ||

            // Windows Phone 7-7.5 - Tested on the HTC Surround (7.0) HTC Trophy (7.5), LG-E900 (7.5), Nokia Lumia 800
            t.version('Windows Phone OS')>=7.0 ||

            // Blackberry 7 - Tested on BlackBerry Torch 9810
            // Blackberry 6.0 - Tested on the Torch 9800 and Style 9670
            t.is('BlackBerry') && t.version('BlackBerry')>=6.0 ||
            // Blackberry Playbook (1.0-2.0) - Tested on PlayBook
            t.match('Playbook.*Tablet') ||

            // Palm WebOS (1.4-2.0) - Tested on the Palm Pixi (1.4), Pre (1.4), Pre 2 (2.0)
            ( t.version('webOS')>=1.4 && t.match('Palm|Pre|Pixi') ) ||
            // Palm WebOS 3.0  - Tested on HP TouchPad
            t.match('hp.*TouchPad') ||

            // Firefox Mobile (12 Beta) - Tested on Android 2.3 device
            ( t.is('Firefox') && t.version('Firefox')>=12 ) ||

            // Chrome for Android - Tested on Android 4.0, 4.1 device
            ( t.is('Chrome') && t.is('AndroidOS') && t.version('Android')>=4.0 ) ||

            // Skyfire 4.1 - Tested on Android 2.3 device
            ( t.is('Skyfire') && t.version('Skyfire')>=4.1 && t.is('AndroidOS') && t.version('Android')>=2.3 ) ||

            // Opera Mobile 11.5-12: Tested on Android 2.3
            ( t.is('Opera') && t.version('Opera Mobi')>11 && t.is('AndroidOS') ) ||

            // Meego 1.2 - Tested on Nokia 950 and N9
            t.is('MeeGoOS') ||

            // Tizen (pre-release) - Tested on early hardware
            t.is('Tizen') ||

            // Samsung Bada 2.0 - Tested on a Samsung Wave 3, Dolphin browser
            // @todo: more tests here!
            t.is('Dolfin') && t.version('Bada')>=2.0 ||

            // UC Browser - Tested on Android 2.3 device
            ( (t.is('UC Browser') || t.is('Dolfin')) && t.version('Android')>=2.3 ) ||

            // Kindle 3 and Fire  - Tested on the built-in WebKit browser for each
            ( t.match('Kindle Fire') ||
                t.is('Kindle') && t.version('Kindle')>=3.0 ) ||

            // Nook Color 1.4.1 - Tested on original Nook Color, not Nook Tablet
            t.is('AndroidOS') && t.is('NookTablet') ||

            // Chrome Desktop 11-21 - Tested on OS X 10.7 and Windows 7
            t.version('Chrome')>=11 && !$isMobile ||

            // Safari Desktop 4-5 - Tested on OS X 10.7 and Windows 7
            t.version('Safari')>=5.0 && !$isMobile ||

            // Firefox Desktop 4-13 - Tested on OS X 10.7 and Windows 7
            t.version('Firefox')>=4.0 && !$isMobile ||

            // Internet Explorer 7-9 - Tested on Windows XP, Vista and 7
            t.version('MSIE')>=7.0 && !$isMobile ||

            // Opera Desktop 10-12 - Tested on OS X 10.7 and Windows 7
            // @reference: http://my.opera.com/community/openweb/idopera/
            t.version('Opera')>=10 && !$isMobile

            ){
            return 'A';
        }

        if (
            t.os('iOS') && t.version('iPad')<4.3 ||
            t.os('iOS') && t.version('iPhone')<3.1 ||
            t.os('iOS') && t.version('iPod')<3.1 ||

            // Blackberry 5.0: Tested on the Storm 2 9550, Bold 9770
            t.is('Blackberry') && t.version('BlackBerry')>=5 && t.version('BlackBerry')<6 ||

            //Opera Mini (5.0-6.5) - Tested on iOS 3.2/4.3 and Android 2.3
            ( t.version('Opera Mini')>=5.0 && t.version('Opera Mini')<=6.5 &&
                (t.version('Android')>=2.3 || t.is('iOS')) ) ||

            // Nokia Symbian^3 - Tested on Nokia N8 (Symbian^3), C7 (Symbian^3), also works on N97 (Symbian^1)
            t.match('NokiaN8|NokiaC7|N97.*Series60|Symbian/3') ||

            // @todo: report this (tested on Nokia N71)
            t.version('Opera Mobi')>=11 && t.is('SymbianOS')
            ){
            return 'B';
        }

        if (
        // Blackberry 4.x - Tested on the Curve 8330
            t.version('BlackBerry')<5.0 ||
            // Windows Mobile - Tested on the HTC Leo (WinMo 5.2)
            t.match('MSIEMobile|Windows CE.*Mobile') || t.version('Windows Mobile')<=5.2

            ){
            return 'C';
        }

        //All older smartphone platforms and featurephones - Any device that doesn't support media queries
        //will receive the basic, C grade experience.
        return 'C';
    };

    impl.detectOS = function (ua) {
        return impl.findMatch(impl.mobileDetectRules.oss0, ua) ||
            impl.findMatch(impl.mobileDetectRules.oss, ua);
    };

    impl.getDeviceSmallerSide = function () {
        return window.screen.width < window.screen.height ?
            window.screen.width :
            window.screen.height;
    };

    /**
     * Constructor for MobileDetect object.
     * <br>
     * Such an object will keep a reference to the given user-agent string and cache most of the detect queries.<br>
     * <div style="background-color: #d9edf7; border: 1px solid #bce8f1; color: #3a87ad; padding: 14px; border-radius: 2px; margin-top: 20px">
     *     <strong>Find information how to download and install:</strong>
     *     <a href="https://github.com/hgoebl/mobile-detect.js/">github.com/hgoebl/mobile-detect.js/</a>
     * </div>
     *
     * @example <pre>
     *     var md = new MobileDetect(window.navigator.userAgent);
     *     if (md.mobile()) {
     *         location.href = (md.mobileGrade() === 'A') ? '/mobile/' : '/lynx/';
     *     }
     * </pre>
     *
     * @param {string} userAgent typically taken from window.navigator.userAgent or http_header['User-Agent']
     * @param {number} [maxPhoneWidth=600] <strong>only for browsers</strong> specify a value for the maximum
     *        width of smallest device side (in logical "CSS" pixels) until a device detected as mobile will be handled
     *        as phone.
     *        This is only used in cases where the device cannot be classified as phone or tablet.<br>
     *        See <a href="http://developer.android.com/guide/practices/screens_support.html">Declaring Tablet Layouts
     *        for Android</a>.<br>
     *        If you provide a value < 0, then this "fuzzy" check is disabled.
     * @constructor
     * @global
     */
    function MobileDetect(userAgent, maxPhoneWidth) {
        this.ua = prepareUserAgent(userAgent);
        this._cache = {};
        //600dp is typical 7" tablet minimum width
        this.maxPhoneWidth = maxPhoneWidth || 600;
    }

    MobileDetect.prototype = {
        constructor: MobileDetect,

        /**
         * Returns the detected phone or tablet type or <tt>null</tt> if it is not a mobile device.
         * <br>
         * For a list of possible return values see {@link MobileDetect#phone} and {@link MobileDetect#tablet}.<br>
         * <br>
         * If the device is not detected by the regular expressions from Mobile-Detect, a test is made against
         * the patterns of <a href="http://detectmobilebrowsers.com/">detectmobilebrowsers.com</a>. If this test
         * is positive, a value of <code>UnknownPhone</code>, <code>UnknownTablet</code> or
         * <code>UnknownMobile</code> is returned.<br>
         * When used in browser, the decision whether phone or tablet is made based on <code>screen.width/height</code>.<br>
         * <br>
         * When used server-side (node.js), there is no way to tell the difference between <code>UnknownTablet</code>
         * and <code>UnknownMobile</code>, so you will get <code>UnknownMobile</code> here.<br>
         * Be aware that since v1.0.0 in this special case you will get <code>UnknownMobile</code> only for:
         * {@link MobileDetect#mobile}, not for {@link MobileDetect#phone} and {@link MobileDetect#tablet}.
         * In versions before v1.0.0 all 3 methods returned <code>UnknownMobile</code> which was tedious to use.
         * <br>
         * In most cases you will use the return value just as a boolean.
         *
         * @returns {String} the key for the phone family or tablet family, e.g. "Nexus".
         * @function MobileDetect#mobile
         */
        mobile: function () {
            impl.prepareDetectionCache(this._cache, this.ua, this.maxPhoneWidth);
            return this._cache.mobile;
        },

        /**
         * Returns the detected phone type/family string or <tt>null</tt>.
         * <br>
         * The returned tablet (family or producer) is one of following keys:<br>
         * <br><tt>iPhone, BlackBerry, HTC, Nexus, Dell, Motorola, Samsung, LG, Sony, Asus,
         * NokiaLumia, Micromax, Palm, Vertu, Pantech, Fly, Wiko, iMobile, SimValley,
         * Wolfgang, Alcatel, Nintendo, Amoi, INQ, OnePlus, GenericPhone</tt><br>
         * <br>
         * If the device is not detected by the regular expressions from Mobile-Detect, a test is made against
         * the patterns of <a href="http://detectmobilebrowsers.com/">detectmobilebrowsers.com</a>. If this test
         * is positive, a value of <code>UnknownPhone</code> or <code>UnknownMobile</code> is returned.<br>
         * When used in browser, the decision whether phone or tablet is made based on <code>screen.width/height</code>.<br>
         * <br>
         * When used server-side (node.js), there is no way to tell the difference between <code>UnknownTablet</code>
         * and <code>UnknownMobile</code>, so you will get <code>null</code> here, while {@link MobileDetect#mobile}
         * will return <code>UnknownMobile</code>.<br>
         * Be aware that since v1.0.0 in this special case you will get <code>UnknownMobile</code> only for:
         * {@link MobileDetect#mobile}, not for {@link MobileDetect#phone} and {@link MobileDetect#tablet}.
         * In versions before v1.0.0 all 3 methods returned <code>UnknownMobile</code> which was tedious to use.
         * <br>
         * In most cases you will use the return value just as a boolean.
         *
         * @returns {String} the key of the phone family or producer, e.g. "iPhone"
         * @function MobileDetect#phone
         */
        phone: function () {
            impl.prepareDetectionCache(this._cache, this.ua, this.maxPhoneWidth);
            return this._cache.phone;
        },

        /**
         * Returns the detected tablet type/family string or <tt>null</tt>.
         * <br>
         * The returned tablet (family or producer) is one of following keys:<br>
         * <br><tt>iPad, NexusTablet, GoogleTablet, SamsungTablet, Kindle, SurfaceTablet,
         * HPTablet, AsusTablet, BlackBerryTablet, HTCtablet, MotorolaTablet, NookTablet,
         * AcerTablet, ToshibaTablet, LGTablet, FujitsuTablet, PrestigioTablet,
         * LenovoTablet, DellTablet, YarvikTablet, MedionTablet, ArnovaTablet,
         * IntensoTablet, IRUTablet, MegafonTablet, EbodaTablet, AllViewTablet,
         * ArchosTablet, AinolTablet, NokiaLumiaTablet, SonyTablet, PhilipsTablet,
         * CubeTablet, CobyTablet, MIDTablet, MSITablet, SMiTTablet, RockChipTablet,
         * FlyTablet, bqTablet, HuaweiTablet, NecTablet, PantechTablet, BronchoTablet,
         * VersusTablet, ZyncTablet, PositivoTablet, NabiTablet, KoboTablet, DanewTablet,
         * TexetTablet, PlaystationTablet, TrekstorTablet, PyleAudioTablet, AdvanTablet,
         * DanyTechTablet, GalapadTablet, MicromaxTablet, KarbonnTablet, AllFineTablet,
         * PROSCANTablet, YONESTablet, ChangJiaTablet, GUTablet, PointOfViewTablet,
         * OvermaxTablet, HCLTablet, DPSTablet, VistureTablet, CrestaTablet,
         * MediatekTablet, ConcordeTablet, GoCleverTablet, ModecomTablet, VoninoTablet,
         * ECSTablet, StorexTablet, VodafoneTablet, EssentielBTablet, RossMoorTablet,
         * iMobileTablet, TolinoTablet, AudioSonicTablet, AMPETablet, SkkTablet,
         * TecnoTablet, JXDTablet, iJoyTablet, FX2Tablet, XoroTablet, ViewsonicTablet,
         * VerizonTablet, OdysTablet, CaptivaTablet, IconbitTablet, TeclastTablet,
         * OndaTablet, JaytechTablet, BlaupunktTablet, DigmaTablet, EvolioTablet,
         * LavaTablet, AocTablet, MpmanTablet, CelkonTablet, WolderTablet, MediacomTablet,
         * MiTablet, NibiruTablet, NexoTablet, LeaderTablet, UbislateTablet,
         * PocketBookTablet, KocasoTablet, HisenseTablet, Hudl, TelstraTablet,
         * GenericTablet</tt><br>
         * <br>
         * If the device is not detected by the regular expressions from Mobile-Detect, a test is made against
         * the patterns of <a href="http://detectmobilebrowsers.com/">detectmobilebrowsers.com</a>. If this test
         * is positive, a value of <code>UnknownTablet</code> or <code>UnknownMobile</code> is returned.<br>
         * When used in browser, the decision whether phone or tablet is made based on <code>screen.width/height</code>.<br>
         * <br>
         * When used server-side (node.js), there is no way to tell the difference between <code>UnknownTablet</code>
         * and <code>UnknownMobile</code>, so you will get <code>null</code> here, while {@link MobileDetect#mobile}
         * will return <code>UnknownMobile</code>.<br>
         * Be aware that since v1.0.0 in this special case you will get <code>UnknownMobile</code> only for:
         * {@link MobileDetect#mobile}, not for {@link MobileDetect#phone} and {@link MobileDetect#tablet}.
         * In versions before v1.0.0 all 3 methods returned <code>UnknownMobile</code> which was tedious to use.
         * <br>
         * In most cases you will use the return value just as a boolean.
         *
         * @returns {String} the key of the tablet family or producer, e.g. "SamsungTablet"
         * @function MobileDetect#tablet
         */
        tablet: function () {
            impl.prepareDetectionCache(this._cache, this.ua, this.maxPhoneWidth);
            return this._cache.tablet;
        },

        /**
         * Returns the (first) detected user-agent string or <tt>null</tt>.
         * <br>
         * The returned user-agent is one of following keys:<br>
         * <br><tt>Chrome, Dolfin, Opera, Skyfire, Edge, IE, Firefox, Bolt, TeaShark, Blazer,
         * Safari, WeChat, UCBrowser, baiduboxapp, baidubrowser, DiigoBrowser, Mercury,
         * ObigoBrowser, NetFront, GenericBrowser, PaleMoon</tt><br>
         * <br>
         * In most cases calling {@link MobileDetect#userAgent} will be sufficient. But there are rare
         * cases where a mobile device pretends to be more than one particular browser. You can get the
         * list of all matches with {@link MobileDetect#userAgents} or check for a particular value by
         * providing one of the defined keys as first argument to {@link MobileDetect#is}.
         *
         * @returns {String} the key for the detected user-agent or <tt>null</tt>
         * @function MobileDetect#userAgent
         */
        userAgent: function () {
            if (this._cache.userAgent === undefined) {
                this._cache.userAgent = impl.findMatch(impl.mobileDetectRules.uas, this.ua);
            }
            return this._cache.userAgent;
        },

        /**
         * Returns all detected user-agent strings.
         * <br>
         * The array is empty or contains one or more of following keys:<br>
         * <br><tt>Chrome, Dolfin, Opera, Skyfire, Edge, IE, Firefox, Bolt, TeaShark, Blazer,
         * Safari, WeChat, UCBrowser, baiduboxapp, baidubrowser, DiigoBrowser, Mercury,
         * ObigoBrowser, NetFront, GenericBrowser, PaleMoon</tt><br>
         * <br>
         * In most cases calling {@link MobileDetect#userAgent} will be sufficient. But there are rare
         * cases where a mobile device pretends to be more than one particular browser. You can get the
         * list of all matches with {@link MobileDetect#userAgents} or check for a particular value by
         * providing one of the defined keys as first argument to {@link MobileDetect#is}.
         *
         * @returns {Array} the array of detected user-agent keys or <tt>[]</tt>
         * @function MobileDetect#userAgents
         */
        userAgents: function () {
            if (this._cache.userAgents === undefined) {
                this._cache.userAgents = impl.findMatches(impl.mobileDetectRules.uas, this.ua);
            }
            return this._cache.userAgents;
        },

        /**
         * Returns the detected operating system string or <tt>null</tt>.
         * <br>
         * The operating system is one of following keys:<br>
         * <br><tt>AndroidOS, BlackBerryOS, PalmOS, SymbianOS, WindowsMobileOS, WindowsPhoneOS,
         * iOS, iPadOS, MeeGoOS, MaemoOS, JavaOS, webOS, badaOS, BREWOS</tt><br>
         *
         * @returns {String} the key for the detected operating system.
         * @function MobileDetect#os
         */
        os: function () {
            if (this._cache.os === undefined) {
                this._cache.os = impl.detectOS(this.ua);
            }
            return this._cache.os;
        },

        /**
         * Get the version (as Number) of the given property in the User-Agent.
         * <br>
         * Will return a float number. (eg. 2_0 will return 2.0, 4.3.1 will return 4.31)
         *
         * @param {String} key a key defining a thing which has a version.<br>
         *        You can use one of following keys:<br>
         * <br><tt>Mobile, Build, Version, VendorID, iPad, iPhone, iPod, Kindle, Chrome, Coast,
         * Dolfin, Firefox, Fennec, Edge, IE, NetFront, NokiaBrowser, Opera, Opera Mini,
         * Opera Mobi, UCBrowser, MQQBrowser, MicroMessenger, baiduboxapp, baidubrowser,
         * SamsungBrowser, Iron, Safari, Skyfire, Tizen, Webkit, PaleMoon, Gecko, Trident,
         * Presto, Goanna, iOS, Android, BlackBerry, BREW, Java, Windows Phone OS, Windows
         * Phone, Windows CE, Windows NT, Symbian, webOS</tt><br>
         *
         * @returns {Number} the version as float or <tt>NaN</tt> if User-Agent doesn't contain this version.
         *          Be careful when comparing this value with '==' operator!
         * @function MobileDetect#version
         */
        version: function (key) {
            return impl.getVersion(key, this.ua);
        },

        /**
         * Get the version (as String) of the given property in the User-Agent.
         * <br>
         *
         * @param {String} key a key defining a thing which has a version.<br>
         *        You can use one of following keys:<br>
         * <br><tt>Mobile, Build, Version, VendorID, iPad, iPhone, iPod, Kindle, Chrome, Coast,
         * Dolfin, Firefox, Fennec, Edge, IE, NetFront, NokiaBrowser, Opera, Opera Mini,
         * Opera Mobi, UCBrowser, MQQBrowser, MicroMessenger, baiduboxapp, baidubrowser,
         * SamsungBrowser, Iron, Safari, Skyfire, Tizen, Webkit, PaleMoon, Gecko, Trident,
         * Presto, Goanna, iOS, Android, BlackBerry, BREW, Java, Windows Phone OS, Windows
         * Phone, Windows CE, Windows NT, Symbian, webOS</tt><br>
         *
         * @returns {String} the "raw" version as String or <tt>null</tt> if User-Agent doesn't contain this version.
         *
         * @function MobileDetect#versionStr
         */
        versionStr: function (key) {
            return impl.getVersionStr(key, this.ua);
        },

        /**
         * Global test key against userAgent, os, phone, tablet and some other properties of userAgent string.
         *
         * @param {String} key the key (case-insensitive) of a userAgent, an operating system, phone or
         *        tablet family.<br>
         *        For a complete list of possible values, see {@link MobileDetect#userAgent},
         *        {@link MobileDetect#os}, {@link MobileDetect#phone}, {@link MobileDetect#tablet}.<br>
         *        Additionally you have following keys:<br>
         * <br><tt>Bot, MobileBot, DesktopMode, TV, WebKit, Console, Watch</tt><br>
         *
         * @returns {boolean} <tt>true</tt> when the given key is one of the defined keys of userAgent, os, phone,
         *                    tablet or one of the listed additional keys, otherwise <tt>false</tt>
         * @function MobileDetect#is
         */
        is: function (key) {
            return containsIC(this.userAgents(), key) ||
                   equalIC(key, this.os()) ||
                   equalIC(key, this.phone()) ||
                   equalIC(key, this.tablet()) ||
                   containsIC(impl.findMatches(impl.mobileDetectRules.utils, this.ua), key);
        },

        /**
         * Do a quick test against navigator::userAgent.
         *
         * @param {String|RegExp} pattern the pattern, either as String or RegExp
         *                        (a string will be converted to a case-insensitive RegExp).
         * @returns {boolean} <tt>true</tt> when the pattern matches, otherwise <tt>false</tt>
         * @function MobileDetect#match
         */
        match: function (pattern) {
            if (!(pattern instanceof RegExp)) {
                pattern = new RegExp(pattern, 'i');
            }
            return pattern.test(this.ua);
        },

        /**
         * Checks whether the mobile device can be considered as phone regarding <code>screen.width</code>.
         * <br>
         * Obviously this method makes sense in browser environments only (not for Node.js)!
         * @param {number} [maxPhoneWidth] the maximum logical pixels (aka. CSS-pixels) to be considered as phone.<br>
         *        The argument is optional and if not present or falsy, the value of the constructor is taken.
         * @returns {boolean|undefined} <code>undefined</code> if screen size wasn't detectable, else <code>true</code>
         *          when screen.width is less or equal to maxPhoneWidth, otherwise <code>false</code>.<br>
         *          Will always return <code>undefined</code> server-side.
         */
        isPhoneSized: function (maxPhoneWidth) {
            return MobileDetect.isPhoneSized(maxPhoneWidth || this.maxPhoneWidth);
        },

        /**
         * Returns the mobile grade ('A', 'B', 'C').
         *
         * @returns {String} one of the mobile grades ('A', 'B', 'C').
         * @function MobileDetect#mobileGrade
         */
        mobileGrade: function () {
            if (this._cache.grade === undefined) {
                this._cache.grade = impl.mobileGrade(this);
            }
            return this._cache.grade;
        }
    };

    // environment-dependent
    if (typeof window !== 'undefined' && window.screen) {
        MobileDetect.isPhoneSized = function (maxPhoneWidth) {
            return maxPhoneWidth < 0 ? undefined : impl.getDeviceSmallerSide() <= maxPhoneWidth;
        };
    } else {
        MobileDetect.isPhoneSized = function () {};
    }

    // should not be replaced by a completely new object - just overwrite existing methods
    MobileDetect._impl = impl;
    
    MobileDetect.version = '1.4.4 2019-09-21';

    return MobileDetect;
}); // end of call of define()
})((function (undefined) {
    if ( true && module.exports) {
        return function (factory) { module.exports = factory(); };
    } else if (true) {
        return __webpack_require__(/*! !webpack amd define */ "../../node_modules/webpack/buildin/amd-define.js");
    } else {}
})());

/***/ }),

/***/ "../../node_modules/webpack/buildin/amd-define.js":
/*!***************************************!*\
  !*** (webpack)/buildin/amd-define.js ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = function() {
	throw new Error("define cannot be used indirect");
};


/***/ }),

/***/ "../../node_modules/webpack/buildin/global.js":
/*!***********************************!*\
  !*** (webpack)/buildin/global.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

var g;

// This works in non-strict mode
g = (function() {
	return this;
})();

try {
	// This works if eval is allowed (see CSP)
	g = g || new Function("return this")();
} catch (e) {
	// This works if the window reference is available
	if (typeof window === "object") g = window;
}

// g can still be undefined, but nothing to do about it...
// We return undefined, instead of nothing here, so it's
// easier to handle this case. if(!global) { ...}

module.exports = g;


/***/ }),

/***/ "../../node_modules/webpack/buildin/module.js":
/*!***********************************!*\
  !*** (webpack)/buildin/module.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = function(module) {
	if (!module.webpackPolyfill) {
		module.deprecate = function() {};
		module.paths = [];
		// module.parent = undefined by default
		if (!module.children) module.children = [];
		Object.defineProperty(module, "loaded", {
			enumerable: true,
			get: function() {
				return module.l;
			}
		});
		Object.defineProperty(module, "id", {
			enumerable: true,
			get: function() {
				return module.i;
			}
		});
		module.webpackPolyfill = 1;
	}
	return module;
};


/***/ }),

/***/ "./src/ElasticNumber.js":
/*!******************************!*\
  !*** ./src/ElasticNumber.js ***!
  \******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return ElasticNumber; });
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/classCallCheck */ "../../node_modules/@babel/runtime/helpers/classCallCheck.js");
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime/helpers/createClass */ "../../node_modules/@babel/runtime/helpers/createClass.js");
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1__);



/**
 * An Utility class that allows to tween a numeric value (_target_) into a smoothed value (_value_)
 *
 *
 * @class ElasticNumber
 * @property {Number} value - The real value of the number.
 * @property {Number} target - The desired value of the number.
 * @property {Number} speed - The speed at which the ElasticNumber will make value reach target.
 * @example
 *
 * constructor () {
 *  this.myNumber = new ElasticNumber();
 *  this.time = new Date().getTime()
 * }
 *
 * onUpdate () {
 *  const now = new Date().getTime()
 *  const delta = now - this.time
 *  this.myNumber.target = Math.random() * 100
 *  this.myNumber.update(delta);
 *
 *  this.mySprite.x = this.myNumber.value
 *  this.time = now
 * }
 */
var ElasticNumber = /*#__PURE__*/function () {
  /**
   * Initializes the ElasticNumber with a
   * @ignore
   * @param {Number} value predefined value (0 by default)
   * @memberOf ElasticNumber
   */
  function ElasticNumber() {
    var value = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;

    _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default()(this, ElasticNumber);

    this.value = value;
    this.target = value;
    this.speed = 3;
  }
  /**
   * Updates the ElasticNumber on tick.
   * Value will be updated from target.
   *
   * @param {Number} delta delta time in milliseconds
   * @memberOf ElasticNumber
   */


  _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1___default()(ElasticNumber, [{
    key: "update",
    value: function update(delta) {
      // delta = Math.min(delta, 1 / 20);
      var dist = this.target - this.value;
      this.value += dist * (this.speed * delta);
    }
  }]);

  return ElasticNumber;
}();



/***/ }),

/***/ "./src/FileUtils.js":
/*!**************************!*\
  !*** ./src/FileUtils.js ***!
  \**************************/
/*! exports provided: downloadFile, loadFile, loadJSON, loadImage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "downloadFile", function() { return downloadFile; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "loadFile", function() { return loadFile; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "loadJSON", function() { return loadJSON; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "loadImage", function() { return loadImage; });
/**
 * A collection of File related utilities.
 *
 * @module FileUtils
 */

/**
 * Downloads a file to the user's machine.
 *
 * @param {String} content - The content to be downloaded.
 * @param {String} fileName - The name of the file downloaded into the user's PC.
 * @param {String} contentType - The file type.
 */
var downloadFile = function downloadFile(content, fileName, contentType) {
  var a = document.createElement('a');
  var file = new Blob([content], {
    type: contentType
  });
  a.href = URL.createObjectURL(file);
  a.download = fileName;
  a.click();
};
/**
 * Prompts the user to select a single file from his harddrive.
 *
 * @param {String} [accept] - Accept string to restrict file selection to certain file types.
 * @returns {Promise<Array>} Array of files selected by the user.
 */


var loadFile = function loadFile(accept) {
  return new Promise(function (resolve, reject) {
    var input = document.createElement('input');
    input.type = 'file';
    input.accept = accept;

    input.onchange = function (event) {
      resolve(event.target.files);
    };

    input.click();
  });
};
/**
 * Prompts the user to select a single JSON file from his harddrive
 * and returns the result of his selection.
 *
 * @returns {Promise<String>} The parsed JSON file.
 */


var loadJSON = function loadJSON() {
  return new Promise(function (resolve, reject) {
    loadFile('.json').then(function (files) {
      var fr = new FileReader();

      fr.onload = function (event) {
        resolve(JSON.parse(event.target.result));
      };

      fr.readAsText(files[0]);
      return true;
    }).catch(function (e) {
      throw e;
    });
  });
};
/**
 * Prompts the user to select a single JSON file from his harddrive
 * and returns the result of his selection
 *
 * @returns {Promise<Image>} The selected image object.
 */


var loadImage = function loadImage() {
  return new Promise(function (resolve, reject) {
    var img = new Image();

    img.onload = function (event) {
      resolve(img);
    };

    loadFile('image/*').then(function (files) {
      var fr = new FileReader();

      fr.onload = function (event) {
        img.src = event.target.result;
      };

      fr.readAsDataURL(files[0]);
      return true;
    }).catch(function (e) {
      throw e;
    });
  });
};



/***/ }),

/***/ "./src/Gyroscope.js":
/*!**************************!*\
  !*** ./src/Gyroscope.js ***!
  \**************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/classCallCheck */ "../../node_modules/@babel/runtime/helpers/classCallCheck.js");
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime/helpers/createClass */ "../../node_modules/@babel/runtime/helpers/createClass.js");
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _RAF__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./RAF */ "./src/RAF.js");




__webpack_require__(/*! @hughsk/fulltilt/dist/fulltilt */ "../../node_modules/@hughsk/fulltilt/dist/fulltilt.js");
/**
 * An utility singleton that returns a device's Gyroscope
 * data in the form of orientationX and orientationY.
 *
 * The values are updated every frame and the system stops updating
 * if it determines that the device does not have gyroscope capabilities.
 *
 * @class Gyroscope
 * @property {Number} orientationX
 * @property {Number} orientationY
 * @property {Boolean} enabled
 */


var Gyroscope = /*#__PURE__*/function () {
  /**
   * @ignore
   */
  function Gyroscope() {
    _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default()(this, Gyroscope);

    this.orientationX = 0;
    this.orientationY = 0;
    this.enabled = true;
    this._update = this._update.bind(this);
    _RAF__WEBPACK_IMPORTED_MODULE_2__["default"].add(this._update);
  }
  /**
   * @ignore
   */


  _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1___default()(Gyroscope, [{
    key: "_update",
    value: function _update() {
      var _this = this;

      var fulltiltPromise = new window.FULLTILT.getDeviceOrientation({
        type: 'game'
      }); // eslint-disable-line new-cap

      fulltiltPromise.then(function (deviceOrientation) {
        var euler = deviceOrientation.getScreenAdjustedEuler();
        _this.orientationX = euler.beta;
        _this.orientationY = euler.gamma;

        if (_this.orientationX > 90) {
          _this.orientationY = -_this.orientationY;
        }

        return null; // console.log(this.orientationX, this.orientationY)
      }).catch(function (message) {
        console.warn('Disabling Gyroscope, reason:', message);
        _this.enabled = false;
        _RAF__WEBPACK_IMPORTED_MODULE_2__["default"].remove(_this._update);
      });
    }
  }]);

  return Gyroscope;
}();
/**
 * @ignore
 */


/* harmony default export */ __webpack_exports__["default"] = (Gyroscope);

/***/ }),

/***/ "./src/NumberUtils.js":
/*!****************************!*\
  !*** ./src/NumberUtils.js ***!
  \****************************/
/*! exports provided: lerpNumber, distanceCompare, distance, mapToPower, randByPower, smoothstep, angleDistanceSign, angleDistance, map, clamp, range, mix */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "lerpNumber", function() { return lerpNumber; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "distanceCompare", function() { return distanceCompare; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "distance", function() { return distance; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "mapToPower", function() { return mapToPower; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "randByPower", function() { return randByPower; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "smoothstep", function() { return smoothstep; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "angleDistanceSign", function() { return angleDistanceSign; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "angleDistance", function() { return angleDistance; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "map", function() { return map; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "clamp", function() { return clamp; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "range", function() { return range; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "mix", function() { return mix; });
/**
 * A collection of Number related utilities
 *
 * @module NumberUtils
 */

/**
  * Linearly interpolates a Number
  *
  * @param {Number} v0 Initial value
  * @param {Number} v1 Final value
  * @param {Number} t zero to one
  * @returns {Number} Interpolation between v0 and v1 based on t
  */
var lerpNumber = function lerpNumber(v0, v1, t) {
  return v0 + t * (v1 - v0);
};
/**
 * TODO: Documentation
 *
 * @param {*} oldValue
 * @param {*} oldMin
 * @param {*} oldMax
 * @param {*} newMin
 * @param {*} newMax
 * @param {*} clamped
 */


var range = function range(oldValue, oldMin, oldMax, newMin, newMax, clamped) {
  var oldRange = oldMax - oldMin;
  var newRange = newMax - newMin;
  var newValue = (oldValue - oldMin) * newRange / oldRange + newMin;

  if (clamped) {
    return clamp(newValue, newMin, newMax);
  }

  return newValue;
};
/**
 * Clamps a value between min and max values
 *
 * @param {Number} value - The value to be clamped.
 * @param {Number} min - Minimum value.
 * @param {Number} max - Maximum value.
 * @returns {Number} A number clamped between min and max
 */


var clamp = function clamp(value) {
  var min = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
  var max = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 1;
  return Math.max(min, Math.min(value, max));
};
/**
 * TODO: Documentation
 *
 * @param {Number} value
 * @param {Number} min
 * @param {Number} max
 */


var map = function map(value, min, max) {
  return clamp((value - min) / (max - min), 0, 1);
};
/**
 * TODO: Documentation
 *
 * @param {Number} alpha
 * @param {Number} beta
 */


var angleDistance = function angleDistance(alpha, beta) {
  var phi = Math.abs(beta - alpha) % Math.PI * 2;
  return phi > Math.PI ? Math.PI * 2 - phi : phi;
};
/**
 * TODO: Documentation
 *
 * @param {Number} alpha
 * @param {Number} beta
 */


var angleDistanceSign = function angleDistanceSign(alpha, beta) {
  var sign = alpha - beta >= 0 && alpha - beta <= Math.PI || alpha - beta <= -Math.PI && alpha - beta >= -Math.PI * 2 ? 1 : -1;
  return angleDistance(alpha, beta) * sign;
};
/**
 * Smoothstep implementation.
 * https://en.wikipedia.org/wiki/Smoothstep
 *
 * @param {Number} min - Initial value
 * @param {Number} max - Final Value
 * @param {Number} value -
 * @returns {Number} Value of the interpolation
 */


var smoothstep = function smoothstep(min, max, value) {
  var x = Math.max(0, Math.min(1, (value - min) / (max - min)));
  return x * x * (3 - 2 * x);
};
/**
 * Similar to linear interpolation.
 *
 * @param {Number} value1 - Minimum value.
 * @param {Number} value2 - Maximum value.
 * @param {Number} percent - zero to one value percent
 */


var mix = function mix(value1, value2, percent) {
  return value1 * (1 - percent) + value2 * percent;
};
/**
 * Returns the sign of a number in -1,0,1 form.
 *
 * @param {Number} value - a number to be tested
 * @returns {Number} 0 if value is zero, -1 if value is less than 0, 1 otherwise
 */


var sign = function sign(value) {
  return value !== 0 ? value < 0 ? -1 : 1 : 0;
};
/**
 * TODO: Documentation
 *
 * @param {Number} min
 * @param {Number} max
 * @param {Number} power
 * @param {Number} [rd = Math.random] - Optional Random number generator
 */


var randByPower = function randByPower(min, max, power) {
  var rd = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : Math.random;
  var value = rd() * (max - min) + min;
  return mapToPower(value, min, max, power);
};
/**
 * TODO: Documentation
 *
 * @param {Number} value
 * @param {Number} min
 * @param {Number} max
 * @param {Number} power
 */


var mapToPower = function mapToPower(value, min, max, power) {
  var valueSign = sign(value);
  value = (value - min) / (max - min);
  value = valueSign > 0 ? value : value - 1;
  value = Math.pow(value * valueSign, Math.abs(power));
  value = valueSign > 0 ? value : 1 - value;
  return value * (max - min) + min;
};
/**
 * Computes distance between two points in 2D space.
 *
 * @param {Number} x1 - x coordinate of first point
 * @param {Number} y1 - y coordinate of first point
 * @param {Number} x2 - x coordinate of second point
 * @param {Number} y2 - y coordinate of second point
 * @returns {Number} Distance between x1,y1 and x2,y2
 */


var distance = function distance(x1, y1, x2, y2) {
  var dx = x1 - x2;
  var dy = y1 - y2;
  return Math.sqrt(dx * dx + dy * dy);
};
/**
 * Not a real distance calculation.
 * Useful to sort objects by distance ( much faster because no sqrt )
 *
 * @param {Number} x1 - x coordinate of first point
 * @param {Number} y1 - y coordinate of first point
 * @param {Number} x2 - x coordinate of second point
 * @param {Number} y2 - y coordinate of second point
 * @returns {Number} bogus distance between x1,y1 and x2,y2. DO NOT USE WHEN A REAL DISTANCE IS NEEDED
 */


var distanceCompare = function distanceCompare(x1, y1, x2, y2) {
  var dx = x1 - x2;
  var dy = y1 - y2;
  return dx * dx + dy * dy;
};



/***/ }),

/***/ "./src/ObjectUtils.js":
/*!****************************!*\
  !*** ./src/ObjectUtils.js ***!
  \****************************/
/*! exports provided: isPlainObject */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isPlainObject", function() { return isPlainObject; });
/* harmony import */ var _babel_runtime_helpers_typeof__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/typeof */ "../../node_modules/@babel/runtime/helpers/typeof.js");
/* harmony import */ var _babel_runtime_helpers_typeof__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_typeof__WEBPACK_IMPORTED_MODULE_0__);


/**
 * A collection of Object related utilities
 *
 * @module ObjectUtils
 */

/**
 * Determines if the object has been constructed using
 * javascript generic {} syntax
 *
 * @param {*} o - The object to be tested.
 * @returns {Boolean} true if the object is a plain object, false otherwise.
 */
var isPlainObject = function isPlainObject(o) {
  return o && _babel_runtime_helpers_typeof__WEBPACK_IMPORTED_MODULE_0___default()(o) === 'object' && o.constructor === Object;
};



/***/ }),

/***/ "./src/Platform.js":
/*!*************************!*\
  !*** ./src/Platform.js ***!
  \*************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
var MobileDetect = __webpack_require__(/*! mobile-detect */ "../../node_modules/mobile-detect/mobile-detect.js");

var md = new MobileDetect(window.navigator.userAgent);
var volumeTest = document.createElement('audio');
volumeTest.volume = 0.5;
/**
 * Holds information on the Platform where this code is run
 *
 * @module Platform
 * @property {Boolean} mobile - Device is a mobile (includes tablets and phones)
 * @property {Boolean} phone - Device is a phone (excludes tablets)
 * @property {Boolean} tablet - Device is a tablet (excludes phones)
 * @property {Boolean} android - Device is Android based
 * @property {Boolean} ios - Device is iOS based
 * @property {Boolean} ipad - Device is an iPad
 * @property {Boolean} iphone - Device is an iPhone
 * @property {Boolean} wphone - Device is an Windows Phone
 * @property {Boolean} edge - Browser is Microsoft Edge
 * @property {Boolean} firefox - Browser is Mozilla Firefox
 * @property {Boolean} ie11 - Browser is Microsoft Internet Explorer 11
 * @property {Boolean} safari - Browser is Safari
 * @property {Boolean} prerenderer - Page is visited by a prerenderer (like Phantom JS)
 * @property {Boolean} volume - Device supports volume setting via js (iOS doesn't support this)
 */

var Platform = {
  mobile: !!md.mobile(),
  phone: !!md.phone(),
  tablet: !!md.tablet(),
  android: !!md.is('AndroidOS'),
  ios: !!md.is('iOS'),
  ipad: !!md.is('iPad'),
  iphone: !!md.is('iPhone'),
  wphone: !!md.is('WindowsPhoneOS'),
  edge: !!/Edge\/\d+/i.test(window.navigator.userAgent),
  firefox: md.version('Gecko') > 1,
  ie11: !!/Trident.*rv:11\./i.test(window.navigator.userAgent),
  safari: /Safari/.test(window.navigator.userAgent) && /Apple Computer/.test(window.navigator.vendor),
  prerenderer: window.__PRERENDER_INJECTED !== undefined,
  // /PhantomJS/.test(window.navigator.userAgent),
  volume: volumeTest.volume === 0.5
}; // if not prerenderer

if (!Platform.prerenderer && window.Modernizr) {
  var _loop = function _loop(key) {
    window.Modernizr.addTest(key, function () {
      return Platform[key];
    });
  };

  // add custom modernizr tests
  for (var key in Platform) {
    _loop(key);
  }
}

/* harmony default export */ __webpack_exports__["default"] = (Platform);

/***/ }),

/***/ "./src/RAF.js":
/*!********************!*\
  !*** ./src/RAF.js ***!
  \********************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/classCallCheck */ "../../node_modules/@babel/runtime/helpers/classCallCheck.js");
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime/helpers/createClass */ "../../node_modules/@babel/runtime/helpers/createClass.js");
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1__);



/**
 * An utility singleton that holds all requestAnimationFrame subscribers
 * and calls them in sequence each frame.
 *
 * @class RAF
 * @example
 *
 * import RAF from 'js-utils'
 *
 * constructor () {
 *  RAF.add(this.onUpdate)
 * }
 *
 * onUpdate () {
 *  // do stuff
 * }
 *
 * onDestroy () {
 *  RAF.remove(this.onUpdate)
 * }
 */
var RAF = /*#__PURE__*/function () {
  function RAF() {
    _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default()(this, RAF);

    this.listeners = [];
    this._update = this._update.bind(this);

    this._update();
  }
  /**
   * Adds a subscriber to be called at each requestAnimationFrame
   *
   * @param {Function} listener A subscriber function
   */


  _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1___default()(RAF, [{
    key: "add",
    value: function add(listener) {
      var index = this.listeners.indexOf(listener);

      if (index === -1) {
        this.listeners.push(listener);
      }
    }
    /**
     * Removes a subscriber from requestAnimationFrame
     *
     * @param {Function} listener A subscriber function
     */

  }, {
    key: "remove",
    value: function remove(listener) {
      var index = this.listeners.indexOf(listener);

      if (index !== -1) {
        this.listeners.splice(index, 1);
      }
    }
    /**
     * @ignore
     */

  }, {
    key: "_update",
    value: function _update() {
      for (var i = 0; i < this.listeners.length; i++) {
        this.listeners[i]();
      }

      window.requestAnimationFrame(this._update);
    }
  }]);

  return RAF;
}();
/**
 * @ignore
 */


/* harmony default export */ __webpack_exports__["default"] = (new RAF());

/***/ }),

/***/ "./src/VueUtils.js":
/*!*************************!*\
  !*** ./src/VueUtils.js ***!
  \*************************/
/*! exports provided: defineReactive */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "defineReactive", function() { return defineReactive; });
/* harmony import */ var _babel_runtime_helpers_typeof__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/typeof */ "../../node_modules/@babel/runtime/helpers/typeof.js");
/* harmony import */ var _babel_runtime_helpers_typeof__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_typeof__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var lodash_keys__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! lodash/keys */ "../../node_modules/lodash/keys.js");
/* harmony import */ var lodash_keys__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(lodash_keys__WEBPACK_IMPORTED_MODULE_1__);


/**
 * A collection of Vue.js related utilities
 *
 * @module VueUtils
 */

/**
 * Allows to define a set of reactive properties of "value" (Object)
 * by looking into "model" (Object) and comparing the two
 *
 * @param {Object} value
 * @param {Object} model
 */

var defineReactive = function defineReactive(value, model) {
  var valueKeys = lodash_keys__WEBPACK_IMPORTED_MODULE_1___default()(value);
  var modelKeys = lodash_keys__WEBPACK_IMPORTED_MODULE_1___default()(model);
  valueKeys.forEach(function (key) {
    if (modelKeys.indexOf(key) === -1) {
      Object.defineProperty(value, key, {
        configurable: false
      });
    } else {
      // found on model, recurse
      if (_babel_runtime_helpers_typeof__WEBPACK_IMPORTED_MODULE_0___default()(model[key]) === 'object') {
        defineReactive(value[key], model[key]);
      }
    }
  });
};



/***/ }),

/***/ "./src/js-utils.js":
/*!*************************!*\
  !*** ./src/js-utils.js ***!
  \*************************/
/*! exports provided: ElasticNumber, Gyroscope, normalizeWheel, RAF, Platform, FileUtils, NumberUtils, VueUtils, ObjectUtils */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FileUtils", function() { return FileUtils; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NumberUtils", function() { return NumberUtils; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VueUtils", function() { return VueUtils; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ObjectUtils", function() { return ObjectUtils; });
/* harmony import */ var _FileUtils__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./FileUtils */ "./src/FileUtils.js");
/* harmony import */ var _NumberUtils__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./NumberUtils */ "./src/NumberUtils.js");
/* harmony import */ var _ObjectUtils__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./ObjectUtils */ "./src/ObjectUtils.js");
/* harmony import */ var _VueUtils__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./VueUtils */ "./src/VueUtils.js");
/* harmony import */ var _ElasticNumber__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./ElasticNumber */ "./src/ElasticNumber.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ElasticNumber", function() { return _ElasticNumber__WEBPACK_IMPORTED_MODULE_4__["default"]; });

/* harmony import */ var _Gyroscope__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./Gyroscope */ "./src/Gyroscope.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Gyroscope", function() { return _Gyroscope__WEBPACK_IMPORTED_MODULE_5__["default"]; });

/* harmony import */ var _normalizeWheel__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./normalizeWheel */ "./src/normalizeWheel.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "normalizeWheel", function() { return _normalizeWheel__WEBPACK_IMPORTED_MODULE_6__["default"]; });

/* harmony import */ var _RAF__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./RAF */ "./src/RAF.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "RAF", function() { return _RAF__WEBPACK_IMPORTED_MODULE_7__["default"]; });

/* harmony import */ var _Platform__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./Platform */ "./src/Platform.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Platform", function() { return _Platform__WEBPACK_IMPORTED_MODULE_8__["default"]; });










var FileUtils = _FileUtils__WEBPACK_IMPORTED_MODULE_0__;
var NumberUtils = _NumberUtils__WEBPACK_IMPORTED_MODULE_1__;
var VueUtils = _VueUtils__WEBPACK_IMPORTED_MODULE_3__;
var ObjectUtils = _ObjectUtils__WEBPACK_IMPORTED_MODULE_2__;

/***/ }),

/***/ "./src/normalizeWheel.js":
/*!*******************************!*\
  !*** ./src/normalizeWheel.js ***!
  \*******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/**
 * An utility function to normalize the mouseWheel input in the browser.
 *
 * @module normalizeWheel
 * @typechecks
 */
// Reasonable defaults
var PIXEL_STEP = 10;
var LINE_HEIGHT = 40;
var PAGE_HEIGHT = 800;
/**
 * Mouse wheel (and 2-finger trackpad) support on the web sucks.  It is
 * complicated, thus this doc is long and (hopefully) detailed enough to answer
 * your questions.
 *
 * If you need to react to the mouse wheel in a predictable way, this code is
 * like your bestest friend. * hugs *
 *
 * As of today, there are 4 DOM event types you can listen to:
 *
 *   'wheel'                -- Chrome(31+), FF(17+), IE(9+)
 *   'mousewheel'           -- Chrome, IE(6+), Opera, Safari
 *   'MozMousePixelScroll'  -- FF(3.5 only!) (2010-2013) -- don't bother!
 *   'DOMMouseScroll'       -- FF(0.9.7+) since 2003
 *
 * So what to do?  The is the best:
 *
 *   normalizeWheel.getEventType()
 *
 * In your event callback, use this code to get sane interpretation of the
 * deltas.  This code will return an object with properties:
 *
 *   spinX   -- normalized spin speed (use for zoom) - x plane
 *   spinY   -- " - y plane
 *   pixelX  -- normalized distance (to pixels) - x plane
 *   pixelY  -- " - y plane
 *
 * Wheel values are provided by the browser assuming you are using the wheel to
 * scroll a web page by a number of lines or pixels (or pages).  Values can lety
 * significantly on different platforms and browsers, forgetting that you can
 * scroll at different speeds.  Some devices (like trackpads) emit more events
 * at smaller increments with fine granularity, and some emit massive jumps with
 * linear speed or acceleration.
 *
 * This code does its best to normalize the deltas for you:
 *
 *   - spin is trying to normalize how far the wheel was spun (or trackpad
 *     dragged).  This is super useful for zoom support where you want to
 *     throw away the chunky scroll steps on the PC and make those equal to
 *     the slow and smooth tiny steps on the Mac. Key data: This code tries to
 *     resolve a single slow step on a wheel to 1.
 *
 *   - pixel is normalizing the desired scroll delta in pixel units.  You'll
 *     get the crazy differences between browsers, but at least it'll be in
 *     pixels!
 *
 *   - positive value indicates scrolling DOWN/RIGHT, negative UP/LEFT.  This
 *     should translate to positive value zooming IN, negative zooming OUT.
 *     This matches the newer 'wheel' event.
 *
 * Why are there spinX, spinY (or pixels)?
 *
 *   - spinX is a 2-finger side drag on the trackpad, and a shift + wheel turn
 *     with a mouse.  It results in side-scrolling in the browser by default.
 *
 *   - spinY is what you expect -- it's the classic axis of a mouse wheel.
 *
 *   - I dropped spinZ/pixelZ.  It is supported by the DOM 3 'wheel' event and
 *     probably is by browsers in conjunction with fancy 3D controllers .. but
 *     you know.
 *
 * Implementation info:
 *
 * Examples of 'wheel' event if you scroll slowly (down) by one step with an
 * average mouse:
 *
 *   OS X + Chrome  (mouse)     -    4   pixel delta  (wheelDelta -120)
 *   OS X + Safari  (mouse)     -  N/A   pixel delta  (wheelDelta  -12)
 *   OS X + Firefox (mouse)     -    0.1 line  delta  (wheelDelta  N/A)
 *   Win8 + Chrome  (mouse)     -  100   pixel delta  (wheelDelta -120)
 *   Win8 + Firefox (mouse)     -    3   line  delta  (wheelDelta -120)
 *
 * On the trackpad:
 *
 *   OS X + Chrome  (trackpad)  -    2   pixel delta  (wheelDelta   -6)
 *   OS X + Firefox (trackpad)  -    1   pixel delta  (wheelDelta  N/A)
 *
 * On other/older browsers.. it's more complicated as there can be multiple and
 * also missing delta values.
 *
 * The 'wheel' event is more standard:
 *
 * http://www.w3.org/TR/DOM-Level-3-Events/#events-wheelevents
 *
 * The basics is that it includes a unit, deltaMode (pixels, lines, pages), and
 * deltaX, deltaY and deltaZ.  Some browsers provide other values to maintain
 * backward compatibility with older events.  Those other values help us
 * better normalize spin speed.  Example of what the browsers provide:
 *
 *                          | event.wheelDelta | event.detail
 *        ------------------+------------------+--------------
 *          Safari v5/OS X  |       -120       |       0
 *          Safari v5/Win7  |       -120       |       0
 *         Chrome v17/OS X  |       -120       |       0
 *         Chrome v17/Win7  |       -120       |       0
 *                IE9/Win7  |       -120       |   undefined
 *         Firefox v4/OS X  |     undefined    |       1
 *         Firefox v4/Win7  |     undefined    |       3
 *
 */

function normalizeWheel(event) {
  var sX = 0;
  var sY = 0; // spinX, spinY

  var pX = 0;
  var pY = 0; // pixelX, pixelY
  // Legacy

  if ('detail' in event) {
    sY = event.detail;
  }

  if ('wheelDelta' in event) {
    sY = -event.wheelDelta / 120;
  }

  if ('wheelDeltaY' in event) {
    sY = -event.wheelDeltaY / 120;
  }

  if ('wheelDeltaX' in event) {
    sX = -event.wheelDeltaX / 120;
  } // side scrolling on FF with DOMMouseScroll


  if ('axis' in event && event.axis === event.HORIZONTAL_AXIS) {
    sX = sY;
    sY = 0;
  }

  pX = sX * PIXEL_STEP;
  pY = sY * PIXEL_STEP;

  if ('deltaY' in event) {
    pY = event.deltaY;
  }

  if ('deltaX' in event) {
    pX = event.deltaX;
  }

  if ((pX || pY) && event.deltaMode) {
    if (event.deltaMode === 1) {
      // delta in LINE units
      pX *= LINE_HEIGHT;
      pY *= LINE_HEIGHT;
    } else {
      // delta in PAGE units
      pX *= PAGE_HEIGHT;
      pY *= PAGE_HEIGHT;
    }
  } // Fall-back if spin cannot be determined


  if (pX && !sX) {
    sX = pX < 1 ? -1 : 1;
  }

  if (pY && !sY) {
    sY = pY < 1 ? -1 : 1;
  }

  return {
    spinX: sX,
    spinY: sY,
    pixelX: pX,
    pixelY: pY
  };
} // /**
//  * The best combination if you prefer spinX + spinY normalization.  It favors
//  * the older DOMMouseScroll for Firefox, as FF does not include wheelDelta with
//  * 'wheel' event, making spin speed determination impossible.
//  */
// const getEventType = function () {
//   return (UserAgent_DEPRECATED.firefox())
//     ? 'DOMMouseScroll'
//     : (isEventSupported('wheel'))
//       ? 'wheel'
//       : 'mousewheel'
// }


/* harmony default export */ __webpack_exports__["default"] = (normalizeWheel);

/***/ })

/******/ });
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9qcy11dGlscy93ZWJwYWNrL3VuaXZlcnNhbE1vZHVsZURlZmluaXRpb24iLCJ3ZWJwYWNrOi8vanMtdXRpbHMvd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vanMtdXRpbHMvRDovVXNlcnMvZGFuaS9EZXNrdG9wL0Btb25vZ3JpZC9ub2RlX21vZHVsZXMvQGJhYmVsL3J1bnRpbWUvaGVscGVycy9jbGFzc0NhbGxDaGVjay5qcyIsIndlYnBhY2s6Ly9qcy11dGlscy9EOi9Vc2Vycy9kYW5pL0Rlc2t0b3AvQG1vbm9ncmlkL25vZGVfbW9kdWxlcy9AYmFiZWwvcnVudGltZS9oZWxwZXJzL2NyZWF0ZUNsYXNzLmpzIiwid2VicGFjazovL2pzLXV0aWxzL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL0BiYWJlbC9ydW50aW1lL2hlbHBlcnMvdHlwZW9mLmpzIiwid2VicGFjazovL2pzLXV0aWxzL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL0BodWdoc2svZnVsbHRpbHQvZGlzdC9mdWxsdGlsdC5qcyIsIndlYnBhY2s6Ly9qcy11dGlscy9EOi9Vc2Vycy9kYW5pL0Rlc2t0b3AvQG1vbm9ncmlkL25vZGVfbW9kdWxlcy9sb2Rhc2gvX1N5bWJvbC5qcyIsIndlYnBhY2s6Ly9qcy11dGlscy9EOi9Vc2Vycy9kYW5pL0Rlc2t0b3AvQG1vbm9ncmlkL25vZGVfbW9kdWxlcy9sb2Rhc2gvX2FycmF5TGlrZUtleXMuanMiLCJ3ZWJwYWNrOi8vanMtdXRpbHMvRDovVXNlcnMvZGFuaS9EZXNrdG9wL0Btb25vZ3JpZC9ub2RlX21vZHVsZXMvbG9kYXNoL19iYXNlR2V0VGFnLmpzIiwid2VicGFjazovL2pzLXV0aWxzL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL2xvZGFzaC9fYmFzZUlzQXJndW1lbnRzLmpzIiwid2VicGFjazovL2pzLXV0aWxzL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL2xvZGFzaC9fYmFzZUlzVHlwZWRBcnJheS5qcyIsIndlYnBhY2s6Ly9qcy11dGlscy9EOi9Vc2Vycy9kYW5pL0Rlc2t0b3AvQG1vbm9ncmlkL25vZGVfbW9kdWxlcy9sb2Rhc2gvX2Jhc2VLZXlzLmpzIiwid2VicGFjazovL2pzLXV0aWxzL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL2xvZGFzaC9fYmFzZVRpbWVzLmpzIiwid2VicGFjazovL2pzLXV0aWxzL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL2xvZGFzaC9fYmFzZVVuYXJ5LmpzIiwid2VicGFjazovL2pzLXV0aWxzL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL2xvZGFzaC9fZnJlZUdsb2JhbC5qcyIsIndlYnBhY2s6Ly9qcy11dGlscy9EOi9Vc2Vycy9kYW5pL0Rlc2t0b3AvQG1vbm9ncmlkL25vZGVfbW9kdWxlcy9sb2Rhc2gvX2dldFJhd1RhZy5qcyIsIndlYnBhY2s6Ly9qcy11dGlscy9EOi9Vc2Vycy9kYW5pL0Rlc2t0b3AvQG1vbm9ncmlkL25vZGVfbW9kdWxlcy9sb2Rhc2gvX2lzSW5kZXguanMiLCJ3ZWJwYWNrOi8vanMtdXRpbHMvRDovVXNlcnMvZGFuaS9EZXNrdG9wL0Btb25vZ3JpZC9ub2RlX21vZHVsZXMvbG9kYXNoL19pc1Byb3RvdHlwZS5qcyIsIndlYnBhY2s6Ly9qcy11dGlscy9EOi9Vc2Vycy9kYW5pL0Rlc2t0b3AvQG1vbm9ncmlkL25vZGVfbW9kdWxlcy9sb2Rhc2gvX25hdGl2ZUtleXMuanMiLCJ3ZWJwYWNrOi8vanMtdXRpbHMvRDovVXNlcnMvZGFuaS9EZXNrdG9wL0Btb25vZ3JpZC9ub2RlX21vZHVsZXMvbG9kYXNoL19ub2RlVXRpbC5qcyIsIndlYnBhY2s6Ly9qcy11dGlscy9EOi9Vc2Vycy9kYW5pL0Rlc2t0b3AvQG1vbm9ncmlkL25vZGVfbW9kdWxlcy9sb2Rhc2gvX29iamVjdFRvU3RyaW5nLmpzIiwid2VicGFjazovL2pzLXV0aWxzL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL2xvZGFzaC9fb3ZlckFyZy5qcyIsIndlYnBhY2s6Ly9qcy11dGlscy9EOi9Vc2Vycy9kYW5pL0Rlc2t0b3AvQG1vbm9ncmlkL25vZGVfbW9kdWxlcy9sb2Rhc2gvX3Jvb3QuanMiLCJ3ZWJwYWNrOi8vanMtdXRpbHMvRDovVXNlcnMvZGFuaS9EZXNrdG9wL0Btb25vZ3JpZC9ub2RlX21vZHVsZXMvbG9kYXNoL2lzQXJndW1lbnRzLmpzIiwid2VicGFjazovL2pzLXV0aWxzL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL2xvZGFzaC9pc0FycmF5LmpzIiwid2VicGFjazovL2pzLXV0aWxzL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL2xvZGFzaC9pc0FycmF5TGlrZS5qcyIsIndlYnBhY2s6Ly9qcy11dGlscy9EOi9Vc2Vycy9kYW5pL0Rlc2t0b3AvQG1vbm9ncmlkL25vZGVfbW9kdWxlcy9sb2Rhc2gvaXNCdWZmZXIuanMiLCJ3ZWJwYWNrOi8vanMtdXRpbHMvRDovVXNlcnMvZGFuaS9EZXNrdG9wL0Btb25vZ3JpZC9ub2RlX21vZHVsZXMvbG9kYXNoL2lzRnVuY3Rpb24uanMiLCJ3ZWJwYWNrOi8vanMtdXRpbHMvRDovVXNlcnMvZGFuaS9EZXNrdG9wL0Btb25vZ3JpZC9ub2RlX21vZHVsZXMvbG9kYXNoL2lzTGVuZ3RoLmpzIiwid2VicGFjazovL2pzLXV0aWxzL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL2xvZGFzaC9pc09iamVjdC5qcyIsIndlYnBhY2s6Ly9qcy11dGlscy9EOi9Vc2Vycy9kYW5pL0Rlc2t0b3AvQG1vbm9ncmlkL25vZGVfbW9kdWxlcy9sb2Rhc2gvaXNPYmplY3RMaWtlLmpzIiwid2VicGFjazovL2pzLXV0aWxzL0Q6L1VzZXJzL2RhbmkvRGVza3RvcC9AbW9ub2dyaWQvbm9kZV9tb2R1bGVzL2xvZGFzaC9pc1R5cGVkQXJyYXkuanMiLCJ3ZWJwYWNrOi8vanMtdXRpbHMvRDovVXNlcnMvZGFuaS9EZXNrdG9wL0Btb25vZ3JpZC9ub2RlX21vZHVsZXMvbG9kYXNoL2tleXMuanMiLCJ3ZWJwYWNrOi8vanMtdXRpbHMvRDovVXNlcnMvZGFuaS9EZXNrdG9wL0Btb25vZ3JpZC9ub2RlX21vZHVsZXMvbG9kYXNoL3N0dWJGYWxzZS5qcyIsIndlYnBhY2s6Ly9qcy11dGlscy9EOi9Vc2Vycy9kYW5pL0Rlc2t0b3AvQG1vbm9ncmlkL25vZGVfbW9kdWxlcy9tb2JpbGUtZGV0ZWN0L21vYmlsZS1kZXRlY3QuanMiLCJ3ZWJwYWNrOi8vanMtdXRpbHMvKHdlYnBhY2spL2J1aWxkaW4vYW1kLWRlZmluZS5qcyIsIndlYnBhY2s6Ly9qcy11dGlscy8od2VicGFjaykvYnVpbGRpbi9nbG9iYWwuanMiLCJ3ZWJwYWNrOi8vanMtdXRpbHMvKHdlYnBhY2spL2J1aWxkaW4vbW9kdWxlLmpzIiwid2VicGFjazovL2pzLXV0aWxzLy4vc3JjL0VsYXN0aWNOdW1iZXIuanMiLCJ3ZWJwYWNrOi8vanMtdXRpbHMvLi9zcmMvRmlsZVV0aWxzLmpzIiwid2VicGFjazovL2pzLXV0aWxzLy4vc3JjL0d5cm9zY29wZS5qcyIsIndlYnBhY2s6Ly9qcy11dGlscy8uL3NyYy9OdW1iZXJVdGlscy5qcyIsIndlYnBhY2s6Ly9qcy11dGlscy8uL3NyYy9PYmplY3RVdGlscy5qcyIsIndlYnBhY2s6Ly9qcy11dGlscy8uL3NyYy9QbGF0Zm9ybS5qcyIsIndlYnBhY2s6Ly9qcy11dGlscy8uL3NyYy9SQUYuanMiLCJ3ZWJwYWNrOi8vanMtdXRpbHMvLi9zcmMvVnVlVXRpbHMuanMiLCJ3ZWJwYWNrOi8vanMtdXRpbHMvLi9zcmMvanMtdXRpbHMuanMiLCJ3ZWJwYWNrOi8vanMtdXRpbHMvLi9zcmMvbm9ybWFsaXplV2hlZWwuanMiXSwibmFtZXMiOlsiRWxhc3RpY051bWJlciIsInZhbHVlIiwidGFyZ2V0Iiwic3BlZWQiLCJkZWx0YSIsImRpc3QiLCJkb3dubG9hZEZpbGUiLCJjb250ZW50IiwiZmlsZU5hbWUiLCJjb250ZW50VHlwZSIsImEiLCJkb2N1bWVudCIsImNyZWF0ZUVsZW1lbnQiLCJmaWxlIiwiQmxvYiIsInR5cGUiLCJocmVmIiwiVVJMIiwiY3JlYXRlT2JqZWN0VVJMIiwiZG93bmxvYWQiLCJjbGljayIsImxvYWRGaWxlIiwiYWNjZXB0IiwiUHJvbWlzZSIsInJlc29sdmUiLCJyZWplY3QiLCJpbnB1dCIsIm9uY2hhbmdlIiwiZXZlbnQiLCJmaWxlcyIsImxvYWRKU09OIiwidGhlbiIsImZyIiwiRmlsZVJlYWRlciIsIm9ubG9hZCIsIkpTT04iLCJwYXJzZSIsInJlc3VsdCIsInJlYWRBc1RleHQiLCJjYXRjaCIsImUiLCJsb2FkSW1hZ2UiLCJpbWciLCJJbWFnZSIsInNyYyIsInJlYWRBc0RhdGFVUkwiLCJyZXF1aXJlIiwiR3lyb3Njb3BlIiwib3JpZW50YXRpb25YIiwib3JpZW50YXRpb25ZIiwiZW5hYmxlZCIsIl91cGRhdGUiLCJiaW5kIiwiUkFGIiwiYWRkIiwiZnVsbHRpbHRQcm9taXNlIiwid2luZG93IiwiRlVMTFRJTFQiLCJnZXREZXZpY2VPcmllbnRhdGlvbiIsImRldmljZU9yaWVudGF0aW9uIiwiZXVsZXIiLCJnZXRTY3JlZW5BZGp1c3RlZEV1bGVyIiwiYmV0YSIsImdhbW1hIiwibWVzc2FnZSIsImNvbnNvbGUiLCJ3YXJuIiwicmVtb3ZlIiwibGVycE51bWJlciIsInYwIiwidjEiLCJ0IiwicmFuZ2UiLCJvbGRWYWx1ZSIsIm9sZE1pbiIsIm9sZE1heCIsIm5ld01pbiIsIm5ld01heCIsImNsYW1wZWQiLCJvbGRSYW5nZSIsIm5ld1JhbmdlIiwibmV3VmFsdWUiLCJjbGFtcCIsIm1pbiIsIm1heCIsIk1hdGgiLCJtYXAiLCJhbmdsZURpc3RhbmNlIiwiYWxwaGEiLCJwaGkiLCJhYnMiLCJQSSIsImFuZ2xlRGlzdGFuY2VTaWduIiwic2lnbiIsInNtb290aHN0ZXAiLCJ4IiwibWl4IiwidmFsdWUxIiwidmFsdWUyIiwicGVyY2VudCIsInJhbmRCeVBvd2VyIiwicG93ZXIiLCJyZCIsInJhbmRvbSIsIm1hcFRvUG93ZXIiLCJ2YWx1ZVNpZ24iLCJwb3ciLCJkaXN0YW5jZSIsIngxIiwieTEiLCJ4MiIsInkyIiwiZHgiLCJkeSIsInNxcnQiLCJkaXN0YW5jZUNvbXBhcmUiLCJpc1BsYWluT2JqZWN0IiwibyIsImNvbnN0cnVjdG9yIiwiT2JqZWN0IiwiTW9iaWxlRGV0ZWN0IiwibWQiLCJuYXZpZ2F0b3IiLCJ1c2VyQWdlbnQiLCJ2b2x1bWVUZXN0Iiwidm9sdW1lIiwiUGxhdGZvcm0iLCJtb2JpbGUiLCJwaG9uZSIsInRhYmxldCIsImFuZHJvaWQiLCJpcyIsImlvcyIsImlwYWQiLCJpcGhvbmUiLCJ3cGhvbmUiLCJlZGdlIiwidGVzdCIsImZpcmVmb3giLCJ2ZXJzaW9uIiwiaWUxMSIsInNhZmFyaSIsInZlbmRvciIsInByZXJlbmRlcmVyIiwiX19QUkVSRU5ERVJfSU5KRUNURUQiLCJ1bmRlZmluZWQiLCJNb2Rlcm5penIiLCJrZXkiLCJhZGRUZXN0IiwibGlzdGVuZXJzIiwibGlzdGVuZXIiLCJpbmRleCIsImluZGV4T2YiLCJwdXNoIiwic3BsaWNlIiwiaSIsImxlbmd0aCIsInJlcXVlc3RBbmltYXRpb25GcmFtZSIsImRlZmluZVJlYWN0aXZlIiwibW9kZWwiLCJ2YWx1ZUtleXMiLCJrZXlzIiwibW9kZWxLZXlzIiwiZm9yRWFjaCIsImRlZmluZVByb3BlcnR5IiwiY29uZmlndXJhYmxlIiwiRmlsZVV0aWxzIiwiZnUiLCJOdW1iZXJVdGlscyIsIm51IiwiVnVlVXRpbHMiLCJ2dSIsIk9iamVjdFV0aWxzIiwib3UiLCJQSVhFTF9TVEVQIiwiTElORV9IRUlHSFQiLCJQQUdFX0hFSUdIVCIsIm5vcm1hbGl6ZVdoZWVsIiwic1giLCJzWSIsInBYIiwicFkiLCJkZXRhaWwiLCJ3aGVlbERlbHRhIiwid2hlZWxEZWx0YVkiLCJ3aGVlbERlbHRhWCIsImF4aXMiLCJIT1JJWk9OVEFMX0FYSVMiLCJkZWx0YVkiLCJkZWx0YVgiLCJkZWx0YU1vZGUiLCJzcGluWCIsInNwaW5ZIiwicGl4ZWxYIiwicGl4ZWxZIl0sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDO0FBQ0QsTztRQ1ZBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBOzs7UUFHQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0EsMENBQTBDLGdDQUFnQztRQUMxRTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLHdEQUF3RCxrQkFBa0I7UUFDMUU7UUFDQSxpREFBaUQsY0FBYztRQUMvRDs7UUFFQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0EseUNBQXlDLGlDQUFpQztRQUMxRSxnSEFBZ0gsbUJBQW1CLEVBQUU7UUFDckk7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSwyQkFBMkIsMEJBQTBCLEVBQUU7UUFDdkQsaUNBQWlDLGVBQWU7UUFDaEQ7UUFDQTtRQUNBOztRQUVBO1FBQ0Esc0RBQXNELCtEQUErRDs7UUFFckg7UUFDQTs7O1FBR0E7UUFDQTs7Ozs7Ozs7Ozs7O0FDbEZBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsaUM7Ozs7Ozs7Ozs7O0FDTkE7QUFDQSxpQkFBaUIsa0JBQWtCO0FBQ25DO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsOEI7Ozs7Ozs7Ozs7O0FDaEJBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSx5Qjs7Ozs7Ozs7Ozs7QUNoQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsRUFBRTtBQUNGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxRQUFRO0FBQ1I7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsS0FBSzs7QUFFTDs7QUFFQSxLQUFLOztBQUVMOztBQUVBOztBQUVBLElBQUk7O0FBRUo7O0FBRUE7O0FBRUEsRUFBRTs7QUFFRjs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxFQUFFOztBQUVGOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxHQUFHOztBQUVIO0FBQ0E7O0FBRUEsR0FBRzs7QUFFSCxFQUFFOztBQUVGOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLEdBQUc7O0FBRUg7QUFDQTs7QUFFQSxHQUFHOztBQUVILEVBQUU7O0FBRUY7O0FBRUE7OztBQUdBOztBQUVBOztBQUVBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsRUFBRTs7QUFFRjs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUEsRUFBRTs7QUFFRjs7QUFFQTtBQUNBOztBQUVBOztBQUVBOztBQUVBOztBQUVBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUEsRUFBRTs7QUFFRjs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxHQUFHOztBQUVIOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBOztBQUVBLEVBQUU7O0FBRUY7O0FBRUE7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBOztBQUVBOztBQUVBLEVBQUU7O0FBRUY7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBOztBQUVBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLEVBQUU7O0FBRUY7O0FBRUE7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBOztBQUVBLEVBQUU7O0FBRUY7O0FBRUE7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBOztBQUVBOztBQUVBOztBQUVBOztBQUVBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBOztBQUVBLEVBQUU7O0FBRUY7O0FBRUE7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQSxFQUFFOztBQUVGOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTs7QUFFQSw4QkFBOEI7O0FBRTlCOztBQUVBO0FBQ0E7O0FBRUEsaUVBQWlFOztBQUVqRTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxLQUFLLG9FQUFvRTs7QUFFekU7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsS0FBSyxvRUFBb0U7O0FBRXpFOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLElBQUk7O0FBRUo7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsRUFBRTs7QUFFRjs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUE7QUFDQTtBQUNBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLGtCQUFrQjs7QUFFbEI7QUFDQSw2QkFBNkI7QUFDN0IscUNBQXFDOztBQUVyQyxJQUFJLHFCQUFxQjs7QUFFekI7QUFDQTtBQUNBLDJDQUEyQztBQUMzQyxxQ0FBcUM7O0FBRXJDLElBQUksT0FBTzs7QUFFWCxtQkFBbUI7O0FBRW5CO0FBQ0EsOEJBQThCO0FBQzlCLHVCQUF1Qjs7QUFFdkIsS0FBSyxxQkFBcUI7O0FBRTFCO0FBQ0E7QUFDQSw0Q0FBNEM7QUFDNUMsdUJBQXVCOztBQUV2QixLQUFLLE9BQU87O0FBRVo7QUFDQTtBQUNBLDZDQUE2QztBQUM3QyxnQkFBZ0I7O0FBRWhCOztBQUVBOztBQUVBO0FBQ0E7QUFDQSxxQkFBcUI7QUFDckI7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTs7QUFFQSxFQUFFOztBQUVGOztBQUVBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLDBDQUEwQztBQUMxQztBQUNBLHNCQUFzQjs7QUFFdEI7O0FBRUE7QUFDQTtBQUNBOztBQUVBLElBQUk7O0FBRUo7QUFDQTtBQUNBOztBQUVBLElBQUk7O0FBRUo7QUFDQTs7QUFFQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQSxLQUFLOztBQUVMO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBOztBQUVBO0FBQ0E7QUFDQSxxQkFBcUI7QUFDckI7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTs7QUFFQSxFQUFFOztBQUVGOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxFQUFFOztBQUVGOztBQUVBOztBQUVBOztBQUVBLDhCQUE4Qjs7QUFFOUI7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBOztBQUVBLDRCQUE0QjtBQUM1QjtBQUNBOztBQUVBLHNCQUFzQixpQkFBaUI7QUFDdkM7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUEsR0FBRzs7QUFFSDs7QUFFQTtBQUNBLEVBQUU7O0FBRUY7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsc0JBQXNCLGlCQUFpQjtBQUN2QztBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQSxHQUFHOztBQUVIOztBQUVBLEVBQUU7O0FBRUY7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsSUFBSTs7QUFFSjs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxFQUFFOztBQUVGOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLEVBQUU7O0FBRUY7O0FBRUE7O0FBRUEsRUFBRTs7QUFFRjs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUEsc0RBQXNEOztBQUV0RDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLEVBQUU7O0FBRUY7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQSxFQUFFOztBQUVGOztBQUVBO0FBQ0E7O0FBRUE7O0FBRUEsc0RBQXNEOztBQUV0RDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLEVBQUU7O0FBRUY7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQSxFQUFFOztBQUVGOztBQUVBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsRUFBRTs7QUFFRjs7QUFFQTtBQUNBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLEVBQUU7O0FBRUY7O0FBRUE7QUFDQTtBQUNBOztBQUVBOztBQUVBLEVBQUU7O0FBRUY7O0FBRUE7O0FBRUEsRUFBRTs7QUFFRjtBQUNBO0FBQ0E7O0FBRUE7OztBQUdBOztBQUVBOztBQUVBLDhCQUE4Qjs7QUFFOUI7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsSUFBSTs7QUFFSjs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxFQUFFOztBQUVGOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLEVBQUU7O0FBRUY7O0FBRUE7O0FBRUEsRUFBRTs7QUFFRjs7QUFFQSw4R0FBOEc7QUFDOUc7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUEsRUFBRTs7QUFFRjs7QUFFQSwrSUFBK0k7QUFDL0k7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUEsRUFBRTs7QUFFRjs7QUFFQSxrSEFBa0g7QUFDbEg7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUEsRUFBRTs7QUFFRjs7QUFFQTs7QUFFQTs7QUFFQTs7O0FBR0E7O0FBRUE7O0FBRUEsQ0FBQyxZOzs7Ozs7Ozs7OztBQ3QyQ0QsV0FBVyxtQkFBTyxDQUFDLG1EQUFTOztBQUU1QjtBQUNBOztBQUVBOzs7Ozs7Ozs7Ozs7QUNMQSxnQkFBZ0IsbUJBQU8sQ0FBQyw2REFBYztBQUN0QyxrQkFBa0IsbUJBQU8sQ0FBQywrREFBZTtBQUN6QyxjQUFjLG1CQUFPLENBQUMsdURBQVc7QUFDakMsZUFBZSxtQkFBTyxDQUFDLHlEQUFZO0FBQ25DLGNBQWMsbUJBQU8sQ0FBQyx5REFBWTtBQUNsQyxtQkFBbUIsbUJBQU8sQ0FBQyxpRUFBZ0I7O0FBRTNDO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVcsRUFBRTtBQUNiLFdBQVcsUUFBUTtBQUNuQixhQUFhLE1BQU07QUFDbkI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7Ozs7Ozs7OztBQ2hEQSxhQUFhLG1CQUFPLENBQUMsdURBQVc7QUFDaEMsZ0JBQWdCLG1CQUFPLENBQUMsNkRBQWM7QUFDdEMscUJBQXFCLG1CQUFPLENBQUMsdUVBQW1COztBQUVoRDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVcsRUFBRTtBQUNiLGFBQWEsT0FBTztBQUNwQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7Ozs7Ozs7OztBQzNCQSxpQkFBaUIsbUJBQU8sQ0FBQywrREFBZTtBQUN4QyxtQkFBbUIsbUJBQU8sQ0FBQyxpRUFBZ0I7O0FBRTNDO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXLEVBQUU7QUFDYixhQUFhLFFBQVE7QUFDckI7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7Ozs7Ozs7OztBQ2pCQSxpQkFBaUIsbUJBQU8sQ0FBQywrREFBZTtBQUN4QyxlQUFlLG1CQUFPLENBQUMseURBQVk7QUFDbkMsbUJBQW1CLG1CQUFPLENBQUMsaUVBQWdCOztBQUUzQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVyxFQUFFO0FBQ2IsYUFBYSxRQUFRO0FBQ3JCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7Ozs7Ozs7OztBQzNEQSxrQkFBa0IsbUJBQU8sQ0FBQyxpRUFBZ0I7QUFDMUMsaUJBQWlCLG1CQUFPLENBQUMsK0RBQWU7O0FBRXhDO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVcsT0FBTztBQUNsQixhQUFhLE1BQU07QUFDbkI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7Ozs7Ozs7OztBQzdCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVyxPQUFPO0FBQ2xCLFdBQVcsU0FBUztBQUNwQixhQUFhLE1BQU07QUFDbkI7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7Ozs7Ozs7O0FDbkJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVyxTQUFTO0FBQ3BCLGFBQWEsU0FBUztBQUN0QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7Ozs7Ozs7OztBQ2JBO0FBQ0E7O0FBRUE7Ozs7Ozs7Ozs7Ozs7QUNIQSxhQUFhLG1CQUFPLENBQUMsdURBQVc7O0FBRWhDO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVcsRUFBRTtBQUNiLGFBQWEsT0FBTztBQUNwQjtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxHQUFHOztBQUVIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7Ozs7Ozs7OztBQzdDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXLEVBQUU7QUFDYixXQUFXLE9BQU87QUFDbEIsYUFBYSxRQUFRO0FBQ3JCO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7Ozs7Ozs7OztBQ3hCQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVyxFQUFFO0FBQ2IsYUFBYSxRQUFRO0FBQ3JCO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7Ozs7Ozs7Ozs7OztBQ2pCQSxjQUFjLG1CQUFPLENBQUMseURBQVk7O0FBRWxDO0FBQ0E7O0FBRUE7Ozs7Ozs7Ozs7OztBQ0xBLCtEQUFpQixtQkFBTyxDQUFDLCtEQUFlOztBQUV4QztBQUNBLGtCQUFrQixLQUEwQjs7QUFFNUM7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLEdBQUc7QUFDSCxDQUFDOztBQUVEOzs7Ozs7Ozs7Ozs7O0FDN0JBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVyxFQUFFO0FBQ2IsYUFBYSxPQUFPO0FBQ3BCO0FBQ0E7QUFDQTtBQUNBOztBQUVBOzs7Ozs7Ozs7Ozs7QUNyQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXLFNBQVM7QUFDcEIsV0FBVyxTQUFTO0FBQ3BCLGFBQWEsU0FBUztBQUN0QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7Ozs7Ozs7OztBQ2RBLGlCQUFpQixtQkFBTyxDQUFDLCtEQUFlOztBQUV4QztBQUNBOztBQUVBO0FBQ0E7O0FBRUE7Ozs7Ozs7Ozs7OztBQ1JBLHNCQUFzQixtQkFBTyxDQUFDLHlFQUFvQjtBQUNsRCxtQkFBbUIsbUJBQU8sQ0FBQyxpRUFBZ0I7O0FBRTNDO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVyxFQUFFO0FBQ2IsYUFBYSxRQUFRO0FBQ3JCO0FBQ0E7QUFDQTtBQUNBLDZCQUE2QixrQkFBa0IsRUFBRTtBQUNqRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsOENBQThDLGtCQUFrQixFQUFFO0FBQ2xFO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7Ozs7Ozs7O0FDbkNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVyxFQUFFO0FBQ2IsYUFBYSxRQUFRO0FBQ3JCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7Ozs7Ozs7O0FDekJBLGlCQUFpQixtQkFBTyxDQUFDLDZEQUFjO0FBQ3ZDLGVBQWUsbUJBQU8sQ0FBQyx5REFBWTs7QUFFbkM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVyxFQUFFO0FBQ2IsYUFBYSxRQUFRO0FBQ3JCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7Ozs7Ozs7OztBQ2hDQSx5REFBVyxtQkFBTyxDQUFDLG1EQUFTO0FBQzVCLGdCQUFnQixtQkFBTyxDQUFDLDJEQUFhOztBQUVyQztBQUNBLGtCQUFrQixLQUEwQjs7QUFFNUM7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVcsRUFBRTtBQUNiLGFBQWEsUUFBUTtBQUNyQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7Ozs7Ozs7Ozs7QUNyQ0EsaUJBQWlCLG1CQUFPLENBQUMsK0RBQWU7QUFDeEMsZUFBZSxtQkFBTyxDQUFDLHlEQUFZOztBQUVuQztBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVyxFQUFFO0FBQ2IsYUFBYSxRQUFRO0FBQ3JCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7Ozs7Ozs7OztBQ3BDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVyxFQUFFO0FBQ2IsYUFBYSxRQUFRO0FBQ3JCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7Ozs7Ozs7O0FDbENBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVcsRUFBRTtBQUNiLGFBQWEsUUFBUTtBQUNyQjtBQUNBO0FBQ0EsZ0JBQWdCO0FBQ2hCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7Ozs7Ozs7O0FDOUJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXLEVBQUU7QUFDYixhQUFhLFFBQVE7QUFDckI7QUFDQTtBQUNBLG9CQUFvQjtBQUNwQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOzs7Ozs7Ozs7Ozs7QUM1QkEsdUJBQXVCLG1CQUFPLENBQUMsMkVBQXFCO0FBQ3BELGdCQUFnQixtQkFBTyxDQUFDLDZEQUFjO0FBQ3RDLGVBQWUsbUJBQU8sQ0FBQywyREFBYTs7QUFFcEM7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVcsRUFBRTtBQUNiLGFBQWEsUUFBUTtBQUNyQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7Ozs7Ozs7OztBQzFCQSxvQkFBb0IsbUJBQU8sQ0FBQyxxRUFBa0I7QUFDOUMsZUFBZSxtQkFBTyxDQUFDLDJEQUFhO0FBQ3BDLGtCQUFrQixtQkFBTyxDQUFDLCtEQUFlOztBQUV6QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVyxPQUFPO0FBQ2xCLGFBQWEsTUFBTTtBQUNuQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7Ozs7Ozs7OztBQ3BDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWEsUUFBUTtBQUNyQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOzs7Ozs7Ozs7Ozs7QUNqQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG9TQUFvUztBQUNwUztBQUNBLHVCQUF1QjtBQUN2QjtBQUNBO0FBQ0Esd0JBQXdCO0FBQ3hCO0FBQ0E7QUFDQSxtQ0FBbUMsSUFBSTtBQUN2QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esc0NBQXNDO0FBQ3RDLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSw2Q0FBNkMsS0FBSztBQUNsRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQ0FBaUM7QUFDakM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxRUFBcUUsRUFBRSxPQUFPLEVBQUUsMkNBQTJDO0FBQzNILG9JQUFvSSxLQUFLO0FBQ3pJO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSw2QkFBNkI7QUFDN0I7QUFDQSx3QkFBd0I7QUFDeEIsOEJBQThCO0FBQzlCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxvQ0FBb0MsRUFBRSxFQUFFO0FBQ3hDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDBCQUEwQjtBQUMxQjtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSwwQ0FBMEMsbUVBQW1FOztBQUU3RztBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsbUJBQW1CLFNBQVM7QUFDNUI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0EsZ0RBQWdEO0FBQ2hEOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDJCQUEyQixTQUFTO0FBQ3BDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSzs7QUFFTDtBQUNBO0FBQ0EsZUFBZSxPQUFPO0FBQ3RCLGVBQWUsT0FBTztBQUN0QixpQkFBaUIsWUFBWTtBQUM3QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLGVBQWUsT0FBTztBQUN0QixlQUFlLE9BQU87QUFDdEIsaUJBQWlCLE1BQU07QUFDdkI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsZUFBZSxPQUFPO0FBQ3RCLGVBQWUsT0FBTztBQUN0QixnQkFBZ0IsT0FBTztBQUN2QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHVCQUF1QixTQUFTO0FBQ2hDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxlQUFlLE9BQU87QUFDdEIsZUFBZSxPQUFPO0FBQ3RCLGdCQUFnQixPQUFPO0FBQ3ZCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxlQUFlLE9BQU87QUFDdEIsZ0JBQWdCLE9BQU87QUFDdkI7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsbUJBQW1CO0FBQ25COztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsbUJBQW1CO0FBQ25COztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWE7QUFDYjtBQUNBO0FBQ0EsYUFBYTtBQUNiO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSw2Q0FBNkMsMkJBQTJCLGdCQUFnQixlQUFlLG9CQUFvQjtBQUMzSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZUFBZSxPQUFPO0FBQ3RCLGVBQWUsT0FBTztBQUN0QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxxREFBcUQseUJBQXlCLE1BQU0sMEJBQTBCO0FBQzlHO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsWUFBWSwwQkFBMEIsV0FBVyx5QkFBeUIsTUFBTSwwQkFBMEI7QUFDMUc7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxQkFBcUIsT0FBTztBQUM1QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUzs7QUFFVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsMEZBQTBGO0FBQzFGO0FBQ0E7QUFDQSxZQUFZLDBCQUEwQixXQUFXLHlCQUF5QixNQUFNLDBCQUEwQjtBQUMxRztBQUNBO0FBQ0E7QUFDQTtBQUNBLHFCQUFxQixPQUFPO0FBQzVCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTOztBQUVUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsMEZBQTBGO0FBQzFGO0FBQ0E7QUFDQSxZQUFZLDBCQUEwQixXQUFXLHlCQUF5QixNQUFNLDBCQUEwQjtBQUMxRztBQUNBO0FBQ0E7QUFDQTtBQUNBLHFCQUFxQixPQUFPO0FBQzVCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTOztBQUVUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxrQ0FBa0MsNkJBQTZCO0FBQy9EO0FBQ0EscUNBQXFDLDhCQUE4QjtBQUNuRSxtRUFBbUUsc0JBQXNCO0FBQ3pGO0FBQ0EscUJBQXFCLE9BQU87QUFDNUI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTOztBQUVUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxrQ0FBa0MsNkJBQTZCO0FBQy9EO0FBQ0EscUNBQXFDLDhCQUE4QjtBQUNuRSxtRUFBbUUsc0JBQXNCO0FBQ3pGO0FBQ0EscUJBQXFCLE1BQU07QUFDM0I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTOztBQUVUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscUJBQXFCLE9BQU87QUFDNUI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTOztBQUVUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxtQkFBbUIsT0FBTztBQUMxQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscUJBQXFCLE9BQU87QUFDNUI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7O0FBRVQ7QUFDQTtBQUNBO0FBQ0E7QUFDQSxtQkFBbUIsT0FBTztBQUMxQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscUJBQXFCLE9BQU87QUFDNUI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7O0FBRVQ7QUFDQTtBQUNBO0FBQ0EsbUJBQW1CLE9BQU87QUFDMUI7QUFDQSwrREFBK0QsNkJBQTZCO0FBQzVGLG1CQUFtQixzQkFBc0IsR0FBRyx5QkFBeUIsR0FBRywwQkFBMEI7QUFDbEc7QUFDQTtBQUNBO0FBQ0EscUJBQXFCLFFBQVE7QUFDN0I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUzs7QUFFVDtBQUNBO0FBQ0E7QUFDQSxtQkFBbUIsY0FBYztBQUNqQztBQUNBLHFCQUFxQixRQUFRO0FBQzdCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUzs7QUFFVDtBQUNBO0FBQ0E7QUFDQTtBQUNBLG1CQUFtQixPQUFPO0FBQzFCO0FBQ0EscUJBQXFCLGtCQUFrQjtBQUN2QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUzs7QUFFVDtBQUNBO0FBQ0E7QUFDQSxxQkFBcUIsT0FBTztBQUM1QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQSxDQUFDLEVBQUU7QUFDSCxDQUFDO0FBQ0QsUUFBUSxLQUE2QjtBQUNyQyxtQ0FBbUMsNEJBQTRCO0FBQy9ELEtBQUssVUFBVSxJQUEwQztBQUN6RCxlQUFlLGtHQUFNO0FBQ3JCLEtBQUssTUFBTSxFQUtOO0FBQ0wsQ0FBQyxLOzs7Ozs7Ozs7OztBQzMrQkQ7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7QUNGQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxDQUFDOztBQUVEO0FBQ0E7QUFDQTtBQUNBLENBQUM7QUFDRDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLDRDQUE0Qzs7QUFFNUM7Ozs7Ozs7Ozs7OztBQ25CQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNyQkE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7SUF5QnFCQSxhO0FBQ25COzs7Ozs7QUFNQSwyQkFBd0I7QUFBQSxRQUFYQyxLQUFXLHVFQUFILENBQUc7O0FBQUE7O0FBQ3RCLFNBQUtBLEtBQUwsR0FBYUEsS0FBYjtBQUNBLFNBQUtDLE1BQUwsR0FBY0QsS0FBZDtBQUNBLFNBQUtFLEtBQUwsR0FBYSxDQUFiO0FBQ0Q7QUFFRDs7Ozs7Ozs7Ozs7MkJBT1FDLEssRUFBTztBQUNiO0FBRUEsVUFBTUMsSUFBSSxHQUFHLEtBQUtILE1BQUwsR0FBYyxLQUFLRCxLQUFoQztBQUVBLFdBQUtBLEtBQUwsSUFBY0ksSUFBSSxJQUFJLEtBQUtGLEtBQUwsR0FBYUMsS0FBakIsQ0FBbEI7QUFDRDs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDbkRIO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTs7Ozs7O0FBTUE7Ozs7Ozs7QUFPQSxJQUFNRSxZQUFZLEdBQUcsU0FBZkEsWUFBZSxDQUFVQyxPQUFWLEVBQW1CQyxRQUFuQixFQUE2QkMsV0FBN0IsRUFBMEM7QUFDN0QsTUFBTUMsQ0FBQyxHQUFHQyxRQUFRLENBQUNDLGFBQVQsQ0FBdUIsR0FBdkIsQ0FBVjtBQUNBLE1BQU1DLElBQUksR0FBRyxJQUFJQyxJQUFKLENBQVMsQ0FBQ1AsT0FBRCxDQUFULEVBQW9CO0FBQUVRLFFBQUksRUFBRU47QUFBUixHQUFwQixDQUFiO0FBQ0FDLEdBQUMsQ0FBQ00sSUFBRixHQUFTQyxHQUFHLENBQUNDLGVBQUosQ0FBb0JMLElBQXBCLENBQVQ7QUFDQUgsR0FBQyxDQUFDUyxRQUFGLEdBQWFYLFFBQWI7QUFDQUUsR0FBQyxDQUFDVSxLQUFGO0FBQ0QsQ0FORDtBQVFBOzs7Ozs7OztBQU1BLElBQU1DLFFBQVEsR0FBRyxTQUFYQSxRQUFXLENBQVVDLE1BQVYsRUFBa0I7QUFDakMsU0FBTyxJQUFJQyxPQUFKLENBQVksVUFBQ0MsT0FBRCxFQUFVQyxNQUFWLEVBQXFCO0FBQ3RDLFFBQU1DLEtBQUssR0FBR2YsUUFBUSxDQUFDQyxhQUFULENBQXVCLE9BQXZCLENBQWQ7QUFDQWMsU0FBSyxDQUFDWCxJQUFOLEdBQWEsTUFBYjtBQUNBVyxTQUFLLENBQUNKLE1BQU4sR0FBZUEsTUFBZjs7QUFDQUksU0FBSyxDQUFDQyxRQUFOLEdBQWlCLFVBQUNDLEtBQUQsRUFBVztBQUMxQkosYUFBTyxDQUFDSSxLQUFLLENBQUMxQixNQUFOLENBQWEyQixLQUFkLENBQVA7QUFDRCxLQUZEOztBQUdBSCxTQUFLLENBQUNOLEtBQU47QUFDRCxHQVJNLENBQVA7QUFTRCxDQVZEO0FBWUE7Ozs7Ozs7O0FBTUEsSUFBTVUsUUFBUSxHQUFHLFNBQVhBLFFBQVcsR0FBWTtBQUMzQixTQUFPLElBQUlQLE9BQUosQ0FBWSxVQUFDQyxPQUFELEVBQVVDLE1BQVYsRUFBcUI7QUFDdENKLFlBQVEsQ0FBQyxPQUFELENBQVIsQ0FBa0JVLElBQWxCLENBQXVCLFVBQUFGLEtBQUssRUFBSTtBQUM5QixVQUFNRyxFQUFFLEdBQUcsSUFBSUMsVUFBSixFQUFYOztBQUNBRCxRQUFFLENBQUNFLE1BQUgsR0FBWSxVQUFDTixLQUFELEVBQVc7QUFDckJKLGVBQU8sQ0FBQ1csSUFBSSxDQUFDQyxLQUFMLENBQVdSLEtBQUssQ0FBQzFCLE1BQU4sQ0FBYW1DLE1BQXhCLENBQUQsQ0FBUDtBQUNELE9BRkQ7O0FBR0FMLFFBQUUsQ0FBQ00sVUFBSCxDQUFjVCxLQUFLLENBQUMsQ0FBRCxDQUFuQjtBQUNBLGFBQU8sSUFBUDtBQUNELEtBUEQsRUFPR1UsS0FQSCxDQU9TLFVBQUFDLENBQUMsRUFBSTtBQUFFLFlBQU1BLENBQU47QUFBUyxLQVB6QjtBQVFELEdBVE0sQ0FBUDtBQVVELENBWEQ7QUFZQTs7Ozs7Ozs7QUFNQSxJQUFNQyxTQUFTLEdBQUcsU0FBWkEsU0FBWSxHQUFZO0FBQzVCLFNBQU8sSUFBSWxCLE9BQUosQ0FBWSxVQUFDQyxPQUFELEVBQVVDLE1BQVYsRUFBcUI7QUFDdEMsUUFBTWlCLEdBQUcsR0FBRyxJQUFJQyxLQUFKLEVBQVo7O0FBQ0FELE9BQUcsQ0FBQ1IsTUFBSixHQUFhLFVBQUNOLEtBQUQsRUFBVztBQUN0QkosYUFBTyxDQUFDa0IsR0FBRCxDQUFQO0FBQ0QsS0FGRDs7QUFJQXJCLFlBQVEsQ0FBQyxTQUFELENBQVIsQ0FBb0JVLElBQXBCLENBQXlCLFVBQUFGLEtBQUssRUFBSTtBQUNoQyxVQUFNRyxFQUFFLEdBQUcsSUFBSUMsVUFBSixFQUFYOztBQUNBRCxRQUFFLENBQUNFLE1BQUgsR0FBWSxVQUFDTixLQUFELEVBQVc7QUFDckJjLFdBQUcsQ0FBQ0UsR0FBSixHQUFVaEIsS0FBSyxDQUFDMUIsTUFBTixDQUFhbUMsTUFBdkI7QUFDRCxPQUZEOztBQUdBTCxRQUFFLENBQUNhLGFBQUgsQ0FBaUJoQixLQUFLLENBQUMsQ0FBRCxDQUF0QjtBQUNBLGFBQU8sSUFBUDtBQUNELEtBUEQsRUFPR1UsS0FQSCxDQU9TLFVBQUFDLENBQUMsRUFBSTtBQUFFLFlBQU1BLENBQU47QUFBUyxLQVB6QjtBQVFELEdBZE0sQ0FBUDtBQWVELENBaEJEOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDL0RBOztBQUNBTSxtQkFBTyxDQUFDLDRGQUFELENBQVA7QUFDQTs7Ozs7Ozs7Ozs7Ozs7SUFZTUMsUztBQUNKOzs7QUFHQSx1QkFBZTtBQUFBOztBQUNiLFNBQUtDLFlBQUwsR0FBb0IsQ0FBcEI7QUFDQSxTQUFLQyxZQUFMLEdBQW9CLENBQXBCO0FBQ0EsU0FBS0MsT0FBTCxHQUFlLElBQWY7QUFDQSxTQUFLQyxPQUFMLEdBQWUsS0FBS0EsT0FBTCxDQUFhQyxJQUFiLENBQWtCLElBQWxCLENBQWY7QUFDQUMsZ0RBQUcsQ0FBQ0MsR0FBSixDQUFRLEtBQUtILE9BQWI7QUFDRDtBQUVEOzs7Ozs7OzhCQUdXO0FBQUE7O0FBQ1QsVUFBTUksZUFBZSxHQUFHLElBQUlDLE1BQU0sQ0FBQ0MsUUFBUCxDQUFnQkMsb0JBQXBCLENBQXlDO0FBQUUzQyxZQUFJLEVBQUU7QUFBUixPQUF6QyxDQUF4QixDQURTLENBQzBFOztBQUVuRndDLHFCQUFlLENBQUN4QixJQUFoQixDQUFxQixVQUFDNEIsaUJBQUQsRUFBdUI7QUFDMUMsWUFBTUMsS0FBSyxHQUFHRCxpQkFBaUIsQ0FBQ0Usc0JBQWxCLEVBQWQ7QUFFQSxhQUFJLENBQUNiLFlBQUwsR0FBb0JZLEtBQUssQ0FBQ0UsSUFBMUI7QUFDQSxhQUFJLENBQUNiLFlBQUwsR0FBb0JXLEtBQUssQ0FBQ0csS0FBMUI7O0FBRUEsWUFBSSxLQUFJLENBQUNmLFlBQUwsR0FBb0IsRUFBeEIsRUFBNEI7QUFDMUIsZUFBSSxDQUFDQyxZQUFMLEdBQW9CLENBQUMsS0FBSSxDQUFDQSxZQUExQjtBQUNEOztBQUNELGVBQU8sSUFBUCxDQVQwQyxDQVUxQztBQUNELE9BWEQsRUFXR1YsS0FYSCxDQVdTLFVBQUN5QixPQUFELEVBQWE7QUFDcEJDLGVBQU8sQ0FBQ0MsSUFBUixDQUFhLDhCQUFiLEVBQTZDRixPQUE3QztBQUNBLGFBQUksQ0FBQ2QsT0FBTCxHQUFlLEtBQWY7QUFDQUcsb0RBQUcsQ0FBQ2MsTUFBSixDQUFXLEtBQUksQ0FBQ2hCLE9BQWhCO0FBQ0QsT0FmRDtBQWdCRDs7Ozs7QUFFSDs7Ozs7QUFHZUosd0VBQWYsRTs7Ozs7Ozs7Ozs7O0FDckRBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7Ozs7OztBQU1BOzs7Ozs7OztBQVFBLElBQU1xQixVQUFVLEdBQUcsU0FBYkEsVUFBYSxDQUFVQyxFQUFWLEVBQWNDLEVBQWQsRUFBa0JDLENBQWxCLEVBQXFCO0FBQ3RDLFNBQU9GLEVBQUUsR0FBR0UsQ0FBQyxJQUFJRCxFQUFFLEdBQUdELEVBQVQsQ0FBYjtBQUNELENBRkQ7QUFJQTs7Ozs7Ozs7Ozs7O0FBVUEsSUFBTUcsS0FBSyxHQUFHLFNBQVJBLEtBQVEsQ0FBVUMsUUFBVixFQUFvQkMsTUFBcEIsRUFBNEJDLE1BQTVCLEVBQW9DQyxNQUFwQyxFQUE0Q0MsTUFBNUMsRUFBb0RDLE9BQXBELEVBQTZEO0FBQ3pFLE1BQU1DLFFBQVEsR0FBR0osTUFBTSxHQUFHRCxNQUExQjtBQUNBLE1BQU1NLFFBQVEsR0FBR0gsTUFBTSxHQUFHRCxNQUExQjtBQUNBLE1BQU1LLFFBQVEsR0FBSSxDQUFDUixRQUFRLEdBQUdDLE1BQVosSUFBc0JNLFFBQXZCLEdBQW1DRCxRQUFuQyxHQUE4Q0gsTUFBL0Q7O0FBRUEsTUFBSUUsT0FBSixFQUFhO0FBQ1gsV0FBT0ksS0FBSyxDQUFDRCxRQUFELEVBQVdMLE1BQVgsRUFBbUJDLE1BQW5CLENBQVo7QUFDRDs7QUFFRCxTQUFPSSxRQUFQO0FBQ0QsQ0FWRDtBQVdBOzs7Ozs7Ozs7O0FBUUEsSUFBTUMsS0FBSyxHQUFHLFNBQVJBLEtBQVEsQ0FBQ2pGLEtBQUQ7QUFBQSxNQUFRa0YsR0FBUix1RUFBYyxDQUFkO0FBQUEsTUFBaUJDLEdBQWpCLHVFQUF1QixDQUF2QjtBQUFBLFNBQTZCQyxJQUFJLENBQUNELEdBQUwsQ0FBU0QsR0FBVCxFQUFjRSxJQUFJLENBQUNGLEdBQUwsQ0FBU2xGLEtBQVQsRUFBZ0JtRixHQUFoQixDQUFkLENBQTdCO0FBQUEsQ0FBZDtBQUNBOzs7Ozs7Ozs7QUFPQSxJQUFNRSxHQUFHLEdBQUcsU0FBTkEsR0FBTSxDQUFDckYsS0FBRCxFQUFRa0YsR0FBUixFQUFhQyxHQUFiO0FBQUEsU0FBcUJGLEtBQUssQ0FBQyxDQUFDakYsS0FBSyxHQUFHa0YsR0FBVCxLQUFpQkMsR0FBRyxHQUFHRCxHQUF2QixDQUFELEVBQThCLENBQTlCLEVBQWlDLENBQWpDLENBQTFCO0FBQUEsQ0FBWjtBQUNBOzs7Ozs7OztBQU1BLElBQU1JLGFBQWEsR0FBRyxTQUFoQkEsYUFBZ0IsQ0FBVUMsS0FBVixFQUFpQjFCLElBQWpCLEVBQXVCO0FBQzNDLE1BQU0yQixHQUFHLEdBQUlKLElBQUksQ0FBQ0ssR0FBTCxDQUFTNUIsSUFBSSxHQUFHMEIsS0FBaEIsSUFBeUJILElBQUksQ0FBQ00sRUFBL0IsR0FBcUMsQ0FBakQ7QUFDQSxTQUFPRixHQUFHLEdBQUdKLElBQUksQ0FBQ00sRUFBWCxHQUFnQk4sSUFBSSxDQUFDTSxFQUFMLEdBQVUsQ0FBVixHQUFjRixHQUE5QixHQUFvQ0EsR0FBM0M7QUFDRCxDQUhEO0FBSUE7Ozs7Ozs7O0FBTUEsSUFBTUcsaUJBQWlCLEdBQUcsU0FBcEJBLGlCQUFvQixDQUFVSixLQUFWLEVBQWlCMUIsSUFBakIsRUFBdUI7QUFDL0MsTUFBTStCLElBQUksR0FDUEwsS0FBSyxHQUFHMUIsSUFBUixJQUFnQixDQUFoQixJQUFxQjBCLEtBQUssR0FBRzFCLElBQVIsSUFBZ0J1QixJQUFJLENBQUNNLEVBQTNDLElBQ0NILEtBQUssR0FBRzFCLElBQVIsSUFBZ0IsQ0FBQ3VCLElBQUksQ0FBQ00sRUFBdEIsSUFBNEJILEtBQUssR0FBRzFCLElBQVIsSUFBZ0IsQ0FBQ3VCLElBQUksQ0FBQ00sRUFBTixHQUFXLENBRHhELEdBRUksQ0FGSixHQUdJLENBQUMsQ0FKUDtBQUtBLFNBQU9KLGFBQWEsQ0FBQ0MsS0FBRCxFQUFRMUIsSUFBUixDQUFiLEdBQTZCK0IsSUFBcEM7QUFDRCxDQVBEO0FBUUE7Ozs7Ozs7Ozs7O0FBU0EsSUFBTUMsVUFBVSxHQUFHLFNBQWJBLFVBQWEsQ0FBQ1gsR0FBRCxFQUFNQyxHQUFOLEVBQVduRixLQUFYLEVBQXFCO0FBQ3RDLE1BQU04RixDQUFDLEdBQUdWLElBQUksQ0FBQ0QsR0FBTCxDQUFTLENBQVQsRUFBWUMsSUFBSSxDQUFDRixHQUFMLENBQVMsQ0FBVCxFQUFZLENBQUNsRixLQUFLLEdBQUdrRixHQUFULEtBQWlCQyxHQUFHLEdBQUdELEdBQXZCLENBQVosQ0FBWixDQUFWO0FBQ0EsU0FBT1ksQ0FBQyxHQUFHQSxDQUFKLElBQVMsSUFBSSxJQUFJQSxDQUFqQixDQUFQO0FBQ0QsQ0FIRDtBQUlBOzs7Ozs7Ozs7QUFPQSxJQUFNQyxHQUFHLEdBQUcsU0FBTkEsR0FBTSxDQUFDQyxNQUFELEVBQVNDLE1BQVQsRUFBaUJDLE9BQWpCO0FBQUEsU0FBNkJGLE1BQU0sSUFBSSxJQUFJRSxPQUFSLENBQU4sR0FBeUJELE1BQU0sR0FBR0MsT0FBL0Q7QUFBQSxDQUFaO0FBQ0E7Ozs7Ozs7O0FBTUEsSUFBTU4sSUFBSSxHQUFHLFNBQVBBLElBQU8sQ0FBQTVGLEtBQUs7QUFBQSxTQUFLQSxLQUFLLEtBQUssQ0FBVixHQUFlQSxLQUFLLEdBQUcsQ0FBUixHQUFZLENBQUMsQ0FBYixHQUFpQixDQUFoQyxHQUFxQyxDQUExQztBQUFBLENBQWxCO0FBQ0E7Ozs7Ozs7Ozs7QUFRQSxJQUFNbUcsV0FBVyxHQUFHLFNBQWRBLFdBQWMsQ0FBVWpCLEdBQVYsRUFBZUMsR0FBZixFQUFvQmlCLEtBQXBCLEVBQTZDO0FBQUEsTUFBbEJDLEVBQWtCLHVFQUFiakIsSUFBSSxDQUFDa0IsTUFBUTtBQUMvRCxNQUFNdEcsS0FBSyxHQUFHcUcsRUFBRSxNQUFNbEIsR0FBRyxHQUFHRCxHQUFaLENBQUYsR0FBcUJBLEdBQW5DO0FBQ0EsU0FBT3FCLFVBQVUsQ0FBQ3ZHLEtBQUQsRUFBUWtGLEdBQVIsRUFBYUMsR0FBYixFQUFrQmlCLEtBQWxCLENBQWpCO0FBQ0QsQ0FIRDtBQUlBOzs7Ozs7Ozs7O0FBUUEsSUFBTUcsVUFBVSxHQUFHLFNBQWJBLFVBQWEsQ0FBVXZHLEtBQVYsRUFBaUJrRixHQUFqQixFQUFzQkMsR0FBdEIsRUFBMkJpQixLQUEzQixFQUFrQztBQUNuRCxNQUFNSSxTQUFTLEdBQUdaLElBQUksQ0FBQzVGLEtBQUQsQ0FBdEI7QUFFQUEsT0FBSyxHQUFHLENBQUNBLEtBQUssR0FBR2tGLEdBQVQsS0FBaUJDLEdBQUcsR0FBR0QsR0FBdkIsQ0FBUjtBQUNBbEYsT0FBSyxHQUFHd0csU0FBUyxHQUFHLENBQVosR0FBZ0J4RyxLQUFoQixHQUF3QkEsS0FBSyxHQUFHLENBQXhDO0FBQ0FBLE9BQUssR0FBR29GLElBQUksQ0FBQ3FCLEdBQUwsQ0FBU3pHLEtBQUssR0FBR3dHLFNBQWpCLEVBQTRCcEIsSUFBSSxDQUFDSyxHQUFMLENBQVNXLEtBQVQsQ0FBNUIsQ0FBUjtBQUNBcEcsT0FBSyxHQUFHd0csU0FBUyxHQUFHLENBQVosR0FBZ0J4RyxLQUFoQixHQUF3QixJQUFJQSxLQUFwQztBQUVBLFNBQU9BLEtBQUssSUFBSW1GLEdBQUcsR0FBR0QsR0FBVixDQUFMLEdBQXNCQSxHQUE3QjtBQUNELENBVEQ7QUFVQTs7Ozs7Ozs7Ozs7QUFTQSxJQUFNd0IsUUFBUSxHQUFHLFNBQVhBLFFBQVcsQ0FBVUMsRUFBVixFQUFjQyxFQUFkLEVBQWtCQyxFQUFsQixFQUFzQkMsRUFBdEIsRUFBMEI7QUFDekMsTUFBTUMsRUFBRSxHQUFHSixFQUFFLEdBQUdFLEVBQWhCO0FBQ0EsTUFBTUcsRUFBRSxHQUFHSixFQUFFLEdBQUdFLEVBQWhCO0FBQ0EsU0FBTzFCLElBQUksQ0FBQzZCLElBQUwsQ0FBVUYsRUFBRSxHQUFHQSxFQUFMLEdBQVVDLEVBQUUsR0FBR0EsRUFBekIsQ0FBUDtBQUNELENBSkQ7QUFNQTs7Ozs7Ozs7Ozs7O0FBVUEsSUFBTUUsZUFBZSxHQUFHLFNBQWxCQSxlQUFrQixDQUFVUCxFQUFWLEVBQWNDLEVBQWQsRUFBa0JDLEVBQWxCLEVBQXNCQyxFQUF0QixFQUEwQjtBQUNoRCxNQUFNQyxFQUFFLEdBQUdKLEVBQUUsR0FBR0UsRUFBaEI7QUFDQSxNQUFNRyxFQUFFLEdBQUdKLEVBQUUsR0FBR0UsRUFBaEI7QUFDQSxTQUFPQyxFQUFFLEdBQUdBLEVBQUwsR0FBVUMsRUFBRSxHQUFHQSxFQUF0QjtBQUNELENBSkQ7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDbktBOzs7Ozs7QUFNQTs7Ozs7OztBQU9BLElBQU1HLGFBQWEsR0FBRyxTQUFoQkEsYUFBZ0IsQ0FBVUMsQ0FBVixFQUFhO0FBQ2pDLFNBQU9BLENBQUMsSUFBSyxxRUFBT0EsQ0FBUCxNQUFhLFFBQW5CLElBQWdDQSxDQUFDLENBQUNDLFdBQUYsS0FBa0JDLE1BQXpEO0FBQ0QsQ0FGRDs7Ozs7Ozs7Ozs7Ozs7QUNaQTtBQUFBLElBQU1DLFlBQVksR0FBRzFFLG1CQUFPLENBQUMsd0VBQUQsQ0FBNUI7O0FBQ0EsSUFBTTJFLEVBQUUsR0FBRyxJQUFJRCxZQUFKLENBQWlCaEUsTUFBTSxDQUFDa0UsU0FBUCxDQUFpQkMsU0FBbEMsQ0FBWDtBQUVBLElBQU1DLFVBQVUsR0FBR2pILFFBQVEsQ0FBQ0MsYUFBVCxDQUF1QixPQUF2QixDQUFuQjtBQUNBZ0gsVUFBVSxDQUFDQyxNQUFYLEdBQW9CLEdBQXBCO0FBRUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBbUJBLElBQU1DLFFBQVEsR0FBRztBQUNmQyxRQUFNLEVBQUUsQ0FBQyxDQUFDTixFQUFFLENBQUNNLE1BQUgsRUFESztBQUVmQyxPQUFLLEVBQUUsQ0FBQyxDQUFDUCxFQUFFLENBQUNPLEtBQUgsRUFGTTtBQUdmQyxRQUFNLEVBQUUsQ0FBQyxDQUFDUixFQUFFLENBQUNRLE1BQUgsRUFISztBQUlmQyxTQUFPLEVBQUUsQ0FBQyxDQUFDVCxFQUFFLENBQUNVLEVBQUgsQ0FBTSxXQUFOLENBSkk7QUFLZkMsS0FBRyxFQUFFLENBQUMsQ0FBQ1gsRUFBRSxDQUFDVSxFQUFILENBQU0sS0FBTixDQUxRO0FBTWZFLE1BQUksRUFBRSxDQUFDLENBQUNaLEVBQUUsQ0FBQ1UsRUFBSCxDQUFNLE1BQU4sQ0FOTztBQU9mRyxRQUFNLEVBQUUsQ0FBQyxDQUFDYixFQUFFLENBQUNVLEVBQUgsQ0FBTSxRQUFOLENBUEs7QUFRZkksUUFBTSxFQUFFLENBQUMsQ0FBQ2QsRUFBRSxDQUFDVSxFQUFILENBQU0sZ0JBQU4sQ0FSSztBQVNmSyxNQUFJLEVBQUUsQ0FBQyxDQUFDLGFBQWFDLElBQWIsQ0FBa0JqRixNQUFNLENBQUNrRSxTQUFQLENBQWlCQyxTQUFuQyxDQVRPO0FBVWZlLFNBQU8sRUFBRWpCLEVBQUUsQ0FBQ2tCLE9BQUgsQ0FBVyxPQUFYLElBQXNCLENBVmhCO0FBV2ZDLE1BQUksRUFBRSxDQUFDLENBQUMsb0JBQW9CSCxJQUFwQixDQUF5QmpGLE1BQU0sQ0FBQ2tFLFNBQVAsQ0FBaUJDLFNBQTFDLENBWE87QUFZZmtCLFFBQU0sRUFBRSxTQUFTSixJQUFULENBQWNqRixNQUFNLENBQUNrRSxTQUFQLENBQWlCQyxTQUEvQixLQUE2QyxpQkFBaUJjLElBQWpCLENBQXNCakYsTUFBTSxDQUFDa0UsU0FBUCxDQUFpQm9CLE1BQXZDLENBWnRDO0FBYWZDLGFBQVcsRUFBRXZGLE1BQU0sQ0FBQ3dGLG9CQUFQLEtBQWdDQyxTQWI5QjtBQWF5QztBQUN4RHBCLFFBQU0sRUFBRUQsVUFBVSxDQUFDQyxNQUFYLEtBQXNCO0FBZGYsQ0FBakIsQyxDQWlCQTs7QUFDQSxJQUFJLENBQUNDLFFBQVEsQ0FBQ2lCLFdBQVYsSUFBeUJ2RixNQUFNLENBQUMwRixTQUFwQyxFQUErQztBQUFBLDZCQUVsQ0MsR0FGa0M7QUFHM0MzRixVQUFNLENBQUMwRixTQUFQLENBQWlCRSxPQUFqQixDQUF5QkQsR0FBekIsRUFBOEIsWUFBTTtBQUNsQyxhQUFPckIsUUFBUSxDQUFDcUIsR0FBRCxDQUFmO0FBQ0QsS0FGRDtBQUgyQzs7QUFDN0M7QUFDQSxPQUFLLElBQU1BLEdBQVgsSUFBa0JyQixRQUFsQixFQUE0QjtBQUFBLFVBQWpCcUIsR0FBaUI7QUFJM0I7QUFDRjs7QUFFY3JCLHVFQUFmLEU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDckRBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7SUFxQk16RSxHO0FBQ0osaUJBQWU7QUFBQTs7QUFDYixTQUFLZ0csU0FBTCxHQUFpQixFQUFqQjtBQUNBLFNBQUtsRyxPQUFMLEdBQWUsS0FBS0EsT0FBTCxDQUFhQyxJQUFiLENBQWtCLElBQWxCLENBQWY7O0FBQ0EsU0FBS0QsT0FBTDtBQUNEO0FBRUQ7Ozs7Ozs7Ozt3QkFLS21HLFEsRUFBVTtBQUNiLFVBQU1DLEtBQUssR0FBRyxLQUFLRixTQUFMLENBQWVHLE9BQWYsQ0FBdUJGLFFBQXZCLENBQWQ7O0FBQ0EsVUFBSUMsS0FBSyxLQUFLLENBQUMsQ0FBZixFQUFrQjtBQUNoQixhQUFLRixTQUFMLENBQWVJLElBQWYsQ0FBb0JILFFBQXBCO0FBQ0Q7QUFDRjtBQUVEOzs7Ozs7OzsyQkFLUUEsUSxFQUFVO0FBQ2hCLFVBQU1DLEtBQUssR0FBRyxLQUFLRixTQUFMLENBQWVHLE9BQWYsQ0FBdUJGLFFBQXZCLENBQWQ7O0FBQ0EsVUFBSUMsS0FBSyxLQUFLLENBQUMsQ0FBZixFQUFrQjtBQUNoQixhQUFLRixTQUFMLENBQWVLLE1BQWYsQ0FBc0JILEtBQXRCLEVBQTZCLENBQTdCO0FBQ0Q7QUFDRjtBQUVEOzs7Ozs7OEJBR1c7QUFDVCxXQUFLLElBQUlJLENBQUMsR0FBRyxDQUFiLEVBQWdCQSxDQUFDLEdBQUcsS0FBS04sU0FBTCxDQUFlTyxNQUFuQyxFQUEyQ0QsQ0FBQyxFQUE1QyxFQUFnRDtBQUM5QyxhQUFLTixTQUFMLENBQWVNLENBQWY7QUFDRDs7QUFDRG5HLFlBQU0sQ0FBQ3FHLHFCQUFQLENBQTZCLEtBQUsxRyxPQUFsQztBQUNEOzs7OztBQUVIOzs7OztBQUdlLG1FQUFJRSxHQUFKLEVBQWYsRTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNqRUE7Ozs7O0FBTUE7QUFFQTs7Ozs7Ozs7QUFPQSxJQUFNeUcsY0FBYyxHQUFHLFNBQWpCQSxjQUFpQixDQUFVN0osS0FBVixFQUFpQjhKLEtBQWpCLEVBQXdCO0FBQzdDLE1BQU1DLFNBQVMsR0FBR0Msa0RBQUksQ0FBQ2hLLEtBQUQsQ0FBdEI7QUFDQSxNQUFNaUssU0FBUyxHQUFHRCxrREFBSSxDQUFDRixLQUFELENBQXRCO0FBRUFDLFdBQVMsQ0FBQ0csT0FBVixDQUFrQixVQUFBaEIsR0FBRyxFQUFJO0FBQ3ZCLFFBQUllLFNBQVMsQ0FBQ1YsT0FBVixDQUFrQkwsR0FBbEIsTUFBMkIsQ0FBQyxDQUFoQyxFQUFtQztBQUNqQzVCLFlBQU0sQ0FBQzZDLGNBQVAsQ0FBc0JuSyxLQUF0QixFQUE2QmtKLEdBQTdCLEVBQWtDO0FBQUVrQixvQkFBWSxFQUFFO0FBQWhCLE9BQWxDO0FBQ0QsS0FGRCxNQUVPO0FBQ0w7QUFDQSxVQUFJLHFFQUFPTixLQUFLLENBQUNaLEdBQUQsQ0FBWixNQUFzQixRQUExQixFQUFvQztBQUNsQ1csc0JBQWMsQ0FBQzdKLEtBQUssQ0FBQ2tKLEdBQUQsQ0FBTixFQUFhWSxLQUFLLENBQUNaLEdBQUQsQ0FBbEIsQ0FBZDtBQUNEO0FBQ0Y7QUFDRixHQVREO0FBVUQsQ0FkRDs7Ozs7Ozs7Ozs7Ozs7QUNmQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDTyxJQUFNbUIsU0FBUyxHQUFHQyx1Q0FBbEI7QUFDQSxJQUFNQyxXQUFXLEdBQUdDLHlDQUFwQjtBQUNBLElBQU1DLFFBQVEsR0FBR0Msc0NBQWpCO0FBQ0EsSUFBTUMsV0FBVyxHQUFHQyx5Q0FBcEIsQzs7Ozs7Ozs7Ozs7O0FDYlA7QUFBQTs7Ozs7O0FBT0E7QUFDQSxJQUFNQyxVQUFVLEdBQUcsRUFBbkI7QUFDQSxJQUFNQyxXQUFXLEdBQUcsRUFBcEI7QUFDQSxJQUFNQyxXQUFXLEdBQUcsR0FBcEI7QUFFQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFvR0EsU0FBU0MsY0FBVCxDQUF5QnJKLEtBQXpCLEVBQWdDO0FBQzlCLE1BQUlzSixFQUFFLEdBQUcsQ0FBVDtBQUNBLE1BQUlDLEVBQUUsR0FBRyxDQUFULENBRjhCLENBRW5COztBQUNYLE1BQUlDLEVBQUUsR0FBRyxDQUFUO0FBQ0EsTUFBSUMsRUFBRSxHQUFHLENBQVQsQ0FKOEIsQ0FJbkI7QUFFWDs7QUFDQSxNQUFJLFlBQVl6SixLQUFoQixFQUF1QjtBQUFFdUosTUFBRSxHQUFHdkosS0FBSyxDQUFDMEosTUFBWDtBQUFtQjs7QUFDNUMsTUFBSSxnQkFBZ0IxSixLQUFwQixFQUEyQjtBQUFFdUosTUFBRSxHQUFHLENBQUN2SixLQUFLLENBQUMySixVQUFQLEdBQW9CLEdBQXpCO0FBQThCOztBQUMzRCxNQUFJLGlCQUFpQjNKLEtBQXJCLEVBQTRCO0FBQUV1SixNQUFFLEdBQUcsQ0FBQ3ZKLEtBQUssQ0FBQzRKLFdBQVAsR0FBcUIsR0FBMUI7QUFBK0I7O0FBQzdELE1BQUksaUJBQWlCNUosS0FBckIsRUFBNEI7QUFBRXNKLE1BQUUsR0FBRyxDQUFDdEosS0FBSyxDQUFDNkosV0FBUCxHQUFxQixHQUExQjtBQUErQixHQVYvQixDQVk5Qjs7O0FBQ0EsTUFBSSxVQUFVN0osS0FBVixJQUFtQkEsS0FBSyxDQUFDOEosSUFBTixLQUFlOUosS0FBSyxDQUFDK0osZUFBNUMsRUFBNkQ7QUFDM0RULE1BQUUsR0FBR0MsRUFBTDtBQUNBQSxNQUFFLEdBQUcsQ0FBTDtBQUNEOztBQUVEQyxJQUFFLEdBQUdGLEVBQUUsR0FBR0osVUFBVjtBQUNBTyxJQUFFLEdBQUdGLEVBQUUsR0FBR0wsVUFBVjs7QUFFQSxNQUFJLFlBQVlsSixLQUFoQixFQUF1QjtBQUFFeUosTUFBRSxHQUFHekosS0FBSyxDQUFDZ0ssTUFBWDtBQUFtQjs7QUFDNUMsTUFBSSxZQUFZaEssS0FBaEIsRUFBdUI7QUFBRXdKLE1BQUUsR0FBR3hKLEtBQUssQ0FBQ2lLLE1BQVg7QUFBbUI7O0FBRTVDLE1BQUksQ0FBQ1QsRUFBRSxJQUFJQyxFQUFQLEtBQWN6SixLQUFLLENBQUNrSyxTQUF4QixFQUFtQztBQUNqQyxRQUFJbEssS0FBSyxDQUFDa0ssU0FBTixLQUFvQixDQUF4QixFQUEyQjtBQUFFO0FBQzNCVixRQUFFLElBQUlMLFdBQU47QUFDQU0sUUFBRSxJQUFJTixXQUFOO0FBQ0QsS0FIRCxNQUdPO0FBQUU7QUFDUEssUUFBRSxJQUFJSixXQUFOO0FBQ0FLLFFBQUUsSUFBSUwsV0FBTjtBQUNEO0FBQ0YsR0FoQzZCLENBa0M5Qjs7O0FBQ0EsTUFBSUksRUFBRSxJQUFJLENBQUNGLEVBQVgsRUFBZTtBQUFFQSxNQUFFLEdBQUlFLEVBQUUsR0FBRyxDQUFOLEdBQVcsQ0FBQyxDQUFaLEdBQWdCLENBQXJCO0FBQXdCOztBQUN6QyxNQUFJQyxFQUFFLElBQUksQ0FBQ0YsRUFBWCxFQUFlO0FBQUVBLE1BQUUsR0FBSUUsRUFBRSxHQUFHLENBQU4sR0FBVyxDQUFDLENBQVosR0FBZ0IsQ0FBckI7QUFBd0I7O0FBRXpDLFNBQU87QUFDTFUsU0FBSyxFQUFFYixFQURGO0FBRUxjLFNBQUssRUFBRWIsRUFGRjtBQUdMYyxVQUFNLEVBQUViLEVBSEg7QUFJTGMsVUFBTSxFQUFFYjtBQUpILEdBQVA7QUFNRCxDLENBRUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFFZUosNkVBQWYsRSIsImZpbGUiOiJqcy11dGlscy5qcyIsInNvdXJjZXNDb250ZW50IjpbIihmdW5jdGlvbiB3ZWJwYWNrVW5pdmVyc2FsTW9kdWxlRGVmaW5pdGlvbihyb290LCBmYWN0b3J5KSB7XG5cdGlmKHR5cGVvZiBleHBvcnRzID09PSAnb2JqZWN0JyAmJiB0eXBlb2YgbW9kdWxlID09PSAnb2JqZWN0Jylcblx0XHRtb2R1bGUuZXhwb3J0cyA9IGZhY3RvcnkoKTtcblx0ZWxzZSBpZih0eXBlb2YgZGVmaW5lID09PSAnZnVuY3Rpb24nICYmIGRlZmluZS5hbWQpXG5cdFx0ZGVmaW5lKFwianMtdXRpbHNcIiwgW10sIGZhY3RvcnkpO1xuXHRlbHNlIGlmKHR5cGVvZiBleHBvcnRzID09PSAnb2JqZWN0Jylcblx0XHRleHBvcnRzW1wianMtdXRpbHNcIl0gPSBmYWN0b3J5KCk7XG5cdGVsc2Vcblx0XHRyb290W1wianMtdXRpbHNcIl0gPSBmYWN0b3J5KCk7XG59KSh0eXBlb2Ygc2VsZiAhPT0gJ3VuZGVmaW5lZCcgPyBzZWxmIDogdGhpcywgZnVuY3Rpb24oKSB7XG5yZXR1cm4gIiwiIFx0Ly8gVGhlIG1vZHVsZSBjYWNoZVxuIFx0dmFyIGluc3RhbGxlZE1vZHVsZXMgPSB7fTtcblxuIFx0Ly8gVGhlIHJlcXVpcmUgZnVuY3Rpb25cbiBcdGZ1bmN0aW9uIF9fd2VicGFja19yZXF1aXJlX18obW9kdWxlSWQpIHtcblxuIFx0XHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcbiBcdFx0aWYoaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0pIHtcbiBcdFx0XHRyZXR1cm4gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0uZXhwb3J0cztcbiBcdFx0fVxuIFx0XHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuIFx0XHR2YXIgbW9kdWxlID0gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0gPSB7XG4gXHRcdFx0aTogbW9kdWxlSWQsXG4gXHRcdFx0bDogZmFsc2UsXG4gXHRcdFx0ZXhwb3J0czoge31cbiBcdFx0fTtcblxuIFx0XHQvLyBFeGVjdXRlIHRoZSBtb2R1bGUgZnVuY3Rpb25cbiBcdFx0bW9kdWxlc1ttb2R1bGVJZF0uY2FsbChtb2R1bGUuZXhwb3J0cywgbW9kdWxlLCBtb2R1bGUuZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXyk7XG5cbiBcdFx0Ly8gRmxhZyB0aGUgbW9kdWxlIGFzIGxvYWRlZFxuIFx0XHRtb2R1bGUubCA9IHRydWU7XG5cbiBcdFx0Ly8gUmV0dXJuIHRoZSBleHBvcnRzIG9mIHRoZSBtb2R1bGVcbiBcdFx0cmV0dXJuIG1vZHVsZS5leHBvcnRzO1xuIFx0fVxuXG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlcyBvYmplY3QgKF9fd2VicGFja19tb2R1bGVzX18pXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm0gPSBtb2R1bGVzO1xuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZSBjYWNoZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5jID0gaW5zdGFsbGVkTW9kdWxlcztcblxuIFx0Ly8gZGVmaW5lIGdldHRlciBmdW5jdGlvbiBmb3IgaGFybW9ueSBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQgPSBmdW5jdGlvbihleHBvcnRzLCBuYW1lLCBnZXR0ZXIpIHtcbiBcdFx0aWYoIV9fd2VicGFja19yZXF1aXJlX18ubyhleHBvcnRzLCBuYW1lKSkge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBuYW1lLCB7IGVudW1lcmFibGU6IHRydWUsIGdldDogZ2V0dGVyIH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBkZWZpbmUgX19lc01vZHVsZSBvbiBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIgPSBmdW5jdGlvbihleHBvcnRzKSB7XG4gXHRcdGlmKHR5cGVvZiBTeW1ib2wgIT09ICd1bmRlZmluZWQnICYmIFN5bWJvbC50b1N0cmluZ1RhZykge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBTeW1ib2wudG9TdHJpbmdUYWcsIHsgdmFsdWU6ICdNb2R1bGUnIH0pO1xuIFx0XHR9XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnX19lc01vZHVsZScsIHsgdmFsdWU6IHRydWUgfSk7XG4gXHR9O1xuXG4gXHQvLyBjcmVhdGUgYSBmYWtlIG5hbWVzcGFjZSBvYmplY3RcbiBcdC8vIG1vZGUgJiAxOiB2YWx1ZSBpcyBhIG1vZHVsZSBpZCwgcmVxdWlyZSBpdFxuIFx0Ly8gbW9kZSAmIDI6IG1lcmdlIGFsbCBwcm9wZXJ0aWVzIG9mIHZhbHVlIGludG8gdGhlIG5zXG4gXHQvLyBtb2RlICYgNDogcmV0dXJuIHZhbHVlIHdoZW4gYWxyZWFkeSBucyBvYmplY3RcbiBcdC8vIG1vZGUgJiA4fDE6IGJlaGF2ZSBsaWtlIHJlcXVpcmVcbiBcdF9fd2VicGFja19yZXF1aXJlX18udCA9IGZ1bmN0aW9uKHZhbHVlLCBtb2RlKSB7XG4gXHRcdGlmKG1vZGUgJiAxKSB2YWx1ZSA9IF9fd2VicGFja19yZXF1aXJlX18odmFsdWUpO1xuIFx0XHRpZihtb2RlICYgOCkgcmV0dXJuIHZhbHVlO1xuIFx0XHRpZigobW9kZSAmIDQpICYmIHR5cGVvZiB2YWx1ZSA9PT0gJ29iamVjdCcgJiYgdmFsdWUgJiYgdmFsdWUuX19lc01vZHVsZSkgcmV0dXJuIHZhbHVlO1xuIFx0XHR2YXIgbnMgPSBPYmplY3QuY3JlYXRlKG51bGwpO1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIobnMpO1xuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkobnMsICdkZWZhdWx0JywgeyBlbnVtZXJhYmxlOiB0cnVlLCB2YWx1ZTogdmFsdWUgfSk7XG4gXHRcdGlmKG1vZGUgJiAyICYmIHR5cGVvZiB2YWx1ZSAhPSAnc3RyaW5nJykgZm9yKHZhciBrZXkgaW4gdmFsdWUpIF9fd2VicGFja19yZXF1aXJlX18uZChucywga2V5LCBmdW5jdGlvbihrZXkpIHsgcmV0dXJuIHZhbHVlW2tleV07IH0uYmluZChudWxsLCBrZXkpKTtcbiBcdFx0cmV0dXJuIG5zO1xuIFx0fTtcblxuIFx0Ly8gZ2V0RGVmYXVsdEV4cG9ydCBmdW5jdGlvbiBmb3IgY29tcGF0aWJpbGl0eSB3aXRoIG5vbi1oYXJtb255IG1vZHVsZXNcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubiA9IGZ1bmN0aW9uKG1vZHVsZSkge1xuIFx0XHR2YXIgZ2V0dGVyID0gbW9kdWxlICYmIG1vZHVsZS5fX2VzTW9kdWxlID9cbiBcdFx0XHRmdW5jdGlvbiBnZXREZWZhdWx0KCkgeyByZXR1cm4gbW9kdWxlWydkZWZhdWx0J107IH0gOlxuIFx0XHRcdGZ1bmN0aW9uIGdldE1vZHVsZUV4cG9ydHMoKSB7IHJldHVybiBtb2R1bGU7IH07XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18uZChnZXR0ZXIsICdhJywgZ2V0dGVyKTtcbiBcdFx0cmV0dXJuIGdldHRlcjtcbiBcdH07XG5cbiBcdC8vIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5vID0gZnVuY3Rpb24ob2JqZWN0LCBwcm9wZXJ0eSkgeyByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iamVjdCwgcHJvcGVydHkpOyB9O1xuXG4gXHQvLyBfX3dlYnBhY2tfcHVibGljX3BhdGhfX1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5wID0gXCJcIjtcblxuXG4gXHQvLyBMb2FkIGVudHJ5IG1vZHVsZSBhbmQgcmV0dXJuIGV4cG9ydHNcbiBcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKF9fd2VicGFja19yZXF1aXJlX18ucyA9IFwiLi9zcmMvanMtdXRpbHMuanNcIik7XG4iLCJmdW5jdGlvbiBfY2xhc3NDYWxsQ2hlY2soaW5zdGFuY2UsIENvbnN0cnVjdG9yKSB7XG4gIGlmICghKGluc3RhbmNlIGluc3RhbmNlb2YgQ29uc3RydWN0b3IpKSB7XG4gICAgdGhyb3cgbmV3IFR5cGVFcnJvcihcIkNhbm5vdCBjYWxsIGEgY2xhc3MgYXMgYSBmdW5jdGlvblwiKTtcbiAgfVxufVxuXG5tb2R1bGUuZXhwb3J0cyA9IF9jbGFzc0NhbGxDaGVjazsiLCJmdW5jdGlvbiBfZGVmaW5lUHJvcGVydGllcyh0YXJnZXQsIHByb3BzKSB7XG4gIGZvciAodmFyIGkgPSAwOyBpIDwgcHJvcHMubGVuZ3RoOyBpKyspIHtcbiAgICB2YXIgZGVzY3JpcHRvciA9IHByb3BzW2ldO1xuICAgIGRlc2NyaXB0b3IuZW51bWVyYWJsZSA9IGRlc2NyaXB0b3IuZW51bWVyYWJsZSB8fCBmYWxzZTtcbiAgICBkZXNjcmlwdG9yLmNvbmZpZ3VyYWJsZSA9IHRydWU7XG4gICAgaWYgKFwidmFsdWVcIiBpbiBkZXNjcmlwdG9yKSBkZXNjcmlwdG9yLndyaXRhYmxlID0gdHJ1ZTtcbiAgICBPYmplY3QuZGVmaW5lUHJvcGVydHkodGFyZ2V0LCBkZXNjcmlwdG9yLmtleSwgZGVzY3JpcHRvcik7XG4gIH1cbn1cblxuZnVuY3Rpb24gX2NyZWF0ZUNsYXNzKENvbnN0cnVjdG9yLCBwcm90b1Byb3BzLCBzdGF0aWNQcm9wcykge1xuICBpZiAocHJvdG9Qcm9wcykgX2RlZmluZVByb3BlcnRpZXMoQ29uc3RydWN0b3IucHJvdG90eXBlLCBwcm90b1Byb3BzKTtcbiAgaWYgKHN0YXRpY1Byb3BzKSBfZGVmaW5lUHJvcGVydGllcyhDb25zdHJ1Y3Rvciwgc3RhdGljUHJvcHMpO1xuICByZXR1cm4gQ29uc3RydWN0b3I7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gX2NyZWF0ZUNsYXNzOyIsImZ1bmN0aW9uIF90eXBlb2Yob2JqKSB7XG4gIFwiQGJhYmVsL2hlbHBlcnMgLSB0eXBlb2ZcIjtcblxuICBpZiAodHlwZW9mIFN5bWJvbCA9PT0gXCJmdW5jdGlvblwiICYmIHR5cGVvZiBTeW1ib2wuaXRlcmF0b3IgPT09IFwic3ltYm9sXCIpIHtcbiAgICBtb2R1bGUuZXhwb3J0cyA9IF90eXBlb2YgPSBmdW5jdGlvbiBfdHlwZW9mKG9iaikge1xuICAgICAgcmV0dXJuIHR5cGVvZiBvYmo7XG4gICAgfTtcbiAgfSBlbHNlIHtcbiAgICBtb2R1bGUuZXhwb3J0cyA9IF90eXBlb2YgPSBmdW5jdGlvbiBfdHlwZW9mKG9iaikge1xuICAgICAgcmV0dXJuIG9iaiAmJiB0eXBlb2YgU3ltYm9sID09PSBcImZ1bmN0aW9uXCIgJiYgb2JqLmNvbnN0cnVjdG9yID09PSBTeW1ib2wgJiYgb2JqICE9PSBTeW1ib2wucHJvdG90eXBlID8gXCJzeW1ib2xcIiA6IHR5cGVvZiBvYmo7XG4gICAgfTtcbiAgfVxuXG4gIHJldHVybiBfdHlwZW9mKG9iaik7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gX3R5cGVvZjsiLCIvKipcbiAqXG4gKiBGVUxMIFRJTFRcbiAqIGh0dHA6Ly9naXRodWIuY29tL3JpY2h0ci9GdWxsLVRpbHRcbiAqXG4gKiBBIHN0YW5kYWxvbmUgRGV2aWNlT3JpZW50YXRpb24gKyBEZXZpY2VNb3Rpb24gSmF2YVNjcmlwdCBsaWJyYXJ5IHRoYXRcbiAqIG5vcm1hbGlzZXMgb3JpZW50YXRpb24gc2Vuc29yIGlucHV0LCBhcHBsaWVzIHJlbGV2YW50IHNjcmVlbiBvcmllbnRhdGlvblxuICogdHJhbnNmb3JtcywgcmV0dXJucyBFdWxlciBBbmdsZSwgUXVhdGVybmlvbiBhbmQgUm90YXRpb25cbiAqIE1hdHJpeCByZXByZXNlbnRhdGlvbnMgYmFjayB0byB3ZWIgZGV2ZWxvcGVycyBhbmQgcHJvdmlkZXMgY29udmVyc2lvblxuICogYmV0d2VlbiBhbGwgc3VwcG9ydGVkIG9yaWVudGF0aW9uIHJlcHJlc2VudGF0aW9uIHR5cGVzLlxuICpcbiAqIENvcHlyaWdodDogMjAxNCBSaWNoIFRpYmJldHRcbiAqIExpY2Vuc2U6ICAgTUlUXG4gKlxuICovXG5cbihmdW5jdGlvbiAoIHdpbmRvdyApIHtcblxuLy8gT25seSBpbml0aWFsaXplIHRoZSBGVUxMVElMVCBBUEkgaWYgaXQgaXMgbm90IGFscmVhZHkgYXR0YWNoZWQgdG8gdGhlIERPTVxuaWYgKCB3aW5kb3cuRlVMTFRJTFQgIT09IHVuZGVmaW5lZCAmJiB3aW5kb3cuRlVMTFRJTFQgIT09IG51bGwgKSB7XG5cdHJldHVybjtcbn1cblxudmFyIE1fUEkgICA9IE1hdGguUEk7XG52YXIgTV9QSV8yID0gTV9QSSAvIDI7XG52YXIgTV8yX1BJID0gMiAqIE1fUEk7XG5cbi8vIERlZ3JlZSB0byBSYWRpYW4gY29udmVyc2lvblxudmFyIGRlZ1RvUmFkID0gTV9QSSAvIDE4MDtcbnZhciByYWRUb0RlZyA9IDE4MCAvIE1fUEk7XG5cbi8vIEludGVybmFsIGRldmljZSBvcmllbnRhdGlvbiArIG1vdGlvbiB2YXJpYWJsZXNcbnZhciBzZW5zb3JzID0ge1xuXHRcIm9yaWVudGF0aW9uXCI6IHtcblx0XHRhY3RpdmU6ICAgIGZhbHNlLFxuXHRcdGNhbGxiYWNrczogW10sXG5cdFx0ZGF0YTogICAgICB1bmRlZmluZWRcblx0fSxcblx0XCJtb3Rpb25cIjoge1xuXHRcdGFjdGl2ZTogICAgZmFsc2UsXG5cdFx0Y2FsbGJhY2tzOiBbXSxcblx0XHRkYXRhOiAgICAgIHVuZGVmaW5lZFxuXHR9XG59O1xudmFyIHNjcmVlbkFjdGl2ZSA9IGZhbHNlO1xuXG4vLyBJbnRlcm5hbCBzY3JlZW4gb3JpZW50YXRpb24gdmFyaWFibGVzXG52YXIgaGFzU2NyZWVuT3JpZW50YXRpb25BUEkgPSB3aW5kb3cuc2NyZWVuICYmIHdpbmRvdy5zY3JlZW4ub3JpZW50YXRpb24gJiYgd2luZG93LnNjcmVlbi5vcmllbnRhdGlvbi5hbmdsZSAhPT0gdW5kZWZpbmVkICYmIHdpbmRvdy5zY3JlZW4ub3JpZW50YXRpb24uYW5nbGUgIT09IG51bGwgPyB0cnVlIDogZmFsc2U7XG52YXIgc2NyZWVuT3JpZW50YXRpb25BbmdsZSA9ICggaGFzU2NyZWVuT3JpZW50YXRpb25BUEkgPyB3aW5kb3cuc2NyZWVuLm9yaWVudGF0aW9uLmFuZ2xlIDogKCB3aW5kb3cub3JpZW50YXRpb24gfHwgMCApICkgKiBkZWdUb1JhZDtcblxudmFyIFNDUkVFTl9ST1RBVElPTl8wICAgICAgICA9IDAsXG4gICAgU0NSRUVOX1JPVEFUSU9OXzkwICAgICAgID0gTV9QSV8yLFxuICAgIFNDUkVFTl9ST1RBVElPTl8xODAgICAgICA9IE1fUEksXG4gICAgU0NSRUVOX1JPVEFUSU9OXzI3MCAgICAgID0gTV8yX1BJIC8gMyxcbiAgICBTQ1JFRU5fUk9UQVRJT05fTUlOVVNfOTAgPSAtIE1fUElfMjtcblxuLy8gTWF0aC5zaWduIHBvbHlmaWxsXG5mdW5jdGlvbiBzaWduKHgpIHtcblx0eCA9ICt4OyAvLyBjb252ZXJ0IHRvIGEgbnVtYmVyXG5cdGlmICh4ID09PSAwIHx8IGlzTmFOKHgpKVxuXHRcdHJldHVybiB4O1xuXHRyZXR1cm4geCA+IDAgPyAxIDogLTE7XG59XG5cbi8vLy8vIFByb21pc2UtYmFzZWQgU2Vuc29yIERhdGEgY2hlY2tlciAvLy8vLy9cblxuZnVuY3Rpb24gU2Vuc29yQ2hlY2soc2Vuc29yUm9vdE9iaikge1xuXG5cdHZhciBwcm9taXNlID0gbmV3IFByb21pc2UoZnVuY3Rpb24ocmVzb2x2ZSwgcmVqZWN0KSB7XG5cblx0XHR2YXIgcnVuQ2hlY2sgPSBmdW5jdGlvbiAodHJpZXMpIHtcblxuXHRcdFx0c2V0VGltZW91dChmdW5jdGlvbigpIHtcblxuXHRcdFx0XHRpZiAoc2Vuc29yUm9vdE9iaiAmJiBzZW5zb3JSb290T2JqLmRhdGEpIHtcblxuXHRcdFx0XHRcdHJlc29sdmUoKTtcblxuXHRcdFx0XHR9IGVsc2UgaWYgKHRyaWVzID49IDIwKSB7XG5cblx0XHRcdFx0XHRyZWplY3QoKTtcblxuXHRcdFx0XHR9IGVsc2Uge1xuXG5cdFx0XHRcdFx0cnVuQ2hlY2soKyt0cmllcyk7XG5cblx0XHRcdFx0fVxuXG5cdFx0XHR9LCA1MCk7XG5cblx0XHR9O1xuXG5cdFx0cnVuQ2hlY2soMCk7XG5cblx0fSk7XG5cblx0cmV0dXJuIHByb21pc2U7XG5cbn1cblxuLy8vLy8vIEludGVybmFsIEV2ZW50IEhhbmRsZXJzIC8vLy8vL1xuXG5mdW5jdGlvbiBoYW5kbGVTY3JlZW5PcmllbnRhdGlvbkNoYW5nZSAoKSB7XG5cblx0aWYgKCBoYXNTY3JlZW5PcmllbnRhdGlvbkFQSSApIHtcblxuXHRcdHNjcmVlbk9yaWVudGF0aW9uQW5nbGUgPSAoIHdpbmRvdy5zY3JlZW4ub3JpZW50YXRpb24uYW5nbGUgfHwgMCApICogZGVnVG9SYWQ7XG5cblx0fSBlbHNlIHtcblxuXHRcdHNjcmVlbk9yaWVudGF0aW9uQW5nbGUgPSAoIHdpbmRvdy5vcmllbnRhdGlvbiB8fCAwICkgKiBkZWdUb1JhZDtcblxuXHR9XG5cbn1cblxuZnVuY3Rpb24gaGFuZGxlRGV2aWNlT3JpZW50YXRpb25DaGFuZ2UgKCBldmVudCApIHtcblxuXHRzZW5zb3JzLm9yaWVudGF0aW9uLmRhdGEgPSBldmVudDtcblxuXHQvLyBGaXJlIGV2ZXJ5IGNhbGxiYWNrIGZ1bmN0aW9uIGVhY2ggdGltZSBkZXZpY2VvcmllbnRhdGlvbiBpcyB1cGRhdGVkXG5cdGZvciAoIHZhciBpIGluIHNlbnNvcnMub3JpZW50YXRpb24uY2FsbGJhY2tzICkge1xuXG5cdFx0c2Vuc29ycy5vcmllbnRhdGlvbi5jYWxsYmFja3NbIGkgXS5jYWxsKCB0aGlzICk7XG5cblx0fVxuXG59XG5cbmZ1bmN0aW9uIGhhbmRsZURldmljZU1vdGlvbkNoYW5nZSAoIGV2ZW50ICkge1xuXG5cdHNlbnNvcnMubW90aW9uLmRhdGEgPSBldmVudDtcblxuXHQvLyBGaXJlIGV2ZXJ5IGNhbGxiYWNrIGZ1bmN0aW9uIGVhY2ggdGltZSBkZXZpY2Vtb3Rpb24gaXMgdXBkYXRlZFxuXHRmb3IgKCB2YXIgaSBpbiBzZW5zb3JzLm1vdGlvbi5jYWxsYmFja3MgKSB7XG5cblx0XHRzZW5zb3JzLm1vdGlvbi5jYWxsYmFja3NbIGkgXS5jYWxsKCB0aGlzICk7XG5cblx0fVxuXG59XG5cbi8vLy8vIEZVTExUSUxUIEFQSSBSb290IE9iamVjdCAvLy8vL1xuXG52YXIgRlVMTFRJTFQgPSB7fTtcblxuRlVMTFRJTFQudmVyc2lvbiA9IFwiMC41LjNcIjtcblxuLy8vLy8gRlVMTFRJTFQgQVBJIFJvb3QgTWV0aG9kcyAvLy8vL1xuXG5GVUxMVElMVC5nZXREZXZpY2VPcmllbnRhdGlvbiA9IGZ1bmN0aW9uKG9wdGlvbnMpIHtcblxuXHR2YXIgcHJvbWlzZSA9IG5ldyBQcm9taXNlKGZ1bmN0aW9uKHJlc29sdmUsIHJlamVjdCkge1xuXG5cdFx0dmFyIGNvbnRyb2wgPSBuZXcgRlVMTFRJTFQuRGV2aWNlT3JpZW50YXRpb24ob3B0aW9ucyk7XG5cblx0XHRjb250cm9sLnN0YXJ0KCk7XG5cblx0XHR2YXIgb3JpZW50YXRpb25TZW5zb3JDaGVjayA9IG5ldyBTZW5zb3JDaGVjayhzZW5zb3JzLm9yaWVudGF0aW9uKTtcblxuXHRcdG9yaWVudGF0aW9uU2Vuc29yQ2hlY2sudGhlbihmdW5jdGlvbigpIHtcblxuXHRcdFx0cmVzb2x2ZShjb250cm9sKTtcblxuXHRcdH0pLmNhdGNoKGZ1bmN0aW9uKCkge1xuXG5cdFx0XHRjb250cm9sLnN0b3AoKTtcblx0XHRcdHJlamVjdCgnRGV2aWNlT3JpZW50YXRpb24gaXMgbm90IHN1cHBvcnRlZCcpO1xuXG5cdFx0fSk7XG5cblx0fSk7XG5cblx0cmV0dXJuIHByb21pc2U7XG5cbn07XG5cbkZVTExUSUxULmdldERldmljZU1vdGlvbiA9IGZ1bmN0aW9uKG9wdGlvbnMpIHtcblxuXHR2YXIgcHJvbWlzZSA9IG5ldyBQcm9taXNlKGZ1bmN0aW9uKHJlc29sdmUsIHJlamVjdCkge1xuXG5cdFx0dmFyIGNvbnRyb2wgPSBuZXcgRlVMTFRJTFQuRGV2aWNlTW90aW9uKG9wdGlvbnMpO1xuXG5cdFx0Y29udHJvbC5zdGFydCgpO1xuXG5cdFx0dmFyIG1vdGlvblNlbnNvckNoZWNrID0gbmV3IFNlbnNvckNoZWNrKHNlbnNvcnMubW90aW9uKTtcblxuXHRcdG1vdGlvblNlbnNvckNoZWNrLnRoZW4oZnVuY3Rpb24oKSB7XG5cblx0XHRcdHJlc29sdmUoY29udHJvbCk7XG5cblx0XHR9KS5jYXRjaChmdW5jdGlvbigpIHtcblxuXHRcdFx0Y29udHJvbC5zdG9wKCk7XG5cdFx0XHRyZWplY3QoJ0RldmljZU1vdGlvbiBpcyBub3Qgc3VwcG9ydGVkJyk7XG5cblx0XHR9KTtcblxuXHR9KTtcblxuXHRyZXR1cm4gcHJvbWlzZTtcblxufTtcblxuXG4vLy8vLy8gRlVMTFRJTFQuUXVhdGVybmlvbiAvLy8vLy9cblxuRlVMTFRJTFQuUXVhdGVybmlvbiA9IGZ1bmN0aW9uICggeCwgeSwgeiwgdyApIHtcblxuXHR2YXIgcXVhdCwgb3V0UXVhdDtcblxuXHR0aGlzLnNldCA9IGZ1bmN0aW9uICggeCwgeSwgeiwgdyApIHtcblxuXHRcdHRoaXMueCA9IHggfHwgMDtcblx0XHR0aGlzLnkgPSB5IHx8IDA7XG5cdFx0dGhpcy56ID0geiB8fCAwO1xuXHRcdHRoaXMudyA9IHcgfHwgMTtcblxuXHR9O1xuXG5cdHRoaXMuY29weSA9IGZ1bmN0aW9uICggcXVhdGVybmlvbiApIHtcblxuXHRcdHRoaXMueCA9IHF1YXRlcm5pb24ueDtcblx0XHR0aGlzLnkgPSBxdWF0ZXJuaW9uLnk7XG5cdFx0dGhpcy56ID0gcXVhdGVybmlvbi56O1xuXHRcdHRoaXMudyA9IHF1YXRlcm5pb24udztcblxuXHR9O1xuXG5cdHRoaXMuc2V0RnJvbUV1bGVyID0gKGZ1bmN0aW9uICgpIHtcblxuXHRcdHZhciBfeCwgX3ksIF96O1xuXHRcdHZhciBfeF8yLCBfeV8yLCBfel8yO1xuXHRcdHZhciBjWCwgY1ksIGNaLCBzWCwgc1ksIHNaO1xuXG5cdFx0cmV0dXJuIGZ1bmN0aW9uICggZXVsZXIgKSB7XG5cblx0XHRcdGV1bGVyID0gZXVsZXIgfHwge307XG5cblx0XHRcdF96ID0gKCBldWxlci5hbHBoYSB8fCAwICkgKiBkZWdUb1JhZDtcblx0XHRcdF94ID0gKCBldWxlci5iZXRhIHx8IDAgKSAqIGRlZ1RvUmFkO1xuXHRcdFx0X3kgPSAoIGV1bGVyLmdhbW1hIHx8IDAgKSAqIGRlZ1RvUmFkO1xuXG5cdFx0XHRfel8yID0gX3ogLyAyO1xuXHRcdFx0X3hfMiA9IF94IC8gMjtcblx0XHRcdF95XzIgPSBfeSAvIDI7XG5cblx0XHRcdGNYID0gTWF0aC5jb3MoIF94XzIgKTtcblx0XHRcdGNZID0gTWF0aC5jb3MoIF95XzIgKTtcblx0XHRcdGNaID0gTWF0aC5jb3MoIF96XzIgKTtcblx0XHRcdHNYID0gTWF0aC5zaW4oIF94XzIgKTtcblx0XHRcdHNZID0gTWF0aC5zaW4oIF95XzIgKTtcblx0XHRcdHNaID0gTWF0aC5zaW4oIF96XzIgKTtcblxuXHRcdFx0dGhpcy5zZXQoXG5cdFx0XHRcdHNYICogY1kgKiBjWiAtIGNYICogc1kgKiBzWiwgLy8geFxuXHRcdFx0XHRjWCAqIHNZICogY1ogKyBzWCAqIGNZICogc1osIC8vIHlcblx0XHRcdFx0Y1ggKiBjWSAqIHNaICsgc1ggKiBzWSAqIGNaLCAvLyB6XG5cdFx0XHRcdGNYICogY1kgKiBjWiAtIHNYICogc1kgKiBzWiAgLy8gd1xuXHRcdFx0KTtcblxuXHRcdFx0dGhpcy5ub3JtYWxpemUoKTtcblxuXHRcdFx0cmV0dXJuIHRoaXM7XG5cblx0XHR9O1xuXG5cdH0pKCk7XG5cblx0dGhpcy5zZXRGcm9tUm90YXRpb25NYXRyaXggPSAoZnVuY3Rpb24gKCkge1xuXG5cdFx0dmFyIFI7XG5cblx0XHRyZXR1cm4gZnVuY3Rpb24oIG1hdHJpeCApIHtcblxuXHRcdFx0UiA9IG1hdHJpeC5lbGVtZW50cztcblxuXHRcdFx0dGhpcy5zZXQoXG5cdFx0XHRcdDAuNSAqIE1hdGguc3FydCggMSArIFJbMF0gLSBSWzRdIC0gUls4XSApICogc2lnbiggUls3XSAtIFJbNV0gKSwgLy8geFxuXHRcdFx0XHQwLjUgKiBNYXRoLnNxcnQoIDEgLSBSWzBdICsgUls0XSAtIFJbOF0gKSAqIHNpZ24oIFJbMl0gLSBSWzZdICksIC8vIHlcblx0XHRcdFx0MC41ICogTWF0aC5zcXJ0KCAxIC0gUlswXSAtIFJbNF0gKyBSWzhdICkgKiBzaWduKCBSWzNdIC0gUlsxXSApLCAvLyB6XG5cdFx0XHRcdDAuNSAqIE1hdGguc3FydCggMSArIFJbMF0gKyBSWzRdICsgUls4XSApICAgICAgICAgICAgICAgICAgICAgICAgLy8gd1xuXHRcdFx0KTtcblxuXHRcdFx0cmV0dXJuIHRoaXM7XG5cblx0XHR9O1xuXG5cdH0pKCk7XG5cblx0dGhpcy5tdWx0aXBseSA9IGZ1bmN0aW9uICggcXVhdGVybmlvbiApIHtcblxuXHRcdG91dFF1YXQgPSBGVUxMVElMVC5RdWF0ZXJuaW9uLnByb3RvdHlwZS5tdWx0aXBseVF1YXRlcm5pb25zKCB0aGlzLCBxdWF0ZXJuaW9uICk7XG5cdFx0dGhpcy5jb3B5KCBvdXRRdWF0ICk7XG5cblx0XHRyZXR1cm4gdGhpcztcblxuXHR9O1xuXG5cdHRoaXMucm90YXRlWCA9IGZ1bmN0aW9uICggYW5nbGUgKSB7XG5cblx0XHRvdXRRdWF0ID0gRlVMTFRJTFQuUXVhdGVybmlvbi5wcm90b3R5cGUucm90YXRlQnlBeGlzQW5nbGUoIHRoaXMsIFsgMSwgMCwgMCBdLCBhbmdsZSApO1xuXHRcdHRoaXMuY29weSggb3V0UXVhdCApO1xuXG5cdFx0cmV0dXJuIHRoaXM7XG5cblx0fTtcblxuXHR0aGlzLnJvdGF0ZVkgPSBmdW5jdGlvbiAoIGFuZ2xlICkge1xuXG5cdFx0b3V0UXVhdCA9IEZVTExUSUxULlF1YXRlcm5pb24ucHJvdG90eXBlLnJvdGF0ZUJ5QXhpc0FuZ2xlKCB0aGlzLCBbIDAsIDEsIDAgXSwgYW5nbGUgKTtcblx0XHR0aGlzLmNvcHkoIG91dFF1YXQgKTtcblxuXHRcdHJldHVybiB0aGlzO1xuXG5cdH07XG5cblx0dGhpcy5yb3RhdGVaID0gZnVuY3Rpb24gKCBhbmdsZSApIHtcblxuXHRcdG91dFF1YXQgPSBGVUxMVElMVC5RdWF0ZXJuaW9uLnByb3RvdHlwZS5yb3RhdGVCeUF4aXNBbmdsZSggdGhpcywgWyAwLCAwLCAxIF0sIGFuZ2xlICk7XG5cdFx0dGhpcy5jb3B5KCBvdXRRdWF0ICk7XG5cblx0XHRyZXR1cm4gdGhpcztcblxuXHR9O1xuXG5cdHRoaXMubm9ybWFsaXplID0gZnVuY3Rpb24gKCkge1xuXG5cdFx0cmV0dXJuIEZVTExUSUxULlF1YXRlcm5pb24ucHJvdG90eXBlLm5vcm1hbGl6ZSggdGhpcyApO1xuXG5cdH07XG5cblx0Ly8gSW5pdGlhbGl6ZSBvYmplY3QgdmFsdWVzXG5cdHRoaXMuc2V0KCB4LCB5LCB6LCB3ICk7XG5cbn07XG5cbkZVTExUSUxULlF1YXRlcm5pb24ucHJvdG90eXBlID0ge1xuXG5cdGNvbnN0cnVjdG9yOiBGVUxMVElMVC5RdWF0ZXJuaW9uLFxuXG5cdG11bHRpcGx5UXVhdGVybmlvbnM6IGZ1bmN0aW9uICgpIHtcblxuXHRcdHZhciBtdWx0aXBsaWVkUXVhdCA9IG5ldyBGVUxMVElMVC5RdWF0ZXJuaW9uKCk7XG5cblx0XHRyZXR1cm4gZnVuY3Rpb24gKCBhLCBiICkge1xuXG5cdFx0XHR2YXIgcWF4ID0gYS54LCBxYXkgPSBhLnksIHFheiA9IGEueiwgcWF3ID0gYS53O1xuXHRcdFx0dmFyIHFieCA9IGIueCwgcWJ5ID0gYi55LCBxYnogPSBiLnosIHFidyA9IGIudztcblxuXHRcdFx0bXVsdGlwbGllZFF1YXQuc2V0KFxuXHRcdFx0XHRxYXggKiBxYncgKyBxYXcgKiBxYnggKyBxYXkgKiBxYnogLSBxYXogKiBxYnksIC8vIHhcblx0XHRcdFx0cWF5ICogcWJ3ICsgcWF3ICogcWJ5ICsgcWF6ICogcWJ4IC0gcWF4ICogcWJ6LCAvLyB5XG5cdFx0XHRcdHFheiAqIHFidyArIHFhdyAqIHFieiArIHFheCAqIHFieSAtIHFheSAqIHFieCwgLy8gelxuXHRcdFx0XHRxYXcgKiBxYncgLSBxYXggKiBxYnggLSBxYXkgKiBxYnkgLSBxYXogKiBxYnogIC8vIHdcblx0XHRcdCk7XG5cblx0XHRcdHJldHVybiBtdWx0aXBsaWVkUXVhdDtcblxuXHRcdH07XG5cblx0fSgpLFxuXG5cdG5vcm1hbGl6ZTogZnVuY3Rpb24oIHEgKSB7XG5cblx0XHR2YXIgbGVuID0gTWF0aC5zcXJ0KCBxLnggKiBxLnggKyBxLnkgKiBxLnkgKyBxLnogKiBxLnogKyBxLncgKiBxLncgKTtcblxuXHRcdGlmICggbGVuID09PSAwICkge1xuXG5cdFx0XHRxLnggPSAwO1xuXHRcdFx0cS55ID0gMDtcblx0XHRcdHEueiA9IDA7XG5cdFx0XHRxLncgPSAxO1xuXG5cdFx0fSBlbHNlIHtcblxuXHRcdFx0bGVuID0gMSAvIGxlbjtcblxuXHRcdFx0cS54ICo9IGxlbjtcblx0XHRcdHEueSAqPSBsZW47XG5cdFx0XHRxLnogKj0gbGVuO1xuXHRcdFx0cS53ICo9IGxlbjtcblxuXHRcdH1cblxuXHRcdHJldHVybiBxO1xuXG5cdH0sXG5cblx0cm90YXRlQnlBeGlzQW5nbGU6IGZ1bmN0aW9uICgpIHtcblxuXHRcdHZhciBvdXRwdXRRdWF0ZXJuaW9uID0gbmV3IEZVTExUSUxULlF1YXRlcm5pb24oKTtcblx0XHR2YXIgdHJhbnNmb3JtUXVhdGVybmlvbiA9IG5ldyBGVUxMVElMVC5RdWF0ZXJuaW9uKCk7XG5cblx0XHR2YXIgaGFsZkFuZ2xlLCBzQTtcblxuXHRcdHJldHVybiBmdW5jdGlvbiAoIHRhcmdldFF1YXRlcm5pb24sIGF4aXMsIGFuZ2xlICkge1xuXG5cdFx0XHRoYWxmQW5nbGUgPSAoIGFuZ2xlIHx8IDAgKSAvIDI7XG5cdFx0XHRzQSA9IE1hdGguc2luKCBoYWxmQW5nbGUgKTtcblxuXHRcdFx0dHJhbnNmb3JtUXVhdGVybmlvbi5zZXQoXG5cdFx0XHRcdCggYXhpc1sgMCBdIHx8IDAgKSAqIHNBLCAvLyB4XG5cdFx0XHRcdCggYXhpc1sgMSBdIHx8IDAgKSAqIHNBLCAvLyB5XG5cdFx0XHRcdCggYXhpc1sgMiBdIHx8IDAgKSAqIHNBLCAvLyB6XG5cdFx0XHRcdE1hdGguY29zKCBoYWxmQW5nbGUgKSAgICAvLyB3XG5cdFx0XHQpO1xuXG5cdFx0XHQvLyBNdWx0aXBseSBxdWF0ZXJuaW9uIGJ5IHFcblx0XHRcdG91dHB1dFF1YXRlcm5pb24gPSBGVUxMVElMVC5RdWF0ZXJuaW9uLnByb3RvdHlwZS5tdWx0aXBseVF1YXRlcm5pb25zKCB0YXJnZXRRdWF0ZXJuaW9uLCB0cmFuc2Zvcm1RdWF0ZXJuaW9uICk7XG5cblx0XHRcdHJldHVybiBGVUxMVElMVC5RdWF0ZXJuaW9uLnByb3RvdHlwZS5ub3JtYWxpemUoIG91dHB1dFF1YXRlcm5pb24gKTtcblxuXHRcdH07XG5cblx0fSgpXG5cbn07XG5cbi8vLy8vLyBGVUxMVElMVC5Sb3RhdGlvbk1hdHJpeCAvLy8vLy9cblxuRlVMTFRJTFQuUm90YXRpb25NYXRyaXggPSBmdW5jdGlvbiAoIG0xMSwgbTEyLCBtMTMsIG0yMSwgbTIyLCBtMjMsIG0zMSwgbTMyLCBtMzMgKSB7XG5cblx0dmFyIG91dE1hdHJpeDtcblxuXHR0aGlzLmVsZW1lbnRzID0gbmV3IEZsb2F0MzJBcnJheSggOSApO1xuXG5cdHRoaXMuaWRlbnRpdHkgPSBmdW5jdGlvbiAoKSB7XG5cblx0XHR0aGlzLnNldChcblx0XHRcdDEsIDAsIDAsXG5cdFx0XHQwLCAxLCAwLFxuXHRcdFx0MCwgMCwgMVxuXHRcdCk7XG5cblx0XHRyZXR1cm4gdGhpcztcblxuXHR9O1xuXG5cdHRoaXMuc2V0ID0gZnVuY3Rpb24gKCBtMTEsIG0xMiwgbTEzLCBtMjEsIG0yMiwgbTIzLCBtMzEsIG0zMiwgbTMzICkge1xuXG5cdFx0dGhpcy5lbGVtZW50c1sgMCBdID0gbTExIHx8IDE7XG5cdFx0dGhpcy5lbGVtZW50c1sgMSBdID0gbTEyIHx8IDA7XG5cdFx0dGhpcy5lbGVtZW50c1sgMiBdID0gbTEzIHx8IDA7XG5cdFx0dGhpcy5lbGVtZW50c1sgMyBdID0gbTIxIHx8IDA7XG5cdFx0dGhpcy5lbGVtZW50c1sgNCBdID0gbTIyIHx8IDE7XG5cdFx0dGhpcy5lbGVtZW50c1sgNSBdID0gbTIzIHx8IDA7XG5cdFx0dGhpcy5lbGVtZW50c1sgNiBdID0gbTMxIHx8IDA7XG5cdFx0dGhpcy5lbGVtZW50c1sgNyBdID0gbTMyIHx8IDA7XG5cdFx0dGhpcy5lbGVtZW50c1sgOCBdID0gbTMzIHx8IDE7XG5cblx0fTtcblxuXHR0aGlzLmNvcHkgPSBmdW5jdGlvbiAoIG1hdHJpeCApIHtcblxuXHRcdHRoaXMuZWxlbWVudHNbIDAgXSA9IG1hdHJpeC5lbGVtZW50c1sgMCBdO1xuXHRcdHRoaXMuZWxlbWVudHNbIDEgXSA9IG1hdHJpeC5lbGVtZW50c1sgMSBdO1xuXHRcdHRoaXMuZWxlbWVudHNbIDIgXSA9IG1hdHJpeC5lbGVtZW50c1sgMiBdO1xuXHRcdHRoaXMuZWxlbWVudHNbIDMgXSA9IG1hdHJpeC5lbGVtZW50c1sgMyBdO1xuXHRcdHRoaXMuZWxlbWVudHNbIDQgXSA9IG1hdHJpeC5lbGVtZW50c1sgNCBdO1xuXHRcdHRoaXMuZWxlbWVudHNbIDUgXSA9IG1hdHJpeC5lbGVtZW50c1sgNSBdO1xuXHRcdHRoaXMuZWxlbWVudHNbIDYgXSA9IG1hdHJpeC5lbGVtZW50c1sgNiBdO1xuXHRcdHRoaXMuZWxlbWVudHNbIDcgXSA9IG1hdHJpeC5lbGVtZW50c1sgNyBdO1xuXHRcdHRoaXMuZWxlbWVudHNbIDggXSA9IG1hdHJpeC5lbGVtZW50c1sgOCBdO1xuXG5cdH07XG5cblx0dGhpcy5zZXRGcm9tRXVsZXIgPSAoZnVuY3Rpb24oKSB7XG5cblx0XHR2YXIgX3gsIF95LCBfejtcblx0XHR2YXIgY1gsIGNZLCBjWiwgc1gsIHNZLCBzWjtcblxuXHRcdHJldHVybiBmdW5jdGlvbiAoIGV1bGVyICkge1xuXG5cdFx0XHRldWxlciA9IGV1bGVyIHx8IHt9O1xuXG5cdFx0XHRfeiA9ICggZXVsZXIuYWxwaGEgfHwgMCApICogZGVnVG9SYWQ7XG5cdFx0XHRfeCA9ICggZXVsZXIuYmV0YSB8fCAwICkgKiBkZWdUb1JhZDtcblx0XHRcdF95ID0gKCBldWxlci5nYW1tYSB8fCAwICkgKiBkZWdUb1JhZDtcblxuXHRcdFx0Y1ggPSBNYXRoLmNvcyggX3ggKTtcblx0XHRcdGNZID0gTWF0aC5jb3MoIF95ICk7XG5cdFx0XHRjWiA9IE1hdGguY29zKCBfeiApO1xuXHRcdFx0c1ggPSBNYXRoLnNpbiggX3ggKTtcblx0XHRcdHNZID0gTWF0aC5zaW4oIF95ICk7XG5cdFx0XHRzWiA9IE1hdGguc2luKCBfeiApO1xuXG5cdFx0XHQvL1xuXHRcdFx0Ly8gWlhZLW9yZGVyZWQgcm90YXRpb24gbWF0cml4IGNvbnN0cnVjdGlvbi5cblx0XHRcdC8vXG5cblx0XHRcdHRoaXMuc2V0KFxuXHRcdFx0XHRjWiAqIGNZIC0gc1ogKiBzWCAqIHNZLCAvLyAxLDFcblx0XHRcdFx0LSBjWCAqIHNaLCAgICAgICAgICAgICAgLy8gMSwyXG5cdFx0XHRcdGNZICogc1ogKiBzWCArIGNaICogc1ksIC8vIDEsM1xuXG5cdFx0XHRcdGNZICogc1ogKyBjWiAqIHNYICogc1ksIC8vIDIsMVxuXHRcdFx0XHRjWiAqIGNYLCAgICAgICAgICAgICAgICAvLyAyLDJcblx0XHRcdFx0c1ogKiBzWSAtIGNaICogY1kgKiBzWCwgLy8gMiwzXG5cblx0XHRcdFx0LSBjWCAqIHNZLCAgICAgICAgICAgICAgLy8gMywxXG5cdFx0XHRcdHNYLCAgICAgICAgICAgICAgICAgICAgIC8vIDMsMlxuXHRcdFx0XHRjWCAqIGNZICAgICAgICAgICAgICAgICAvLyAzLDNcblx0XHRcdCk7XG5cblx0XHRcdHRoaXMubm9ybWFsaXplKCk7XG5cblx0XHRcdHJldHVybiB0aGlzO1xuXG5cdFx0fTtcblxuXHR9KSgpO1xuXG5cdHRoaXMuc2V0RnJvbVF1YXRlcm5pb24gPSAoZnVuY3Rpb24oKSB7XG5cblx0XHR2YXIgc3F3LCBzcXgsIHNxeSwgc3F6O1xuXG5cdFx0cmV0dXJuIGZ1bmN0aW9uKCBxICkge1xuXG5cdFx0XHRzcXcgPSBxLncgKiBxLnc7XG5cdFx0XHRzcXggPSBxLnggKiBxLng7XG5cdFx0XHRzcXkgPSBxLnkgKiBxLnk7XG5cdFx0XHRzcXogPSBxLnogKiBxLno7XG5cblx0XHRcdHRoaXMuc2V0KFxuXHRcdFx0XHRzcXcgKyBzcXggLSBzcXkgLSBzcXosICAgICAgIC8vIDEsMVxuXHRcdFx0XHQyICogKHEueCAqIHEueSAtIHEudyAqIHEueiksIC8vIDEsMlxuXHRcdFx0XHQyICogKHEueCAqIHEueiArIHEudyAqIHEueSksIC8vIDEsM1xuXG5cdFx0XHRcdDIgKiAocS54ICogcS55ICsgcS53ICogcS56KSwgLy8gMiwxXG5cdFx0XHRcdHNxdyAtIHNxeCArIHNxeSAtIHNxeiwgICAgICAgLy8gMiwyXG5cdFx0XHRcdDIgKiAocS55ICogcS56IC0gcS53ICogcS54KSwgLy8gMiwzXG5cblx0XHRcdFx0MiAqIChxLnggKiBxLnogLSBxLncgKiBxLnkpLCAvLyAzLDFcblx0XHRcdFx0MiAqIChxLnkgKiBxLnogKyBxLncgKiBxLngpLCAvLyAzLDJcblx0XHRcdFx0c3F3IC0gc3F4IC0gc3F5ICsgc3F6ICAgICAgICAvLyAzLDNcblx0XHRcdCk7XG5cblx0XHRcdHJldHVybiB0aGlzO1xuXG5cdFx0fTtcblxuXHR9KSgpO1xuXG5cdHRoaXMubXVsdGlwbHkgPSBmdW5jdGlvbiAoIG0gKSB7XG5cblx0XHRvdXRNYXRyaXggPSBGVUxMVElMVC5Sb3RhdGlvbk1hdHJpeC5wcm90b3R5cGUubXVsdGlwbHlNYXRyaWNlcyggdGhpcywgbSApO1xuXHRcdHRoaXMuY29weSggb3V0TWF0cml4ICk7XG5cblx0XHRyZXR1cm4gdGhpcztcblxuXHR9O1xuXG5cdHRoaXMucm90YXRlWCA9IGZ1bmN0aW9uICggYW5nbGUgKSB7XG5cblx0XHRvdXRNYXRyaXggPSBGVUxMVElMVC5Sb3RhdGlvbk1hdHJpeC5wcm90b3R5cGUucm90YXRlQnlBeGlzQW5nbGUoIHRoaXMsIFsgMSwgMCwgMCBdLCBhbmdsZSApO1xuXHRcdHRoaXMuY29weSggb3V0TWF0cml4ICk7XG5cblx0XHRyZXR1cm4gdGhpcztcblxuXHR9O1xuXG5cdHRoaXMucm90YXRlWSA9IGZ1bmN0aW9uICggYW5nbGUgKSB7XG5cblx0XHRvdXRNYXRyaXggPSBGVUxMVElMVC5Sb3RhdGlvbk1hdHJpeC5wcm90b3R5cGUucm90YXRlQnlBeGlzQW5nbGUoIHRoaXMsIFsgMCwgMSwgMCBdLCBhbmdsZSApO1xuXHRcdHRoaXMuY29weSggb3V0TWF0cml4ICk7XG5cblx0XHRyZXR1cm4gdGhpcztcblxuXHR9O1xuXG5cdHRoaXMucm90YXRlWiA9IGZ1bmN0aW9uICggYW5nbGUgKSB7XG5cblx0XHRvdXRNYXRyaXggPSBGVUxMVElMVC5Sb3RhdGlvbk1hdHJpeC5wcm90b3R5cGUucm90YXRlQnlBeGlzQW5nbGUoIHRoaXMsIFsgMCwgMCwgMSBdLCBhbmdsZSApO1xuXHRcdHRoaXMuY29weSggb3V0TWF0cml4ICk7XG5cblx0XHRyZXR1cm4gdGhpcztcblxuXHR9O1xuXG5cdHRoaXMubm9ybWFsaXplID0gZnVuY3Rpb24gKCkge1xuXG5cdFx0cmV0dXJuIEZVTExUSUxULlJvdGF0aW9uTWF0cml4LnByb3RvdHlwZS5ub3JtYWxpemUoIHRoaXMgKTtcblxuXHR9O1xuXG5cdC8vIEluaXRpYWxpemUgb2JqZWN0IHZhbHVlc1xuXHR0aGlzLnNldCggbTExLCBtMTIsIG0xMywgbTIxLCBtMjIsIG0yMywgbTMxLCBtMzIsIG0zMyApO1xuXG59O1xuXG5GVUxMVElMVC5Sb3RhdGlvbk1hdHJpeC5wcm90b3R5cGUgPSB7XG5cblx0Y29uc3RydWN0b3I6IEZVTExUSUxULlJvdGF0aW9uTWF0cml4LFxuXG5cdG11bHRpcGx5TWF0cmljZXM6IGZ1bmN0aW9uICgpIHtcblxuXHRcdHZhciBtYXRyaXggPSBuZXcgRlVMTFRJTFQuUm90YXRpb25NYXRyaXgoKTtcblxuXHRcdHZhciBhRSwgYkU7XG5cblx0XHRyZXR1cm4gZnVuY3Rpb24gKCBhLCBiICkge1xuXG5cdFx0XHRhRSA9IGEuZWxlbWVudHM7XG5cdFx0XHRiRSA9IGIuZWxlbWVudHM7XG5cblx0XHRcdG1hdHJpeC5zZXQoXG5cdFx0XHRcdGFFWzBdICogYkVbMF0gKyBhRVsxXSAqIGJFWzNdICsgYUVbMl0gKiBiRVs2XSxcblx0XHRcdFx0YUVbMF0gKiBiRVsxXSArIGFFWzFdICogYkVbNF0gKyBhRVsyXSAqIGJFWzddLFxuXHRcdFx0XHRhRVswXSAqIGJFWzJdICsgYUVbMV0gKiBiRVs1XSArIGFFWzJdICogYkVbOF0sXG5cblx0XHRcdFx0YUVbM10gKiBiRVswXSArIGFFWzRdICogYkVbM10gKyBhRVs1XSAqIGJFWzZdLFxuXHRcdFx0XHRhRVszXSAqIGJFWzFdICsgYUVbNF0gKiBiRVs0XSArIGFFWzVdICogYkVbN10sXG5cdFx0XHRcdGFFWzNdICogYkVbMl0gKyBhRVs0XSAqIGJFWzVdICsgYUVbNV0gKiBiRVs4XSxcblxuXHRcdFx0XHRhRVs2XSAqIGJFWzBdICsgYUVbN10gKiBiRVszXSArIGFFWzhdICogYkVbNl0sXG5cdFx0XHRcdGFFWzZdICogYkVbMV0gKyBhRVs3XSAqIGJFWzRdICsgYUVbOF0gKiBiRVs3XSxcblx0XHRcdFx0YUVbNl0gKiBiRVsyXSArIGFFWzddICogYkVbNV0gKyBhRVs4XSAqIGJFWzhdXG5cdFx0XHQpO1xuXG5cdFx0XHRyZXR1cm4gbWF0cml4O1xuXG5cdFx0fTtcblxuXHR9KCksXG5cblx0bm9ybWFsaXplOiBmdW5jdGlvbiggbWF0cml4ICkge1xuXG5cdFx0dmFyIFIgPSBtYXRyaXguZWxlbWVudHM7XG5cblx0XHQvLyBDYWxjdWxhdGUgbWF0cml4IGRldGVybWluYW50XG5cdFx0dmFyIGRldGVybWluYW50ID0gUlswXSAqIFJbNF0gKiBSWzhdIC0gUlswXSAqIFJbNV0gKiBSWzddIC0gUlsxXSAqIFJbM10gKiBSWzhdICsgUlsxXSAqIFJbNV0gKiBSWzZdICsgUlsyXSAqIFJbM10gKiBSWzddIC0gUlsyXSAqIFJbNF0gKiBSWzZdO1xuXG5cdFx0Ly8gTm9ybWFsaXplIG1hdHJpeCB2YWx1ZXNcblx0XHRSWzBdIC89IGRldGVybWluYW50O1xuXHRcdFJbMV0gLz0gZGV0ZXJtaW5hbnQ7XG5cdFx0UlsyXSAvPSBkZXRlcm1pbmFudDtcblx0XHRSWzNdIC89IGRldGVybWluYW50O1xuXHRcdFJbNF0gLz0gZGV0ZXJtaW5hbnQ7XG5cdFx0Uls1XSAvPSBkZXRlcm1pbmFudDtcblx0XHRSWzZdIC89IGRldGVybWluYW50O1xuXHRcdFJbN10gLz0gZGV0ZXJtaW5hbnQ7XG5cdFx0Uls4XSAvPSBkZXRlcm1pbmFudDtcblxuXHRcdG1hdHJpeC5lbGVtZW50cyA9IFI7XG5cblx0XHRyZXR1cm4gbWF0cml4O1xuXG5cdH0sXG5cblx0cm90YXRlQnlBeGlzQW5nbGU6IGZ1bmN0aW9uICgpIHtcblxuXHRcdHZhciBvdXRwdXRNYXRyaXggPSBuZXcgRlVMTFRJTFQuUm90YXRpb25NYXRyaXgoKTtcblx0XHR2YXIgdHJhbnNmb3JtTWF0cml4ID0gbmV3IEZVTExUSUxULlJvdGF0aW9uTWF0cml4KCk7XG5cblx0XHR2YXIgc0EsIGNBO1xuXHRcdHZhciB2YWxpZEF4aXMgPSBmYWxzZTtcblxuXHRcdHJldHVybiBmdW5jdGlvbiAoIHRhcmdldFJvdGF0aW9uTWF0cml4LCBheGlzLCBhbmdsZSApIHtcblxuXHRcdFx0dHJhbnNmb3JtTWF0cml4LmlkZW50aXR5KCk7IC8vIHJlc2V0IHRyYW5zZm9ybSBtYXRyaXhcblxuXHRcdFx0dmFsaWRBeGlzID0gZmFsc2U7XG5cblx0XHRcdHNBID0gTWF0aC5zaW4oIGFuZ2xlICk7XG5cdFx0XHRjQSA9IE1hdGguY29zKCBhbmdsZSApO1xuXG5cdFx0XHRpZiAoIGF4aXNbIDAgXSA9PT0gMSAmJiBheGlzWyAxIF0gPT09IDAgJiYgYXhpc1sgMiBdID09PSAwICkgeyAvLyB4XG5cblx0XHRcdFx0dmFsaWRBeGlzID0gdHJ1ZTtcblxuXHRcdFx0XHR0cmFuc2Zvcm1NYXRyaXguZWxlbWVudHNbNF0gPSBjQTtcblx0XHRcdFx0dHJhbnNmb3JtTWF0cml4LmVsZW1lbnRzWzVdID0gLXNBO1xuXHRcdFx0XHR0cmFuc2Zvcm1NYXRyaXguZWxlbWVudHNbN10gPSBzQTtcblx0XHRcdFx0dHJhbnNmb3JtTWF0cml4LmVsZW1lbnRzWzhdID0gY0E7XG5cblx0IFx0XHR9IGVsc2UgaWYgKCBheGlzWyAxIF0gPT09IDEgJiYgYXhpc1sgMCBdID09PSAwICYmIGF4aXNbIDIgXSA9PT0gMCApIHsgLy8geVxuXG5cdFx0XHRcdHZhbGlkQXhpcyA9IHRydWU7XG5cblx0XHRcdFx0dHJhbnNmb3JtTWF0cml4LmVsZW1lbnRzWzBdID0gY0E7XG5cdFx0XHRcdHRyYW5zZm9ybU1hdHJpeC5lbGVtZW50c1syXSA9IHNBO1xuXHRcdFx0XHR0cmFuc2Zvcm1NYXRyaXguZWxlbWVudHNbNl0gPSAtc0E7XG5cdFx0XHRcdHRyYW5zZm9ybU1hdHJpeC5lbGVtZW50c1s4XSA9IGNBO1xuXG5cdCBcdFx0fSBlbHNlIGlmICggYXhpc1sgMiBdID09PSAxICYmIGF4aXNbIDAgXSA9PT0gMCAmJiBheGlzWyAxIF0gPT09IDAgKSB7IC8vIHpcblxuXHRcdFx0XHR2YWxpZEF4aXMgPSB0cnVlO1xuXG5cdFx0XHRcdHRyYW5zZm9ybU1hdHJpeC5lbGVtZW50c1swXSA9IGNBO1xuXHRcdFx0XHR0cmFuc2Zvcm1NYXRyaXguZWxlbWVudHNbMV0gPSAtc0E7XG5cdFx0XHRcdHRyYW5zZm9ybU1hdHJpeC5lbGVtZW50c1szXSA9IHNBO1xuXHRcdFx0XHR0cmFuc2Zvcm1NYXRyaXguZWxlbWVudHNbNF0gPSBjQTtcblxuXHQgXHRcdH1cblxuXHRcdFx0aWYgKCB2YWxpZEF4aXMgKSB7XG5cblx0XHRcdFx0b3V0cHV0TWF0cml4ID0gRlVMTFRJTFQuUm90YXRpb25NYXRyaXgucHJvdG90eXBlLm11bHRpcGx5TWF0cmljZXMoIHRhcmdldFJvdGF0aW9uTWF0cml4LCB0cmFuc2Zvcm1NYXRyaXggKTtcblxuXHRcdFx0XHRvdXRwdXRNYXRyaXggPSBGVUxMVElMVC5Sb3RhdGlvbk1hdHJpeC5wcm90b3R5cGUubm9ybWFsaXplKCBvdXRwdXRNYXRyaXggKTtcblxuXHRcdFx0fSBlbHNlIHtcblxuXHRcdFx0XHRvdXRwdXRNYXRyaXggPSB0YXJnZXRSb3RhdGlvbk1hdHJpeDtcblxuXHRcdFx0fVxuXG5cdFx0XHRyZXR1cm4gb3V0cHV0TWF0cml4O1xuXG5cdFx0fTtcblxuXHR9KClcblxufTtcblxuLy8vLy8vIEZVTExUSUxULkV1bGVyIC8vLy8vL1xuXG5GVUxMVElMVC5FdWxlciA9IGZ1bmN0aW9uICggYWxwaGEsIGJldGEsIGdhbW1hICkge1xuXG5cdHRoaXMuc2V0ID0gZnVuY3Rpb24gKCBhbHBoYSwgYmV0YSwgZ2FtbWEgKSB7XG5cblx0XHR0aGlzLmFscGhhID0gYWxwaGEgfHwgMDtcblx0XHR0aGlzLmJldGEgID0gYmV0YSAgfHwgMDtcblx0XHR0aGlzLmdhbW1hID0gZ2FtbWEgfHwgMDtcblxuXHR9O1xuXG5cdHRoaXMuY29weSA9IGZ1bmN0aW9uICggaW5FdWxlciApIHtcblxuXHRcdHRoaXMuYWxwaGEgPSBpbkV1bGVyLmFscGhhO1xuXHRcdHRoaXMuYmV0YSAgPSBpbkV1bGVyLmJldGE7XG5cdFx0dGhpcy5nYW1tYSA9IGluRXVsZXIuZ2FtbWE7XG5cblx0fTtcblxuXHR0aGlzLnNldEZyb21Sb3RhdGlvbk1hdHJpeCA9IChmdW5jdGlvbiAoKSB7XG5cblx0XHR2YXIgUiwgX2FscGhhLCBfYmV0YSwgX2dhbW1hO1xuXG5cdFx0cmV0dXJuIGZ1bmN0aW9uICggbWF0cml4ICkge1xuXG5cdFx0XHRSID0gbWF0cml4LmVsZW1lbnRzO1xuXG5cdFx0XHRpZiAoUls4XSA+IDApIHsgLy8gY29zKGJldGEpID4gMFxuXG5cdFx0XHRcdF9hbHBoYSA9IE1hdGguYXRhbjIoLVJbMV0sIFJbNF0pO1xuXHRcdFx0XHRfYmV0YSAgPSBNYXRoLmFzaW4oUls3XSk7IC8vIGJldGEgKC1waS8yLCBwaS8yKVxuXHRcdFx0XHRfZ2FtbWEgPSBNYXRoLmF0YW4yKC1SWzZdLCBSWzhdKTsgLy8gZ2FtbWEgKC1waS8yLCBwaS8yKVxuXG5cdFx0XHR9IGVsc2UgaWYgKFJbOF0gPCAwKSB7ICAvLyBjb3MoYmV0YSkgPCAwXG5cblx0XHRcdFx0X2FscGhhID0gTWF0aC5hdGFuMihSWzFdLCAtUls0XSk7XG5cdFx0XHRcdF9iZXRhICA9IC1NYXRoLmFzaW4oUls3XSk7XG5cdFx0XHRcdF9iZXRhICArPSAoX2JldGEgPj0gMCkgPyAtIE1fUEkgOiBNX1BJOyAvLyBiZXRhIFstcGksLXBpLzIpIFUgKHBpLzIscGkpXG5cdFx0XHRcdF9nYW1tYSA9IE1hdGguYXRhbjIoUls2XSwgLVJbOF0pOyAvLyBnYW1tYSAoLXBpLzIsIHBpLzIpXG5cblx0XHRcdH0gZWxzZSB7IC8vIFJbOF0gPT0gMFxuXG5cdFx0XHRcdGlmIChSWzZdID4gMCkgeyAgLy8gY29zKGdhbW1hKSA9PSAwLCBjb3MoYmV0YSkgPiAwXG5cblx0XHRcdFx0XHRfYWxwaGEgPSBNYXRoLmF0YW4yKC1SWzFdLCBSWzRdKTtcblx0XHRcdFx0XHRfYmV0YSAgPSBNYXRoLmFzaW4oUls3XSk7IC8vIGJldGEgWy1waS8yLCBwaS8yXVxuXHRcdFx0XHRcdF9nYW1tYSA9IC0gTV9QSV8yOyAvLyBnYW1tYSA9IC1waS8yXG5cblx0XHRcdFx0fSBlbHNlIGlmIChSWzZdIDwgMCkgeyAvLyBjb3MoZ2FtbWEpID09IDAsIGNvcyhiZXRhKSA8IDBcblxuXHRcdFx0XHRcdF9hbHBoYSA9IE1hdGguYXRhbjIoUlsxXSwgLVJbNF0pO1xuXHRcdFx0XHRcdF9iZXRhICA9IC1NYXRoLmFzaW4oUls3XSk7XG5cdFx0XHRcdFx0X2JldGEgICs9IChfYmV0YSA+PSAwKSA/IC0gTV9QSSA6IE1fUEk7IC8vIGJldGEgWy1waSwtcGkvMikgVSAocGkvMixwaSlcblx0XHRcdFx0XHRfZ2FtbWEgPSAtIE1fUElfMjsgLy8gZ2FtbWEgPSAtcGkvMlxuXG5cdFx0XHRcdH0gZWxzZSB7IC8vIFJbNl0gPT0gMCwgY29zKGJldGEpID09IDBcblxuXHRcdFx0XHRcdC8vIGdpbWJhbCBsb2NrIGRpc2NvbnRpbnVpdHlcblx0XHRcdFx0XHRfYWxwaGEgPSBNYXRoLmF0YW4yKFJbM10sIFJbMF0pO1xuXHRcdFx0XHRcdF9iZXRhICA9IChSWzddID4gMCkgPyBNX1BJXzIgOiAtIE1fUElfMjsgLy8gYmV0YSA9ICstcGkvMlxuXHRcdFx0XHRcdF9nYW1tYSA9IDA7IC8vIGdhbW1hID0gMFxuXG5cdFx0XHRcdH1cblxuXHRcdFx0fVxuXG5cdFx0XHQvLyBhbHBoYSBpcyBpbiBbLXBpLCBwaV0sIG1ha2Ugc3VyZSBpdCBpcyBpbiBbMCwgMipwaSkuXG5cdFx0XHRpZiAoX2FscGhhIDwgMCkge1xuXHRcdFx0XHRfYWxwaGEgKz0gTV8yX1BJOyAvLyBhbHBoYSBbMCwgMipwaSlcblx0XHRcdH1cblxuXHRcdFx0Ly8gQ29udmVydCB0byBkZWdyZWVzXG5cdFx0XHRfYWxwaGEgKj0gcmFkVG9EZWc7XG5cdFx0XHRfYmV0YSAgKj0gcmFkVG9EZWc7XG5cdFx0XHRfZ2FtbWEgKj0gcmFkVG9EZWc7XG5cblx0XHRcdC8vIGFwcGx5IGRlcml2ZWQgZXVsZXIgYW5nbGVzIHRvIGN1cnJlbnQgb2JqZWN0XG5cdFx0XHR0aGlzLnNldCggX2FscGhhLCBfYmV0YSwgX2dhbW1hICk7XG5cblx0XHR9O1xuXG5cdH0pKCk7XG5cblx0dGhpcy5zZXRGcm9tUXVhdGVybmlvbiA9IChmdW5jdGlvbiAoKSB7XG5cblx0XHR2YXIgX2FscGhhLCBfYmV0YSwgX2dhbW1hO1xuXG5cdFx0cmV0dXJuIGZ1bmN0aW9uICggcSApIHtcblxuXHRcdFx0dmFyIHNxdyA9IHEudyAqIHEudztcblx0XHRcdHZhciBzcXggPSBxLnggKiBxLng7XG5cdFx0XHR2YXIgc3F5ID0gcS55ICogcS55O1xuXHRcdFx0dmFyIHNxeiA9IHEueiAqIHEuejtcblxuXHRcdFx0dmFyIHVuaXRMZW5ndGggPSBzcXcgKyBzcXggKyBzcXkgKyBzcXo7IC8vIE5vcm1hbGlzZWQgPT0gMSwgb3RoZXJ3aXNlIGNvcnJlY3Rpb24gZGl2aXNvci5cblx0XHRcdHZhciB3eHl6ID0gcS53ICogcS54ICsgcS55ICogcS56O1xuXHRcdFx0dmFyIGVwc2lsb24gPSAxZS02OyAvLyByb3VuZGluZyBmYWN0b3JcblxuXHRcdFx0aWYgKHd4eXogPiAoMC41IC0gZXBzaWxvbikgKiB1bml0TGVuZ3RoKSB7XG5cblx0XHRcdFx0X2FscGhhID0gMiAqIE1hdGguYXRhbjIocS55LCBxLncpO1xuXHRcdFx0XHRfYmV0YSA9IE1fUElfMjtcblx0XHRcdFx0X2dhbW1hID0gMDtcblxuXHRcdFx0fSBlbHNlIGlmICh3eHl6IDwgKC0wLjUgKyBlcHNpbG9uKSAqIHVuaXRMZW5ndGgpIHtcblxuXHRcdFx0XHRfYWxwaGEgPSAtMiAqIE1hdGguYXRhbjIocS55LCBxLncpO1xuXHRcdFx0XHRfYmV0YSA9IC1NX1BJXzI7XG5cdFx0XHRcdF9nYW1tYSA9IDA7XG5cblx0XHRcdH0gZWxzZSB7XG5cblx0XHRcdFx0dmFyIGFYID0gc3F3IC0gc3F4ICsgc3F5IC0gc3F6O1xuXHRcdFx0XHR2YXIgYVkgPSAyICogKHEudyAqIHEueiAtIHEueCAqIHEueSk7XG5cblx0XHRcdFx0dmFyIGdYID0gc3F3IC0gc3F4IC0gc3F5ICsgc3F6O1xuXHRcdFx0XHR2YXIgZ1kgPSAyICogKHEudyAqIHEueSAtIHEueCAqIHEueik7XG5cblx0XHRcdFx0aWYgKGdYID4gMCkge1xuXG5cdFx0XHRcdFx0X2FscGhhID0gTWF0aC5hdGFuMihhWSwgYVgpO1xuXHRcdFx0XHRcdF9iZXRhICA9IE1hdGguYXNpbigyICogd3h5eiAvIHVuaXRMZW5ndGgpO1xuXHRcdFx0XHRcdF9nYW1tYSA9IE1hdGguYXRhbjIoZ1ksIGdYKTtcblxuXHRcdFx0XHR9IGVsc2Uge1xuXG5cdFx0XHRcdFx0X2FscGhhID0gTWF0aC5hdGFuMigtYVksIC1hWCk7XG5cdFx0XHRcdFx0X2JldGEgID0gLU1hdGguYXNpbigyICogd3h5eiAvIHVuaXRMZW5ndGgpO1xuXHRcdFx0XHRcdF9iZXRhICArPSBfYmV0YSA8IDAgPyBNX1BJIDogLSBNX1BJO1xuXHRcdFx0XHRcdF9nYW1tYSA9IE1hdGguYXRhbjIoLWdZLCAtZ1gpO1xuXG5cdFx0XHRcdH1cblxuXHRcdFx0fVxuXG5cdFx0XHQvLyBhbHBoYSBpcyBpbiBbLXBpLCBwaV0sIG1ha2Ugc3VyZSBpdCBpcyBpbiBbMCwgMipwaSkuXG5cdFx0XHRpZiAoX2FscGhhIDwgMCkge1xuXHRcdFx0XHRfYWxwaGEgKz0gTV8yX1BJOyAvLyBhbHBoYSBbMCwgMipwaSlcblx0XHRcdH1cblxuXHRcdFx0Ly8gQ29udmVydCB0byBkZWdyZWVzXG5cdFx0XHRfYWxwaGEgKj0gcmFkVG9EZWc7XG5cdFx0XHRfYmV0YSAgKj0gcmFkVG9EZWc7XG5cdFx0XHRfZ2FtbWEgKj0gcmFkVG9EZWc7XG5cblx0XHRcdC8vIGFwcGx5IGRlcml2ZWQgZXVsZXIgYW5nbGVzIHRvIGN1cnJlbnQgb2JqZWN0XG5cdFx0XHR0aGlzLnNldCggX2FscGhhLCBfYmV0YSwgX2dhbW1hICk7XG5cblx0XHR9O1xuXG5cdH0pKCk7XG5cblx0dGhpcy5yb3RhdGVYID0gZnVuY3Rpb24gKCBhbmdsZSApIHtcblxuXHRcdEZVTExUSUxULkV1bGVyLnByb3RvdHlwZS5yb3RhdGVCeUF4aXNBbmdsZSggdGhpcywgWyAxLCAwLCAwIF0sIGFuZ2xlICk7XG5cblx0XHRyZXR1cm4gdGhpcztcblxuXHR9O1xuXG5cdHRoaXMucm90YXRlWSA9IGZ1bmN0aW9uICggYW5nbGUgKSB7XG5cblx0XHRGVUxMVElMVC5FdWxlci5wcm90b3R5cGUucm90YXRlQnlBeGlzQW5nbGUoIHRoaXMsIFsgMCwgMSwgMCBdLCBhbmdsZSApO1xuXG5cdFx0cmV0dXJuIHRoaXM7XG5cblx0fTtcblxuXHR0aGlzLnJvdGF0ZVogPSBmdW5jdGlvbiAoIGFuZ2xlICkge1xuXG5cdFx0RlVMTFRJTFQuRXVsZXIucHJvdG90eXBlLnJvdGF0ZUJ5QXhpc0FuZ2xlKCB0aGlzLCBbIDAsIDAsIDEgXSwgYW5nbGUgKTtcblxuXHRcdHJldHVybiB0aGlzO1xuXG5cdH07XG5cblx0Ly8gSW5pdGlhbGl6ZSBvYmplY3QgdmFsdWVzXG5cdHRoaXMuc2V0KCBhbHBoYSwgYmV0YSwgZ2FtbWEgKTtcblxufTtcblxuRlVMTFRJTFQuRXVsZXIucHJvdG90eXBlID0ge1xuXG5cdGNvbnN0cnVjdG9yOiBGVUxMVElMVC5FdWxlcixcblxuXHRyb3RhdGVCeUF4aXNBbmdsZTogZnVuY3Rpb24gKCkge1xuXG5cdFx0dmFyIF9tYXRyaXggPSBuZXcgRlVMTFRJTFQuUm90YXRpb25NYXRyaXgoKTtcblx0XHR2YXIgb3V0RXVsZXI7XG5cblx0XHRyZXR1cm4gZnVuY3Rpb24gKCB0YXJnZXRFdWxlciwgYXhpcywgYW5nbGUgKSB7XG5cblx0XHRcdF9tYXRyaXguc2V0RnJvbUV1bGVyKCB0YXJnZXRFdWxlciApO1xuXG5cdFx0XHRfbWF0cml4ID0gRlVMTFRJTFQuUm90YXRpb25NYXRyaXgucHJvdG90eXBlLnJvdGF0ZUJ5QXhpc0FuZ2xlKCBfbWF0cml4LCBheGlzLCBhbmdsZSApO1xuXG5cdFx0XHR0YXJnZXRFdWxlci5zZXRGcm9tUm90YXRpb25NYXRyaXgoIF9tYXRyaXggKTtcblxuXHRcdFx0cmV0dXJuIHRhcmdldEV1bGVyO1xuXG5cdFx0fTtcblxuXHR9KClcblxufTtcblxuLy8vLy8gRlVMTFRJTFQuRGV2aWNlT3JpZW50YXRpb24gLy8vLy8vXG5cbkZVTExUSUxULkRldmljZU9yaWVudGF0aW9uID0gZnVuY3Rpb24gKG9wdGlvbnMpIHtcblxuXHR0aGlzLm9wdGlvbnMgPSBvcHRpb25zIHx8IHt9OyAvLyBieSBkZWZhdWx0IHVzZSBVQSBkZXZpY2VvcmllbnRhdGlvbiAndHlwZScgKFwiZ2FtZVwiIG9uIGlPUywgXCJ3b3JsZFwiIG9uIEFuZHJvaWQpXG5cblx0dmFyIHRyaWVzID0gMDtcblx0dmFyIG1heFRyaWVzID0gMjAwO1xuXHR2YXIgc3VjY2Vzc0NvdW50ID0gMDtcblx0dmFyIHN1Y2Nlc3NUaHJlc2hvbGQgPSAxMDtcblxuXHR0aGlzLmFscGhhT2Zmc2V0U2NyZWVuID0gMDtcblx0dGhpcy5hbHBoYU9mZnNldERldmljZSA9IHVuZGVmaW5lZDtcblxuXHQvLyBDcmVhdGUgYSBnYW1lLWJhc2VkIGRldmljZW9yaWVudGF0aW9uIG9iamVjdCAoaW5pdGlhbCBhbHBoYSA9PT0gMCBkZWdyZWVzKVxuXHRpZiAodGhpcy5vcHRpb25zLnR5cGUgPT09IFwiZ2FtZVwiKSB7XG5cblx0XHR2YXIgc2V0R2FtZUFscGhhT2Zmc2V0ID0gZnVuY3Rpb24oZXZ0KSB7XG5cblx0XHRcdGlmIChldnQuYWxwaGEgIT09IG51bGwpIHsgLy8gZG8gcmVnYXJkbGVzcyBvZiB3aGV0aGVyICdldnQuYWJzb2x1dGUnIGlzIGFsc28gdHJ1ZVxuXHRcdFx0XHR0aGlzLmFscGhhT2Zmc2V0RGV2aWNlID0gbmV3IEZVTExUSUxULkV1bGVyKGV2dC5hbHBoYSwgMCwgMCk7XG5cdFx0XHRcdHRoaXMuYWxwaGFPZmZzZXREZXZpY2Uucm90YXRlWiggLXNjcmVlbk9yaWVudGF0aW9uQW5nbGUgKTtcblxuXHRcdFx0XHQvLyBEaXNjYXJkIGZpcnN0IHtzdWNjZXNzVGhyZXNob2xkfSByZXNwb25zZXMgd2hpbGUgYSBiZXR0ZXIgY29tcGFzcyBsb2NrIGlzIGZvdW5kIGJ5IFVBXG5cdFx0XHRcdGlmKCsrc3VjY2Vzc0NvdW50ID49IHN1Y2Nlc3NUaHJlc2hvbGQpIHtcblx0XHRcdFx0XHR3aW5kb3cucmVtb3ZlRXZlbnRMaXN0ZW5lciggJ2RldmljZW9yaWVudGF0aW9uJywgc2V0R2FtZUFscGhhT2Zmc2V0LCBmYWxzZSApO1xuXHRcdFx0XHRcdHJldHVybjtcblx0XHRcdFx0fVxuXHRcdFx0fVxuXG5cdFx0XHRpZigrK3RyaWVzID49IG1heFRyaWVzKSB7XG5cdFx0XHRcdHdpbmRvdy5yZW1vdmVFdmVudExpc3RlbmVyKCAnZGV2aWNlb3JpZW50YXRpb24nLCBzZXRHYW1lQWxwaGFPZmZzZXQsIGZhbHNlICk7XG5cdFx0XHR9XG5cblx0XHR9LmJpbmQodGhpcyk7XG5cblx0XHR3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lciggJ2RldmljZW9yaWVudGF0aW9uJywgc2V0R2FtZUFscGhhT2Zmc2V0LCBmYWxzZSApO1xuXG5cdC8vIENyZWF0ZSBhIGNvbXBhc3MtYmFzZWQgZGV2aWNlb3JpZW50YXRpb24gb2JqZWN0IChpbml0aWFsIGFscGhhID09PSBjb21wYXNzIGRlZ3JlZXMpXG5cdH0gZWxzZSBpZiAodGhpcy5vcHRpb25zLnR5cGUgPT09IFwid29ybGRcIikge1xuXG5cdFx0dmFyIHNldENvbXBhc3NBbHBoYU9mZnNldCA9IGZ1bmN0aW9uKGV2dCkge1xuXG5cdFx0XHRpZiAoZXZ0LmFic29sdXRlICE9PSB0cnVlICYmIGV2dC53ZWJraXRDb21wYXNzQWNjdXJhY3kgIT09IHVuZGVmaW5lZCAmJiBldnQud2Via2l0Q29tcGFzc0FjY3VyYWN5ICE9PSBudWxsICYmICtldnQud2Via2l0Q29tcGFzc0FjY3VyYWN5ID49IDAgJiYgK2V2dC53ZWJraXRDb21wYXNzQWNjdXJhY3kgPCA1MCkge1xuXHRcdFx0XHR0aGlzLmFscGhhT2Zmc2V0RGV2aWNlID0gbmV3IEZVTExUSUxULkV1bGVyKGV2dC53ZWJraXRDb21wYXNzSGVhZGluZywgMCwgMCk7XG5cdFx0XHRcdHRoaXMuYWxwaGFPZmZzZXREZXZpY2Uucm90YXRlWiggc2NyZWVuT3JpZW50YXRpb25BbmdsZSApO1xuXHRcdFx0XHR0aGlzLmFscGhhT2Zmc2V0U2NyZWVuID0gc2NyZWVuT3JpZW50YXRpb25BbmdsZTtcblxuXHRcdFx0XHQvLyBEaXNjYXJkIGZpcnN0IHtzdWNjZXNzVGhyZXNob2xkfSByZXNwb25zZXMgd2hpbGUgYSBiZXR0ZXIgY29tcGFzcyBsb2NrIGlzIGZvdW5kIGJ5IFVBXG5cdFx0XHRcdGlmKCsrc3VjY2Vzc0NvdW50ID49IHN1Y2Nlc3NUaHJlc2hvbGQpIHtcblx0XHRcdFx0XHR3aW5kb3cucmVtb3ZlRXZlbnRMaXN0ZW5lciggJ2RldmljZW9yaWVudGF0aW9uJywgc2V0Q29tcGFzc0FscGhhT2Zmc2V0LCBmYWxzZSApO1xuXHRcdFx0XHRcdHJldHVybjtcblx0XHRcdFx0fVxuXHRcdFx0fVxuXG5cdFx0XHRpZigrK3RyaWVzID49IG1heFRyaWVzKSB7XG5cdFx0XHRcdHdpbmRvdy5yZW1vdmVFdmVudExpc3RlbmVyKCAnZGV2aWNlb3JpZW50YXRpb24nLCBzZXRDb21wYXNzQWxwaGFPZmZzZXQsIGZhbHNlICk7XG5cdFx0XHR9XG5cblx0XHR9LmJpbmQodGhpcyk7XG5cblx0XHR3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lciggJ2RldmljZW9yaWVudGF0aW9uJywgc2V0Q29tcGFzc0FscGhhT2Zmc2V0LCBmYWxzZSApO1xuXG5cdH0gLy8gZWxzZS4uLiB1c2Ugd2hhdGV2ZXIgb3JpZW50YXRpb24gc3lzdGVtIHRoZSBVQSBwcm92aWRlcyAoXCJnYW1lXCIgb24gaU9TLCBcIndvcmxkXCIgb24gQW5kcm9pZClcblxufTtcblxuRlVMTFRJTFQuRGV2aWNlT3JpZW50YXRpb24ucHJvdG90eXBlID0ge1xuXG5cdGNvbnN0cnVjdG9yOiBGVUxMVElMVC5EZXZpY2VPcmllbnRhdGlvbixcblxuXHRzdGFydDogZnVuY3Rpb24gKCBjYWxsYmFjayApIHtcblxuXHRcdGlmICggY2FsbGJhY2sgJiYgT2JqZWN0LnByb3RvdHlwZS50b1N0cmluZy5jYWxsKCBjYWxsYmFjayApID09ICdbb2JqZWN0IEZ1bmN0aW9uXScgKSB7XG5cblx0XHRcdHNlbnNvcnMub3JpZW50YXRpb24uY2FsbGJhY2tzLnB1c2goIGNhbGxiYWNrICk7XG5cblx0XHR9XG5cblx0XHRpZiggIXNjcmVlbkFjdGl2ZSApIHtcblxuXHRcdFx0aWYgKCBoYXNTY3JlZW5PcmllbnRhdGlvbkFQSSApIHtcblxuXHRcdFx0d2luZG93LnNjcmVlbi5vcmllbnRhdGlvbi5hZGRFdmVudExpc3RlbmVyKCAnY2hhbmdlJywgaGFuZGxlU2NyZWVuT3JpZW50YXRpb25DaGFuZ2UsIGZhbHNlICk7XG5cblx0XHRcdH0gZWxzZSB7XG5cblx0XHRcdFx0d2luZG93LmFkZEV2ZW50TGlzdGVuZXIoICdvcmllbnRhdGlvbmNoYW5nZScsIGhhbmRsZVNjcmVlbk9yaWVudGF0aW9uQ2hhbmdlLCBmYWxzZSApO1xuXG5cdFx0XHR9XG5cblx0XHR9XG5cblx0XHRpZiAoICFzZW5zb3JzLm9yaWVudGF0aW9uLmFjdGl2ZSApIHtcblxuXHRcdFx0d2luZG93LmFkZEV2ZW50TGlzdGVuZXIoICdkZXZpY2VvcmllbnRhdGlvbicsIGhhbmRsZURldmljZU9yaWVudGF0aW9uQ2hhbmdlLCBmYWxzZSApO1xuXG5cdFx0XHRzZW5zb3JzLm9yaWVudGF0aW9uLmFjdGl2ZSA9IHRydWU7XG5cblx0XHR9XG5cblx0fSxcblxuXHRzdG9wOiBmdW5jdGlvbiAoKSB7XG5cblx0XHRpZiAoIHNlbnNvcnMub3JpZW50YXRpb24uYWN0aXZlICkge1xuXG5cdFx0XHR3aW5kb3cucmVtb3ZlRXZlbnRMaXN0ZW5lciggJ2RldmljZW9yaWVudGF0aW9uJywgaGFuZGxlRGV2aWNlT3JpZW50YXRpb25DaGFuZ2UsIGZhbHNlICk7XG5cblx0XHRcdHNlbnNvcnMub3JpZW50YXRpb24uYWN0aXZlID0gZmFsc2U7XG5cblx0XHR9XG5cblx0fSxcblxuXHRsaXN0ZW46IGZ1bmN0aW9uKCBjYWxsYmFjayApIHtcblxuXHRcdHRoaXMuc3RhcnQoIGNhbGxiYWNrICk7XG5cblx0fSxcblxuXHRnZXRGaXhlZEZyYW1lUXVhdGVybmlvbjogKGZ1bmN0aW9uICgpIHtcblxuXHRcdHZhciBldWxlciA9IG5ldyBGVUxMVElMVC5FdWxlcigpO1xuXHRcdHZhciBtYXRyaXggPSBuZXcgRlVMTFRJTFQuUm90YXRpb25NYXRyaXgoKTtcblx0XHR2YXIgcXVhdGVybmlvbiA9IG5ldyBGVUxMVElMVC5RdWF0ZXJuaW9uKCk7XG5cblx0XHRyZXR1cm4gZnVuY3Rpb24oKSB7XG5cblx0XHRcdHZhciBvcmllbnRhdGlvbkRhdGEgPSBzZW5zb3JzLm9yaWVudGF0aW9uLmRhdGEgfHwgeyBhbHBoYTogMCwgYmV0YTogMCwgZ2FtbWE6IDAgfTtcblxuXHRcdFx0dmFyIGFkanVzdGVkQWxwaGEgPSBvcmllbnRhdGlvbkRhdGEuYWxwaGE7XG5cblx0XHRcdGlmICh0aGlzLmFscGhhT2Zmc2V0RGV2aWNlKSB7XG5cdFx0XHRcdG1hdHJpeC5zZXRGcm9tRXVsZXIoIHRoaXMuYWxwaGFPZmZzZXREZXZpY2UgKTtcblx0XHRcdFx0bWF0cml4LnJvdGF0ZVooIC0gdGhpcy5hbHBoYU9mZnNldFNjcmVlbiApO1xuXHRcdFx0XHRldWxlci5zZXRGcm9tUm90YXRpb25NYXRyaXgoIG1hdHJpeCApO1xuXG5cdFx0XHRcdGlmIChldWxlci5hbHBoYSA8IDApIHtcblx0XHRcdFx0XHRldWxlci5hbHBoYSArPSAzNjA7XG5cdFx0XHRcdH1cblxuXHRcdFx0XHRldWxlci5hbHBoYSAlPSAzNjA7XG5cblx0XHRcdFx0YWRqdXN0ZWRBbHBoYSAtPSBldWxlci5hbHBoYTtcblx0XHRcdH1cblxuXHRcdFx0ZXVsZXIuc2V0KFxuXHRcdFx0XHRhZGp1c3RlZEFscGhhLFxuXHRcdFx0XHRvcmllbnRhdGlvbkRhdGEuYmV0YSxcblx0XHRcdFx0b3JpZW50YXRpb25EYXRhLmdhbW1hXG5cdFx0XHQpO1xuXG5cdFx0XHRxdWF0ZXJuaW9uLnNldEZyb21FdWxlciggZXVsZXIgKTtcblxuXHRcdFx0cmV0dXJuIHF1YXRlcm5pb247XG5cblx0XHR9O1xuXG5cdH0pKCksXG5cblx0Z2V0U2NyZWVuQWRqdXN0ZWRRdWF0ZXJuaW9uOiAoZnVuY3Rpb24gKCkge1xuXG5cdFx0dmFyIHF1YXRlcm5pb247XG5cblx0XHRyZXR1cm4gZnVuY3Rpb24oKSB7XG5cblx0XHRcdHF1YXRlcm5pb24gPSB0aGlzLmdldEZpeGVkRnJhbWVRdWF0ZXJuaW9uKCk7XG5cblx0XHRcdC8vIEF1dG9tYXRpY2FsbHkgYXBwbHkgc2NyZWVuIG9yaWVudGF0aW9uIHRyYW5zZm9ybVxuXHRcdFx0cXVhdGVybmlvbi5yb3RhdGVaKCAtIHNjcmVlbk9yaWVudGF0aW9uQW5nbGUgKTtcblxuXHRcdFx0cmV0dXJuIHF1YXRlcm5pb247XG5cblx0XHR9O1xuXG5cdH0pKCksXG5cblx0Z2V0Rml4ZWRGcmFtZU1hdHJpeDogKGZ1bmN0aW9uICgpIHtcblxuXHRcdHZhciBldWxlciA9IG5ldyBGVUxMVElMVC5FdWxlcigpO1xuXHRcdHZhciBtYXRyaXggPSBuZXcgRlVMTFRJTFQuUm90YXRpb25NYXRyaXgoKTtcblxuXHRcdHJldHVybiBmdW5jdGlvbiAoKSB7XG5cblx0XHRcdHZhciBvcmllbnRhdGlvbkRhdGEgPSBzZW5zb3JzLm9yaWVudGF0aW9uLmRhdGEgfHwgeyBhbHBoYTogMCwgYmV0YTogMCwgZ2FtbWE6IDAgfTtcblxuXHRcdFx0dmFyIGFkanVzdGVkQWxwaGEgPSBvcmllbnRhdGlvbkRhdGEuYWxwaGE7XG5cblx0XHRcdGlmICh0aGlzLmFscGhhT2Zmc2V0RGV2aWNlKSB7XG5cdFx0XHRcdG1hdHJpeC5zZXRGcm9tRXVsZXIoIHRoaXMuYWxwaGFPZmZzZXREZXZpY2UgKTtcblx0XHRcdFx0bWF0cml4LnJvdGF0ZVooIC0gdGhpcy5hbHBoYU9mZnNldFNjcmVlbiApO1xuXHRcdFx0XHRldWxlci5zZXRGcm9tUm90YXRpb25NYXRyaXgoIG1hdHJpeCApO1xuXG5cdFx0XHRcdGlmIChldWxlci5hbHBoYSA8IDApIHtcblx0XHRcdFx0XHRldWxlci5hbHBoYSArPSAzNjA7XG5cdFx0XHRcdH1cblxuXHRcdFx0XHRldWxlci5hbHBoYSAlPSAzNjA7XG5cblx0XHRcdFx0YWRqdXN0ZWRBbHBoYSAtPSBldWxlci5hbHBoYTtcblx0XHRcdH1cblxuXHRcdFx0ZXVsZXIuc2V0KFxuXHRcdFx0XHRhZGp1c3RlZEFscGhhLFxuXHRcdFx0XHRvcmllbnRhdGlvbkRhdGEuYmV0YSxcblx0XHRcdFx0b3JpZW50YXRpb25EYXRhLmdhbW1hXG5cdFx0XHQpO1xuXG5cdFx0XHRtYXRyaXguc2V0RnJvbUV1bGVyKCBldWxlciApO1xuXG5cdFx0XHRyZXR1cm4gbWF0cml4O1xuXG5cdFx0fTtcblxuXHR9KSgpLFxuXG5cdGdldFNjcmVlbkFkanVzdGVkTWF0cml4OiAoZnVuY3Rpb24gKCkge1xuXG5cdFx0dmFyIG1hdHJpeDtcblxuXHRcdHJldHVybiBmdW5jdGlvbiAoKSB7XG5cblx0XHRcdG1hdHJpeCA9IHRoaXMuZ2V0Rml4ZWRGcmFtZU1hdHJpeCgpO1xuXG5cdFx0XHQvLyBBdXRvbWF0aWNhbGx5IGFwcGx5IHNjcmVlbiBvcmllbnRhdGlvbiB0cmFuc2Zvcm1cblx0XHRcdG1hdHJpeC5yb3RhdGVaKCAtIHNjcmVlbk9yaWVudGF0aW9uQW5nbGUgKTtcblxuXHRcdFx0cmV0dXJuIG1hdHJpeDtcblxuXHRcdH07XG5cblx0fSkoKSxcblxuXHRnZXRGaXhlZEZyYW1lRXVsZXI6IChmdW5jdGlvbiAoKSB7XG5cblx0XHR2YXIgZXVsZXIgPSBuZXcgRlVMTFRJTFQuRXVsZXIoKTtcblx0XHR2YXIgbWF0cml4O1xuXG5cdFx0cmV0dXJuIGZ1bmN0aW9uICgpIHtcblxuXHRcdFx0bWF0cml4ID0gdGhpcy5nZXRGaXhlZEZyYW1lTWF0cml4KCk7XG5cblx0XHRcdGV1bGVyLnNldEZyb21Sb3RhdGlvbk1hdHJpeCggbWF0cml4ICk7XG5cblx0XHRcdHJldHVybiBldWxlcjtcblxuXHRcdH07XG5cblx0fSkoKSxcblxuXHRnZXRTY3JlZW5BZGp1c3RlZEV1bGVyOiAoZnVuY3Rpb24gKCkge1xuXG5cdFx0dmFyIGV1bGVyID0gbmV3IEZVTExUSUxULkV1bGVyKCk7XG5cdFx0dmFyIG1hdHJpeDtcblxuXHRcdHJldHVybiBmdW5jdGlvbiAoKSB7XG5cblx0XHRcdG1hdHJpeCA9IHRoaXMuZ2V0U2NyZWVuQWRqdXN0ZWRNYXRyaXgoKTtcblxuXHRcdFx0ZXVsZXIuc2V0RnJvbVJvdGF0aW9uTWF0cml4KCBtYXRyaXggKTtcblxuXHRcdFx0cmV0dXJuIGV1bGVyO1xuXG5cdFx0fTtcblxuXHR9KSgpLFxuXG5cdGlzQWJzb2x1dGU6IGZ1bmN0aW9uICgpIHtcblxuXHRcdGlmICggc2Vuc29ycy5vcmllbnRhdGlvbi5kYXRhICYmIHNlbnNvcnMub3JpZW50YXRpb24uZGF0YS5hYnNvbHV0ZSA9PT0gdHJ1ZSApIHtcblx0XHRcdHJldHVybiB0cnVlO1xuXHRcdH1cblxuXHRcdHJldHVybiBmYWxzZTtcblxuXHR9LFxuXG5cdGdldExhc3RSYXdFdmVudERhdGE6IGZ1bmN0aW9uICgpIHtcblxuXHRcdHJldHVybiBzZW5zb3JzLm9yaWVudGF0aW9uLmRhdGEgfHwge307XG5cblx0fSxcblxuXHRBTFBIQTogJ2FscGhhJyxcblx0QkVUQTogJ2JldGEnLFxuXHRHQU1NQTogJ2dhbW1hJ1xuXG59O1xuXG5cbi8vLy8vIEZVTExUSUxULkRldmljZU1vdGlvbiAvLy8vLy9cblxuRlVMTFRJTFQuRGV2aWNlTW90aW9uID0gZnVuY3Rpb24gKG9wdGlvbnMpIHtcblxuXHR0aGlzLm9wdGlvbnMgPSBvcHRpb25zIHx8IHt9OyAvLyBwbGFjZWhvbGRlciBvYmplY3Qgc2luY2Ugbm8gb3B0aW9ucyBhcmUgY3VycmVudGx5IHN1cHBvcnRlZFxuXG59O1xuXG5GVUxMVElMVC5EZXZpY2VNb3Rpb24ucHJvdG90eXBlID0ge1xuXG5cdGNvbnN0cnVjdG9yOiBGVUxMVElMVC5EZXZpY2VNb3Rpb24sXG5cblx0c3RhcnQ6IGZ1bmN0aW9uICggY2FsbGJhY2sgKSB7XG5cblx0XHRpZiAoIGNhbGxiYWNrICYmIE9iamVjdC5wcm90b3R5cGUudG9TdHJpbmcuY2FsbCggY2FsbGJhY2sgKSA9PSAnW29iamVjdCBGdW5jdGlvbl0nICkge1xuXG5cdFx0XHRzZW5zb3JzLm1vdGlvbi5jYWxsYmFja3MucHVzaCggY2FsbGJhY2sgKTtcblxuXHRcdH1cblxuXHRcdGlmKCAhc2NyZWVuQWN0aXZlICkge1xuXG5cdFx0XHRpZiAoIGhhc1NjcmVlbk9yaWVudGF0aW9uQVBJICkge1xuXG5cdFx0XHRcdHdpbmRvdy5zY3JlZW4ub3JpZW50YXRpb24uYWRkRXZlbnRMaXN0ZW5lciggJ2NoYW5nZScsIGhhbmRsZVNjcmVlbk9yaWVudGF0aW9uQ2hhbmdlLCBmYWxzZSApO1xuXG5cdFx0XHR9IGVsc2Uge1xuXG5cdFx0XHRcdHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKCAnb3JpZW50YXRpb25jaGFuZ2UnLCBoYW5kbGVTY3JlZW5PcmllbnRhdGlvbkNoYW5nZSwgZmFsc2UgKTtcblxuXHRcdFx0fVxuXG5cdFx0fVxuXG5cdFx0aWYgKCAhc2Vuc29ycy5tb3Rpb24uYWN0aXZlICkge1xuXG5cdFx0XHR3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lciggJ2RldmljZW1vdGlvbicsIGhhbmRsZURldmljZU1vdGlvbkNoYW5nZSwgZmFsc2UgKTtcblxuXHRcdFx0c2Vuc29ycy5tb3Rpb24uYWN0aXZlID0gdHJ1ZTtcblxuXHRcdH1cblxuXHR9LFxuXG5cdHN0b3A6IGZ1bmN0aW9uICgpIHtcblxuXHRcdGlmICggc2Vuc29ycy5tb3Rpb24uYWN0aXZlICkge1xuXG5cdFx0XHR3aW5kb3cucmVtb3ZlRXZlbnRMaXN0ZW5lciggJ2RldmljZW1vdGlvbicsIGhhbmRsZURldmljZU1vdGlvbkNoYW5nZSwgZmFsc2UgKTtcblxuXHRcdFx0c2Vuc29ycy5tb3Rpb24uYWN0aXZlID0gZmFsc2U7XG5cblx0XHR9XG5cblx0fSxcblxuXHRsaXN0ZW46IGZ1bmN0aW9uKCBjYWxsYmFjayApIHtcblxuXHRcdHRoaXMuc3RhcnQoIGNhbGxiYWNrICk7XG5cblx0fSxcblxuXHRnZXRTY3JlZW5BZGp1c3RlZEFjY2VsZXJhdGlvbjogZnVuY3Rpb24gKCkge1xuXG5cdFx0dmFyIGFjY0RhdGEgPSBzZW5zb3JzLm1vdGlvbi5kYXRhICYmIHNlbnNvcnMubW90aW9uLmRhdGEuYWNjZWxlcmF0aW9uID8gc2Vuc29ycy5tb3Rpb24uZGF0YS5hY2NlbGVyYXRpb24gOiB7IHg6IDAsIHk6IDAsIHo6IDAgfTtcblx0XHR2YXIgc2NyZWVuQWNjRGF0YSA9IHt9O1xuXG5cdFx0c3dpdGNoICggc2NyZWVuT3JpZW50YXRpb25BbmdsZSApIHtcblx0XHRcdGNhc2UgU0NSRUVOX1JPVEFUSU9OXzkwOlxuXHRcdFx0XHRzY3JlZW5BY2NEYXRhLnggPSAtIGFjY0RhdGEueTtcblx0XHRcdFx0c2NyZWVuQWNjRGF0YS55ID0gICBhY2NEYXRhLng7XG5cdFx0XHRcdGJyZWFrO1xuXHRcdFx0Y2FzZSBTQ1JFRU5fUk9UQVRJT05fMTgwOlxuXHRcdFx0XHRzY3JlZW5BY2NEYXRhLnggPSAtIGFjY0RhdGEueDtcblx0XHRcdFx0c2NyZWVuQWNjRGF0YS55ID0gLSBhY2NEYXRhLnk7XG5cdFx0XHRcdGJyZWFrO1xuXHRcdFx0Y2FzZSBTQ1JFRU5fUk9UQVRJT05fMjcwOlxuXHRcdFx0Y2FzZSBTQ1JFRU5fUk9UQVRJT05fTUlOVVNfOTA6XG5cdFx0XHRcdHNjcmVlbkFjY0RhdGEueCA9ICAgYWNjRGF0YS55O1xuXHRcdFx0XHRzY3JlZW5BY2NEYXRhLnkgPSAtIGFjY0RhdGEueDtcblx0XHRcdFx0YnJlYWs7XG5cdFx0XHRkZWZhdWx0OiAvLyBTQ1JFRU5fUk9UQVRJT05fMFxuXHRcdFx0XHRzY3JlZW5BY2NEYXRhLnggPSAgIGFjY0RhdGEueDtcblx0XHRcdFx0c2NyZWVuQWNjRGF0YS55ID0gICBhY2NEYXRhLnk7XG5cdFx0XHRcdGJyZWFrO1xuXHRcdH1cblxuXHRcdHNjcmVlbkFjY0RhdGEueiA9IGFjY0RhdGEuejtcblxuXHRcdHJldHVybiBzY3JlZW5BY2NEYXRhO1xuXG5cdH0sXG5cblx0Z2V0U2NyZWVuQWRqdXN0ZWRBY2NlbGVyYXRpb25JbmNsdWRpbmdHcmF2aXR5OiBmdW5jdGlvbiAoKSB7XG5cblx0XHR2YXIgYWNjR0RhdGEgPSBzZW5zb3JzLm1vdGlvbi5kYXRhICYmIHNlbnNvcnMubW90aW9uLmRhdGEuYWNjZWxlcmF0aW9uSW5jbHVkaW5nR3Jhdml0eSA/IHNlbnNvcnMubW90aW9uLmRhdGEuYWNjZWxlcmF0aW9uSW5jbHVkaW5nR3Jhdml0eSA6IHsgeDogMCwgeTogMCwgejogMCB9O1xuXHRcdHZhciBzY3JlZW5BY2NHRGF0YSA9IHt9O1xuXG5cdFx0c3dpdGNoICggc2NyZWVuT3JpZW50YXRpb25BbmdsZSApIHtcblx0XHRcdGNhc2UgU0NSRUVOX1JPVEFUSU9OXzkwOlxuXHRcdFx0XHRzY3JlZW5BY2NHRGF0YS54ID0gLSBhY2NHRGF0YS55O1xuXHRcdFx0XHRzY3JlZW5BY2NHRGF0YS55ID0gICBhY2NHRGF0YS54O1xuXHRcdFx0XHRicmVhaztcblx0XHRcdGNhc2UgU0NSRUVOX1JPVEFUSU9OXzE4MDpcblx0XHRcdFx0c2NyZWVuQWNjR0RhdGEueCA9IC0gYWNjR0RhdGEueDtcblx0XHRcdFx0c2NyZWVuQWNjR0RhdGEueSA9IC0gYWNjR0RhdGEueTtcblx0XHRcdFx0YnJlYWs7XG5cdFx0XHRjYXNlIFNDUkVFTl9ST1RBVElPTl8yNzA6XG5cdFx0XHRjYXNlIFNDUkVFTl9ST1RBVElPTl9NSU5VU185MDpcblx0XHRcdFx0c2NyZWVuQWNjR0RhdGEueCA9ICAgYWNjR0RhdGEueTtcblx0XHRcdFx0c2NyZWVuQWNjR0RhdGEueSA9IC0gYWNjR0RhdGEueDtcblx0XHRcdFx0YnJlYWs7XG5cdFx0XHRkZWZhdWx0OiAvLyBTQ1JFRU5fUk9UQVRJT05fMFxuXHRcdFx0XHRzY3JlZW5BY2NHRGF0YS54ID0gICBhY2NHRGF0YS54O1xuXHRcdFx0XHRzY3JlZW5BY2NHRGF0YS55ID0gICBhY2NHRGF0YS55O1xuXHRcdFx0XHRicmVhaztcblx0XHR9XG5cblx0XHRzY3JlZW5BY2NHRGF0YS56ID0gYWNjR0RhdGEuejtcblxuXHRcdHJldHVybiBzY3JlZW5BY2NHRGF0YTtcblxuXHR9LFxuXG5cdGdldFNjcmVlbkFkanVzdGVkUm90YXRpb25SYXRlOiBmdW5jdGlvbiAoKSB7XG5cblx0XHR2YXIgcm90UmF0ZURhdGEgPSBzZW5zb3JzLm1vdGlvbi5kYXRhICYmIHNlbnNvcnMubW90aW9uLmRhdGEucm90YXRpb25SYXRlID8gc2Vuc29ycy5tb3Rpb24uZGF0YS5yb3RhdGlvblJhdGUgOiB7IGFscGhhOiAwLCBiZXRhOiAwLCBnYW1tYTogMCB9O1xuXHRcdHZhciBzY3JlZW5Sb3RSYXRlRGF0YSA9IHt9O1xuXG5cdFx0c3dpdGNoICggc2NyZWVuT3JpZW50YXRpb25BbmdsZSApIHtcblx0XHRcdGNhc2UgU0NSRUVOX1JPVEFUSU9OXzkwOlxuXHRcdFx0XHRzY3JlZW5Sb3RSYXRlRGF0YS5iZXRhICA9IC0gcm90UmF0ZURhdGEuZ2FtbWE7XG5cdFx0XHRcdHNjcmVlblJvdFJhdGVEYXRhLmdhbW1hID0gICByb3RSYXRlRGF0YS5iZXRhO1xuXHRcdFx0XHRicmVhaztcblx0XHRcdGNhc2UgU0NSRUVOX1JPVEFUSU9OXzE4MDpcblx0XHRcdFx0c2NyZWVuUm90UmF0ZURhdGEuYmV0YSAgPSAtIHJvdFJhdGVEYXRhLmJldGE7XG5cdFx0XHRcdHNjcmVlblJvdFJhdGVEYXRhLmdhbW1hID0gLSByb3RSYXRlRGF0YS5nYW1tYTtcblx0XHRcdFx0YnJlYWs7XG5cdFx0XHRjYXNlIFNDUkVFTl9ST1RBVElPTl8yNzA6XG5cdFx0XHRjYXNlIFNDUkVFTl9ST1RBVElPTl9NSU5VU185MDpcblx0XHRcdFx0c2NyZWVuUm90UmF0ZURhdGEuYmV0YSAgPSAgIHJvdFJhdGVEYXRhLmdhbW1hO1xuXHRcdFx0XHRzY3JlZW5Sb3RSYXRlRGF0YS5nYW1tYSA9IC0gcm90UmF0ZURhdGEuYmV0YTtcblx0XHRcdFx0YnJlYWs7XG5cdFx0XHRkZWZhdWx0OiAvLyBTQ1JFRU5fUk9UQVRJT05fMFxuXHRcdFx0XHRzY3JlZW5Sb3RSYXRlRGF0YS5iZXRhICA9ICAgcm90UmF0ZURhdGEuYmV0YTtcblx0XHRcdFx0c2NyZWVuUm90UmF0ZURhdGEuZ2FtbWEgPSAgIHJvdFJhdGVEYXRhLmdhbW1hO1xuXHRcdFx0XHRicmVhaztcblx0XHR9XG5cblx0XHRzY3JlZW5Sb3RSYXRlRGF0YS5hbHBoYSA9IHJvdFJhdGVEYXRhLmFscGhhO1xuXG5cdFx0cmV0dXJuIHNjcmVlblJvdFJhdGVEYXRhO1xuXG5cdH0sXG5cblx0Z2V0TGFzdFJhd0V2ZW50RGF0YTogZnVuY3Rpb24gKCkge1xuXG5cdFx0cmV0dXJuIHNlbnNvcnMubW90aW9uLmRhdGEgfHwge307XG5cblx0fVxuXG59O1xuXG5cbi8vLy8vLyBBdHRhY2ggRlVMTFRJTFQgdG8gcm9vdCBET00gZWxlbWVudCAvLy8vLy9cblxud2luZG93LkZVTExUSUxUID0gRlVMTFRJTFQ7XG5cbn0pKCB3aW5kb3cgKTsiLCJ2YXIgcm9vdCA9IHJlcXVpcmUoJy4vX3Jvb3QnKTtcblxuLyoqIEJ1aWx0LWluIHZhbHVlIHJlZmVyZW5jZXMuICovXG52YXIgU3ltYm9sID0gcm9vdC5TeW1ib2w7XG5cbm1vZHVsZS5leHBvcnRzID0gU3ltYm9sO1xuIiwidmFyIGJhc2VUaW1lcyA9IHJlcXVpcmUoJy4vX2Jhc2VUaW1lcycpLFxuICAgIGlzQXJndW1lbnRzID0gcmVxdWlyZSgnLi9pc0FyZ3VtZW50cycpLFxuICAgIGlzQXJyYXkgPSByZXF1aXJlKCcuL2lzQXJyYXknKSxcbiAgICBpc0J1ZmZlciA9IHJlcXVpcmUoJy4vaXNCdWZmZXInKSxcbiAgICBpc0luZGV4ID0gcmVxdWlyZSgnLi9faXNJbmRleCcpLFxuICAgIGlzVHlwZWRBcnJheSA9IHJlcXVpcmUoJy4vaXNUeXBlZEFycmF5Jyk7XG5cbi8qKiBVc2VkIGZvciBidWlsdC1pbiBtZXRob2QgcmVmZXJlbmNlcy4gKi9cbnZhciBvYmplY3RQcm90byA9IE9iamVjdC5wcm90b3R5cGU7XG5cbi8qKiBVc2VkIHRvIGNoZWNrIG9iamVjdHMgZm9yIG93biBwcm9wZXJ0aWVzLiAqL1xudmFyIGhhc093blByb3BlcnR5ID0gb2JqZWN0UHJvdG8uaGFzT3duUHJvcGVydHk7XG5cbi8qKlxuICogQ3JlYXRlcyBhbiBhcnJheSBvZiB0aGUgZW51bWVyYWJsZSBwcm9wZXJ0eSBuYW1lcyBvZiB0aGUgYXJyYXktbGlrZSBgdmFsdWVgLlxuICpcbiAqIEBwcml2YXRlXG4gKiBAcGFyYW0geyp9IHZhbHVlIFRoZSB2YWx1ZSB0byBxdWVyeS5cbiAqIEBwYXJhbSB7Ym9vbGVhbn0gaW5oZXJpdGVkIFNwZWNpZnkgcmV0dXJuaW5nIGluaGVyaXRlZCBwcm9wZXJ0eSBuYW1lcy5cbiAqIEByZXR1cm5zIHtBcnJheX0gUmV0dXJucyB0aGUgYXJyYXkgb2YgcHJvcGVydHkgbmFtZXMuXG4gKi9cbmZ1bmN0aW9uIGFycmF5TGlrZUtleXModmFsdWUsIGluaGVyaXRlZCkge1xuICB2YXIgaXNBcnIgPSBpc0FycmF5KHZhbHVlKSxcbiAgICAgIGlzQXJnID0gIWlzQXJyICYmIGlzQXJndW1lbnRzKHZhbHVlKSxcbiAgICAgIGlzQnVmZiA9ICFpc0FyciAmJiAhaXNBcmcgJiYgaXNCdWZmZXIodmFsdWUpLFxuICAgICAgaXNUeXBlID0gIWlzQXJyICYmICFpc0FyZyAmJiAhaXNCdWZmICYmIGlzVHlwZWRBcnJheSh2YWx1ZSksXG4gICAgICBza2lwSW5kZXhlcyA9IGlzQXJyIHx8IGlzQXJnIHx8IGlzQnVmZiB8fCBpc1R5cGUsXG4gICAgICByZXN1bHQgPSBza2lwSW5kZXhlcyA/IGJhc2VUaW1lcyh2YWx1ZS5sZW5ndGgsIFN0cmluZykgOiBbXSxcbiAgICAgIGxlbmd0aCA9IHJlc3VsdC5sZW5ndGg7XG5cbiAgZm9yICh2YXIga2V5IGluIHZhbHVlKSB7XG4gICAgaWYgKChpbmhlcml0ZWQgfHwgaGFzT3duUHJvcGVydHkuY2FsbCh2YWx1ZSwga2V5KSkgJiZcbiAgICAgICAgIShza2lwSW5kZXhlcyAmJiAoXG4gICAgICAgICAgIC8vIFNhZmFyaSA5IGhhcyBlbnVtZXJhYmxlIGBhcmd1bWVudHMubGVuZ3RoYCBpbiBzdHJpY3QgbW9kZS5cbiAgICAgICAgICAga2V5ID09ICdsZW5ndGgnIHx8XG4gICAgICAgICAgIC8vIE5vZGUuanMgMC4xMCBoYXMgZW51bWVyYWJsZSBub24taW5kZXggcHJvcGVydGllcyBvbiBidWZmZXJzLlxuICAgICAgICAgICAoaXNCdWZmICYmIChrZXkgPT0gJ29mZnNldCcgfHwga2V5ID09ICdwYXJlbnQnKSkgfHxcbiAgICAgICAgICAgLy8gUGhhbnRvbUpTIDIgaGFzIGVudW1lcmFibGUgbm9uLWluZGV4IHByb3BlcnRpZXMgb24gdHlwZWQgYXJyYXlzLlxuICAgICAgICAgICAoaXNUeXBlICYmIChrZXkgPT0gJ2J1ZmZlcicgfHwga2V5ID09ICdieXRlTGVuZ3RoJyB8fCBrZXkgPT0gJ2J5dGVPZmZzZXQnKSkgfHxcbiAgICAgICAgICAgLy8gU2tpcCBpbmRleCBwcm9wZXJ0aWVzLlxuICAgICAgICAgICBpc0luZGV4KGtleSwgbGVuZ3RoKVxuICAgICAgICApKSkge1xuICAgICAgcmVzdWx0LnB1c2goa2V5KTtcbiAgICB9XG4gIH1cbiAgcmV0dXJuIHJlc3VsdDtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBhcnJheUxpa2VLZXlzO1xuIiwidmFyIFN5bWJvbCA9IHJlcXVpcmUoJy4vX1N5bWJvbCcpLFxuICAgIGdldFJhd1RhZyA9IHJlcXVpcmUoJy4vX2dldFJhd1RhZycpLFxuICAgIG9iamVjdFRvU3RyaW5nID0gcmVxdWlyZSgnLi9fb2JqZWN0VG9TdHJpbmcnKTtcblxuLyoqIGBPYmplY3QjdG9TdHJpbmdgIHJlc3VsdCByZWZlcmVuY2VzLiAqL1xudmFyIG51bGxUYWcgPSAnW29iamVjdCBOdWxsXScsXG4gICAgdW5kZWZpbmVkVGFnID0gJ1tvYmplY3QgVW5kZWZpbmVkXSc7XG5cbi8qKiBCdWlsdC1pbiB2YWx1ZSByZWZlcmVuY2VzLiAqL1xudmFyIHN5bVRvU3RyaW5nVGFnID0gU3ltYm9sID8gU3ltYm9sLnRvU3RyaW5nVGFnIDogdW5kZWZpbmVkO1xuXG4vKipcbiAqIFRoZSBiYXNlIGltcGxlbWVudGF0aW9uIG9mIGBnZXRUYWdgIHdpdGhvdXQgZmFsbGJhY2tzIGZvciBidWdneSBlbnZpcm9ubWVudHMuXG4gKlxuICogQHByaXZhdGVcbiAqIEBwYXJhbSB7Kn0gdmFsdWUgVGhlIHZhbHVlIHRvIHF1ZXJ5LlxuICogQHJldHVybnMge3N0cmluZ30gUmV0dXJucyB0aGUgYHRvU3RyaW5nVGFnYC5cbiAqL1xuZnVuY3Rpb24gYmFzZUdldFRhZyh2YWx1ZSkge1xuICBpZiAodmFsdWUgPT0gbnVsbCkge1xuICAgIHJldHVybiB2YWx1ZSA9PT0gdW5kZWZpbmVkID8gdW5kZWZpbmVkVGFnIDogbnVsbFRhZztcbiAgfVxuICByZXR1cm4gKHN5bVRvU3RyaW5nVGFnICYmIHN5bVRvU3RyaW5nVGFnIGluIE9iamVjdCh2YWx1ZSkpXG4gICAgPyBnZXRSYXdUYWcodmFsdWUpXG4gICAgOiBvYmplY3RUb1N0cmluZyh2YWx1ZSk7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gYmFzZUdldFRhZztcbiIsInZhciBiYXNlR2V0VGFnID0gcmVxdWlyZSgnLi9fYmFzZUdldFRhZycpLFxuICAgIGlzT2JqZWN0TGlrZSA9IHJlcXVpcmUoJy4vaXNPYmplY3RMaWtlJyk7XG5cbi8qKiBgT2JqZWN0I3RvU3RyaW5nYCByZXN1bHQgcmVmZXJlbmNlcy4gKi9cbnZhciBhcmdzVGFnID0gJ1tvYmplY3QgQXJndW1lbnRzXSc7XG5cbi8qKlxuICogVGhlIGJhc2UgaW1wbGVtZW50YXRpb24gb2YgYF8uaXNBcmd1bWVudHNgLlxuICpcbiAqIEBwcml2YXRlXG4gKiBAcGFyYW0geyp9IHZhbHVlIFRoZSB2YWx1ZSB0byBjaGVjay5cbiAqIEByZXR1cm5zIHtib29sZWFufSBSZXR1cm5zIGB0cnVlYCBpZiBgdmFsdWVgIGlzIGFuIGBhcmd1bWVudHNgIG9iamVjdCxcbiAqL1xuZnVuY3Rpb24gYmFzZUlzQXJndW1lbnRzKHZhbHVlKSB7XG4gIHJldHVybiBpc09iamVjdExpa2UodmFsdWUpICYmIGJhc2VHZXRUYWcodmFsdWUpID09IGFyZ3NUYWc7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gYmFzZUlzQXJndW1lbnRzO1xuIiwidmFyIGJhc2VHZXRUYWcgPSByZXF1aXJlKCcuL19iYXNlR2V0VGFnJyksXG4gICAgaXNMZW5ndGggPSByZXF1aXJlKCcuL2lzTGVuZ3RoJyksXG4gICAgaXNPYmplY3RMaWtlID0gcmVxdWlyZSgnLi9pc09iamVjdExpa2UnKTtcblxuLyoqIGBPYmplY3QjdG9TdHJpbmdgIHJlc3VsdCByZWZlcmVuY2VzLiAqL1xudmFyIGFyZ3NUYWcgPSAnW29iamVjdCBBcmd1bWVudHNdJyxcbiAgICBhcnJheVRhZyA9ICdbb2JqZWN0IEFycmF5XScsXG4gICAgYm9vbFRhZyA9ICdbb2JqZWN0IEJvb2xlYW5dJyxcbiAgICBkYXRlVGFnID0gJ1tvYmplY3QgRGF0ZV0nLFxuICAgIGVycm9yVGFnID0gJ1tvYmplY3QgRXJyb3JdJyxcbiAgICBmdW5jVGFnID0gJ1tvYmplY3QgRnVuY3Rpb25dJyxcbiAgICBtYXBUYWcgPSAnW29iamVjdCBNYXBdJyxcbiAgICBudW1iZXJUYWcgPSAnW29iamVjdCBOdW1iZXJdJyxcbiAgICBvYmplY3RUYWcgPSAnW29iamVjdCBPYmplY3RdJyxcbiAgICByZWdleHBUYWcgPSAnW29iamVjdCBSZWdFeHBdJyxcbiAgICBzZXRUYWcgPSAnW29iamVjdCBTZXRdJyxcbiAgICBzdHJpbmdUYWcgPSAnW29iamVjdCBTdHJpbmddJyxcbiAgICB3ZWFrTWFwVGFnID0gJ1tvYmplY3QgV2Vha01hcF0nO1xuXG52YXIgYXJyYXlCdWZmZXJUYWcgPSAnW29iamVjdCBBcnJheUJ1ZmZlcl0nLFxuICAgIGRhdGFWaWV3VGFnID0gJ1tvYmplY3QgRGF0YVZpZXddJyxcbiAgICBmbG9hdDMyVGFnID0gJ1tvYmplY3QgRmxvYXQzMkFycmF5XScsXG4gICAgZmxvYXQ2NFRhZyA9ICdbb2JqZWN0IEZsb2F0NjRBcnJheV0nLFxuICAgIGludDhUYWcgPSAnW29iamVjdCBJbnQ4QXJyYXldJyxcbiAgICBpbnQxNlRhZyA9ICdbb2JqZWN0IEludDE2QXJyYXldJyxcbiAgICBpbnQzMlRhZyA9ICdbb2JqZWN0IEludDMyQXJyYXldJyxcbiAgICB1aW50OFRhZyA9ICdbb2JqZWN0IFVpbnQ4QXJyYXldJyxcbiAgICB1aW50OENsYW1wZWRUYWcgPSAnW29iamVjdCBVaW50OENsYW1wZWRBcnJheV0nLFxuICAgIHVpbnQxNlRhZyA9ICdbb2JqZWN0IFVpbnQxNkFycmF5XScsXG4gICAgdWludDMyVGFnID0gJ1tvYmplY3QgVWludDMyQXJyYXldJztcblxuLyoqIFVzZWQgdG8gaWRlbnRpZnkgYHRvU3RyaW5nVGFnYCB2YWx1ZXMgb2YgdHlwZWQgYXJyYXlzLiAqL1xudmFyIHR5cGVkQXJyYXlUYWdzID0ge307XG50eXBlZEFycmF5VGFnc1tmbG9hdDMyVGFnXSA9IHR5cGVkQXJyYXlUYWdzW2Zsb2F0NjRUYWddID1cbnR5cGVkQXJyYXlUYWdzW2ludDhUYWddID0gdHlwZWRBcnJheVRhZ3NbaW50MTZUYWddID1cbnR5cGVkQXJyYXlUYWdzW2ludDMyVGFnXSA9IHR5cGVkQXJyYXlUYWdzW3VpbnQ4VGFnXSA9XG50eXBlZEFycmF5VGFnc1t1aW50OENsYW1wZWRUYWddID0gdHlwZWRBcnJheVRhZ3NbdWludDE2VGFnXSA9XG50eXBlZEFycmF5VGFnc1t1aW50MzJUYWddID0gdHJ1ZTtcbnR5cGVkQXJyYXlUYWdzW2FyZ3NUYWddID0gdHlwZWRBcnJheVRhZ3NbYXJyYXlUYWddID1cbnR5cGVkQXJyYXlUYWdzW2FycmF5QnVmZmVyVGFnXSA9IHR5cGVkQXJyYXlUYWdzW2Jvb2xUYWddID1cbnR5cGVkQXJyYXlUYWdzW2RhdGFWaWV3VGFnXSA9IHR5cGVkQXJyYXlUYWdzW2RhdGVUYWddID1cbnR5cGVkQXJyYXlUYWdzW2Vycm9yVGFnXSA9IHR5cGVkQXJyYXlUYWdzW2Z1bmNUYWddID1cbnR5cGVkQXJyYXlUYWdzW21hcFRhZ10gPSB0eXBlZEFycmF5VGFnc1tudW1iZXJUYWddID1cbnR5cGVkQXJyYXlUYWdzW29iamVjdFRhZ10gPSB0eXBlZEFycmF5VGFnc1tyZWdleHBUYWddID1cbnR5cGVkQXJyYXlUYWdzW3NldFRhZ10gPSB0eXBlZEFycmF5VGFnc1tzdHJpbmdUYWddID1cbnR5cGVkQXJyYXlUYWdzW3dlYWtNYXBUYWddID0gZmFsc2U7XG5cbi8qKlxuICogVGhlIGJhc2UgaW1wbGVtZW50YXRpb24gb2YgYF8uaXNUeXBlZEFycmF5YCB3aXRob3V0IE5vZGUuanMgb3B0aW1pemF0aW9ucy5cbiAqXG4gKiBAcHJpdmF0ZVxuICogQHBhcmFtIHsqfSB2YWx1ZSBUaGUgdmFsdWUgdG8gY2hlY2suXG4gKiBAcmV0dXJucyB7Ym9vbGVhbn0gUmV0dXJucyBgdHJ1ZWAgaWYgYHZhbHVlYCBpcyBhIHR5cGVkIGFycmF5LCBlbHNlIGBmYWxzZWAuXG4gKi9cbmZ1bmN0aW9uIGJhc2VJc1R5cGVkQXJyYXkodmFsdWUpIHtcbiAgcmV0dXJuIGlzT2JqZWN0TGlrZSh2YWx1ZSkgJiZcbiAgICBpc0xlbmd0aCh2YWx1ZS5sZW5ndGgpICYmICEhdHlwZWRBcnJheVRhZ3NbYmFzZUdldFRhZyh2YWx1ZSldO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IGJhc2VJc1R5cGVkQXJyYXk7XG4iLCJ2YXIgaXNQcm90b3R5cGUgPSByZXF1aXJlKCcuL19pc1Byb3RvdHlwZScpLFxuICAgIG5hdGl2ZUtleXMgPSByZXF1aXJlKCcuL19uYXRpdmVLZXlzJyk7XG5cbi8qKiBVc2VkIGZvciBidWlsdC1pbiBtZXRob2QgcmVmZXJlbmNlcy4gKi9cbnZhciBvYmplY3RQcm90byA9IE9iamVjdC5wcm90b3R5cGU7XG5cbi8qKiBVc2VkIHRvIGNoZWNrIG9iamVjdHMgZm9yIG93biBwcm9wZXJ0aWVzLiAqL1xudmFyIGhhc093blByb3BlcnR5ID0gb2JqZWN0UHJvdG8uaGFzT3duUHJvcGVydHk7XG5cbi8qKlxuICogVGhlIGJhc2UgaW1wbGVtZW50YXRpb24gb2YgYF8ua2V5c2Agd2hpY2ggZG9lc24ndCB0cmVhdCBzcGFyc2UgYXJyYXlzIGFzIGRlbnNlLlxuICpcbiAqIEBwcml2YXRlXG4gKiBAcGFyYW0ge09iamVjdH0gb2JqZWN0IFRoZSBvYmplY3QgdG8gcXVlcnkuXG4gKiBAcmV0dXJucyB7QXJyYXl9IFJldHVybnMgdGhlIGFycmF5IG9mIHByb3BlcnR5IG5hbWVzLlxuICovXG5mdW5jdGlvbiBiYXNlS2V5cyhvYmplY3QpIHtcbiAgaWYgKCFpc1Byb3RvdHlwZShvYmplY3QpKSB7XG4gICAgcmV0dXJuIG5hdGl2ZUtleXMob2JqZWN0KTtcbiAgfVxuICB2YXIgcmVzdWx0ID0gW107XG4gIGZvciAodmFyIGtleSBpbiBPYmplY3Qob2JqZWN0KSkge1xuICAgIGlmIChoYXNPd25Qcm9wZXJ0eS5jYWxsKG9iamVjdCwga2V5KSAmJiBrZXkgIT0gJ2NvbnN0cnVjdG9yJykge1xuICAgICAgcmVzdWx0LnB1c2goa2V5KTtcbiAgICB9XG4gIH1cbiAgcmV0dXJuIHJlc3VsdDtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBiYXNlS2V5cztcbiIsIi8qKlxuICogVGhlIGJhc2UgaW1wbGVtZW50YXRpb24gb2YgYF8udGltZXNgIHdpdGhvdXQgc3VwcG9ydCBmb3IgaXRlcmF0ZWUgc2hvcnRoYW5kc1xuICogb3IgbWF4IGFycmF5IGxlbmd0aCBjaGVja3MuXG4gKlxuICogQHByaXZhdGVcbiAqIEBwYXJhbSB7bnVtYmVyfSBuIFRoZSBudW1iZXIgb2YgdGltZXMgdG8gaW52b2tlIGBpdGVyYXRlZWAuXG4gKiBAcGFyYW0ge0Z1bmN0aW9ufSBpdGVyYXRlZSBUaGUgZnVuY3Rpb24gaW52b2tlZCBwZXIgaXRlcmF0aW9uLlxuICogQHJldHVybnMge0FycmF5fSBSZXR1cm5zIHRoZSBhcnJheSBvZiByZXN1bHRzLlxuICovXG5mdW5jdGlvbiBiYXNlVGltZXMobiwgaXRlcmF0ZWUpIHtcbiAgdmFyIGluZGV4ID0gLTEsXG4gICAgICByZXN1bHQgPSBBcnJheShuKTtcblxuICB3aGlsZSAoKytpbmRleCA8IG4pIHtcbiAgICByZXN1bHRbaW5kZXhdID0gaXRlcmF0ZWUoaW5kZXgpO1xuICB9XG4gIHJldHVybiByZXN1bHQ7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gYmFzZVRpbWVzO1xuIiwiLyoqXG4gKiBUaGUgYmFzZSBpbXBsZW1lbnRhdGlvbiBvZiBgXy51bmFyeWAgd2l0aG91dCBzdXBwb3J0IGZvciBzdG9yaW5nIG1ldGFkYXRhLlxuICpcbiAqIEBwcml2YXRlXG4gKiBAcGFyYW0ge0Z1bmN0aW9ufSBmdW5jIFRoZSBmdW5jdGlvbiB0byBjYXAgYXJndW1lbnRzIGZvci5cbiAqIEByZXR1cm5zIHtGdW5jdGlvbn0gUmV0dXJucyB0aGUgbmV3IGNhcHBlZCBmdW5jdGlvbi5cbiAqL1xuZnVuY3Rpb24gYmFzZVVuYXJ5KGZ1bmMpIHtcbiAgcmV0dXJuIGZ1bmN0aW9uKHZhbHVlKSB7XG4gICAgcmV0dXJuIGZ1bmModmFsdWUpO1xuICB9O1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IGJhc2VVbmFyeTtcbiIsIi8qKiBEZXRlY3QgZnJlZSB2YXJpYWJsZSBgZ2xvYmFsYCBmcm9tIE5vZGUuanMuICovXG52YXIgZnJlZUdsb2JhbCA9IHR5cGVvZiBnbG9iYWwgPT0gJ29iamVjdCcgJiYgZ2xvYmFsICYmIGdsb2JhbC5PYmplY3QgPT09IE9iamVjdCAmJiBnbG9iYWw7XG5cbm1vZHVsZS5leHBvcnRzID0gZnJlZUdsb2JhbDtcbiIsInZhciBTeW1ib2wgPSByZXF1aXJlKCcuL19TeW1ib2wnKTtcblxuLyoqIFVzZWQgZm9yIGJ1aWx0LWluIG1ldGhvZCByZWZlcmVuY2VzLiAqL1xudmFyIG9iamVjdFByb3RvID0gT2JqZWN0LnByb3RvdHlwZTtcblxuLyoqIFVzZWQgdG8gY2hlY2sgb2JqZWN0cyBmb3Igb3duIHByb3BlcnRpZXMuICovXG52YXIgaGFzT3duUHJvcGVydHkgPSBvYmplY3RQcm90by5oYXNPd25Qcm9wZXJ0eTtcblxuLyoqXG4gKiBVc2VkIHRvIHJlc29sdmUgdGhlXG4gKiBbYHRvU3RyaW5nVGFnYF0oaHR0cDovL2VjbWEtaW50ZXJuYXRpb25hbC5vcmcvZWNtYS0yNjIvNy4wLyNzZWMtb2JqZWN0LnByb3RvdHlwZS50b3N0cmluZylcbiAqIG9mIHZhbHVlcy5cbiAqL1xudmFyIG5hdGl2ZU9iamVjdFRvU3RyaW5nID0gb2JqZWN0UHJvdG8udG9TdHJpbmc7XG5cbi8qKiBCdWlsdC1pbiB2YWx1ZSByZWZlcmVuY2VzLiAqL1xudmFyIHN5bVRvU3RyaW5nVGFnID0gU3ltYm9sID8gU3ltYm9sLnRvU3RyaW5nVGFnIDogdW5kZWZpbmVkO1xuXG4vKipcbiAqIEEgc3BlY2lhbGl6ZWQgdmVyc2lvbiBvZiBgYmFzZUdldFRhZ2Agd2hpY2ggaWdub3JlcyBgU3ltYm9sLnRvU3RyaW5nVGFnYCB2YWx1ZXMuXG4gKlxuICogQHByaXZhdGVcbiAqIEBwYXJhbSB7Kn0gdmFsdWUgVGhlIHZhbHVlIHRvIHF1ZXJ5LlxuICogQHJldHVybnMge3N0cmluZ30gUmV0dXJucyB0aGUgcmF3IGB0b1N0cmluZ1RhZ2AuXG4gKi9cbmZ1bmN0aW9uIGdldFJhd1RhZyh2YWx1ZSkge1xuICB2YXIgaXNPd24gPSBoYXNPd25Qcm9wZXJ0eS5jYWxsKHZhbHVlLCBzeW1Ub1N0cmluZ1RhZyksXG4gICAgICB0YWcgPSB2YWx1ZVtzeW1Ub1N0cmluZ1RhZ107XG5cbiAgdHJ5IHtcbiAgICB2YWx1ZVtzeW1Ub1N0cmluZ1RhZ10gPSB1bmRlZmluZWQ7XG4gICAgdmFyIHVubWFza2VkID0gdHJ1ZTtcbiAgfSBjYXRjaCAoZSkge31cblxuICB2YXIgcmVzdWx0ID0gbmF0aXZlT2JqZWN0VG9TdHJpbmcuY2FsbCh2YWx1ZSk7XG4gIGlmICh1bm1hc2tlZCkge1xuICAgIGlmIChpc093bikge1xuICAgICAgdmFsdWVbc3ltVG9TdHJpbmdUYWddID0gdGFnO1xuICAgIH0gZWxzZSB7XG4gICAgICBkZWxldGUgdmFsdWVbc3ltVG9TdHJpbmdUYWddO1xuICAgIH1cbiAgfVxuICByZXR1cm4gcmVzdWx0O1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IGdldFJhd1RhZztcbiIsIi8qKiBVc2VkIGFzIHJlZmVyZW5jZXMgZm9yIHZhcmlvdXMgYE51bWJlcmAgY29uc3RhbnRzLiAqL1xudmFyIE1BWF9TQUZFX0lOVEVHRVIgPSA5MDA3MTk5MjU0NzQwOTkxO1xuXG4vKiogVXNlZCB0byBkZXRlY3QgdW5zaWduZWQgaW50ZWdlciB2YWx1ZXMuICovXG52YXIgcmVJc1VpbnQgPSAvXig/OjB8WzEtOV1cXGQqKSQvO1xuXG4vKipcbiAqIENoZWNrcyBpZiBgdmFsdWVgIGlzIGEgdmFsaWQgYXJyYXktbGlrZSBpbmRleC5cbiAqXG4gKiBAcHJpdmF0ZVxuICogQHBhcmFtIHsqfSB2YWx1ZSBUaGUgdmFsdWUgdG8gY2hlY2suXG4gKiBAcGFyYW0ge251bWJlcn0gW2xlbmd0aD1NQVhfU0FGRV9JTlRFR0VSXSBUaGUgdXBwZXIgYm91bmRzIG9mIGEgdmFsaWQgaW5kZXguXG4gKiBAcmV0dXJucyB7Ym9vbGVhbn0gUmV0dXJucyBgdHJ1ZWAgaWYgYHZhbHVlYCBpcyBhIHZhbGlkIGluZGV4LCBlbHNlIGBmYWxzZWAuXG4gKi9cbmZ1bmN0aW9uIGlzSW5kZXgodmFsdWUsIGxlbmd0aCkge1xuICB2YXIgdHlwZSA9IHR5cGVvZiB2YWx1ZTtcbiAgbGVuZ3RoID0gbGVuZ3RoID09IG51bGwgPyBNQVhfU0FGRV9JTlRFR0VSIDogbGVuZ3RoO1xuXG4gIHJldHVybiAhIWxlbmd0aCAmJlxuICAgICh0eXBlID09ICdudW1iZXInIHx8XG4gICAgICAodHlwZSAhPSAnc3ltYm9sJyAmJiByZUlzVWludC50ZXN0KHZhbHVlKSkpICYmXG4gICAgICAgICh2YWx1ZSA+IC0xICYmIHZhbHVlICUgMSA9PSAwICYmIHZhbHVlIDwgbGVuZ3RoKTtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBpc0luZGV4O1xuIiwiLyoqIFVzZWQgZm9yIGJ1aWx0LWluIG1ldGhvZCByZWZlcmVuY2VzLiAqL1xudmFyIG9iamVjdFByb3RvID0gT2JqZWN0LnByb3RvdHlwZTtcblxuLyoqXG4gKiBDaGVja3MgaWYgYHZhbHVlYCBpcyBsaWtlbHkgYSBwcm90b3R5cGUgb2JqZWN0LlxuICpcbiAqIEBwcml2YXRlXG4gKiBAcGFyYW0geyp9IHZhbHVlIFRoZSB2YWx1ZSB0byBjaGVjay5cbiAqIEByZXR1cm5zIHtib29sZWFufSBSZXR1cm5zIGB0cnVlYCBpZiBgdmFsdWVgIGlzIGEgcHJvdG90eXBlLCBlbHNlIGBmYWxzZWAuXG4gKi9cbmZ1bmN0aW9uIGlzUHJvdG90eXBlKHZhbHVlKSB7XG4gIHZhciBDdG9yID0gdmFsdWUgJiYgdmFsdWUuY29uc3RydWN0b3IsXG4gICAgICBwcm90byA9ICh0eXBlb2YgQ3RvciA9PSAnZnVuY3Rpb24nICYmIEN0b3IucHJvdG90eXBlKSB8fCBvYmplY3RQcm90bztcblxuICByZXR1cm4gdmFsdWUgPT09IHByb3RvO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IGlzUHJvdG90eXBlO1xuIiwidmFyIG92ZXJBcmcgPSByZXF1aXJlKCcuL19vdmVyQXJnJyk7XG5cbi8qIEJ1aWx0LWluIG1ldGhvZCByZWZlcmVuY2VzIGZvciB0aG9zZSB3aXRoIHRoZSBzYW1lIG5hbWUgYXMgb3RoZXIgYGxvZGFzaGAgbWV0aG9kcy4gKi9cbnZhciBuYXRpdmVLZXlzID0gb3ZlckFyZyhPYmplY3Qua2V5cywgT2JqZWN0KTtcblxubW9kdWxlLmV4cG9ydHMgPSBuYXRpdmVLZXlzO1xuIiwidmFyIGZyZWVHbG9iYWwgPSByZXF1aXJlKCcuL19mcmVlR2xvYmFsJyk7XG5cbi8qKiBEZXRlY3QgZnJlZSB2YXJpYWJsZSBgZXhwb3J0c2AuICovXG52YXIgZnJlZUV4cG9ydHMgPSB0eXBlb2YgZXhwb3J0cyA9PSAnb2JqZWN0JyAmJiBleHBvcnRzICYmICFleHBvcnRzLm5vZGVUeXBlICYmIGV4cG9ydHM7XG5cbi8qKiBEZXRlY3QgZnJlZSB2YXJpYWJsZSBgbW9kdWxlYC4gKi9cbnZhciBmcmVlTW9kdWxlID0gZnJlZUV4cG9ydHMgJiYgdHlwZW9mIG1vZHVsZSA9PSAnb2JqZWN0JyAmJiBtb2R1bGUgJiYgIW1vZHVsZS5ub2RlVHlwZSAmJiBtb2R1bGU7XG5cbi8qKiBEZXRlY3QgdGhlIHBvcHVsYXIgQ29tbW9uSlMgZXh0ZW5zaW9uIGBtb2R1bGUuZXhwb3J0c2AuICovXG52YXIgbW9kdWxlRXhwb3J0cyA9IGZyZWVNb2R1bGUgJiYgZnJlZU1vZHVsZS5leHBvcnRzID09PSBmcmVlRXhwb3J0cztcblxuLyoqIERldGVjdCBmcmVlIHZhcmlhYmxlIGBwcm9jZXNzYCBmcm9tIE5vZGUuanMuICovXG52YXIgZnJlZVByb2Nlc3MgPSBtb2R1bGVFeHBvcnRzICYmIGZyZWVHbG9iYWwucHJvY2VzcztcblxuLyoqIFVzZWQgdG8gYWNjZXNzIGZhc3RlciBOb2RlLmpzIGhlbHBlcnMuICovXG52YXIgbm9kZVV0aWwgPSAoZnVuY3Rpb24oKSB7XG4gIHRyeSB7XG4gICAgLy8gVXNlIGB1dGlsLnR5cGVzYCBmb3IgTm9kZS5qcyAxMCsuXG4gICAgdmFyIHR5cGVzID0gZnJlZU1vZHVsZSAmJiBmcmVlTW9kdWxlLnJlcXVpcmUgJiYgZnJlZU1vZHVsZS5yZXF1aXJlKCd1dGlsJykudHlwZXM7XG5cbiAgICBpZiAodHlwZXMpIHtcbiAgICAgIHJldHVybiB0eXBlcztcbiAgICB9XG5cbiAgICAvLyBMZWdhY3kgYHByb2Nlc3MuYmluZGluZygndXRpbCcpYCBmb3IgTm9kZS5qcyA8IDEwLlxuICAgIHJldHVybiBmcmVlUHJvY2VzcyAmJiBmcmVlUHJvY2Vzcy5iaW5kaW5nICYmIGZyZWVQcm9jZXNzLmJpbmRpbmcoJ3V0aWwnKTtcbiAgfSBjYXRjaCAoZSkge31cbn0oKSk7XG5cbm1vZHVsZS5leHBvcnRzID0gbm9kZVV0aWw7XG4iLCIvKiogVXNlZCBmb3IgYnVpbHQtaW4gbWV0aG9kIHJlZmVyZW5jZXMuICovXG52YXIgb2JqZWN0UHJvdG8gPSBPYmplY3QucHJvdG90eXBlO1xuXG4vKipcbiAqIFVzZWQgdG8gcmVzb2x2ZSB0aGVcbiAqIFtgdG9TdHJpbmdUYWdgXShodHRwOi8vZWNtYS1pbnRlcm5hdGlvbmFsLm9yZy9lY21hLTI2Mi83LjAvI3NlYy1vYmplY3QucHJvdG90eXBlLnRvc3RyaW5nKVxuICogb2YgdmFsdWVzLlxuICovXG52YXIgbmF0aXZlT2JqZWN0VG9TdHJpbmcgPSBvYmplY3RQcm90by50b1N0cmluZztcblxuLyoqXG4gKiBDb252ZXJ0cyBgdmFsdWVgIHRvIGEgc3RyaW5nIHVzaW5nIGBPYmplY3QucHJvdG90eXBlLnRvU3RyaW5nYC5cbiAqXG4gKiBAcHJpdmF0ZVxuICogQHBhcmFtIHsqfSB2YWx1ZSBUaGUgdmFsdWUgdG8gY29udmVydC5cbiAqIEByZXR1cm5zIHtzdHJpbmd9IFJldHVybnMgdGhlIGNvbnZlcnRlZCBzdHJpbmcuXG4gKi9cbmZ1bmN0aW9uIG9iamVjdFRvU3RyaW5nKHZhbHVlKSB7XG4gIHJldHVybiBuYXRpdmVPYmplY3RUb1N0cmluZy5jYWxsKHZhbHVlKTtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBvYmplY3RUb1N0cmluZztcbiIsIi8qKlxuICogQ3JlYXRlcyBhIHVuYXJ5IGZ1bmN0aW9uIHRoYXQgaW52b2tlcyBgZnVuY2Agd2l0aCBpdHMgYXJndW1lbnQgdHJhbnNmb3JtZWQuXG4gKlxuICogQHByaXZhdGVcbiAqIEBwYXJhbSB7RnVuY3Rpb259IGZ1bmMgVGhlIGZ1bmN0aW9uIHRvIHdyYXAuXG4gKiBAcGFyYW0ge0Z1bmN0aW9ufSB0cmFuc2Zvcm0gVGhlIGFyZ3VtZW50IHRyYW5zZm9ybS5cbiAqIEByZXR1cm5zIHtGdW5jdGlvbn0gUmV0dXJucyB0aGUgbmV3IGZ1bmN0aW9uLlxuICovXG5mdW5jdGlvbiBvdmVyQXJnKGZ1bmMsIHRyYW5zZm9ybSkge1xuICByZXR1cm4gZnVuY3Rpb24oYXJnKSB7XG4gICAgcmV0dXJuIGZ1bmModHJhbnNmb3JtKGFyZykpO1xuICB9O1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IG92ZXJBcmc7XG4iLCJ2YXIgZnJlZUdsb2JhbCA9IHJlcXVpcmUoJy4vX2ZyZWVHbG9iYWwnKTtcblxuLyoqIERldGVjdCBmcmVlIHZhcmlhYmxlIGBzZWxmYC4gKi9cbnZhciBmcmVlU2VsZiA9IHR5cGVvZiBzZWxmID09ICdvYmplY3QnICYmIHNlbGYgJiYgc2VsZi5PYmplY3QgPT09IE9iamVjdCAmJiBzZWxmO1xuXG4vKiogVXNlZCBhcyBhIHJlZmVyZW5jZSB0byB0aGUgZ2xvYmFsIG9iamVjdC4gKi9cbnZhciByb290ID0gZnJlZUdsb2JhbCB8fCBmcmVlU2VsZiB8fCBGdW5jdGlvbigncmV0dXJuIHRoaXMnKSgpO1xuXG5tb2R1bGUuZXhwb3J0cyA9IHJvb3Q7XG4iLCJ2YXIgYmFzZUlzQXJndW1lbnRzID0gcmVxdWlyZSgnLi9fYmFzZUlzQXJndW1lbnRzJyksXG4gICAgaXNPYmplY3RMaWtlID0gcmVxdWlyZSgnLi9pc09iamVjdExpa2UnKTtcblxuLyoqIFVzZWQgZm9yIGJ1aWx0LWluIG1ldGhvZCByZWZlcmVuY2VzLiAqL1xudmFyIG9iamVjdFByb3RvID0gT2JqZWN0LnByb3RvdHlwZTtcblxuLyoqIFVzZWQgdG8gY2hlY2sgb2JqZWN0cyBmb3Igb3duIHByb3BlcnRpZXMuICovXG52YXIgaGFzT3duUHJvcGVydHkgPSBvYmplY3RQcm90by5oYXNPd25Qcm9wZXJ0eTtcblxuLyoqIEJ1aWx0LWluIHZhbHVlIHJlZmVyZW5jZXMuICovXG52YXIgcHJvcGVydHlJc0VudW1lcmFibGUgPSBvYmplY3RQcm90by5wcm9wZXJ0eUlzRW51bWVyYWJsZTtcblxuLyoqXG4gKiBDaGVja3MgaWYgYHZhbHVlYCBpcyBsaWtlbHkgYW4gYGFyZ3VtZW50c2Agb2JqZWN0LlxuICpcbiAqIEBzdGF0aWNcbiAqIEBtZW1iZXJPZiBfXG4gKiBAc2luY2UgMC4xLjBcbiAqIEBjYXRlZ29yeSBMYW5nXG4gKiBAcGFyYW0geyp9IHZhbHVlIFRoZSB2YWx1ZSB0byBjaGVjay5cbiAqIEByZXR1cm5zIHtib29sZWFufSBSZXR1cm5zIGB0cnVlYCBpZiBgdmFsdWVgIGlzIGFuIGBhcmd1bWVudHNgIG9iamVjdCxcbiAqICBlbHNlIGBmYWxzZWAuXG4gKiBAZXhhbXBsZVxuICpcbiAqIF8uaXNBcmd1bWVudHMoZnVuY3Rpb24oKSB7IHJldHVybiBhcmd1bWVudHM7IH0oKSk7XG4gKiAvLyA9PiB0cnVlXG4gKlxuICogXy5pc0FyZ3VtZW50cyhbMSwgMiwgM10pO1xuICogLy8gPT4gZmFsc2VcbiAqL1xudmFyIGlzQXJndW1lbnRzID0gYmFzZUlzQXJndW1lbnRzKGZ1bmN0aW9uKCkgeyByZXR1cm4gYXJndW1lbnRzOyB9KCkpID8gYmFzZUlzQXJndW1lbnRzIDogZnVuY3Rpb24odmFsdWUpIHtcbiAgcmV0dXJuIGlzT2JqZWN0TGlrZSh2YWx1ZSkgJiYgaGFzT3duUHJvcGVydHkuY2FsbCh2YWx1ZSwgJ2NhbGxlZScpICYmXG4gICAgIXByb3BlcnR5SXNFbnVtZXJhYmxlLmNhbGwodmFsdWUsICdjYWxsZWUnKTtcbn07XG5cbm1vZHVsZS5leHBvcnRzID0gaXNBcmd1bWVudHM7XG4iLCIvKipcbiAqIENoZWNrcyBpZiBgdmFsdWVgIGlzIGNsYXNzaWZpZWQgYXMgYW4gYEFycmF5YCBvYmplY3QuXG4gKlxuICogQHN0YXRpY1xuICogQG1lbWJlck9mIF9cbiAqIEBzaW5jZSAwLjEuMFxuICogQGNhdGVnb3J5IExhbmdcbiAqIEBwYXJhbSB7Kn0gdmFsdWUgVGhlIHZhbHVlIHRvIGNoZWNrLlxuICogQHJldHVybnMge2Jvb2xlYW59IFJldHVybnMgYHRydWVgIGlmIGB2YWx1ZWAgaXMgYW4gYXJyYXksIGVsc2UgYGZhbHNlYC5cbiAqIEBleGFtcGxlXG4gKlxuICogXy5pc0FycmF5KFsxLCAyLCAzXSk7XG4gKiAvLyA9PiB0cnVlXG4gKlxuICogXy5pc0FycmF5KGRvY3VtZW50LmJvZHkuY2hpbGRyZW4pO1xuICogLy8gPT4gZmFsc2VcbiAqXG4gKiBfLmlzQXJyYXkoJ2FiYycpO1xuICogLy8gPT4gZmFsc2VcbiAqXG4gKiBfLmlzQXJyYXkoXy5ub29wKTtcbiAqIC8vID0+IGZhbHNlXG4gKi9cbnZhciBpc0FycmF5ID0gQXJyYXkuaXNBcnJheTtcblxubW9kdWxlLmV4cG9ydHMgPSBpc0FycmF5O1xuIiwidmFyIGlzRnVuY3Rpb24gPSByZXF1aXJlKCcuL2lzRnVuY3Rpb24nKSxcbiAgICBpc0xlbmd0aCA9IHJlcXVpcmUoJy4vaXNMZW5ndGgnKTtcblxuLyoqXG4gKiBDaGVja3MgaWYgYHZhbHVlYCBpcyBhcnJheS1saWtlLiBBIHZhbHVlIGlzIGNvbnNpZGVyZWQgYXJyYXktbGlrZSBpZiBpdCdzXG4gKiBub3QgYSBmdW5jdGlvbiBhbmQgaGFzIGEgYHZhbHVlLmxlbmd0aGAgdGhhdCdzIGFuIGludGVnZXIgZ3JlYXRlciB0aGFuIG9yXG4gKiBlcXVhbCB0byBgMGAgYW5kIGxlc3MgdGhhbiBvciBlcXVhbCB0byBgTnVtYmVyLk1BWF9TQUZFX0lOVEVHRVJgLlxuICpcbiAqIEBzdGF0aWNcbiAqIEBtZW1iZXJPZiBfXG4gKiBAc2luY2UgNC4wLjBcbiAqIEBjYXRlZ29yeSBMYW5nXG4gKiBAcGFyYW0geyp9IHZhbHVlIFRoZSB2YWx1ZSB0byBjaGVjay5cbiAqIEByZXR1cm5zIHtib29sZWFufSBSZXR1cm5zIGB0cnVlYCBpZiBgdmFsdWVgIGlzIGFycmF5LWxpa2UsIGVsc2UgYGZhbHNlYC5cbiAqIEBleGFtcGxlXG4gKlxuICogXy5pc0FycmF5TGlrZShbMSwgMiwgM10pO1xuICogLy8gPT4gdHJ1ZVxuICpcbiAqIF8uaXNBcnJheUxpa2UoZG9jdW1lbnQuYm9keS5jaGlsZHJlbik7XG4gKiAvLyA9PiB0cnVlXG4gKlxuICogXy5pc0FycmF5TGlrZSgnYWJjJyk7XG4gKiAvLyA9PiB0cnVlXG4gKlxuICogXy5pc0FycmF5TGlrZShfLm5vb3ApO1xuICogLy8gPT4gZmFsc2VcbiAqL1xuZnVuY3Rpb24gaXNBcnJheUxpa2UodmFsdWUpIHtcbiAgcmV0dXJuIHZhbHVlICE9IG51bGwgJiYgaXNMZW5ndGgodmFsdWUubGVuZ3RoKSAmJiAhaXNGdW5jdGlvbih2YWx1ZSk7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gaXNBcnJheUxpa2U7XG4iLCJ2YXIgcm9vdCA9IHJlcXVpcmUoJy4vX3Jvb3QnKSxcbiAgICBzdHViRmFsc2UgPSByZXF1aXJlKCcuL3N0dWJGYWxzZScpO1xuXG4vKiogRGV0ZWN0IGZyZWUgdmFyaWFibGUgYGV4cG9ydHNgLiAqL1xudmFyIGZyZWVFeHBvcnRzID0gdHlwZW9mIGV4cG9ydHMgPT0gJ29iamVjdCcgJiYgZXhwb3J0cyAmJiAhZXhwb3J0cy5ub2RlVHlwZSAmJiBleHBvcnRzO1xuXG4vKiogRGV0ZWN0IGZyZWUgdmFyaWFibGUgYG1vZHVsZWAuICovXG52YXIgZnJlZU1vZHVsZSA9IGZyZWVFeHBvcnRzICYmIHR5cGVvZiBtb2R1bGUgPT0gJ29iamVjdCcgJiYgbW9kdWxlICYmICFtb2R1bGUubm9kZVR5cGUgJiYgbW9kdWxlO1xuXG4vKiogRGV0ZWN0IHRoZSBwb3B1bGFyIENvbW1vbkpTIGV4dGVuc2lvbiBgbW9kdWxlLmV4cG9ydHNgLiAqL1xudmFyIG1vZHVsZUV4cG9ydHMgPSBmcmVlTW9kdWxlICYmIGZyZWVNb2R1bGUuZXhwb3J0cyA9PT0gZnJlZUV4cG9ydHM7XG5cbi8qKiBCdWlsdC1pbiB2YWx1ZSByZWZlcmVuY2VzLiAqL1xudmFyIEJ1ZmZlciA9IG1vZHVsZUV4cG9ydHMgPyByb290LkJ1ZmZlciA6IHVuZGVmaW5lZDtcblxuLyogQnVpbHQtaW4gbWV0aG9kIHJlZmVyZW5jZXMgZm9yIHRob3NlIHdpdGggdGhlIHNhbWUgbmFtZSBhcyBvdGhlciBgbG9kYXNoYCBtZXRob2RzLiAqL1xudmFyIG5hdGl2ZUlzQnVmZmVyID0gQnVmZmVyID8gQnVmZmVyLmlzQnVmZmVyIDogdW5kZWZpbmVkO1xuXG4vKipcbiAqIENoZWNrcyBpZiBgdmFsdWVgIGlzIGEgYnVmZmVyLlxuICpcbiAqIEBzdGF0aWNcbiAqIEBtZW1iZXJPZiBfXG4gKiBAc2luY2UgNC4zLjBcbiAqIEBjYXRlZ29yeSBMYW5nXG4gKiBAcGFyYW0geyp9IHZhbHVlIFRoZSB2YWx1ZSB0byBjaGVjay5cbiAqIEByZXR1cm5zIHtib29sZWFufSBSZXR1cm5zIGB0cnVlYCBpZiBgdmFsdWVgIGlzIGEgYnVmZmVyLCBlbHNlIGBmYWxzZWAuXG4gKiBAZXhhbXBsZVxuICpcbiAqIF8uaXNCdWZmZXIobmV3IEJ1ZmZlcigyKSk7XG4gKiAvLyA9PiB0cnVlXG4gKlxuICogXy5pc0J1ZmZlcihuZXcgVWludDhBcnJheSgyKSk7XG4gKiAvLyA9PiBmYWxzZVxuICovXG52YXIgaXNCdWZmZXIgPSBuYXRpdmVJc0J1ZmZlciB8fCBzdHViRmFsc2U7XG5cbm1vZHVsZS5leHBvcnRzID0gaXNCdWZmZXI7XG4iLCJ2YXIgYmFzZUdldFRhZyA9IHJlcXVpcmUoJy4vX2Jhc2VHZXRUYWcnKSxcbiAgICBpc09iamVjdCA9IHJlcXVpcmUoJy4vaXNPYmplY3QnKTtcblxuLyoqIGBPYmplY3QjdG9TdHJpbmdgIHJlc3VsdCByZWZlcmVuY2VzLiAqL1xudmFyIGFzeW5jVGFnID0gJ1tvYmplY3QgQXN5bmNGdW5jdGlvbl0nLFxuICAgIGZ1bmNUYWcgPSAnW29iamVjdCBGdW5jdGlvbl0nLFxuICAgIGdlblRhZyA9ICdbb2JqZWN0IEdlbmVyYXRvckZ1bmN0aW9uXScsXG4gICAgcHJveHlUYWcgPSAnW29iamVjdCBQcm94eV0nO1xuXG4vKipcbiAqIENoZWNrcyBpZiBgdmFsdWVgIGlzIGNsYXNzaWZpZWQgYXMgYSBgRnVuY3Rpb25gIG9iamVjdC5cbiAqXG4gKiBAc3RhdGljXG4gKiBAbWVtYmVyT2YgX1xuICogQHNpbmNlIDAuMS4wXG4gKiBAY2F0ZWdvcnkgTGFuZ1xuICogQHBhcmFtIHsqfSB2YWx1ZSBUaGUgdmFsdWUgdG8gY2hlY2suXG4gKiBAcmV0dXJucyB7Ym9vbGVhbn0gUmV0dXJucyBgdHJ1ZWAgaWYgYHZhbHVlYCBpcyBhIGZ1bmN0aW9uLCBlbHNlIGBmYWxzZWAuXG4gKiBAZXhhbXBsZVxuICpcbiAqIF8uaXNGdW5jdGlvbihfKTtcbiAqIC8vID0+IHRydWVcbiAqXG4gKiBfLmlzRnVuY3Rpb24oL2FiYy8pO1xuICogLy8gPT4gZmFsc2VcbiAqL1xuZnVuY3Rpb24gaXNGdW5jdGlvbih2YWx1ZSkge1xuICBpZiAoIWlzT2JqZWN0KHZhbHVlKSkge1xuICAgIHJldHVybiBmYWxzZTtcbiAgfVxuICAvLyBUaGUgdXNlIG9mIGBPYmplY3QjdG9TdHJpbmdgIGF2b2lkcyBpc3N1ZXMgd2l0aCB0aGUgYHR5cGVvZmAgb3BlcmF0b3JcbiAgLy8gaW4gU2FmYXJpIDkgd2hpY2ggcmV0dXJucyAnb2JqZWN0JyBmb3IgdHlwZWQgYXJyYXlzIGFuZCBvdGhlciBjb25zdHJ1Y3RvcnMuXG4gIHZhciB0YWcgPSBiYXNlR2V0VGFnKHZhbHVlKTtcbiAgcmV0dXJuIHRhZyA9PSBmdW5jVGFnIHx8IHRhZyA9PSBnZW5UYWcgfHwgdGFnID09IGFzeW5jVGFnIHx8IHRhZyA9PSBwcm94eVRhZztcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBpc0Z1bmN0aW9uO1xuIiwiLyoqIFVzZWQgYXMgcmVmZXJlbmNlcyBmb3IgdmFyaW91cyBgTnVtYmVyYCBjb25zdGFudHMuICovXG52YXIgTUFYX1NBRkVfSU5URUdFUiA9IDkwMDcxOTkyNTQ3NDA5OTE7XG5cbi8qKlxuICogQ2hlY2tzIGlmIGB2YWx1ZWAgaXMgYSB2YWxpZCBhcnJheS1saWtlIGxlbmd0aC5cbiAqXG4gKiAqKk5vdGU6KiogVGhpcyBtZXRob2QgaXMgbG9vc2VseSBiYXNlZCBvblxuICogW2BUb0xlbmd0aGBdKGh0dHA6Ly9lY21hLWludGVybmF0aW9uYWwub3JnL2VjbWEtMjYyLzcuMC8jc2VjLXRvbGVuZ3RoKS5cbiAqXG4gKiBAc3RhdGljXG4gKiBAbWVtYmVyT2YgX1xuICogQHNpbmNlIDQuMC4wXG4gKiBAY2F0ZWdvcnkgTGFuZ1xuICogQHBhcmFtIHsqfSB2YWx1ZSBUaGUgdmFsdWUgdG8gY2hlY2suXG4gKiBAcmV0dXJucyB7Ym9vbGVhbn0gUmV0dXJucyBgdHJ1ZWAgaWYgYHZhbHVlYCBpcyBhIHZhbGlkIGxlbmd0aCwgZWxzZSBgZmFsc2VgLlxuICogQGV4YW1wbGVcbiAqXG4gKiBfLmlzTGVuZ3RoKDMpO1xuICogLy8gPT4gdHJ1ZVxuICpcbiAqIF8uaXNMZW5ndGgoTnVtYmVyLk1JTl9WQUxVRSk7XG4gKiAvLyA9PiBmYWxzZVxuICpcbiAqIF8uaXNMZW5ndGgoSW5maW5pdHkpO1xuICogLy8gPT4gZmFsc2VcbiAqXG4gKiBfLmlzTGVuZ3RoKCczJyk7XG4gKiAvLyA9PiBmYWxzZVxuICovXG5mdW5jdGlvbiBpc0xlbmd0aCh2YWx1ZSkge1xuICByZXR1cm4gdHlwZW9mIHZhbHVlID09ICdudW1iZXInICYmXG4gICAgdmFsdWUgPiAtMSAmJiB2YWx1ZSAlIDEgPT0gMCAmJiB2YWx1ZSA8PSBNQVhfU0FGRV9JTlRFR0VSO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IGlzTGVuZ3RoO1xuIiwiLyoqXG4gKiBDaGVja3MgaWYgYHZhbHVlYCBpcyB0aGVcbiAqIFtsYW5ndWFnZSB0eXBlXShodHRwOi8vd3d3LmVjbWEtaW50ZXJuYXRpb25hbC5vcmcvZWNtYS0yNjIvNy4wLyNzZWMtZWNtYXNjcmlwdC1sYW5ndWFnZS10eXBlcylcbiAqIG9mIGBPYmplY3RgLiAoZS5nLiBhcnJheXMsIGZ1bmN0aW9ucywgb2JqZWN0cywgcmVnZXhlcywgYG5ldyBOdW1iZXIoMClgLCBhbmQgYG5ldyBTdHJpbmcoJycpYClcbiAqXG4gKiBAc3RhdGljXG4gKiBAbWVtYmVyT2YgX1xuICogQHNpbmNlIDAuMS4wXG4gKiBAY2F0ZWdvcnkgTGFuZ1xuICogQHBhcmFtIHsqfSB2YWx1ZSBUaGUgdmFsdWUgdG8gY2hlY2suXG4gKiBAcmV0dXJucyB7Ym9vbGVhbn0gUmV0dXJucyBgdHJ1ZWAgaWYgYHZhbHVlYCBpcyBhbiBvYmplY3QsIGVsc2UgYGZhbHNlYC5cbiAqIEBleGFtcGxlXG4gKlxuICogXy5pc09iamVjdCh7fSk7XG4gKiAvLyA9PiB0cnVlXG4gKlxuICogXy5pc09iamVjdChbMSwgMiwgM10pO1xuICogLy8gPT4gdHJ1ZVxuICpcbiAqIF8uaXNPYmplY3QoXy5ub29wKTtcbiAqIC8vID0+IHRydWVcbiAqXG4gKiBfLmlzT2JqZWN0KG51bGwpO1xuICogLy8gPT4gZmFsc2VcbiAqL1xuZnVuY3Rpb24gaXNPYmplY3QodmFsdWUpIHtcbiAgdmFyIHR5cGUgPSB0eXBlb2YgdmFsdWU7XG4gIHJldHVybiB2YWx1ZSAhPSBudWxsICYmICh0eXBlID09ICdvYmplY3QnIHx8IHR5cGUgPT0gJ2Z1bmN0aW9uJyk7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gaXNPYmplY3Q7XG4iLCIvKipcbiAqIENoZWNrcyBpZiBgdmFsdWVgIGlzIG9iamVjdC1saWtlLiBBIHZhbHVlIGlzIG9iamVjdC1saWtlIGlmIGl0J3Mgbm90IGBudWxsYFxuICogYW5kIGhhcyBhIGB0eXBlb2ZgIHJlc3VsdCBvZiBcIm9iamVjdFwiLlxuICpcbiAqIEBzdGF0aWNcbiAqIEBtZW1iZXJPZiBfXG4gKiBAc2luY2UgNC4wLjBcbiAqIEBjYXRlZ29yeSBMYW5nXG4gKiBAcGFyYW0geyp9IHZhbHVlIFRoZSB2YWx1ZSB0byBjaGVjay5cbiAqIEByZXR1cm5zIHtib29sZWFufSBSZXR1cm5zIGB0cnVlYCBpZiBgdmFsdWVgIGlzIG9iamVjdC1saWtlLCBlbHNlIGBmYWxzZWAuXG4gKiBAZXhhbXBsZVxuICpcbiAqIF8uaXNPYmplY3RMaWtlKHt9KTtcbiAqIC8vID0+IHRydWVcbiAqXG4gKiBfLmlzT2JqZWN0TGlrZShbMSwgMiwgM10pO1xuICogLy8gPT4gdHJ1ZVxuICpcbiAqIF8uaXNPYmplY3RMaWtlKF8ubm9vcCk7XG4gKiAvLyA9PiBmYWxzZVxuICpcbiAqIF8uaXNPYmplY3RMaWtlKG51bGwpO1xuICogLy8gPT4gZmFsc2VcbiAqL1xuZnVuY3Rpb24gaXNPYmplY3RMaWtlKHZhbHVlKSB7XG4gIHJldHVybiB2YWx1ZSAhPSBudWxsICYmIHR5cGVvZiB2YWx1ZSA9PSAnb2JqZWN0Jztcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBpc09iamVjdExpa2U7XG4iLCJ2YXIgYmFzZUlzVHlwZWRBcnJheSA9IHJlcXVpcmUoJy4vX2Jhc2VJc1R5cGVkQXJyYXknKSxcbiAgICBiYXNlVW5hcnkgPSByZXF1aXJlKCcuL19iYXNlVW5hcnknKSxcbiAgICBub2RlVXRpbCA9IHJlcXVpcmUoJy4vX25vZGVVdGlsJyk7XG5cbi8qIE5vZGUuanMgaGVscGVyIHJlZmVyZW5jZXMuICovXG52YXIgbm9kZUlzVHlwZWRBcnJheSA9IG5vZGVVdGlsICYmIG5vZGVVdGlsLmlzVHlwZWRBcnJheTtcblxuLyoqXG4gKiBDaGVja3MgaWYgYHZhbHVlYCBpcyBjbGFzc2lmaWVkIGFzIGEgdHlwZWQgYXJyYXkuXG4gKlxuICogQHN0YXRpY1xuICogQG1lbWJlck9mIF9cbiAqIEBzaW5jZSAzLjAuMFxuICogQGNhdGVnb3J5IExhbmdcbiAqIEBwYXJhbSB7Kn0gdmFsdWUgVGhlIHZhbHVlIHRvIGNoZWNrLlxuICogQHJldHVybnMge2Jvb2xlYW59IFJldHVybnMgYHRydWVgIGlmIGB2YWx1ZWAgaXMgYSB0eXBlZCBhcnJheSwgZWxzZSBgZmFsc2VgLlxuICogQGV4YW1wbGVcbiAqXG4gKiBfLmlzVHlwZWRBcnJheShuZXcgVWludDhBcnJheSk7XG4gKiAvLyA9PiB0cnVlXG4gKlxuICogXy5pc1R5cGVkQXJyYXkoW10pO1xuICogLy8gPT4gZmFsc2VcbiAqL1xudmFyIGlzVHlwZWRBcnJheSA9IG5vZGVJc1R5cGVkQXJyYXkgPyBiYXNlVW5hcnkobm9kZUlzVHlwZWRBcnJheSkgOiBiYXNlSXNUeXBlZEFycmF5O1xuXG5tb2R1bGUuZXhwb3J0cyA9IGlzVHlwZWRBcnJheTtcbiIsInZhciBhcnJheUxpa2VLZXlzID0gcmVxdWlyZSgnLi9fYXJyYXlMaWtlS2V5cycpLFxuICAgIGJhc2VLZXlzID0gcmVxdWlyZSgnLi9fYmFzZUtleXMnKSxcbiAgICBpc0FycmF5TGlrZSA9IHJlcXVpcmUoJy4vaXNBcnJheUxpa2UnKTtcblxuLyoqXG4gKiBDcmVhdGVzIGFuIGFycmF5IG9mIHRoZSBvd24gZW51bWVyYWJsZSBwcm9wZXJ0eSBuYW1lcyBvZiBgb2JqZWN0YC5cbiAqXG4gKiAqKk5vdGU6KiogTm9uLW9iamVjdCB2YWx1ZXMgYXJlIGNvZXJjZWQgdG8gb2JqZWN0cy4gU2VlIHRoZVxuICogW0VTIHNwZWNdKGh0dHA6Ly9lY21hLWludGVybmF0aW9uYWwub3JnL2VjbWEtMjYyLzcuMC8jc2VjLW9iamVjdC5rZXlzKVxuICogZm9yIG1vcmUgZGV0YWlscy5cbiAqXG4gKiBAc3RhdGljXG4gKiBAc2luY2UgMC4xLjBcbiAqIEBtZW1iZXJPZiBfXG4gKiBAY2F0ZWdvcnkgT2JqZWN0XG4gKiBAcGFyYW0ge09iamVjdH0gb2JqZWN0IFRoZSBvYmplY3QgdG8gcXVlcnkuXG4gKiBAcmV0dXJucyB7QXJyYXl9IFJldHVybnMgdGhlIGFycmF5IG9mIHByb3BlcnR5IG5hbWVzLlxuICogQGV4YW1wbGVcbiAqXG4gKiBmdW5jdGlvbiBGb28oKSB7XG4gKiAgIHRoaXMuYSA9IDE7XG4gKiAgIHRoaXMuYiA9IDI7XG4gKiB9XG4gKlxuICogRm9vLnByb3RvdHlwZS5jID0gMztcbiAqXG4gKiBfLmtleXMobmV3IEZvbyk7XG4gKiAvLyA9PiBbJ2EnLCAnYiddIChpdGVyYXRpb24gb3JkZXIgaXMgbm90IGd1YXJhbnRlZWQpXG4gKlxuICogXy5rZXlzKCdoaScpO1xuICogLy8gPT4gWycwJywgJzEnXVxuICovXG5mdW5jdGlvbiBrZXlzKG9iamVjdCkge1xuICByZXR1cm4gaXNBcnJheUxpa2Uob2JqZWN0KSA/IGFycmF5TGlrZUtleXMob2JqZWN0KSA6IGJhc2VLZXlzKG9iamVjdCk7XG59XG5cbm1vZHVsZS5leHBvcnRzID0ga2V5cztcbiIsIi8qKlxuICogVGhpcyBtZXRob2QgcmV0dXJucyBgZmFsc2VgLlxuICpcbiAqIEBzdGF0aWNcbiAqIEBtZW1iZXJPZiBfXG4gKiBAc2luY2UgNC4xMy4wXG4gKiBAY2F0ZWdvcnkgVXRpbFxuICogQHJldHVybnMge2Jvb2xlYW59IFJldHVybnMgYGZhbHNlYC5cbiAqIEBleGFtcGxlXG4gKlxuICogXy50aW1lcygyLCBfLnN0dWJGYWxzZSk7XG4gKiAvLyA9PiBbZmFsc2UsIGZhbHNlXVxuICovXG5mdW5jdGlvbiBzdHViRmFsc2UoKSB7XG4gIHJldHVybiBmYWxzZTtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBzdHViRmFsc2U7XG4iLCIvLyBUSElTIEZJTEUgSVMgR0VORVJBVEVEIC0gRE8gTk9UIEVESVQhXG4vKiFtb2JpbGUtZGV0ZWN0IHYxLjQuNCAyMDE5LTA5LTIxKi9cbi8qZ2xvYmFsIG1vZHVsZTpmYWxzZSwgZGVmaW5lOmZhbHNlKi9cbi8qanNoaW50IGxhdGVkZWY6ZmFsc2UqL1xuLyohQGxpY2Vuc2UgQ29weXJpZ2h0IDIwMTMsIEhlaW5yaWNoIEdvZWJsLCBMaWNlbnNlOiBNSVQsIHNlZSBodHRwczovL2dpdGh1Yi5jb20vaGdvZWJsL21vYmlsZS1kZXRlY3QuanMqL1xuKGZ1bmN0aW9uIChkZWZpbmUsIHVuZGVmaW5lZCkge1xuZGVmaW5lKGZ1bmN0aW9uICgpIHtcbiAgICAndXNlIHN0cmljdCc7XG5cbiAgICB2YXIgaW1wbCA9IHt9O1xuXG4gICAgaW1wbC5tb2JpbGVEZXRlY3RSdWxlcyA9IHtcbiAgICBcInBob25lc1wiOiB7XG4gICAgICAgIFwiaVBob25lXCI6IFwiXFxcXGJpUGhvbmVcXFxcYnxcXFxcYmlQb2RcXFxcYlwiLFxuICAgICAgICBcIkJsYWNrQmVycnlcIjogXCJCbGFja0JlcnJ5fFxcXFxiQkIxMFxcXFxifHJpbVswLTldK3xcXFxcYihCQkExMDB8QkJCMTAwfEJCRDEwMHxCQkUxMDB8QkJGMTAwfFNUSDEwMClcXFxcYi1bMC05XStcIixcbiAgICAgICAgXCJIVENcIjogXCJIVEN8SFRDLiooU2Vuc2F0aW9ufEV2b3xWaXNpb258RXhwbG9yZXJ8NjgwMHw4MTAwfDg5MDB8QTcyNzJ8UzUxMGV8QzExMGV8TGVnZW5kfERlc2lyZXxUODI4Mil8QVBYNTE1Q0tUfFF0ZWs5MDkwfEFQQTkyOTJLVHxIRF9taW5pfFNlbnNhdGlvbi4qWjcxMGV8UEc4NjEwMHxaNzE1ZXxEZXNpcmUuKihBODE4MXxIRCl8QURSNjIwMHxBRFI2NDAwTHxBRFI2NDI1fDAwMUhUfEluc3BpcmUgNEd8QW5kcm9pZC4qXFxcXGJFVk9cXFxcYnxULU1vYmlsZSBHMXxaNTIwbXxBbmRyb2lkIFswLTkuXSs7IFBpeGVsXCIsXG4gICAgICAgIFwiTmV4dXNcIjogXCJOZXh1cyBPbmV8TmV4dXMgU3xHYWxheHkuKk5leHVzfEFuZHJvaWQuKk5leHVzLipNb2JpbGV8TmV4dXMgNHxOZXh1cyA1fE5leHVzIDZcIixcbiAgICAgICAgXCJEZWxsXCI6IFwiRGVsbFs7XT8gKFN0cmVha3xBZXJvfFZlbnVlfFZlbnVlIFByb3xGbGFzaHxTbW9rZXxNaW5pIDNpWCl8WENEMjh8WENEMzV8XFxcXGIwMDFETFxcXFxifFxcXFxiMTAxRExcXFxcYnxcXFxcYkdTMDFcXFxcYlwiLFxuICAgICAgICBcIk1vdG9yb2xhXCI6IFwiTW90b3JvbGF8RFJPSURYfERST0lEIEJJT05JQ3xcXFxcYkRyb2lkXFxcXGIuKkJ1aWxkfEFuZHJvaWQuKlhvb218SFJJMzl8TU9ULXxBMTI2MHxBMTY4MHxBNTU1fEE4NTN8QTg1NXxBOTUzfEE5NTV8QTk1NnxNb3Rvcm9sYS4qRUxFQ1RSSUZZfE1vdG9yb2xhLippMXxpODY3fGk5NDB8TUIyMDB8TUIzMDB8TUI1MDF8TUI1MDJ8TUI1MDh8TUI1MTF8TUI1MjB8TUI1MjV8TUI1MjZ8TUI2MTF8TUI2MTJ8TUI2MzJ8TUI4MTB8TUI4NTV8TUI4NjB8TUI4NjF8TUI4NjV8TUI4NzB8TUU1MDF8TUU1MDJ8TUU1MTF8TUU1MjV8TUU2MDB8TUU2MzJ8TUU3MjJ8TUU4MTF8TUU4NjB8TUU4NjN8TUU4NjV8TVQ2MjB8TVQ3MTB8TVQ3MTZ8TVQ3MjB8TVQ4MTB8TVQ4NzB8TVQ5MTd8TW90b3JvbGEuKlRJVEFOSVVNfFdYNDM1fFdYNDQ1fFhUMzAwfFhUMzAxfFhUMzExfFhUMzE2fFhUMzE3fFhUMzE5fFhUMzIwfFhUMzkwfFhUNTAyfFhUNTMwfFhUNTMxfFhUNTMyfFhUNTM1fFhUNjAzfFhUNjEwfFhUNjExfFhUNjE1fFhUNjgxfFhUNzAxfFhUNzAyfFhUNzExfFhUNzIwfFhUODAwfFhUODA2fFhUODYwfFhUODYyfFhUODc1fFhUODgyfFhUODgzfFhUODk0fFhUOTAxfFhUOTA3fFhUOTA5fFhUOTEwfFhUOTEyfFhUOTI4fFhUOTI2fFhUOTE1fFhUOTE5fFhUOTI1fFhUMTAyMXxcXFxcYk1vdG8gRVxcXFxifFhUMTA2OHxYVDEwOTJ8WFQxMDUyXCIsXG4gICAgICAgIFwiU2Ftc3VuZ1wiOiBcIlxcXFxiU2Ftc3VuZ1xcXFxifFNNLUc5NTBGfFNNLUc5NTVGfFNNLUc5MjUwfEdULTE5MzAwfFNHSC1JMzM3fEJHVC1TNTIzMHxHVC1CMjEwMHxHVC1CMjcwMHxHVC1CMjcxMHxHVC1CMzIxMHxHVC1CMzMxMHxHVC1CMzQxMHxHVC1CMzczMHxHVC1CMzc0MHxHVC1CNTUxMHxHVC1CNTUxMnxHVC1CNTcyMnxHVC1CNjUyMHxHVC1CNzMwMHxHVC1CNzMyMHxHVC1CNzMzMHxHVC1CNzM1MHxHVC1CNzUxMHxHVC1CNzcyMnxHVC1CNzgwMHxHVC1DMzAxMHxHVC1DMzAxMXxHVC1DMzA2MHxHVC1DMzIwMHxHVC1DMzIxMnxHVC1DMzIxMkl8R1QtQzMyNjJ8R1QtQzMyMjJ8R1QtQzMzMDB8R1QtQzMzMDBLfEdULUMzMzAzfEdULUMzMzAzS3xHVC1DMzMxMHxHVC1DMzMyMnxHVC1DMzMzMHxHVC1DMzM1MHxHVC1DMzUwMHxHVC1DMzUxMHxHVC1DMzUzMHxHVC1DMzYzMHxHVC1DMzc4MHxHVC1DNTAxMHxHVC1DNTIxMnxHVC1DNjYyMHxHVC1DNjYyNXxHVC1DNjcxMnxHVC1FMTA1MHxHVC1FMTA3MHxHVC1FMTA3NXxHVC1FMTA4MHxHVC1FMTA4MXxHVC1FMTA4NXxHVC1FMTA4N3xHVC1FMTEwMHxHVC1FMTEwN3xHVC1FMTExMHxHVC1FMTEyMHxHVC1FMTEyNXxHVC1FMTEzMHxHVC1FMTE2MHxHVC1FMTE3MHxHVC1FMTE3NXxHVC1FMTE4MHxHVC1FMTE4MnxHVC1FMTIwMHxHVC1FMTIxMHxHVC1FMTIyNXxHVC1FMTIzMHxHVC1FMTM5MHxHVC1FMjEwMHxHVC1FMjEyMHxHVC1FMjEyMXxHVC1FMjE1MnxHVC1FMjIyMHxHVC1FMjIyMnxHVC1FMjIzMHxHVC1FMjIzMnxHVC1FMjI1MHxHVC1FMjM3MHxHVC1FMjU1MHxHVC1FMjY1MnxHVC1FMzIxMHxHVC1FMzIxM3xHVC1JNTUwMHxHVC1JNTUwM3xHVC1JNTcwMHxHVC1JNTgwMHxHVC1JNTgwMXxHVC1JNjQxMHxHVC1JNjQyMHxHVC1JNzExMHxHVC1JNzQxMHxHVC1JNzUwMHxHVC1JODAwMHxHVC1JODE1MHxHVC1JODE2MHxHVC1JODE5MHxHVC1JODMyMHxHVC1JODMzMHxHVC1JODM1MHxHVC1JODUzMHxHVC1JODcwMHxHVC1JODcwM3xHVC1JODkxMHxHVC1JOTAwMHxHVC1JOTAwMXxHVC1JOTAwM3xHVC1JOTAxMHxHVC1JOTAyMHxHVC1JOTAyM3xHVC1JOTA3MHxHVC1JOTA4MnxHVC1JOTEwMHxHVC1JOTEwM3xHVC1JOTIyMHxHVC1JOTI1MHxHVC1JOTMwMHxHVC1JOTMwNXxHVC1JOTUwMHxHVC1JOTUwNXxHVC1NMzUxMHxHVC1NNTY1MHxHVC1NNzUwMHxHVC1NNzYwMHxHVC1NNzYwM3xHVC1NODgwMHxHVC1NODkxMHxHVC1ONzAwMHxHVC1TMzExMHxHVC1TMzMxMHxHVC1TMzM1MHxHVC1TMzM1M3xHVC1TMzM3MHxHVC1TMzY1MHxHVC1TMzY1M3xHVC1TMzc3MHxHVC1TMzg1MHxHVC1TNTIxMHxHVC1TNTIyMHxHVC1TNTIyOXxHVC1TNTIzMHxHVC1TNTIzM3xHVC1TNTI1MHxHVC1TNTI1M3xHVC1TNTI2MHxHVC1TNTI2M3xHVC1TNTI3MHxHVC1TNTMwMHxHVC1TNTMzMHxHVC1TNTM1MHxHVC1TNTM2MHxHVC1TNTM2M3xHVC1TNTM2OXxHVC1TNTM4MHxHVC1TNTM4MER8R1QtUzU1NjB8R1QtUzU1NzB8R1QtUzU2MDB8R1QtUzU2MDN8R1QtUzU2MTB8R1QtUzU2MjB8R1QtUzU2NjB8R1QtUzU2NzB8R1QtUzU2OTB8R1QtUzU3NTB8R1QtUzU3ODB8R1QtUzU4MzB8R1QtUzU4Mzl8R1QtUzYxMDJ8R1QtUzY1MDB8R1QtUzcwNzB8R1QtUzcyMDB8R1QtUzcyMjB8R1QtUzcyMzB8R1QtUzcyMzN8R1QtUzcyNTB8R1QtUzc1MDB8R1QtUzc1MzB8R1QtUzc1NTB8R1QtUzc1NjJ8R1QtUzc3MTB8R1QtUzgwMDB8R1QtUzgwMDN8R1QtUzg1MDB8R1QtUzg1MzB8R1QtUzg2MDB8U0NILUEzMTB8U0NILUE1MzB8U0NILUE1NzB8U0NILUE2MTB8U0NILUE2MzB8U0NILUE2NTB8U0NILUE3OTB8U0NILUE3OTV8U0NILUE4NTB8U0NILUE4NzB8U0NILUE4OTB8U0NILUE5MzB8U0NILUE5NTB8U0NILUE5NzB8U0NILUE5OTB8U0NILUkxMDB8U0NILUkxMTB8U0NILUk0MDB8U0NILUk0MDV8U0NILUk1MDB8U0NILUk1MTB8U0NILUk1MTV8U0NILUk2MDB8U0NILUk3MzB8U0NILUk3NjB8U0NILUk3NzB8U0NILUk4MzB8U0NILUk5MTB8U0NILUk5MjB8U0NILUk5NTl8U0NILUxDMTF8U0NILU4xNTB8U0NILU4zMDB8U0NILVIxMDB8U0NILVIzMDB8U0NILVIzNTF8U0NILVI0MDB8U0NILVI0MTB8U0NILVQzMDB8U0NILVUzMTB8U0NILVUzMjB8U0NILVUzNTB8U0NILVUzNjB8U0NILVUzNjV8U0NILVUzNzB8U0NILVUzODB8U0NILVU0MTB8U0NILVU0MzB8U0NILVU0NTB8U0NILVU0NjB8U0NILVU0NzB8U0NILVU0OTB8U0NILVU1NDB8U0NILVU1NTB8U0NILVU2MjB8U0NILVU2NDB8U0NILVU2NTB8U0NILVU2NjB8U0NILVU3MDB8U0NILVU3NDB8U0NILVU3NTB8U0NILVU4MTB8U0NILVU4MjB8U0NILVU5MDB8U0NILVU5NDB8U0NILVU5NjB8U0NTLTI2VUN8U0dILUExMDd8U0dILUExMTd8U0dILUExMjd8U0dILUExMzd8U0dILUExNTd8U0dILUExNjd8U0dILUExNzd8U0dILUExODd8U0dILUExOTd8U0dILUEyMjd8U0dILUEyMzd8U0dILUEyNTd8U0dILUE0Mzd8U0dILUE1MTd8U0dILUE1OTd8U0dILUE2Mzd8U0dILUE2NTd8U0dILUE2Njd8U0dILUE2ODd8U0dILUE2OTd8U0dILUE3MDd8U0dILUE3MTd8U0dILUE3Mjd8U0dILUE3Mzd8U0dILUE3NDd8U0dILUE3Njd8U0dILUE3Nzd8U0dILUE3OTd8U0dILUE4MTd8U0dILUE4Mjd8U0dILUE4Mzd8U0dILUE4NDd8U0dILUE4Njd8U0dILUE4Nzd8U0dILUE4ODd8U0dILUE4OTd8U0dILUE5Mjd8U0dILUIxMDB8U0dILUIxMzB8U0dILUIyMDB8U0dILUIyMjB8U0dILUMxMDB8U0dILUMxMTB8U0dILUMxMjB8U0dILUMxMzB8U0dILUMxNDB8U0dILUMxNjB8U0dILUMxNzB8U0dILUMxODB8U0dILUMyMDB8U0dILUMyMDd8U0dILUMyMTB8U0dILUMyMjV8U0dILUMyMzB8U0dILUM0MTd8U0dILUM0NTB8U0dILUQzMDd8U0dILUQzNDd8U0dILUQzNTd8U0dILUQ0MDd8U0dILUQ0MTV8U0dILUQ3ODB8U0dILUQ4MDd8U0dILUQ5ODB8U0dILUUxMDV8U0dILUUyMDB8U0dILUUzMTV8U0dILUUzMTZ8U0dILUUzMTd8U0dILUUzMzV8U0dILUU1OTB8U0dILUU2MzV8U0dILUU3MTV8U0dILUU4OTB8U0dILUYzMDB8U0dILUY0ODB8U0dILUkyMDB8U0dILUkzMDB8U0dILUkzMjB8U0dILUk1NTB8U0dILUk1Nzd8U0dILUk2MDB8U0dILUk2MDd8U0dILUk2MTd8U0dILUk2Mjd8U0dILUk2Mzd8U0dILUk2Nzd8U0dILUk3MDB8U0dILUk3MTd8U0dILUk3Mjd8U0dILWk3NDdNfFNHSC1JNzc3fFNHSC1JNzgwfFNHSC1JODI3fFNHSC1JODQ3fFNHSC1JODU3fFNHSC1JODk2fFNHSC1JODk3fFNHSC1JOTAwfFNHSC1JOTA3fFNHSC1JOTE3fFNHSC1JOTI3fFNHSC1JOTM3fFNHSC1JOTk3fFNHSC1KMTUwfFNHSC1KMjAwfFNHSC1MMTcwfFNHSC1MNzAwfFNHSC1NMTEwfFNHSC1NMTUwfFNHSC1NMjAwfFNHSC1OMTA1fFNHSC1ONTAwfFNHSC1ONjAwfFNHSC1ONjIwfFNHSC1ONjI1fFNHSC1ONzAwfFNHSC1ONzEwfFNHSC1QMTA3fFNHSC1QMjA3fFNHSC1QMzAwfFNHSC1QMzEwfFNHSC1QNTIwfFNHSC1QNzM1fFNHSC1QNzc3fFNHSC1RMTA1fFNHSC1SMjEwfFNHSC1SMjIwfFNHSC1SMjI1fFNHSC1TMTA1fFNHSC1TMzA3fFNHSC1UMTA5fFNHSC1UMTE5fFNHSC1UMTM5fFNHSC1UMjA5fFNHSC1UMjE5fFNHSC1UMjI5fFNHSC1UMjM5fFNHSC1UMjQ5fFNHSC1UMjU5fFNHSC1UMzA5fFNHSC1UMzE5fFNHSC1UMzI5fFNHSC1UMzM5fFNHSC1UMzQ5fFNHSC1UMzU5fFNHSC1UMzY5fFNHSC1UMzc5fFNHSC1UNDA5fFNHSC1UNDI5fFNHSC1UNDM5fFNHSC1UNDU5fFNHSC1UNDY5fFNHSC1UNDc5fFNHSC1UNDk5fFNHSC1UNTA5fFNHSC1UNTE5fFNHSC1UNTM5fFNHSC1UNTU5fFNHSC1UNTg5fFNHSC1UNjA5fFNHSC1UNjE5fFNHSC1UNjI5fFNHSC1UNjM5fFNHSC1UNjU5fFNHSC1UNjY5fFNHSC1UNjc5fFNHSC1UNzA5fFNHSC1UNzE5fFNHSC1UNzI5fFNHSC1UNzM5fFNHSC1UNzQ2fFNHSC1UNzQ5fFNHSC1UNzU5fFNHSC1UNzY5fFNHSC1UODA5fFNHSC1UODE5fFNHSC1UODM5fFNHSC1UOTE5fFNHSC1UOTI5fFNHSC1UOTM5fFNHSC1UOTU5fFNHSC1UOTg5fFNHSC1VMTAwfFNHSC1VMjAwfFNHSC1VODAwfFNHSC1WMjA1fFNHSC1WMjA2fFNHSC1YMTAwfFNHSC1YMTA1fFNHSC1YMTIwfFNHSC1YMTQwfFNHSC1YNDI2fFNHSC1YNDI3fFNHSC1YNDc1fFNHSC1YNDk1fFNHSC1YNDk3fFNHSC1YNTA3fFNHSC1YNjAwfFNHSC1YNjEwfFNHSC1YNjIwfFNHSC1YNjMwfFNHSC1YNzAwfFNHSC1YODIwfFNHSC1YODkwfFNHSC1aMTMwfFNHSC1aMTUwfFNHSC1aMTcwfFNHSC1aWDEwfFNHSC1aWDIwfFNIVy1NMTEwfFNQSC1BMTIwfFNQSC1BNDAwfFNQSC1BNDIwfFNQSC1BNDYwfFNQSC1BNTAwfFNQSC1BNTYwfFNQSC1BNjAwfFNQSC1BNjIwfFNQSC1BNjYwfFNQSC1BNzAwfFNQSC1BNzQwfFNQSC1BNzYwfFNQSC1BNzkwfFNQSC1BODAwfFNQSC1BODIwfFNQSC1BODQwfFNQSC1BODgwfFNQSC1BOTAwfFNQSC1BOTQwfFNQSC1BOTYwfFNQSC1ENjAwfFNQSC1ENzAwfFNQSC1ENzEwfFNQSC1ENzIwfFNQSC1JMzAwfFNQSC1JMzI1fFNQSC1JMzMwfFNQSC1JMzUwfFNQSC1JNTAwfFNQSC1JNjAwfFNQSC1JNzAwfFNQSC1MNzAwfFNQSC1NMTAwfFNQSC1NMjIwfFNQSC1NMjQwfFNQSC1NMzAwfFNQSC1NMzA1fFNQSC1NMzIwfFNQSC1NMzMwfFNQSC1NMzUwfFNQSC1NMzYwfFNQSC1NMzcwfFNQSC1NMzgwfFNQSC1NNTEwfFNQSC1NNTQwfFNQSC1NNTUwfFNQSC1NNTYwfFNQSC1NNTcwfFNQSC1NNTgwfFNQSC1NNjEwfFNQSC1NNjIwfFNQSC1NNjMwfFNQSC1NODAwfFNQSC1NODEwfFNQSC1NODUwfFNQSC1NOTAwfFNQSC1NOTEwfFNQSC1NOTIwfFNQSC1NOTMwfFNQSC1OMTAwfFNQSC1OMjAwfFNQSC1OMjQwfFNQSC1OMzAwfFNQSC1ONDAwfFNQSC1aNDAwfFNXQy1FMTAwfFNDSC1pOTA5fEdULU43MTAwfEdULU43MTA1fFNDSC1JNTM1fFNNLU45MDBBfFNHSC1JMzE3fFNHSC1UOTk5THxHVC1TNTM2MEJ8R1QtSTgyNjJ8R1QtUzY4MDJ8R1QtUzYzMTJ8R1QtUzYzMTB8R1QtUzUzMTJ8R1QtUzUzMTB8R1QtSTkxMDV8R1QtSTg1MTB8R1QtUzY3OTBOfFNNLUc3MTA1fFNNLU45MDA1fEdULVM1MzAxfEdULUk5Mjk1fEdULUk5MTk1fFNNLUMxMDF8R1QtUzczOTJ8R1QtUzc1NjB8R1QtQjc2MTB8R1QtSTU1MTB8R1QtUzc1ODJ8R1QtUzc1MzBFfEdULUk4NzUwfFNNLUc5MDA2VnxTTS1HOTAwOFZ8U00tRzkwMDlEfFNNLUc5MDBBfFNNLUc5MDBEfFNNLUc5MDBGfFNNLUc5MDBIfFNNLUc5MDBJfFNNLUc5MDBKfFNNLUc5MDBLfFNNLUc5MDBMfFNNLUc5MDBNfFNNLUc5MDBQfFNNLUc5MDBSNHxTTS1HOTAwU3xTTS1HOTAwVHxTTS1HOTAwVnxTTS1HOTAwVzh8U0hWLUUxNjBLfFNDSC1QNzA5fFNDSC1QNzI5fFNNLVQyNTU4fEdULUk5MjA1fFNNLUc5MzUwfFNNLUoxMjBGfFNNLUc5MjBGfFNNLUc5MjBWfFNNLUc5MzBGfFNNLU45MTBDfFNNLUEzMTBGfEdULUk5MTkwfFNNLUo1MDBGTnxTTS1HOTAzRnxTTS1KMzMwRlwiLFxuICAgICAgICBcIkxHXCI6IFwiXFxcXGJMR1xcXFxiO3xMR1stIF0/KEM4MDB8QzkwMHxFNDAwfEU2MTB8RTkwMHxFLTkwMHxGMTYwfEYxODBLfEYxODBMfEYxODBTfDczMHw4NTV8TDE2MHxMUzc0MHxMUzg0MHxMUzk3MHxMVTYyMDB8TVM2OTB8TVM2OTV8TVM3NzB8TVM4NDB8TVM4NzB8TVM5MTB8UDUwMHxQNzAwfFA3MDV8Vk02OTZ8QVM2ODB8QVM2OTV8QVg4NDB8QzcyOXxFOTcwfEdTNTA1fDI3MnxDMzk1fEU3MzlCS3xFOTYwfEw1NUN8TDc1Q3xMUzY5NnxMUzg2MHxQNzY5Qkt8UDM1MHxQNTAwfFA1MDl8UDg3MHxVTjI3MnxVUzczMHxWUzg0MHxWUzk1MHxMTjI3MnxMTjUxMHxMUzY3MHxMUzg1NXxMVzY5MHxNTjI3MHxNTjUxMHxQNTA5fFA3Njl8UDkzMHxVTjIwMHxVTjI3MHxVTjUxMHxVTjYxMHxVUzY3MHxVUzc0MHxVUzc2MHxVWDI2NXxVWDg0MHxWTjI3MXxWTjUzMHxWUzY2MHxWUzcwMHxWUzc0MHxWUzc1MHxWUzkxMHxWUzkyMHxWUzkzMHxWWDkyMDB8VlgxMTAwMHxBWDg0MEF8TFc3NzB8UDUwNnxQOTI1fFA5OTl8RTYxMnxEOTU1fEQ4MDJ8TVMzMjN8TTI1Nyl8TE0tRzcxMFwiLFxuICAgICAgICBcIlNvbnlcIjogXCJTb255U1R8U29ueUxUfFNvbnlFcmljc3NvbnxTb255RXJpY3Nzb25MVDE1aXZ8TFQxOGl8RTEwaXxMVDI4aHxMVDI2d3xTb255RXJpY3Nzb25NVDI3aXxDNTMwM3xDNjkwMnxDNjkwM3xDNjkwNnxDNjk0M3xEMjUzM1wiLFxuICAgICAgICBcIkFzdXNcIjogXCJBc3VzLipHYWxheHl8UGFkRm9uZS4qTW9iaWxlXCIsXG4gICAgICAgIFwiTm9raWFMdW1pYVwiOiBcIkx1bWlhIFswLTldezMsNH1cIixcbiAgICAgICAgXCJNaWNyb21heFwiOiBcIk1pY3JvbWF4LipcXFxcYihBMjEwfEE5MnxBODh8QTcyfEExMTF8QTExMFF8QTExNXxBMTE2fEExMTB8QTkwU3xBMjZ8QTUxfEEzNXxBNTR8QTI1fEEyN3xBODl8QTY4fEE2NXxBNTd8QTkwKVxcXFxiXCIsXG4gICAgICAgIFwiUGFsbVwiOiBcIlBhbG1Tb3VyY2V8UGFsbVwiLFxuICAgICAgICBcIlZlcnR1XCI6IFwiVmVydHV8VmVydHUuKkx0ZHxWZXJ0dS4qQXNjZW50fFZlcnR1LipBeXh0YXxWZXJ0dS4qQ29uc3RlbGxhdGlvbihGfFF1ZXN0KT98VmVydHUuKk1vbmlrYXxWZXJ0dS4qU2lnbmF0dXJlXCIsXG4gICAgICAgIFwiUGFudGVjaFwiOiBcIlBBTlRFQ0h8SU0tQTg1MFN8SU0tQTg0MFN8SU0tQTgzMEx8SU0tQTgzMEt8SU0tQTgzMFN8SU0tQTgyMEx8SU0tQTgxMEt8SU0tQTgxMFN8SU0tQTgwMFN8SU0tVDEwMEt8SU0tQTcyNUx8SU0tQTc4MEx8SU0tQTc3NUN8SU0tQTc3MEt8SU0tQTc2MFN8SU0tQTc1MEt8SU0tQTc0MFN8SU0tQTczMFN8SU0tQTcyMEx8SU0tQTcxMEt8SU0tQTY5MEx8SU0tQTY5MFN8SU0tQTY1MFN8SU0tQTYzMEt8SU0tQTYwMFN8VkVHQSBQVEwyMXxQVDAwM3xQODAxMHxBRFI5MTBMfFA2MDMwfFA2MDIwfFA5MDcwfFA0MTAwfFA5MDYwfFA1MDAwfENETTg5OTJ8VFhUODA0NXxBRFI4OTk1fElTMTFQVHxQMjAzMHxQNjAxMHxQODAwMHxQVDAwMnxJUzA2fENETTg5OTl8UDkwNTB8UFQwMDF8VFhUODA0MHxQMjAyMHxQOTAyMHxQMjAwMHxQNzA0MHxQNzAwMHxDNzkwXCIsXG4gICAgICAgIFwiRmx5XCI6IFwiSVEyMzB8SVE0NDR8SVE0NTB8SVE0NDB8SVE0NDJ8SVE0NDF8SVEyNDV8SVEyNTZ8SVEyMzZ8SVEyNTV8SVEyMzV8SVEyNDV8SVEyNzV8SVEyNDB8SVEyODV8SVEyODB8SVEyNzB8SVEyNjB8SVEyNTBcIixcbiAgICAgICAgXCJXaWtvXCI6IFwiS0lURSA0R3xISUdIV0FZfEdFVEFXQVl8U1RBSVJXQVl8REFSS1NJREV8REFSS0ZVTEx8REFSS05JR0hUfERBUktNT09OfFNMSURFfFdBWCA0R3xSQUlOQk9XfEJMT09NfFNVTlNFVHxHT0EoPyFubmEpfExFTk5ZfEJBUlJZfElHR1l8T1paWXxDSU5LIEZJVkV8Q0lOSyBQRUFYfENJTksgUEVBWCAyfENJTksgU0xJTXxDSU5LIFNMSU0gMnxDSU5LICt8Q0lOSyBLSU5HfENJTksgUEVBWHxDSU5LIFNMSU18U1VCTElNXCIsXG4gICAgICAgIFwiaU1vYmlsZVwiOiBcImktbW9iaWxlIChJUXxpLVNUWUxFfGlkZWF8WkFBfEhpdHopXCIsXG4gICAgICAgIFwiU2ltVmFsbGV5XCI6IFwiXFxcXGIoU1AtODB8WFQtOTMwfFNYLTM0MHxYVC05MzB8U1gtMzEwfFNQLTM2MHxTUDYwfFNQVC04MDB8U1AtMTIwfFNQVC04MDB8U1AtMTQwfFNQWC01fFNQWC04fFNQLTEwMHxTUFgtOHxTUFgtMTIpXFxcXGJcIixcbiAgICAgICAgXCJXb2xmZ2FuZ1wiOiBcIkFULUIyNER8QVQtQVM1MEhEfEFULUFTNDBXfEFULUFTNTVIRHxBVC1BUzQ1cTJ8QVQtQjI2RHxBVC1BUzUwUVwiLFxuICAgICAgICBcIkFsY2F0ZWxcIjogXCJBbGNhdGVsXCIsXG4gICAgICAgIFwiTmludGVuZG9cIjogXCJOaW50ZW5kbyAoM0RTfFN3aXRjaClcIixcbiAgICAgICAgXCJBbW9pXCI6IFwiQW1vaVwiLFxuICAgICAgICBcIklOUVwiOiBcIklOUVwiLFxuICAgICAgICBcIk9uZVBsdXNcIjogXCJPTkVQTFVTXCIsXG4gICAgICAgIFwiR2VuZXJpY1Bob25lXCI6IFwiVGFwYXRhbGt8UERBO3xTQUdFTXxcXFxcYm1tcFxcXFxifHBvY2tldHxcXFxcYnBzcFxcXFxifHN5bWJpYW58U21hcnRwaG9uZXxzbWFydGZvbnx0cmVvfHVwLmJyb3dzZXJ8dXAubGlua3x2b2RhZm9uZXxcXFxcYndhcFxcXFxifG5va2lhfFNlcmllczQwfFNlcmllczYwfFM2MHxTb255RXJpY3Nzb258TjkwMHxNQVVJLipXQVAuKkJyb3dzZXJcIlxuICAgIH0sXG4gICAgXCJ0YWJsZXRzXCI6IHtcbiAgICAgICAgXCJpUGFkXCI6IFwiaVBhZHxpUGFkLipNb2JpbGVcIixcbiAgICAgICAgXCJOZXh1c1RhYmxldFwiOiBcIkFuZHJvaWQuKk5leHVzW1xcXFxzXSsoN3w5fDEwKVwiLFxuICAgICAgICBcIkdvb2dsZVRhYmxldFwiOiBcIkFuZHJvaWQuKlBpeGVsIENcIixcbiAgICAgICAgXCJTYW1zdW5nVGFibGV0XCI6IFwiU0FNU1VORy4qVGFibGV0fEdhbGF4eS4qVGFifFNDLTAxQ3xHVC1QMTAwMHxHVC1QMTAwM3xHVC1QMTAxMHxHVC1QMzEwNXxHVC1QNjIxMHxHVC1QNjgwMHxHVC1QNjgxMHxHVC1QNzEwMHxHVC1QNzMwMHxHVC1QNzMxMHxHVC1QNzUwMHxHVC1QNzUxMHxTQ0gtSTgwMHxTQ0gtSTgxNXxTQ0gtSTkwNXxTR0gtSTk1N3xTR0gtSTk4N3xTR0gtVDg0OXxTR0gtVDg1OXxTR0gtVDg2OXxTUEgtUDEwMHxHVC1QMzEwMHxHVC1QMzEwOHxHVC1QMzExMHxHVC1QNTEwMHxHVC1QNTExMHxHVC1QNjIwMHxHVC1QNzMyMHxHVC1QNzUxMXxHVC1OODAwMHxHVC1QODUxMHxTR0gtSTQ5N3xTUEgtUDUwMHxTR0gtVDc3OXxTQ0gtSTcwNXxTQ0gtSTkxNXxHVC1OODAxM3xHVC1QMzExM3xHVC1QNTExM3xHVC1QODExMHxHVC1OODAxMHxHVC1OODAwNXxHVC1OODAyMHxHVC1QMTAxM3xHVC1QNjIwMXxHVC1QNzUwMXxHVC1ONTEwMHxHVC1ONTEwNXxHVC1ONTExMHxTSFYtRTE0MEt8U0hWLUUxNDBMfFNIVi1FMTQwU3xTSFYtRTE1MFN8U0hWLUUyMzBLfFNIVi1FMjMwTHxTSFYtRTIzMFN8U0hXLU0xODBLfFNIVy1NMTgwTHxTSFctTTE4MFN8U0hXLU0xODBXfFNIVy1NMzAwV3xTSFctTTMwNVd8U0hXLU0zODBLfFNIVy1NMzgwU3xTSFctTTM4MFd8U0hXLU00MzBXfFNIVy1NNDgwS3xTSFctTTQ4MFN8U0hXLU00ODBXfFNIVy1NNDg1V3xTSFctTTQ4Nld8U0hXLU01MDBXfEdULUk5MjI4fFNDSC1QNzM5fFNDSC1JOTI1fEdULUk5MjAwfEdULVA1MjAwfEdULVA1MjEwfEdULVA1MjEwWHxTTS1UMzExfFNNLVQzMTB8U00tVDMxMFh8U00tVDIxMHxTTS1UMjEwUnxTTS1UMjExfFNNLVA2MDB8U00tUDYwMXxTTS1QNjA1fFNNLVA5MDB8U00tUDkwMXxTTS1UMjE3fFNNLVQyMTdBfFNNLVQyMTdTfFNNLVA2MDAwfFNNLVQzMTAwfFNHSC1JNDY3fFhFNTAwfFNNLVQxMTB8R1QtUDUyMjB8R1QtSTkyMDBYfEdULU41MTEwWHxHVC1ONTEyMHxTTS1QOTA1fFNNLVQxMTF8U00tVDIxMDV8U00tVDMxNXxTTS1UMzIwfFNNLVQzMjBYfFNNLVQzMjF8U00tVDUyMHxTTS1UNTI1fFNNLVQ1MzBOVXxTTS1UMjMwTlV8U00tVDMzME5VfFNNLVQ5MDB8WEU1MDBUMUN8U00tUDYwNVZ8U00tUDkwNVZ8U00tVDMzN1Z8U00tVDUzN1Z8U00tVDcwN1Z8U00tVDgwN1Z8U00tUDYwMFh8U00tUDkwMFh8U00tVDIxMFh8U00tVDIzMHxTTS1UMjMwWHxTTS1UMzI1fEdULVA3NTAzfFNNLVQ1MzF8U00tVDMzMHxTTS1UNTMwfFNNLVQ3MDV8U00tVDcwNUN8U00tVDUzNXxTTS1UMzMxfFNNLVQ4MDB8U00tVDcwMHxTTS1UNTM3fFNNLVQ4MDd8U00tUDkwN0F8U00tVDMzN0F8U00tVDUzN0F8U00tVDcwN0F8U00tVDgwN0F8U00tVDIzN3xTTS1UODA3UHxTTS1QNjA3VHxTTS1UMjE3VHxTTS1UMzM3VHxTTS1UODA3VHxTTS1UMTE2TlF8U00tVDExNkJVfFNNLVA1NTB8U00tVDM1MHxTTS1UNTUwfFNNLVQ5MDAwfFNNLVA5MDAwfFNNLVQ3MDVZfFNNLVQ4MDV8R1QtUDMxMTN8U00tVDcxMHxTTS1UODEwfFNNLVQ4MTV8U00tVDM2MHxTTS1UNTMzfFNNLVQxMTN8U00tVDMzNXxTTS1UNzE1fFNNLVQ1NjB8U00tVDY3MHxTTS1UNjc3fFNNLVQzNzd8U00tVDU2N3xTTS1UMzU3VHxTTS1UNTU1fFNNLVQ1NjF8U00tVDcxM3xTTS1UNzE5fFNNLVQ4MTN8U00tVDgxOXxTTS1UNTgwfFNNLVQzNTVZP3xTTS1UMjgwfFNNLVQ4MTdBfFNNLVQ4MjB8U00tVzcwMHxTTS1QNTgwfFNNLVQ1ODd8U00tUDM1MHxTTS1QNTU1TXxTTS1QMzU1TXxTTS1UMTEzTlV8U00tVDgxNVl8U00tVDU4NXxTTS1UMjg1fFNNLVQ4MjV8U00tVzcwOHxTTS1UODM1fFNNLVQ4MzB8U00tVDgzN1Z8U00tVDcyMHxTTS1UNTEwfFNNLVQzODdWXCIsXG4gICAgICAgIFwiS2luZGxlXCI6IFwiS2luZGxlfFNpbGsuKkFjY2VsZXJhdGVkfEFuZHJvaWQuKlxcXFxiKEtGT1R8S0ZUVHxLRkpXSXxLRkpXQXxLRk9URXxLRlNPV0l8S0ZUSFdJfEtGVEhXQXxLRkFQV0l8S0ZBUFdBfFdGSldBRXxLRlNBV0F8S0ZTQVdJfEtGQVNXSXxLRkFSV0l8S0ZGT1dJfEtGR0lXSXxLRk1FV0kpXFxcXGJ8QW5kcm9pZC4qU2lsa1xcL1swLTkuXSsgbGlrZSBDaHJvbWVcXC9bMC05Ll0rICg/IU1vYmlsZSlcIixcbiAgICAgICAgXCJTdXJmYWNlVGFibGV0XCI6IFwiV2luZG93cyBOVCBbMC05Ll0rOyBBUk07LiooVGFibGV0fEFSTUJKUylcIixcbiAgICAgICAgXCJIUFRhYmxldFwiOiBcIkhQIFNsYXRlICg3fDh8MTApfEhQIEVsaXRlUGFkIDkwMHxocC10YWJsZXR8RWxpdGVCb29rLipUb3VjaHxIUCA4fFNsYXRlIDIxfEhQIFNsYXRlQm9vayAxMFwiLFxuICAgICAgICBcIkFzdXNUYWJsZXRcIjogXCJeLipQYWRGb25lKCg/IU1vYmlsZSkuKSokfFRyYW5zZm9ybWVyfFRGMTAxfFRGMTAxR3xURjMwMFR8VEYzMDBUR3xURjMwMFRMfFRGNzAwVHxURjcwMEtMfFRGNzAxVHxURjgxMEN8TUUxNzF8TUUzMDFUfE1FMzAyQ3xNRTM3MU1HfE1FMzcwVHxNRTM3Mk1HfE1FMTcyVnxNRTE3M1h8TUU0MDBDfFNsaWRlciBTTDEwMXxcXFxcYkswMEZcXFxcYnxcXFxcYkswMENcXFxcYnxcXFxcYkswMEVcXFxcYnxcXFxcYkswMExcXFxcYnxUWDIwMUxBfE1FMTc2Q3xNRTEwMkF8XFxcXGJNODBUQVxcXFxifE1FMzcyQ0x8TUU1NjBDR3xNRTM3MkNHfE1FMzAyS0x8IEswMTAgfCBLMDExIHwgSzAxNyB8IEswMUUgfE1FNTcyQ3xNRTEwM0t8TUUxNzBDfE1FMTcxQ3xcXFxcYk1FNzBDXFxcXGJ8TUU1ODFDfE1FNTgxQ0x8TUU4NTEwQ3xNRTE4MUN8UDAxWXxQTzFNQXxQMDFafFxcXFxiUDAyN1xcXFxifFxcXFxiUDAyNFxcXFxifFxcXFxiUDAwQ1xcXFxiXCIsXG4gICAgICAgIFwiQmxhY2tCZXJyeVRhYmxldFwiOiBcIlBsYXlCb29rfFJJTSBUYWJsZXRcIixcbiAgICAgICAgXCJIVEN0YWJsZXRcIjogXCJIVENfRmx5ZXJfUDUxMnxIVEMgRmx5ZXJ8SFRDIEpldHN0cmVhbXxIVEMtUDcxNWF8SFRDIEVWTyBWaWV3IDRHfFBHNDEyMDB8UEcwOTQxMFwiLFxuICAgICAgICBcIk1vdG9yb2xhVGFibGV0XCI6IFwieG9vbXxzaG9sZXN0fE1aNjE1fE1aNjA1fE1aNTA1fE1aNjAxfE1aNjAyfE1aNjAzfE1aNjA0fE1aNjA2fE1aNjA3fE1aNjA4fE1aNjA5fE1aNjE1fE1aNjE2fE1aNjE3XCIsXG4gICAgICAgIFwiTm9va1RhYmxldFwiOiBcIkFuZHJvaWQuKk5vb2t8Tm9va0NvbG9yfG5vb2sgYnJvd3NlcnxCTlJWMjAwfEJOUlYyMDBBfEJOVFYyNTB8Qk5UVjI1MEF8Qk5UVjQwMHxCTlRWNjAwfExvZ2ljUEQgWm9vbTJcIixcbiAgICAgICAgXCJBY2VyVGFibGV0XCI6IFwiQW5kcm9pZC4qOyBcXFxcYihBMTAwfEExMDF8QTExMHxBMjAwfEEyMTB8QTIxMXxBNTAwfEE1MDF8QTUxMHxBNTExfEE3MDB8QTcwMXxXNTAwfFc1MDBQfFc1MDF8VzUwMVB8VzUxMHxXNTExfFc3MDB8RzEwMHxHMTAwV3xCMS1BNzF8QjEtNzEwfEIxLTcxMXxBMS04MTB8QTEtODExfEExLTgzMClcXFxcYnxXMy04MTB8XFxcXGJBMy1BMTBcXFxcYnxcXFxcYkEzLUExMVxcXFxifFxcXFxiQTMtQTIwXFxcXGJ8XFxcXGJBMy1BMzBcIixcbiAgICAgICAgXCJUb3NoaWJhVGFibGV0XCI6IFwiQW5kcm9pZC4qKEFUMTAwfEFUMTA1fEFUMjAwfEFUMjA1fEFUMjcwfEFUMjc1fEFUMzAwfEFUMzA1fEFUMVM1fEFUNTAwfEFUNTcwfEFUNzAwfEFUODMwKXxUT1NISUJBLipGT0xJT1wiLFxuICAgICAgICBcIkxHVGFibGV0XCI6IFwiXFxcXGJMLTA2Q3xMRy1WOTA5fExHLVY5MDB8TEctVjcwMHxMRy1WNTEwfExHLVY1MDB8TEctVjQxMHxMRy1WNDAwfExHLVZLODEwXFxcXGJcIixcbiAgICAgICAgXCJGdWppdHN1VGFibGV0XCI6IFwiQW5kcm9pZC4qXFxcXGIoRi0wMUR8Ri0wMkZ8Ri0wNUV8Ri0xMER8TTUzMnxRNTcyKVxcXFxiXCIsXG4gICAgICAgIFwiUHJlc3RpZ2lvVGFibGV0XCI6IFwiUE1QMzE3MEJ8UE1QMzI3MEJ8UE1QMzQ3MEJ8UE1QNzE3MEJ8UE1QMzM3MEJ8UE1QMzU3MEN8UE1QNTg3MEN8UE1QMzY3MEJ8UE1QNTU3MEN8UE1QNTc3MER8UE1QMzk3MEJ8UE1QMzg3MEN8UE1QNTU4MEN8UE1QNTg4MER8UE1QNTc4MER8UE1QNTU4OEN8UE1QNzI4MEN8UE1QNzI4MEMzR3xQTVA3MjgwfFBNUDc4ODBEfFBNUDU1OTdEfFBNUDU1OTd8UE1QNzEwMER8UEVSMzQ2NHxQRVIzMjc0fFBFUjM1NzR8UEVSMzg4NHxQRVI1Mjc0fFBFUjU0NzR8UE1QNTA5N0NQUk98UE1QNTA5N3xQTVA3MzgwRHxQTVA1Mjk3Q3xQTVA1Mjk3Q19RVUFEfFBNUDgxMkV8UE1QODEyRTNHfFBNUDgxMkZ8UE1QODEwRXxQTVA4ODBURHxQTVQzMDE3fFBNVDMwMzd8UE1UMzA0N3xQTVQzMDU3fFBNVDcwMDh8UE1UNTg4N3xQTVQ1MDAxfFBNVDUwMDJcIixcbiAgICAgICAgXCJMZW5vdm9UYWJsZXRcIjogXCJMZW5vdm8gVEFCfElkZWEoVGFifFBhZCkoIEExfEExMHwgSzF8KXxUaGlua1BhZChbIF0rKT9UYWJsZXR8WVQzLTg1ME18WVQzLVg5MEx8WVQzLVg5MEZ8WVQzLVg5MFh8TGVub3ZvLiooUzIxMDl8UzIxMTB8UzUwMDB8UzYwMDB8SzMwMTF8QTMwMDB8QTM1MDB8QTEwMDB8QTIxMDd8QTIxMDl8QTExMDd8QTU1MDB8QTc2MDB8QjYwMDB8QjgwMDB8QjgwODApKC18KShGTHxGfEhWfEh8KXxUQi1YMTAzRnxUQi1YMzA0WHxUQi1YMzA0RnxUQi1YMzA0THxUQi1YNTA1RnxUQi1YNTA1THxUQi1YNTA1WHxUQi1YNjA1RnxUQi1YNjA1THxUQi04NzAzRnxUQi04NzAzWHxUQi04NzAzTnxUQi04NzA0TnxUQi04NzA0RnxUQi04NzA0WHxUQi04NzA0VnxUQi03MzA0RnxUQi03MzA0SXxUQi03MzA0WHxUYWIyQTctMTBGfFRhYjJBNy0yMEZ8VEIyLVgzMEx8WVQzLVg1MEx8WVQzLVg1MEZ8WVQzLVg1ME18WVQtWDcwNUZ8WVQtWDcwM0Z8WVQtWDcwM0x8WVQtWDcwNUx8WVQtWDcwNVh8VEIyLVgzMEZ8VEIyLVgzMEx8VEIyLVgzME18QTIxMDdBLUZ8QTIxMDdBLUh8VEIzLTczMEZ8VEIzLTczME18VEIzLTczMFh8VEItNzUwNEZ8VEItNzUwNFhcIixcbiAgICAgICAgXCJEZWxsVGFibGV0XCI6IFwiVmVudWUgMTF8VmVudWUgOHxWZW51ZSA3fERlbGwgU3RyZWFrIDEwfERlbGwgU3RyZWFrIDdcIixcbiAgICAgICAgXCJZYXJ2aWtUYWJsZXRcIjogXCJBbmRyb2lkLipcXFxcYihUQUIyMTB8VEFCMjExfFRBQjIyNHxUQUIyNTB8VEFCMjYwfFRBQjI2NHxUQUIzMTB8VEFCMzYwfFRBQjM2NHxUQUI0MTB8VEFCNDExfFRBQjQyMHxUQUI0MjR8VEFCNDUwfFRBQjQ2MHxUQUI0NjF8VEFCNDY0fFRBQjQ2NXxUQUI0Njd8VEFCNDY4fFRBQjA3LTEwMHxUQUIwNy0xMDF8VEFCMDctMTUwfFRBQjA3LTE1MXxUQUIwNy0xNTJ8VEFCMDctMjAwfFRBQjA3LTIwMS0zR3xUQUIwNy0yMTB8VEFCMDctMjExfFRBQjA3LTIxMnxUQUIwNy0yMTR8VEFCMDctMjIwfFRBQjA3LTQwMHxUQUIwNy00ODV8VEFCMDgtMTUwfFRBQjA4LTIwMHxUQUIwOC0yMDEtM0d8VEFCMDgtMjAxLTMwfFRBQjA5LTEwMHxUQUIwOS0yMTF8VEFCMDktNDEwfFRBQjEwLTE1MHxUQUIxMC0yMDF8VEFCMTAtMjExfFRBQjEwLTQwMHxUQUIxMC00MTB8VEFCMTMtMjAxfFRBQjI3NEVVS3xUQUIyNzVFVUt8VEFCMzc0RVVLfFRBQjQ2MkVVS3xUQUI0NzRFVUt8VEFCOS0yMDApXFxcXGJcIixcbiAgICAgICAgXCJNZWRpb25UYWJsZXRcIjogXCJBbmRyb2lkLipcXFxcYk9ZT1xcXFxifExJRkUuKihQOTIxMnxQOTUxNHxQOTUxNnxTOTUxMil8TElGRVRBQlwiLFxuICAgICAgICBcIkFybm92YVRhYmxldFwiOiBcIjk3RzR8QU4xMEcyfEFON2JHM3xBTjdmRzN8QU44RzN8QU44Y0czfEFON0czfEFOOUczfEFON2RHM3xBTjdkRzNTVHxBTjdkRzNDaGlsZFBhZHxBTjEwYkczfEFOMTBiRzNEVHxBTjlHMlwiLFxuICAgICAgICBcIkludGVuc29UYWJsZXRcIjogXCJJTk04MDAyS1B8SU5NMTAxMEZQfElOTTgwNU5EfEludGVuc28gVGFifFRBQjEwMDRcIixcbiAgICAgICAgXCJJUlVUYWJsZXRcIjogXCJNNzAycHJvXCIsXG4gICAgICAgIFwiTWVnYWZvblRhYmxldFwiOiBcIk1lZ2FGb24gVjl8XFxcXGJaVEUgVjlcXFxcYnxBbmRyb2lkLipcXFxcYk1UN0FcXFxcYlwiLFxuICAgICAgICBcIkVib2RhVGFibGV0XCI6IFwiRS1Cb2RhIChTdXByZW1lfEltcHJlc3NwZWVkfEl6enljb21tfEVzc2VudGlhbClcIixcbiAgICAgICAgXCJBbGxWaWV3VGFibGV0XCI6IFwiQWxsdmlldy4qKFZpdmF8QWxsZHJvfENpdHl8U3BlZWR8QWxsIFRWfEZyZW56eXxRdWFzYXJ8U2hpbmV8VFgxfEFYMXxBWDIpXCIsXG4gICAgICAgIFwiQXJjaG9zVGFibGV0XCI6IFwiXFxcXGIoMTAxRzl8ODBHOXxBMTAxSVQpXFxcXGJ8UWlsaXZlIDk3UnxBcmNob3M1fFxcXFxiQVJDSE9TICg3MHw3OXw4MHw5MHw5N3wxMDF8RkFNSUxZUEFEfCkoYnxjfCkoRzEwfCBDb2JhbHR8IFRJVEFOSVVNKEhEfCl8IFhlbm9ufCBOZW9ufFhTS3wgMnwgWFMgMnwgUExBVElOVU18IENBUkJPTnxHQU1FUEFEKVxcXFxiXCIsXG4gICAgICAgIFwiQWlub2xUYWJsZXRcIjogXCJOT1ZPN3xOT1ZPOHxOT1ZPMTB8Tm92bzdBdXJvcmF8Tm92bzdCYXNpY3xOT1ZPN1BBTEFESU58bm92bzktU3BhcmtcIixcbiAgICAgICAgXCJOb2tpYUx1bWlhVGFibGV0XCI6IFwiTHVtaWEgMjUyMFwiLFxuICAgICAgICBcIlNvbnlUYWJsZXRcIjogXCJTb255LipUYWJsZXR8WHBlcmlhIFRhYmxldHxTb255IFRhYmxldCBTfFNPLTAzRXxTR1BUMTJ8U0dQVDEzfFNHUFQxMTR8U0dQVDEyMXxTR1BUMTIyfFNHUFQxMjN8U0dQVDExMXxTR1BUMTEyfFNHUFQxMTN8U0dQVDEzMXxTR1BUMTMyfFNHUFQxMzN8U0dQVDIxMXxTR1BUMjEyfFNHUFQyMTN8U0dQMzExfFNHUDMxMnxTR1AzMjF8RUJSRDExMDF8RUJSRDExMDJ8RUJSRDEyMDF8U0dQMzUxfFNHUDM0MXxTR1A1MTF8U0dQNTEyfFNHUDUyMXxTR1A1NDF8U0dQNTUxfFNHUDYyMXxTR1A2NDF8U0dQNjEyfFNPVDMxfFNHUDc3MXxTR1A2MTF8U0dQNjEyfFNHUDcxMlwiLFxuICAgICAgICBcIlBoaWxpcHNUYWJsZXRcIjogXCJcXFxcYihQSTIwMTB8UEkzMDAwfFBJMzEwMHxQSTMxMDV8UEkzMTEwfFBJMzIwNXxQSTMyMTB8UEkzOTAwfFBJNDAxMHxQSTcwMDB8UEk3MTAwKVxcXFxiXCIsXG4gICAgICAgIFwiQ3ViZVRhYmxldFwiOiBcIkFuZHJvaWQuKihLOEdUfFU5R1R8VTEwR1R8VTE2R1R8VTE3R1R8VTE4R1R8VTE5R1R8VTIwR1R8VTIzR1R8VTMwR1QpfENVQkUgVThHVFwiLFxuICAgICAgICBcIkNvYnlUYWJsZXRcIjogXCJNSUQxMDQyfE1JRDEwNDV8TUlEMTEyNXxNSUQxMTI2fE1JRDcwMTJ8TUlENzAxNHxNSUQ3MDE1fE1JRDcwMzR8TUlENzAzNXxNSUQ3MDM2fE1JRDcwNDJ8TUlENzA0OHxNSUQ3MTI3fE1JRDgwNDJ8TUlEODA0OHxNSUQ4MTI3fE1JRDkwNDJ8TUlEOTc0MHxNSUQ5NzQyfE1JRDcwMjJ8TUlENzAxMFwiLFxuICAgICAgICBcIk1JRFRhYmxldFwiOiBcIk05NzAxfE05MDAwfE05MTAwfE04MDZ8TTEwNTJ8TTgwNnxUNzAzfE1JRDcwMXxNSUQ3MTN8TUlENzEwfE1JRDcyN3xNSUQ3NjB8TUlEODMwfE1JRDcyOHxNSUQ5MzN8TUlEMTI1fE1JRDgxMHxNSUQ3MzJ8TUlEMTIwfE1JRDkzMHxNSUQ4MDB8TUlENzMxfE1JRDkwMHxNSUQxMDB8TUlEODIwfE1JRDczNXxNSUQ5ODB8TUlEMTMwfE1JRDgzM3xNSUQ3Mzd8TUlEOTYwfE1JRDEzNXxNSUQ4NjB8TUlENzM2fE1JRDE0MHxNSUQ5MzB8TUlEODM1fE1JRDczM3xNSUQ0WDEwXCIsXG4gICAgICAgIFwiTVNJVGFibGV0XCI6IFwiTVNJIFxcXFxiKFByaW1vIDczS3xQcmltbyA3M0x8UHJpbW8gODFMfFByaW1vIDc3fFByaW1vIDkzfFByaW1vIDc1fFByaW1vIDc2fFByaW1vIDczfFByaW1vIDgxfFByaW1vIDkxfFByaW1vIDkwfEVuam95IDcxfEVuam95IDd8RW5qb3kgMTApXFxcXGJcIixcbiAgICAgICAgXCJTTWlUVGFibGV0XCI6IFwiQW5kcm9pZC4qKFxcXFxiTUlEXFxcXGJ8TUlELTU2MHxNVFYtVDEyMDB8TVRWLVBORDUzMXxNVFYtUDExMDF8TVRWLVBORDUzMClcIixcbiAgICAgICAgXCJSb2NrQ2hpcFRhYmxldFwiOiBcIkFuZHJvaWQuKihSSzI4MTh8UksyODA4QXxSSzI5MTh8UkszMDY2KXxSSzI3Mzh8UksyODA4QVwiLFxuICAgICAgICBcIkZseVRhYmxldFwiOiBcIklRMzEwfEZseSBWaXNpb25cIixcbiAgICAgICAgXCJicVRhYmxldFwiOiBcIkFuZHJvaWQuKihicSk/LipcXFxcYihFbGNhbm98Q3VyaWV8RWRpc29ufE1heHdlbGx8S2VwbGVyfFBhc2NhbHxUZXNsYXxIeXBhdGlhfFBsYXRvbnxOZXd0b258TGl2aW5nc3RvbmV8Q2VydmFudGVzfEF2YW50fEFxdWFyaXMgKFtFfE1dMTB8TTgpKVxcXFxifE1heHdlbGwuKkxpdGV8TWF4d2VsbC4qUGx1c1wiLFxuICAgICAgICBcIkh1YXdlaVRhYmxldFwiOiBcIk1lZGlhUGFkfE1lZGlhUGFkIDcgWW91dGh8SURFT1MgUzd8UzctMjAxY3xTNy0yMDJ1fFM3LTEwMXxTNy0xMDN8UzctMTA0fFM3LTEwNXxTNy0xMDZ8UzctMjAxfFM3LVNsaW18TTItQTAxTHxCQUgtTDA5fEJBSC1XMDl8QUdTLUwwOXxDTVItQUwxOVwiLFxuICAgICAgICBcIk5lY1RhYmxldFwiOiBcIlxcXFxiTi0wNkR8XFxcXGJOLTA4RFwiLFxuICAgICAgICBcIlBhbnRlY2hUYWJsZXRcIjogXCJQYW50ZWNoLipQNDEwMFwiLFxuICAgICAgICBcIkJyb25jaG9UYWJsZXRcIjogXCJCcm9uY2hvLiooTjcwMXxONzA4fE44MDJ8YTcxMClcIixcbiAgICAgICAgXCJWZXJzdXNUYWJsZXRcIjogXCJUT1VDSFBBRC4qWzc4OTEwXXxcXFxcYlRPVUNIVEFCXFxcXGJcIixcbiAgICAgICAgXCJaeW5jVGFibGV0XCI6IFwiejEwMDB8Wjk5IDJHfHo5MzB8ejk5MHx6OTA5fFo5MTl8ejkwMFwiLFxuICAgICAgICBcIlBvc2l0aXZvVGFibGV0XCI6IFwiVEIwN1NUQXxUQjEwU1RBfFRCMDdGVEF8VEIxMEZUQVwiLFxuICAgICAgICBcIk5hYmlUYWJsZXRcIjogXCJBbmRyb2lkLipcXFxcYk5hYmlcIixcbiAgICAgICAgXCJLb2JvVGFibGV0XCI6IFwiS29ibyBUb3VjaHxcXFxcYkswODBcXFxcYnxcXFxcYlZveFxcXFxiIEJ1aWxkfFxcXFxiQXJjXFxcXGIgQnVpbGRcIixcbiAgICAgICAgXCJEYW5ld1RhYmxldFwiOiBcIkRTbGlkZS4qXFxcXGIoNzAwfDcwMVJ8NzAyfDcwM1J8NzA0fDgwMnw5NzB8OTcxfDk3Mnw5NzN8OTc0fDEwMTB8MTAxMilcXFxcYlwiLFxuICAgICAgICBcIlRleGV0VGFibGV0XCI6IFwiTmF2aVBhZHxUQi03NzJBfFRNLTcwNDV8VE0tNzA1NXxUTS05NzUwfFRNLTcwMTZ8VE0tNzAyNHxUTS03MDI2fFRNLTcwNDF8VE0tNzA0M3xUTS03MDQ3fFRNLTgwNDF8VE0tOTc0MXxUTS05NzQ3fFRNLTk3NDh8VE0tOTc1MXxUTS03MDIyfFRNLTcwMjF8VE0tNzAyMHxUTS03MDExfFRNLTcwMTB8VE0tNzAyM3xUTS03MDI1fFRNLTcwMzdXfFRNLTcwMzhXfFRNLTcwMjdXfFRNLTk3MjB8VE0tOTcyNXxUTS05NzM3V3xUTS0xMDIwfFRNLTk3MzhXfFRNLTk3NDB8VE0tOTc0M1d8VEItODA3QXxUQi03NzFBfFRCLTcyN0F8VEItNzI1QXxUQi03MTlBfFRCLTgyM0F8VEItODA1QXxUQi03MjNBfFRCLTcxNUF8VEItNzA3QXxUQi03MDVBfFRCLTcwOUF8VEItNzExQXxUQi04OTBIRHxUQi04ODBIRHxUQi03OTBIRHxUQi03ODBIRHxUQi03NzBIRHxUQi03MjFIRHxUQi03MTBIRHxUQi00MzRIRHxUQi04NjBIRHxUQi04NDBIRHxUQi03NjBIRHxUQi03NTBIRHxUQi03NDBIRHxUQi03MzBIRHxUQi03MjJIRHxUQi03MjBIRHxUQi03MDBIRHxUQi01MDBIRHxUQi00NzBIRHxUQi00MzFIRHxUQi00MzBIRHxUQi01MDZ8VEItNTA0fFRCLTQ0NnxUQi00MzZ8VEItNDE2fFRCLTE0NlNFfFRCLTEyNlNFXCIsXG4gICAgICAgIFwiUGxheXN0YXRpb25UYWJsZXRcIjogXCJQbGF5c3RhdGlvbi4qKFBvcnRhYmxlfFZpdGEpXCIsXG4gICAgICAgIFwiVHJla3N0b3JUYWJsZXRcIjogXCJTVDEwNDE2LTF8VlQxMDQxNi0xfFNUNzA0MDgtMXxTVDcwMnh4LTF8U1Q3MDJ4eC0yfFNUODAyMDh8U1Q5NzIxNnxTVDcwMTA0LTJ8VlQxMDQxNi0yfFNUMTAyMTYtMkF8U3VyZlRhYlwiLFxuICAgICAgICBcIlB5bGVBdWRpb1RhYmxldFwiOiBcIlxcXFxiKFBUQkwxMENFVXxQVEJMMTBDfFBUQkw3MkJDfFBUQkw3MkJDRVV8UFRCTDdDRVV8UFRCTDdDfFBUQkw5MkJDfFBUQkw5MkJDRVV8UFRCTDlDRVV8UFRCTDlDVUt8UFRCTDlDKVxcXFxiXCIsXG4gICAgICAgIFwiQWR2YW5UYWJsZXRcIjogXCJBbmRyb2lkLiogXFxcXGIoRTNBfFQzWHxUNUN8VDVCfFQzRXxUM0N8VDNCfFQxSnxUMUZ8VDJBfFQxSHxUMWl8RTFDfFQxLUV8VDUtQXxUNHxFMS1CfFQyQ2l8VDEtQnxUMS1EfE8xLUF8RTEtQXxUMS1BfFQzQXxUNGkpXFxcXGIgXCIsXG4gICAgICAgIFwiRGFueVRlY2hUYWJsZXRcIjogXCJHZW5pdXMgVGFiIEczfEdlbml1cyBUYWIgUzJ8R2VuaXVzIFRhYiBRM3xHZW5pdXMgVGFiIEc0fEdlbml1cyBUYWIgUTR8R2VuaXVzIFRhYiBHLUlJfEdlbml1cyBUQUIgR0lJfEdlbml1cyBUQUIgR0lJSXxHZW5pdXMgVGFiIFMxXCIsXG4gICAgICAgIFwiR2FsYXBhZFRhYmxldFwiOiBcIkFuZHJvaWQuKlxcXFxiRzFcXFxcYig/IVxcXFwpKVwiLFxuICAgICAgICBcIk1pY3JvbWF4VGFibGV0XCI6IFwiRnVuYm9va3xNaWNyb21heC4qXFxcXGIoUDI1MHxQNTYwfFAzNjB8UDM2MnxQNjAwfFAzMDB8UDM1MHxQNTAwfFAyNzUpXFxcXGJcIixcbiAgICAgICAgXCJLYXJib25uVGFibGV0XCI6IFwiQW5kcm9pZC4qXFxcXGIoQTM5fEEzN3xBMzR8U1Q4fFNUMTB8U1Q3fFNtYXJ0IFRhYjN8U21hcnQgVGFiMilcXFxcYlwiLFxuICAgICAgICBcIkFsbEZpbmVUYWJsZXRcIjogXCJGaW5lNyBHZW5pdXN8RmluZTcgU2hpbmV8RmluZTcgQWlyfEZpbmU4IFN0eWxlfEZpbmU5IE1vcmV8RmluZTEwIEpveXxGaW5lMTEgV2lkZVwiLFxuICAgICAgICBcIlBST1NDQU5UYWJsZXRcIjogXCJcXFxcYihQRU02M3xQTFQxMDIzR3xQTFQxMDQxfFBMVDEwNDR8UExUMTA0NEd8UExUMTA5MXxQTFQ0MzExfFBMVDQzMTFQTHxQTFQ0MzE1fFBMVDcwMzB8UExUNzAzM3xQTFQ3MDMzRHxQTFQ3MDM1fFBMVDcwMzVEfFBMVDcwNDRLfFBMVDcwNDVLfFBMVDcwNDVLQnxQTFQ3MDcxS0d8UExUNzA3MnxQTFQ3MjIzR3xQTFQ3MjI1R3xQTFQ3Nzc3R3xQTFQ3ODEwS3xQTFQ3ODQ5R3xQTFQ3ODUxR3xQTFQ3ODUyR3xQTFQ4MDE1fFBMVDgwMzF8UExUODAzNHxQTFQ4MDM2fFBMVDgwODBLfFBMVDgwODJ8UExUODA4OHxQTFQ4MjIzR3xQTFQ4MjM0R3xQTFQ4MjM1R3xQTFQ4ODE2S3xQTFQ5MDExfFBMVDkwNDVLfFBMVDkyMzNHfFBMVDk3MzV8UExUOTc2MEd8UExUOTc3MEcpXFxcXGJcIixcbiAgICAgICAgXCJZT05FU1RhYmxldFwiOiBcIkJRMTA3OHxCQzEwMDN8QkMxMDc3fFJLOTcwMnxCQzk3MzB8QkM5MDAxfElUOTAwMXxCQzcwMDh8QkM3MDEwfEJDNzA4fEJDNzI4fEJDNzAxMnxCQzcwMzB8QkM3MDI3fEJDNzAyNlwiLFxuICAgICAgICBcIkNoYW5nSmlhVGFibGV0XCI6IFwiVFBDNzEwMnxUUEM3MTAzfFRQQzcxMDV8VFBDNzEwNnxUUEM3MTA3fFRQQzcyMDF8VFBDNzIwM3xUUEM3MjA1fFRQQzcyMTB8VFBDNzcwOHxUUEM3NzA5fFRQQzc3MTJ8VFBDNzExMHxUUEM4MTAxfFRQQzgxMDN8VFBDODEwNXxUUEM4MTA2fFRQQzgyMDN8VFBDODIwNXxUUEM4NTAzfFRQQzkxMDZ8VFBDOTcwMXxUUEM5NzEwMXxUUEM5NzEwM3xUUEM5NzEwNXxUUEM5NzEwNnxUUEM5NzExMXxUUEM5NzExM3xUUEM5NzIwM3xUUEM5NzYwM3xUUEM5NzgwOXxUUEM5NzIwNXxUUEMxMDEwMXxUUEMxMDEwM3xUUEMxMDEwNnxUUEMxMDExMXxUUEMxMDIwM3xUUEMxMDIwNXxUUEMxMDUwM1wiLFxuICAgICAgICBcIkdVVGFibGV0XCI6IFwiVFgtQTEzMDF8VFgtTTkwMDJ8UTcwMnxrZjAyNlwiLFxuICAgICAgICBcIlBvaW50T2ZWaWV3VGFibGV0XCI6IFwiVEFCLVA1MDZ8VEFCLW5hdmktNy0zRy1NfFRBQi1QNTE3fFRBQi1QLTUyN3xUQUItUDcwMXxUQUItUDcwM3xUQUItUDcyMXxUQUItUDczMU58VEFCLVA3NDF8VEFCLVA4MjV8VEFCLVA5MDV8VEFCLVA5MjV8VEFCLVBSOTQ1fFRBQi1QTDEwMTV8VEFCLVAxMDI1fFRBQi1QSTEwNDV8VEFCLVAxMzI1fFRBQi1QUk9UQUJbMC05XSt8VEFCLVBST1RBQjI1fFRBQi1QUk9UQUIyNnxUQUItUFJPVEFCMjd8VEFCLVBST1RBQjI2WEx8VEFCLVBST1RBQjItSVBTOXxUQUItUFJPVEFCMzAtSVBTOXxUQUItUFJPVEFCMjVYWEx8VEFCLVBST1RBQjI2LUlQUzEwfFRBQi1QUk9UQUIzMC1JUFMxMFwiLFxuICAgICAgICBcIk92ZXJtYXhUYWJsZXRcIjogXCJPVi0oU3RlZWxDb3JlfE5ld0Jhc2V8QmFzZWNvcmV8QmFzZW9uZXxFeGVsbGVufFF1YXR0b3J8RWR1VGFifFNvbHV0aW9ufEFDVElPTnxCYXNpY1RhYnxUZWRkeVRhYnxNYWdpY1RhYnxTdHJlYW18VEItMDh8VEItMDkpfFF1YWxjb3JlIDEwMjdcIixcbiAgICAgICAgXCJIQ0xUYWJsZXRcIjogXCJIQ0wuKlRhYmxldHxDb25uZWN0LTNHLTIuMHxDb25uZWN0LTJHLTIuMHxNRSBUYWJsZXQgVTF8TUUgVGFibGV0IFUyfE1FIFRhYmxldCBHMXxNRSBUYWJsZXQgWDF8TUUgVGFibGV0IFkyfE1FIFRhYmxldCBTeW5jXCIsXG4gICAgICAgIFwiRFBTVGFibGV0XCI6IFwiRFBTIERyZWFtIDl8RFBTIER1YWwgN1wiLFxuICAgICAgICBcIlZpc3R1cmVUYWJsZXRcIjogXCJWOTcgSER8aTc1IDNHfFZpc3R1cmUgVjQoIEhEKT98VmlzdHVyZSBWNSggSEQpP3xWaXN0dXJlIFYxMFwiLFxuICAgICAgICBcIkNyZXN0YVRhYmxldFwiOiBcIkNUUCgtKT84MTB8Q1RQKC0pPzgxOHxDVFAoLSk/ODI4fENUUCgtKT84Mzh8Q1RQKC0pPzg4OHxDVFAoLSk/OTc4fENUUCgtKT85ODB8Q1RQKC0pPzk4N3xDVFAoLSk/OTg4fENUUCgtKT85ODlcIixcbiAgICAgICAgXCJNZWRpYXRla1RhYmxldFwiOiBcIlxcXFxiTVQ4MTI1fE1UODM4OXxNVDgxMzV8TVQ4Mzc3XFxcXGJcIixcbiAgICAgICAgXCJDb25jb3JkZVRhYmxldFwiOiBcIkNvbmNvcmRlKFsgXSspP1RhYnxDb25Db3JkZSBSZWFkTWFuXCIsXG4gICAgICAgIFwiR29DbGV2ZXJUYWJsZXRcIjogXCJHT0NMRVZFUiBUQUJ8QTdHT0NMRVZFUnxNMTA0MnxNNzg0MXxNNzQyfFIxMDQyQkt8UjEwNDF8VEFCIEE5NzV8VEFCIEE3ODQyfFRBQiBBNzQxfFRBQiBBNzQxTHxUQUIgTTcyM0d8VEFCIE03MjF8VEFCIEExMDIxfFRBQiBJOTIxfFRBQiBSNzIxfFRBQiBJNzIwfFRBQiBUNzZ8VEFCIFI3MHxUQUIgUjc2LjJ8VEFCIFIxMDZ8VEFCIFI4My4yfFRBQiBNODEzR3xUQUIgSTcyMXxHQ1RBNzIyfFRBQiBJNzB8VEFCIEk3MXxUQUIgUzczfFRBQiBSNzN8VEFCIFI3NHxUQUIgUjkzfFRBQiBSNzV8VEFCIFI3Ni4xfFRBQiBBNzN8VEFCIEE5M3xUQUIgQTkzLjJ8VEFCIFQ3MnxUQUIgUjgzfFRBQiBSOTc0fFRBQiBSOTczfFRBQiBBMTAxfFRBQiBBMTAzfFRBQiBBMTA0fFRBQiBBMTA0LjJ8UjEwNUJLfE03MTNHfEE5NzJCS3xUQUIgQTk3MXxUQUIgUjk3NC4yfFRBQiBSMTA0fFRBQiBSODMuM3xUQUIgQTEwNDJcIixcbiAgICAgICAgXCJNb2RlY29tVGFibGV0XCI6IFwiRnJlZVRBQiA5MDAwfEZyZWVUQUIgNy40fEZyZWVUQUIgNzAwNHxGcmVlVEFCIDc4MDB8RnJlZVRBQiAyMDk2fEZyZWVUQUIgNy41fEZyZWVUQUIgMTAxNHxGcmVlVEFCIDEwMDEgfEZyZWVUQUIgODAwMXxGcmVlVEFCIDk3MDZ8RnJlZVRBQiA5NzAyfEZyZWVUQUIgNzAwM3xGcmVlVEFCIDcwMDJ8RnJlZVRBQiAxMDAyfEZyZWVUQUIgNzgwMXxGcmVlVEFCIDEzMzF8RnJlZVRBQiAxMDA0fEZyZWVUQUIgODAwMnxGcmVlVEFCIDgwMTR8RnJlZVRBQiA5NzA0fEZyZWVUQUIgMTAwM1wiLFxuICAgICAgICBcIlZvbmlub1RhYmxldFwiOiBcIlxcXFxiKEFyZ3VzWyBfXT9TfERpYW1vbmRbIF9dPzc5SER8RW1lcmFsZFsgX10/NzhFfEx1bmFbIF9dPzcwQ3xPbnl4WyBfXT9TfE9ueXhbIF9dP1p8T3JpblsgX10/SER8T3JpblsgX10/U3xPdGlzWyBfXT9TfFNwZWVkU3RhclsgX10/U3xNYWduZXRbIF9dP005fFByaW11c1sgX10/OTRbIF9dPzNHfFByaW11c1sgX10/OTRIRHxQcmltdXNbIF9dP1FTfEFuZHJvaWQuKlxcXFxiUThcXFxcYnxTaXJpdXNbIF9dP0VWT1sgX10/UVN8U2lyaXVzWyBfXT9RU3xTcGlyaXRbIF9dP1MpXFxcXGJcIixcbiAgICAgICAgXCJFQ1NUYWJsZXRcIjogXCJWMDdPVDJ8VE0xMDVBfFMxME9UMXxUUjEwQ1MxXCIsXG4gICAgICAgIFwiU3RvcmV4VGFibGV0XCI6IFwiZVplZVtfJ10/KFRhYnxHbylbMC05XSt8VGFiTEM3fExvb25leSBUdW5lcyBUYWJcIixcbiAgICAgICAgXCJWb2RhZm9uZVRhYmxldFwiOiBcIlNtYXJ0VGFiKFsgXSspP1swLTldK3xTbWFydFRhYklJMTB8U21hcnRUYWJJSTd8VkYtMTQ5N3xWRkQgMTQwMFwiLFxuICAgICAgICBcIkVzc2VudGllbEJUYWJsZXRcIjogXCJTbWFydFsgJ10/VEFCWyBdKz9bMC05XSt8RmFtaWx5WyAnXT9UQUIyXCIsXG4gICAgICAgIFwiUm9zc01vb3JUYWJsZXRcIjogXCJSTS03OTB8Uk0tOTk3fFJNRC04NzhHfFJNRC05NzRSfFJNVC03MDVBfFJNVC03MDF8Uk1FLTYwMXxSTVQtNTAxfFJNVC03MTFcIixcbiAgICAgICAgXCJpTW9iaWxlVGFibGV0XCI6IFwiaS1tb2JpbGUgaS1ub3RlXCIsXG4gICAgICAgIFwiVG9saW5vVGFibGV0XCI6IFwidG9saW5vIHRhYiBbMC05Ll0rfHRvbGlubyBzaGluZVwiLFxuICAgICAgICBcIkF1ZGlvU29uaWNUYWJsZXRcIjogXCJcXFxcYkMtMjJRfFQ3LVFDfFQtMTdCfFQtMTdQXFxcXGJcIixcbiAgICAgICAgXCJBTVBFVGFibGV0XCI6IFwiQW5kcm9pZC4qIEE3OCBcIixcbiAgICAgICAgXCJTa2tUYWJsZXRcIjogXCJBbmRyb2lkLiogKFNLWVBBRHxQSE9FTklYfENZQ0xPUFMpXCIsXG4gICAgICAgIFwiVGVjbm9UYWJsZXRcIjogXCJURUNOTyBQOXxURUNOTyBEUDhEXCIsXG4gICAgICAgIFwiSlhEVGFibGV0XCI6IFwiQW5kcm9pZC4qIFxcXFxiKEYzMDAwfEEzMzAwfEpYRDUwMDB8SlhEMzAwMHxKWEQyMDAwfEpYRDMwMEJ8SlhEMzAwfFM1ODAwfFM3ODAwfFM2MDJifFM1MTEwYnxTNzMwMHxTNTMwMHxTNjAyfFM2MDN8UzUxMDB8UzUxMTB8UzYwMXxTNzEwMGF8UDMwMDBGfFAzMDAwc3xQMTAxfFAyMDBzfFAxMDAwbXxQMjAwbXxQOTEwMHxQMTAwMHN8UzY2MDBifFM5MDh8UDEwMDB8UDMwMHxTMTh8UzY2MDB8UzkxMDApXFxcXGJcIixcbiAgICAgICAgXCJpSm95VGFibGV0XCI6IFwiVGFibGV0IChTcGlyaXQgN3xFc3NlbnRpYXxHYWxhdGVhfEZ1c2lvbnxPbml4IDd8TGFuZGF8VGl0YW58U2Nvb2J5fERlb3h8U3RlbGxhfFRoZW1pc3xBcmdvbnxVbmlxdWUgN3xTeWdudXN8SGV4ZW58RmluaXR5IDd8Q3JlYW18Q3JlYW0gWDJ8SmFkZXxOZW9uIDd8TmVyb24gN3xLYW5keXxTY2FwZXxTYXBoeXIgN3xSZWJlbHxCaW94fFJlYmVsfFJlYmVsIDhHQnxNeXN0fERyYWNvIDd8TXlzdHxUYWI3LTAwNHxNeXN0fFRhZGVvIEpvbmVzfFRhYmxldCBCb2luZ3xBcnJvd3xEcmFjbyBEdWFsIENhbXxBdXJpeHxNaW50fEFtaXR5fFJldm9sdXRpb258RmluaXR5IDl8TmVvbiA5fFQ5d3xBbWl0eSA0R0IgRHVhbCBDYW18U3RvbmUgNEdCfFN0b25lIDhHQnxBbmRyb21lZGF8U2lsa2VufFgyfEFuZHJvbWVkYSBJSXxIYWxsZXl8RmxhbWV8U2FwaHlyIDksN3xUb3VjaCA4fFBsYW5ldHxUcml0b258VW5pcXVlIDEwfEhleGVuIDEwfE1lbXBoaXMgNEdCfE1lbXBoaXMgOEdCfE9uaXggMTApXCIsXG4gICAgICAgIFwiRlgyVGFibGV0XCI6IFwiRlgyIFBBRDd8RlgyIFBBRDEwXCIsXG4gICAgICAgIFwiWG9yb1RhYmxldFwiOiBcIktpZHNQQUQgNzAxfFBBRFsgXT83MTJ8UEFEWyBdPzcxNHxQQURbIF0/NzE2fFBBRFsgXT83MTd8UEFEWyBdPzcxOHxQQURbIF0/NzIwfFBBRFsgXT83MjF8UEFEWyBdPzcyMnxQQURbIF0/NzkwfFBBRFsgXT83OTJ8UEFEWyBdPzkwMHxQQURbIF0/OTcxNUR8UEFEWyBdPzk3MTZEUnxQQURbIF0/OTcxOERSfFBBRFsgXT85NzE5UVJ8UEFEWyBdPzk3MjBRUnxUZWxlUEFEMTAzMHxUZWxlcGFkMTAzMnxUZWxlUEFENzMwfFRlbGVQQUQ3MzF8VGVsZVBBRDczMnxUZWxlUEFENzM1UXxUZWxlUEFEODMwfFRlbGVQQUQ5NzMwfFRlbGVQQUQ3OTV8TWVnYVBBRCAxMzMxfE1lZ2FQQUQgMTg1MXxNZWdhUEFEIDIxNTFcIixcbiAgICAgICAgXCJWaWV3c29uaWNUYWJsZXRcIjogXCJWaWV3UGFkIDEwcGl8Vmlld1BhZCAxMGV8Vmlld1BhZCAxMHN8Vmlld1BhZCBFNzJ8Vmlld1BhZDd8Vmlld1BhZCBFMTAwfFZpZXdQYWQgN2V8Vmlld1NvbmljIFZCNzMzfFZCMTAwYVwiLFxuICAgICAgICBcIlZlcml6b25UYWJsZXRcIjogXCJRVEFRWjN8UVRBSVI3fFFUQVFUWjN8UVRBU1VOMXxRVEFTVU4yfFFUQVhJQTFcIixcbiAgICAgICAgXCJPZHlzVGFibGV0XCI6IFwiTE9PWHxYRU5PMTB8T0RZU1sgLV0oU3BhY2V8RVZPfFhwcmVzc3xOT09OKXxcXFxcYlhFTElPXFxcXGJ8WGVsaW8xMFByb3xYRUxJTzdQSE9ORVRBQnxYRUxJTzEwRVhUUkVNRXxYRUxJT1BUMnxORU9fUVVBRDEwXCIsXG4gICAgICAgIFwiQ2FwdGl2YVRhYmxldFwiOiBcIkNBUFRJVkEgUEFEXCIsXG4gICAgICAgIFwiSWNvbmJpdFRhYmxldFwiOiBcIk5ldFRBQnxOVC0zNzAyfE5ULTM3MDJTfE5ULTM3MDJTfE5ULTM2MDNQfE5ULTM2MDNQfE5ULTA3MDRTfE5ULTA3MDRTfE5ULTM4MDVDfE5ULTM4MDVDfE5ULTA4MDZDfE5ULTA4MDZDfE5ULTA5MDlUfE5ULTA5MDlUfE5ULTA5MDdTfE5ULTA5MDdTfE5ULTA5MDJTfE5ULTA5MDJTXCIsXG4gICAgICAgIFwiVGVjbGFzdFRhYmxldFwiOiBcIlQ5OCA0R3xcXFxcYlA4MFxcXFxifFxcXFxiWDkwSERcXFxcYnxYOTggQWlyfFg5OCBBaXIgM0d8XFxcXGJYODlcXFxcYnxQODAgM0d8XFxcXGJYODBoXFxcXGJ8UDk4IEFpcnxcXFxcYlg4OUhEXFxcXGJ8UDk4IDNHfFxcXFxiUDkwSERcXFxcYnxQODkgM0d8WDk4IDNHfFxcXFxiUDcwaFxcXFxifFA3OUhEIDNHfEcxOGQgM0d8XFxcXGJQNzlIRFxcXFxifFxcXFxiUDg5c1xcXFxifFxcXFxiQTg4XFxcXGJ8XFxcXGJQMTBIRFxcXFxifFxcXFxiUDE5SERcXFxcYnxHMTggM0d8XFxcXGJQNzhIRFxcXFxifFxcXFxiQTc4XFxcXGJ8XFxcXGJQNzVcXFxcYnxHMTdzIDNHfEcxN2ggM0d8XFxcXGJQODV0XFxcXGJ8XFxcXGJQOTBcXFxcYnxcXFxcYlAxMVxcXFxifFxcXFxiUDk4dFxcXFxifFxcXFxiUDk4SERcXFxcYnxcXFxcYkcxOGRcXFxcYnxcXFxcYlA4NXNcXFxcYnxcXFxcYlAxMUhEXFxcXGJ8XFxcXGJQODhzXFxcXGJ8XFxcXGJBODBIRFxcXFxifFxcXFxiQTgwc2VcXFxcYnxcXFxcYkExMGhcXFxcYnxcXFxcYlA4OVxcXFxifFxcXFxiUDc4c1xcXFxifFxcXFxiRzE4XFxcXGJ8XFxcXGJQODVcXFxcYnxcXFxcYkE3MGhcXFxcYnxcXFxcYkE3MFxcXFxifFxcXFxiRzE3XFxcXGJ8XFxcXGJQMThcXFxcYnxcXFxcYkE4MHNcXFxcYnxcXFxcYkExMXNcXFxcYnxcXFxcYlA4OEhEXFxcXGJ8XFxcXGJBODBoXFxcXGJ8XFxcXGJQNzZzXFxcXGJ8XFxcXGJQNzZoXFxcXGJ8XFxcXGJQOThcXFxcYnxcXFxcYkExMEhEXFxcXGJ8XFxcXGJQNzhcXFxcYnxcXFxcYlA4OFxcXFxifFxcXFxiQTExXFxcXGJ8XFxcXGJBMTB0XFxcXGJ8XFxcXGJQNzZhXFxcXGJ8XFxcXGJQNzZ0XFxcXGJ8XFxcXGJQNzZlXFxcXGJ8XFxcXGJQODVIRFxcXFxifFxcXFxiUDg1YVxcXFxifFxcXFxiUDg2XFxcXGJ8XFxcXGJQNzVIRFxcXFxifFxcXFxiUDc2dlxcXFxifFxcXFxiQTEyXFxcXGJ8XFxcXGJQNzVhXFxcXGJ8XFxcXGJBMTVcXFxcYnxcXFxcYlA3NlRpXFxcXGJ8XFxcXGJQODFIRFxcXFxifFxcXFxiQTEwXFxcXGJ8XFxcXGJUNzYwVkVcXFxcYnxcXFxcYlQ3MjBIRFxcXFxifFxcXFxiUDc2XFxcXGJ8XFxcXGJQNzNcXFxcYnxcXFxcYlA3MVxcXFxifFxcXFxiUDcyXFxcXGJ8XFxcXGJUNzIwU0VcXFxcYnxcXFxcYkM1MjBUaVxcXFxifFxcXFxiVDc2MFxcXFxifFxcXFxiVDcyMFZFXFxcXGJ8VDcyMC0zR0V8VDcyMC1XaUZpXCIsXG4gICAgICAgIFwiT25kYVRhYmxldFwiOiBcIlxcXFxiKFY5NzVpfFZpMzB8Vlg1MzB8VjcwMXxWaTYwfFY3MDFzfFZpNTB8VjgwMXN8VjcxOXxWeDYxMHd8Vlg2MTBXfFY4MTlpfFZpMTB8Vlg1ODBXfFZpMTB8VjcxMXN8VjgxM3xWODExfFY4MjB3fFY4MjB8VmkyMHxWNzExfFZJMzBXfFY3MTJ8Vjg5MXd8Vjk3MnxWODE5d3xWODIwd3xWaTYwfFY4MjB3fFY3MTF8VjgxM3N8VjgwMXxWODE5fFY5NzVzfFY4MDF8VjgxOXxWODE5fFY4MTh8VjgxMXxWNzEyfFY5NzVtfFYxMDF3fFY5NjF3fFY4MTJ8VjgxOHxWOTcxfFY5NzFzfFY5MTl8Vjk4OXxWMTE2d3xWMTAyd3xWOTczfFZpNDApXFxcXGJbXFxcXHNdK3xWMTAgXFxcXGI0R1xcXFxiXCIsXG4gICAgICAgIFwiSmF5dGVjaFRhYmxldFwiOiBcIlRQQy1QQTc2MlwiLFxuICAgICAgICBcIkJsYXVwdW5rdFRhYmxldFwiOiBcIkVuZGVhdm91ciA4MDBOR3xFbmRlYXZvdXIgMTAxMFwiLFxuICAgICAgICBcIkRpZ21hVGFibGV0XCI6IFwiXFxcXGIoaUR4MTB8aUR4OXxpRHg4fGlEeDd8aUR4RDd8aUR4RDh8aURzUTh8aURzUTd8aURzUTh8aURzRDEwfGlEbkQ3fDNUUzgwNEh8aURzUTExfGlEajd8aURzMTApXFxcXGJcIixcbiAgICAgICAgXCJFdm9saW9UYWJsZXRcIjogXCJBUklBX01pbmlfd2lmaXxBcmlhWyBfXU1pbml8RXZvbGlvIFgxMHxFdm9saW8gWDd8RXZvbGlvIFg4fFxcXFxiRXZvdGFiXFxcXGJ8XFxcXGJOZXVyYVxcXFxiXCIsXG4gICAgICAgIFwiTGF2YVRhYmxldFwiOiBcIlFQQUQgRTcwNHxcXFxcYkl2b3J5U1xcXFxifEUtVEFCIElWT1JZfFxcXFxiRS1UQUJcXFxcYlwiLFxuICAgICAgICBcIkFvY1RhYmxldFwiOiBcIk1XMDgxMXxNVzA4MTJ8TVcwOTIyfE1USzgzODJ8TVcxMDMxfE1XMDgzMXxNVzA4MjF8TVcwOTMxfE1XMDcxMlwiLFxuICAgICAgICBcIk1wbWFuVGFibGV0XCI6IFwiTVAxMSBPQ1RBfE1QMTAgT0NUQXxNUFFDMTExNHxNUFFDMTAwNHxNUFFDOTk0fE1QUUM5NzR8TVBRQzk3M3xNUFFDODA0fE1QUUM3ODR8TVBRQzc4MHxcXFxcYk1QRzdcXFxcYnxNUERDRzc1fE1QRENHNzF8TVBEQzEwMDZ8TVAxMDFEQ3xNUERDOTAwMHxNUERDOTA1fE1QREM3MDZIRHxNUERDNzA2fE1QREM3MDV8TVBEQzExMHxNUERDMTAwfE1QREM5OXxNUERDOTd8TVBEQzg4fE1QREM4fE1QREM3N3xNUDcwOXxNSUQ3MDF8TUlENzExfE1JRDE3MHxNUERDNzAzfE1QUUMxMDEwXCIsXG4gICAgICAgIFwiQ2Vsa29uVGFibGV0XCI6IFwiQ1Q2OTV8Q1Q4ODh8Q1RbXFxcXHNdPzkxMHxDVDcgVGFifENUOSBUYWJ8Q1QzIFRhYnxDVDIgVGFifENUMSBUYWJ8QzgyMHxDNzIwfFxcXFxiQ1QtMVxcXFxiXCIsXG4gICAgICAgIFwiV29sZGVyVGFibGV0XCI6IFwibWlUYWIgXFxcXGIoRElBTU9ORHxTUEFDRXxCUk9PS0xZTnxORU98RkxZfE1BTkhBVFRBTnxGVU5LfEVWT0xVVElPTnxTS1l8R09DQVJ8SVJPTnxHRU5JVVN8UE9QfE1JTlR8RVBTSUxPTnxCUk9BRFdBWXxKVU1QfEhPUHxMRUdFTkR8TkVXIEFHRXxMSU5FfEFEVkFOQ0V8RkVFTHxGT0xMT1d8TElLRXxMSU5LfExJVkV8VEhJTkt8RlJFRURPTXxDSElDQUdPfENMRVZFTEFORHxCQUxUSU1PUkUtR0h8SU9XQXxCT1NUT058U0VBVFRMRXxQSE9FTklYfERBTExBU3xJTiAxMDF8TWFzdGVyQ2hlZilcXFxcYlwiLFxuICAgICAgICBcIk1lZGlhY29tVGFibGV0XCI6IFwiTS1NUEkxMEMzR3xNLVNQMTBFR3xNLVNQMTBFR1B8TS1TUDEwSFhBSHxNLVNQN0hYQUh8TS1TUDEwSFhCSHxNLVNQOEhYQUh8TS1TUDhNWEFcIixcbiAgICAgICAgXCJNaVRhYmxldFwiOiBcIlxcXFxiTUkgUEFEXFxcXGJ8XFxcXGJITSBOT1RFIDFXXFxcXGJcIixcbiAgICAgICAgXCJOaWJpcnVUYWJsZXRcIjogXCJOaWJpcnUgTTF8TmliaXJ1IEp1cGl0ZXIgT25lXCIsXG4gICAgICAgIFwiTmV4b1RhYmxldFwiOiBcIk5FWE8gTk9WQXxORVhPIDEwfE5FWE8gQVZJT3xORVhPIEZSRUV8TkVYTyBHT3xORVhPIEVWT3xORVhPIDNHfE5FWE8gU01BUlR8TkVYTyBLSURET3xORVhPIE1PQklcIixcbiAgICAgICAgXCJMZWFkZXJUYWJsZXRcIjogXCJUQkxUMTBRfFRCTFQxMEl8VEJMLTEwV0RLQnxUQkwtMTBXREtCTzIwMTN8VEJMLVcyMzBWMnxUQkwtVzQ1MHxUQkwtVzUwMHxTVjU3MnxUQkxUN0l8VEJBLUFDNy04R3xUQkxUNzl8VEJMLThXMTZ8VEJMLTEwVzMyfFRCTC0xMFdLQnxUQkwtVzEwMFwiLFxuICAgICAgICBcIlViaXNsYXRlVGFibGV0XCI6IFwiVWJpU2xhdGVbXFxcXHNdPzdDXCIsXG4gICAgICAgIFwiUG9ja2V0Qm9va1RhYmxldFwiOiBcIlBvY2tldGJvb2tcIixcbiAgICAgICAgXCJLb2Nhc29UYWJsZXRcIjogXCJcXFxcYihUQi0xMjA3KVxcXFxiXCIsXG4gICAgICAgIFwiSGlzZW5zZVRhYmxldFwiOiBcIlxcXFxiKEY1MjgxfEUyMzcxKVxcXFxiXCIsXG4gICAgICAgIFwiSHVkbFwiOiBcIkh1ZGwgSFQ3UzN8SHVkbCAyXCIsXG4gICAgICAgIFwiVGVsc3RyYVRhYmxldFwiOiBcIlQtSHViMlwiLFxuICAgICAgICBcIkdlbmVyaWNUYWJsZXRcIjogXCJBbmRyb2lkLipcXFxcYjk3RFxcXFxifFRhYmxldCg/IS4qUEMpfEJOVFYyNTBBfE1JRC1XQ0RNQXxMb2dpY1BEIFpvb20yfFxcXFxiQTdFQlxcXFxifENhdE5vdmE4fEExXzA3fENUNzA0fENUMTAwMnxcXFxcYk03MjFcXFxcYnxyazMwc2RrfFxcXFxiRVZPVEFCXFxcXGJ8TTc1OEF8RVQ5MDR8QUxVTUlVTTEwfFNtYXJ0ZnJlbiBUYWJ8RW5kZWF2b3VyIDEwMTB8VGFibGV0LVBDLTR8VGFnaSBUYWJ8XFxcXGJNNnByb1xcXFxifENUMTAyMFd8YXJjIDEwSER8XFxcXGJUUDc1MFxcXFxifFxcXFxiUVRBUVozXFxcXGJ8V1ZUMTAxfFRNMTA4OHxLVDEwN1wiXG4gICAgfSxcbiAgICBcIm9zc1wiOiB7XG4gICAgICAgIFwiQW5kcm9pZE9TXCI6IFwiQW5kcm9pZFwiLFxuICAgICAgICBcIkJsYWNrQmVycnlPU1wiOiBcImJsYWNrYmVycnl8XFxcXGJCQjEwXFxcXGJ8cmltIHRhYmxldCBvc1wiLFxuICAgICAgICBcIlBhbG1PU1wiOiBcIlBhbG1PU3xhdmFudGdvfGJsYXplcnxlbGFpbmV8aGlwdG9wfHBhbG18cGx1Y2tlcnx4aWlub1wiLFxuICAgICAgICBcIlN5bWJpYW5PU1wiOiBcIlN5bWJpYW58U3ltYk9TfFNlcmllczYwfFNlcmllczQwfFNZQi1bMC05XSt8XFxcXGJTNjBcXFxcYlwiLFxuICAgICAgICBcIldpbmRvd3NNb2JpbGVPU1wiOiBcIldpbmRvd3MgQ0UuKihQUEN8U21hcnRwaG9uZXxNb2JpbGV8WzAtOV17M314WzAtOV17M30pfFdpbmRvd3MgTW9iaWxlfFdpbmRvd3MgUGhvbmUgWzAtOS5dK3xXQ0U7XCIsXG4gICAgICAgIFwiV2luZG93c1Bob25lT1NcIjogXCJXaW5kb3dzIFBob25lIDEwLjB8V2luZG93cyBQaG9uZSA4LjF8V2luZG93cyBQaG9uZSA4LjB8V2luZG93cyBQaG9uZSBPU3xYQkxXUDd8WnVuZVdQN3xXaW5kb3dzIE5UIDYuWzIzXTsgQVJNO1wiLFxuICAgICAgICBcImlPU1wiOiBcIlxcXFxiaVBob25lLipNb2JpbGV8XFxcXGJpUG9kfFxcXFxiaVBhZHxBcHBsZUNvcmVNZWRpYVwiLFxuICAgICAgICBcImlQYWRPU1wiOiBcIkNQVSBPUyAxM1wiLFxuICAgICAgICBcIk1lZUdvT1NcIjogXCJNZWVHb1wiLFxuICAgICAgICBcIk1hZW1vT1NcIjogXCJNYWVtb1wiLFxuICAgICAgICBcIkphdmFPU1wiOiBcIkoyTUVcXC98XFxcXGJNSURQXFxcXGJ8XFxcXGJDTERDXFxcXGJcIixcbiAgICAgICAgXCJ3ZWJPU1wiOiBcIndlYk9TfGhwd09TXCIsXG4gICAgICAgIFwiYmFkYU9TXCI6IFwiXFxcXGJCYWRhXFxcXGJcIixcbiAgICAgICAgXCJCUkVXT1NcIjogXCJCUkVXXCJcbiAgICB9LFxuICAgIFwidWFzXCI6IHtcbiAgICAgICAgXCJDaHJvbWVcIjogXCJcXFxcYkNyTW9cXFxcYnxDcmlPU3xBbmRyb2lkLipDaHJvbWVcXC9bLjAtOV0qIChNb2JpbGUpP1wiLFxuICAgICAgICBcIkRvbGZpblwiOiBcIlxcXFxiRG9sZmluXFxcXGJcIixcbiAgICAgICAgXCJPcGVyYVwiOiBcIk9wZXJhLipNaW5pfE9wZXJhLipNb2JpfEFuZHJvaWQuKk9wZXJhfE1vYmlsZS4qT1BSXFwvWzAtOS5dKyR8Q29hc3RcXC9bMC05Ll0rXCIsXG4gICAgICAgIFwiU2t5ZmlyZVwiOiBcIlNreWZpcmVcIixcbiAgICAgICAgXCJFZGdlXCI6IFwiTW9iaWxlIFNhZmFyaVxcL1suMC05XSogRWRnZVwiLFxuICAgICAgICBcIklFXCI6IFwiSUVNb2JpbGV8TVNJRU1vYmlsZVwiLFxuICAgICAgICBcIkZpcmVmb3hcIjogXCJmZW5uZWN8ZmlyZWZveC4qbWFlbW98KE1vYmlsZXxUYWJsZXQpLipGaXJlZm94fEZpcmVmb3guKk1vYmlsZXxGeGlPU1wiLFxuICAgICAgICBcIkJvbHRcIjogXCJib2x0XCIsXG4gICAgICAgIFwiVGVhU2hhcmtcIjogXCJ0ZWFzaGFya1wiLFxuICAgICAgICBcIkJsYXplclwiOiBcIkJsYXplclwiLFxuICAgICAgICBcIlNhZmFyaVwiOiBcIlZlcnNpb24uKk1vYmlsZS4qU2FmYXJpfFNhZmFyaS4qTW9iaWxlfE1vYmlsZVNhZmFyaVwiLFxuICAgICAgICBcIldlQ2hhdFwiOiBcIlxcXFxiTWljcm9NZXNzZW5nZXJcXFxcYlwiLFxuICAgICAgICBcIlVDQnJvd3NlclwiOiBcIlVDLipCcm93c2VyfFVDV0VCXCIsXG4gICAgICAgIFwiYmFpZHVib3hhcHBcIjogXCJiYWlkdWJveGFwcFwiLFxuICAgICAgICBcImJhaWR1YnJvd3NlclwiOiBcImJhaWR1YnJvd3NlclwiLFxuICAgICAgICBcIkRpaWdvQnJvd3NlclwiOiBcIkRpaWdvQnJvd3NlclwiLFxuICAgICAgICBcIk1lcmN1cnlcIjogXCJcXFxcYk1lcmN1cnlcXFxcYlwiLFxuICAgICAgICBcIk9iaWdvQnJvd3NlclwiOiBcIk9iaWdvXCIsXG4gICAgICAgIFwiTmV0RnJvbnRcIjogXCJORi1Ccm93c2VyXCIsXG4gICAgICAgIFwiR2VuZXJpY0Jyb3dzZXJcIjogXCJOb2tpYUJyb3dzZXJ8T3ZpQnJvd3NlcnxPbmVCcm93c2VyfFR3b25reUJlYW1Ccm93c2VyfFNFTUMuKkJyb3dzZXJ8Rmx5Rmxvd3xNaW5pbW98TmV0RnJvbnR8Tm92YXJyYS1WaXNpb258TVFRQnJvd3NlcnxNaWNyb01lc3NlbmdlclwiLFxuICAgICAgICBcIlBhbGVNb29uXCI6IFwiQW5kcm9pZC4qUGFsZU1vb258TW9iaWxlLipQYWxlTW9vblwiXG4gICAgfSxcbiAgICBcInByb3BzXCI6IHtcbiAgICAgICAgXCJNb2JpbGVcIjogXCJNb2JpbGVcXC9bVkVSXVwiLFxuICAgICAgICBcIkJ1aWxkXCI6IFwiQnVpbGRcXC9bVkVSXVwiLFxuICAgICAgICBcIlZlcnNpb25cIjogXCJWZXJzaW9uXFwvW1ZFUl1cIixcbiAgICAgICAgXCJWZW5kb3JJRFwiOiBcIlZlbmRvcklEXFwvW1ZFUl1cIixcbiAgICAgICAgXCJpUGFkXCI6IFwiaVBhZC4qQ1BVW2EteiBdK1tWRVJdXCIsXG4gICAgICAgIFwiaVBob25lXCI6IFwiaVBob25lLipDUFVbYS16IF0rW1ZFUl1cIixcbiAgICAgICAgXCJpUG9kXCI6IFwiaVBvZC4qQ1BVW2EteiBdK1tWRVJdXCIsXG4gICAgICAgIFwiS2luZGxlXCI6IFwiS2luZGxlXFwvW1ZFUl1cIixcbiAgICAgICAgXCJDaHJvbWVcIjogW1xuICAgICAgICAgICAgXCJDaHJvbWVcXC9bVkVSXVwiLFxuICAgICAgICAgICAgXCJDcmlPU1xcL1tWRVJdXCIsXG4gICAgICAgICAgICBcIkNyTW9cXC9bVkVSXVwiXG4gICAgICAgIF0sXG4gICAgICAgIFwiQ29hc3RcIjogW1xuICAgICAgICAgICAgXCJDb2FzdFxcL1tWRVJdXCJcbiAgICAgICAgXSxcbiAgICAgICAgXCJEb2xmaW5cIjogXCJEb2xmaW5cXC9bVkVSXVwiLFxuICAgICAgICBcIkZpcmVmb3hcIjogW1xuICAgICAgICAgICAgXCJGaXJlZm94XFwvW1ZFUl1cIixcbiAgICAgICAgICAgIFwiRnhpT1NcXC9bVkVSXVwiXG4gICAgICAgIF0sXG4gICAgICAgIFwiRmVubmVjXCI6IFwiRmVubmVjXFwvW1ZFUl1cIixcbiAgICAgICAgXCJFZGdlXCI6IFwiRWRnZVxcL1tWRVJdXCIsXG4gICAgICAgIFwiSUVcIjogW1xuICAgICAgICAgICAgXCJJRU1vYmlsZVxcL1tWRVJdO1wiLFxuICAgICAgICAgICAgXCJJRU1vYmlsZSBbVkVSXVwiLFxuICAgICAgICAgICAgXCJNU0lFIFtWRVJdO1wiLFxuICAgICAgICAgICAgXCJUcmlkZW50XFwvWzAtOS5dKzsuKnJ2OltWRVJdXCJcbiAgICAgICAgXSxcbiAgICAgICAgXCJOZXRGcm9udFwiOiBcIk5ldEZyb250XFwvW1ZFUl1cIixcbiAgICAgICAgXCJOb2tpYUJyb3dzZXJcIjogXCJOb2tpYUJyb3dzZXJcXC9bVkVSXVwiLFxuICAgICAgICBcIk9wZXJhXCI6IFtcbiAgICAgICAgICAgIFwiIE9QUlxcL1tWRVJdXCIsXG4gICAgICAgICAgICBcIk9wZXJhIE1pbmlcXC9bVkVSXVwiLFxuICAgICAgICAgICAgXCJWZXJzaW9uXFwvW1ZFUl1cIlxuICAgICAgICBdLFxuICAgICAgICBcIk9wZXJhIE1pbmlcIjogXCJPcGVyYSBNaW5pXFwvW1ZFUl1cIixcbiAgICAgICAgXCJPcGVyYSBNb2JpXCI6IFwiVmVyc2lvblxcL1tWRVJdXCIsXG4gICAgICAgIFwiVUNCcm93c2VyXCI6IFtcbiAgICAgICAgICAgIFwiVUNXRUJbVkVSXVwiLFxuICAgICAgICAgICAgXCJVQy4qQnJvd3NlclxcL1tWRVJdXCJcbiAgICAgICAgXSxcbiAgICAgICAgXCJNUVFCcm93c2VyXCI6IFwiTVFRQnJvd3NlclxcL1tWRVJdXCIsXG4gICAgICAgIFwiTWljcm9NZXNzZW5nZXJcIjogXCJNaWNyb01lc3NlbmdlclxcL1tWRVJdXCIsXG4gICAgICAgIFwiYmFpZHVib3hhcHBcIjogXCJiYWlkdWJveGFwcFxcL1tWRVJdXCIsXG4gICAgICAgIFwiYmFpZHVicm93c2VyXCI6IFwiYmFpZHVicm93c2VyXFwvW1ZFUl1cIixcbiAgICAgICAgXCJTYW1zdW5nQnJvd3NlclwiOiBcIlNhbXN1bmdCcm93c2VyXFwvW1ZFUl1cIixcbiAgICAgICAgXCJJcm9uXCI6IFwiSXJvblxcL1tWRVJdXCIsXG4gICAgICAgIFwiU2FmYXJpXCI6IFtcbiAgICAgICAgICAgIFwiVmVyc2lvblxcL1tWRVJdXCIsXG4gICAgICAgICAgICBcIlNhZmFyaVxcL1tWRVJdXCJcbiAgICAgICAgXSxcbiAgICAgICAgXCJTa3lmaXJlXCI6IFwiU2t5ZmlyZVxcL1tWRVJdXCIsXG4gICAgICAgIFwiVGl6ZW5cIjogXCJUaXplblxcL1tWRVJdXCIsXG4gICAgICAgIFwiV2Via2l0XCI6IFwid2Via2l0WyBcXC9dW1ZFUl1cIixcbiAgICAgICAgXCJQYWxlTW9vblwiOiBcIlBhbGVNb29uXFwvW1ZFUl1cIixcbiAgICAgICAgXCJHZWNrb1wiOiBcIkdlY2tvXFwvW1ZFUl1cIixcbiAgICAgICAgXCJUcmlkZW50XCI6IFwiVHJpZGVudFxcL1tWRVJdXCIsXG4gICAgICAgIFwiUHJlc3RvXCI6IFwiUHJlc3RvXFwvW1ZFUl1cIixcbiAgICAgICAgXCJHb2FubmFcIjogXCJHb2FubmFcXC9bVkVSXVwiLFxuICAgICAgICBcImlPU1wiOiBcIiBcXFxcYmk/T1NcXFxcYiBbVkVSXVsgO117MX1cIixcbiAgICAgICAgXCJBbmRyb2lkXCI6IFwiQW5kcm9pZCBbVkVSXVwiLFxuICAgICAgICBcIkJsYWNrQmVycnlcIjogW1xuICAgICAgICAgICAgXCJCbGFja0JlcnJ5W1xcXFx3XStcXC9bVkVSXVwiLFxuICAgICAgICAgICAgXCJCbGFja0JlcnJ5LipWZXJzaW9uXFwvW1ZFUl1cIixcbiAgICAgICAgICAgIFwiVmVyc2lvblxcL1tWRVJdXCJcbiAgICAgICAgXSxcbiAgICAgICAgXCJCUkVXXCI6IFwiQlJFVyBbVkVSXVwiLFxuICAgICAgICBcIkphdmFcIjogXCJKYXZhXFwvW1ZFUl1cIixcbiAgICAgICAgXCJXaW5kb3dzIFBob25lIE9TXCI6IFtcbiAgICAgICAgICAgIFwiV2luZG93cyBQaG9uZSBPUyBbVkVSXVwiLFxuICAgICAgICAgICAgXCJXaW5kb3dzIFBob25lIFtWRVJdXCJcbiAgICAgICAgXSxcbiAgICAgICAgXCJXaW5kb3dzIFBob25lXCI6IFwiV2luZG93cyBQaG9uZSBbVkVSXVwiLFxuICAgICAgICBcIldpbmRvd3MgQ0VcIjogXCJXaW5kb3dzIENFXFwvW1ZFUl1cIixcbiAgICAgICAgXCJXaW5kb3dzIE5UXCI6IFwiV2luZG93cyBOVCBbVkVSXVwiLFxuICAgICAgICBcIlN5bWJpYW5cIjogW1xuICAgICAgICAgICAgXCJTeW1iaWFuT1NcXC9bVkVSXVwiLFxuICAgICAgICAgICAgXCJTeW1iaWFuXFwvW1ZFUl1cIlxuICAgICAgICBdLFxuICAgICAgICBcIndlYk9TXCI6IFtcbiAgICAgICAgICAgIFwid2ViT1NcXC9bVkVSXVwiLFxuICAgICAgICAgICAgXCJocHdPU1xcL1tWRVJdO1wiXG4gICAgICAgIF1cbiAgICB9LFxuICAgIFwidXRpbHNcIjoge1xuICAgICAgICBcIkJvdFwiOiBcIkdvb2dsZWJvdHxmYWNlYm9va2V4dGVybmFsaGl0fEdvb2dsZS1BTVBIVE1MfHN+YW1wLXZhbGlkYXRvcnxBZHNCb3QtR29vZ2xlfEdvb2dsZSBLZXl3b3JkIFN1Z2dlc3Rpb258RmFjZWJvdHxZYW5kZXhCb3R8WWFuZGV4TW9iaWxlQm90fGJpbmdib3R8aWFfYXJjaGl2ZXJ8QWhyZWZzQm90fEV6b29tc3xHU0xGYm90fFdCU2VhcmNoQm90fFR3aXR0ZXJib3R8VHdlZXRtZW1lQm90fFR3aWtsZXxQYXBlckxpQm90fFdvdGJveHxVbndpbmRGZXRjaG9yfEV4YWJvdHxNSjEyYm90fFlhbmRleEltYWdlc3xUdXJuaXRpbkJvdHxQaW5nZG9tfGNvbnRlbnRraW5nYXBwXCIsXG4gICAgICAgIFwiTW9iaWxlQm90XCI6IFwiR29vZ2xlYm90LU1vYmlsZXxBZHNCb3QtR29vZ2xlLU1vYmlsZXxZYWhvb1NlZWtlclxcL00xQTEtUjJEMlwiLFxuICAgICAgICBcIkRlc2t0b3BNb2RlXCI6IFwiV1BEZXNrdG9wXCIsXG4gICAgICAgIFwiVFZcIjogXCJTb255RFRWfEhiYlRWXCIsXG4gICAgICAgIFwiV2ViS2l0XCI6IFwiKHdlYmtpdClbIFxcL10oW1xcXFx3Ll0rKVwiLFxuICAgICAgICBcIkNvbnNvbGVcIjogXCJcXFxcYihOaW50ZW5kb3xOaW50ZW5kbyBXaWlVfE5pbnRlbmRvIDNEU3xOaW50ZW5kbyBTd2l0Y2h8UExBWVNUQVRJT058WGJveClcXFxcYlwiLFxuICAgICAgICBcIldhdGNoXCI6IFwiU00tVjcwMFwiXG4gICAgfVxufTtcblxuICAgIC8vIGZvbGxvd2luZyBwYXR0ZXJucyBjb21lIGZyb20gaHR0cDovL2RldGVjdG1vYmlsZWJyb3dzZXJzLmNvbS9cbiAgICBpbXBsLmRldGVjdE1vYmlsZUJyb3dzZXJzID0ge1xuICAgICAgICBmdWxsUGF0dGVybjogLyhhbmRyb2lkfGJiXFxkK3xtZWVnbykuK21vYmlsZXxhdmFudGdvfGJhZGFcXC98YmxhY2tiZXJyeXxibGF6ZXJ8Y29tcGFsfGVsYWluZXxmZW5uZWN8aGlwdG9wfGllbW9iaWxlfGlwKGhvbmV8b2QpfGlyaXN8a2luZGxlfGxnZSB8bWFlbW98bWlkcHxtbXB8bW9iaWxlLitmaXJlZm94fG5ldGZyb250fG9wZXJhIG0ob2J8aW4paXxwYWxtKCBvcyk/fHBob25lfHAoaXhpfHJlKVxcL3xwbHVja2VyfHBvY2tldHxwc3B8c2VyaWVzKDR8NikwfHN5bWJpYW58dHJlb3x1cFxcLihicm93c2VyfGxpbmspfHZvZGFmb25lfHdhcHx3aW5kb3dzIGNlfHhkYXx4aWluby9pLFxuICAgICAgICBzaG9ydFBhdHRlcm46IC8xMjA3fDYzMTB8NjU5MHwzZ3NvfDR0aHB8NTBbMS02XWl8Nzcwc3w4MDJzfGEgd2F8YWJhY3xhYyhlcnxvb3xzXFwtKXxhaShrb3xybil8YWwoYXZ8Y2F8Y28pfGFtb2l8YW4oZXh8bnl8eXcpfGFwdHV8YXIoY2h8Z28pfGFzKHRlfHVzKXxhdHR3fGF1KGRpfFxcLW18ciB8cyApfGF2YW58YmUoY2t8bGx8bnEpfGJpKGxifHJkKXxibChhY3xheil8YnIoZXx2KXd8YnVtYnxid1xcLShufHUpfGM1NVxcL3xjYXBpfGNjd2F8Y2RtXFwtfGNlbGx8Y2h0bXxjbGRjfGNtZFxcLXxjbyhtcHxuZCl8Y3Jhd3xkYShpdHxsbHxuZyl8ZGJ0ZXxkY1xcLXN8ZGV2aXxkaWNhfGRtb2J8ZG8oY3xwKW98ZHMoMTJ8XFwtZCl8ZWwoNDl8YWkpfGVtKGwyfHVsKXxlcihpY3xrMCl8ZXNsOHxleihbNC03XTB8b3N8d2F8emUpfGZldGN8Zmx5KFxcLXxfKXxnMSB1fGc1NjB8Z2VuZXxnZlxcLTV8Z1xcLW1vfGdvKFxcLnd8b2QpfGdyKGFkfHVuKXxoYWllfGhjaXR8aGRcXC0obXxwfHQpfGhlaVxcLXxoaShwdHx0YSl8aHAoIGl8aXApfGhzXFwtY3xodChjKFxcLXwgfF98YXxnfHB8c3x0KXx0cCl8aHUoYXd8dGMpfGlcXC0oMjB8Z298bWEpfGkyMzB8aWFjKCB8XFwtfFxcLyl8aWJyb3xpZGVhfGlnMDF8aWtvbXxpbTFrfGlubm98aXBhcXxpcmlzfGphKHR8dilhfGpicm98amVtdXxqaWdzfGtkZGl8a2VqaXxrZ3QoIHxcXC8pfGtsb258a3B0IHxrd2NcXC18a3lvKGN8ayl8bGUobm98eGkpfGxnKCBnfFxcLyhrfGx8dSl8NTB8NTR8XFwtW2Etd10pfGxpYnd8bHlueHxtMVxcLXd8bTNnYXxtNTBcXC98bWEodGV8dWl8eG8pfG1jKDAxfDIxfGNhKXxtXFwtY3J8bWUocmN8cmkpfG1pKG84fG9hfHRzKXxtbWVmfG1vKDAxfDAyfGJpfGRlfGRvfHQoXFwtfCB8b3x2KXx6eil8bXQoNTB8cDF8diApfG13YnB8bXl3YXxuMTBbMC0yXXxuMjBbMi0zXXxuMzAoMHwyKXxuNTAoMHwyfDUpfG43KDAoMHwxKXwxMCl8bmUoKGN8bSlcXC18b258dGZ8d2Z8d2d8d3QpfG5vayg2fGkpfG56cGh8bzJpbXxvcCh0aXx3dil8b3Jhbnxvd2cxfHA4MDB8cGFuKGF8ZHx0KXxwZHhnfHBnKDEzfFxcLShbMS04XXxjKSl8cGhpbHxwaXJlfHBsKGF5fHVjKXxwblxcLTJ8cG8oY2t8cnR8c2UpfHByb3h8cHNpb3xwdFxcLWd8cWFcXC1hfHFjKDA3fDEyfDIxfDMyfDYwfFxcLVsyLTddfGlcXC0pfHF0ZWt8cjM4MHxyNjAwfHJha3N8cmltOXxybyh2ZXx6byl8czU1XFwvfHNhKGdlfG1hfG1tfG1zfG55fHZhKXxzYygwMXxoXFwtfG9vfHBcXC0pfHNka1xcL3xzZShjKFxcLXwwfDEpfDQ3fG1jfG5kfHJpKXxzZ2hcXC18c2hhcnxzaWUoXFwtfG0pfHNrXFwtMHxzbCg0NXxpZCl8c20oYWx8YXJ8YjN8aXR8dDUpfHNvKGZ0fG55KXxzcCgwMXxoXFwtfHZcXC18diApfHN5KDAxfG1iKXx0MigxOHw1MCl8dDYoMDB8MTB8MTgpfHRhKGd0fGxrKXx0Y2xcXC18dGRnXFwtfHRlbChpfG0pfHRpbVxcLXx0XFwtbW98dG8ocGx8c2gpfHRzKDcwfG1cXC18bTN8bTUpfHR4XFwtOXx1cChcXC5ifGcxfHNpKXx1dHN0fHY0MDB8djc1MHx2ZXJpfHZpKHJnfHRlKXx2ayg0MHw1WzAtM118XFwtdil8dm00MHx2b2RhfHZ1bGN8dngoNTJ8NTN8NjB8NjF8NzB8ODB8ODF8ODN8ODV8OTgpfHczYyhcXC18ICl8d2ViY3x3aGl0fHdpKGcgfG5jfG53KXx3bWxifHdvbnV8eDcwMHx5YXNcXC18eW91cnx6ZXRvfHp0ZVxcLS9pLFxuICAgICAgICB0YWJsZXRQYXR0ZXJuOiAvYW5kcm9pZHxpcGFkfHBsYXlib29rfHNpbGsvaVxuICAgIH07XG5cbiAgICB2YXIgaGFzT3duUHJvcCA9IE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHksXG4gICAgICAgIGlzQXJyYXk7XG5cbiAgICBpbXBsLkZBTExCQUNLX1BIT05FID0gJ1Vua25vd25QaG9uZSc7XG4gICAgaW1wbC5GQUxMQkFDS19UQUJMRVQgPSAnVW5rbm93blRhYmxldCc7XG4gICAgaW1wbC5GQUxMQkFDS19NT0JJTEUgPSAnVW5rbm93bk1vYmlsZSc7XG5cbiAgICBpc0FycmF5ID0gKCdpc0FycmF5JyBpbiBBcnJheSkgP1xuICAgICAgICBBcnJheS5pc0FycmF5IDogZnVuY3Rpb24gKHZhbHVlKSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLnRvU3RyaW5nLmNhbGwodmFsdWUpID09PSAnW29iamVjdCBBcnJheV0nOyB9O1xuXG4gICAgZnVuY3Rpb24gZXF1YWxJQyhhLCBiKSB7XG4gICAgICAgIHJldHVybiBhICE9IG51bGwgJiYgYiAhPSBudWxsICYmIGEudG9Mb3dlckNhc2UoKSA9PT0gYi50b0xvd2VyQ2FzZSgpO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIGNvbnRhaW5zSUMoYXJyYXksIHZhbHVlKSB7XG4gICAgICAgIHZhciB2YWx1ZUxDLCBpLCBsZW4gPSBhcnJheS5sZW5ndGg7XG4gICAgICAgIGlmICghbGVuIHx8ICF2YWx1ZSkge1xuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICB9XG4gICAgICAgIHZhbHVlTEMgPSB2YWx1ZS50b0xvd2VyQ2FzZSgpO1xuICAgICAgICBmb3IgKGkgPSAwOyBpIDwgbGVuOyArK2kpIHtcbiAgICAgICAgICAgIGlmICh2YWx1ZUxDID09PSBhcnJheVtpXS50b0xvd2VyQ2FzZSgpKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIGNvbnZlcnRQcm9wc1RvUmVnRXhwKG9iamVjdCkge1xuICAgICAgICBmb3IgKHZhciBrZXkgaW4gb2JqZWN0KSB7XG4gICAgICAgICAgICBpZiAoaGFzT3duUHJvcC5jYWxsKG9iamVjdCwga2V5KSkge1xuICAgICAgICAgICAgICAgIG9iamVjdFtrZXldID0gbmV3IFJlZ0V4cChvYmplY3Rba2V5XSwgJ2knKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cblxuICAgIGZ1bmN0aW9uIHByZXBhcmVVc2VyQWdlbnQodXNlckFnZW50KSB7XG4gICAgICAgIHJldHVybiAodXNlckFnZW50IHx8ICcnKS5zdWJzdHIoMCwgNTAwKTsgLy8gbWl0aWdhdGUgdnVsbmVyYWJsZSB0byBSZURvU1xuICAgIH1cblxuICAgIChmdW5jdGlvbiBpbml0KCkge1xuICAgICAgICB2YXIga2V5LCB2YWx1ZXMsIHZhbHVlLCBpLCBsZW4sIHZlclBvcywgbW9iaWxlRGV0ZWN0UnVsZXMgPSBpbXBsLm1vYmlsZURldGVjdFJ1bGVzO1xuICAgICAgICBmb3IgKGtleSBpbiBtb2JpbGVEZXRlY3RSdWxlcy5wcm9wcykge1xuICAgICAgICAgICAgaWYgKGhhc093blByb3AuY2FsbChtb2JpbGVEZXRlY3RSdWxlcy5wcm9wcywga2V5KSkge1xuICAgICAgICAgICAgICAgIHZhbHVlcyA9IG1vYmlsZURldGVjdFJ1bGVzLnByb3BzW2tleV07XG4gICAgICAgICAgICAgICAgaWYgKCFpc0FycmF5KHZhbHVlcykpIHtcbiAgICAgICAgICAgICAgICAgICAgdmFsdWVzID0gW3ZhbHVlc107XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGxlbiA9IHZhbHVlcy5sZW5ndGg7XG4gICAgICAgICAgICAgICAgZm9yIChpID0gMDsgaSA8IGxlbjsgKytpKSB7XG4gICAgICAgICAgICAgICAgICAgIHZhbHVlID0gdmFsdWVzW2ldO1xuICAgICAgICAgICAgICAgICAgICB2ZXJQb3MgPSB2YWx1ZS5pbmRleE9mKCdbVkVSXScpO1xuICAgICAgICAgICAgICAgICAgICBpZiAodmVyUG9zID49IDApIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlID0gdmFsdWUuc3Vic3RyaW5nKDAsIHZlclBvcykgKyAnKFtcXFxcdy5fXFxcXCtdKyknICsgdmFsdWUuc3Vic3RyaW5nKHZlclBvcyArIDUpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIHZhbHVlc1tpXSA9IG5ldyBSZWdFeHAodmFsdWUsICdpJyk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIG1vYmlsZURldGVjdFJ1bGVzLnByb3BzW2tleV0gPSB2YWx1ZXM7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgY29udmVydFByb3BzVG9SZWdFeHAobW9iaWxlRGV0ZWN0UnVsZXMub3NzKTtcbiAgICAgICAgY29udmVydFByb3BzVG9SZWdFeHAobW9iaWxlRGV0ZWN0UnVsZXMucGhvbmVzKTtcbiAgICAgICAgY29udmVydFByb3BzVG9SZWdFeHAobW9iaWxlRGV0ZWN0UnVsZXMudGFibGV0cyk7XG4gICAgICAgIGNvbnZlcnRQcm9wc1RvUmVnRXhwKG1vYmlsZURldGVjdFJ1bGVzLnVhcyk7XG4gICAgICAgIGNvbnZlcnRQcm9wc1RvUmVnRXhwKG1vYmlsZURldGVjdFJ1bGVzLnV0aWxzKTtcblxuICAgICAgICAvLyBjb3B5IHNvbWUgcGF0dGVybnMgdG8gb3NzMCB3aGljaCBhcmUgdGVzdGVkIGZpcnN0IChzZWUgaXNzdWUjMTUpXG4gICAgICAgIG1vYmlsZURldGVjdFJ1bGVzLm9zczAgPSB7XG4gICAgICAgICAgICBXaW5kb3dzUGhvbmVPUzogbW9iaWxlRGV0ZWN0UnVsZXMub3NzLldpbmRvd3NQaG9uZU9TLFxuICAgICAgICAgICAgV2luZG93c01vYmlsZU9TOiBtb2JpbGVEZXRlY3RSdWxlcy5vc3MuV2luZG93c01vYmlsZU9TXG4gICAgICAgIH07XG4gICAgfSgpKTtcblxuICAgIC8qKlxuICAgICAqIFRlc3QgdXNlckFnZW50IHN0cmluZyBhZ2FpbnN0IGEgc2V0IG9mIHJ1bGVzIGFuZCBmaW5kIHRoZSBmaXJzdCBtYXRjaGVkIGtleS5cbiAgICAgKiBAcGFyYW0ge09iamVjdH0gcnVsZXMgKGtleSBpcyBTdHJpbmcsIHZhbHVlIGlzIFJlZ0V4cClcbiAgICAgKiBAcGFyYW0ge1N0cmluZ30gdXNlckFnZW50IHRoZSBuYXZpZ2F0b3IudXNlckFnZW50IChvciBIVFRQLUhlYWRlciAnVXNlci1BZ2VudCcpLlxuICAgICAqIEByZXR1cm5zIHtTdHJpbmd8bnVsbH0gdGhlIG1hdGNoZWQga2V5IGlmIGZvdW5kLCBvdGhlcndpc2UgPHR0Pm51bGw8L3R0PlxuICAgICAqIEBwcml2YXRlXG4gICAgICovXG4gICAgaW1wbC5maW5kTWF0Y2ggPSBmdW5jdGlvbihydWxlcywgdXNlckFnZW50KSB7XG4gICAgICAgIGZvciAodmFyIGtleSBpbiBydWxlcykge1xuICAgICAgICAgICAgaWYgKGhhc093blByb3AuY2FsbChydWxlcywga2V5KSkge1xuICAgICAgICAgICAgICAgIGlmIChydWxlc1trZXldLnRlc3QodXNlckFnZW50KSkge1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4ga2V5O1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gbnVsbDtcbiAgICB9O1xuXG4gICAgLyoqXG4gICAgICogVGVzdCB1c2VyQWdlbnQgc3RyaW5nIGFnYWluc3QgYSBzZXQgb2YgcnVsZXMgYW5kIHJldHVybiBhbiBhcnJheSBvZiBtYXRjaGVkIGtleXMuXG4gICAgICogQHBhcmFtIHtPYmplY3R9IHJ1bGVzIChrZXkgaXMgU3RyaW5nLCB2YWx1ZSBpcyBSZWdFeHApXG4gICAgICogQHBhcmFtIHtTdHJpbmd9IHVzZXJBZ2VudCB0aGUgbmF2aWdhdG9yLnVzZXJBZ2VudCAob3IgSFRUUC1IZWFkZXIgJ1VzZXItQWdlbnQnKS5cbiAgICAgKiBAcmV0dXJucyB7QXJyYXl9IGFuIGFycmF5IG9mIG1hdGNoZWQga2V5cywgbWF5IGJlIGVtcHR5IHdoZW4gdGhlcmUgaXMgbm8gbWF0Y2gsIGJ1dCBub3QgPHR0Pm51bGw8L3R0PlxuICAgICAqIEBwcml2YXRlXG4gICAgICovXG4gICAgaW1wbC5maW5kTWF0Y2hlcyA9IGZ1bmN0aW9uKHJ1bGVzLCB1c2VyQWdlbnQpIHtcbiAgICAgICAgdmFyIHJlc3VsdCA9IFtdO1xuICAgICAgICBmb3IgKHZhciBrZXkgaW4gcnVsZXMpIHtcbiAgICAgICAgICAgIGlmIChoYXNPd25Qcm9wLmNhbGwocnVsZXMsIGtleSkpIHtcbiAgICAgICAgICAgICAgICBpZiAocnVsZXNba2V5XS50ZXN0KHVzZXJBZ2VudCkpIHtcbiAgICAgICAgICAgICAgICAgICAgcmVzdWx0LnB1c2goa2V5KTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIHJlc3VsdDtcbiAgICB9O1xuXG4gICAgLyoqXG4gICAgICogQ2hlY2sgdGhlIHZlcnNpb24gb2YgdGhlIGdpdmVuIHByb3BlcnR5IGluIHRoZSBVc2VyLUFnZW50LlxuICAgICAqXG4gICAgICogQHBhcmFtIHtTdHJpbmd9IHByb3BlcnR5TmFtZVxuICAgICAqIEBwYXJhbSB7U3RyaW5nfSB1c2VyQWdlbnRcbiAgICAgKiBAcmV0dXJuIHtTdHJpbmd9IHZlcnNpb24gb3IgPHR0Pm51bGw8L3R0PiBpZiB2ZXJzaW9uIG5vdCBmb3VuZFxuICAgICAqIEBwcml2YXRlXG4gICAgICovXG4gICAgaW1wbC5nZXRWZXJzaW9uU3RyID0gZnVuY3Rpb24gKHByb3BlcnR5TmFtZSwgdXNlckFnZW50KSB7XG4gICAgICAgIHZhciBwcm9wcyA9IGltcGwubW9iaWxlRGV0ZWN0UnVsZXMucHJvcHMsIHBhdHRlcm5zLCBpLCBsZW4sIG1hdGNoO1xuICAgICAgICBpZiAoaGFzT3duUHJvcC5jYWxsKHByb3BzLCBwcm9wZXJ0eU5hbWUpKSB7XG4gICAgICAgICAgICBwYXR0ZXJucyA9IHByb3BzW3Byb3BlcnR5TmFtZV07XG4gICAgICAgICAgICBsZW4gPSBwYXR0ZXJucy5sZW5ndGg7XG4gICAgICAgICAgICBmb3IgKGkgPSAwOyBpIDwgbGVuOyArK2kpIHtcbiAgICAgICAgICAgICAgICBtYXRjaCA9IHBhdHRlcm5zW2ldLmV4ZWModXNlckFnZW50KTtcbiAgICAgICAgICAgICAgICBpZiAobWF0Y2ggIT09IG51bGwpIHtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIG1hdGNoWzFdO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gbnVsbDtcbiAgICB9O1xuXG4gICAgLyoqXG4gICAgICogQ2hlY2sgdGhlIHZlcnNpb24gb2YgdGhlIGdpdmVuIHByb3BlcnR5IGluIHRoZSBVc2VyLUFnZW50LlxuICAgICAqIFdpbGwgcmV0dXJuIGEgZmxvYXQgbnVtYmVyLiAoZWcuIDJfMCB3aWxsIHJldHVybiAyLjAsIDQuMy4xIHdpbGwgcmV0dXJuIDQuMzEpXG4gICAgICpcbiAgICAgKiBAcGFyYW0ge1N0cmluZ30gcHJvcGVydHlOYW1lXG4gICAgICogQHBhcmFtIHtTdHJpbmd9IHVzZXJBZ2VudFxuICAgICAqIEByZXR1cm4ge051bWJlcn0gdmVyc2lvbiBvciA8dHQ+TmFOPC90dD4gaWYgdmVyc2lvbiBub3QgZm91bmRcbiAgICAgKiBAcHJpdmF0ZVxuICAgICAqL1xuICAgIGltcGwuZ2V0VmVyc2lvbiA9IGZ1bmN0aW9uIChwcm9wZXJ0eU5hbWUsIHVzZXJBZ2VudCkge1xuICAgICAgICB2YXIgdmVyc2lvbiA9IGltcGwuZ2V0VmVyc2lvblN0cihwcm9wZXJ0eU5hbWUsIHVzZXJBZ2VudCk7XG4gICAgICAgIHJldHVybiB2ZXJzaW9uID8gaW1wbC5wcmVwYXJlVmVyc2lvbk5vKHZlcnNpb24pIDogTmFOO1xuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBQcmVwYXJlIHRoZSB2ZXJzaW9uIG51bWJlci5cbiAgICAgKlxuICAgICAqIEBwYXJhbSB7U3RyaW5nfSB2ZXJzaW9uXG4gICAgICogQHJldHVybiB7TnVtYmVyfSB0aGUgdmVyc2lvbiBudW1iZXIgYXMgYSBmbG9hdGluZyBudW1iZXJcbiAgICAgKiBAcHJpdmF0ZVxuICAgICAqL1xuICAgIGltcGwucHJlcGFyZVZlcnNpb25ObyA9IGZ1bmN0aW9uICh2ZXJzaW9uKSB7XG4gICAgICAgIHZhciBudW1iZXJzO1xuXG4gICAgICAgIG51bWJlcnMgPSB2ZXJzaW9uLnNwbGl0KC9bYS16Ll8gXFwvXFwtXS9pKTtcbiAgICAgICAgaWYgKG51bWJlcnMubGVuZ3RoID09PSAxKSB7XG4gICAgICAgICAgICB2ZXJzaW9uID0gbnVtYmVyc1swXTtcbiAgICAgICAgfVxuICAgICAgICBpZiAobnVtYmVycy5sZW5ndGggPiAxKSB7XG4gICAgICAgICAgICB2ZXJzaW9uID0gbnVtYmVyc1swXSArICcuJztcbiAgICAgICAgICAgIG51bWJlcnMuc2hpZnQoKTtcbiAgICAgICAgICAgIHZlcnNpb24gKz0gbnVtYmVycy5qb2luKCcnKTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gTnVtYmVyKHZlcnNpb24pO1xuICAgIH07XG5cbiAgICBpbXBsLmlzTW9iaWxlRmFsbGJhY2sgPSBmdW5jdGlvbiAodXNlckFnZW50KSB7XG4gICAgICAgIHJldHVybiBpbXBsLmRldGVjdE1vYmlsZUJyb3dzZXJzLmZ1bGxQYXR0ZXJuLnRlc3QodXNlckFnZW50KSB8fFxuICAgICAgICAgICAgaW1wbC5kZXRlY3RNb2JpbGVCcm93c2Vycy5zaG9ydFBhdHRlcm4udGVzdCh1c2VyQWdlbnQuc3Vic3RyKDAsNCkpO1xuICAgIH07XG5cbiAgICBpbXBsLmlzVGFibGV0RmFsbGJhY2sgPSBmdW5jdGlvbiAodXNlckFnZW50KSB7XG4gICAgICAgIHJldHVybiBpbXBsLmRldGVjdE1vYmlsZUJyb3dzZXJzLnRhYmxldFBhdHRlcm4udGVzdCh1c2VyQWdlbnQpO1xuICAgIH07XG5cbiAgICBpbXBsLnByZXBhcmVEZXRlY3Rpb25DYWNoZSA9IGZ1bmN0aW9uIChjYWNoZSwgdXNlckFnZW50LCBtYXhQaG9uZVdpZHRoKSB7XG4gICAgICAgIGlmIChjYWNoZS5tb2JpbGUgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG4gICAgICAgIHZhciBwaG9uZSwgdGFibGV0LCBwaG9uZVNpemVkO1xuXG4gICAgICAgIC8vIGZpcnN0IGNoZWNrIGZvciBzdHJvbmdlciB0YWJsZXQgcnVsZXMsIHRoZW4gcGhvbmUgKHNlZSBpc3N1ZSM1KVxuICAgICAgICB0YWJsZXQgPSBpbXBsLmZpbmRNYXRjaChpbXBsLm1vYmlsZURldGVjdFJ1bGVzLnRhYmxldHMsIHVzZXJBZ2VudCk7XG4gICAgICAgIGlmICh0YWJsZXQpIHtcbiAgICAgICAgICAgIGNhY2hlLm1vYmlsZSA9IGNhY2hlLnRhYmxldCA9IHRhYmxldDtcbiAgICAgICAgICAgIGNhY2hlLnBob25lID0gbnVsbDtcbiAgICAgICAgICAgIHJldHVybjsgLy8gdW5hbWJpZ3VvdXNseSBpZGVudGlmaWVkIGFzIHRhYmxldFxuICAgICAgICB9XG5cbiAgICAgICAgcGhvbmUgPSBpbXBsLmZpbmRNYXRjaChpbXBsLm1vYmlsZURldGVjdFJ1bGVzLnBob25lcywgdXNlckFnZW50KTtcbiAgICAgICAgaWYgKHBob25lKSB7XG4gICAgICAgICAgICBjYWNoZS5tb2JpbGUgPSBjYWNoZS5waG9uZSA9IHBob25lO1xuICAgICAgICAgICAgY2FjaGUudGFibGV0ID0gbnVsbDtcbiAgICAgICAgICAgIHJldHVybjsgLy8gdW5hbWJpZ3VvdXNseSBpZGVudGlmaWVkIGFzIHBob25lXG4gICAgICAgIH1cblxuICAgICAgICAvLyBvdXIgcnVsZXMgaGF2ZW4ndCBmb3VuZCBhIG1hdGNoIC0+IHRyeSBtb3JlIGdlbmVyYWwgZmFsbGJhY2sgcnVsZXNcbiAgICAgICAgaWYgKGltcGwuaXNNb2JpbGVGYWxsYmFjayh1c2VyQWdlbnQpKSB7XG4gICAgICAgICAgICBwaG9uZVNpemVkID0gTW9iaWxlRGV0ZWN0LmlzUGhvbmVTaXplZChtYXhQaG9uZVdpZHRoKTtcbiAgICAgICAgICAgIGlmIChwaG9uZVNpemVkID09PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgICAgICBjYWNoZS5tb2JpbGUgPSBpbXBsLkZBTExCQUNLX01PQklMRTtcbiAgICAgICAgICAgICAgICBjYWNoZS50YWJsZXQgPSBjYWNoZS5waG9uZSA9IG51bGw7XG4gICAgICAgICAgICB9IGVsc2UgaWYgKHBob25lU2l6ZWQpIHtcbiAgICAgICAgICAgICAgICBjYWNoZS5tb2JpbGUgPSBjYWNoZS5waG9uZSA9IGltcGwuRkFMTEJBQ0tfUEhPTkU7XG4gICAgICAgICAgICAgICAgY2FjaGUudGFibGV0ID0gbnVsbDtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgY2FjaGUubW9iaWxlID0gY2FjaGUudGFibGV0ID0gaW1wbC5GQUxMQkFDS19UQUJMRVQ7XG4gICAgICAgICAgICAgICAgY2FjaGUucGhvbmUgPSBudWxsO1xuICAgICAgICAgICAgfVxuICAgICAgICB9IGVsc2UgaWYgKGltcGwuaXNUYWJsZXRGYWxsYmFjayh1c2VyQWdlbnQpKSB7XG4gICAgICAgICAgICBjYWNoZS5tb2JpbGUgPSBjYWNoZS50YWJsZXQgPSBpbXBsLkZBTExCQUNLX1RBQkxFVDtcbiAgICAgICAgICAgIGNhY2hlLnBob25lID0gbnVsbDtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIC8vIG5vdCBtb2JpbGUgYXQgYWxsIVxuICAgICAgICAgICAgY2FjaGUubW9iaWxlID0gY2FjaGUudGFibGV0ID0gY2FjaGUucGhvbmUgPSBudWxsO1xuICAgICAgICB9XG4gICAgfTtcblxuICAgIC8vIHQgaXMgYSByZWZlcmVuY2UgdG8gYSBNb2JpbGVEZXRlY3QgaW5zdGFuY2VcbiAgICBpbXBsLm1vYmlsZUdyYWRlID0gZnVuY3Rpb24gKHQpIHtcbiAgICAgICAgLy8gaW1wbCBub3RlOlxuICAgICAgICAvLyBUbyBrZWVwIGluIHN5bmMgdy8gTW9iaWxlX0RldGVjdC5waHAgZWFzaWx5LCB0aGUgZm9sbG93aW5nIGNvZGUgaXMgdGlnaHRseSBhbGlnbmVkIHRvIHRoZSBQSFAgdmVyc2lvbi5cbiAgICAgICAgLy8gV2hlbiBjaGFuZ2VzIGFyZSBtYWRlIGluIE1vYmlsZV9EZXRlY3QucGhwLCBjb3B5IHRoaXMgbWV0aG9kIGFuZCByZXBsYWNlOlxuICAgICAgICAvLyAgICAgJHRoaXMtPiAvIHQuXG4gICAgICAgIC8vICAgICBzZWxmOjpNT0JJTEVfR1JBREVfKC4pIC8gJyQxJ1xuICAgICAgICAvLyAgICAgLCBzZWxmOjpWRVJTSU9OX1RZUEVfRkxPQVQgLyAobm90aGluZylcbiAgICAgICAgLy8gICAgIGlzSU9TKCkgLyBvcygnaU9TJylcbiAgICAgICAgLy8gICAgIFtyZWddIC8gKG5vdGhpbmcpICAgPC0tIGpzZGVsaXZyIGNvbXBsYWluaW5nIGFib3V0IHVuZXNjYXBlZCB1bmljb2RlIGNoYXJhY3RlciBVKzAwQUVcbiAgICAgICAgdmFyICRpc01vYmlsZSA9IHQubW9iaWxlKCkgIT09IG51bGw7XG5cbiAgICAgICAgaWYgKFxuICAgICAgICAgICAgLy8gQXBwbGUgaU9TIDMuMi01LjEgLSBUZXN0ZWQgb24gdGhlIG9yaWdpbmFsIGlQYWQgKDQuMyAvIDUuMCksIGlQYWQgMiAoNC4zKSwgaVBhZCAzICg1LjEpLCBvcmlnaW5hbCBpUGhvbmUgKDMuMSksIGlQaG9uZSAzICgzLjIpLCAzR1MgKDQuMyksIDQgKDQuMyAvIDUuMCksIGFuZCA0UyAoNS4xKVxuICAgICAgICAgICAgdC5vcygnaU9TJykgJiYgdC52ZXJzaW9uKCdpUGFkJyk+PTQuMyB8fFxuICAgICAgICAgICAgdC5vcygnaU9TJykgJiYgdC52ZXJzaW9uKCdpUGhvbmUnKT49My4xIHx8XG4gICAgICAgICAgICB0Lm9zKCdpT1MnKSAmJiB0LnZlcnNpb24oJ2lQb2QnKT49My4xIHx8XG5cbiAgICAgICAgICAgIC8vIEFuZHJvaWQgMi4xLTIuMyAtIFRlc3RlZCBvbiB0aGUgSFRDIEluY3JlZGlibGUgKDIuMiksIG9yaWdpbmFsIERyb2lkICgyLjIpLCBIVEMgQXJpYSAoMi4xKSwgR29vZ2xlIE5leHVzIFMgKDIuMykuIEZ1bmN0aW9uYWwgb24gMS41ICYgMS42IGJ1dCBwZXJmb3JtYW5jZSBtYXkgYmUgc2x1Z2dpc2gsIHRlc3RlZCBvbiBHb29nbGUgRzEgKDEuNSlcbiAgICAgICAgICAgIC8vIEFuZHJvaWQgMy4xIChIb25leWNvbWIpICAtIFRlc3RlZCBvbiB0aGUgU2Ftc3VuZyBHYWxheHkgVGFiIDEwLjEgYW5kIE1vdG9yb2xhIFhPT01cbiAgICAgICAgICAgIC8vIEFuZHJvaWQgNC4wIChJQ1MpICAtIFRlc3RlZCBvbiBhIEdhbGF4eSBOZXh1cy4gTm90ZTogdHJhbnNpdGlvbiBwZXJmb3JtYW5jZSBjYW4gYmUgcG9vciBvbiB1cGdyYWRlZCBkZXZpY2VzXG4gICAgICAgICAgICAvLyBBbmRyb2lkIDQuMSAoSmVsbHkgQmVhbikgIC0gVGVzdGVkIG9uIGEgR2FsYXh5IE5leHVzIGFuZCBHYWxheHkgN1xuICAgICAgICAgICAgKCB0LnZlcnNpb24oJ0FuZHJvaWQnKT4yLjEgJiYgdC5pcygnV2Via2l0JykgKSB8fFxuXG4gICAgICAgICAgICAvLyBXaW5kb3dzIFBob25lIDctNy41IC0gVGVzdGVkIG9uIHRoZSBIVEMgU3Vycm91bmQgKDcuMCkgSFRDIFRyb3BoeSAoNy41KSwgTEctRTkwMCAoNy41KSwgTm9raWEgTHVtaWEgODAwXG4gICAgICAgICAgICB0LnZlcnNpb24oJ1dpbmRvd3MgUGhvbmUgT1MnKT49Ny4wIHx8XG5cbiAgICAgICAgICAgIC8vIEJsYWNrYmVycnkgNyAtIFRlc3RlZCBvbiBCbGFja0JlcnJ5IFRvcmNoIDk4MTBcbiAgICAgICAgICAgIC8vIEJsYWNrYmVycnkgNi4wIC0gVGVzdGVkIG9uIHRoZSBUb3JjaCA5ODAwIGFuZCBTdHlsZSA5NjcwXG4gICAgICAgICAgICB0LmlzKCdCbGFja0JlcnJ5JykgJiYgdC52ZXJzaW9uKCdCbGFja0JlcnJ5Jyk+PTYuMCB8fFxuICAgICAgICAgICAgLy8gQmxhY2tiZXJyeSBQbGF5Ym9vayAoMS4wLTIuMCkgLSBUZXN0ZWQgb24gUGxheUJvb2tcbiAgICAgICAgICAgIHQubWF0Y2goJ1BsYXlib29rLipUYWJsZXQnKSB8fFxuXG4gICAgICAgICAgICAvLyBQYWxtIFdlYk9TICgxLjQtMi4wKSAtIFRlc3RlZCBvbiB0aGUgUGFsbSBQaXhpICgxLjQpLCBQcmUgKDEuNCksIFByZSAyICgyLjApXG4gICAgICAgICAgICAoIHQudmVyc2lvbignd2ViT1MnKT49MS40ICYmIHQubWF0Y2goJ1BhbG18UHJlfFBpeGknKSApIHx8XG4gICAgICAgICAgICAvLyBQYWxtIFdlYk9TIDMuMCAgLSBUZXN0ZWQgb24gSFAgVG91Y2hQYWRcbiAgICAgICAgICAgIHQubWF0Y2goJ2hwLipUb3VjaFBhZCcpIHx8XG5cbiAgICAgICAgICAgIC8vIEZpcmVmb3ggTW9iaWxlICgxMiBCZXRhKSAtIFRlc3RlZCBvbiBBbmRyb2lkIDIuMyBkZXZpY2VcbiAgICAgICAgICAgICggdC5pcygnRmlyZWZveCcpICYmIHQudmVyc2lvbignRmlyZWZveCcpPj0xMiApIHx8XG5cbiAgICAgICAgICAgIC8vIENocm9tZSBmb3IgQW5kcm9pZCAtIFRlc3RlZCBvbiBBbmRyb2lkIDQuMCwgNC4xIGRldmljZVxuICAgICAgICAgICAgKCB0LmlzKCdDaHJvbWUnKSAmJiB0LmlzKCdBbmRyb2lkT1MnKSAmJiB0LnZlcnNpb24oJ0FuZHJvaWQnKT49NC4wICkgfHxcblxuICAgICAgICAgICAgLy8gU2t5ZmlyZSA0LjEgLSBUZXN0ZWQgb24gQW5kcm9pZCAyLjMgZGV2aWNlXG4gICAgICAgICAgICAoIHQuaXMoJ1NreWZpcmUnKSAmJiB0LnZlcnNpb24oJ1NreWZpcmUnKT49NC4xICYmIHQuaXMoJ0FuZHJvaWRPUycpICYmIHQudmVyc2lvbignQW5kcm9pZCcpPj0yLjMgKSB8fFxuXG4gICAgICAgICAgICAvLyBPcGVyYSBNb2JpbGUgMTEuNS0xMjogVGVzdGVkIG9uIEFuZHJvaWQgMi4zXG4gICAgICAgICAgICAoIHQuaXMoJ09wZXJhJykgJiYgdC52ZXJzaW9uKCdPcGVyYSBNb2JpJyk+MTEgJiYgdC5pcygnQW5kcm9pZE9TJykgKSB8fFxuXG4gICAgICAgICAgICAvLyBNZWVnbyAxLjIgLSBUZXN0ZWQgb24gTm9raWEgOTUwIGFuZCBOOVxuICAgICAgICAgICAgdC5pcygnTWVlR29PUycpIHx8XG5cbiAgICAgICAgICAgIC8vIFRpemVuIChwcmUtcmVsZWFzZSkgLSBUZXN0ZWQgb24gZWFybHkgaGFyZHdhcmVcbiAgICAgICAgICAgIHQuaXMoJ1RpemVuJykgfHxcblxuICAgICAgICAgICAgLy8gU2Ftc3VuZyBCYWRhIDIuMCAtIFRlc3RlZCBvbiBhIFNhbXN1bmcgV2F2ZSAzLCBEb2xwaGluIGJyb3dzZXJcbiAgICAgICAgICAgIC8vIEB0b2RvOiBtb3JlIHRlc3RzIGhlcmUhXG4gICAgICAgICAgICB0LmlzKCdEb2xmaW4nKSAmJiB0LnZlcnNpb24oJ0JhZGEnKT49Mi4wIHx8XG5cbiAgICAgICAgICAgIC8vIFVDIEJyb3dzZXIgLSBUZXN0ZWQgb24gQW5kcm9pZCAyLjMgZGV2aWNlXG4gICAgICAgICAgICAoICh0LmlzKCdVQyBCcm93c2VyJykgfHwgdC5pcygnRG9sZmluJykpICYmIHQudmVyc2lvbignQW5kcm9pZCcpPj0yLjMgKSB8fFxuXG4gICAgICAgICAgICAvLyBLaW5kbGUgMyBhbmQgRmlyZSAgLSBUZXN0ZWQgb24gdGhlIGJ1aWx0LWluIFdlYktpdCBicm93c2VyIGZvciBlYWNoXG4gICAgICAgICAgICAoIHQubWF0Y2goJ0tpbmRsZSBGaXJlJykgfHxcbiAgICAgICAgICAgICAgICB0LmlzKCdLaW5kbGUnKSAmJiB0LnZlcnNpb24oJ0tpbmRsZScpPj0zLjAgKSB8fFxuXG4gICAgICAgICAgICAvLyBOb29rIENvbG9yIDEuNC4xIC0gVGVzdGVkIG9uIG9yaWdpbmFsIE5vb2sgQ29sb3IsIG5vdCBOb29rIFRhYmxldFxuICAgICAgICAgICAgdC5pcygnQW5kcm9pZE9TJykgJiYgdC5pcygnTm9va1RhYmxldCcpIHx8XG5cbiAgICAgICAgICAgIC8vIENocm9tZSBEZXNrdG9wIDExLTIxIC0gVGVzdGVkIG9uIE9TIFggMTAuNyBhbmQgV2luZG93cyA3XG4gICAgICAgICAgICB0LnZlcnNpb24oJ0Nocm9tZScpPj0xMSAmJiAhJGlzTW9iaWxlIHx8XG5cbiAgICAgICAgICAgIC8vIFNhZmFyaSBEZXNrdG9wIDQtNSAtIFRlc3RlZCBvbiBPUyBYIDEwLjcgYW5kIFdpbmRvd3MgN1xuICAgICAgICAgICAgdC52ZXJzaW9uKCdTYWZhcmknKT49NS4wICYmICEkaXNNb2JpbGUgfHxcblxuICAgICAgICAgICAgLy8gRmlyZWZveCBEZXNrdG9wIDQtMTMgLSBUZXN0ZWQgb24gT1MgWCAxMC43IGFuZCBXaW5kb3dzIDdcbiAgICAgICAgICAgIHQudmVyc2lvbignRmlyZWZveCcpPj00LjAgJiYgISRpc01vYmlsZSB8fFxuXG4gICAgICAgICAgICAvLyBJbnRlcm5ldCBFeHBsb3JlciA3LTkgLSBUZXN0ZWQgb24gV2luZG93cyBYUCwgVmlzdGEgYW5kIDdcbiAgICAgICAgICAgIHQudmVyc2lvbignTVNJRScpPj03LjAgJiYgISRpc01vYmlsZSB8fFxuXG4gICAgICAgICAgICAvLyBPcGVyYSBEZXNrdG9wIDEwLTEyIC0gVGVzdGVkIG9uIE9TIFggMTAuNyBhbmQgV2luZG93cyA3XG4gICAgICAgICAgICAvLyBAcmVmZXJlbmNlOiBodHRwOi8vbXkub3BlcmEuY29tL2NvbW11bml0eS9vcGVud2ViL2lkb3BlcmEvXG4gICAgICAgICAgICB0LnZlcnNpb24oJ09wZXJhJyk+PTEwICYmICEkaXNNb2JpbGVcblxuICAgICAgICAgICAgKXtcbiAgICAgICAgICAgIHJldHVybiAnQSc7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAoXG4gICAgICAgICAgICB0Lm9zKCdpT1MnKSAmJiB0LnZlcnNpb24oJ2lQYWQnKTw0LjMgfHxcbiAgICAgICAgICAgIHQub3MoJ2lPUycpICYmIHQudmVyc2lvbignaVBob25lJyk8My4xIHx8XG4gICAgICAgICAgICB0Lm9zKCdpT1MnKSAmJiB0LnZlcnNpb24oJ2lQb2QnKTwzLjEgfHxcblxuICAgICAgICAgICAgLy8gQmxhY2tiZXJyeSA1LjA6IFRlc3RlZCBvbiB0aGUgU3Rvcm0gMiA5NTUwLCBCb2xkIDk3NzBcbiAgICAgICAgICAgIHQuaXMoJ0JsYWNrYmVycnknKSAmJiB0LnZlcnNpb24oJ0JsYWNrQmVycnknKT49NSAmJiB0LnZlcnNpb24oJ0JsYWNrQmVycnknKTw2IHx8XG5cbiAgICAgICAgICAgIC8vT3BlcmEgTWluaSAoNS4wLTYuNSkgLSBUZXN0ZWQgb24gaU9TIDMuMi80LjMgYW5kIEFuZHJvaWQgMi4zXG4gICAgICAgICAgICAoIHQudmVyc2lvbignT3BlcmEgTWluaScpPj01LjAgJiYgdC52ZXJzaW9uKCdPcGVyYSBNaW5pJyk8PTYuNSAmJlxuICAgICAgICAgICAgICAgICh0LnZlcnNpb24oJ0FuZHJvaWQnKT49Mi4zIHx8IHQuaXMoJ2lPUycpKSApIHx8XG5cbiAgICAgICAgICAgIC8vIE5va2lhIFN5bWJpYW5eMyAtIFRlc3RlZCBvbiBOb2tpYSBOOCAoU3ltYmlhbl4zKSwgQzcgKFN5bWJpYW5eMyksIGFsc28gd29ya3Mgb24gTjk3IChTeW1iaWFuXjEpXG4gICAgICAgICAgICB0Lm1hdGNoKCdOb2tpYU44fE5va2lhQzd8Tjk3LipTZXJpZXM2MHxTeW1iaWFuLzMnKSB8fFxuXG4gICAgICAgICAgICAvLyBAdG9kbzogcmVwb3J0IHRoaXMgKHRlc3RlZCBvbiBOb2tpYSBONzEpXG4gICAgICAgICAgICB0LnZlcnNpb24oJ09wZXJhIE1vYmknKT49MTEgJiYgdC5pcygnU3ltYmlhbk9TJylcbiAgICAgICAgICAgICl7XG4gICAgICAgICAgICByZXR1cm4gJ0InO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKFxuICAgICAgICAvLyBCbGFja2JlcnJ5IDQueCAtIFRlc3RlZCBvbiB0aGUgQ3VydmUgODMzMFxuICAgICAgICAgICAgdC52ZXJzaW9uKCdCbGFja0JlcnJ5Jyk8NS4wIHx8XG4gICAgICAgICAgICAvLyBXaW5kb3dzIE1vYmlsZSAtIFRlc3RlZCBvbiB0aGUgSFRDIExlbyAoV2luTW8gNS4yKVxuICAgICAgICAgICAgdC5tYXRjaCgnTVNJRU1vYmlsZXxXaW5kb3dzIENFLipNb2JpbGUnKSB8fCB0LnZlcnNpb24oJ1dpbmRvd3MgTW9iaWxlJyk8PTUuMlxuXG4gICAgICAgICAgICApe1xuICAgICAgICAgICAgcmV0dXJuICdDJztcbiAgICAgICAgfVxuXG4gICAgICAgIC8vQWxsIG9sZGVyIHNtYXJ0cGhvbmUgcGxhdGZvcm1zIGFuZCBmZWF0dXJlcGhvbmVzIC0gQW55IGRldmljZSB0aGF0IGRvZXNuJ3Qgc3VwcG9ydCBtZWRpYSBxdWVyaWVzXG4gICAgICAgIC8vd2lsbCByZWNlaXZlIHRoZSBiYXNpYywgQyBncmFkZSBleHBlcmllbmNlLlxuICAgICAgICByZXR1cm4gJ0MnO1xuICAgIH07XG5cbiAgICBpbXBsLmRldGVjdE9TID0gZnVuY3Rpb24gKHVhKSB7XG4gICAgICAgIHJldHVybiBpbXBsLmZpbmRNYXRjaChpbXBsLm1vYmlsZURldGVjdFJ1bGVzLm9zczAsIHVhKSB8fFxuICAgICAgICAgICAgaW1wbC5maW5kTWF0Y2goaW1wbC5tb2JpbGVEZXRlY3RSdWxlcy5vc3MsIHVhKTtcbiAgICB9O1xuXG4gICAgaW1wbC5nZXREZXZpY2VTbWFsbGVyU2lkZSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgcmV0dXJuIHdpbmRvdy5zY3JlZW4ud2lkdGggPCB3aW5kb3cuc2NyZWVuLmhlaWdodCA/XG4gICAgICAgICAgICB3aW5kb3cuc2NyZWVuLndpZHRoIDpcbiAgICAgICAgICAgIHdpbmRvdy5zY3JlZW4uaGVpZ2h0O1xuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBDb25zdHJ1Y3RvciBmb3IgTW9iaWxlRGV0ZWN0IG9iamVjdC5cbiAgICAgKiA8YnI+XG4gICAgICogU3VjaCBhbiBvYmplY3Qgd2lsbCBrZWVwIGEgcmVmZXJlbmNlIHRvIHRoZSBnaXZlbiB1c2VyLWFnZW50IHN0cmluZyBhbmQgY2FjaGUgbW9zdCBvZiB0aGUgZGV0ZWN0IHF1ZXJpZXMuPGJyPlxuICAgICAqIDxkaXYgc3R5bGU9XCJiYWNrZ3JvdW5kLWNvbG9yOiAjZDllZGY3OyBib3JkZXI6IDFweCBzb2xpZCAjYmNlOGYxOyBjb2xvcjogIzNhODdhZDsgcGFkZGluZzogMTRweDsgYm9yZGVyLXJhZGl1czogMnB4OyBtYXJnaW4tdG9wOiAyMHB4XCI+XG4gICAgICogICAgIDxzdHJvbmc+RmluZCBpbmZvcm1hdGlvbiBob3cgdG8gZG93bmxvYWQgYW5kIGluc3RhbGw6PC9zdHJvbmc+XG4gICAgICogICAgIDxhIGhyZWY9XCJodHRwczovL2dpdGh1Yi5jb20vaGdvZWJsL21vYmlsZS1kZXRlY3QuanMvXCI+Z2l0aHViLmNvbS9oZ29lYmwvbW9iaWxlLWRldGVjdC5qcy88L2E+XG4gICAgICogPC9kaXY+XG4gICAgICpcbiAgICAgKiBAZXhhbXBsZSA8cHJlPlxuICAgICAqICAgICB2YXIgbWQgPSBuZXcgTW9iaWxlRGV0ZWN0KHdpbmRvdy5uYXZpZ2F0b3IudXNlckFnZW50KTtcbiAgICAgKiAgICAgaWYgKG1kLm1vYmlsZSgpKSB7XG4gICAgICogICAgICAgICBsb2NhdGlvbi5ocmVmID0gKG1kLm1vYmlsZUdyYWRlKCkgPT09ICdBJykgPyAnL21vYmlsZS8nIDogJy9seW54Lyc7XG4gICAgICogICAgIH1cbiAgICAgKiA8L3ByZT5cbiAgICAgKlxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSB1c2VyQWdlbnQgdHlwaWNhbGx5IHRha2VuIGZyb20gd2luZG93Lm5hdmlnYXRvci51c2VyQWdlbnQgb3IgaHR0cF9oZWFkZXJbJ1VzZXItQWdlbnQnXVxuICAgICAqIEBwYXJhbSB7bnVtYmVyfSBbbWF4UGhvbmVXaWR0aD02MDBdIDxzdHJvbmc+b25seSBmb3IgYnJvd3NlcnM8L3N0cm9uZz4gc3BlY2lmeSBhIHZhbHVlIGZvciB0aGUgbWF4aW11bVxuICAgICAqICAgICAgICB3aWR0aCBvZiBzbWFsbGVzdCBkZXZpY2Ugc2lkZSAoaW4gbG9naWNhbCBcIkNTU1wiIHBpeGVscykgdW50aWwgYSBkZXZpY2UgZGV0ZWN0ZWQgYXMgbW9iaWxlIHdpbGwgYmUgaGFuZGxlZFxuICAgICAqICAgICAgICBhcyBwaG9uZS5cbiAgICAgKiAgICAgICAgVGhpcyBpcyBvbmx5IHVzZWQgaW4gY2FzZXMgd2hlcmUgdGhlIGRldmljZSBjYW5ub3QgYmUgY2xhc3NpZmllZCBhcyBwaG9uZSBvciB0YWJsZXQuPGJyPlxuICAgICAqICAgICAgICBTZWUgPGEgaHJlZj1cImh0dHA6Ly9kZXZlbG9wZXIuYW5kcm9pZC5jb20vZ3VpZGUvcHJhY3RpY2VzL3NjcmVlbnNfc3VwcG9ydC5odG1sXCI+RGVjbGFyaW5nIFRhYmxldCBMYXlvdXRzXG4gICAgICogICAgICAgIGZvciBBbmRyb2lkPC9hPi48YnI+XG4gICAgICogICAgICAgIElmIHlvdSBwcm92aWRlIGEgdmFsdWUgPCAwLCB0aGVuIHRoaXMgXCJmdXp6eVwiIGNoZWNrIGlzIGRpc2FibGVkLlxuICAgICAqIEBjb25zdHJ1Y3RvclxuICAgICAqIEBnbG9iYWxcbiAgICAgKi9cbiAgICBmdW5jdGlvbiBNb2JpbGVEZXRlY3QodXNlckFnZW50LCBtYXhQaG9uZVdpZHRoKSB7XG4gICAgICAgIHRoaXMudWEgPSBwcmVwYXJlVXNlckFnZW50KHVzZXJBZ2VudCk7XG4gICAgICAgIHRoaXMuX2NhY2hlID0ge307XG4gICAgICAgIC8vNjAwZHAgaXMgdHlwaWNhbCA3XCIgdGFibGV0IG1pbmltdW0gd2lkdGhcbiAgICAgICAgdGhpcy5tYXhQaG9uZVdpZHRoID0gbWF4UGhvbmVXaWR0aCB8fCA2MDA7XG4gICAgfVxuXG4gICAgTW9iaWxlRGV0ZWN0LnByb3RvdHlwZSA9IHtcbiAgICAgICAgY29uc3RydWN0b3I6IE1vYmlsZURldGVjdCxcblxuICAgICAgICAvKipcbiAgICAgICAgICogUmV0dXJucyB0aGUgZGV0ZWN0ZWQgcGhvbmUgb3IgdGFibGV0IHR5cGUgb3IgPHR0Pm51bGw8L3R0PiBpZiBpdCBpcyBub3QgYSBtb2JpbGUgZGV2aWNlLlxuICAgICAgICAgKiA8YnI+XG4gICAgICAgICAqIEZvciBhIGxpc3Qgb2YgcG9zc2libGUgcmV0dXJuIHZhbHVlcyBzZWUge0BsaW5rIE1vYmlsZURldGVjdCNwaG9uZX0gYW5kIHtAbGluayBNb2JpbGVEZXRlY3QjdGFibGV0fS48YnI+XG4gICAgICAgICAqIDxicj5cbiAgICAgICAgICogSWYgdGhlIGRldmljZSBpcyBub3QgZGV0ZWN0ZWQgYnkgdGhlIHJlZ3VsYXIgZXhwcmVzc2lvbnMgZnJvbSBNb2JpbGUtRGV0ZWN0LCBhIHRlc3QgaXMgbWFkZSBhZ2FpbnN0XG4gICAgICAgICAqIHRoZSBwYXR0ZXJucyBvZiA8YSBocmVmPVwiaHR0cDovL2RldGVjdG1vYmlsZWJyb3dzZXJzLmNvbS9cIj5kZXRlY3Rtb2JpbGVicm93c2Vycy5jb208L2E+LiBJZiB0aGlzIHRlc3RcbiAgICAgICAgICogaXMgcG9zaXRpdmUsIGEgdmFsdWUgb2YgPGNvZGU+VW5rbm93blBob25lPC9jb2RlPiwgPGNvZGU+VW5rbm93blRhYmxldDwvY29kZT4gb3JcbiAgICAgICAgICogPGNvZGU+VW5rbm93bk1vYmlsZTwvY29kZT4gaXMgcmV0dXJuZWQuPGJyPlxuICAgICAgICAgKiBXaGVuIHVzZWQgaW4gYnJvd3NlciwgdGhlIGRlY2lzaW9uIHdoZXRoZXIgcGhvbmUgb3IgdGFibGV0IGlzIG1hZGUgYmFzZWQgb24gPGNvZGU+c2NyZWVuLndpZHRoL2hlaWdodDwvY29kZT4uPGJyPlxuICAgICAgICAgKiA8YnI+XG4gICAgICAgICAqIFdoZW4gdXNlZCBzZXJ2ZXItc2lkZSAobm9kZS5qcyksIHRoZXJlIGlzIG5vIHdheSB0byB0ZWxsIHRoZSBkaWZmZXJlbmNlIGJldHdlZW4gPGNvZGU+VW5rbm93blRhYmxldDwvY29kZT5cbiAgICAgICAgICogYW5kIDxjb2RlPlVua25vd25Nb2JpbGU8L2NvZGU+LCBzbyB5b3Ugd2lsbCBnZXQgPGNvZGU+VW5rbm93bk1vYmlsZTwvY29kZT4gaGVyZS48YnI+XG4gICAgICAgICAqIEJlIGF3YXJlIHRoYXQgc2luY2UgdjEuMC4wIGluIHRoaXMgc3BlY2lhbCBjYXNlIHlvdSB3aWxsIGdldCA8Y29kZT5Vbmtub3duTW9iaWxlPC9jb2RlPiBvbmx5IGZvcjpcbiAgICAgICAgICoge0BsaW5rIE1vYmlsZURldGVjdCNtb2JpbGV9LCBub3QgZm9yIHtAbGluayBNb2JpbGVEZXRlY3QjcGhvbmV9IGFuZCB7QGxpbmsgTW9iaWxlRGV0ZWN0I3RhYmxldH0uXG4gICAgICAgICAqIEluIHZlcnNpb25zIGJlZm9yZSB2MS4wLjAgYWxsIDMgbWV0aG9kcyByZXR1cm5lZCA8Y29kZT5Vbmtub3duTW9iaWxlPC9jb2RlPiB3aGljaCB3YXMgdGVkaW91cyB0byB1c2UuXG4gICAgICAgICAqIDxicj5cbiAgICAgICAgICogSW4gbW9zdCBjYXNlcyB5b3Ugd2lsbCB1c2UgdGhlIHJldHVybiB2YWx1ZSBqdXN0IGFzIGEgYm9vbGVhbi5cbiAgICAgICAgICpcbiAgICAgICAgICogQHJldHVybnMge1N0cmluZ30gdGhlIGtleSBmb3IgdGhlIHBob25lIGZhbWlseSBvciB0YWJsZXQgZmFtaWx5LCBlLmcuIFwiTmV4dXNcIi5cbiAgICAgICAgICogQGZ1bmN0aW9uIE1vYmlsZURldGVjdCNtb2JpbGVcbiAgICAgICAgICovXG4gICAgICAgIG1vYmlsZTogZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgaW1wbC5wcmVwYXJlRGV0ZWN0aW9uQ2FjaGUodGhpcy5fY2FjaGUsIHRoaXMudWEsIHRoaXMubWF4UGhvbmVXaWR0aCk7XG4gICAgICAgICAgICByZXR1cm4gdGhpcy5fY2FjaGUubW9iaWxlO1xuICAgICAgICB9LFxuXG4gICAgICAgIC8qKlxuICAgICAgICAgKiBSZXR1cm5zIHRoZSBkZXRlY3RlZCBwaG9uZSB0eXBlL2ZhbWlseSBzdHJpbmcgb3IgPHR0Pm51bGw8L3R0Pi5cbiAgICAgICAgICogPGJyPlxuICAgICAgICAgKiBUaGUgcmV0dXJuZWQgdGFibGV0IChmYW1pbHkgb3IgcHJvZHVjZXIpIGlzIG9uZSBvZiBmb2xsb3dpbmcga2V5czo8YnI+XG4gICAgICAgICAqIDxicj48dHQ+aVBob25lLCBCbGFja0JlcnJ5LCBIVEMsIE5leHVzLCBEZWxsLCBNb3Rvcm9sYSwgU2Ftc3VuZywgTEcsIFNvbnksIEFzdXMsXG4gICAgICAgICAqIE5va2lhTHVtaWEsIE1pY3JvbWF4LCBQYWxtLCBWZXJ0dSwgUGFudGVjaCwgRmx5LCBXaWtvLCBpTW9iaWxlLCBTaW1WYWxsZXksXG4gICAgICAgICAqIFdvbGZnYW5nLCBBbGNhdGVsLCBOaW50ZW5kbywgQW1vaSwgSU5RLCBPbmVQbHVzLCBHZW5lcmljUGhvbmU8L3R0Pjxicj5cbiAgICAgICAgICogPGJyPlxuICAgICAgICAgKiBJZiB0aGUgZGV2aWNlIGlzIG5vdCBkZXRlY3RlZCBieSB0aGUgcmVndWxhciBleHByZXNzaW9ucyBmcm9tIE1vYmlsZS1EZXRlY3QsIGEgdGVzdCBpcyBtYWRlIGFnYWluc3RcbiAgICAgICAgICogdGhlIHBhdHRlcm5zIG9mIDxhIGhyZWY9XCJodHRwOi8vZGV0ZWN0bW9iaWxlYnJvd3NlcnMuY29tL1wiPmRldGVjdG1vYmlsZWJyb3dzZXJzLmNvbTwvYT4uIElmIHRoaXMgdGVzdFxuICAgICAgICAgKiBpcyBwb3NpdGl2ZSwgYSB2YWx1ZSBvZiA8Y29kZT5Vbmtub3duUGhvbmU8L2NvZGU+IG9yIDxjb2RlPlVua25vd25Nb2JpbGU8L2NvZGU+IGlzIHJldHVybmVkLjxicj5cbiAgICAgICAgICogV2hlbiB1c2VkIGluIGJyb3dzZXIsIHRoZSBkZWNpc2lvbiB3aGV0aGVyIHBob25lIG9yIHRhYmxldCBpcyBtYWRlIGJhc2VkIG9uIDxjb2RlPnNjcmVlbi53aWR0aC9oZWlnaHQ8L2NvZGU+Ljxicj5cbiAgICAgICAgICogPGJyPlxuICAgICAgICAgKiBXaGVuIHVzZWQgc2VydmVyLXNpZGUgKG5vZGUuanMpLCB0aGVyZSBpcyBubyB3YXkgdG8gdGVsbCB0aGUgZGlmZmVyZW5jZSBiZXR3ZWVuIDxjb2RlPlVua25vd25UYWJsZXQ8L2NvZGU+XG4gICAgICAgICAqIGFuZCA8Y29kZT5Vbmtub3duTW9iaWxlPC9jb2RlPiwgc28geW91IHdpbGwgZ2V0IDxjb2RlPm51bGw8L2NvZGU+IGhlcmUsIHdoaWxlIHtAbGluayBNb2JpbGVEZXRlY3QjbW9iaWxlfVxuICAgICAgICAgKiB3aWxsIHJldHVybiA8Y29kZT5Vbmtub3duTW9iaWxlPC9jb2RlPi48YnI+XG4gICAgICAgICAqIEJlIGF3YXJlIHRoYXQgc2luY2UgdjEuMC4wIGluIHRoaXMgc3BlY2lhbCBjYXNlIHlvdSB3aWxsIGdldCA8Y29kZT5Vbmtub3duTW9iaWxlPC9jb2RlPiBvbmx5IGZvcjpcbiAgICAgICAgICoge0BsaW5rIE1vYmlsZURldGVjdCNtb2JpbGV9LCBub3QgZm9yIHtAbGluayBNb2JpbGVEZXRlY3QjcGhvbmV9IGFuZCB7QGxpbmsgTW9iaWxlRGV0ZWN0I3RhYmxldH0uXG4gICAgICAgICAqIEluIHZlcnNpb25zIGJlZm9yZSB2MS4wLjAgYWxsIDMgbWV0aG9kcyByZXR1cm5lZCA8Y29kZT5Vbmtub3duTW9iaWxlPC9jb2RlPiB3aGljaCB3YXMgdGVkaW91cyB0byB1c2UuXG4gICAgICAgICAqIDxicj5cbiAgICAgICAgICogSW4gbW9zdCBjYXNlcyB5b3Ugd2lsbCB1c2UgdGhlIHJldHVybiB2YWx1ZSBqdXN0IGFzIGEgYm9vbGVhbi5cbiAgICAgICAgICpcbiAgICAgICAgICogQHJldHVybnMge1N0cmluZ30gdGhlIGtleSBvZiB0aGUgcGhvbmUgZmFtaWx5IG9yIHByb2R1Y2VyLCBlLmcuIFwiaVBob25lXCJcbiAgICAgICAgICogQGZ1bmN0aW9uIE1vYmlsZURldGVjdCNwaG9uZVxuICAgICAgICAgKi9cbiAgICAgICAgcGhvbmU6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIGltcGwucHJlcGFyZURldGVjdGlvbkNhY2hlKHRoaXMuX2NhY2hlLCB0aGlzLnVhLCB0aGlzLm1heFBob25lV2lkdGgpO1xuICAgICAgICAgICAgcmV0dXJuIHRoaXMuX2NhY2hlLnBob25lO1xuICAgICAgICB9LFxuXG4gICAgICAgIC8qKlxuICAgICAgICAgKiBSZXR1cm5zIHRoZSBkZXRlY3RlZCB0YWJsZXQgdHlwZS9mYW1pbHkgc3RyaW5nIG9yIDx0dD5udWxsPC90dD4uXG4gICAgICAgICAqIDxicj5cbiAgICAgICAgICogVGhlIHJldHVybmVkIHRhYmxldCAoZmFtaWx5IG9yIHByb2R1Y2VyKSBpcyBvbmUgb2YgZm9sbG93aW5nIGtleXM6PGJyPlxuICAgICAgICAgKiA8YnI+PHR0PmlQYWQsIE5leHVzVGFibGV0LCBHb29nbGVUYWJsZXQsIFNhbXN1bmdUYWJsZXQsIEtpbmRsZSwgU3VyZmFjZVRhYmxldCxcbiAgICAgICAgICogSFBUYWJsZXQsIEFzdXNUYWJsZXQsIEJsYWNrQmVycnlUYWJsZXQsIEhUQ3RhYmxldCwgTW90b3JvbGFUYWJsZXQsIE5vb2tUYWJsZXQsXG4gICAgICAgICAqIEFjZXJUYWJsZXQsIFRvc2hpYmFUYWJsZXQsIExHVGFibGV0LCBGdWppdHN1VGFibGV0LCBQcmVzdGlnaW9UYWJsZXQsXG4gICAgICAgICAqIExlbm92b1RhYmxldCwgRGVsbFRhYmxldCwgWWFydmlrVGFibGV0LCBNZWRpb25UYWJsZXQsIEFybm92YVRhYmxldCxcbiAgICAgICAgICogSW50ZW5zb1RhYmxldCwgSVJVVGFibGV0LCBNZWdhZm9uVGFibGV0LCBFYm9kYVRhYmxldCwgQWxsVmlld1RhYmxldCxcbiAgICAgICAgICogQXJjaG9zVGFibGV0LCBBaW5vbFRhYmxldCwgTm9raWFMdW1pYVRhYmxldCwgU29ueVRhYmxldCwgUGhpbGlwc1RhYmxldCxcbiAgICAgICAgICogQ3ViZVRhYmxldCwgQ29ieVRhYmxldCwgTUlEVGFibGV0LCBNU0lUYWJsZXQsIFNNaVRUYWJsZXQsIFJvY2tDaGlwVGFibGV0LFxuICAgICAgICAgKiBGbHlUYWJsZXQsIGJxVGFibGV0LCBIdWF3ZWlUYWJsZXQsIE5lY1RhYmxldCwgUGFudGVjaFRhYmxldCwgQnJvbmNob1RhYmxldCxcbiAgICAgICAgICogVmVyc3VzVGFibGV0LCBaeW5jVGFibGV0LCBQb3NpdGl2b1RhYmxldCwgTmFiaVRhYmxldCwgS29ib1RhYmxldCwgRGFuZXdUYWJsZXQsXG4gICAgICAgICAqIFRleGV0VGFibGV0LCBQbGF5c3RhdGlvblRhYmxldCwgVHJla3N0b3JUYWJsZXQsIFB5bGVBdWRpb1RhYmxldCwgQWR2YW5UYWJsZXQsXG4gICAgICAgICAqIERhbnlUZWNoVGFibGV0LCBHYWxhcGFkVGFibGV0LCBNaWNyb21heFRhYmxldCwgS2FyYm9ublRhYmxldCwgQWxsRmluZVRhYmxldCxcbiAgICAgICAgICogUFJPU0NBTlRhYmxldCwgWU9ORVNUYWJsZXQsIENoYW5nSmlhVGFibGV0LCBHVVRhYmxldCwgUG9pbnRPZlZpZXdUYWJsZXQsXG4gICAgICAgICAqIE92ZXJtYXhUYWJsZXQsIEhDTFRhYmxldCwgRFBTVGFibGV0LCBWaXN0dXJlVGFibGV0LCBDcmVzdGFUYWJsZXQsXG4gICAgICAgICAqIE1lZGlhdGVrVGFibGV0LCBDb25jb3JkZVRhYmxldCwgR29DbGV2ZXJUYWJsZXQsIE1vZGVjb21UYWJsZXQsIFZvbmlub1RhYmxldCxcbiAgICAgICAgICogRUNTVGFibGV0LCBTdG9yZXhUYWJsZXQsIFZvZGFmb25lVGFibGV0LCBFc3NlbnRpZWxCVGFibGV0LCBSb3NzTW9vclRhYmxldCxcbiAgICAgICAgICogaU1vYmlsZVRhYmxldCwgVG9saW5vVGFibGV0LCBBdWRpb1NvbmljVGFibGV0LCBBTVBFVGFibGV0LCBTa2tUYWJsZXQsXG4gICAgICAgICAqIFRlY25vVGFibGV0LCBKWERUYWJsZXQsIGlKb3lUYWJsZXQsIEZYMlRhYmxldCwgWG9yb1RhYmxldCwgVmlld3NvbmljVGFibGV0LFxuICAgICAgICAgKiBWZXJpem9uVGFibGV0LCBPZHlzVGFibGV0LCBDYXB0aXZhVGFibGV0LCBJY29uYml0VGFibGV0LCBUZWNsYXN0VGFibGV0LFxuICAgICAgICAgKiBPbmRhVGFibGV0LCBKYXl0ZWNoVGFibGV0LCBCbGF1cHVua3RUYWJsZXQsIERpZ21hVGFibGV0LCBFdm9saW9UYWJsZXQsXG4gICAgICAgICAqIExhdmFUYWJsZXQsIEFvY1RhYmxldCwgTXBtYW5UYWJsZXQsIENlbGtvblRhYmxldCwgV29sZGVyVGFibGV0LCBNZWRpYWNvbVRhYmxldCxcbiAgICAgICAgICogTWlUYWJsZXQsIE5pYmlydVRhYmxldCwgTmV4b1RhYmxldCwgTGVhZGVyVGFibGV0LCBVYmlzbGF0ZVRhYmxldCxcbiAgICAgICAgICogUG9ja2V0Qm9va1RhYmxldCwgS29jYXNvVGFibGV0LCBIaXNlbnNlVGFibGV0LCBIdWRsLCBUZWxzdHJhVGFibGV0LFxuICAgICAgICAgKiBHZW5lcmljVGFibGV0PC90dD48YnI+XG4gICAgICAgICAqIDxicj5cbiAgICAgICAgICogSWYgdGhlIGRldmljZSBpcyBub3QgZGV0ZWN0ZWQgYnkgdGhlIHJlZ3VsYXIgZXhwcmVzc2lvbnMgZnJvbSBNb2JpbGUtRGV0ZWN0LCBhIHRlc3QgaXMgbWFkZSBhZ2FpbnN0XG4gICAgICAgICAqIHRoZSBwYXR0ZXJucyBvZiA8YSBocmVmPVwiaHR0cDovL2RldGVjdG1vYmlsZWJyb3dzZXJzLmNvbS9cIj5kZXRlY3Rtb2JpbGVicm93c2Vycy5jb208L2E+LiBJZiB0aGlzIHRlc3RcbiAgICAgICAgICogaXMgcG9zaXRpdmUsIGEgdmFsdWUgb2YgPGNvZGU+VW5rbm93blRhYmxldDwvY29kZT4gb3IgPGNvZGU+VW5rbm93bk1vYmlsZTwvY29kZT4gaXMgcmV0dXJuZWQuPGJyPlxuICAgICAgICAgKiBXaGVuIHVzZWQgaW4gYnJvd3NlciwgdGhlIGRlY2lzaW9uIHdoZXRoZXIgcGhvbmUgb3IgdGFibGV0IGlzIG1hZGUgYmFzZWQgb24gPGNvZGU+c2NyZWVuLndpZHRoL2hlaWdodDwvY29kZT4uPGJyPlxuICAgICAgICAgKiA8YnI+XG4gICAgICAgICAqIFdoZW4gdXNlZCBzZXJ2ZXItc2lkZSAobm9kZS5qcyksIHRoZXJlIGlzIG5vIHdheSB0byB0ZWxsIHRoZSBkaWZmZXJlbmNlIGJldHdlZW4gPGNvZGU+VW5rbm93blRhYmxldDwvY29kZT5cbiAgICAgICAgICogYW5kIDxjb2RlPlVua25vd25Nb2JpbGU8L2NvZGU+LCBzbyB5b3Ugd2lsbCBnZXQgPGNvZGU+bnVsbDwvY29kZT4gaGVyZSwgd2hpbGUge0BsaW5rIE1vYmlsZURldGVjdCNtb2JpbGV9XG4gICAgICAgICAqIHdpbGwgcmV0dXJuIDxjb2RlPlVua25vd25Nb2JpbGU8L2NvZGU+Ljxicj5cbiAgICAgICAgICogQmUgYXdhcmUgdGhhdCBzaW5jZSB2MS4wLjAgaW4gdGhpcyBzcGVjaWFsIGNhc2UgeW91IHdpbGwgZ2V0IDxjb2RlPlVua25vd25Nb2JpbGU8L2NvZGU+IG9ubHkgZm9yOlxuICAgICAgICAgKiB7QGxpbmsgTW9iaWxlRGV0ZWN0I21vYmlsZX0sIG5vdCBmb3Ige0BsaW5rIE1vYmlsZURldGVjdCNwaG9uZX0gYW5kIHtAbGluayBNb2JpbGVEZXRlY3QjdGFibGV0fS5cbiAgICAgICAgICogSW4gdmVyc2lvbnMgYmVmb3JlIHYxLjAuMCBhbGwgMyBtZXRob2RzIHJldHVybmVkIDxjb2RlPlVua25vd25Nb2JpbGU8L2NvZGU+IHdoaWNoIHdhcyB0ZWRpb3VzIHRvIHVzZS5cbiAgICAgICAgICogPGJyPlxuICAgICAgICAgKiBJbiBtb3N0IGNhc2VzIHlvdSB3aWxsIHVzZSB0aGUgcmV0dXJuIHZhbHVlIGp1c3QgYXMgYSBib29sZWFuLlxuICAgICAgICAgKlxuICAgICAgICAgKiBAcmV0dXJucyB7U3RyaW5nfSB0aGUga2V5IG9mIHRoZSB0YWJsZXQgZmFtaWx5IG9yIHByb2R1Y2VyLCBlLmcuIFwiU2Ftc3VuZ1RhYmxldFwiXG4gICAgICAgICAqIEBmdW5jdGlvbiBNb2JpbGVEZXRlY3QjdGFibGV0XG4gICAgICAgICAqL1xuICAgICAgICB0YWJsZXQ6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIGltcGwucHJlcGFyZURldGVjdGlvbkNhY2hlKHRoaXMuX2NhY2hlLCB0aGlzLnVhLCB0aGlzLm1heFBob25lV2lkdGgpO1xuICAgICAgICAgICAgcmV0dXJuIHRoaXMuX2NhY2hlLnRhYmxldDtcbiAgICAgICAgfSxcblxuICAgICAgICAvKipcbiAgICAgICAgICogUmV0dXJucyB0aGUgKGZpcnN0KSBkZXRlY3RlZCB1c2VyLWFnZW50IHN0cmluZyBvciA8dHQ+bnVsbDwvdHQ+LlxuICAgICAgICAgKiA8YnI+XG4gICAgICAgICAqIFRoZSByZXR1cm5lZCB1c2VyLWFnZW50IGlzIG9uZSBvZiBmb2xsb3dpbmcga2V5czo8YnI+XG4gICAgICAgICAqIDxicj48dHQ+Q2hyb21lLCBEb2xmaW4sIE9wZXJhLCBTa3lmaXJlLCBFZGdlLCBJRSwgRmlyZWZveCwgQm9sdCwgVGVhU2hhcmssIEJsYXplcixcbiAgICAgICAgICogU2FmYXJpLCBXZUNoYXQsIFVDQnJvd3NlciwgYmFpZHVib3hhcHAsIGJhaWR1YnJvd3NlciwgRGlpZ29Ccm93c2VyLCBNZXJjdXJ5LFxuICAgICAgICAgKiBPYmlnb0Jyb3dzZXIsIE5ldEZyb250LCBHZW5lcmljQnJvd3NlciwgUGFsZU1vb248L3R0Pjxicj5cbiAgICAgICAgICogPGJyPlxuICAgICAgICAgKiBJbiBtb3N0IGNhc2VzIGNhbGxpbmcge0BsaW5rIE1vYmlsZURldGVjdCN1c2VyQWdlbnR9IHdpbGwgYmUgc3VmZmljaWVudC4gQnV0IHRoZXJlIGFyZSByYXJlXG4gICAgICAgICAqIGNhc2VzIHdoZXJlIGEgbW9iaWxlIGRldmljZSBwcmV0ZW5kcyB0byBiZSBtb3JlIHRoYW4gb25lIHBhcnRpY3VsYXIgYnJvd3Nlci4gWW91IGNhbiBnZXQgdGhlXG4gICAgICAgICAqIGxpc3Qgb2YgYWxsIG1hdGNoZXMgd2l0aCB7QGxpbmsgTW9iaWxlRGV0ZWN0I3VzZXJBZ2VudHN9IG9yIGNoZWNrIGZvciBhIHBhcnRpY3VsYXIgdmFsdWUgYnlcbiAgICAgICAgICogcHJvdmlkaW5nIG9uZSBvZiB0aGUgZGVmaW5lZCBrZXlzIGFzIGZpcnN0IGFyZ3VtZW50IHRvIHtAbGluayBNb2JpbGVEZXRlY3QjaXN9LlxuICAgICAgICAgKlxuICAgICAgICAgKiBAcmV0dXJucyB7U3RyaW5nfSB0aGUga2V5IGZvciB0aGUgZGV0ZWN0ZWQgdXNlci1hZ2VudCBvciA8dHQ+bnVsbDwvdHQ+XG4gICAgICAgICAqIEBmdW5jdGlvbiBNb2JpbGVEZXRlY3QjdXNlckFnZW50XG4gICAgICAgICAqL1xuICAgICAgICB1c2VyQWdlbnQ6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIGlmICh0aGlzLl9jYWNoZS51c2VyQWdlbnQgPT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgICAgIHRoaXMuX2NhY2hlLnVzZXJBZ2VudCA9IGltcGwuZmluZE1hdGNoKGltcGwubW9iaWxlRGV0ZWN0UnVsZXMudWFzLCB0aGlzLnVhKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybiB0aGlzLl9jYWNoZS51c2VyQWdlbnQ7XG4gICAgICAgIH0sXG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqIFJldHVybnMgYWxsIGRldGVjdGVkIHVzZXItYWdlbnQgc3RyaW5ncy5cbiAgICAgICAgICogPGJyPlxuICAgICAgICAgKiBUaGUgYXJyYXkgaXMgZW1wdHkgb3IgY29udGFpbnMgb25lIG9yIG1vcmUgb2YgZm9sbG93aW5nIGtleXM6PGJyPlxuICAgICAgICAgKiA8YnI+PHR0PkNocm9tZSwgRG9sZmluLCBPcGVyYSwgU2t5ZmlyZSwgRWRnZSwgSUUsIEZpcmVmb3gsIEJvbHQsIFRlYVNoYXJrLCBCbGF6ZXIsXG4gICAgICAgICAqIFNhZmFyaSwgV2VDaGF0LCBVQ0Jyb3dzZXIsIGJhaWR1Ym94YXBwLCBiYWlkdWJyb3dzZXIsIERpaWdvQnJvd3NlciwgTWVyY3VyeSxcbiAgICAgICAgICogT2JpZ29Ccm93c2VyLCBOZXRGcm9udCwgR2VuZXJpY0Jyb3dzZXIsIFBhbGVNb29uPC90dD48YnI+XG4gICAgICAgICAqIDxicj5cbiAgICAgICAgICogSW4gbW9zdCBjYXNlcyBjYWxsaW5nIHtAbGluayBNb2JpbGVEZXRlY3QjdXNlckFnZW50fSB3aWxsIGJlIHN1ZmZpY2llbnQuIEJ1dCB0aGVyZSBhcmUgcmFyZVxuICAgICAgICAgKiBjYXNlcyB3aGVyZSBhIG1vYmlsZSBkZXZpY2UgcHJldGVuZHMgdG8gYmUgbW9yZSB0aGFuIG9uZSBwYXJ0aWN1bGFyIGJyb3dzZXIuIFlvdSBjYW4gZ2V0IHRoZVxuICAgICAgICAgKiBsaXN0IG9mIGFsbCBtYXRjaGVzIHdpdGgge0BsaW5rIE1vYmlsZURldGVjdCN1c2VyQWdlbnRzfSBvciBjaGVjayBmb3IgYSBwYXJ0aWN1bGFyIHZhbHVlIGJ5XG4gICAgICAgICAqIHByb3ZpZGluZyBvbmUgb2YgdGhlIGRlZmluZWQga2V5cyBhcyBmaXJzdCBhcmd1bWVudCB0byB7QGxpbmsgTW9iaWxlRGV0ZWN0I2lzfS5cbiAgICAgICAgICpcbiAgICAgICAgICogQHJldHVybnMge0FycmF5fSB0aGUgYXJyYXkgb2YgZGV0ZWN0ZWQgdXNlci1hZ2VudCBrZXlzIG9yIDx0dD5bXTwvdHQ+XG4gICAgICAgICAqIEBmdW5jdGlvbiBNb2JpbGVEZXRlY3QjdXNlckFnZW50c1xuICAgICAgICAgKi9cbiAgICAgICAgdXNlckFnZW50czogZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgaWYgKHRoaXMuX2NhY2hlLnVzZXJBZ2VudHMgPT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgICAgIHRoaXMuX2NhY2hlLnVzZXJBZ2VudHMgPSBpbXBsLmZpbmRNYXRjaGVzKGltcGwubW9iaWxlRGV0ZWN0UnVsZXMudWFzLCB0aGlzLnVhKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybiB0aGlzLl9jYWNoZS51c2VyQWdlbnRzO1xuICAgICAgICB9LFxuXG4gICAgICAgIC8qKlxuICAgICAgICAgKiBSZXR1cm5zIHRoZSBkZXRlY3RlZCBvcGVyYXRpbmcgc3lzdGVtIHN0cmluZyBvciA8dHQ+bnVsbDwvdHQ+LlxuICAgICAgICAgKiA8YnI+XG4gICAgICAgICAqIFRoZSBvcGVyYXRpbmcgc3lzdGVtIGlzIG9uZSBvZiBmb2xsb3dpbmcga2V5czo8YnI+XG4gICAgICAgICAqIDxicj48dHQ+QW5kcm9pZE9TLCBCbGFja0JlcnJ5T1MsIFBhbG1PUywgU3ltYmlhbk9TLCBXaW5kb3dzTW9iaWxlT1MsIFdpbmRvd3NQaG9uZU9TLFxuICAgICAgICAgKiBpT1MsIGlQYWRPUywgTWVlR29PUywgTWFlbW9PUywgSmF2YU9TLCB3ZWJPUywgYmFkYU9TLCBCUkVXT1M8L3R0Pjxicj5cbiAgICAgICAgICpcbiAgICAgICAgICogQHJldHVybnMge1N0cmluZ30gdGhlIGtleSBmb3IgdGhlIGRldGVjdGVkIG9wZXJhdGluZyBzeXN0ZW0uXG4gICAgICAgICAqIEBmdW5jdGlvbiBNb2JpbGVEZXRlY3Qjb3NcbiAgICAgICAgICovXG4gICAgICAgIG9zOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICBpZiAodGhpcy5fY2FjaGUub3MgPT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgICAgIHRoaXMuX2NhY2hlLm9zID0gaW1wbC5kZXRlY3RPUyh0aGlzLnVhKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybiB0aGlzLl9jYWNoZS5vcztcbiAgICAgICAgfSxcblxuICAgICAgICAvKipcbiAgICAgICAgICogR2V0IHRoZSB2ZXJzaW9uIChhcyBOdW1iZXIpIG9mIHRoZSBnaXZlbiBwcm9wZXJ0eSBpbiB0aGUgVXNlci1BZ2VudC5cbiAgICAgICAgICogPGJyPlxuICAgICAgICAgKiBXaWxsIHJldHVybiBhIGZsb2F0IG51bWJlci4gKGVnLiAyXzAgd2lsbCByZXR1cm4gMi4wLCA0LjMuMSB3aWxsIHJldHVybiA0LjMxKVxuICAgICAgICAgKlxuICAgICAgICAgKiBAcGFyYW0ge1N0cmluZ30ga2V5IGEga2V5IGRlZmluaW5nIGEgdGhpbmcgd2hpY2ggaGFzIGEgdmVyc2lvbi48YnI+XG4gICAgICAgICAqICAgICAgICBZb3UgY2FuIHVzZSBvbmUgb2YgZm9sbG93aW5nIGtleXM6PGJyPlxuICAgICAgICAgKiA8YnI+PHR0Pk1vYmlsZSwgQnVpbGQsIFZlcnNpb24sIFZlbmRvcklELCBpUGFkLCBpUGhvbmUsIGlQb2QsIEtpbmRsZSwgQ2hyb21lLCBDb2FzdCxcbiAgICAgICAgICogRG9sZmluLCBGaXJlZm94LCBGZW5uZWMsIEVkZ2UsIElFLCBOZXRGcm9udCwgTm9raWFCcm93c2VyLCBPcGVyYSwgT3BlcmEgTWluaSxcbiAgICAgICAgICogT3BlcmEgTW9iaSwgVUNCcm93c2VyLCBNUVFCcm93c2VyLCBNaWNyb01lc3NlbmdlciwgYmFpZHVib3hhcHAsIGJhaWR1YnJvd3NlcixcbiAgICAgICAgICogU2Ftc3VuZ0Jyb3dzZXIsIElyb24sIFNhZmFyaSwgU2t5ZmlyZSwgVGl6ZW4sIFdlYmtpdCwgUGFsZU1vb24sIEdlY2tvLCBUcmlkZW50LFxuICAgICAgICAgKiBQcmVzdG8sIEdvYW5uYSwgaU9TLCBBbmRyb2lkLCBCbGFja0JlcnJ5LCBCUkVXLCBKYXZhLCBXaW5kb3dzIFBob25lIE9TLCBXaW5kb3dzXG4gICAgICAgICAqIFBob25lLCBXaW5kb3dzIENFLCBXaW5kb3dzIE5ULCBTeW1iaWFuLCB3ZWJPUzwvdHQ+PGJyPlxuICAgICAgICAgKlxuICAgICAgICAgKiBAcmV0dXJucyB7TnVtYmVyfSB0aGUgdmVyc2lvbiBhcyBmbG9hdCBvciA8dHQ+TmFOPC90dD4gaWYgVXNlci1BZ2VudCBkb2Vzbid0IGNvbnRhaW4gdGhpcyB2ZXJzaW9uLlxuICAgICAgICAgKiAgICAgICAgICBCZSBjYXJlZnVsIHdoZW4gY29tcGFyaW5nIHRoaXMgdmFsdWUgd2l0aCAnPT0nIG9wZXJhdG9yIVxuICAgICAgICAgKiBAZnVuY3Rpb24gTW9iaWxlRGV0ZWN0I3ZlcnNpb25cbiAgICAgICAgICovXG4gICAgICAgIHZlcnNpb246IGZ1bmN0aW9uIChrZXkpIHtcbiAgICAgICAgICAgIHJldHVybiBpbXBsLmdldFZlcnNpb24oa2V5LCB0aGlzLnVhKTtcbiAgICAgICAgfSxcblxuICAgICAgICAvKipcbiAgICAgICAgICogR2V0IHRoZSB2ZXJzaW9uIChhcyBTdHJpbmcpIG9mIHRoZSBnaXZlbiBwcm9wZXJ0eSBpbiB0aGUgVXNlci1BZ2VudC5cbiAgICAgICAgICogPGJyPlxuICAgICAgICAgKlxuICAgICAgICAgKiBAcGFyYW0ge1N0cmluZ30ga2V5IGEga2V5IGRlZmluaW5nIGEgdGhpbmcgd2hpY2ggaGFzIGEgdmVyc2lvbi48YnI+XG4gICAgICAgICAqICAgICAgICBZb3UgY2FuIHVzZSBvbmUgb2YgZm9sbG93aW5nIGtleXM6PGJyPlxuICAgICAgICAgKiA8YnI+PHR0Pk1vYmlsZSwgQnVpbGQsIFZlcnNpb24sIFZlbmRvcklELCBpUGFkLCBpUGhvbmUsIGlQb2QsIEtpbmRsZSwgQ2hyb21lLCBDb2FzdCxcbiAgICAgICAgICogRG9sZmluLCBGaXJlZm94LCBGZW5uZWMsIEVkZ2UsIElFLCBOZXRGcm9udCwgTm9raWFCcm93c2VyLCBPcGVyYSwgT3BlcmEgTWluaSxcbiAgICAgICAgICogT3BlcmEgTW9iaSwgVUNCcm93c2VyLCBNUVFCcm93c2VyLCBNaWNyb01lc3NlbmdlciwgYmFpZHVib3hhcHAsIGJhaWR1YnJvd3NlcixcbiAgICAgICAgICogU2Ftc3VuZ0Jyb3dzZXIsIElyb24sIFNhZmFyaSwgU2t5ZmlyZSwgVGl6ZW4sIFdlYmtpdCwgUGFsZU1vb24sIEdlY2tvLCBUcmlkZW50LFxuICAgICAgICAgKiBQcmVzdG8sIEdvYW5uYSwgaU9TLCBBbmRyb2lkLCBCbGFja0JlcnJ5LCBCUkVXLCBKYXZhLCBXaW5kb3dzIFBob25lIE9TLCBXaW5kb3dzXG4gICAgICAgICAqIFBob25lLCBXaW5kb3dzIENFLCBXaW5kb3dzIE5ULCBTeW1iaWFuLCB3ZWJPUzwvdHQ+PGJyPlxuICAgICAgICAgKlxuICAgICAgICAgKiBAcmV0dXJucyB7U3RyaW5nfSB0aGUgXCJyYXdcIiB2ZXJzaW9uIGFzIFN0cmluZyBvciA8dHQ+bnVsbDwvdHQ+IGlmIFVzZXItQWdlbnQgZG9lc24ndCBjb250YWluIHRoaXMgdmVyc2lvbi5cbiAgICAgICAgICpcbiAgICAgICAgICogQGZ1bmN0aW9uIE1vYmlsZURldGVjdCN2ZXJzaW9uU3RyXG4gICAgICAgICAqL1xuICAgICAgICB2ZXJzaW9uU3RyOiBmdW5jdGlvbiAoa2V5KSB7XG4gICAgICAgICAgICByZXR1cm4gaW1wbC5nZXRWZXJzaW9uU3RyKGtleSwgdGhpcy51YSk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqIEdsb2JhbCB0ZXN0IGtleSBhZ2FpbnN0IHVzZXJBZ2VudCwgb3MsIHBob25lLCB0YWJsZXQgYW5kIHNvbWUgb3RoZXIgcHJvcGVydGllcyBvZiB1c2VyQWdlbnQgc3RyaW5nLlxuICAgICAgICAgKlxuICAgICAgICAgKiBAcGFyYW0ge1N0cmluZ30ga2V5IHRoZSBrZXkgKGNhc2UtaW5zZW5zaXRpdmUpIG9mIGEgdXNlckFnZW50LCBhbiBvcGVyYXRpbmcgc3lzdGVtLCBwaG9uZSBvclxuICAgICAgICAgKiAgICAgICAgdGFibGV0IGZhbWlseS48YnI+XG4gICAgICAgICAqICAgICAgICBGb3IgYSBjb21wbGV0ZSBsaXN0IG9mIHBvc3NpYmxlIHZhbHVlcywgc2VlIHtAbGluayBNb2JpbGVEZXRlY3QjdXNlckFnZW50fSxcbiAgICAgICAgICogICAgICAgIHtAbGluayBNb2JpbGVEZXRlY3Qjb3N9LCB7QGxpbmsgTW9iaWxlRGV0ZWN0I3Bob25lfSwge0BsaW5rIE1vYmlsZURldGVjdCN0YWJsZXR9Ljxicj5cbiAgICAgICAgICogICAgICAgIEFkZGl0aW9uYWxseSB5b3UgaGF2ZSBmb2xsb3dpbmcga2V5czo8YnI+XG4gICAgICAgICAqIDxicj48dHQ+Qm90LCBNb2JpbGVCb3QsIERlc2t0b3BNb2RlLCBUViwgV2ViS2l0LCBDb25zb2xlLCBXYXRjaDwvdHQ+PGJyPlxuICAgICAgICAgKlxuICAgICAgICAgKiBAcmV0dXJucyB7Ym9vbGVhbn0gPHR0PnRydWU8L3R0PiB3aGVuIHRoZSBnaXZlbiBrZXkgaXMgb25lIG9mIHRoZSBkZWZpbmVkIGtleXMgb2YgdXNlckFnZW50LCBvcywgcGhvbmUsXG4gICAgICAgICAqICAgICAgICAgICAgICAgICAgICB0YWJsZXQgb3Igb25lIG9mIHRoZSBsaXN0ZWQgYWRkaXRpb25hbCBrZXlzLCBvdGhlcndpc2UgPHR0PmZhbHNlPC90dD5cbiAgICAgICAgICogQGZ1bmN0aW9uIE1vYmlsZURldGVjdCNpc1xuICAgICAgICAgKi9cbiAgICAgICAgaXM6IGZ1bmN0aW9uIChrZXkpIHtcbiAgICAgICAgICAgIHJldHVybiBjb250YWluc0lDKHRoaXMudXNlckFnZW50cygpLCBrZXkpIHx8XG4gICAgICAgICAgICAgICAgICAgZXF1YWxJQyhrZXksIHRoaXMub3MoKSkgfHxcbiAgICAgICAgICAgICAgICAgICBlcXVhbElDKGtleSwgdGhpcy5waG9uZSgpKSB8fFxuICAgICAgICAgICAgICAgICAgIGVxdWFsSUMoa2V5LCB0aGlzLnRhYmxldCgpKSB8fFxuICAgICAgICAgICAgICAgICAgIGNvbnRhaW5zSUMoaW1wbC5maW5kTWF0Y2hlcyhpbXBsLm1vYmlsZURldGVjdFJ1bGVzLnV0aWxzLCB0aGlzLnVhKSwga2V5KTtcbiAgICAgICAgfSxcblxuICAgICAgICAvKipcbiAgICAgICAgICogRG8gYSBxdWljayB0ZXN0IGFnYWluc3QgbmF2aWdhdG9yOjp1c2VyQWdlbnQuXG4gICAgICAgICAqXG4gICAgICAgICAqIEBwYXJhbSB7U3RyaW5nfFJlZ0V4cH0gcGF0dGVybiB0aGUgcGF0dGVybiwgZWl0aGVyIGFzIFN0cmluZyBvciBSZWdFeHBcbiAgICAgICAgICogICAgICAgICAgICAgICAgICAgICAgICAoYSBzdHJpbmcgd2lsbCBiZSBjb252ZXJ0ZWQgdG8gYSBjYXNlLWluc2Vuc2l0aXZlIFJlZ0V4cCkuXG4gICAgICAgICAqIEByZXR1cm5zIHtib29sZWFufSA8dHQ+dHJ1ZTwvdHQ+IHdoZW4gdGhlIHBhdHRlcm4gbWF0Y2hlcywgb3RoZXJ3aXNlIDx0dD5mYWxzZTwvdHQ+XG4gICAgICAgICAqIEBmdW5jdGlvbiBNb2JpbGVEZXRlY3QjbWF0Y2hcbiAgICAgICAgICovXG4gICAgICAgIG1hdGNoOiBmdW5jdGlvbiAocGF0dGVybikge1xuICAgICAgICAgICAgaWYgKCEocGF0dGVybiBpbnN0YW5jZW9mIFJlZ0V4cCkpIHtcbiAgICAgICAgICAgICAgICBwYXR0ZXJuID0gbmV3IFJlZ0V4cChwYXR0ZXJuLCAnaScpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuIHBhdHRlcm4udGVzdCh0aGlzLnVhKTtcbiAgICAgICAgfSxcblxuICAgICAgICAvKipcbiAgICAgICAgICogQ2hlY2tzIHdoZXRoZXIgdGhlIG1vYmlsZSBkZXZpY2UgY2FuIGJlIGNvbnNpZGVyZWQgYXMgcGhvbmUgcmVnYXJkaW5nIDxjb2RlPnNjcmVlbi53aWR0aDwvY29kZT4uXG4gICAgICAgICAqIDxicj5cbiAgICAgICAgICogT2J2aW91c2x5IHRoaXMgbWV0aG9kIG1ha2VzIHNlbnNlIGluIGJyb3dzZXIgZW52aXJvbm1lbnRzIG9ubHkgKG5vdCBmb3IgTm9kZS5qcykhXG4gICAgICAgICAqIEBwYXJhbSB7bnVtYmVyfSBbbWF4UGhvbmVXaWR0aF0gdGhlIG1heGltdW0gbG9naWNhbCBwaXhlbHMgKGFrYS4gQ1NTLXBpeGVscykgdG8gYmUgY29uc2lkZXJlZCBhcyBwaG9uZS48YnI+XG4gICAgICAgICAqICAgICAgICBUaGUgYXJndW1lbnQgaXMgb3B0aW9uYWwgYW5kIGlmIG5vdCBwcmVzZW50IG9yIGZhbHN5LCB0aGUgdmFsdWUgb2YgdGhlIGNvbnN0cnVjdG9yIGlzIHRha2VuLlxuICAgICAgICAgKiBAcmV0dXJucyB7Ym9vbGVhbnx1bmRlZmluZWR9IDxjb2RlPnVuZGVmaW5lZDwvY29kZT4gaWYgc2NyZWVuIHNpemUgd2Fzbid0IGRldGVjdGFibGUsIGVsc2UgPGNvZGU+dHJ1ZTwvY29kZT5cbiAgICAgICAgICogICAgICAgICAgd2hlbiBzY3JlZW4ud2lkdGggaXMgbGVzcyBvciBlcXVhbCB0byBtYXhQaG9uZVdpZHRoLCBvdGhlcndpc2UgPGNvZGU+ZmFsc2U8L2NvZGU+Ljxicj5cbiAgICAgICAgICogICAgICAgICAgV2lsbCBhbHdheXMgcmV0dXJuIDxjb2RlPnVuZGVmaW5lZDwvY29kZT4gc2VydmVyLXNpZGUuXG4gICAgICAgICAqL1xuICAgICAgICBpc1Bob25lU2l6ZWQ6IGZ1bmN0aW9uIChtYXhQaG9uZVdpZHRoKSB7XG4gICAgICAgICAgICByZXR1cm4gTW9iaWxlRGV0ZWN0LmlzUGhvbmVTaXplZChtYXhQaG9uZVdpZHRoIHx8IHRoaXMubWF4UGhvbmVXaWR0aCk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqIFJldHVybnMgdGhlIG1vYmlsZSBncmFkZSAoJ0EnLCAnQicsICdDJykuXG4gICAgICAgICAqXG4gICAgICAgICAqIEByZXR1cm5zIHtTdHJpbmd9IG9uZSBvZiB0aGUgbW9iaWxlIGdyYWRlcyAoJ0EnLCAnQicsICdDJykuXG4gICAgICAgICAqIEBmdW5jdGlvbiBNb2JpbGVEZXRlY3QjbW9iaWxlR3JhZGVcbiAgICAgICAgICovXG4gICAgICAgIG1vYmlsZUdyYWRlOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICBpZiAodGhpcy5fY2FjaGUuZ3JhZGUgPT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgICAgIHRoaXMuX2NhY2hlLmdyYWRlID0gaW1wbC5tb2JpbGVHcmFkZSh0aGlzKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybiB0aGlzLl9jYWNoZS5ncmFkZTtcbiAgICAgICAgfVxuICAgIH07XG5cbiAgICAvLyBlbnZpcm9ubWVudC1kZXBlbmRlbnRcbiAgICBpZiAodHlwZW9mIHdpbmRvdyAhPT0gJ3VuZGVmaW5lZCcgJiYgd2luZG93LnNjcmVlbikge1xuICAgICAgICBNb2JpbGVEZXRlY3QuaXNQaG9uZVNpemVkID0gZnVuY3Rpb24gKG1heFBob25lV2lkdGgpIHtcbiAgICAgICAgICAgIHJldHVybiBtYXhQaG9uZVdpZHRoIDwgMCA/IHVuZGVmaW5lZCA6IGltcGwuZ2V0RGV2aWNlU21hbGxlclNpZGUoKSA8PSBtYXhQaG9uZVdpZHRoO1xuICAgICAgICB9O1xuICAgIH0gZWxzZSB7XG4gICAgICAgIE1vYmlsZURldGVjdC5pc1Bob25lU2l6ZWQgPSBmdW5jdGlvbiAoKSB7fTtcbiAgICB9XG5cbiAgICAvLyBzaG91bGQgbm90IGJlIHJlcGxhY2VkIGJ5IGEgY29tcGxldGVseSBuZXcgb2JqZWN0IC0ganVzdCBvdmVyd3JpdGUgZXhpc3RpbmcgbWV0aG9kc1xuICAgIE1vYmlsZURldGVjdC5faW1wbCA9IGltcGw7XG4gICAgXG4gICAgTW9iaWxlRGV0ZWN0LnZlcnNpb24gPSAnMS40LjQgMjAxOS0wOS0yMSc7XG5cbiAgICByZXR1cm4gTW9iaWxlRGV0ZWN0O1xufSk7IC8vIGVuZCBvZiBjYWxsIG9mIGRlZmluZSgpXG59KSgoZnVuY3Rpb24gKHVuZGVmaW5lZCkge1xuICAgIGlmICh0eXBlb2YgbW9kdWxlICE9PSAndW5kZWZpbmVkJyAmJiBtb2R1bGUuZXhwb3J0cykge1xuICAgICAgICByZXR1cm4gZnVuY3Rpb24gKGZhY3RvcnkpIHsgbW9kdWxlLmV4cG9ydHMgPSBmYWN0b3J5KCk7IH07XG4gICAgfSBlbHNlIGlmICh0eXBlb2YgZGVmaW5lID09PSAnZnVuY3Rpb24nICYmIGRlZmluZS5hbWQpIHtcbiAgICAgICAgcmV0dXJuIGRlZmluZTtcbiAgICB9IGVsc2UgaWYgKHR5cGVvZiB3aW5kb3cgIT09ICd1bmRlZmluZWQnKSB7XG4gICAgICAgIHJldHVybiBmdW5jdGlvbiAoZmFjdG9yeSkgeyB3aW5kb3cuTW9iaWxlRGV0ZWN0ID0gZmFjdG9yeSgpOyB9O1xuICAgIH0gZWxzZSB7XG4gICAgICAgIC8vIHBsZWFzZSBmaWxlIGEgYnVnIGlmIHlvdSBnZXQgdGhpcyBlcnJvciFcbiAgICAgICAgdGhyb3cgbmV3IEVycm9yKCd1bmtub3duIGVudmlyb25tZW50Jyk7XG4gICAgfVxufSkoKSk7IiwibW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbigpIHtcblx0dGhyb3cgbmV3IEVycm9yKFwiZGVmaW5lIGNhbm5vdCBiZSB1c2VkIGluZGlyZWN0XCIpO1xufTtcbiIsInZhciBnO1xuXG4vLyBUaGlzIHdvcmtzIGluIG5vbi1zdHJpY3QgbW9kZVxuZyA9IChmdW5jdGlvbigpIHtcblx0cmV0dXJuIHRoaXM7XG59KSgpO1xuXG50cnkge1xuXHQvLyBUaGlzIHdvcmtzIGlmIGV2YWwgaXMgYWxsb3dlZCAoc2VlIENTUClcblx0ZyA9IGcgfHwgbmV3IEZ1bmN0aW9uKFwicmV0dXJuIHRoaXNcIikoKTtcbn0gY2F0Y2ggKGUpIHtcblx0Ly8gVGhpcyB3b3JrcyBpZiB0aGUgd2luZG93IHJlZmVyZW5jZSBpcyBhdmFpbGFibGVcblx0aWYgKHR5cGVvZiB3aW5kb3cgPT09IFwib2JqZWN0XCIpIGcgPSB3aW5kb3c7XG59XG5cbi8vIGcgY2FuIHN0aWxsIGJlIHVuZGVmaW5lZCwgYnV0IG5vdGhpbmcgdG8gZG8gYWJvdXQgaXQuLi5cbi8vIFdlIHJldHVybiB1bmRlZmluZWQsIGluc3RlYWQgb2Ygbm90aGluZyBoZXJlLCBzbyBpdCdzXG4vLyBlYXNpZXIgdG8gaGFuZGxlIHRoaXMgY2FzZS4gaWYoIWdsb2JhbCkgeyAuLi59XG5cbm1vZHVsZS5leHBvcnRzID0gZztcbiIsIm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24obW9kdWxlKSB7XG5cdGlmICghbW9kdWxlLndlYnBhY2tQb2x5ZmlsbCkge1xuXHRcdG1vZHVsZS5kZXByZWNhdGUgPSBmdW5jdGlvbigpIHt9O1xuXHRcdG1vZHVsZS5wYXRocyA9IFtdO1xuXHRcdC8vIG1vZHVsZS5wYXJlbnQgPSB1bmRlZmluZWQgYnkgZGVmYXVsdFxuXHRcdGlmICghbW9kdWxlLmNoaWxkcmVuKSBtb2R1bGUuY2hpbGRyZW4gPSBbXTtcblx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkobW9kdWxlLCBcImxvYWRlZFwiLCB7XG5cdFx0XHRlbnVtZXJhYmxlOiB0cnVlLFxuXHRcdFx0Z2V0OiBmdW5jdGlvbigpIHtcblx0XHRcdFx0cmV0dXJuIG1vZHVsZS5sO1xuXHRcdFx0fVxuXHRcdH0pO1xuXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShtb2R1bGUsIFwiaWRcIiwge1xuXHRcdFx0ZW51bWVyYWJsZTogdHJ1ZSxcblx0XHRcdGdldDogZnVuY3Rpb24oKSB7XG5cdFx0XHRcdHJldHVybiBtb2R1bGUuaTtcblx0XHRcdH1cblx0XHR9KTtcblx0XHRtb2R1bGUud2VicGFja1BvbHlmaWxsID0gMTtcblx0fVxuXHRyZXR1cm4gbW9kdWxlO1xufTtcbiIsIi8qKlxuICogQW4gVXRpbGl0eSBjbGFzcyB0aGF0IGFsbG93cyB0byB0d2VlbiBhIG51bWVyaWMgdmFsdWUgKF90YXJnZXRfKSBpbnRvIGEgc21vb3RoZWQgdmFsdWUgKF92YWx1ZV8pXG4gKlxuICpcbiAqIEBjbGFzcyBFbGFzdGljTnVtYmVyXG4gKiBAcHJvcGVydHkge051bWJlcn0gdmFsdWUgLSBUaGUgcmVhbCB2YWx1ZSBvZiB0aGUgbnVtYmVyLlxuICogQHByb3BlcnR5IHtOdW1iZXJ9IHRhcmdldCAtIFRoZSBkZXNpcmVkIHZhbHVlIG9mIHRoZSBudW1iZXIuXG4gKiBAcHJvcGVydHkge051bWJlcn0gc3BlZWQgLSBUaGUgc3BlZWQgYXQgd2hpY2ggdGhlIEVsYXN0aWNOdW1iZXIgd2lsbCBtYWtlIHZhbHVlIHJlYWNoIHRhcmdldC5cbiAqIEBleGFtcGxlXG4gKlxuICogY29uc3RydWN0b3IgKCkge1xuICogIHRoaXMubXlOdW1iZXIgPSBuZXcgRWxhc3RpY051bWJlcigpO1xuICogIHRoaXMudGltZSA9IG5ldyBEYXRlKCkuZ2V0VGltZSgpXG4gKiB9XG4gKlxuICogb25VcGRhdGUgKCkge1xuICogIGNvbnN0IG5vdyA9IG5ldyBEYXRlKCkuZ2V0VGltZSgpXG4gKiAgY29uc3QgZGVsdGEgPSBub3cgLSB0aGlzLnRpbWVcbiAqICB0aGlzLm15TnVtYmVyLnRhcmdldCA9IE1hdGgucmFuZG9tKCkgKiAxMDBcbiAqICB0aGlzLm15TnVtYmVyLnVwZGF0ZShkZWx0YSk7XG4gKlxuICogIHRoaXMubXlTcHJpdGUueCA9IHRoaXMubXlOdW1iZXIudmFsdWVcbiAqICB0aGlzLnRpbWUgPSBub3dcbiAqIH1cbiAqL1xuZXhwb3J0IGRlZmF1bHQgY2xhc3MgRWxhc3RpY051bWJlciB7XG4gIC8qKlxuICAgKiBJbml0aWFsaXplcyB0aGUgRWxhc3RpY051bWJlciB3aXRoIGFcbiAgICogQGlnbm9yZVxuICAgKiBAcGFyYW0ge051bWJlcn0gdmFsdWUgcHJlZGVmaW5lZCB2YWx1ZSAoMCBieSBkZWZhdWx0KVxuICAgKiBAbWVtYmVyT2YgRWxhc3RpY051bWJlclxuICAgKi9cbiAgY29uc3RydWN0b3IgKHZhbHVlID0gMCkge1xuICAgIHRoaXMudmFsdWUgPSB2YWx1ZVxuICAgIHRoaXMudGFyZ2V0ID0gdmFsdWVcbiAgICB0aGlzLnNwZWVkID0gM1xuICB9XG5cbiAgLyoqXG4gICAqIFVwZGF0ZXMgdGhlIEVsYXN0aWNOdW1iZXIgb24gdGljay5cbiAgICogVmFsdWUgd2lsbCBiZSB1cGRhdGVkIGZyb20gdGFyZ2V0LlxuICAgKlxuICAgKiBAcGFyYW0ge051bWJlcn0gZGVsdGEgZGVsdGEgdGltZSBpbiBtaWxsaXNlY29uZHNcbiAgICogQG1lbWJlck9mIEVsYXN0aWNOdW1iZXJcbiAgICovXG4gIHVwZGF0ZSAoZGVsdGEpIHtcbiAgICAvLyBkZWx0YSA9IE1hdGgubWluKGRlbHRhLCAxIC8gMjApO1xuXG4gICAgY29uc3QgZGlzdCA9IHRoaXMudGFyZ2V0IC0gdGhpcy52YWx1ZVxuXG4gICAgdGhpcy52YWx1ZSArPSBkaXN0ICogKHRoaXMuc3BlZWQgKiBkZWx0YSlcbiAgfVxufVxuIiwiLyoqXG4gKiBBIGNvbGxlY3Rpb24gb2YgRmlsZSByZWxhdGVkIHV0aWxpdGllcy5cbiAqXG4gKiBAbW9kdWxlIEZpbGVVdGlsc1xuICovXG5cbi8qKlxuICogRG93bmxvYWRzIGEgZmlsZSB0byB0aGUgdXNlcidzIG1hY2hpbmUuXG4gKlxuICogQHBhcmFtIHtTdHJpbmd9IGNvbnRlbnQgLSBUaGUgY29udGVudCB0byBiZSBkb3dubG9hZGVkLlxuICogQHBhcmFtIHtTdHJpbmd9IGZpbGVOYW1lIC0gVGhlIG5hbWUgb2YgdGhlIGZpbGUgZG93bmxvYWRlZCBpbnRvIHRoZSB1c2VyJ3MgUEMuXG4gKiBAcGFyYW0ge1N0cmluZ30gY29udGVudFR5cGUgLSBUaGUgZmlsZSB0eXBlLlxuICovXG5jb25zdCBkb3dubG9hZEZpbGUgPSBmdW5jdGlvbiAoY29udGVudCwgZmlsZU5hbWUsIGNvbnRlbnRUeXBlKSB7XG4gIGNvbnN0IGEgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdhJylcbiAgY29uc3QgZmlsZSA9IG5ldyBCbG9iKFtjb250ZW50XSwgeyB0eXBlOiBjb250ZW50VHlwZSB9KVxuICBhLmhyZWYgPSBVUkwuY3JlYXRlT2JqZWN0VVJMKGZpbGUpXG4gIGEuZG93bmxvYWQgPSBmaWxlTmFtZVxuICBhLmNsaWNrKClcbn1cblxuLyoqXG4gKiBQcm9tcHRzIHRoZSB1c2VyIHRvIHNlbGVjdCBhIHNpbmdsZSBmaWxlIGZyb20gaGlzIGhhcmRkcml2ZS5cbiAqXG4gKiBAcGFyYW0ge1N0cmluZ30gW2FjY2VwdF0gLSBBY2NlcHQgc3RyaW5nIHRvIHJlc3RyaWN0IGZpbGUgc2VsZWN0aW9uIHRvIGNlcnRhaW4gZmlsZSB0eXBlcy5cbiAqIEByZXR1cm5zIHtQcm9taXNlPEFycmF5Pn0gQXJyYXkgb2YgZmlsZXMgc2VsZWN0ZWQgYnkgdGhlIHVzZXIuXG4gKi9cbmNvbnN0IGxvYWRGaWxlID0gZnVuY3Rpb24gKGFjY2VwdCkge1xuICByZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuICAgIGNvbnN0IGlucHV0ID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnaW5wdXQnKVxuICAgIGlucHV0LnR5cGUgPSAnZmlsZSdcbiAgICBpbnB1dC5hY2NlcHQgPSBhY2NlcHRcbiAgICBpbnB1dC5vbmNoYW5nZSA9IChldmVudCkgPT4ge1xuICAgICAgcmVzb2x2ZShldmVudC50YXJnZXQuZmlsZXMpXG4gICAgfVxuICAgIGlucHV0LmNsaWNrKClcbiAgfSlcbn1cblxuLyoqXG4gKiBQcm9tcHRzIHRoZSB1c2VyIHRvIHNlbGVjdCBhIHNpbmdsZSBKU09OIGZpbGUgZnJvbSBoaXMgaGFyZGRyaXZlXG4gKiBhbmQgcmV0dXJucyB0aGUgcmVzdWx0IG9mIGhpcyBzZWxlY3Rpb24uXG4gKlxuICogQHJldHVybnMge1Byb21pc2U8U3RyaW5nPn0gVGhlIHBhcnNlZCBKU09OIGZpbGUuXG4gKi9cbmNvbnN0IGxvYWRKU09OID0gZnVuY3Rpb24gKCkge1xuICByZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuICAgIGxvYWRGaWxlKCcuanNvbicpLnRoZW4oZmlsZXMgPT4ge1xuICAgICAgY29uc3QgZnIgPSBuZXcgRmlsZVJlYWRlcigpXG4gICAgICBmci5vbmxvYWQgPSAoZXZlbnQpID0+IHtcbiAgICAgICAgcmVzb2x2ZShKU09OLnBhcnNlKGV2ZW50LnRhcmdldC5yZXN1bHQpKVxuICAgICAgfVxuICAgICAgZnIucmVhZEFzVGV4dChmaWxlc1swXSlcbiAgICAgIHJldHVybiB0cnVlXG4gICAgfSkuY2F0Y2goZSA9PiB7IHRocm93IGUgfSlcbiAgfSlcbn1cbi8qKlxuICogUHJvbXB0cyB0aGUgdXNlciB0byBzZWxlY3QgYSBzaW5nbGUgSlNPTiBmaWxlIGZyb20gaGlzIGhhcmRkcml2ZVxuICogYW5kIHJldHVybnMgdGhlIHJlc3VsdCBvZiBoaXMgc2VsZWN0aW9uXG4gKlxuICogQHJldHVybnMge1Byb21pc2U8SW1hZ2U+fSBUaGUgc2VsZWN0ZWQgaW1hZ2Ugb2JqZWN0LlxuICovXG5jb25zdCBsb2FkSW1hZ2UgPSBmdW5jdGlvbiAoKSB7XG4gIHJldHVybiBuZXcgUHJvbWlzZSgocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XG4gICAgY29uc3QgaW1nID0gbmV3IEltYWdlKClcbiAgICBpbWcub25sb2FkID0gKGV2ZW50KSA9PiB7XG4gICAgICByZXNvbHZlKGltZylcbiAgICB9XG5cbiAgICBsb2FkRmlsZSgnaW1hZ2UvKicpLnRoZW4oZmlsZXMgPT4ge1xuICAgICAgY29uc3QgZnIgPSBuZXcgRmlsZVJlYWRlcigpXG4gICAgICBmci5vbmxvYWQgPSAoZXZlbnQpID0+IHtcbiAgICAgICAgaW1nLnNyYyA9IGV2ZW50LnRhcmdldC5yZXN1bHRcbiAgICAgIH1cbiAgICAgIGZyLnJlYWRBc0RhdGFVUkwoZmlsZXNbMF0pXG4gICAgICByZXR1cm4gdHJ1ZVxuICAgIH0pLmNhdGNoKGUgPT4geyB0aHJvdyBlIH0pXG4gIH0pXG59XG5cbmV4cG9ydCB7XG4gIGRvd25sb2FkRmlsZSxcbiAgbG9hZEZpbGUsXG4gIGxvYWRKU09OLFxuICBsb2FkSW1hZ2Vcbn1cbiIsImltcG9ydCBSQUYgZnJvbSAnLi9SQUYnXG5yZXF1aXJlKCdAaHVnaHNrL2Z1bGx0aWx0L2Rpc3QvZnVsbHRpbHQnKVxuLyoqXG4gKiBBbiB1dGlsaXR5IHNpbmdsZXRvbiB0aGF0IHJldHVybnMgYSBkZXZpY2UncyBHeXJvc2NvcGVcbiAqIGRhdGEgaW4gdGhlIGZvcm0gb2Ygb3JpZW50YXRpb25YIGFuZCBvcmllbnRhdGlvblkuXG4gKlxuICogVGhlIHZhbHVlcyBhcmUgdXBkYXRlZCBldmVyeSBmcmFtZSBhbmQgdGhlIHN5c3RlbSBzdG9wcyB1cGRhdGluZ1xuICogaWYgaXQgZGV0ZXJtaW5lcyB0aGF0IHRoZSBkZXZpY2UgZG9lcyBub3QgaGF2ZSBneXJvc2NvcGUgY2FwYWJpbGl0aWVzLlxuICpcbiAqIEBjbGFzcyBHeXJvc2NvcGVcbiAqIEBwcm9wZXJ0eSB7TnVtYmVyfSBvcmllbnRhdGlvblhcbiAqIEBwcm9wZXJ0eSB7TnVtYmVyfSBvcmllbnRhdGlvbllcbiAqIEBwcm9wZXJ0eSB7Qm9vbGVhbn0gZW5hYmxlZFxuICovXG5jbGFzcyBHeXJvc2NvcGUge1xuICAvKipcbiAgICogQGlnbm9yZVxuICAgKi9cbiAgY29uc3RydWN0b3IgKCkge1xuICAgIHRoaXMub3JpZW50YXRpb25YID0gMFxuICAgIHRoaXMub3JpZW50YXRpb25ZID0gMFxuICAgIHRoaXMuZW5hYmxlZCA9IHRydWVcbiAgICB0aGlzLl91cGRhdGUgPSB0aGlzLl91cGRhdGUuYmluZCh0aGlzKVxuICAgIFJBRi5hZGQodGhpcy5fdXBkYXRlKVxuICB9XG5cbiAgLyoqXG4gICAqIEBpZ25vcmVcbiAgICovXG4gIF91cGRhdGUgKCkge1xuICAgIGNvbnN0IGZ1bGx0aWx0UHJvbWlzZSA9IG5ldyB3aW5kb3cuRlVMTFRJTFQuZ2V0RGV2aWNlT3JpZW50YXRpb24oeyB0eXBlOiAnZ2FtZScgfSkgLy8gZXNsaW50LWRpc2FibGUtbGluZSBuZXctY2FwXG5cbiAgICBmdWxsdGlsdFByb21pc2UudGhlbigoZGV2aWNlT3JpZW50YXRpb24pID0+IHtcbiAgICAgIGNvbnN0IGV1bGVyID0gZGV2aWNlT3JpZW50YXRpb24uZ2V0U2NyZWVuQWRqdXN0ZWRFdWxlcigpXG5cbiAgICAgIHRoaXMub3JpZW50YXRpb25YID0gZXVsZXIuYmV0YVxuICAgICAgdGhpcy5vcmllbnRhdGlvblkgPSBldWxlci5nYW1tYVxuXG4gICAgICBpZiAodGhpcy5vcmllbnRhdGlvblggPiA5MCkge1xuICAgICAgICB0aGlzLm9yaWVudGF0aW9uWSA9IC10aGlzLm9yaWVudGF0aW9uWVxuICAgICAgfVxuICAgICAgcmV0dXJuIG51bGxcbiAgICAgIC8vIGNvbnNvbGUubG9nKHRoaXMub3JpZW50YXRpb25YLCB0aGlzLm9yaWVudGF0aW9uWSlcbiAgICB9KS5jYXRjaCgobWVzc2FnZSkgPT4ge1xuICAgICAgY29uc29sZS53YXJuKCdEaXNhYmxpbmcgR3lyb3Njb3BlLCByZWFzb246JywgbWVzc2FnZSlcbiAgICAgIHRoaXMuZW5hYmxlZCA9IGZhbHNlXG4gICAgICBSQUYucmVtb3ZlKHRoaXMuX3VwZGF0ZSlcbiAgICB9KVxuICB9XG59XG4vKipcbiAqIEBpZ25vcmVcbiAqL1xuZXhwb3J0IGRlZmF1bHQgR3lyb3Njb3BlXG4iLCIvKipcbiAqIEEgY29sbGVjdGlvbiBvZiBOdW1iZXIgcmVsYXRlZCB1dGlsaXRpZXNcbiAqXG4gKiBAbW9kdWxlIE51bWJlclV0aWxzXG4gKi9cblxuLyoqXG4gICogTGluZWFybHkgaW50ZXJwb2xhdGVzIGEgTnVtYmVyXG4gICpcbiAgKiBAcGFyYW0ge051bWJlcn0gdjAgSW5pdGlhbCB2YWx1ZVxuICAqIEBwYXJhbSB7TnVtYmVyfSB2MSBGaW5hbCB2YWx1ZVxuICAqIEBwYXJhbSB7TnVtYmVyfSB0IHplcm8gdG8gb25lXG4gICogQHJldHVybnMge051bWJlcn0gSW50ZXJwb2xhdGlvbiBiZXR3ZWVuIHYwIGFuZCB2MSBiYXNlZCBvbiB0XG4gICovXG5jb25zdCBsZXJwTnVtYmVyID0gZnVuY3Rpb24gKHYwLCB2MSwgdCkge1xuICByZXR1cm4gdjAgKyB0ICogKHYxIC0gdjApXG59XG5cbi8qKlxuICogVE9ETzogRG9jdW1lbnRhdGlvblxuICpcbiAqIEBwYXJhbSB7Kn0gb2xkVmFsdWVcbiAqIEBwYXJhbSB7Kn0gb2xkTWluXG4gKiBAcGFyYW0geyp9IG9sZE1heFxuICogQHBhcmFtIHsqfSBuZXdNaW5cbiAqIEBwYXJhbSB7Kn0gbmV3TWF4XG4gKiBAcGFyYW0geyp9IGNsYW1wZWRcbiAqL1xuY29uc3QgcmFuZ2UgPSBmdW5jdGlvbiAob2xkVmFsdWUsIG9sZE1pbiwgb2xkTWF4LCBuZXdNaW4sIG5ld01heCwgY2xhbXBlZCkge1xuICBjb25zdCBvbGRSYW5nZSA9IG9sZE1heCAtIG9sZE1pblxuICBjb25zdCBuZXdSYW5nZSA9IG5ld01heCAtIG5ld01pblxuICBjb25zdCBuZXdWYWx1ZSA9ICgob2xkVmFsdWUgLSBvbGRNaW4pICogbmV3UmFuZ2UpIC8gb2xkUmFuZ2UgKyBuZXdNaW5cblxuICBpZiAoY2xhbXBlZCkge1xuICAgIHJldHVybiBjbGFtcChuZXdWYWx1ZSwgbmV3TWluLCBuZXdNYXgpXG4gIH1cblxuICByZXR1cm4gbmV3VmFsdWVcbn1cbi8qKlxuICogQ2xhbXBzIGEgdmFsdWUgYmV0d2VlbiBtaW4gYW5kIG1heCB2YWx1ZXNcbiAqXG4gKiBAcGFyYW0ge051bWJlcn0gdmFsdWUgLSBUaGUgdmFsdWUgdG8gYmUgY2xhbXBlZC5cbiAqIEBwYXJhbSB7TnVtYmVyfSBtaW4gLSBNaW5pbXVtIHZhbHVlLlxuICogQHBhcmFtIHtOdW1iZXJ9IG1heCAtIE1heGltdW0gdmFsdWUuXG4gKiBAcmV0dXJucyB7TnVtYmVyfSBBIG51bWJlciBjbGFtcGVkIGJldHdlZW4gbWluIGFuZCBtYXhcbiAqL1xuY29uc3QgY2xhbXAgPSAodmFsdWUsIG1pbiA9IDAsIG1heCA9IDEpID0+IE1hdGgubWF4KG1pbiwgTWF0aC5taW4odmFsdWUsIG1heCkpXG4vKipcbiAqIFRPRE86IERvY3VtZW50YXRpb25cbiAqXG4gKiBAcGFyYW0ge051bWJlcn0gdmFsdWVcbiAqIEBwYXJhbSB7TnVtYmVyfSBtaW5cbiAqIEBwYXJhbSB7TnVtYmVyfSBtYXhcbiAqL1xuY29uc3QgbWFwID0gKHZhbHVlLCBtaW4sIG1heCkgPT4gY2xhbXAoKHZhbHVlIC0gbWluKSAvIChtYXggLSBtaW4pLCAwLCAxKVxuLyoqXG4gKiBUT0RPOiBEb2N1bWVudGF0aW9uXG4gKlxuICogQHBhcmFtIHtOdW1iZXJ9IGFscGhhXG4gKiBAcGFyYW0ge051bWJlcn0gYmV0YVxuICovXG5jb25zdCBhbmdsZURpc3RhbmNlID0gZnVuY3Rpb24gKGFscGhhLCBiZXRhKSB7XG4gIGNvbnN0IHBoaSA9IChNYXRoLmFicyhiZXRhIC0gYWxwaGEpICUgTWF0aC5QSSkgKiAyXG4gIHJldHVybiBwaGkgPiBNYXRoLlBJID8gTWF0aC5QSSAqIDIgLSBwaGkgOiBwaGlcbn1cbi8qKlxuICogVE9ETzogRG9jdW1lbnRhdGlvblxuICpcbiAqIEBwYXJhbSB7TnVtYmVyfSBhbHBoYVxuICogQHBhcmFtIHtOdW1iZXJ9IGJldGFcbiAqL1xuY29uc3QgYW5nbGVEaXN0YW5jZVNpZ24gPSBmdW5jdGlvbiAoYWxwaGEsIGJldGEpIHtcbiAgY29uc3Qgc2lnbiA9XG4gICAgKGFscGhhIC0gYmV0YSA+PSAwICYmIGFscGhhIC0gYmV0YSA8PSBNYXRoLlBJKSB8fFxuICAgIChhbHBoYSAtIGJldGEgPD0gLU1hdGguUEkgJiYgYWxwaGEgLSBiZXRhID49IC1NYXRoLlBJICogMilcbiAgICAgID8gMVxuICAgICAgOiAtMVxuICByZXR1cm4gYW5nbGVEaXN0YW5jZShhbHBoYSwgYmV0YSkgKiBzaWduXG59XG4vKipcbiAqIFNtb290aHN0ZXAgaW1wbGVtZW50YXRpb24uXG4gKiBodHRwczovL2VuLndpa2lwZWRpYS5vcmcvd2lraS9TbW9vdGhzdGVwXG4gKlxuICogQHBhcmFtIHtOdW1iZXJ9IG1pbiAtIEluaXRpYWwgdmFsdWVcbiAqIEBwYXJhbSB7TnVtYmVyfSBtYXggLSBGaW5hbCBWYWx1ZVxuICogQHBhcmFtIHtOdW1iZXJ9IHZhbHVlIC1cbiAqIEByZXR1cm5zIHtOdW1iZXJ9IFZhbHVlIG9mIHRoZSBpbnRlcnBvbGF0aW9uXG4gKi9cbmNvbnN0IHNtb290aHN0ZXAgPSAobWluLCBtYXgsIHZhbHVlKSA9PiB7XG4gIGNvbnN0IHggPSBNYXRoLm1heCgwLCBNYXRoLm1pbigxLCAodmFsdWUgLSBtaW4pIC8gKG1heCAtIG1pbikpKVxuICByZXR1cm4geCAqIHggKiAoMyAtIDIgKiB4KVxufVxuLyoqXG4gKiBTaW1pbGFyIHRvIGxpbmVhciBpbnRlcnBvbGF0aW9uLlxuICpcbiAqIEBwYXJhbSB7TnVtYmVyfSB2YWx1ZTEgLSBNaW5pbXVtIHZhbHVlLlxuICogQHBhcmFtIHtOdW1iZXJ9IHZhbHVlMiAtIE1heGltdW0gdmFsdWUuXG4gKiBAcGFyYW0ge051bWJlcn0gcGVyY2VudCAtIHplcm8gdG8gb25lIHZhbHVlIHBlcmNlbnRcbiAqL1xuY29uc3QgbWl4ID0gKHZhbHVlMSwgdmFsdWUyLCBwZXJjZW50KSA9PiB2YWx1ZTEgKiAoMSAtIHBlcmNlbnQpICsgdmFsdWUyICogcGVyY2VudFxuLyoqXG4gKiBSZXR1cm5zIHRoZSBzaWduIG9mIGEgbnVtYmVyIGluIC0xLDAsMSBmb3JtLlxuICpcbiAqIEBwYXJhbSB7TnVtYmVyfSB2YWx1ZSAtIGEgbnVtYmVyIHRvIGJlIHRlc3RlZFxuICogQHJldHVybnMge051bWJlcn0gMCBpZiB2YWx1ZSBpcyB6ZXJvLCAtMSBpZiB2YWx1ZSBpcyBsZXNzIHRoYW4gMCwgMSBvdGhlcndpc2VcbiAqL1xuY29uc3Qgc2lnbiA9IHZhbHVlID0+ICh2YWx1ZSAhPT0gMCA/ICh2YWx1ZSA8IDAgPyAtMSA6IDEpIDogMClcbi8qKlxuICogVE9ETzogRG9jdW1lbnRhdGlvblxuICpcbiAqIEBwYXJhbSB7TnVtYmVyfSBtaW5cbiAqIEBwYXJhbSB7TnVtYmVyfSBtYXhcbiAqIEBwYXJhbSB7TnVtYmVyfSBwb3dlclxuICogQHBhcmFtIHtOdW1iZXJ9IFtyZCA9IE1hdGgucmFuZG9tXSAtIE9wdGlvbmFsIFJhbmRvbSBudW1iZXIgZ2VuZXJhdG9yXG4gKi9cbmNvbnN0IHJhbmRCeVBvd2VyID0gZnVuY3Rpb24gKG1pbiwgbWF4LCBwb3dlciwgcmQgPSBNYXRoLnJhbmRvbSkge1xuICBjb25zdCB2YWx1ZSA9IHJkKCkgKiAobWF4IC0gbWluKSArIG1pblxuICByZXR1cm4gbWFwVG9Qb3dlcih2YWx1ZSwgbWluLCBtYXgsIHBvd2VyKVxufVxuLyoqXG4gKiBUT0RPOiBEb2N1bWVudGF0aW9uXG4gKlxuICogQHBhcmFtIHtOdW1iZXJ9IHZhbHVlXG4gKiBAcGFyYW0ge051bWJlcn0gbWluXG4gKiBAcGFyYW0ge051bWJlcn0gbWF4XG4gKiBAcGFyYW0ge051bWJlcn0gcG93ZXJcbiAqL1xuY29uc3QgbWFwVG9Qb3dlciA9IGZ1bmN0aW9uICh2YWx1ZSwgbWluLCBtYXgsIHBvd2VyKSB7XG4gIGNvbnN0IHZhbHVlU2lnbiA9IHNpZ24odmFsdWUpXG5cbiAgdmFsdWUgPSAodmFsdWUgLSBtaW4pIC8gKG1heCAtIG1pbilcbiAgdmFsdWUgPSB2YWx1ZVNpZ24gPiAwID8gdmFsdWUgOiB2YWx1ZSAtIDFcbiAgdmFsdWUgPSBNYXRoLnBvdyh2YWx1ZSAqIHZhbHVlU2lnbiwgTWF0aC5hYnMocG93ZXIpKVxuICB2YWx1ZSA9IHZhbHVlU2lnbiA+IDAgPyB2YWx1ZSA6IDEgLSB2YWx1ZVxuXG4gIHJldHVybiB2YWx1ZSAqIChtYXggLSBtaW4pICsgbWluXG59XG4vKipcbiAqIENvbXB1dGVzIGRpc3RhbmNlIGJldHdlZW4gdHdvIHBvaW50cyBpbiAyRCBzcGFjZS5cbiAqXG4gKiBAcGFyYW0ge051bWJlcn0geDEgLSB4IGNvb3JkaW5hdGUgb2YgZmlyc3QgcG9pbnRcbiAqIEBwYXJhbSB7TnVtYmVyfSB5MSAtIHkgY29vcmRpbmF0ZSBvZiBmaXJzdCBwb2ludFxuICogQHBhcmFtIHtOdW1iZXJ9IHgyIC0geCBjb29yZGluYXRlIG9mIHNlY29uZCBwb2ludFxuICogQHBhcmFtIHtOdW1iZXJ9IHkyIC0geSBjb29yZGluYXRlIG9mIHNlY29uZCBwb2ludFxuICogQHJldHVybnMge051bWJlcn0gRGlzdGFuY2UgYmV0d2VlbiB4MSx5MSBhbmQgeDIseTJcbiAqL1xuY29uc3QgZGlzdGFuY2UgPSBmdW5jdGlvbiAoeDEsIHkxLCB4MiwgeTIpIHtcbiAgY29uc3QgZHggPSB4MSAtIHgyXG4gIGNvbnN0IGR5ID0geTEgLSB5MlxuICByZXR1cm4gTWF0aC5zcXJ0KGR4ICogZHggKyBkeSAqIGR5KVxufVxuXG4vKipcbiAqIE5vdCBhIHJlYWwgZGlzdGFuY2UgY2FsY3VsYXRpb24uXG4gKiBVc2VmdWwgdG8gc29ydCBvYmplY3RzIGJ5IGRpc3RhbmNlICggbXVjaCBmYXN0ZXIgYmVjYXVzZSBubyBzcXJ0IClcbiAqXG4gKiBAcGFyYW0ge051bWJlcn0geDEgLSB4IGNvb3JkaW5hdGUgb2YgZmlyc3QgcG9pbnRcbiAqIEBwYXJhbSB7TnVtYmVyfSB5MSAtIHkgY29vcmRpbmF0ZSBvZiBmaXJzdCBwb2ludFxuICogQHBhcmFtIHtOdW1iZXJ9IHgyIC0geCBjb29yZGluYXRlIG9mIHNlY29uZCBwb2ludFxuICogQHBhcmFtIHtOdW1iZXJ9IHkyIC0geSBjb29yZGluYXRlIG9mIHNlY29uZCBwb2ludFxuICogQHJldHVybnMge051bWJlcn0gYm9ndXMgZGlzdGFuY2UgYmV0d2VlbiB4MSx5MSBhbmQgeDIseTIuIERPIE5PVCBVU0UgV0hFTiBBIFJFQUwgRElTVEFOQ0UgSVMgTkVFREVEXG4gKi9cbmNvbnN0IGRpc3RhbmNlQ29tcGFyZSA9IGZ1bmN0aW9uICh4MSwgeTEsIHgyLCB5Mikge1xuICBjb25zdCBkeCA9IHgxIC0geDJcbiAgY29uc3QgZHkgPSB5MSAtIHkyXG4gIHJldHVybiBkeCAqIGR4ICsgZHkgKiBkeVxufVxuXG5leHBvcnQge1xuICBsZXJwTnVtYmVyLFxuICBkaXN0YW5jZUNvbXBhcmUsXG4gIGRpc3RhbmNlLFxuICBtYXBUb1Bvd2VyLFxuICByYW5kQnlQb3dlcixcbiAgc21vb3Roc3RlcCxcbiAgYW5nbGVEaXN0YW5jZVNpZ24sXG4gIGFuZ2xlRGlzdGFuY2UsXG4gIG1hcCxcbiAgY2xhbXAsXG4gIHJhbmdlLFxuICBtaXhcbn1cbiIsIi8qKlxuICogQSBjb2xsZWN0aW9uIG9mIE9iamVjdCByZWxhdGVkIHV0aWxpdGllc1xuICpcbiAqIEBtb2R1bGUgT2JqZWN0VXRpbHNcbiAqL1xuXG4vKipcbiAqIERldGVybWluZXMgaWYgdGhlIG9iamVjdCBoYXMgYmVlbiBjb25zdHJ1Y3RlZCB1c2luZ1xuICogamF2YXNjcmlwdCBnZW5lcmljIHt9IHN5bnRheFxuICpcbiAqIEBwYXJhbSB7Kn0gbyAtIFRoZSBvYmplY3QgdG8gYmUgdGVzdGVkLlxuICogQHJldHVybnMge0Jvb2xlYW59IHRydWUgaWYgdGhlIG9iamVjdCBpcyBhIHBsYWluIG9iamVjdCwgZmFsc2Ugb3RoZXJ3aXNlLlxuICovXG5jb25zdCBpc1BsYWluT2JqZWN0ID0gZnVuY3Rpb24gKG8pIHtcbiAgcmV0dXJuIG8gJiYgKHR5cGVvZiBvID09PSAnb2JqZWN0JykgJiYgby5jb25zdHJ1Y3RvciA9PT0gT2JqZWN0XG59XG5cbmV4cG9ydCB7IGlzUGxhaW5PYmplY3QgfVxuIiwiXG5jb25zdCBNb2JpbGVEZXRlY3QgPSByZXF1aXJlKCdtb2JpbGUtZGV0ZWN0JylcbmNvbnN0IG1kID0gbmV3IE1vYmlsZURldGVjdCh3aW5kb3cubmF2aWdhdG9yLnVzZXJBZ2VudClcblxuY29uc3Qgdm9sdW1lVGVzdCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2F1ZGlvJylcbnZvbHVtZVRlc3Qudm9sdW1lID0gMC41XG5cbi8qKlxuICogSG9sZHMgaW5mb3JtYXRpb24gb24gdGhlIFBsYXRmb3JtIHdoZXJlIHRoaXMgY29kZSBpcyBydW5cbiAqXG4gKiBAbW9kdWxlIFBsYXRmb3JtXG4gKiBAcHJvcGVydHkge0Jvb2xlYW59IG1vYmlsZSAtIERldmljZSBpcyBhIG1vYmlsZSAoaW5jbHVkZXMgdGFibGV0cyBhbmQgcGhvbmVzKVxuICogQHByb3BlcnR5IHtCb29sZWFufSBwaG9uZSAtIERldmljZSBpcyBhIHBob25lIChleGNsdWRlcyB0YWJsZXRzKVxuICogQHByb3BlcnR5IHtCb29sZWFufSB0YWJsZXQgLSBEZXZpY2UgaXMgYSB0YWJsZXQgKGV4Y2x1ZGVzIHBob25lcylcbiAqIEBwcm9wZXJ0eSB7Qm9vbGVhbn0gYW5kcm9pZCAtIERldmljZSBpcyBBbmRyb2lkIGJhc2VkXG4gKiBAcHJvcGVydHkge0Jvb2xlYW59IGlvcyAtIERldmljZSBpcyBpT1MgYmFzZWRcbiAqIEBwcm9wZXJ0eSB7Qm9vbGVhbn0gaXBhZCAtIERldmljZSBpcyBhbiBpUGFkXG4gKiBAcHJvcGVydHkge0Jvb2xlYW59IGlwaG9uZSAtIERldmljZSBpcyBhbiBpUGhvbmVcbiAqIEBwcm9wZXJ0eSB7Qm9vbGVhbn0gd3Bob25lIC0gRGV2aWNlIGlzIGFuIFdpbmRvd3MgUGhvbmVcbiAqIEBwcm9wZXJ0eSB7Qm9vbGVhbn0gZWRnZSAtIEJyb3dzZXIgaXMgTWljcm9zb2Z0IEVkZ2VcbiAqIEBwcm9wZXJ0eSB7Qm9vbGVhbn0gZmlyZWZveCAtIEJyb3dzZXIgaXMgTW96aWxsYSBGaXJlZm94XG4gKiBAcHJvcGVydHkge0Jvb2xlYW59IGllMTEgLSBCcm93c2VyIGlzIE1pY3Jvc29mdCBJbnRlcm5ldCBFeHBsb3JlciAxMVxuICogQHByb3BlcnR5IHtCb29sZWFufSBzYWZhcmkgLSBCcm93c2VyIGlzIFNhZmFyaVxuICogQHByb3BlcnR5IHtCb29sZWFufSBwcmVyZW5kZXJlciAtIFBhZ2UgaXMgdmlzaXRlZCBieSBhIHByZXJlbmRlcmVyIChsaWtlIFBoYW50b20gSlMpXG4gKiBAcHJvcGVydHkge0Jvb2xlYW59IHZvbHVtZSAtIERldmljZSBzdXBwb3J0cyB2b2x1bWUgc2V0dGluZyB2aWEganMgKGlPUyBkb2Vzbid0IHN1cHBvcnQgdGhpcylcbiAqL1xuY29uc3QgUGxhdGZvcm0gPSB7XG4gIG1vYmlsZTogISFtZC5tb2JpbGUoKSxcbiAgcGhvbmU6ICEhbWQucGhvbmUoKSxcbiAgdGFibGV0OiAhIW1kLnRhYmxldCgpLFxuICBhbmRyb2lkOiAhIW1kLmlzKCdBbmRyb2lkT1MnKSxcbiAgaW9zOiAhIW1kLmlzKCdpT1MnKSxcbiAgaXBhZDogISFtZC5pcygnaVBhZCcpLFxuICBpcGhvbmU6ICEhbWQuaXMoJ2lQaG9uZScpLFxuICB3cGhvbmU6ICEhbWQuaXMoJ1dpbmRvd3NQaG9uZU9TJyksXG4gIGVkZ2U6ICEhL0VkZ2VcXC9cXGQrL2kudGVzdCh3aW5kb3cubmF2aWdhdG9yLnVzZXJBZ2VudCksXG4gIGZpcmVmb3g6IG1kLnZlcnNpb24oJ0dlY2tvJykgPiAxLFxuICBpZTExOiAhIS9UcmlkZW50LipydjoxMVxcLi9pLnRlc3Qod2luZG93Lm5hdmlnYXRvci51c2VyQWdlbnQpLFxuICBzYWZhcmk6IC9TYWZhcmkvLnRlc3Qod2luZG93Lm5hdmlnYXRvci51c2VyQWdlbnQpICYmIC9BcHBsZSBDb21wdXRlci8udGVzdCh3aW5kb3cubmF2aWdhdG9yLnZlbmRvciksXG4gIHByZXJlbmRlcmVyOiB3aW5kb3cuX19QUkVSRU5ERVJfSU5KRUNURUQgIT09IHVuZGVmaW5lZCwgLy8gL1BoYW50b21KUy8udGVzdCh3aW5kb3cubmF2aWdhdG9yLnVzZXJBZ2VudCksXG4gIHZvbHVtZTogdm9sdW1lVGVzdC52b2x1bWUgPT09IDAuNVxufVxuXG4vLyBpZiBub3QgcHJlcmVuZGVyZXJcbmlmICghUGxhdGZvcm0ucHJlcmVuZGVyZXIgJiYgd2luZG93Lk1vZGVybml6cikge1xuICAvLyBhZGQgY3VzdG9tIG1vZGVybml6ciB0ZXN0c1xuICBmb3IgKGNvbnN0IGtleSBpbiBQbGF0Zm9ybSkge1xuICAgIHdpbmRvdy5Nb2Rlcm5penIuYWRkVGVzdChrZXksICgpID0+IHtcbiAgICAgIHJldHVybiBQbGF0Zm9ybVtrZXldXG4gICAgfSlcbiAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCBQbGF0Zm9ybVxuIiwiLyoqXG4gKiBBbiB1dGlsaXR5IHNpbmdsZXRvbiB0aGF0IGhvbGRzIGFsbCByZXF1ZXN0QW5pbWF0aW9uRnJhbWUgc3Vic2NyaWJlcnNcbiAqIGFuZCBjYWxscyB0aGVtIGluIHNlcXVlbmNlIGVhY2ggZnJhbWUuXG4gKlxuICogQGNsYXNzIFJBRlxuICogQGV4YW1wbGVcbiAqXG4gKiBpbXBvcnQgUkFGIGZyb20gJ2pzLXV0aWxzJ1xuICpcbiAqIGNvbnN0cnVjdG9yICgpIHtcbiAqICBSQUYuYWRkKHRoaXMub25VcGRhdGUpXG4gKiB9XG4gKlxuICogb25VcGRhdGUgKCkge1xuICogIC8vIGRvIHN0dWZmXG4gKiB9XG4gKlxuICogb25EZXN0cm95ICgpIHtcbiAqICBSQUYucmVtb3ZlKHRoaXMub25VcGRhdGUpXG4gKiB9XG4gKi9cbmNsYXNzIFJBRiB7XG4gIGNvbnN0cnVjdG9yICgpIHtcbiAgICB0aGlzLmxpc3RlbmVycyA9IFtdXG4gICAgdGhpcy5fdXBkYXRlID0gdGhpcy5fdXBkYXRlLmJpbmQodGhpcylcbiAgICB0aGlzLl91cGRhdGUoKVxuICB9XG5cbiAgLyoqXG4gICAqIEFkZHMgYSBzdWJzY3JpYmVyIHRvIGJlIGNhbGxlZCBhdCBlYWNoIHJlcXVlc3RBbmltYXRpb25GcmFtZVxuICAgKlxuICAgKiBAcGFyYW0ge0Z1bmN0aW9ufSBsaXN0ZW5lciBBIHN1YnNjcmliZXIgZnVuY3Rpb25cbiAgICovXG4gIGFkZCAobGlzdGVuZXIpIHtcbiAgICBjb25zdCBpbmRleCA9IHRoaXMubGlzdGVuZXJzLmluZGV4T2YobGlzdGVuZXIpXG4gICAgaWYgKGluZGV4ID09PSAtMSkge1xuICAgICAgdGhpcy5saXN0ZW5lcnMucHVzaChsaXN0ZW5lcilcbiAgICB9XG4gIH1cblxuICAvKipcbiAgICogUmVtb3ZlcyBhIHN1YnNjcmliZXIgZnJvbSByZXF1ZXN0QW5pbWF0aW9uRnJhbWVcbiAgICpcbiAgICogQHBhcmFtIHtGdW5jdGlvbn0gbGlzdGVuZXIgQSBzdWJzY3JpYmVyIGZ1bmN0aW9uXG4gICAqL1xuICByZW1vdmUgKGxpc3RlbmVyKSB7XG4gICAgY29uc3QgaW5kZXggPSB0aGlzLmxpc3RlbmVycy5pbmRleE9mKGxpc3RlbmVyKVxuICAgIGlmIChpbmRleCAhPT0gLTEpIHtcbiAgICAgIHRoaXMubGlzdGVuZXJzLnNwbGljZShpbmRleCwgMSlcbiAgICB9XG4gIH1cblxuICAvKipcbiAgICogQGlnbm9yZVxuICAgKi9cbiAgX3VwZGF0ZSAoKSB7XG4gICAgZm9yIChsZXQgaSA9IDA7IGkgPCB0aGlzLmxpc3RlbmVycy5sZW5ndGg7IGkrKykge1xuICAgICAgdGhpcy5saXN0ZW5lcnNbaV0oKVxuICAgIH1cbiAgICB3aW5kb3cucmVxdWVzdEFuaW1hdGlvbkZyYW1lKHRoaXMuX3VwZGF0ZSlcbiAgfVxufVxuLyoqXG4gKiBAaWdub3JlXG4gKi9cbmV4cG9ydCBkZWZhdWx0IG5ldyBSQUYoKVxuIiwiLyoqXG4gKiBBIGNvbGxlY3Rpb24gb2YgVnVlLmpzIHJlbGF0ZWQgdXRpbGl0aWVzXG4gKlxuICogQG1vZHVsZSBWdWVVdGlsc1xuICovXG5cbmltcG9ydCBrZXlzIGZyb20gJ2xvZGFzaC9rZXlzJ1xuXG4vKipcbiAqIEFsbG93cyB0byBkZWZpbmUgYSBzZXQgb2YgcmVhY3RpdmUgcHJvcGVydGllcyBvZiBcInZhbHVlXCIgKE9iamVjdClcbiAqIGJ5IGxvb2tpbmcgaW50byBcIm1vZGVsXCIgKE9iamVjdCkgYW5kIGNvbXBhcmluZyB0aGUgdHdvXG4gKlxuICogQHBhcmFtIHtPYmplY3R9IHZhbHVlXG4gKiBAcGFyYW0ge09iamVjdH0gbW9kZWxcbiAqL1xuY29uc3QgZGVmaW5lUmVhY3RpdmUgPSBmdW5jdGlvbiAodmFsdWUsIG1vZGVsKSB7XG4gIGNvbnN0IHZhbHVlS2V5cyA9IGtleXModmFsdWUpXG4gIGNvbnN0IG1vZGVsS2V5cyA9IGtleXMobW9kZWwpXG5cbiAgdmFsdWVLZXlzLmZvckVhY2goa2V5ID0+IHtcbiAgICBpZiAobW9kZWxLZXlzLmluZGV4T2Yoa2V5KSA9PT0gLTEpIHtcbiAgICAgIE9iamVjdC5kZWZpbmVQcm9wZXJ0eSh2YWx1ZSwga2V5LCB7IGNvbmZpZ3VyYWJsZTogZmFsc2UgfSlcbiAgICB9IGVsc2Uge1xuICAgICAgLy8gZm91bmQgb24gbW9kZWwsIHJlY3Vyc2VcbiAgICAgIGlmICh0eXBlb2YgbW9kZWxba2V5XSA9PT0gJ29iamVjdCcpIHtcbiAgICAgICAgZGVmaW5lUmVhY3RpdmUodmFsdWVba2V5XSwgbW9kZWxba2V5XSlcbiAgICAgIH1cbiAgICB9XG4gIH0pXG59XG5cbmV4cG9ydCB7IGRlZmluZVJlYWN0aXZlIH1cbiIsImltcG9ydCAqIGFzIGZ1IGZyb20gJy4vRmlsZVV0aWxzJ1xuaW1wb3J0ICogYXMgbnUgZnJvbSAnLi9OdW1iZXJVdGlscydcbmltcG9ydCAqIGFzIG91IGZyb20gJy4vT2JqZWN0VXRpbHMnXG5pbXBvcnQgKiBhcyB2dSBmcm9tICcuL1Z1ZVV0aWxzJ1xuXG5leHBvcnQgeyBkZWZhdWx0IGFzIEVsYXN0aWNOdW1iZXIgfSBmcm9tICcuL0VsYXN0aWNOdW1iZXInXG5leHBvcnQgeyBkZWZhdWx0IGFzIEd5cm9zY29wZSB9IGZyb20gJy4vR3lyb3Njb3BlJ1xuZXhwb3J0IHsgZGVmYXVsdCBhcyBub3JtYWxpemVXaGVlbCB9IGZyb20gJy4vbm9ybWFsaXplV2hlZWwnXG5leHBvcnQgeyBkZWZhdWx0IGFzIFJBRiB9IGZyb20gJy4vUkFGJ1xuZXhwb3J0IHsgZGVmYXVsdCBhcyBQbGF0Zm9ybSB9IGZyb20gJy4vUGxhdGZvcm0nXG5leHBvcnQgY29uc3QgRmlsZVV0aWxzID0gZnVcbmV4cG9ydCBjb25zdCBOdW1iZXJVdGlscyA9IG51XG5leHBvcnQgY29uc3QgVnVlVXRpbHMgPSB2dVxuZXhwb3J0IGNvbnN0IE9iamVjdFV0aWxzID0gb3VcbiIsIi8qKlxuICogQW4gdXRpbGl0eSBmdW5jdGlvbiB0byBub3JtYWxpemUgdGhlIG1vdXNlV2hlZWwgaW5wdXQgaW4gdGhlIGJyb3dzZXIuXG4gKlxuICogQG1vZHVsZSBub3JtYWxpemVXaGVlbFxuICogQHR5cGVjaGVja3NcbiAqL1xuXG4vLyBSZWFzb25hYmxlIGRlZmF1bHRzXG5jb25zdCBQSVhFTF9TVEVQID0gMTBcbmNvbnN0IExJTkVfSEVJR0hUID0gNDBcbmNvbnN0IFBBR0VfSEVJR0hUID0gODAwXG5cbi8qKlxuICogTW91c2Ugd2hlZWwgKGFuZCAyLWZpbmdlciB0cmFja3BhZCkgc3VwcG9ydCBvbiB0aGUgd2ViIHN1Y2tzLiAgSXQgaXNcbiAqIGNvbXBsaWNhdGVkLCB0aHVzIHRoaXMgZG9jIGlzIGxvbmcgYW5kIChob3BlZnVsbHkpIGRldGFpbGVkIGVub3VnaCB0byBhbnN3ZXJcbiAqIHlvdXIgcXVlc3Rpb25zLlxuICpcbiAqIElmIHlvdSBuZWVkIHRvIHJlYWN0IHRvIHRoZSBtb3VzZSB3aGVlbCBpbiBhIHByZWRpY3RhYmxlIHdheSwgdGhpcyBjb2RlIGlzXG4gKiBsaWtlIHlvdXIgYmVzdGVzdCBmcmllbmQuICogaHVncyAqXG4gKlxuICogQXMgb2YgdG9kYXksIHRoZXJlIGFyZSA0IERPTSBldmVudCB0eXBlcyB5b3UgY2FuIGxpc3RlbiB0bzpcbiAqXG4gKiAgICd3aGVlbCcgICAgICAgICAgICAgICAgLS0gQ2hyb21lKDMxKyksIEZGKDE3KyksIElFKDkrKVxuICogICAnbW91c2V3aGVlbCcgICAgICAgICAgIC0tIENocm9tZSwgSUUoNispLCBPcGVyYSwgU2FmYXJpXG4gKiAgICdNb3pNb3VzZVBpeGVsU2Nyb2xsJyAgLS0gRkYoMy41IG9ubHkhKSAoMjAxMC0yMDEzKSAtLSBkb24ndCBib3RoZXIhXG4gKiAgICdET01Nb3VzZVNjcm9sbCcgICAgICAgLS0gRkYoMC45LjcrKSBzaW5jZSAyMDAzXG4gKlxuICogU28gd2hhdCB0byBkbz8gIFRoZSBpcyB0aGUgYmVzdDpcbiAqXG4gKiAgIG5vcm1hbGl6ZVdoZWVsLmdldEV2ZW50VHlwZSgpXG4gKlxuICogSW4geW91ciBldmVudCBjYWxsYmFjaywgdXNlIHRoaXMgY29kZSB0byBnZXQgc2FuZSBpbnRlcnByZXRhdGlvbiBvZiB0aGVcbiAqIGRlbHRhcy4gIFRoaXMgY29kZSB3aWxsIHJldHVybiBhbiBvYmplY3Qgd2l0aCBwcm9wZXJ0aWVzOlxuICpcbiAqICAgc3BpblggICAtLSBub3JtYWxpemVkIHNwaW4gc3BlZWQgKHVzZSBmb3Igem9vbSkgLSB4IHBsYW5lXG4gKiAgIHNwaW5ZICAgLS0gXCIgLSB5IHBsYW5lXG4gKiAgIHBpeGVsWCAgLS0gbm9ybWFsaXplZCBkaXN0YW5jZSAodG8gcGl4ZWxzKSAtIHggcGxhbmVcbiAqICAgcGl4ZWxZICAtLSBcIiAtIHkgcGxhbmVcbiAqXG4gKiBXaGVlbCB2YWx1ZXMgYXJlIHByb3ZpZGVkIGJ5IHRoZSBicm93c2VyIGFzc3VtaW5nIHlvdSBhcmUgdXNpbmcgdGhlIHdoZWVsIHRvXG4gKiBzY3JvbGwgYSB3ZWIgcGFnZSBieSBhIG51bWJlciBvZiBsaW5lcyBvciBwaXhlbHMgKG9yIHBhZ2VzKS4gIFZhbHVlcyBjYW4gbGV0eVxuICogc2lnbmlmaWNhbnRseSBvbiBkaWZmZXJlbnQgcGxhdGZvcm1zIGFuZCBicm93c2VycywgZm9yZ2V0dGluZyB0aGF0IHlvdSBjYW5cbiAqIHNjcm9sbCBhdCBkaWZmZXJlbnQgc3BlZWRzLiAgU29tZSBkZXZpY2VzIChsaWtlIHRyYWNrcGFkcykgZW1pdCBtb3JlIGV2ZW50c1xuICogYXQgc21hbGxlciBpbmNyZW1lbnRzIHdpdGggZmluZSBncmFudWxhcml0eSwgYW5kIHNvbWUgZW1pdCBtYXNzaXZlIGp1bXBzIHdpdGhcbiAqIGxpbmVhciBzcGVlZCBvciBhY2NlbGVyYXRpb24uXG4gKlxuICogVGhpcyBjb2RlIGRvZXMgaXRzIGJlc3QgdG8gbm9ybWFsaXplIHRoZSBkZWx0YXMgZm9yIHlvdTpcbiAqXG4gKiAgIC0gc3BpbiBpcyB0cnlpbmcgdG8gbm9ybWFsaXplIGhvdyBmYXIgdGhlIHdoZWVsIHdhcyBzcHVuIChvciB0cmFja3BhZFxuICogICAgIGRyYWdnZWQpLiAgVGhpcyBpcyBzdXBlciB1c2VmdWwgZm9yIHpvb20gc3VwcG9ydCB3aGVyZSB5b3Ugd2FudCB0b1xuICogICAgIHRocm93IGF3YXkgdGhlIGNodW5reSBzY3JvbGwgc3RlcHMgb24gdGhlIFBDIGFuZCBtYWtlIHRob3NlIGVxdWFsIHRvXG4gKiAgICAgdGhlIHNsb3cgYW5kIHNtb290aCB0aW55IHN0ZXBzIG9uIHRoZSBNYWMuIEtleSBkYXRhOiBUaGlzIGNvZGUgdHJpZXMgdG9cbiAqICAgICByZXNvbHZlIGEgc2luZ2xlIHNsb3cgc3RlcCBvbiBhIHdoZWVsIHRvIDEuXG4gKlxuICogICAtIHBpeGVsIGlzIG5vcm1hbGl6aW5nIHRoZSBkZXNpcmVkIHNjcm9sbCBkZWx0YSBpbiBwaXhlbCB1bml0cy4gIFlvdSdsbFxuICogICAgIGdldCB0aGUgY3JhenkgZGlmZmVyZW5jZXMgYmV0d2VlbiBicm93c2VycywgYnV0IGF0IGxlYXN0IGl0J2xsIGJlIGluXG4gKiAgICAgcGl4ZWxzIVxuICpcbiAqICAgLSBwb3NpdGl2ZSB2YWx1ZSBpbmRpY2F0ZXMgc2Nyb2xsaW5nIERPV04vUklHSFQsIG5lZ2F0aXZlIFVQL0xFRlQuICBUaGlzXG4gKiAgICAgc2hvdWxkIHRyYW5zbGF0ZSB0byBwb3NpdGl2ZSB2YWx1ZSB6b29taW5nIElOLCBuZWdhdGl2ZSB6b29taW5nIE9VVC5cbiAqICAgICBUaGlzIG1hdGNoZXMgdGhlIG5ld2VyICd3aGVlbCcgZXZlbnQuXG4gKlxuICogV2h5IGFyZSB0aGVyZSBzcGluWCwgc3BpblkgKG9yIHBpeGVscyk/XG4gKlxuICogICAtIHNwaW5YIGlzIGEgMi1maW5nZXIgc2lkZSBkcmFnIG9uIHRoZSB0cmFja3BhZCwgYW5kIGEgc2hpZnQgKyB3aGVlbCB0dXJuXG4gKiAgICAgd2l0aCBhIG1vdXNlLiAgSXQgcmVzdWx0cyBpbiBzaWRlLXNjcm9sbGluZyBpbiB0aGUgYnJvd3NlciBieSBkZWZhdWx0LlxuICpcbiAqICAgLSBzcGluWSBpcyB3aGF0IHlvdSBleHBlY3QgLS0gaXQncyB0aGUgY2xhc3NpYyBheGlzIG9mIGEgbW91c2Ugd2hlZWwuXG4gKlxuICogICAtIEkgZHJvcHBlZCBzcGluWi9waXhlbFouICBJdCBpcyBzdXBwb3J0ZWQgYnkgdGhlIERPTSAzICd3aGVlbCcgZXZlbnQgYW5kXG4gKiAgICAgcHJvYmFibHkgaXMgYnkgYnJvd3NlcnMgaW4gY29uanVuY3Rpb24gd2l0aCBmYW5jeSAzRCBjb250cm9sbGVycyAuLiBidXRcbiAqICAgICB5b3Uga25vdy5cbiAqXG4gKiBJbXBsZW1lbnRhdGlvbiBpbmZvOlxuICpcbiAqIEV4YW1wbGVzIG9mICd3aGVlbCcgZXZlbnQgaWYgeW91IHNjcm9sbCBzbG93bHkgKGRvd24pIGJ5IG9uZSBzdGVwIHdpdGggYW5cbiAqIGF2ZXJhZ2UgbW91c2U6XG4gKlxuICogICBPUyBYICsgQ2hyb21lICAobW91c2UpICAgICAtICAgIDQgICBwaXhlbCBkZWx0YSAgKHdoZWVsRGVsdGEgLTEyMClcbiAqICAgT1MgWCArIFNhZmFyaSAgKG1vdXNlKSAgICAgLSAgTi9BICAgcGl4ZWwgZGVsdGEgICh3aGVlbERlbHRhICAtMTIpXG4gKiAgIE9TIFggKyBGaXJlZm94IChtb3VzZSkgICAgIC0gICAgMC4xIGxpbmUgIGRlbHRhICAod2hlZWxEZWx0YSAgTi9BKVxuICogICBXaW44ICsgQ2hyb21lICAobW91c2UpICAgICAtICAxMDAgICBwaXhlbCBkZWx0YSAgKHdoZWVsRGVsdGEgLTEyMClcbiAqICAgV2luOCArIEZpcmVmb3ggKG1vdXNlKSAgICAgLSAgICAzICAgbGluZSAgZGVsdGEgICh3aGVlbERlbHRhIC0xMjApXG4gKlxuICogT24gdGhlIHRyYWNrcGFkOlxuICpcbiAqICAgT1MgWCArIENocm9tZSAgKHRyYWNrcGFkKSAgLSAgICAyICAgcGl4ZWwgZGVsdGEgICh3aGVlbERlbHRhICAgLTYpXG4gKiAgIE9TIFggKyBGaXJlZm94ICh0cmFja3BhZCkgIC0gICAgMSAgIHBpeGVsIGRlbHRhICAod2hlZWxEZWx0YSAgTi9BKVxuICpcbiAqIE9uIG90aGVyL29sZGVyIGJyb3dzZXJzLi4gaXQncyBtb3JlIGNvbXBsaWNhdGVkIGFzIHRoZXJlIGNhbiBiZSBtdWx0aXBsZSBhbmRcbiAqIGFsc28gbWlzc2luZyBkZWx0YSB2YWx1ZXMuXG4gKlxuICogVGhlICd3aGVlbCcgZXZlbnQgaXMgbW9yZSBzdGFuZGFyZDpcbiAqXG4gKiBodHRwOi8vd3d3LnczLm9yZy9UUi9ET00tTGV2ZWwtMy1FdmVudHMvI2V2ZW50cy13aGVlbGV2ZW50c1xuICpcbiAqIFRoZSBiYXNpY3MgaXMgdGhhdCBpdCBpbmNsdWRlcyBhIHVuaXQsIGRlbHRhTW9kZSAocGl4ZWxzLCBsaW5lcywgcGFnZXMpLCBhbmRcbiAqIGRlbHRhWCwgZGVsdGFZIGFuZCBkZWx0YVouICBTb21lIGJyb3dzZXJzIHByb3ZpZGUgb3RoZXIgdmFsdWVzIHRvIG1haW50YWluXG4gKiBiYWNrd2FyZCBjb21wYXRpYmlsaXR5IHdpdGggb2xkZXIgZXZlbnRzLiAgVGhvc2Ugb3RoZXIgdmFsdWVzIGhlbHAgdXNcbiAqIGJldHRlciBub3JtYWxpemUgc3BpbiBzcGVlZC4gIEV4YW1wbGUgb2Ygd2hhdCB0aGUgYnJvd3NlcnMgcHJvdmlkZTpcbiAqXG4gKiAgICAgICAgICAgICAgICAgICAgICAgICAgfCBldmVudC53aGVlbERlbHRhIHwgZXZlbnQuZGV0YWlsXG4gKiAgICAgICAgLS0tLS0tLS0tLS0tLS0tLS0tKy0tLS0tLS0tLS0tLS0tLS0tLSstLS0tLS0tLS0tLS0tLVxuICogICAgICAgICAgU2FmYXJpIHY1L09TIFggIHwgICAgICAgLTEyMCAgICAgICB8ICAgICAgIDBcbiAqICAgICAgICAgIFNhZmFyaSB2NS9XaW43ICB8ICAgICAgIC0xMjAgICAgICAgfCAgICAgICAwXG4gKiAgICAgICAgIENocm9tZSB2MTcvT1MgWCAgfCAgICAgICAtMTIwICAgICAgIHwgICAgICAgMFxuICogICAgICAgICBDaHJvbWUgdjE3L1dpbjcgIHwgICAgICAgLTEyMCAgICAgICB8ICAgICAgIDBcbiAqICAgICAgICAgICAgICAgIElFOS9XaW43ICB8ICAgICAgIC0xMjAgICAgICAgfCAgIHVuZGVmaW5lZFxuICogICAgICAgICBGaXJlZm94IHY0L09TIFggIHwgICAgIHVuZGVmaW5lZCAgICB8ICAgICAgIDFcbiAqICAgICAgICAgRmlyZWZveCB2NC9XaW43ICB8ICAgICB1bmRlZmluZWQgICAgfCAgICAgICAzXG4gKlxuICovXG5mdW5jdGlvbiBub3JtYWxpemVXaGVlbCAoZXZlbnQpIHtcbiAgbGV0IHNYID0gMFxuICBsZXQgc1kgPSAwIC8vIHNwaW5YLCBzcGluWVxuICBsZXQgcFggPSAwXG4gIGxldCBwWSA9IDAgLy8gcGl4ZWxYLCBwaXhlbFlcblxuICAvLyBMZWdhY3lcbiAgaWYgKCdkZXRhaWwnIGluIGV2ZW50KSB7IHNZID0gZXZlbnQuZGV0YWlsIH1cbiAgaWYgKCd3aGVlbERlbHRhJyBpbiBldmVudCkgeyBzWSA9IC1ldmVudC53aGVlbERlbHRhIC8gMTIwIH1cbiAgaWYgKCd3aGVlbERlbHRhWScgaW4gZXZlbnQpIHsgc1kgPSAtZXZlbnQud2hlZWxEZWx0YVkgLyAxMjAgfVxuICBpZiAoJ3doZWVsRGVsdGFYJyBpbiBldmVudCkgeyBzWCA9IC1ldmVudC53aGVlbERlbHRhWCAvIDEyMCB9XG5cbiAgLy8gc2lkZSBzY3JvbGxpbmcgb24gRkYgd2l0aCBET01Nb3VzZVNjcm9sbFxuICBpZiAoJ2F4aXMnIGluIGV2ZW50ICYmIGV2ZW50LmF4aXMgPT09IGV2ZW50LkhPUklaT05UQUxfQVhJUykge1xuICAgIHNYID0gc1lcbiAgICBzWSA9IDBcbiAgfVxuXG4gIHBYID0gc1ggKiBQSVhFTF9TVEVQXG4gIHBZID0gc1kgKiBQSVhFTF9TVEVQXG5cbiAgaWYgKCdkZWx0YVknIGluIGV2ZW50KSB7IHBZID0gZXZlbnQuZGVsdGFZIH1cbiAgaWYgKCdkZWx0YVgnIGluIGV2ZW50KSB7IHBYID0gZXZlbnQuZGVsdGFYIH1cblxuICBpZiAoKHBYIHx8IHBZKSAmJiBldmVudC5kZWx0YU1vZGUpIHtcbiAgICBpZiAoZXZlbnQuZGVsdGFNb2RlID09PSAxKSB7IC8vIGRlbHRhIGluIExJTkUgdW5pdHNcbiAgICAgIHBYICo9IExJTkVfSEVJR0hUXG4gICAgICBwWSAqPSBMSU5FX0hFSUdIVFxuICAgIH0gZWxzZSB7IC8vIGRlbHRhIGluIFBBR0UgdW5pdHNcbiAgICAgIHBYICo9IFBBR0VfSEVJR0hUXG4gICAgICBwWSAqPSBQQUdFX0hFSUdIVFxuICAgIH1cbiAgfVxuXG4gIC8vIEZhbGwtYmFjayBpZiBzcGluIGNhbm5vdCBiZSBkZXRlcm1pbmVkXG4gIGlmIChwWCAmJiAhc1gpIHsgc1ggPSAocFggPCAxKSA/IC0xIDogMSB9XG4gIGlmIChwWSAmJiAhc1kpIHsgc1kgPSAocFkgPCAxKSA/IC0xIDogMSB9XG5cbiAgcmV0dXJuIHtcbiAgICBzcGluWDogc1gsXG4gICAgc3Bpblk6IHNZLFxuICAgIHBpeGVsWDogcFgsXG4gICAgcGl4ZWxZOiBwWVxuICB9XG59XG5cbi8vIC8qKlxuLy8gICogVGhlIGJlc3QgY29tYmluYXRpb24gaWYgeW91IHByZWZlciBzcGluWCArIHNwaW5ZIG5vcm1hbGl6YXRpb24uICBJdCBmYXZvcnNcbi8vICAqIHRoZSBvbGRlciBET01Nb3VzZVNjcm9sbCBmb3IgRmlyZWZveCwgYXMgRkYgZG9lcyBub3QgaW5jbHVkZSB3aGVlbERlbHRhIHdpdGhcbi8vICAqICd3aGVlbCcgZXZlbnQsIG1ha2luZyBzcGluIHNwZWVkIGRldGVybWluYXRpb24gaW1wb3NzaWJsZS5cbi8vICAqL1xuLy8gY29uc3QgZ2V0RXZlbnRUeXBlID0gZnVuY3Rpb24gKCkge1xuLy8gICByZXR1cm4gKFVzZXJBZ2VudF9ERVBSRUNBVEVELmZpcmVmb3goKSlcbi8vICAgICA/ICdET01Nb3VzZVNjcm9sbCdcbi8vICAgICA6IChpc0V2ZW50U3VwcG9ydGVkKCd3aGVlbCcpKVxuLy8gICAgICAgPyAnd2hlZWwnXG4vLyAgICAgICA6ICdtb3VzZXdoZWVsJ1xuLy8gfVxuXG5leHBvcnQgZGVmYXVsdCBub3JtYWxpemVXaGVlbFxuIl0sInNvdXJjZVJvb3QiOiIifQ==