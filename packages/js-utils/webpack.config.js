/* global __dirname require, module */
const path = require('path')
const glob = require('glob')
const env = require('yargs').argv.env // use --env with webpack 2
const pkg = require('./package.json')
const TerserPlugin = require('terser-webpack-plugin')
const merge = require('webpack-merge')

const libraryName = pkg.name.split('/')[1]
// let outputFile
let mode

if (env === 'build') {
  mode = 'production'
} else {
  mode = 'development'
}

const baseConfig = {
  mode: mode,
  entry: path.join(__dirname, '/src/index.js'),
  devtool: mode === 'development' ? 'inline-source-map' : false,
  output: {
    path: path.join(__dirname, '/lib'),
    filename: 'outputFile',
    library: libraryName,
    libraryTarget: 'umd',
    umdNamedDefine: true,
    globalObject: "typeof self !== 'undefined' ? self : this"
  },
  optimization: {
    minimizer: mode === 'production' ? [new TerserPlugin({
      cache: true,
      parallel: true,
      sourceMap: true
    })] : []
  },
  module: {
    rules: [
      {
        test: /(\.jsx|\.js)$/,
        loader: 'babel-loader',
        exclude: /node_modules/
      },
      {
        test: /(\.jsx|\.js)$/,
        loader: 'eslint-loader',
        exclude: /node_modules/
      }
    ]
  }
}

const files = glob.sync('src/**/*.js').concat(glob.sync('src/**/*.vue')).concat(glob.sync('src/**/*.jsx'))
const config = []
files.forEach(file => {
  const baseName = path.basename(file).split('.')
  const filename = baseName[0] + (mode === 'production' ? '.min.' + baseName[1] : '.' + baseName[1])
  config.push(merge(baseConfig, {
    entry: path.join(__dirname, file),
    output: {
      filename
    }
  }))
})

module.exports = config
