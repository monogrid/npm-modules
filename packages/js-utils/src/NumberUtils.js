/**
 * A collection of Number related utilities
 *
 * @module NumberUtils
 */

/**
  * Linearly interpolates a Number
  *
  * @param {Number} v0 Initial value
  * @param {Number} v1 Final value
  * @param {Number} t zero to one
  * @returns {Number} Interpolation between v0 and v1 based on t
  */
const lerpNumber = function (v0, v1, t) {
  return v0 + t * (v1 - v0)
}

/**
 * TODO: Documentation
 *
 * @param {*} oldValue
 * @param {*} oldMin
 * @param {*} oldMax
 * @param {*} newMin
 * @param {*} newMax
 * @param {*} clamped
 */
const range = function (oldValue, oldMin, oldMax, newMin, newMax, clamped) {
  const oldRange = oldMax - oldMin
  const newRange = newMax - newMin
  const newValue = ((oldValue - oldMin) * newRange) / oldRange + newMin

  if (clamped) {
    return clamp(newValue, newMin, newMax)
  }

  return newValue
}
/**
 * Clamps a value between min and max values
 *
 * @param {Number} value - The value to be clamped.
 * @param {Number} min - Minimum value.
 * @param {Number} max - Maximum value.
 * @returns {Number} A number clamped between min and max
 */
const clamp = (value, min = 0, max = 1) => Math.max(min, Math.min(value, max))
/**
 * TODO: Documentation
 *
 * @param {Number} value
 * @param {Number} min
 * @param {Number} max
 */
const map = (value, min, max) => clamp((value - min) / (max - min), 0, 1)
/**
 * TODO: Documentation
 *
 * @param {Number} alpha
 * @param {Number} beta
 */
const angleDistance = function (alpha, beta) {
  const phi = (Math.abs(beta - alpha) % Math.PI) * 2
  return phi > Math.PI ? Math.PI * 2 - phi : phi
}
/**
 * TODO: Documentation
 *
 * @param {Number} alpha
 * @param {Number} beta
 */
const angleDistanceSign = function (alpha, beta) {
  const sign =
    (alpha - beta >= 0 && alpha - beta <= Math.PI) ||
    (alpha - beta <= -Math.PI && alpha - beta >= -Math.PI * 2)
      ? 1
      : -1
  return angleDistance(alpha, beta) * sign
}
/**
 * Smoothstep implementation.
 * https://en.wikipedia.org/wiki/Smoothstep
 *
 * @param {Number} min - Initial value
 * @param {Number} max - Final Value
 * @param {Number} value -
 * @returns {Number} Value of the interpolation
 */
const smoothstep = (min, max, value) => {
  const x = Math.max(0, Math.min(1, (value - min) / (max - min)))
  return x * x * (3 - 2 * x)
}
/**
 * Similar to linear interpolation.
 *
 * @param {Number} value1 - Minimum value.
 * @param {Number} value2 - Maximum value.
 * @param {Number} percent - zero to one value percent
 */
const mix = (value1, value2, percent) => value1 * (1 - percent) + value2 * percent
/**
 * Returns the sign of a number in -1,0,1 form.
 *
 * @param {Number} value - a number to be tested
 * @returns {Number} 0 if value is zero, -1 if value is less than 0, 1 otherwise
 */
const sign = value => (value !== 0 ? (value < 0 ? -1 : 1) : 0)
/**
 * TODO: Documentation
 *
 * @param {Number} min
 * @param {Number} max
 * @param {Number} power
 * @param {Number} [rd = Math.random] - Optional Random number generator
 */
const randByPower = function (min, max, power, rd = Math.random) {
  const value = rd() * (max - min) + min
  return mapToPower(value, min, max, power)
}
/**
 * TODO: Documentation
 *
 * @param {Number} value
 * @param {Number} min
 * @param {Number} max
 * @param {Number} power
 */
const mapToPower = function (value, min, max, power) {
  const valueSign = sign(value)

  value = (value - min) / (max - min)
  value = valueSign > 0 ? value : value - 1
  value = Math.pow(value * valueSign, Math.abs(power))
  value = valueSign > 0 ? value : 1 - value

  return value * (max - min) + min
}
/**
 * Computes distance between two points in 2D space.
 *
 * @param {Number} x1 - x coordinate of first point
 * @param {Number} y1 - y coordinate of first point
 * @param {Number} x2 - x coordinate of second point
 * @param {Number} y2 - y coordinate of second point
 * @returns {Number} Distance between x1,y1 and x2,y2
 */
const distance = function (x1, y1, x2, y2) {
  const dx = x1 - x2
  const dy = y1 - y2
  return Math.sqrt(dx * dx + dy * dy)
}

/**
 * Not a real distance calculation.
 * Useful to sort objects by distance ( much faster because no sqrt )
 *
 * @param {Number} x1 - x coordinate of first point
 * @param {Number} y1 - y coordinate of first point
 * @param {Number} x2 - x coordinate of second point
 * @param {Number} y2 - y coordinate of second point
 * @returns {Number} bogus distance between x1,y1 and x2,y2. DO NOT USE WHEN A REAL DISTANCE IS NEEDED
 */
const distanceCompare = function (x1, y1, x2, y2) {
  const dx = x1 - x2
  const dy = y1 - y2
  return dx * dx + dy * dy
}

export {
  lerpNumber,
  distanceCompare,
  distance,
  mapToPower,
  randByPower,
  smoothstep,
  angleDistanceSign,
  angleDistance,
  map,
  clamp,
  range,
  mix
}
