/**
 * A collection of Object related utilities
 *
 * @module ObjectUtils
 */

/**
 * Determines if the object has been constructed using
 * javascript generic {} syntax
 *
 * @param {*} o - The object to be tested.
 * @returns {Boolean} true if the object is a plain object, false otherwise.
 */
const isPlainObject = function (o) {
  return o && (typeof o === 'object') && o.constructor === Object
}

export { isPlainObject }
