
const MobileDetect = require('mobile-detect')
const md = new MobileDetect(window.navigator.userAgent)

const volumeTest = document.createElement('audio')
volumeTest.volume = 0.5

/**
 * Holds information on the Platform where this code is run
 *
 * @module Platform
 * @property {Boolean} mobile - Device is a mobile (includes tablets and phones)
 * @property {Boolean} phone - Device is a phone (excludes tablets)
 * @property {Boolean} tablet - Device is a tablet (excludes phones)
 * @property {Boolean} android - Device is Android based
 * @property {Boolean} ios - Device is iOS based
 * @property {Boolean} ipad - Device is an iPad
 * @property {Boolean} iphone - Device is an iPhone
 * @property {Boolean} wphone - Device is an Windows Phone
 * @property {Boolean} edge - Browser is Microsoft Edge
 * @property {Boolean} firefox - Browser is Mozilla Firefox
 * @property {Boolean} ie11 - Browser is Microsoft Internet Explorer 11
 * @property {Boolean} safari - Browser is Safari
 * @property {Boolean} prerenderer - Page is visited by a prerenderer (like Phantom JS)
 * @property {Boolean} volume - Device supports volume setting via js (iOS doesn't support this)
 */
const Platform = {
  mobile: !!md.mobile(),
  phone: !!md.phone(),
  tablet: !!md.tablet(),
  android: !!md.is('AndroidOS'),
  ios: !!md.is('iOS'),
  ipad: !!md.is('iPad'),
  iphone: !!md.is('iPhone'),
  wphone: !!md.is('WindowsPhoneOS'),
  edge: !!/Edge\/\d+/i.test(window.navigator.userAgent),
  firefox: md.version('Gecko') > 1,
  ie11: !!/Trident.*rv:11\./i.test(window.navigator.userAgent),
  safari: /Safari/.test(window.navigator.userAgent) && /Apple Computer/.test(window.navigator.vendor),
  prerenderer: window.__PRERENDER_INJECTED !== undefined, // /PhantomJS/.test(window.navigator.userAgent),
  volume: volumeTest.volume === 0.5
}

// if not prerenderer
if (!Platform.prerenderer && window.Modernizr) {
  // add custom modernizr tests
  for (const key in Platform) {
    window.Modernizr.addTest(key, () => {
      return Platform[key]
    })
  }
}

export default Platform
