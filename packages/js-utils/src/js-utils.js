import * as fu from './FileUtils'
import * as nu from './NumberUtils'
import * as ou from './ObjectUtils'
import * as vu from './VueUtils'

export { default as ElasticNumber } from './ElasticNumber'
export { default as Gyroscope } from './Gyroscope'
export { default as normalizeWheel } from './normalizeWheel'
export { default as RAF } from './RAF'
export { default as Platform } from './Platform'
export const FileUtils = fu
export const NumberUtils = nu
export const VueUtils = vu
export const ObjectUtils = ou
