/**
 * An utility singleton that holds all requestAnimationFrame subscribers
 * and calls them in sequence each frame.
 *
 * @class RAF
 * @example
 *
 * import RAF from 'js-utils'
 *
 * constructor () {
 *  RAF.add(this.onUpdate)
 * }
 *
 * onUpdate () {
 *  // do stuff
 * }
 *
 * onDestroy () {
 *  RAF.remove(this.onUpdate)
 * }
 */
class RAF {
  constructor () {
    this.listeners = []
    this._update = this._update.bind(this)
    this._update()
  }

  /**
   * Adds a subscriber to be called at each requestAnimationFrame
   *
   * @param {Function} listener A subscriber function
   */
  add (listener) {
    const index = this.listeners.indexOf(listener)
    if (index === -1) {
      this.listeners.push(listener)
    }
  }

  /**
   * Removes a subscriber from requestAnimationFrame
   *
   * @param {Function} listener A subscriber function
   */
  remove (listener) {
    const index = this.listeners.indexOf(listener)
    if (index !== -1) {
      this.listeners.splice(index, 1)
    }
  }

  /**
   * @ignore
   */
  _update () {
    for (let i = 0; i < this.listeners.length; i++) {
      this.listeners[i]()
    }
    window.requestAnimationFrame(this._update)
  }
}
/**
 * @ignore
 */
export default new RAF()
