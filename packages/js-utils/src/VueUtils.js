/**
 * A collection of Vue.js related utilities
 *
 * @module VueUtils
 */

import keys from 'lodash/keys'

/**
 * Allows to define a set of reactive properties of "value" (Object)
 * by looking into "model" (Object) and comparing the two
 *
 * @param {Object} value
 * @param {Object} model
 */
const defineReactive = function (value, model) {
  const valueKeys = keys(value)
  const modelKeys = keys(model)

  valueKeys.forEach(key => {
    if (modelKeys.indexOf(key) === -1) {
      Object.defineProperty(value, key, { configurable: false })
    } else {
      // found on model, recurse
      if (typeof model[key] === 'object') {
        defineReactive(value[key], model[key])
      }
    }
  })
}

export { defineReactive }
