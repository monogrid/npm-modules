import RAF from './RAF'
require('@hughsk/fulltilt/dist/fulltilt')
/**
 * An utility singleton that returns a device's Gyroscope
 * data in the form of orientationX and orientationY.
 *
 * The values are updated every frame and the system stops updating
 * if it determines that the device does not have gyroscope capabilities.
 *
 * @class Gyroscope
 * @property {Number} orientationX
 * @property {Number} orientationY
 * @property {Boolean} enabled
 */
class Gyroscope {
  /**
   * @ignore
   */
  constructor () {
    this.orientationX = 0
    this.orientationY = 0
    this.enabled = true
    this._update = this._update.bind(this)
    RAF.add(this._update)
  }

  /**
   * @ignore
   */
  _update () {
    const fulltiltPromise = new window.FULLTILT.getDeviceOrientation({ type: 'game' }) // eslint-disable-line new-cap

    fulltiltPromise.then((deviceOrientation) => {
      const euler = deviceOrientation.getScreenAdjustedEuler()

      this.orientationX = euler.beta
      this.orientationY = euler.gamma

      if (this.orientationX > 90) {
        this.orientationY = -this.orientationY
      }
      return null
      // console.log(this.orientationX, this.orientationY)
    }).catch((message) => {
      console.warn('Disabling Gyroscope, reason:', message)
      this.enabled = false
      RAF.remove(this._update)
    })
  }
}
/**
 * @ignore
 */
export default Gyroscope
