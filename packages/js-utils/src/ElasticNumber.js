/**
 * An Utility class that allows to tween a numeric value (_target_) into a smoothed value (_value_)
 *
 *
 * @class ElasticNumber
 * @property {Number} value - The real value of the number.
 * @property {Number} target - The desired value of the number.
 * @property {Number} speed - The speed at which the ElasticNumber will make value reach target.
 * @example
 *
 * constructor () {
 *  this.myNumber = new ElasticNumber();
 *  this.time = new Date().getTime()
 * }
 *
 * onUpdate () {
 *  const now = new Date().getTime()
 *  const delta = now - this.time
 *  this.myNumber.target = Math.random() * 100
 *  this.myNumber.update(delta);
 *
 *  this.mySprite.x = this.myNumber.value
 *  this.time = now
 * }
 */
export default class ElasticNumber {
  /**
   * Initializes the ElasticNumber with a
   * @ignore
   * @param {Number} value predefined value (0 by default)
   * @memberOf ElasticNumber
   */
  constructor (value = 0) {
    this.value = value
    this.target = value
    this.speed = 3
  }

  /**
   * Updates the ElasticNumber on tick.
   * Value will be updated from target.
   *
   * @param {Number} delta delta time in milliseconds
   * @memberOf ElasticNumber
   */
  update (delta) {
    // delta = Math.min(delta, 1 / 20);

    const dist = this.target - this.value

    this.value += dist * (this.speed * delta)
  }
}
