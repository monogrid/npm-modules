/**
 * A collection of File related utilities.
 *
 * @module FileUtils
 */

/**
 * Downloads a file to the user's machine.
 *
 * @param {String} content - The content to be downloaded.
 * @param {String} fileName - The name of the file downloaded into the user's PC.
 * @param {String} contentType - The file type.
 */
const downloadFile = function (content, fileName, contentType) {
  const a = document.createElement('a')
  const file = new Blob([content], { type: contentType })
  a.href = URL.createObjectURL(file)
  a.download = fileName
  a.click()
}

/**
 * Prompts the user to select a single file from his harddrive.
 *
 * @param {String} [accept] - Accept string to restrict file selection to certain file types.
 * @returns {Promise<Array>} Array of files selected by the user.
 */
const loadFile = function (accept) {
  return new Promise((resolve, reject) => {
    const input = document.createElement('input')
    input.type = 'file'
    input.accept = accept
    input.onchange = (event) => {
      resolve(event.target.files)
    }
    input.click()
  })
}

/**
 * Prompts the user to select a single JSON file from his harddrive
 * and returns the result of his selection.
 *
 * @returns {Promise<String>} The parsed JSON file.
 */
const loadJSON = function () {
  return new Promise((resolve, reject) => {
    loadFile('.json').then(files => {
      const fr = new FileReader()
      fr.onload = (event) => {
        resolve(JSON.parse(event.target.result))
      }
      fr.readAsText(files[0])
      return true
    }).catch(e => { throw e })
  })
}
/**
 * Prompts the user to select a single JSON file from his harddrive
 * and returns the result of his selection
 *
 * @returns {Promise<Image>} The selected image object.
 */
const loadImage = function () {
  return new Promise((resolve, reject) => {
    const img = new Image()
    img.onload = (event) => {
      resolve(img)
    }

    loadFile('image/*').then(files => {
      const fr = new FileReader()
      fr.onload = (event) => {
        img.src = event.target.result
      }
      fr.readAsDataURL(files[0])
      return true
    }).catch(e => { throw e })
  })
}

export {
  downloadFile,
  loadFile,
  loadJSON,
  loadImage
}
