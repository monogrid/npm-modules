module.exports = {
  presets: [
    ['@babel/env', {
      modules: false,
      targets: ['> 1%', 'last 2 versions', 'not ie <= 10']
    }]
  ],
  sourceType: 'unambiguous',
  plugins: [
    '@babel/plugin-transform-runtime', // babel-plugin-transform-runtime
    '@babel/plugin-syntax-dynamic-import', // https://babeljs.io/docs/en/babel-plugin-syntax-dynamic-import
    'babel-plugin-add-module-exports' // https://www.npmjs.com/package/babel-plugin-add-module-exports
  ]
}
