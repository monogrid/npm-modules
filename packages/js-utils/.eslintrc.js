const path = require('path')

// https://eslint.org/docs/user-guide/configuring
module.exports = {
  root: true,
  parserOptions: {
    parser: 'babel-eslint',
    sourceType: 'module'
  },
  env: {
    browser: true,
  },
  extends: [
    // https://github.com/standard/standard/blob/master/docs/RULES-en.md
    'standard'
  ],
  plugins: [
    'import',
    'promise',
    'compat',
  ],
  settings: {
    // this gets confused and we don't need it
    // 'import/resolver': {
    //   webpack: {
    //     config: path.join(__dirname, './webpack.config.js')
    //   }
    // },
    polyfills: [
      // Example of marking entire API and all methods and properties as polyfilled
      "Promise"
    ]
  },
  // add your custom rules here
  rules: {
    // allow async-await
    'generator-star-spacing': 'off',
    // allow debugger during development
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off'
  }
}
