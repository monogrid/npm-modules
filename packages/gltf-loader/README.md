# Webpack GLTF loader

> A webpack loader for ``.gltf`` files, should automatically bundles all referenced files.

Based on the work by [Magnus Bergman](https://github.com/magnus-bergman) but rewritten almost from scratch.

https://www.npmjs.com/package/gltf-loader this is his original loader that didn't work for me when I tried so I decided to rewrite it.

(it also doesn't have a valid github repository to contribute to, as of today, 10/05/2018)

**WARNING: this is pretty much untested and alpha version, contributions appreciated**

## Installation

```bash
npm install --save-dev @monogrid/gltf-loader
```

or

```bash
yarn add --dev @monogrid/gltf-loader
```

## Webpack configuration 

``` js
rules: [
  // IMPORTANT: if you have a loader for Image files (you would, normally)
  // you NEED to add an exclude option IN IT for GLTF images
  {
    // following is an example of YOUR loader config
    test: /\.(png|jpe?g|gif)(\?.*)?$/,
    // here I decided to put all my gltf files under a folder named 'gltf'
    // so I added and exclude rule to my existing loader
    exclude: /gltf/, // only add this line
    // (etc)
    loader: 'url-loader',
    options: {
      limit: 10000,
      name: 'img/[name].[hash:7].[ext]'
    }
  },
  // GLTF configuration: add this to rules
  {
    // match all .gltf files
    test: /\.(gltf)$/,
    loader: '@monogrid/gltf-loader'
  },
  {
    // here I match only IMAGE and BIN files under the gltf folder
    test: /gltf.*\.(bin|png|jpe?g|gif)$/,
    // or use url-loader if you would like to embed images in the source gltf
    loader: 'file-loader',
    options: {
      // output folder for bin and image files, configure as needed
      name: 'gltf/[name].[hash:7].[ext]'
    }
  }
  // end GLTF configuration
]
```

## Usage Example in Three.js

 ``` js
 // please notice the file and all its dependencies 
 // are under a folder named 'gltf' as per webpack config
import gltfFile from 'assets/gltf/some.gltf';
// per configuration only the GLTF json het embedded in the source
// .bin and .png images are processed and outputted by webpack

var loader = new GLTFLoader()
loader.parse(shieldGLTF, '', (gltf) => {
  // here we go
  let scene = gltf.scene
})
 ```


## CHANGELOG

### 0.1.0
 * dropped support for nodejs8.x
 * loader now uses es6 exports, can be forced to export a CommonJS module with "moduleExport" option set to "CommonJS" 

## LICENSE
Copyright (c) 2020, MONOGRID S.R.L.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
